#define nx_struct struct
#define nx_union union
#define dbg(mode, format, ...) ((void)0)
#define dbg_clear(mode, format, ...) ((void)0)
#define dbg_active(mode) 0
# 152 "/opt/local/lib/gcc/avr/4.1.2/include/stddef.h" 3
typedef int ptrdiff_t;
#line 214
typedef unsigned int size_t;
#line 326
typedef int wchar_t;
# 8 "/opt/local/lib/ncc/deputy_nodeputy.h"
struct __nesc_attr_nonnull {
#line 8
  int dummy;
}  ;
#line 9
struct __nesc_attr_bnd {
#line 9
  void *lo, *hi;
}  ;
#line 10
struct __nesc_attr_bnd_nok {
#line 10
  void *lo, *hi;
}  ;
#line 11
struct __nesc_attr_count {
#line 11
  int n;
}  ;
#line 12
struct __nesc_attr_count_nok {
#line 12
  int n;
}  ;
#line 13
struct __nesc_attr_one {
#line 13
  int dummy;
}  ;
#line 14
struct __nesc_attr_one_nok {
#line 14
  int dummy;
}  ;
#line 15
struct __nesc_attr_dmemset {
#line 15
  int a1, a2, a3;
}  ;
#line 16
struct __nesc_attr_dmemcpy {
#line 16
  int a1, a2, a3;
}  ;
#line 17
struct __nesc_attr_nts {
#line 17
  int dummy;
}  ;
# 121 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdint.h" 3
typedef int int8_t __attribute((__mode__(__QI__))) ;
typedef unsigned int uint8_t __attribute((__mode__(__QI__))) ;
typedef int int16_t __attribute((__mode__(__HI__))) ;
typedef unsigned int uint16_t __attribute((__mode__(__HI__))) ;
typedef int int32_t __attribute((__mode__(__SI__))) ;
typedef unsigned int uint32_t __attribute((__mode__(__SI__))) ;

typedef int int64_t __attribute((__mode__(__DI__))) ;
typedef unsigned int uint64_t __attribute((__mode__(__DI__))) ;
#line 142
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
#line 159
typedef int8_t int_least8_t;




typedef uint8_t uint_least8_t;




typedef int16_t int_least16_t;




typedef uint16_t uint_least16_t;




typedef int32_t int_least32_t;




typedef uint32_t uint_least32_t;







typedef int64_t int_least64_t;






typedef uint64_t uint_least64_t;
#line 213
typedef int8_t int_fast8_t;




typedef uint8_t uint_fast8_t;




typedef int16_t int_fast16_t;




typedef uint16_t uint_fast16_t;




typedef int32_t int_fast32_t;




typedef uint32_t uint_fast32_t;







typedef int64_t int_fast64_t;






typedef uint64_t uint_fast64_t;
#line 273
typedef int64_t intmax_t;




typedef uint64_t uintmax_t;
# 77 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/inttypes.h" 3
typedef int32_t int_farptr_t;



typedef uint32_t uint_farptr_t;
# 431 "/opt/local/lib/ncc/nesc_nx.h"
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nx_int8_t;typedef int8_t __nesc_nxbase_nx_int8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nx_int16_t;typedef int16_t __nesc_nxbase_nx_int16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nx_int32_t;typedef int32_t __nesc_nxbase_nx_int32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nx_int64_t;typedef int64_t __nesc_nxbase_nx_int64_t  ;
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nx_uint8_t;typedef uint8_t __nesc_nxbase_nx_uint8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nx_uint16_t;typedef uint16_t __nesc_nxbase_nx_uint16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nx_uint32_t;typedef uint32_t __nesc_nxbase_nx_uint32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nx_uint64_t;typedef uint64_t __nesc_nxbase_nx_uint64_t  ;


typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nxle_int8_t;typedef int8_t __nesc_nxbase_nxle_int8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nxle_int16_t;typedef int16_t __nesc_nxbase_nxle_int16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nxle_int32_t;typedef int32_t __nesc_nxbase_nxle_int32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nxle_int64_t;typedef int64_t __nesc_nxbase_nxle_int64_t  ;
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nxle_uint8_t;typedef uint8_t __nesc_nxbase_nxle_uint8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nxle_uint16_t;typedef uint16_t __nesc_nxbase_nxle_uint16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nxle_uint32_t;typedef uint32_t __nesc_nxbase_nxle_uint32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nxle_uint64_t;typedef uint64_t __nesc_nxbase_nxle_uint64_t  ;
# 71 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdlib.h" 3
#line 68
typedef struct __nesc_unnamed4242 {
  int quot;
  int rem;
} div_t;





#line 74
typedef struct __nesc_unnamed4243 {
  long quot;
  long rem;
} ldiv_t;


typedef int (*__compar_fn_t)(const void *arg_0x10076d370, const void *arg_0x10076d648);
# 71 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
typedef unsigned char bool;






enum __nesc_unnamed4244 {
  FALSE = 0, 
  TRUE = 1
};


uint16_t TOS_LOCAL_ADDRESS = 1;





uint8_t TOS_ROUTE_PROTOCOL = 0x90;
#line 104
uint8_t TOS_BASE_STATION = 0;



const uint8_t TOS_DATA_LENGTH = 70;
#line 120
uint8_t TOS_PLATFORM = 3;
#line 143
enum __nesc_unnamed4245 {
  FAIL = 0, 
  SUCCESS = 1
};


static inline uint8_t rcombine(uint8_t r1, uint8_t r2);
typedef uint8_t result_t  ;







static inline result_t rcombine(result_t r1, result_t r2);
#line 171
static inline result_t rcombine4(result_t r1, result_t r2, result_t r3, 
result_t r4);





enum __nesc_unnamed4246 {
  NULL = 0x0
};
# 57 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/avr/fuse.h" 3
#line 52
typedef struct __nesc_unnamed4247 {

  unsigned char low;
  unsigned char high;
  unsigned char extended;
} __fuse_t;
# 210 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/avr/pgmspace.h" 3
typedef void prog_void __attribute((__progmem__)) ;
typedef char prog_char __attribute((__progmem__)) ;
typedef unsigned char prog_uchar __attribute((__progmem__)) ;

typedef int8_t prog_int8_t __attribute((__progmem__)) ;
typedef uint8_t prog_uint8_t __attribute((__progmem__)) ;
typedef int16_t prog_int16_t __attribute((__progmem__)) ;
typedef uint16_t prog_uint16_t __attribute((__progmem__)) ;
typedef int32_t prog_int32_t __attribute((__progmem__)) ;
typedef uint32_t prog_uint32_t __attribute((__progmem__)) ;

typedef int64_t prog_int64_t __attribute((__progmem__)) ;
typedef uint64_t prog_uint64_t __attribute((__progmem__)) ;
# 117 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/avrhardware.h"
enum __nesc_unnamed4248 {
  TOSH_period16 = 0x00, 
  TOSH_period32 = 0x01, 
  TOSH_period64 = 0x02, 
  TOSH_period128 = 0x03, 
  TOSH_period256 = 0x04, 
  TOSH_period512 = 0x05, 
  TOSH_period1024 = 0x06, 
  TOSH_period2048 = 0x07
};

static inline void TOSH_wait();







typedef uint8_t __nesc_atomic_t;

__nesc_atomic_t __nesc_atomic_start(void );
void __nesc_atomic_end(__nesc_atomic_t oldSreg);



__inline __nesc_atomic_t __nesc_atomic_start(void )  ;






__inline void __nesc_atomic_end(__nesc_atomic_t oldSreg)  ;




static __inline void __nesc_atomic_sleep();









static __inline void __nesc_enable_interrupt();
# 83 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420Const.h"
uint8_t TOS_CC2420_TXPOWER = 0x1F;
uint8_t TOS_CC2420_CHANNEL = 11;
#line 267
enum __nesc_unnamed4249 {
  CP_MAIN = 0, 
  CP_MDMCTRL0, 
  CP_MDMCTRL1, 
  CP_RSSI, 
  CP_SYNCWORD, 
  CP_TXCTRL, 
  CP_RXCTRL0, 
  CP_RXCTRL1, 
  CP_FSCTRL, 
  CP_SECCTRL0, 
  CP_SECCTRL1, 
  CP_BATTMON, 
  CP_IOCFG0, 
  CP_IOCFG1
};
#line 298
static __inline void TOSH_uwait2(int u_sec);
# 102 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline void TOSH_uwait(int u_sec);
#line 117
static __inline void TOSH_SET_RED_LED_PIN();
#line 117
static __inline void TOSH_CLR_RED_LED_PIN();
#line 117
static __inline void TOSH_MAKE_RED_LED_OUTPUT();
static __inline void TOSH_SET_GREEN_LED_PIN();
#line 118
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT();
static __inline void TOSH_SET_YELLOW_LED_PIN();
#line 119
static __inline void TOSH_CLR_YELLOW_LED_PIN();
#line 119
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT();

static __inline void TOSH_CLR_SERIAL_ID_PIN();
#line 121
static __inline void TOSH_MAKE_SERIAL_ID_INPUT();
#line 142
static __inline void TOSH_SET_CC_RSTN_PIN();
#line 142
static __inline void TOSH_CLR_CC_RSTN_PIN();
#line 142
static __inline void TOSH_MAKE_CC_RSTN_OUTPUT();
static __inline void TOSH_SET_CC_VREN_PIN();
#line 143
static __inline void TOSH_MAKE_CC_VREN_OUTPUT();


static __inline void TOSH_MAKE_CC_FIFOP1_INPUT();

static __inline void TOSH_MAKE_CC_CCA_INPUT();
static __inline int TOSH_READ_CC_SFD_PIN();
#line 149
static __inline void TOSH_MAKE_CC_SFD_INPUT();
static __inline void TOSH_SET_CC_CS_PIN();
#line 150
static __inline void TOSH_CLR_CC_CS_PIN();
#line 150
static __inline void TOSH_MAKE_CC_CS_OUTPUT();
#line 150
static __inline void TOSH_MAKE_CC_CS_INPUT();
static __inline int TOSH_READ_CC_FIFO_PIN();
#line 151
static __inline void TOSH_MAKE_CC_FIFO_INPUT();

static __inline int TOSH_READ_RADIO_CCA_PIN();
#line 153
static __inline void TOSH_MAKE_RADIO_CCA_INPUT();


static __inline void TOSH_SET_FLASH_SELECT_PIN();
#line 156
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT();
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT();
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT();









static __inline void TOSH_MAKE_MOSI_OUTPUT();
static __inline void TOSH_MAKE_MISO_INPUT();

static __inline void TOSH_MAKE_SPI_SCK_OUTPUT();


static __inline void TOSH_MAKE_PW0_OUTPUT();
static __inline void TOSH_MAKE_PW1_OUTPUT();
static __inline void TOSH_MAKE_PW2_OUTPUT();
static __inline void TOSH_MAKE_PW3_OUTPUT();
static __inline void TOSH_MAKE_PW4_OUTPUT();
static __inline void TOSH_MAKE_PW5_OUTPUT();
static __inline void TOSH_MAKE_PW6_OUTPUT();
static __inline void TOSH_MAKE_PW7_OUTPUT();
#line 212
static inline void TOSH_SET_PIN_DIRECTIONS(void );
#line 265
enum __nesc_unnamed4250 {
  TOSH_ADC_PORTMAPSIZE = 12
};

enum __nesc_unnamed4251 {


  TOSH_ACTUAL_VOLTAGE_PORT = 30, 
  TOSH_ACTUAL_BANDGAP_PORT = 30, 
  TOSH_ACTUAL_GND_PORT = 31
};

enum __nesc_unnamed4252 {


  TOS_ADC_VOLTAGE_PORT = 7, 
  TOS_ADC_BANDGAP_PORT = 10, 
  TOS_ADC_GND_PORT = 11
};




uint32_t TOS_UART0_BAUDRATE = 57600u;
# 33 "/Users/wbennett/opt/MoteWorks/tos/types/dbg_modes.h"
typedef long long TOS_dbg_mode;



enum __nesc_unnamed4253 {
  DBG_ALL = ~0ULL, 


  DBG_BOOT = 1ULL << 0, 
  DBG_CLOCK = 1ULL << 1, 
  DBG_TASK = 1ULL << 2, 
  DBG_SCHED = 1ULL << 3, 
  DBG_SENSOR = 1ULL << 4, 
  DBG_LED = 1ULL << 5, 
  DBG_CRYPTO = 1ULL << 6, 


  DBG_ROUTE = 1ULL << 7, 
  DBG_AM = 1ULL << 8, 
  DBG_CRC = 1ULL << 9, 
  DBG_PACKET = 1ULL << 10, 
  DBG_ENCODE = 1ULL << 11, 
  DBG_RADIO = 1ULL << 12, 


  DBG_LOG = 1ULL << 13, 
  DBG_ADC = 1ULL << 14, 
  DBG_I2C = 1ULL << 15, 
  DBG_UART = 1ULL << 16, 
  DBG_PROG = 1ULL << 17, 
  DBG_SOUNDER = 1ULL << 18, 
  DBG_TIME = 1ULL << 19, 
  DBG_POWER = 1ULL << 20, 



  DBG_SIM = 1ULL << 21, 
  DBG_QUEUE = 1ULL << 22, 
  DBG_SIMRADIO = 1ULL << 23, 
  DBG_HARD = 1ULL << 24, 
  DBG_MEM = 1ULL << 25, 



  DBG_USR1 = 1ULL << 27, 
  DBG_USR2 = 1ULL << 28, 
  DBG_USR3 = 1ULL << 29, 
  DBG_TEMP = 1ULL << 30, 

  DBG_ERROR = 1ULL << 31, 
  DBG_NONE = 0, 

  DBG_DEFAULT = DBG_ALL
};
# 41 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
#line 39
typedef struct __nesc_unnamed4254 {
  void (*tp)();
} TOSH_sched_entry_T;

enum __nesc_unnamed4255 {






  TOSH_MAX_TASKS = 32, 

  TOSH_TASK_BITMASK = TOSH_MAX_TASKS - 1
};

volatile TOSH_sched_entry_T TOSH_queue[TOSH_MAX_TASKS];
uint8_t TOSH_sched_full;
volatile uint8_t TOSH_sched_free;

static inline void TOSH_sched_init(void );








bool TOS_post(void (*tp)());
#line 82
bool TOS_post(void (*tp)())  ;
#line 116
static inline bool TOSH_run_next_task();
#line 139
static inline void TOSH_run_task();
# 14 "/Users/wbennett/opt/MoteWorks/tos/system/Ident.h"
enum __nesc_unnamed4256 {

  IDENT_MAX_PROGRAM_NAME_LENGTH = 17
};






#line 19
typedef struct __nesc_unnamed4257 {

  uint32_t unix_time;
  uint32_t user_hash;
  char program_name[IDENT_MAX_PROGRAM_NAME_LENGTH];
} Ident_t;
# 12 "/Users/wbennett/opt/MoteWorks/include/Tracker/compass_msg.h"
#line 4
typedef struct __nesc_unnamed4258 {
  uint8_t seqnum;
  uint16_t heading;
  int16_t pitch;
  int16_t roll;
  int16_t ax;
  int16_t ay;
  int16_t az;
} __attribute((packed))  compass_msg_t;

enum __nesc_unnamed4259 {
  AM_COMPASS_BASE_ADDR = 0, 
  AM_COMPASS_MSG = 0x17
};
# 43 "/opt/local/lib/gcc/avr/4.1.2/include/stdarg.h" 3
typedef __builtin_va_list __gnuc_va_list;
#line 105
typedef __gnuc_va_list va_list;
# 242 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdio.h" 3
struct __file {
  char *buf;
  unsigned char unget;
  uint8_t flags;
#line 261
  int size;
  int len;
  int (*put)(char arg_0x1012960c8, struct __file *arg_0x101296408);
  int (*get)(struct __file *arg_0x101296af8);
  void *udata;
};
#line 405
struct __file;
#line 417
struct __file;
#line 656
extern int sprintf(char *__s, const char *__fmt, ...);
# 42 "/Users/wbennett/opt/MoteWorks/lib/SOdebug0.h"
uint8_t debugStarted;
static const char hex[16] = "0123456789ABCDEF";



inline static void init_debug();
#line 82
static void UARTPutChar(char c);
#line 102
static inline int so_printf(const uint8_t *format, ...);
# 91 "/Users/wbennett/opt/MoteWorks/lib/debug_constants.h"
static char dbg_buffer[256];
# 51 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/AM.h"
enum __nesc_unnamed4260 {
  TOS_BCAST_ADDR = 0xffff, 
  TOS_UART_ADDR = 0x007e
};
#line 70
enum __nesc_unnamed4261 {
  TOS_DEFAULT_AM_GROUP = 0x7d
};


uint8_t TOS_AM_GROUP = TOS_DEFAULT_AM_GROUP;
#line 124
#line 100
typedef struct TOS_Msg {


  uint8_t length;
  uint8_t fcfhi;
  uint8_t fcflo;
  uint8_t dsn;
  uint16_t destpan;
  uint16_t addr;
  uint8_t type;
  uint8_t group;
  int8_t data[70];







  uint8_t strength;
  uint8_t lqi;
  bool crc;
  uint8_t ack;
  uint16_t time;
} TOS_Msg;




#line 126
typedef struct TinySec_Msg {

  uint8_t invalid;
} TinySec_Msg;
enum __nesc_unnamed4262 {

  MSG_HEADER_SIZE = (unsigned short )& ((struct TOS_Msg *)0)->data - 1, 

  MSG_FOOTER_SIZE = 2, 

  MSG_DATA_SIZE = (unsigned short )& ((struct TOS_Msg *)0)->strength + sizeof(uint16_t ), 

  DATA_LENGTH = 70, 

  LENGTH_BYTE_NUMBER = (unsigned short )& ((struct TOS_Msg *)0)->length + 1, 

  TOS_HEADER_SIZE = 5
};

typedef TOS_Msg *TOS_MsgPtr;
# 18 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/Timer.h"
enum __nesc_unnamed4263 {
  TIMER_REPEAT = 0, 
  TIMER_ONE_SHOT = 1, 
  NUM_TIMERS = 1U + 0U + 0U
};
# 40 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/byteorder.h"
static __inline int is_host_lsb();





static __inline uint16_t toLSB16(uint16_t a);




static __inline uint16_t fromLSB16(uint16_t a);
# 30 "/Users/wbennett/opt/MoteWorks/lib/avrtime.h"
struct avr_tm {
  int8_t sec;
  int8_t min;
  int8_t hour;
  int8_t day;
  int8_t mon;
  int16_t year;
  int8_t wday;
  int16_t day_of_year;
  uint8_t is_dst;
  uint8_t hundreth;
};

typedef uint64_t avrtime_t;


static inline void set_time_millis(avrtime_t set_time);

static inline void add_time_millis(uint32_t time_to_add);
static inline void init_avrtime();

static inline avrtime_t get_time_millis();





static inline void reset_start_time();



struct avr_tm;

struct avr_tm;
# 15 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static avrtime_t start_time;
static avrtime_t sys_time;
static bool isInit = 0;


static inline void set_time_millis(avrtime_t set_time);








static inline void init_avrtime();








static inline void add_time_millis(uint32_t time_to_add);







static inline avrtime_t get_time_millis();
#line 70
static inline void reset_start_time();
#line 133
struct avr_tm;
# 12 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/Clock.h"
enum __nesc_unnamed4264 {
  TOS_I1000PS = 32, TOS_S1000PS = 1, 
  TOS_I100PS = 40, TOS_S100PS = 2, 
  TOS_I10PS = 101, TOS_S10PS = 3, 
  TOS_I1024PS = 0, TOS_S1024PS = 3, 
  TOS_I512PS = 1, TOS_S512PS = 3, 
  TOS_I256PS = 3, TOS_S256PS = 3, 
  TOS_I128PS = 7, TOS_S128PS = 3, 
  TOS_I64PS = 15, TOS_S64PS = 3, 
  TOS_I32PS = 31, TOS_S32PS = 3, 
  TOS_I16PS = 63, TOS_S16PS = 3, 
  TOS_I8PS = 127, TOS_S8PS = 3, 
  TOS_I4PS = 255, TOS_S4PS = 3, 
  TOS_I2PS = 15, TOS_S2PS = 7, 
  TOS_I1PS = 31, TOS_S1PS = 7, 
  TOS_I0PS = 0, TOS_S0PS = 0
};
enum __nesc_unnamed4265 {
  DEFAULT_SCALE = 3, DEFAULT_INTERVAL = 127
};
# 11 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/crc.h"
uint16_t crcTable[256] __attribute((__progmem__))  = { 
0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 
0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef, 
0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6, 
0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de, 
0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485, 
0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d, 
0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4, 
0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc, 
0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823, 
0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b, 
0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12, 
0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a, 
0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41, 
0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49, 
0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70, 
0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78, 
0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f, 
0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067, 
0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e, 
0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256, 
0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d, 
0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 
0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c, 
0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634, 
0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab, 
0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3, 
0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a, 
0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92, 
0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9, 
0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1, 
0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8, 
0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0 };





static inline uint16_t crcByte(uint16_t oldCrc, uint8_t byte);
# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
static result_t PotM$Pot$init(uint8_t initialSetting);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
static result_t HPLPotC$Pot$finalise(void );
#line 38
static result_t HPLPotC$Pot$decrease(void );







static result_t HPLPotC$Pot$increase(void );
# 31 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/HPLInit.nc"
static result_t HPLInit$init(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr BaseM$ReceiveMsg$receive(TOS_MsgPtr m);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t BaseM$StdControl$init(void );






static result_t BaseM$StdControl$start(void );
# 101 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t LedsC$Leds$yellowOff(void );
#line 93
static result_t LedsC$Leds$yellowOn(void );
#line 35
static result_t LedsC$Leds$init(void );
#line 51
static result_t LedsC$Leds$redOff(void );
#line 110
static result_t LedsC$Leds$yellowToggle(void );
#line 60
static result_t LedsC$Leds$redToggle(void );
#line 43
static result_t LedsC$Leds$redOn(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr AMStandard$ReceiveMsg$default$receive(
# 35 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
uint8_t arg_0x101352108, 
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr m);
# 51 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/Timer.nc"
static result_t AMStandard$ActivityTimer$fired(void );
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
static result_t AMStandard$UARTSend$sendDone(TOS_MsgPtr msg, result_t success);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr AMStandard$RadioReceive$receive(TOS_MsgPtr m);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t AMStandard$Control$init(void );






static result_t AMStandard$Control$start(void );
# 44 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static result_t AMStandard$default$sendDone(void );
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
static result_t AMStandard$RadioSend$sendDone(TOS_MsgPtr msg, result_t success);
# 27 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendMsg.nc"
static result_t AMStandard$SendMsg$default$sendDone(
# 34 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
uint8_t arg_0x101353470, 
# 27 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendMsg.nc"
TOS_MsgPtr msg, result_t success);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr AMStandard$UARTReceive$receive(TOS_MsgPtr m);
# 40 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/TimerJiffyAsync.nc"
static result_t CC2420RadioM$BackoffTimerJiffy$fired(void );
# 73 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static TOS_MsgPtr CC2420RadioM$default$asyncReceive(TOS_MsgPtr pBuf);
# 44 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/MacControl.nc"
static void CC2420RadioM$MacControl$disableAck(void );







static void CC2420RadioM$MacControl$disableAddrDecode(void );
# 44 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
static result_t CC2420RadioM$HPLChipcon$FIFOPIntr(void );
# 15 "/Users/wbennett/opt/MoteWorks/tos/interfaces/RadioCoordinator.nc"
static void CC2420RadioM$RadioSendCoordinator$default$startSymbol(uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t CC2420RadioM$StdControl$init(void );






static result_t CC2420RadioM$StdControl$start(void );
# 44 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/MacBackoff.nc"
static int16_t CC2420RadioM$MacBackoff$default$congestionBackoff(TOS_MsgPtr m);
# 74 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static void CC2420RadioM$default$shortReceived(void );
# 44 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
static result_t CC2420ControlM$HPLChipcon$FIFOPIntr(void );
# 48 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420RAM.nc"
static result_t CC2420ControlM$HPLChipconRAM$writeDone(uint16_t addr, uint8_t length, uint8_t *buffer);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t CC2420ControlM$StdControl$init(void );






static result_t CC2420ControlM$StdControl$start(void );
# 148 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420Control.nc"
static result_t CC2420ControlM$CC2420Control$disableAutoAck(void );
#line 52
static result_t CC2420ControlM$CC2420Control$TunePreset(uint8_t channel);
#line 112
static result_t CC2420ControlM$CC2420Control$RxMode(void );
#line 168
static result_t CC2420ControlM$CC2420Control$disableAddrDecode(void );
#line 155
static result_t CC2420ControlM$CC2420Control$setShortAddress(uint16_t addr);
#line 83
static result_t CC2420ControlM$CC2420Control$OscillatorOn(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
static result_t HPLCC2420M$HPLCC2420$enableFIFOP(void );
#line 65
static uint16_t HPLCC2420M$HPLCC2420$read(uint8_t addr);
#line 58
static uint8_t HPLCC2420M$HPLCC2420$write(uint8_t addr, uint16_t data);
#line 51
static uint8_t HPLCC2420M$HPLCC2420$cmd(uint8_t addr);
#line 42
static result_t HPLCC2420M$HPLCC2420$disableFIFOP(void );
# 46 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420RAM.nc"
static result_t HPLCC2420M$HPLCC2420RAM$write(uint16_t addr, uint8_t length, uint8_t *buffer);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t HPLCC2420M$StdControl$init(void );






static result_t HPLCC2420M$StdControl$start(void );
# 56 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420FIFO.nc"
static result_t HPLCC2420FIFOM$HPLCC2420FIFO$writeTXFIFO(uint8_t length, uint8_t *data);
#line 46
static result_t HPLCC2420FIFOM$HPLCC2420FIFO$readRXFIFO(uint8_t length, uint8_t *data);
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Random.nc"
static uint16_t RandomLFSR$Random$rand(void );
#line 36
static result_t RandomLFSR$Random$init(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t TimerM$Clock$fire(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t TimerM$StdControl$init(void );






static result_t TimerM$StdControl$start(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/Timer.nc"
static result_t TimerM$Timer$default$fired(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x1016082f0);
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/Timer.nc"
static result_t TimerM$Timer$start(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x1016082f0, 
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/Timer.nc"
char type, uint32_t interval);
# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static void HPLClock$Clock$setInterval(uint8_t value);
#line 132
static uint8_t HPLClock$Clock$readCounter(void );
#line 75
static result_t HPLClock$Clock$setRate(char interval, char scale);
#line 100
static uint8_t HPLClock$Clock$getInterval(void );
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void );
# 34 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/TimerJiffyAsync.nc"
static result_t TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(uint32_t jiffy);



static bool TimerJiffyAsyncM$TimerJiffyAsync$isSet(void );
#line 36
static result_t TimerJiffyAsyncM$TimerJiffyAsync$stop(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t TimerJiffyAsyncM$Timer$fire(void );
#line 127
static result_t HPLTimer2$Timer2$setIntervalAndScale(uint8_t interval, uint8_t scale);
#line 147
static void HPLTimer2$Timer2$intDisable(void );
# 62 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t FramerM$ByteComm$txDone(void );
#line 54
static result_t FramerM$ByteComm$txByteReady(bool success);
#line 45
static result_t FramerM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t FramerM$StdControl$init(void );






static result_t FramerM$StdControl$start(void );
# 59 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
static result_t FramerM$TokenReceiveMsg$ReflectToken(uint8_t Token);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr FramerAckM$ReceiveMsg$receive(TOS_MsgPtr m);
# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
static TOS_MsgPtr FramerAckM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t Token);
# 66 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t UARTM$HPLUART$get(uint8_t data);







static result_t UARTM$HPLUART$putDone(void );
# 34 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t UARTM$ByteComm$txByte(uint8_t data);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t UARTM$Control$init(void );






static result_t UARTM$Control$start(void );
# 40 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t HPLUART0M$UART$init(void );
#line 58
static result_t HPLUART0M$UART$put(uint8_t data);
# 36 "/Users/wbennett/opt/MoteWorks/tos/platform/mica2/HPLUART0M.nc"
static result_t HPLUART0M$Setbaud(uint32_t baud_rate);
# 27 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
static result_t RealMain$hardwareInit(void );
# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
static result_t RealMain$Pot$init(uint8_t initialSetting);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t RealMain$StdControl$init(void );






static result_t RealMain$StdControl$start(void );
# 34 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
int main(void )   ;
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
static result_t PotM$HPLPot$finalise(void );
#line 38
static result_t PotM$HPLPot$decrease(void );







static result_t PotM$HPLPot$increase(void );
# 70 "/Users/wbennett/opt/MoteWorks/tos/system/PotM.nc"
uint8_t PotM$potSetting;

static inline void PotM$setPot(uint8_t value);
#line 85
static inline result_t PotM$Pot$init(uint8_t initialSetting);
# 36 "/Users/wbennett/opt/MoteWorks/tos/platform/mica2/HPLPotC.nc"
static inline result_t HPLPotC$Pot$decrease(void );








static inline result_t HPLPotC$Pot$increase(void );








static inline result_t HPLPotC$Pot$finalise(void );
# 36 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/HPLInit.nc"
static inline result_t HPLInit$init(void );
# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t BaseM$Leds$init(void );
#line 110
static result_t BaseM$Leds$yellowToggle(void );
#line 60
static result_t BaseM$Leds$redToggle(void );
# 20 "BaseM.nc"
static inline result_t BaseM$StdControl$init(void );






static inline result_t BaseM$StdControl$start(void );






static inline TOS_MsgPtr BaseM$ReceiveMsg$receive(TOS_MsgPtr m);
# 28 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
uint8_t LedsC$ledsOn;

enum LedsC$__nesc_unnamed4266 {
  LedsC$RED_BIT = 1, 
  LedsC$GREEN_BIT = 2, 
  LedsC$YELLOW_BIT = 4
};

static inline result_t LedsC$Leds$init(void );
#line 50
static inline result_t LedsC$Leds$redOn(void );








static inline result_t LedsC$Leds$redOff(void );








static inline result_t LedsC$Leds$redToggle(void );
#line 108
static inline result_t LedsC$Leds$yellowOn(void );








static inline result_t LedsC$Leds$yellowOff(void );








static inline result_t LedsC$Leds$yellowToggle(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr AMStandard$ReceiveMsg$receive(
# 35 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
uint8_t arg_0x101352108, 
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr m);
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/Timer.nc"
static result_t AMStandard$ActivityTimer$start(char type, uint32_t interval);
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t AMStandard$PowerManagement$adjustPower(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t AMStandard$RadioControl$init(void );






static result_t AMStandard$RadioControl$start(void );
#line 41
static result_t AMStandard$TimerControl$init(void );






static result_t AMStandard$TimerControl$start(void );
#line 41
static result_t AMStandard$UARTControl$init(void );






static result_t AMStandard$UARTControl$start(void );
# 44 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static result_t AMStandard$sendDone(void );
# 27 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendMsg.nc"
static result_t AMStandard$SendMsg$sendDone(
# 34 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
uint8_t arg_0x101353470, 
# 27 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendMsg.nc"
TOS_MsgPtr msg, result_t success);
# 60 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
bool AMStandard$state;

uint16_t AMStandard$lastCount;
uint16_t AMStandard$counter;


static inline result_t AMStandard$Control$init(void );
#line 82
static inline result_t AMStandard$Control$start(void );
#line 111
static inline void AMStandard$dbgPacket(TOS_MsgPtr data);










static result_t AMStandard$reportSendDone(TOS_MsgPtr msg, result_t success);







static inline result_t AMStandard$ActivityTimer$fired(void );





static inline result_t AMStandard$SendMsg$default$sendDone(uint8_t id, TOS_MsgPtr msg, result_t success);


static inline result_t AMStandard$default$sendDone(void );
#line 186
static inline result_t AMStandard$UARTSend$sendDone(TOS_MsgPtr msg, result_t success);


static inline result_t AMStandard$RadioSend$sendDone(TOS_MsgPtr msg, result_t success);




TOS_MsgPtr received(TOS_MsgPtr packet)   ;
#line 221
static inline TOS_MsgPtr AMStandard$ReceiveMsg$default$receive(uint8_t id, TOS_MsgPtr msg);



static inline TOS_MsgPtr AMStandard$UARTReceive$receive(TOS_MsgPtr packet);





static inline TOS_MsgPtr AMStandard$RadioReceive$receive(TOS_MsgPtr packet);
# 34 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/TimerJiffyAsync.nc"
static result_t CC2420RadioM$BackoffTimerJiffy$setOneShot(uint32_t jiffy);



static bool CC2420RadioM$BackoffTimerJiffy$isSet(void );
#line 36
static result_t CC2420RadioM$BackoffTimerJiffy$stop(void );
# 73 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static TOS_MsgPtr CC2420RadioM$asyncReceive(TOS_MsgPtr pBuf);
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Random.nc"
static uint16_t CC2420RadioM$Random$rand(void );
#line 36
static result_t CC2420RadioM$Random$init(void );
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
static result_t CC2420RadioM$Send$sendDone(TOS_MsgPtr msg, result_t success);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t CC2420RadioM$TimerControl$init(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr CC2420RadioM$Receive$receive(TOS_MsgPtr m);
# 41 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
static result_t CC2420RadioM$HPLChipcon$enableFIFOP(void );
#line 65
static uint16_t CC2420RadioM$HPLChipcon$read(uint8_t addr);
#line 58
static uint8_t CC2420RadioM$HPLChipcon$write(uint8_t addr, uint16_t data);
#line 51
static uint8_t CC2420RadioM$HPLChipcon$cmd(uint8_t addr);
#line 42
static result_t CC2420RadioM$HPLChipcon$disableFIFOP(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t CC2420RadioM$CC2420StdControl$init(void );






static result_t CC2420RadioM$CC2420StdControl$start(void );
# 15 "/Users/wbennett/opt/MoteWorks/tos/interfaces/RadioCoordinator.nc"
static void CC2420RadioM$RadioSendCoordinator$startSymbol(uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff);
# 56 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420FIFO.nc"
static result_t CC2420RadioM$HPLChipconFIFO$writeTXFIFO(uint8_t length, uint8_t *data);
#line 46
static result_t CC2420RadioM$HPLChipconFIFO$readRXFIFO(uint8_t length, uint8_t *data);
# 148 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420Control.nc"
static result_t CC2420RadioM$CC2420Control$disableAutoAck(void );
#line 112
static result_t CC2420RadioM$CC2420Control$RxMode(void );
#line 168
static result_t CC2420RadioM$CC2420Control$disableAddrDecode(void );
# 44 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/MacBackoff.nc"
static int16_t CC2420RadioM$MacBackoff$congestionBackoff(TOS_MsgPtr m);
# 74 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static void CC2420RadioM$shortReceived(void );
#line 120
enum CC2420RadioM$__nesc_unnamed4267 {
  CC2420RadioM$DISABLED_STATE = 0, 
  CC2420RadioM$IDLE_STATE, 
  CC2420RadioM$PRE_TX_STATE, 
  CC2420RadioM$TX_STATE, 
  CC2420RadioM$POST_TX_STATE, 
  CC2420RadioM$RAW_TX_STATE, 



  CC2420RadioM$TIMER_IDLE = 0, 
  CC2420RadioM$TIMER_INITIAL, 
  CC2420RadioM$TIMER_BACKOFF, 
  CC2420RadioM$TIMER_ACK, 
  CC2420RadioM$TIMER_SNIFF
};



uint8_t CC2420RadioM$stateTimer;
bool CC2420RadioM$bAckEnable;
bool CC2420RadioM$bAckManual;



uint8_t CC2420RadioM$cnttryToSend;



uint8_t CC2420RadioM$RadioState;
uint8_t CC2420RadioM$bRxBufLocked;
uint8_t CC2420RadioM$currentDSN;
uint16_t CC2420RadioM$txlength;
uint16_t CC2420RadioM$rxlength;
TOS_MsgPtr CC2420RadioM$txbufptr;
TOS_MsgPtr CC2420RadioM$rxbufptr;
TOS_Msg CC2420RadioM$RxBuf;



result_t CC2420RadioM$gImmedSendDone;

volatile result_t CC2420RadioM$gSniffDone;








volatile uint16_t CC2420RadioM$LocalAddr;



static __inline void CC2420RadioM$immedPacketSent(void );
static __inline void CC2420RadioM$immedPacketRcvd(void );
static void CC2420RadioM$fSendAborted(void );
static result_t CC2420RadioM$sendPacket(void );
static inline void CC2420RadioM$tryToSend(void );
static inline uint8_t CC2420RadioM$fTXPacket(uint8_t len, uint8_t *pMsg);



static inline void CC2420RadioM$PacketRcvd(void );
static inline void CC2420RadioM$PacketSent(void );
#line 216
static __inline result_t CC2420RadioM$setBackoffTimer(uint16_t jiffy);
#line 231
static __inline result_t CC2420RadioM$setAckTimer(uint16_t jiffy);
#line 247
static inline result_t CC2420RadioM$fTXPacket(uint8_t len, uint8_t *pMsg);
#line 286
static result_t CC2420RadioM$sendPacket(void );
#line 377
static inline void CC2420RadioM$tryToSend(void );
#line 415
static void CC2420RadioM$fSendAborted(void );
#line 458
static __inline void CC2420RadioM$immedPacketSent(void );
#line 502
static __inline void CC2420RadioM$immedPacketRcvd(void );
#line 546
static inline void CC2420RadioM$PacketRcvd(void );




static inline void CC2420RadioM$PacketSent(void );





static inline result_t CC2420RadioM$StdControl$init(void );
#line 609
static inline result_t CC2420RadioM$StdControl$start(void );
#line 658
static inline result_t CC2420RadioM$BackoffTimerJiffy$fired(void );
#line 906
static inline result_t CC2420RadioM$HPLChipcon$FIFOPIntr(void );
#line 1082
static inline void CC2420RadioM$MacControl$disableAddrDecode(void );
#line 1110
static inline void CC2420RadioM$MacControl$disableAck(void );
#line 1298
static inline TOS_MsgPtr CC2420RadioM$default$asyncReceive(TOS_MsgPtr pBuf);






static inline void CC2420RadioM$default$shortReceived(void );
#line 1321
static inline int16_t CC2420RadioM$MacBackoff$default$congestionBackoff(TOS_MsgPtr m);




static inline 
#line 1325
void CC2420RadioM$RadioSendCoordinator$default$startSymbol(
uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff);
# 41 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
static result_t CC2420ControlM$HPLChipcon$enableFIFOP(void );
#line 65
static uint16_t CC2420ControlM$HPLChipcon$read(uint8_t addr);
#line 58
static uint8_t CC2420ControlM$HPLChipcon$write(uint8_t addr, uint16_t data);
#line 51
static uint8_t CC2420ControlM$HPLChipcon$cmd(uint8_t addr);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t CC2420ControlM$HPLChipconControl$init(void );






static result_t CC2420ControlM$HPLChipconControl$start(void );
# 46 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420RAM.nc"
static result_t CC2420ControlM$HPLChipconRAM$write(uint16_t addr, uint8_t length, uint8_t *buffer);
# 87 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420ControlM.nc"
uint16_t CC2420ControlM$gCurrentParameters[14];






static inline bool CC2420ControlM$SetRegs(void );
#line 126
static inline result_t CC2420ControlM$StdControl$init(void );
#line 213
static inline result_t CC2420ControlM$StdControl$start(void );
#line 249
static inline result_t CC2420ControlM$CC2420Control$TunePreset(uint8_t chnl);
#line 301
static inline result_t CC2420ControlM$CC2420Control$RxMode(void );
#line 343
static inline result_t CC2420ControlM$CC2420Control$OscillatorOn(void );
#line 381
static result_t CC2420ControlM$CC2420Control$disableAutoAck(void );




static inline result_t CC2420ControlM$CC2420Control$setShortAddress(uint16_t addr);




static inline result_t CC2420ControlM$HPLChipcon$FIFOPIntr(void );







static inline result_t CC2420ControlM$HPLChipconRAM$writeDone(uint16_t addr, uint8_t length, uint8_t *buffer);
#line 413
static inline result_t CC2420ControlM$CC2420Control$disableAddrDecode(void );
# 44 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
static result_t HPLCC2420M$HPLCC2420$FIFOPIntr(void );
# 48 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420RAM.nc"
static result_t HPLCC2420M$HPLCC2420RAM$writeDone(uint16_t addr, uint8_t length, uint8_t *buffer);
# 48 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLCC2420M.nc"
bool HPLCC2420M$bSpiAvail;
uint8_t *HPLCC2420M$rambuf;
uint8_t HPLCC2420M$ramlen;
uint16_t HPLCC2420M$ramaddr;






static inline result_t HPLCC2420M$StdControl$init(void );
#line 87
static inline result_t HPLCC2420M$StdControl$start(void );
#line 103
static result_t HPLCC2420M$HPLCC2420$enableFIFOP(void );










static inline result_t HPLCC2420M$HPLCC2420$disableFIFOP(void );







static uint8_t HPLCC2420M$HPLCC2420$cmd(uint8_t addr);
#line 147
static result_t HPLCC2420M$HPLCC2420$write(uint8_t addr, uint16_t data);
#line 178
static uint16_t HPLCC2420M$HPLCC2420$read(uint8_t addr);
#line 202
void __vector_7(void )   __attribute((signal)) ;
#line 220
static inline void HPLCC2420M$signalRAMWr(void );










static inline result_t HPLCC2420M$HPLCC2420RAM$write(uint16_t addr, uint8_t length, uint8_t *buffer);
# 47 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLCC2420FIFOM.nc"
bool HPLCC2420FIFOM$bSpiAvail;
uint8_t *HPLCC2420FIFOM$txbuf;
#line 48
uint8_t *HPLCC2420FIFOM$rxbuf;
uint8_t HPLCC2420FIFOM$txlength;
#line 49
uint8_t HPLCC2420FIFOM$rxlength;
#line 71
static result_t HPLCC2420FIFOM$HPLCC2420FIFO$writeTXFIFO(uint8_t len, uint8_t *msg);
#line 125
static result_t HPLCC2420FIFOM$HPLCC2420FIFO$readRXFIFO(uint8_t len, uint8_t *msg);
# 33 "/Users/wbennett/opt/MoteWorks/tos/system/RandomLFSR.nc"
uint16_t RandomLFSR$shiftReg;
uint16_t RandomLFSR$initSeed;
uint16_t RandomLFSR$mask;


static inline result_t RandomLFSR$Random$init(void );










static uint16_t RandomLFSR$Random$rand(void );
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t TimerM$PowerManagement$adjustPower(void );
# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static void TimerM$Clock$setInterval(uint8_t value);
#line 132
static uint8_t TimerM$Clock$readCounter(void );
#line 75
static result_t TimerM$Clock$setRate(char interval, char scale);
#line 100
static uint8_t TimerM$Clock$getInterval(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/Timer.nc"
static result_t TimerM$Timer$fired(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x1016082f0);









extern int sprintf(char *arg_0x101614020, const char *arg_0x1016142f8, ...)  ;
uint32_t TimerM$mState;
uint8_t TimerM$setIntervalFlag;
uint8_t TimerM$mScale;
#line 41
uint8_t TimerM$mInterval;
int8_t TimerM$queue_head;
int8_t TimerM$queue_tail;
uint8_t TimerM$queue_size;
uint8_t TimerM$queue[NUM_TIMERS];
volatile uint16_t TimerM$interval_outstanding;





#line 48
struct TimerM$timer_s {
  uint8_t type;
  int32_t ticks;
  int32_t ticksLeft;
} TimerM$mTimerList[NUM_TIMERS];

enum TimerM$__nesc_unnamed4268 {
  TimerM$maxTimerInterval = 230
};
static result_t TimerM$StdControl$init(void );
#line 69
static inline result_t TimerM$StdControl$start(void );










static inline result_t TimerM$Timer$start(uint8_t id, char type, 
uint32_t interval);
#line 111
inline static void TimerM$adjustInterval(void );
#line 164
static inline result_t TimerM$Timer$default$fired(uint8_t id);



static inline void TimerM$enqueue(uint8_t value);







static inline uint8_t TimerM$dequeue(void );









static inline void TimerM$signalOneTimer(void );





static inline void TimerM$HandleFire(void );
#line 237
static inline result_t TimerM$Clock$fire(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t HPLClock$Clock$fire(void );
# 33 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/HPLClock.nc"
uint8_t HPLClock$set_flag;
uint8_t HPLClock$mscale;
#line 34
uint8_t HPLClock$nextScale;
#line 34
uint8_t HPLClock$minterval;
#line 66
static inline void HPLClock$Clock$setInterval(uint8_t value);









static inline uint8_t HPLClock$Clock$getInterval(void );
#line 113
static inline uint8_t HPLClock$Clock$readCounter(void );
#line 128
static inline result_t HPLClock$Clock$setRate(char interval, char scale);
#line 146
void __vector_15(void )   __attribute((interrupt)) ;
# 43 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLPowerManagementM.nc"
bool HPLPowerManagementM$disabled = 1;

enum HPLPowerManagementM$__nesc_unnamed4269 {
  HPLPowerManagementM$IDLE = 0, 
  HPLPowerManagementM$ADC_NR = 1 << 3, 
  HPLPowerManagementM$POWER_DOWN = 1 << 4, 
  HPLPowerManagementM$POWER_SAVE = (1 << 3) + (1 << 4), 
  HPLPowerManagementM$STANDBY = (1 << 2) + (1 << 4), 
  HPLPowerManagementM$EXT_STANDBY = (1 << 3) + (1 << 4) + (1 << 2)
};




static inline uint8_t HPLPowerManagementM$getPowerLevel(void );
#line 101
static inline void HPLPowerManagementM$doAdjustment(void );
#line 121
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void );
# 40 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/TimerJiffyAsync.nc"
static result_t TimerJiffyAsyncM$TimerJiffyAsync$fired(void );
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t TimerJiffyAsyncM$PowerManagement$adjustPower(void );
# 127 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t TimerJiffyAsyncM$Timer$setIntervalAndScale(uint8_t interval, uint8_t scale);
#line 147
static void TimerJiffyAsyncM$Timer$intDisable(void );
# 45 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/TimerJiffyAsyncM.nc"
uint32_t TimerJiffyAsyncM$jiffy;
bool TimerJiffyAsyncM$bSet;
#line 73
static inline result_t TimerJiffyAsyncM$Timer$fire(void );
#line 93
static result_t TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(uint32_t _jiffy);
#line 110
static inline bool TimerJiffyAsyncM$TimerJiffyAsync$isSet(void );




static inline result_t TimerJiffyAsyncM$TimerJiffyAsync$stop(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t HPLTimer2$Timer2$fire(void );
# 52 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLTimer2.nc"
uint8_t HPLTimer2$set_flag;
uint8_t HPLTimer2$mscale;
#line 53
uint8_t HPLTimer2$nextScale;
#line 53
uint8_t HPLTimer2$minterval;
#line 114
static result_t HPLTimer2$Timer2$setIntervalAndScale(uint8_t interval, uint8_t scale);
#line 162
static inline void HPLTimer2$Timer2$intDisable(void );






void __vector_9(void )   __attribute((interrupt)) ;
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr FramerM$ReceiveMsg$receive(TOS_MsgPtr m);
# 34 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t FramerM$ByteComm$txByte(uint8_t data);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t FramerM$ByteControl$init(void );






static result_t FramerM$ByteControl$start(void );
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
static result_t FramerM$BareSendMsg$sendDone(TOS_MsgPtr msg, result_t success);
# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
static TOS_MsgPtr FramerM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t Token);
# 54 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
enum FramerM$__nesc_unnamed4270 {
  FramerM$HDLC_QUEUESIZE = 2, 

  FramerM$HDLC_MTU = sizeof(TOS_Msg ) - 5, 



  FramerM$HDLC_FLAG_BYTE = 0x7e, 
  FramerM$HDLC_CTLESC_BYTE = 0x7d, 
  FramerM$PROTO_ACK = 64, 
  FramerM$PROTO_PACKET_ACK = 65, 
  FramerM$PROTO_PACKET_NOACK = 66, 
  FramerM$PROTO_UNKNOWN = 255
};

enum FramerM$__nesc_unnamed4271 {
  FramerM$RXSTATE_NOSYNC, 
  FramerM$RXSTATE_PROTO, 
  FramerM$RXSTATE_TOKEN, 
  FramerM$RXSTATE_INFO, 
  FramerM$RXSTATE_ESC
};

enum FramerM$__nesc_unnamed4272 {
  FramerM$TXSTATE_IDLE, 
  FramerM$TXSTATE_PROTO, 
  FramerM$TXSTATE_INFO, 
  FramerM$TXSTATE_ESC, 
  FramerM$TXSTATE_FCS1, 
  FramerM$TXSTATE_FCS2, 
  FramerM$TXSTATE_ENDFLAG, 
  FramerM$TXSTATE_FINISH, 
  FramerM$TXSTATE_ERROR
};

enum FramerM$__nesc_unnamed4273 {
  FramerM$FLAGS_TOKENPEND = 0x2, 
  FramerM$FLAGS_DATAPEND = 0x4, 
  FramerM$FLAGS_UNKNOWN = 0x8
};

TOS_Msg FramerM$gMsgRcvBuf[FramerM$HDLC_QUEUESIZE];






#line 97
typedef struct FramerM$_MsgRcvEntry {
  uint8_t Proto;
  uint8_t Token;
  uint16_t Length;
  TOS_MsgPtr pMsg;
} FramerM$MsgRcvEntry_t;

FramerM$MsgRcvEntry_t FramerM$gMsgRcvTbl[FramerM$HDLC_QUEUESIZE];

uint8_t *FramerM$gpRxBuf;
uint8_t *FramerM$gpTxBuf;

uint8_t FramerM$gFlags;


uint8_t FramerM$gTxState;
uint8_t FramerM$gPrevTxState;
uint8_t FramerM$gTxProto;
uint16_t FramerM$gTxByteCnt;
uint16_t FramerM$gTxLength;
uint16_t FramerM$gTxRunningCRC;


uint8_t FramerM$gRxState;
uint8_t FramerM$gRxHeadIndex;
uint8_t FramerM$gRxTailIndex;
uint16_t FramerM$gRxByteCnt;

uint16_t FramerM$gRxRunningCRC;

TOS_MsgPtr FramerM$gpTxMsg;
uint8_t FramerM$gTxTokenBuf;
uint8_t FramerM$gTxUnknownBuf;
uint8_t FramerM$gTxEscByte;

static void FramerM$PacketSent(void );

static uint8_t FramerM$fRemapRxPos(uint8_t InPos);






static uint8_t FramerM$fRemapRxPos(uint8_t InPos);
#line 156
static result_t FramerM$StartTx(void );
#line 216
static inline void FramerM$PacketUnknown(void );







static inline void FramerM$PacketRcvd(void );
#line 263
static void FramerM$PacketSent(void );
#line 285
static void FramerM$HDLCInitialize(void );
#line 308
static inline result_t FramerM$StdControl$init(void );




static inline result_t FramerM$StdControl$start(void );
#line 345
static inline result_t FramerM$TokenReceiveMsg$ReflectToken(uint8_t Token);
#line 365
static inline result_t FramerM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength);
#line 497
static result_t FramerM$TxArbitraryByte(uint8_t inByte);
#line 510
static inline result_t FramerM$ByteComm$txByteReady(bool LastByteSuccess);
#line 588
static inline result_t FramerM$ByteComm$txDone(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr FramerAckM$ReceiveCombined$receive(TOS_MsgPtr m);
# 59 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
static result_t FramerAckM$TokenReceiveMsg$ReflectToken(uint8_t Token);
# 37 "/Users/wbennett/opt/MoteWorks/tos/system/FramerAckM.nc"
uint8_t FramerAckM$gTokenBuf;

static inline void FramerAckM$SendAckTask(void );




static inline TOS_MsgPtr FramerAckM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t token);
#line 56
static inline TOS_MsgPtr FramerAckM$ReceiveMsg$receive(TOS_MsgPtr Msg);
# 40 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t UARTM$HPLUART$init(void );
#line 58
static result_t UARTM$HPLUART$put(uint8_t data);
# 62 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t UARTM$ByteComm$txDone(void );
#line 54
static result_t UARTM$ByteComm$txByteReady(bool success);
#line 45
static result_t UARTM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength);
# 38 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
bool UARTM$state;

static inline result_t UARTM$Control$init(void );







static inline result_t UARTM$Control$start(void );








static inline result_t UARTM$HPLUART$get(uint8_t data);









static inline result_t UARTM$HPLUART$putDone(void );
#line 90
static result_t UARTM$ByteComm$txByte(uint8_t data);
# 66 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t HPLUART0M$UART$get(uint8_t data);







static result_t HPLUART0M$UART$putDone(void );
# 48 "/Users/wbennett/opt/MoteWorks/tos/platform/mica2/HPLUART0M.nc"
static inline result_t HPLUART0M$Setbaud(uint32_t baud_rate);
#line 87
static inline result_t HPLUART0M$UART$init(void );
#line 102
void __vector_18(void )   __attribute((signal)) ;









void __vector_20(void )   __attribute((interrupt)) ;




static inline result_t HPLUART0M$UART$put(uint8_t data);
# 118 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline void TOSH_SET_GREEN_LED_PIN()
#line 118
{
#line 118
  * (volatile uint8_t *)(0x1B + 0x20) |= 1 << 1;
}

#line 119
static __inline void TOSH_SET_YELLOW_LED_PIN()
#line 119
{
#line 119
  * (volatile uint8_t *)(0x1B + 0x20) |= 1 << 0;
}

#line 117
static __inline void TOSH_SET_RED_LED_PIN()
#line 117
{
#line 117
  * (volatile uint8_t *)(0x1B + 0x20) |= 1 << 2;
}

#line 156
static __inline void TOSH_SET_FLASH_SELECT_PIN()
#line 156
{
#line 156
  * (volatile uint8_t *)(0x1B + 0x20) |= 1 << 3;
}

#line 157
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT()
#line 157
{
#line 157
  * (volatile uint8_t *)(0x11 + 0x20) |= 1 << 5;
}

#line 158
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT()
#line 158
{
#line 158
  * (volatile uint8_t *)(0x11 + 0x20) |= 1 << 3;
}

#line 156
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT()
#line 156
{
#line 156
  * (volatile uint8_t *)(0x1A + 0x20) |= 1 << 3;
}

#line 121
static __inline void TOSH_CLR_SERIAL_ID_PIN()
#line 121
{
#line 121
  * (volatile uint8_t *)(0x1B + 0x20) &= ~(1 << 4);
}

#line 121
static __inline void TOSH_MAKE_SERIAL_ID_INPUT()
#line 121
{
#line 121
  * (volatile uint8_t *)(0x1A + 0x20) &= ~(1 << 4);
}

#line 153
static __inline void TOSH_MAKE_RADIO_CCA_INPUT()
#line 153
{
#line 153
  * (volatile uint8_t *)(0x11 + 0x20) &= ~(1 << 6);
}

#line 151
static __inline void TOSH_MAKE_CC_FIFO_INPUT()
#line 151
{
#line 151
  * (volatile uint8_t *)(0x17 + 0x20) &= ~(1 << 7);
}

#line 149
static __inline void TOSH_MAKE_CC_SFD_INPUT()
#line 149
{
#line 149
  * (volatile uint8_t *)(0x11 + 0x20) &= ~(1 << 4);
}

#line 148
static __inline void TOSH_MAKE_CC_CCA_INPUT()
#line 148
{
#line 148
  * (volatile uint8_t *)(0x11 + 0x20) &= ~(1 << 6);
}

#line 146
static __inline void TOSH_MAKE_CC_FIFOP1_INPUT()
#line 146
{
#line 146
  * (volatile uint8_t *)(0x02 + 0x20) &= ~(1 << 6);
}


static __inline void TOSH_MAKE_CC_CS_INPUT()
#line 150
{
#line 150
  * (volatile uint8_t *)(0x17 + 0x20) &= ~(1 << 0);
}

#line 143
static __inline void TOSH_MAKE_CC_VREN_OUTPUT()
#line 143
{
#line 143
  * (volatile uint8_t *)(0x1A + 0x20) |= 1 << 5;
}

#line 142
static __inline void TOSH_MAKE_CC_RSTN_OUTPUT()
#line 142
{
#line 142
  * (volatile uint8_t *)(0x1A + 0x20) |= 1 << 6;
}

#line 171
static __inline void TOSH_MAKE_SPI_SCK_OUTPUT()
#line 171
{
#line 171
  * (volatile uint8_t *)(0x17 + 0x20) |= 1 << 1;
}

#line 168
static __inline void TOSH_MAKE_MOSI_OUTPUT()
#line 168
{
#line 168
  * (volatile uint8_t *)(0x17 + 0x20) |= 1 << 2;
}

#line 169
static __inline void TOSH_MAKE_MISO_INPUT()
#line 169
{
#line 169
  * (volatile uint8_t *)(0x17 + 0x20) &= ~(1 << 3);
}



static __inline void TOSH_MAKE_PW0_OUTPUT()
#line 174
{
#line 174
  * (volatile uint8_t *)(0x14 + 0x20) |= 1 << 0;
}

#line 175
static __inline void TOSH_MAKE_PW1_OUTPUT()
#line 175
{
#line 175
  * (volatile uint8_t *)(0x14 + 0x20) |= 1 << 1;
}

#line 176
static __inline void TOSH_MAKE_PW2_OUTPUT()
#line 176
{
#line 176
  * (volatile uint8_t *)(0x14 + 0x20) |= 1 << 2;
}

#line 177
static __inline void TOSH_MAKE_PW3_OUTPUT()
#line 177
{
#line 177
  * (volatile uint8_t *)(0x14 + 0x20) |= 1 << 3;
}

#line 178
static __inline void TOSH_MAKE_PW4_OUTPUT()
#line 178
{
#line 178
  * (volatile uint8_t *)(0x14 + 0x20) |= 1 << 4;
}

#line 179
static __inline void TOSH_MAKE_PW5_OUTPUT()
#line 179
{
#line 179
  * (volatile uint8_t *)(0x14 + 0x20) |= 1 << 5;
}

#line 180
static __inline void TOSH_MAKE_PW6_OUTPUT()
#line 180
{
#line 180
  * (volatile uint8_t *)(0x14 + 0x20) |= 1 << 6;
}

#line 181
static __inline void TOSH_MAKE_PW7_OUTPUT()
#line 181
{
#line 181
  * (volatile uint8_t *)(0x14 + 0x20) |= 1 << 7;
}

#line 118
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT()
#line 118
{
#line 118
  * (volatile uint8_t *)(0x1A + 0x20) |= 1 << 1;
}

#line 119
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT()
#line 119
{
#line 119
  * (volatile uint8_t *)(0x1A + 0x20) |= 1 << 0;
}

#line 117
static __inline void TOSH_MAKE_RED_LED_OUTPUT()
#line 117
{
#line 117
  * (volatile uint8_t *)(0x1A + 0x20) |= 1 << 2;
}

#line 212
static inline void TOSH_SET_PIN_DIRECTIONS(void )
{







  TOSH_MAKE_RED_LED_OUTPUT();
  TOSH_MAKE_YELLOW_LED_OUTPUT();
  TOSH_MAKE_GREEN_LED_OUTPUT();


  TOSH_MAKE_PW7_OUTPUT();
  TOSH_MAKE_PW6_OUTPUT();
  TOSH_MAKE_PW5_OUTPUT();
  TOSH_MAKE_PW4_OUTPUT();
  TOSH_MAKE_PW3_OUTPUT();
  TOSH_MAKE_PW2_OUTPUT();
  TOSH_MAKE_PW1_OUTPUT();
  TOSH_MAKE_PW0_OUTPUT();


  TOSH_MAKE_MISO_INPUT();
  TOSH_MAKE_MOSI_OUTPUT();
  TOSH_MAKE_SPI_SCK_OUTPUT();
  TOSH_MAKE_CC_RSTN_OUTPUT();
  TOSH_MAKE_CC_VREN_OUTPUT();
  TOSH_MAKE_CC_CS_INPUT();
  TOSH_MAKE_CC_FIFOP1_INPUT();
  TOSH_MAKE_CC_CCA_INPUT();
  TOSH_MAKE_CC_SFD_INPUT();
  TOSH_MAKE_CC_FIFO_INPUT();

  TOSH_MAKE_RADIO_CCA_INPUT();



  TOSH_MAKE_SERIAL_ID_INPUT();
  TOSH_CLR_SERIAL_ID_PIN();

  TOSH_MAKE_FLASH_SELECT_OUTPUT();
  TOSH_MAKE_FLASH_OUT_OUTPUT();
  TOSH_MAKE_FLASH_CLK_OUTPUT();
  TOSH_SET_FLASH_SELECT_PIN();

  TOSH_SET_RED_LED_PIN();
  TOSH_SET_YELLOW_LED_PIN();
  TOSH_SET_GREEN_LED_PIN();
}

# 36 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/HPLInit.nc"
static inline result_t HPLInit$init(void )
#line 36
{
  TOSH_SET_PIN_DIRECTIONS();
  return SUCCESS;
}

# 27 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
inline static result_t RealMain$hardwareInit(void ){
#line 27
  unsigned char __nesc_result;
#line 27

#line 27
  __nesc_result = HPLInit$init();
#line 27

#line 27
  return __nesc_result;
#line 27
}
#line 27
# 54 "/Users/wbennett/opt/MoteWorks/tos/platform/mica2/HPLPotC.nc"
static inline result_t HPLPotC$Pot$finalise(void )
#line 54
{


  return SUCCESS;
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$finalise(void ){
#line 53
  unsigned char __nesc_result;
#line 53

#line 53
  __nesc_result = HPLPotC$Pot$finalise();
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 45 "/Users/wbennett/opt/MoteWorks/tos/platform/mica2/HPLPotC.nc"
static inline result_t HPLPotC$Pot$increase(void )
#line 45
{





  return SUCCESS;
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$increase(void ){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = HPLPotC$Pot$increase();
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 36 "/Users/wbennett/opt/MoteWorks/tos/platform/mica2/HPLPotC.nc"
static inline result_t HPLPotC$Pot$decrease(void )
#line 36
{





  return SUCCESS;
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$decrease(void ){
#line 38
  unsigned char __nesc_result;
#line 38

#line 38
  __nesc_result = HPLPotC$Pot$decrease();
#line 38

#line 38
  return __nesc_result;
#line 38
}
#line 38
# 72 "/Users/wbennett/opt/MoteWorks/tos/system/PotM.nc"
static inline void PotM$setPot(uint8_t value)
#line 72
{
  uint8_t i;

#line 74
  for (i = 0; i < 151; i++) 
    PotM$HPLPot$decrease();

  for (i = 0; i < value; i++) 
    PotM$HPLPot$increase();

  PotM$HPLPot$finalise();

  PotM$potSetting = value;
}

static inline result_t PotM$Pot$init(uint8_t initialSetting)
#line 85
{
  PotM$setPot(initialSetting);
  return SUCCESS;
}

# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
inline static result_t RealMain$Pot$init(uint8_t initialSetting){
#line 57
  unsigned char __nesc_result;
#line 57

#line 57
  __nesc_result = PotM$Pot$init(initialSetting);
#line 57

#line 57
  return __nesc_result;
#line 57
}
#line 57
# 59 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
static inline void TOSH_sched_init(void )
{
  int i;

#line 62
  TOSH_sched_free = 0;
  TOSH_sched_full = 0;
  for (i = 0; i < TOSH_MAX_TASKS; i++) 
    TOSH_queue[i].tp = (void *)0;
}

# 158 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline result_t rcombine(result_t r1, result_t r2)



{
  return r1 == FAIL ? FAIL : r2;
}

# 148 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420Control.nc"
inline static result_t CC2420RadioM$CC2420Control$disableAutoAck(void ){
#line 148
  unsigned char __nesc_result;
#line 148

#line 148
  __nesc_result = CC2420ControlM$CC2420Control$disableAutoAck();
#line 148

#line 148
  return __nesc_result;
#line 148
}
#line 148
# 58 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
inline static uint8_t CC2420ControlM$HPLChipcon$write(uint8_t addr, uint16_t data){
#line 58
  unsigned char __nesc_result;
#line 58

#line 58
  __nesc_result = HPLCC2420M$HPLCC2420$write(addr, data);
#line 58

#line 58
  return __nesc_result;
#line 58
}
#line 58
# 413 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420ControlM.nc"
static inline result_t CC2420ControlM$CC2420Control$disableAddrDecode(void )
#line 413
{
  CC2420ControlM$gCurrentParameters[CP_MDMCTRL0] &= ~(1 << 11);
  return CC2420ControlM$HPLChipcon$write(0x11, CC2420ControlM$gCurrentParameters[CP_MDMCTRL0]);
}

# 168 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420Control.nc"
inline static result_t CC2420RadioM$CC2420Control$disableAddrDecode(void ){
#line 168
  unsigned char __nesc_result;
#line 168

#line 168
  __nesc_result = CC2420ControlM$CC2420Control$disableAddrDecode();
#line 168

#line 168
  return __nesc_result;
#line 168
}
#line 168
# 1082 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static inline void CC2420RadioM$MacControl$disableAddrDecode(void )
#line 1082
{
  CC2420RadioM$CC2420Control$disableAddrDecode();
  CC2420RadioM$CC2420Control$disableAutoAck();
  CC2420RadioM$bAckManual = TRUE;
}

#line 1110
static inline void CC2420RadioM$MacControl$disableAck(void )
#line 1110
{
  CC2420RadioM$bAckEnable = FALSE;
  CC2420RadioM$CC2420Control$disableAutoAck();
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/system/RandomLFSR.nc"
static inline result_t RandomLFSR$Random$init(void )
#line 38
{
  {
  }
#line 39
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 40
    {
      RandomLFSR$shiftReg = 119 * 119 * (TOS_LOCAL_ADDRESS + 1);
      RandomLFSR$initSeed = RandomLFSR$shiftReg;
      RandomLFSR$mask = 137 * 29 * (TOS_LOCAL_ADDRESS + 1);
    }
#line 44
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 36 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Random.nc"
inline static result_t CC2420RadioM$Random$init(void ){
#line 36
  unsigned char __nesc_result;
#line 36

#line 36
  __nesc_result = RandomLFSR$Random$init();
#line 36

#line 36
  return __nesc_result;
#line 36
}
#line 36
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t CC2420RadioM$TimerControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = TimerM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 150 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline void TOSH_MAKE_CC_CS_OUTPUT()
#line 150
{
#line 150
  * (volatile uint8_t *)(0x17 + 0x20) |= 1 << 0;
}

# 58 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLCC2420M.nc"
static inline result_t HPLCC2420M$StdControl$init(void )
#line 58
{

  HPLCC2420M$bSpiAvail = TRUE;
  TOSH_MAKE_MISO_INPUT();
  TOSH_MAKE_MOSI_OUTPUT();
  TOSH_MAKE_SPI_SCK_OUTPUT();
  TOSH_MAKE_CC_RSTN_OUTPUT();
  TOSH_MAKE_CC_VREN_OUTPUT();
  TOSH_MAKE_CC_CS_OUTPUT();
  TOSH_MAKE_CC_FIFOP1_INPUT();
  TOSH_MAKE_CC_CCA_INPUT();
  TOSH_MAKE_CC_SFD_INPUT();
  TOSH_MAKE_CC_FIFO_INPUT();
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 71
    {
      TOSH_MAKE_SPI_SCK_OUTPUT();
      TOSH_MAKE_MISO_INPUT();
      TOSH_MAKE_MOSI_OUTPUT();
      * (volatile uint8_t *)(0x0E + 0x20) |= 1 << 0;
      * (volatile uint8_t *)(0x0D + 0x20) |= 1 << 4;
      * (volatile uint8_t *)(0x0D + 0x20) &= ~(1 << 3);
      * (volatile uint8_t *)(0x0D + 0x20) &= ~(1 << 2);
      * (volatile uint8_t *)(0x0D + 0x20) &= ~(1 << 1);
      * (volatile uint8_t *)(0x0D + 0x20) &= ~(1 << 0);

      * (volatile uint8_t *)(0x0D + 0x20) |= 1 << 6;
    }
#line 83
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t CC2420ControlM$HPLChipconControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = HPLCC2420M$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 298 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420Const.h"
static __inline void TOSH_uwait2(int u_sec)
#line 298
{
  while (u_sec > 0) {
       __asm volatile ("nop");
      u_sec--;
    }
}

# 143 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline void TOSH_SET_CC_VREN_PIN()
#line 143
{
#line 143
  * (volatile uint8_t *)(0x1B + 0x20) |= 1 << 5;
}

# 126 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420ControlM.nc"
static inline result_t CC2420ControlM$StdControl$init(void )
#line 126
{


  TOSH_SET_CC_VREN_PIN();
  TOSH_uwait2(600);

  CC2420ControlM$HPLChipconControl$init();


  CC2420ControlM$gCurrentParameters[CP_MAIN] = 0xf800;

  CC2420ControlM$gCurrentParameters[CP_MDMCTRL0] = ((((
  0 << 11) | (
  2 << 8)) | (3 << 6)) | (
  1 << 5)) | (2 << 0);

  CC2420ControlM$gCurrentParameters[CP_MDMCTRL1] = 20 << 6;

  CC2420ControlM$gCurrentParameters[CP_RSSI] = 0xE080;

  CC2420ControlM$gCurrentParameters[CP_SYNCWORD] = 0xA70F;

  CC2420ControlM$gCurrentParameters[CP_TXCTRL] = ((((
  1 << 14) | (
  1 << 13)) | (
  3 << 6)) | (
  1 << 5)) | (
  TOS_CC2420_TXPOWER << 0);

  CC2420ControlM$gCurrentParameters[CP_RXCTRL0] = (((((
  1 << 12) | (
  2 << 8)) | (
  3 << 6)) | (
  2 << 4)) | (
  1 << 2)) | (
  1 << 0);

  CC2420ControlM$gCurrentParameters[CP_RXCTRL1] = (((((
  1 << 11) | (
  1 << 9)) | (
  1 << 6)) | (
  1 << 4)) | (
  1 << 2)) | (
  2 << 0);

  CC2420ControlM$gCurrentParameters[CP_FSCTRL] = (
  1 << 14) | ((
  357 + 5 * (TOS_CC2420_CHANNEL - 11)) << 0);

  CC2420ControlM$gCurrentParameters[CP_SECCTRL0] = (((
  1 << 8) | (
  1 << 7)) | (
  1 << 6)) | (
  1 << 2);

  CC2420ControlM$gCurrentParameters[CP_SECCTRL1] = 0;
  CC2420ControlM$gCurrentParameters[CP_BATTMON] = 0;








  CC2420ControlM$gCurrentParameters[CP_IOCFG0] = (
  100 << 0) | (
  1 << 9);

  CC2420ControlM$gCurrentParameters[CP_IOCFG1] = 0;

  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t CC2420RadioM$CC2420StdControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = CC2420ControlM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 557 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static inline result_t CC2420RadioM$StdControl$init(void )
#line 557
{

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 559
    {
      CC2420RadioM$RadioState = CC2420RadioM$DISABLED_STATE;
      CC2420RadioM$currentDSN = 0;
      CC2420RadioM$bAckEnable = FALSE;
      CC2420RadioM$bAckManual = TRUE;
      CC2420RadioM$rxbufptr = &CC2420RadioM$RxBuf;
      CC2420RadioM$rxbufptr->length = 0;
      CC2420RadioM$rxlength = MSG_DATA_SIZE - 2;

      CC2420RadioM$gImmedSendDone = FAIL;
    }
#line 569
    __nesc_atomic_end(__nesc_atomic); }

  CC2420RadioM$CC2420StdControl$init();
  CC2420RadioM$TimerControl$init();
  CC2420RadioM$Random$init();
  CC2420RadioM$LocalAddr = TOS_LOCAL_ADDRESS;

  CC2420RadioM$MacControl$disableAck();
  CC2420RadioM$MacControl$disableAddrDecode();








  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t AMStandard$RadioControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = CC2420RadioM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 40 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
static inline result_t UARTM$Control$init(void )
#line 40
{
  {
  }
#line 41
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 42
    {
      UARTM$state = 0;
    }
#line 44
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t FramerM$ByteControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = UARTM$Control$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 308 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
static inline result_t FramerM$StdControl$init(void )
#line 308
{
  FramerM$HDLCInitialize();
  return FramerM$ByteControl$init();
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t AMStandard$UARTControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = FramerM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
inline static result_t AMStandard$TimerControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = TimerM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 66 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$Control$init(void )
#line 66
{
  result_t ok1;
#line 67
  result_t ok2;

  AMStandard$TimerControl$init();
  ok1 = AMStandard$UARTControl$init();
  ok2 = AMStandard$RadioControl$init();

  AMStandard$state = FALSE;
  AMStandard$lastCount = 0;
  AMStandard$counter = 0;
  {
  }
#line 76
  ;

  return rcombine(ok1, ok2);
}

# 117 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline void TOSH_CLR_RED_LED_PIN()
#line 117
{
#line 117
  * (volatile uint8_t *)(0x1B + 0x20) &= ~(1 << 2);
}

# 50 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$redOn(void )
#line 50
{
  {
  }
#line 51
  ;
  /* atomic removed: atomic calls only */
#line 52
  {
    TOSH_CLR_RED_LED_PIN();
    LedsC$ledsOn |= LedsC$RED_BIT;
  }
  return SUCCESS;
}

static inline result_t LedsC$Leds$redOff(void )
#line 59
{
  {
  }
#line 60
  ;
  /* atomic removed: atomic calls only */
#line 61
  {
    TOSH_SET_RED_LED_PIN();
    LedsC$ledsOn &= ~LedsC$RED_BIT;
  }
  return SUCCESS;
}

static inline result_t LedsC$Leds$redToggle(void )
#line 68
{
  result_t rval;

#line 70
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 70
    {
      if (LedsC$ledsOn & LedsC$RED_BIT) {
        rval = LedsC$Leds$redOff();
        }
      else {
#line 74
        rval = LedsC$Leds$redOn();
        }
    }
#line 76
    __nesc_atomic_end(__nesc_atomic); }
#line 76
  return rval;
}

# 60 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t BaseM$Leds$redToggle(void ){
#line 60
  unsigned char __nesc_result;
#line 60

#line 60
  __nesc_result = LedsC$Leds$redToggle();
#line 60

#line 60
  return __nesc_result;
#line 60
}
#line 60
# 36 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$init(void )
#line 36
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 37
    {
      LedsC$ledsOn = 0;
      {
      }
#line 39
      ;
      TOSH_MAKE_RED_LED_OUTPUT();
      TOSH_MAKE_YELLOW_LED_OUTPUT();
      TOSH_MAKE_GREEN_LED_OUTPUT();
      TOSH_SET_RED_LED_PIN();
      TOSH_SET_YELLOW_LED_PIN();
      TOSH_SET_GREEN_LED_PIN();
    }
#line 46
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t BaseM$Leds$init(void ){
#line 35
  unsigned char __nesc_result;
#line 35

#line 35
  __nesc_result = LedsC$Leds$init();
#line 35

#line 35
  return __nesc_result;
#line 35
}
#line 35
# 20 "BaseM.nc"
static inline result_t BaseM$StdControl$init(void )
#line 20
{
  BaseM$Leds$init();
  BaseM$Leds$redToggle();
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RealMain$StdControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = BaseM$StdControl$init();
#line 41
  __nesc_result = rcombine(__nesc_result, AMStandard$Control$init());
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 46 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static inline avrtime_t get_time_millis()
#line 46
{



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 50
    {
      unsigned long long __nesc_temp = 
#line 50
      sys_time;

      {
#line 50
        __nesc_atomic_end(__nesc_atomic); 
#line 50
        return __nesc_temp;
      }
    }
#line 52
    __nesc_atomic_end(__nesc_atomic); }
}

#line 70
static inline void reset_start_time()
#line 70
{



  start_time = get_time_millis();
}

#line 20
static inline void set_time_millis(avrtime_t set_time)
#line 20
{



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 24
    sys_time = set_time;
#line 24
    __nesc_atomic_end(__nesc_atomic); }
}



static inline void init_avrtime()
#line 29
{
  if (isInit == 0) {
      avrtime_t t = 132489216000ULL;

#line 32
      isInit = 1;
      set_time_millis(t);
      reset_start_time();
    }
}

# 128 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/HPLClock.nc"
static inline result_t HPLClock$Clock$setRate(char interval, char scale)
#line 128
{
  scale &= 0x7;
  scale |= 0x8;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 131
    {
      * (volatile uint8_t *)(0x37 + 0x20) &= ~(1 << 0);
      * (volatile uint8_t *)(0x37 + 0x20) &= ~(1 << 1);
      * (volatile uint8_t *)(0x30 + 0x20) |= 1 << 3;


      * (volatile uint8_t *)(0x33 + 0x20) = scale;
      * (volatile uint8_t *)(0x32 + 0x20) = 0;
      * (volatile uint8_t *)(0x31 + 0x20) = interval;
      * (volatile uint8_t *)(0x37 + 0x20) |= 1 << 1;
    }
#line 141
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 75 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static result_t TimerM$Clock$setRate(char interval, char scale){
#line 75
  unsigned char __nesc_result;
#line 75

#line 75
  __nesc_result = HPLClock$Clock$setRate(interval, scale);
#line 75

#line 75
  return __nesc_result;
#line 75
}
#line 75
# 150 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline void TOSH_SET_CC_CS_PIN()
#line 150
{
#line 150
  * (volatile uint8_t *)(0x18 + 0x20) |= 1 << 0;
}

# 171 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline result_t rcombine4(result_t r1, result_t r2, result_t r3, 
result_t r4)
{
  return rcombine(r1, rcombine(r2, rcombine(r3, r4)));
}

# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
inline static uint8_t AMStandard$PowerManagement$adjustPower(void ){
#line 19
  unsigned char __nesc_result;
#line 19

#line 19
  __nesc_result = HPLPowerManagementM$PowerManagement$adjustPower();
#line 19

#line 19
  return __nesc_result;
#line 19
}
#line 19
inline static uint8_t TimerM$PowerManagement$adjustPower(void ){
#line 19
  unsigned char __nesc_result;
#line 19

#line 19
  __nesc_result = HPLPowerManagementM$PowerManagement$adjustPower();
#line 19

#line 19
  return __nesc_result;
#line 19
}
#line 19
# 66 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/HPLClock.nc"
static inline void HPLClock$Clock$setInterval(uint8_t value)
#line 66
{
  * (volatile uint8_t *)(0x31 + 0x20) = value;
}

# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static void TimerM$Clock$setInterval(uint8_t value){
#line 84
  HPLClock$Clock$setInterval(value);
#line 84
}
#line 84
# 113 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/HPLClock.nc"
static inline uint8_t HPLClock$Clock$readCounter(void )
#line 113
{
  return * (volatile uint8_t *)(0x32 + 0x20);
}

# 132 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static uint8_t TimerM$Clock$readCounter(void ){
#line 132
  unsigned char __nesc_result;
#line 132

#line 132
  __nesc_result = HPLClock$Clock$readCounter();
#line 132

#line 132
  return __nesc_result;
#line 132
}
#line 132
# 80 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline result_t TimerM$Timer$start(uint8_t id, char type, 
uint32_t interval)
#line 81
{
  uint8_t diff;

#line 83
  if (id >= NUM_TIMERS) {
#line 83
    return FAIL;
    }
#line 84
  if (type > TIMER_ONE_SHOT) {
#line 84
    return FAIL;
    }





  if (type == TIMER_REPEAT && interval <= 2) {
#line 91
    return FAIL;
    }
  TimerM$mTimerList[id].ticks = interval;
  TimerM$mTimerList[id].type = type;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 96
    {
      diff = TimerM$Clock$readCounter();
      interval += diff;
      TimerM$mTimerList[id].ticksLeft = interval;
      TimerM$mState |= 0x1L << id;
      if (interval < TimerM$mInterval) {
          TimerM$mInterval = interval;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
          TimerM$PowerManagement$adjustPower();
        }
    }
#line 107
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/Timer.nc"
inline static result_t AMStandard$ActivityTimer$start(char type, uint32_t interval){
#line 37
  unsigned char __nesc_result;
#line 37

#line 37
  __nesc_result = TimerM$Timer$start(0U, type, interval);
#line 37

#line 37
  return __nesc_result;
#line 37
}
#line 37
# 51 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
inline static uint8_t CC2420ControlM$HPLChipcon$cmd(uint8_t addr){
#line 51
  unsigned char __nesc_result;
#line 51

#line 51
  __nesc_result = HPLCC2420M$HPLCC2420$cmd(addr);
#line 51

#line 51
  return __nesc_result;
#line 51
}
#line 51
# 301 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420ControlM.nc"
static inline result_t CC2420ControlM$CC2420Control$RxMode(void )
#line 301
{
  CC2420ControlM$HPLChipcon$cmd(0x03);
  return SUCCESS;
}

# 112 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420Control.nc"
inline static result_t CC2420RadioM$CC2420Control$RxMode(void ){
#line 112
  unsigned char __nesc_result;
#line 112

#line 112
  __nesc_result = CC2420ControlM$CC2420Control$RxMode();
#line 112

#line 112
  return __nesc_result;
#line 112
}
#line 112
# 58 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
inline static uint8_t CC2420RadioM$HPLChipcon$write(uint8_t addr, uint16_t data){
#line 58
  unsigned char __nesc_result;
#line 58

#line 58
  __nesc_result = HPLCC2420M$HPLCC2420$write(addr, data);
#line 58

#line 58
  return __nesc_result;
#line 58
}
#line 58
#line 41
inline static result_t CC2420ControlM$HPLChipcon$enableFIFOP(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = HPLCC2420M$HPLCC2420$enableFIFOP();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 249 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420ControlM.nc"
static inline result_t CC2420ControlM$CC2420Control$TunePreset(uint8_t chnl)
#line 249
{
  int fsctrl;

  fsctrl = 357 + 5 * (chnl - 11);
  CC2420ControlM$gCurrentParameters[CP_FSCTRL] = (CC2420ControlM$gCurrentParameters[CP_FSCTRL] & 0xfc00) | (fsctrl << 0);
  CC2420ControlM$HPLChipcon$write(0x18, CC2420ControlM$gCurrentParameters[CP_FSCTRL]);
  return SUCCESS;
}

#line 399
static inline result_t CC2420ControlM$HPLChipconRAM$writeDone(uint16_t addr, uint8_t length, uint8_t *buffer)
#line 399
{
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420RAM.nc"
inline static result_t HPLCC2420M$HPLCC2420RAM$writeDone(uint16_t addr, uint8_t length, uint8_t *buffer){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = CC2420ControlM$HPLChipconRAM$writeDone(addr, length, buffer);
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 220 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLCC2420M.nc"
static inline void HPLCC2420M$signalRAMWr(void )
#line 220
{
  HPLCC2420M$HPLCC2420RAM$writeDone(HPLCC2420M$ramaddr, HPLCC2420M$ramlen, HPLCC2420M$rambuf);
}

# 150 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline void TOSH_CLR_CC_CS_PIN()
#line 150
{
#line 150
  * (volatile uint8_t *)(0x18 + 0x20) &= ~(1 << 0);
}

# 231 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLCC2420M.nc"
static inline result_t HPLCC2420M$HPLCC2420RAM$write(uint16_t addr, uint8_t length, uint8_t *buffer)
#line 231
{
  uint8_t i = 0;
  uint8_t status;

  if (!HPLCC2420M$bSpiAvail) {
    return FALSE;
    }
  /* atomic removed: atomic calls only */
#line 238
  {
    HPLCC2420M$bSpiAvail = FALSE;
    HPLCC2420M$ramaddr = addr;
    HPLCC2420M$ramlen = length;
    HPLCC2420M$rambuf = buffer;
    TOSH_CLR_CC_CS_PIN();
    * (volatile uint8_t *)(0x0F + 0x20) = (HPLCC2420M$ramaddr & 0x7F) | 0x80;
    while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
      }
#line 245
    ;
    status = * (volatile uint8_t *)(0x0F + 0x20);
    * (volatile uint8_t *)(0x0F + 0x20) = (HPLCC2420M$ramaddr >> 1) & 0xC0;
    while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
      }
#line 248
    ;
    status = * (volatile uint8_t *)(0x0F + 0x20);

    for (i = 0; i < HPLCC2420M$ramlen; i++) {
        * (volatile uint8_t *)(0x0F + 0x20) = HPLCC2420M$rambuf[i];

        while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
          }
#line 254
        ;
      }
  }
  HPLCC2420M$bSpiAvail = TRUE;
  return TOS_post(HPLCC2420M$signalRAMWr);
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420RAM.nc"
inline static result_t CC2420ControlM$HPLChipconRAM$write(uint16_t addr, uint8_t length, uint8_t *buffer){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = HPLCC2420M$HPLCC2420RAM$write(addr, length, buffer);
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 40 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/byteorder.h"
static __inline int is_host_lsb()
{
  const uint8_t n[2] = { 1, 0 };

#line 43
  return * (uint16_t *)n == 1;
}

static __inline uint16_t toLSB16(uint16_t a)
{
  return is_host_lsb() ? a : (a << 8) | (a >> 8);
}

# 386 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420ControlM.nc"
static inline result_t CC2420ControlM$CC2420Control$setShortAddress(uint16_t addr)
#line 386
{
  addr = toLSB16(addr);
  return CC2420ControlM$HPLChipconRAM$write(0x16A, 2, (uint8_t *)&addr);
}

# 65 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
inline static uint16_t CC2420ControlM$HPLChipcon$read(uint8_t addr){
#line 65
  unsigned short __nesc_result;
#line 65

#line 65
  __nesc_result = HPLCC2420M$HPLCC2420$read(addr);
#line 65

#line 65
  return __nesc_result;
#line 65
}
#line 65
# 94 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420ControlM.nc"
static inline bool CC2420ControlM$SetRegs(void )
#line 94
{
  uint16_t data;

  CC2420ControlM$HPLChipcon$write(0x10, CC2420ControlM$gCurrentParameters[CP_MAIN]);
  CC2420ControlM$HPLChipcon$write(0x11, CC2420ControlM$gCurrentParameters[CP_MDMCTRL0]);
  data = CC2420ControlM$HPLChipcon$read(0x11);
  if (data != CC2420ControlM$gCurrentParameters[CP_MDMCTRL0]) {
#line 100
    return FALSE;
    }
  CC2420ControlM$HPLChipcon$write(0x12, CC2420ControlM$gCurrentParameters[CP_MDMCTRL1]);
  CC2420ControlM$HPLChipcon$write(0x13, CC2420ControlM$gCurrentParameters[CP_RSSI]);
  CC2420ControlM$HPLChipcon$write(0x14, CC2420ControlM$gCurrentParameters[CP_SYNCWORD]);
  CC2420ControlM$HPLChipcon$write(0x15, CC2420ControlM$gCurrentParameters[CP_TXCTRL]);
  CC2420ControlM$HPLChipcon$write(0x16, CC2420ControlM$gCurrentParameters[CP_RXCTRL0]);
  CC2420ControlM$HPLChipcon$write(0x17, CC2420ControlM$gCurrentParameters[CP_RXCTRL1]);
  CC2420ControlM$HPLChipcon$write(0x18, CC2420ControlM$gCurrentParameters[CP_FSCTRL]);

  CC2420ControlM$HPLChipcon$write(0x19, CC2420ControlM$gCurrentParameters[CP_SECCTRL0]);
  CC2420ControlM$HPLChipcon$write(0x1A, CC2420ControlM$gCurrentParameters[CP_SECCTRL1]);
  CC2420ControlM$HPLChipcon$write(0x1C, CC2420ControlM$gCurrentParameters[CP_IOCFG0]);
  CC2420ControlM$HPLChipcon$write(0x1D, CC2420ControlM$gCurrentParameters[CP_IOCFG1]);

  CC2420ControlM$HPLChipcon$cmd(0x09);
  CC2420ControlM$HPLChipcon$cmd(0x08);

  return TRUE;
}

# 102 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline void TOSH_uwait(int u_sec)
#line 102
{
  while (u_sec > 0) {
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
      u_sec--;
    }
}

# 343 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420ControlM.nc"
static inline result_t CC2420ControlM$CC2420Control$OscillatorOn(void )
#line 343
{
  uint8_t i;
  uint8_t status;
  bool bXoscOn = FALSE;

  i = 0;
  CC2420ControlM$HPLChipcon$cmd(0x01);
  while (i < 200 && bXoscOn == FALSE) {
      TOSH_uwait(100);
      status = CC2420ControlM$HPLChipcon$cmd(0x00);
      status = status & (1 << 6);
      if (status) {
#line 354
        bXoscOn = TRUE;
        }
#line 355
      i++;
    }
  if (!bXoscOn) {
#line 357
    return FAIL;
    }
#line 358
  return SUCCESS;
}

# 142 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline void TOSH_SET_CC_RSTN_PIN()
#line 142
{
#line 142
  * (volatile uint8_t *)(0x1B + 0x20) |= 1 << 6;
}

#line 142
static __inline void TOSH_CLR_CC_RSTN_PIN()
#line 142
{
#line 142
  * (volatile uint8_t *)(0x1B + 0x20) &= ~(1 << 6);
}

# 87 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLCC2420M.nc"
static inline result_t HPLCC2420M$StdControl$start(void )
#line 87
{
#line 87
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t CC2420ControlM$HPLChipconControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = HPLCC2420M$StdControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 213 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420ControlM.nc"
static inline result_t CC2420ControlM$StdControl$start(void )
#line 213
{
  result_t status;

  CC2420ControlM$HPLChipconControl$start();


  TOSH_CLR_CC_RSTN_PIN();
  TOSH_uwait2(5);


  TOSH_SET_CC_RSTN_PIN();
  TOSH_uwait2(5);



  status = CC2420ControlM$CC2420Control$OscillatorOn();


  status = CC2420ControlM$SetRegs() && status;
  status = status && CC2420ControlM$CC2420Control$setShortAddress(TOS_LOCAL_ADDRESS);
  status = status && CC2420ControlM$CC2420Control$TunePreset(TOS_CC2420_CHANNEL);

  CC2420ControlM$CC2420Control$RxMode();
  CC2420ControlM$HPLChipcon$enableFIFOP();
  return status;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t CC2420RadioM$CC2420StdControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = CC2420ControlM$StdControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 609 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static inline result_t CC2420RadioM$StdControl$start(void )
#line 609
{
  uint8_t chkRadioState;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 612
    chkRadioState = CC2420RadioM$RadioState;
#line 612
    __nesc_atomic_end(__nesc_atomic); }

  if (chkRadioState == CC2420RadioM$DISABLED_STATE) {




      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 619
        {
          CC2420RadioM$rxbufptr->length = 0;
          CC2420RadioM$RadioState = CC2420RadioM$IDLE_STATE;

          CC2420RadioM$CC2420StdControl$start();

          if (CC2420RadioM$gImmedSendDone) {

              CC2420RadioM$HPLChipcon$write(0x13, 0xE080);
            }

          CC2420RadioM$CC2420Control$RxMode();
        }
#line 631
        __nesc_atomic_end(__nesc_atomic); }
    }

  ;

  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t AMStandard$RadioControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = CC2420RadioM$StdControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 48 "/Users/wbennett/opt/MoteWorks/tos/platform/mica2/HPLUART0M.nc"
static inline result_t HPLUART0M$Setbaud(uint32_t baud_rate)
#line 48
{

  switch (baud_rate) {
      case 4800u: 
        * (volatile uint8_t *)0x90 = 0;
      * (volatile uint8_t *)(0x09 + 0x20) = 191;
      break;

      case 9600u: 
        * (volatile uint8_t *)0x90 = 0;
      * (volatile uint8_t *)(0x09 + 0x20) = 95;
      break;

      case 19200u: 
        * (volatile uint8_t *)0x90 = 0;
      * (volatile uint8_t *)(0x09 + 0x20) = 47;
      break;

      case 57600u: 
        * (volatile uint8_t *)0x90 = 0;
      * (volatile uint8_t *)(0x09 + 0x20) = 15;
      break;

      case 115200u: 
        * (volatile uint8_t *)0x90 = 0;
      * (volatile uint8_t *)(0x09 + 0x20) = 7;
      break;

      default: 
        return FAIL;
    }
  * (volatile uint8_t *)(0x0B + 0x20) = 1 << 1;
  * (volatile uint8_t *)0x95 = (1 << 2) | (1 << 1);
  * (volatile uint8_t *)(0x0A + 0x20) = (((1 << 7) | (1 << 6)) | (1 << 4)) | (1 << 3);
  return SUCCESS;
}



static inline result_t HPLUART0M$UART$init(void )
#line 87
{

  HPLUART0M$Setbaud(TOS_UART0_BAUDRATE);
  return SUCCESS;
}

# 40 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t UARTM$HPLUART$init(void ){
#line 40
  unsigned char __nesc_result;
#line 40

#line 40
  __nesc_result = HPLUART0M$UART$init();
#line 40

#line 40
  return __nesc_result;
#line 40
}
#line 40
# 48 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
static inline result_t UARTM$Control$start(void )
#line 48
{
  return UARTM$HPLUART$init();
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t FramerM$ByteControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = UARTM$Control$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 313 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
static inline result_t FramerM$StdControl$start(void )
#line 313
{
  FramerM$HDLCInitialize();
  return FramerM$ByteControl$start();
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t AMStandard$UARTControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = FramerM$StdControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 69 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline result_t TimerM$StdControl$start(void )
#line 69
{
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t AMStandard$TimerControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = TimerM$StdControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 82 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$Control$start(void )
#line 82
{
  result_t ok0 = AMStandard$TimerControl$start();
  result_t ok1 = AMStandard$UARTControl$start();
  result_t ok2 = AMStandard$RadioControl$start();
  result_t ok3 = AMStandard$ActivityTimer$start(TIMER_REPEAT, 1000);



  AMStandard$state = FALSE;

  AMStandard$PowerManagement$adjustPower();

  return rcombine4(ok0, ok1, ok2, ok3);
}

# 27 "BaseM.nc"
static inline result_t BaseM$StdControl$start(void )
#line 27
{
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RealMain$StdControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = BaseM$StdControl$start();
#line 48
  __nesc_result = rcombine(__nesc_result, AMStandard$Control$start());
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 57 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLPowerManagementM.nc"
static inline uint8_t HPLPowerManagementM$getPowerLevel(void )
#line 57
{
  uint8_t diff;


  if (* (volatile uint8_t *)(0x37 + 0x20) & ~((1 << 1) | (1 << 0))) {
      return HPLPowerManagementM$IDLE;
    }
  else {
    if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)(0x0D + 0x20) & (1 << 7)) {
        return HPLPowerManagementM$IDLE;
      }
    else {
#line 80
      if (* (volatile uint8_t *)0x9A & ((((1 << 7) | (1 << 6)) | (
      1 << 4)) | (1 << 3))) {
          return HPLPowerManagementM$IDLE;
        }
      else {
        if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)(0x06 + 0x20) & (1 << 7)) {
            return HPLPowerManagementM$ADC_NR;
          }
        else {
          if (* (volatile uint8_t *)(0x37 + 0x20) & ((1 << 1) | (1 << 0))) {
              diff = * (volatile uint8_t *)(0x31 + 0x20) - * (volatile uint8_t *)(0x32 + 0x20);
              if (diff < 16) {
#line 91
                return HPLPowerManagementM$EXT_STANDBY;
                }
#line 92
              return HPLPowerManagementM$POWER_SAVE;
            }
          else {
              return HPLPowerManagementM$POWER_DOWN;
            }
          }
        }
      }
    }
}

#line 101
static inline void HPLPowerManagementM$doAdjustment(void )
#line 101
{
  uint8_t foo;
#line 102
  uint8_t mcu;

#line 103
  foo = HPLPowerManagementM$getPowerLevel();
  mcu = * (volatile uint8_t *)(0x35 + 0x20);
  mcu &= 0xe3;
  if (foo == HPLPowerManagementM$EXT_STANDBY || foo == HPLPowerManagementM$POWER_SAVE) {
      mcu |= HPLPowerManagementM$IDLE;
      while ((* (volatile uint8_t *)(0x30 + 0x20) & 0x7) != 0) {
           __asm volatile ("nop");}

      mcu &= 0xe3;
    }
  mcu |= foo;
  * (volatile uint8_t *)(0x35 + 0x20) = mcu;
  * (volatile uint8_t *)(0x35 + 0x20) |= 1 << 5;
}

# 165 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/avrhardware.h"
static __inline void __nesc_enable_interrupt()
#line 165
{
   __asm volatile ("sei");}

#line 150
__inline  void __nesc_atomic_end(__nesc_atomic_t oldSreg)
{
  * (volatile uint8_t *)(0x3F + 0x20) = oldSreg;
}

#line 128
static inline void TOSH_wait()
{
   __asm volatile ("nop");
   __asm volatile ("nop");}

#line 155
static __inline void __nesc_atomic_sleep()
{

   __asm volatile ("sei");
   __asm volatile ("sleep");
  TOSH_wait();
}

#line 143
__inline  __nesc_atomic_t __nesc_atomic_start(void )
{
  __nesc_atomic_t result = * (volatile uint8_t *)(0x3F + 0x20);

#line 146
   __asm volatile ("cli");
  return result;
}

# 116 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
static inline bool TOSH_run_next_task()
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t old_full;
  void (*func)(void );

  fInterruptFlags = __nesc_atomic_start();
  old_full = TOSH_sched_full;
  func = TOSH_queue[old_full].tp;
  if (func == (void *)0) 
    {
      __nesc_atomic_sleep();
      return 0;
    }

  TOSH_queue[old_full].tp = (void *)0;
  TOSH_sched_full = (old_full + 1) & TOSH_TASK_BITMASK;
  __nesc_atomic_end(fInterruptFlags);
  func();

  return 1;
}

static inline void TOSH_run_task()
#line 139
{
  for (; ; ) 
    TOSH_run_next_task();
}

# 111 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline void AMStandard$dbgPacket(TOS_MsgPtr data)
#line 111
{
  uint8_t i;

  for (i = 0; i < sizeof(TOS_Msg ); i++) 
    {
      {
      }
#line 116
      ;
    }
  {
  }
#line 118
  ;
}

# 47 "/Users/wbennett/opt/MoteWorks/lib/SOdebug0.h"
inline static void init_debug()
#line 47
{

  * (volatile uint8_t *)0x90 = 0;



  * (volatile uint8_t *)(0x09 + 0x20) = 7;
  * (volatile uint8_t *)(0x0B + 0x20) = 1 << 1;







  * (volatile uint8_t *)0x95 = (1 << 2) | (1 << 1);
  * (volatile uint8_t *)(0x0C + 0x20);
  * (volatile uint8_t *)(0x0A + 0x20) = 1 << 3;
}

#line 102
static inline int so_printf(const uint8_t *format, ...)
{
  uint8_t format_flag;
  uint32_t u_val = 0;
  uint32_t div_val;
  uint8_t base;
  uint8_t *ptr;
  bool longNumber = FALSE;
  va_list ap;

  __builtin_va_start(ap, format);
  if (!debugStarted) {
      init_debug();
      debugStarted = 1;
    }
#line 116
  ;
  if (format == (void *)0) {
#line 117
    format = "NULL\n";
    }
#line 118
  for (; ; ) 
    {
      if (!longNumber) {
          while ((format_flag = * format++) != '%') 
            {
              if (!format_flag) {
                  return 0;
                }
#line 125
              ;
              UARTPutChar(format_flag);
            }
#line 127
          ;
        }
#line 128
      ;
      switch ((format_flag = * format++)) {
          case 'c': 
            format_flag = (__builtin_va_arg(ap, int ));
          default: 
            UARTPutChar(format_flag);
          continue;

          case 'S': 
            case 's': 
              ptr = (uint8_t *)(__builtin_va_arg(ap, char *));
          while ((format_flag = * ptr++) != 0) {
              UARTPutChar(format_flag);
            }
#line 141
          ;
          continue;
#line 207
          case 'l': 
            longNumber = TRUE;
          continue;

          case 'o': 
            base = 8;
          if (!longNumber) {
            div_val = 0x8000;
            }
          else {
#line 216
            div_val = 0x40000000;
            }
#line 217
          goto CONVERSION_LOOP;

          case 'u': 
            case 'i': 
              case 'd': 
                base = 10;
          if (!longNumber) {
            div_val = 10000;
            }
          else {
#line 226
            div_val = 1000000000;
            }
#line 227
          goto CONVERSION_LOOP;

          case 'x': 
            base = 16;
          if (!longNumber) {
            div_val = 0x1000;
            }
          else {
#line 234
            div_val = 0x10000000;
            }
          CONVERSION_LOOP: 
            {
              if (!longNumber) {
                u_val = (__builtin_va_arg(ap, int ));
                }
              else {
#line 241
                u_val = (__builtin_va_arg(ap, long ));
                }
#line 242
              if (format_flag == 'd' || format_flag == 'i') {
                  bool isNegative;

#line 244
                  if (!longNumber) {
                    isNegative = (int )u_val < 0;
                    }
                  else {
#line 247
                    isNegative = (long )u_val < 0;
                    }
#line 248
                  if (isNegative) {
                      u_val = -u_val;
                      UARTPutChar('-');
                    }
#line 251
                  ;
                  while (div_val > 1 && div_val > u_val) {
                      div_val /= 10;
                    }
#line 254
                  ;
                }

              if (format_flag == 'x' && !longNumber) {
#line 257
                u_val &= 0xffff;
                }
#line 258
              do {
                  UARTPutChar(hex[u_val / div_val]);
                  u_val %= div_val;
                  div_val /= base;
                }
              while (div_val);
              longNumber = FALSE;
            }
#line 265
          ;
          break;
        }
#line 267
      ;
    }
#line 268
  ;
}

# 119 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline void TOSH_CLR_YELLOW_LED_PIN()
#line 119
{
#line 119
  * (volatile uint8_t *)(0x1B + 0x20) &= ~(1 << 0);
}

# 108 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$yellowOn(void )
#line 108
{
  {
  }
#line 109
  ;
  /* atomic removed: atomic calls only */
#line 110
  {
    TOSH_CLR_YELLOW_LED_PIN();
    LedsC$ledsOn |= LedsC$YELLOW_BIT;
  }
  return SUCCESS;
}

static inline result_t LedsC$Leds$yellowOff(void )
#line 117
{
  {
  }
#line 118
  ;
  /* atomic removed: atomic calls only */
#line 119
  {
    TOSH_SET_YELLOW_LED_PIN();
    LedsC$ledsOn &= ~LedsC$YELLOW_BIT;
  }
  return SUCCESS;
}

static inline result_t LedsC$Leds$yellowToggle(void )
#line 126
{
  result_t rval;

#line 128
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 128
    {
      if (LedsC$ledsOn & LedsC$YELLOW_BIT) {
        rval = LedsC$Leds$yellowOff();
        }
      else {
#line 132
        rval = LedsC$Leds$yellowOn();
        }
    }
#line 134
    __nesc_atomic_end(__nesc_atomic); }
#line 134
  return rval;
}

# 110 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t BaseM$Leds$yellowToggle(void ){
#line 110
  unsigned char __nesc_result;
#line 110

#line 110
  __nesc_result = LedsC$Leds$yellowToggle();
#line 110

#line 110
  return __nesc_result;
#line 110
}
#line 110
# 34 "BaseM.nc"
static inline TOS_MsgPtr BaseM$ReceiveMsg$receive(TOS_MsgPtr m)
#line 34
{
  compass_msg_t *message = (compass_msg_t *)m->data;
  uint8_t rssi;
#line 36
  uint8_t lqi;

  rssi = m->strength & 0x1F;
  if (rssi > 28) {
      rssi = 28;
    }
  lqi = m->lqi;
  BaseM$Leds$yellowToggle();
  sprintf(dbg_buffer, "%u,%u,%u,%u,%i,%i,%i,%i,%i\n", rssi, lqi, message->seqnum, message->heading, message->pitch, message->roll, message->ax, message->ay, message->az);
#line 44
  {
#line 44
    if (1 != 0) {
#line 44
        so_printf("%s", dbg_buffer);
      }
#line 44
    ;
  }
#line 44
  ;









  return m;
}

# 221 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline TOS_MsgPtr AMStandard$ReceiveMsg$default$receive(uint8_t id, TOS_MsgPtr msg)
#line 221
{
  return msg;
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
inline static TOS_MsgPtr AMStandard$ReceiveMsg$receive(uint8_t arg_0x101352108, TOS_MsgPtr m){
#line 53
  struct TOS_Msg *__nesc_result;
#line 53

#line 53
  switch (arg_0x101352108) {
#line 53
    case AM_COMPASS_MSG:
#line 53
      __nesc_result = BaseM$ReceiveMsg$receive(m);
#line 53
      break;
#line 53
    default:
#line 53
      __nesc_result = AMStandard$ReceiveMsg$default$receive(arg_0x101352108, m);
#line 53
      break;
#line 53
    }
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 391 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420ControlM.nc"
static inline result_t CC2420ControlM$HPLChipcon$FIFOPIntr(void )
#line 391
{
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
inline static result_t CC2420RadioM$HPLChipcon$enableFIFOP(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = HPLCC2420M$HPLCC2420$enableFIFOP();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 231 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline TOS_MsgPtr AMStandard$RadioReceive$receive(TOS_MsgPtr packet)
#line 231
{
  return received(packet);
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
inline static TOS_MsgPtr CC2420RadioM$Receive$receive(TOS_MsgPtr m){
#line 53
  struct TOS_Msg *__nesc_result;
#line 53

#line 53
  __nesc_result = AMStandard$RadioReceive$receive(m);
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 1298 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static inline TOS_MsgPtr CC2420RadioM$default$asyncReceive(TOS_MsgPtr pBuf)
#line 1298
{
  return pBuf;
}

#line 73
inline static TOS_MsgPtr CC2420RadioM$asyncReceive(TOS_MsgPtr pBuf){
#line 73
  struct TOS_Msg *__nesc_result;
#line 73

#line 73
  __nesc_result = CC2420RadioM$default$asyncReceive(pBuf);
#line 73

#line 73
  return __nesc_result;
#line 73
}
#line 73
#line 502
static __inline void CC2420RadioM$immedPacketRcvd(void )
#line 502
{
  TOS_MsgPtr pBuf;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 506
    {
      CC2420RadioM$rxbufptr->time = 0;
      pBuf = CC2420RadioM$rxbufptr;
    }
#line 509
    __nesc_atomic_end(__nesc_atomic); }









  if (CC2420RadioM$gImmedSendDone) {
      pBuf = CC2420RadioM$asyncReceive((TOS_MsgPtr )pBuf);
    }
  else 
#line 521
    {
      pBuf = CC2420RadioM$Receive$receive((TOS_MsgPtr )pBuf);
    }

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 525
    {



      if (pBuf) {
#line 529
        CC2420RadioM$rxbufptr = pBuf;
        }
#line 530
      CC2420RadioM$rxbufptr->length = 0;
      CC2420RadioM$bRxBufLocked = FALSE;
    }
#line 532
    __nesc_atomic_end(__nesc_atomic); }






  CC2420RadioM$HPLChipcon$enableFIFOP();

  ;
}



static inline void CC2420RadioM$PacketRcvd(void )
#line 546
{
  CC2420RadioM$immedPacketRcvd();
}

# 149 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline int TOSH_READ_CC_SFD_PIN()
#line 149
{
#line 149
  return (* (volatile uint8_t *)(0x10 + 0x20) & (1 << 4)) != 0;
}

# 51 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
inline static uint8_t CC2420RadioM$HPLChipcon$cmd(uint8_t addr){
#line 51
  unsigned char __nesc_result;
#line 51

#line 51
  __nesc_result = HPLCC2420M$HPLCC2420$cmd(addr);
#line 51

#line 51
  return __nesc_result;
#line 51
}
#line 51
# 51 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/byteorder.h"
static __inline uint16_t fromLSB16(uint16_t a)
{
  return is_host_lsb() ? a : (a << 8) | (a >> 8);
}

# 162 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLTimer2.nc"
static inline void HPLTimer2$Timer2$intDisable(void )
#line 162
{
  * (volatile uint8_t *)(0x37 + 0x20) &= ~(1 << 7);
}

# 147 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static void TimerJiffyAsyncM$Timer$intDisable(void ){
#line 147
  HPLTimer2$Timer2$intDisable();
#line 147
}
#line 147
# 115 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/TimerJiffyAsyncM.nc"
static inline result_t TimerJiffyAsyncM$TimerJiffyAsync$stop(void )
{
  /* atomic removed: atomic calls only */
#line 117
  {
    TimerJiffyAsyncM$bSet = 0;
    TimerJiffyAsyncM$Timer$intDisable();
  }
  return SUCCESS;
}

# 36 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/TimerJiffyAsync.nc"
inline static result_t CC2420RadioM$BackoffTimerJiffy$stop(void ){
#line 36
  unsigned char __nesc_result;
#line 36

#line 36
  __nesc_result = TimerJiffyAsyncM$TimerJiffyAsync$stop();
#line 36

#line 36
  return __nesc_result;
#line 36
}
#line 36
# 189 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$RadioSend$sendDone(TOS_MsgPtr msg, result_t success)
#line 189
{
  return AMStandard$reportSendDone(msg, success);
}

# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
inline static result_t CC2420RadioM$Send$sendDone(TOS_MsgPtr msg, result_t success){
#line 45
  unsigned char __nesc_result;
#line 45

#line 45
  __nesc_result = AMStandard$RadioSend$sendDone(msg, success);
#line 45

#line 45
  return __nesc_result;
#line 45
}
#line 45
# 458 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static __inline void CC2420RadioM$immedPacketSent(void )
#line 458
{

  TOS_MsgPtr pBuf;
  uint8_t currentstate;

#line 462
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 462
    currentstate = CC2420RadioM$RadioState;
#line 462
    __nesc_atomic_end(__nesc_atomic); }

  if (currentstate == CC2420RadioM$POST_TX_STATE) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 465
        {
          CC2420RadioM$RadioState = CC2420RadioM$IDLE_STATE;
          CC2420RadioM$txbufptr->time = 0;
          pBuf = CC2420RadioM$txbufptr;

          pBuf->length = pBuf->length - MSG_HEADER_SIZE - MSG_FOOTER_SIZE;
        }
#line 471
        __nesc_atomic_end(__nesc_atomic); }





      while (TOSH_READ_CC_SFD_PIN()) {
        }
#line 477
      ;

      ;

      CC2420RadioM$Send$sendDone(pBuf, SUCCESS);
    }






  return;
}

#line 551
static inline void CC2420RadioM$PacketSent(void )
#line 551
{
  CC2420RadioM$immedPacketSent();
}

#line 1305
static inline void CC2420RadioM$default$shortReceived(void )
#line 1305
{
  return;
}

#line 74
inline static void CC2420RadioM$shortReceived(void ){
#line 74
  CC2420RadioM$default$shortReceived();
#line 74
}
#line 74
# 46 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420FIFO.nc"
inline static result_t CC2420RadioM$HPLChipconFIFO$readRXFIFO(uint8_t length, uint8_t *data){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = HPLCC2420FIFOM$HPLCC2420FIFO$readRXFIFO(length, data);
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 65 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
inline static uint16_t CC2420RadioM$HPLChipcon$read(uint8_t addr){
#line 65
  unsigned short __nesc_result;
#line 65

#line 65
  __nesc_result = HPLCC2420M$HPLCC2420$read(addr);
#line 65

#line 65
  return __nesc_result;
#line 65
}
#line 65
# 151 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline int TOSH_READ_CC_FIFO_PIN()
#line 151
{
#line 151
  return (* (volatile uint8_t *)(0x16 + 0x20) & (1 << 7)) != 0;
}

# 34 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/TimerJiffyAsync.nc"
inline static result_t CC2420RadioM$BackoffTimerJiffy$setOneShot(uint32_t jiffy){
#line 34
  unsigned char __nesc_result;
#line 34

#line 34
  __nesc_result = TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(jiffy);
#line 34

#line 34
  return __nesc_result;
#line 34
}
#line 34
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Random.nc"
inline static uint16_t CC2420RadioM$Random$rand(void ){
#line 42
  unsigned short __nesc_result;
#line 42

#line 42
  __nesc_result = RandomLFSR$Random$rand();
#line 42

#line 42
  return __nesc_result;
#line 42
}
#line 42
# 1321 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static inline int16_t CC2420RadioM$MacBackoff$default$congestionBackoff(TOS_MsgPtr m)
#line 1321
{
  return (CC2420RadioM$Random$rand() & 0xF) + 1;
}

# 44 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/MacBackoff.nc"
inline static int16_t CC2420RadioM$MacBackoff$congestionBackoff(TOS_MsgPtr m){
#line 44
  short __nesc_result;
#line 44

#line 44
  __nesc_result = CC2420RadioM$MacBackoff$default$congestionBackoff(m);
#line 44

#line 44
  return __nesc_result;
#line 44
}
#line 44
# 110 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/TimerJiffyAsyncM.nc"
static inline bool TimerJiffyAsyncM$TimerJiffyAsync$isSet(void )
{
  return TimerJiffyAsyncM$bSet;
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/TimerJiffyAsync.nc"
inline static bool CC2420RadioM$BackoffTimerJiffy$isSet(void ){
#line 38
  unsigned char __nesc_result;
#line 38

#line 38
  __nesc_result = TimerJiffyAsyncM$TimerJiffyAsync$isSet();
#line 38

#line 38
  return __nesc_result;
#line 38
}
#line 38
# 906 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static inline result_t CC2420RadioM$HPLChipcon$FIFOPIntr(void )
#line 906
{


  uint8_t *pData;
  uint8_t length = MSG_DATA_SIZE;
  uint8_t currentstate;
  int16_t backoffValue;

  /* atomic removed: atomic calls only */
#line 914
  currentstate = CC2420RadioM$RadioState;

  ;






  if (CC2420RadioM$bAckEnable && currentstate == CC2420RadioM$PRE_TX_STATE) {
      if (CC2420RadioM$BackoffTimerJiffy$isSet()) {
          CC2420RadioM$BackoffTimerJiffy$stop();
          backoffValue = CC2420RadioM$MacBackoff$congestionBackoff(CC2420RadioM$txbufptr)
           * 10 + 20;
          CC2420RadioM$BackoffTimerJiffy$setOneShot(backoffValue);
        }
    }


  if (!TOSH_READ_CC_FIFO_PIN() || CC2420RadioM$bRxBufLocked) {
      CC2420RadioM$HPLChipcon$read(0x3F);
      CC2420RadioM$HPLChipcon$cmd(0x08);
      CC2420RadioM$HPLChipcon$cmd(0x08);
      return FAIL;
    }


  pData = (uint8_t *)CC2420RadioM$rxbufptr;
  length = CC2420RadioM$HPLChipconFIFO$readRXFIFO(1, pData);


  CC2420RadioM$rxbufptr->length &= 0x7f;


  length = CC2420RadioM$rxbufptr->length;


  if (length > MSG_DATA_SIZE - 1) {
      CC2420RadioM$HPLChipcon$read(0x3F);
      CC2420RadioM$HPLChipcon$cmd(0x08);
      CC2420RadioM$HPLChipcon$cmd(0x08);
      /* atomic removed: atomic calls only */
#line 955
      CC2420RadioM$bRxBufLocked = FALSE;
      return FAIL;
    }


  pData = (uint8_t *)CC2420RadioM$rxbufptr + 1;
  length = CC2420RadioM$HPLChipconFIFO$readRXFIFO(length, pData);






  if (!(pData[length - 1] & 0x80)) {
      /* atomic removed: atomic calls only */
#line 969
      CC2420RadioM$bRxBufLocked = FALSE;
      return SUCCESS;
    }


  if (CC2420RadioM$rxbufptr->length < 5) {
      /* atomic removed: atomic calls only */
#line 975
      CC2420RadioM$bRxBufLocked = FALSE;
      CC2420RadioM$shortReceived();
      return SUCCESS;
    }




  if ((
#line 981
  CC2420RadioM$rxbufptr->fcfhi & 0x03) == 0x02 && 
  CC2420RadioM$rxbufptr->dsn == CC2420RadioM$currentDSN && (
  CC2420RadioM$bAckEnable || CC2420RadioM$bAckManual) && 
  currentstate == CC2420RadioM$POST_TX_STATE) {

      CC2420RadioM$txbufptr->ack = 1;

      {
#line 988
        ;
#line 988
        TOSH_uwait(50);
#line 988
        ;
      }
#line 988
      ;



      if (TOS_post(CC2420RadioM$PacketSent)) {
#line 992
        CC2420RadioM$BackoffTimerJiffy$stop();
        }
      ;

      return SUCCESS;
    }



  if ((CC2420RadioM$rxbufptr->fcfhi & 0x03) != 0x01) {
      /* atomic removed: atomic calls only */
#line 1002
      CC2420RadioM$bRxBufLocked = FALSE;
      return SUCCESS;
    }
  /* atomic removed: atomic calls only */
  CC2420RadioM$bRxBufLocked = TRUE;

  CC2420RadioM$rxbufptr->length = CC2420RadioM$rxbufptr->length - MSG_HEADER_SIZE - MSG_FOOTER_SIZE;


  CC2420RadioM$rxbufptr->addr = fromLSB16(CC2420RadioM$rxbufptr->addr);


  CC2420RadioM$rxbufptr->crc = pData[length - 1] >> 7;


  CC2420RadioM$rxbufptr->strength = pData[length - 2];
  CC2420RadioM$rxbufptr->ack = FALSE;



  if (CC2420RadioM$bAckManual) {
      if (CC2420RadioM$rxbufptr->addr == TOS_LOCAL_ADDRESS && 
      CC2420RadioM$rxbufptr->group == TOS_AM_GROUP) {

          CC2420RadioM$HPLChipcon$cmd(0x0A);
          while (!TOSH_READ_CC_SFD_PIN()) {
            }
#line 1027
          ;

          {
#line 1029
            ;
#line 1029
            TOSH_uwait(50);
#line 1029
            ;
          }
#line 1029
          ;
        }
    }









  while (TOSH_READ_CC_SFD_PIN()) {
    }
#line 1041
  ;



  if (CC2420RadioM$gImmedSendDone) {
      CC2420RadioM$immedPacketRcvd();
    }
  else {
#line 1048
    if (!TOS_post(CC2420RadioM$PacketRcvd)) {
      /* atomic removed: atomic calls only */
#line 1049
      CC2420RadioM$bRxBufLocked = FALSE;
      }
    }
#line 1051
  return SUCCESS;
}

# 44 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
inline static result_t HPLCC2420M$HPLCC2420$FIFOPIntr(void ){
#line 44
  unsigned char __nesc_result;
#line 44

#line 44
  __nesc_result = CC2420RadioM$HPLChipcon$FIFOPIntr();
#line 44
  __nesc_result = rcombine(__nesc_result, CC2420ControlM$HPLChipcon$FIFOPIntr());
#line 44

#line 44
  return __nesc_result;
#line 44
}
#line 44
# 136 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$SendMsg$default$sendDone(uint8_t id, TOS_MsgPtr msg, result_t success)
#line 136
{
  return SUCCESS;
}

# 27 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendMsg.nc"
inline static result_t AMStandard$SendMsg$sendDone(uint8_t arg_0x101353470, TOS_MsgPtr msg, result_t success){
#line 27
  unsigned char __nesc_result;
#line 27

#line 27
    __nesc_result = AMStandard$SendMsg$default$sendDone(arg_0x101353470, msg, success);
#line 27

#line 27
  return __nesc_result;
#line 27
}
#line 27
# 139 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$default$sendDone(void )
#line 139
{
  return SUCCESS;
}

#line 44
inline static result_t AMStandard$sendDone(void ){
#line 44
  unsigned char __nesc_result;
#line 44

#line 44
  __nesc_result = AMStandard$default$sendDone();
#line 44

#line 44
  return __nesc_result;
#line 44
}
#line 44
# 76 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/HPLClock.nc"
static inline uint8_t HPLClock$Clock$getInterval(void )
#line 76
{
  return * (volatile uint8_t *)(0x31 + 0x20);
}

# 100 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static uint8_t TimerM$Clock$getInterval(void ){
#line 100
  unsigned char __nesc_result;
#line 100

#line 100
  __nesc_result = HPLClock$Clock$getInterval();
#line 100

#line 100
  return __nesc_result;
#line 100
}
#line 100
# 38 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static inline void add_time_millis(uint32_t time_to_add)
#line 38
{



  sys_time += time_to_add;
}

# 111 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
inline static void TimerM$adjustInterval(void )
#line 111
{
  uint8_t i;
#line 112
  uint8_t val = TimerM$maxTimerInterval;

#line 113
  if (TimerM$mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM$mState & (0x1L << i) && TimerM$mTimerList[i].ticksLeft < val) {
              val = TimerM$mTimerList[i].ticksLeft;
            }
        }
#line 130
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 130
        {
          i = TimerM$Clock$readCounter() + 3;
          if (val < i) {
              val = i;
            }
          TimerM$mInterval = val;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
        }
#line 138
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 141
        {
          TimerM$mInterval = TimerM$maxTimerInterval;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
        }
#line 145
        __nesc_atomic_end(__nesc_atomic); }
    }
  TimerM$PowerManagement$adjustPower();
}

#line 168
static inline void TimerM$enqueue(uint8_t value)
#line 168
{
  if (TimerM$queue_tail == NUM_TIMERS - 1) {
    TimerM$queue_tail = -1;
    }
#line 171
  TimerM$queue_tail++;
  TimerM$queue_size++;
  TimerM$queue[(uint8_t )TimerM$queue_tail] = value;
}

# 130 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$ActivityTimer$fired(void )
#line 130
{
  AMStandard$lastCount = AMStandard$counter;
  AMStandard$counter = 0;
  return SUCCESS;
}

# 164 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline result_t TimerM$Timer$default$fired(uint8_t id)
#line 164
{
  return SUCCESS;
}

# 51 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/Timer.nc"
inline static result_t TimerM$Timer$fired(uint8_t arg_0x1016082f0){
#line 51
  unsigned char __nesc_result;
#line 51

#line 51
  switch (arg_0x1016082f0) {
#line 51
    case 0U:
#line 51
      __nesc_result = AMStandard$ActivityTimer$fired();
#line 51
      break;
#line 51
    default:
#line 51
      __nesc_result = TimerM$Timer$default$fired(arg_0x1016082f0);
#line 51
      break;
#line 51
    }
#line 51

#line 51
  return __nesc_result;
#line 51
}
#line 51
# 176 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline uint8_t TimerM$dequeue(void )
#line 176
{
  if (TimerM$queue_size == 0) {
    return NUM_TIMERS;
    }
#line 179
  if (TimerM$queue_head == NUM_TIMERS - 1) {
    TimerM$queue_head = -1;
    }
#line 181
  TimerM$queue_head++;
  TimerM$queue_size--;
  return TimerM$queue[(uint8_t )TimerM$queue_head];
}

static inline void TimerM$signalOneTimer(void )
#line 186
{
  uint8_t itimer = TimerM$dequeue();

#line 188
  if (itimer < NUM_TIMERS) {
    TimerM$Timer$fired(itimer);
    }
}

#line 192
static inline void TimerM$HandleFire(void )
#line 192
{
  uint8_t i;
  uint16_t int_out;


  TimerM$setIntervalFlag = 1;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 200
    {
      int_out = TimerM$interval_outstanding;
      TimerM$interval_outstanding = 0;
    }
#line 203
    __nesc_atomic_end(__nesc_atomic); }
  if (TimerM$mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM$mState & (0x1L << i)) {
              TimerM$mTimerList[i].ticksLeft -= int_out;
              if (TimerM$mTimerList[i].ticksLeft <= 2) {


                  if (TOS_post(TimerM$signalOneTimer)) {
                      if (TimerM$mTimerList[i].type == TIMER_REPEAT) {
                          TimerM$mTimerList[i].ticksLeft += TimerM$mTimerList[i].ticks;
                        }
                      else 
#line 214
                        {
                          TimerM$mState &= ~(0x1L << i);
                        }
                      TimerM$enqueue(i);
                    }
                  else {
                      {
                      }
#line 220
                      ;


                      TimerM$mTimerList[i].ticksLeft = TimerM$mInterval;
                    }
                }
            }
        }
    }


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 231
    int_out = TimerM$interval_outstanding;
#line 231
    __nesc_atomic_end(__nesc_atomic); }
  if (int_out == 0) {
    TimerM$adjustInterval();
    }
}

static inline result_t TimerM$Clock$fire(void )
#line 237
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 238
    {



      if (TimerM$interval_outstanding == 0) {
          TOS_post(TimerM$HandleFire);
        }
      else 
        {
        }
#line 246
      ;

      TimerM$interval_outstanding += TimerM$Clock$getInterval() + 1;


      add_time_millis(TimerM$Clock$getInterval());
    }
#line 252
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static result_t HPLClock$Clock$fire(void ){
#line 159
  unsigned char __nesc_result;
#line 159

#line 159
  __nesc_result = TimerM$Clock$fire();
#line 159

#line 159
  return __nesc_result;
#line 159
}
#line 159
#line 127
inline static result_t TimerJiffyAsyncM$Timer$setIntervalAndScale(uint8_t interval, uint8_t scale){
#line 127
  unsigned char __nesc_result;
#line 127

#line 127
  __nesc_result = HPLTimer2$Timer2$setIntervalAndScale(interval, scale);
#line 127

#line 127
  return __nesc_result;
#line 127
}
#line 127
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
inline static uint8_t TimerJiffyAsyncM$PowerManagement$adjustPower(void ){
#line 19
  unsigned char __nesc_result;
#line 19

#line 19
  __nesc_result = HPLPowerManagementM$PowerManagement$adjustPower();
#line 19

#line 19
  return __nesc_result;
#line 19
}
#line 19
# 216 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static __inline result_t CC2420RadioM$setBackoffTimer(uint16_t jiffy)
#line 216
{
  CC2420RadioM$stateTimer = CC2420RadioM$TIMER_BACKOFF;

  if (jiffy == 0xffff) {
      return FAIL;
    }
  else 
#line 221
    {
      return CC2420RadioM$BackoffTimerJiffy$setOneShot(jiffy);
    }
}

# 114 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLCC2420M.nc"
static inline result_t HPLCC2420M$HPLCC2420$disableFIFOP(void )
#line 114
{
  * (volatile uint8_t *)(0x39 + 0x20) &= ~(1 << 6);
  return SUCCESS;
}

# 42 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420.nc"
inline static result_t CC2420RadioM$HPLChipcon$disableFIFOP(void ){
#line 42
  unsigned char __nesc_result;
#line 42

#line 42
  __nesc_result = HPLCC2420M$HPLCC2420$disableFIFOP();
#line 42

#line 42
  return __nesc_result;
#line 42
}
#line 42
# 153 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/hardware.h"
static __inline int TOSH_READ_RADIO_CCA_PIN()
#line 153
{
#line 153
  return (* (volatile uint8_t *)(0x10 + 0x20) & (1 << 6)) != 0;
}

# 377 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static inline void CC2420RadioM$tryToSend(void )
#line 377
{
  uint8_t currentstate;
  int16_t backoffValue;

#line 380
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 380
    currentstate = CC2420RadioM$RadioState;
#line 380
    __nesc_atomic_end(__nesc_atomic); }


  if (currentstate == CC2420RadioM$PRE_TX_STATE || currentstate == CC2420RadioM$TX_STATE) {

      if (TOSH_READ_RADIO_CCA_PIN()) {
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 386
            CC2420RadioM$RadioState = CC2420RadioM$TX_STATE;
#line 386
            __nesc_atomic_end(__nesc_atomic); }
          CC2420RadioM$HPLChipcon$disableFIFOP();
          CC2420RadioM$sendPacket();
        }
      else 
#line 389
        {

          if (CC2420RadioM$cnttryToSend-- <= 0) {
              CC2420RadioM$fSendAborted();
              return;
            }

          backoffValue = CC2420RadioM$MacBackoff$congestionBackoff(CC2420RadioM$txbufptr) * 10;
          if (!CC2420RadioM$setBackoffTimer(backoffValue)) {
#line 397
            CC2420RadioM$fSendAborted();
            }
        }
    }
}

# 56 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/HPLCC2420FIFO.nc"
inline static result_t CC2420RadioM$HPLChipconFIFO$writeTXFIFO(uint8_t length, uint8_t *data){
#line 56
  unsigned char __nesc_result;
#line 56

#line 56
  __nesc_result = HPLCC2420FIFOM$HPLCC2420FIFO$writeTXFIFO(length, data);
#line 56

#line 56
  return __nesc_result;
#line 56
}
#line 56
# 247 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static inline result_t CC2420RadioM$fTXPacket(uint8_t len, uint8_t *pMsg)
#line 247
{

  uint8_t lenToWrite;


  if (!CC2420RadioM$HPLChipcon$cmd(0x09)) {
#line 252
    return FAIL;
    }




  lenToWrite = (unsigned short )& ((TOS_Msg *)0)->data;
  if (!(lenToWrite = CC2420RadioM$HPLChipconFIFO$writeTXFIFO(lenToWrite, (uint8_t *)pMsg))) {
    return FAIL;
    }








  return SUCCESS;
}

#line 658
static inline result_t CC2420RadioM$BackoffTimerJiffy$fired(void )
#line 658
{
  uint8_t cret;
  uint8_t currentstate;

#line 661
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 661
    currentstate = CC2420RadioM$RadioState;
#line 661
    __nesc_atomic_end(__nesc_atomic); }

  switch (CC2420RadioM$stateTimer) {

      case CC2420RadioM$TIMER_INITIAL: 
        CC2420RadioM$stateTimer = CC2420RadioM$TIMER_IDLE;


      CC2420RadioM$HPLChipcon$disableFIFOP();


      cret = CC2420RadioM$fTXPacket(CC2420RadioM$txlength + 1, (uint8_t *)CC2420RadioM$txbufptr);


      if (!cret) {
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 676
            CC2420RadioM$RadioState = CC2420RadioM$IDLE_STATE;
#line 676
            __nesc_atomic_end(__nesc_atomic); }
          CC2420RadioM$fSendAborted();
        }
      else 


        {
          CC2420RadioM$sendPacket();
        }


      CC2420RadioM$HPLChipcon$enableFIFOP();
      break;

      case CC2420RadioM$TIMER_BACKOFF: 
        CC2420RadioM$stateTimer = CC2420RadioM$TIMER_IDLE;
      CC2420RadioM$tryToSend();
      break;

      case CC2420RadioM$TIMER_ACK: 
        CC2420RadioM$stateTimer = CC2420RadioM$TIMER_IDLE;
      if (currentstate == CC2420RadioM$POST_TX_STATE) {
          CC2420RadioM$txbufptr->ack = 0;
          if (!TOS_post(CC2420RadioM$PacketSent)) {
              CC2420RadioM$fSendAborted();
            }
        }
      break;


      case CC2420RadioM$TIMER_SNIFF: 
        CC2420RadioM$stateTimer = CC2420RadioM$TIMER_IDLE;
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 708
        CC2420RadioM$gSniffDone = SUCCESS;
#line 708
        __nesc_atomic_end(__nesc_atomic); }
      break;
    }

  return SUCCESS;
}

# 40 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/TimerJiffyAsync.nc"
inline static result_t TimerJiffyAsyncM$TimerJiffyAsync$fired(void ){
#line 40
  unsigned char __nesc_result;
#line 40

#line 40
  __nesc_result = CC2420RadioM$BackoffTimerJiffy$fired();
#line 40

#line 40
  return __nesc_result;
#line 40
}
#line 40
# 73 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/TimerJiffyAsyncM.nc"
static inline result_t TimerJiffyAsyncM$Timer$fire(void )
#line 73
{
  uint16_t localjiffy;

#line 75
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 75
    localjiffy = TimerJiffyAsyncM$jiffy;
#line 75
    __nesc_atomic_end(__nesc_atomic); }
  if (localjiffy < 0xFF) {
      TimerJiffyAsyncM$Timer$intDisable();
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 78
        TimerJiffyAsyncM$bSet = 0;
#line 78
        __nesc_atomic_end(__nesc_atomic); }
      TimerJiffyAsyncM$TimerJiffyAsync$fired();
      TimerJiffyAsyncM$PowerManagement$adjustPower();
    }
  else {

      localjiffy = localjiffy >> 8;
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 85
        TimerJiffyAsyncM$jiffy = localjiffy;
#line 85
        __nesc_atomic_end(__nesc_atomic); }
      TimerJiffyAsyncM$Timer$setIntervalAndScale(localjiffy, 0x4);
    }
  return SUCCESS;
}

# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static result_t HPLTimer2$Timer2$fire(void ){
#line 159
  unsigned char __nesc_result;
#line 159

#line 159
  __nesc_result = TimerJiffyAsyncM$Timer$fire();
#line 159

#line 159
  return __nesc_result;
#line 159
}
#line 159
# 1326 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static inline 
#line 1325
void CC2420RadioM$RadioSendCoordinator$default$startSymbol(
uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff)
#line 1326
{
}

# 15 "/Users/wbennett/opt/MoteWorks/tos/interfaces/RadioCoordinator.nc"
inline static void CC2420RadioM$RadioSendCoordinator$startSymbol(uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff){
#line 15
  CC2420RadioM$RadioSendCoordinator$default$startSymbol(bitsPerBlock, offset, msgBuff);
#line 15
}
#line 15
# 231 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static __inline result_t CC2420RadioM$setAckTimer(uint16_t jiffy)
#line 231
{
  CC2420RadioM$stateTimer = CC2420RadioM$TIMER_ACK;
  return CC2420RadioM$BackoffTimerJiffy$setOneShot(jiffy);
}

# 49 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/crc.h"
static inline uint16_t crcByte(uint16_t oldCrc, uint8_t byte)
{

  uint16_t *table = crcTable;
  uint16_t newCrc;

   __asm ("eor %1,%B3\n"
  "\tlsl %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tadd %A2, %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tlpm\n"
  "\tmov %B0, %A3\n"
  "\tmov %A0, r0\n"
  "\tadiw r30,1\n"
  "\tlpm\n"
  "\teor %B0, r0" : 
  "=r"(newCrc), "+r"(byte), "+z"(table) : "r"(oldCrc));
  return newCrc;
}

# 216 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
static inline void FramerM$PacketUnknown(void )
#line 216
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 217
    {
      FramerM$gFlags |= FramerM$FLAGS_UNKNOWN;
    }
#line 219
    __nesc_atomic_end(__nesc_atomic); }

  FramerM$StartTx();
}

# 225 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline TOS_MsgPtr AMStandard$UARTReceive$receive(TOS_MsgPtr packet)
#line 225
{


  packet->group = TOS_AM_GROUP;
  return received(packet);
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
inline static TOS_MsgPtr FramerAckM$ReceiveCombined$receive(TOS_MsgPtr m){
#line 53
  struct TOS_Msg *__nesc_result;
#line 53

#line 53
  __nesc_result = AMStandard$UARTReceive$receive(m);
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 56 "/Users/wbennett/opt/MoteWorks/tos/system/FramerAckM.nc"
static inline TOS_MsgPtr FramerAckM$ReceiveMsg$receive(TOS_MsgPtr Msg)
#line 56
{
  TOS_MsgPtr pBuf;

  pBuf = FramerAckM$ReceiveCombined$receive(Msg);

  return pBuf;
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
inline static TOS_MsgPtr FramerM$ReceiveMsg$receive(TOS_MsgPtr m){
#line 53
  struct TOS_Msg *__nesc_result;
#line 53

#line 53
  __nesc_result = FramerAckM$ReceiveMsg$receive(m);
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 345 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
static inline result_t FramerM$TokenReceiveMsg$ReflectToken(uint8_t Token)
#line 345
{
  result_t Result = SUCCESS;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 348
    {
      if (!(FramerM$gFlags & FramerM$FLAGS_TOKENPEND)) {
          FramerM$gFlags |= FramerM$FLAGS_TOKENPEND;
          FramerM$gTxTokenBuf = Token;
        }
      else {
          Result = FAIL;
        }
    }
#line 356
    __nesc_atomic_end(__nesc_atomic); }

  if (Result == SUCCESS) {
      Result = FramerM$StartTx();
    }

  return Result;
}

# 59 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
inline static result_t FramerAckM$TokenReceiveMsg$ReflectToken(uint8_t Token){
#line 59
  unsigned char __nesc_result;
#line 59

#line 59
  __nesc_result = FramerM$TokenReceiveMsg$ReflectToken(Token);
#line 59

#line 59
  return __nesc_result;
#line 59
}
#line 59
# 39 "/Users/wbennett/opt/MoteWorks/tos/system/FramerAckM.nc"
static inline void FramerAckM$SendAckTask(void )
#line 39
{

  FramerAckM$TokenReceiveMsg$ReflectToken(FramerAckM$gTokenBuf);
}

static inline TOS_MsgPtr FramerAckM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t token)
#line 44
{
  TOS_MsgPtr pBuf;

  FramerAckM$gTokenBuf = token;

  TOS_post(FramerAckM$SendAckTask);

  pBuf = FramerAckM$ReceiveCombined$receive(Msg);

  return pBuf;
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
inline static TOS_MsgPtr FramerM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t Token){
#line 46
  struct TOS_Msg *__nesc_result;
#line 46

#line 46
  __nesc_result = FramerAckM$TokenReceiveMsg$receive(Msg, Token);
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 224 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
static inline void FramerM$PacketRcvd(void )
#line 224
{
  FramerM$MsgRcvEntry_t *pRcv = &FramerM$gMsgRcvTbl[FramerM$gRxTailIndex];
  TOS_MsgPtr pBuf = pRcv->pMsg;



  if (pRcv->Length >= 5) {



      switch (pRcv->Proto) {
          case FramerM$PROTO_ACK: 
            break;
          case FramerM$PROTO_PACKET_ACK: 
            pBuf->crc = 1;
          pBuf = FramerM$TokenReceiveMsg$receive(pBuf, pRcv->Token);
          break;
          case FramerM$PROTO_PACKET_NOACK: 
            pBuf->crc = 1;
          pBuf = FramerM$ReceiveMsg$receive(pBuf);
          break;
          default: 
            FramerM$gTxUnknownBuf = pRcv->Proto;
          TOS_post(FramerM$PacketUnknown);
          break;
        }
    }

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 252
    {
      if (pBuf) {
          pRcv->pMsg = pBuf;
        }
      pRcv->Length = 0;
      pRcv->Token = 0;
      FramerM$gRxTailIndex++;
      FramerM$gRxTailIndex %= FramerM$HDLC_QUEUESIZE;
    }
#line 260
    __nesc_atomic_end(__nesc_atomic); }
}

#line 365
static inline result_t FramerM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength)
#line 365
{

  switch (FramerM$gRxState) {

      case FramerM$RXSTATE_NOSYNC: 
        if (data == FramerM$HDLC_FLAG_BYTE && FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length == 0) {

            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
            FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
            FramerM$gpRxBuf = (uint8_t *)FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].pMsg;
            FramerM$gRxState = FramerM$RXSTATE_PROTO;
          }
      break;

      case FramerM$RXSTATE_PROTO: 
        if (data == FramerM$HDLC_FLAG_BYTE) {
            break;
          }
      FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Proto = data;
      FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, data);
      switch (data) {
          case FramerM$PROTO_PACKET_ACK: 
            FramerM$gRxState = FramerM$RXSTATE_TOKEN;
          break;
          case FramerM$PROTO_PACKET_NOACK: 
            FramerM$gRxState = FramerM$RXSTATE_INFO;
          break;
          default: 
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          break;
        }
      break;

      case FramerM$RXSTATE_TOKEN: 
        if (data == FramerM$HDLC_FLAG_BYTE) {
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          }
        else {
#line 402
          if (data == FramerM$HDLC_CTLESC_BYTE) {
              FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0x20;
            }
          else {
              FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token ^= data;
              FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token);
              FramerM$gRxState = FramerM$RXSTATE_INFO;
            }
          }
#line 410
      break;


      case FramerM$RXSTATE_INFO: 
        if (FramerM$gRxByteCnt > FramerM$HDLC_MTU) {
            FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          }
        else {
#line 420
          if (data == FramerM$HDLC_CTLESC_BYTE) {
              FramerM$gRxState = FramerM$RXSTATE_ESC;
            }
          else {
#line 423
            if (data == FramerM$HDLC_FLAG_BYTE) {
                if (FramerM$gRxByteCnt >= 2) {

                    uint16_t usRcvdCRC = FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt - 1)] & 0xff;

#line 427
                    usRcvdCRC = (usRcvdCRC << 8) | (FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt - 2)] & 0xff);






                    if (usRcvdCRC == FramerM$gRxRunningCRC) {
                        FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = FramerM$gRxByteCnt - 2;
                        TOS_post(FramerM$PacketRcvd);
                        FramerM$gRxHeadIndex++;
#line 437
                        FramerM$gRxHeadIndex %= FramerM$HDLC_QUEUESIZE;
                      }
                    else {
                        FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
                        FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
                        FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
                        FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
                        break;
                      }
                    if (FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length == 0) {
                        FramerM$gpRxBuf = (uint8_t *)FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].pMsg;
                        FramerM$gRxState = FramerM$RXSTATE_PROTO;
                      }
                    else {
                        FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
                      }
                  }
                else {
                    FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
                    FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
                    FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
                  }
                FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
              }
            else {
                FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt)] = data;
                if (FramerM$gRxByteCnt >= 2) {
                    FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt - 2)]);
                  }
                FramerM$gRxByteCnt++;
              }
            }
          }
#line 468
      break;

      case FramerM$RXSTATE_ESC: 
        if (data == FramerM$HDLC_FLAG_BYTE) {

            FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          }
        else {
            data = data ^ 0x20;
            FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt)] = data;
            if (FramerM$gRxByteCnt >= 2) {
                FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt - 2)]);
              }
            FramerM$gRxByteCnt++;
            FramerM$gRxState = FramerM$RXSTATE_INFO;
          }
      break;

      default: 
        FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
      break;
    }

  return SUCCESS;
}

# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t UARTM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength){
#line 45
  unsigned char __nesc_result;
#line 45

#line 45
  __nesc_result = FramerM$ByteComm$rxByteReady(data, error, strength);
#line 45

#line 45
  return __nesc_result;
#line 45
}
#line 45
# 57 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
static inline result_t UARTM$HPLUART$get(uint8_t data)
#line 57
{




  UARTM$ByteComm$rxByteReady(data, 0, 0);
  {
  }
#line 63
  ;
  return SUCCESS;
}

# 66 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t HPLUART0M$UART$get(uint8_t data){
#line 66
  unsigned char __nesc_result;
#line 66

#line 66
  __nesc_result = UARTM$HPLUART$get(data);
#line 66

#line 66
  return __nesc_result;
#line 66
}
#line 66
# 117 "/Users/wbennett/opt/MoteWorks/tos/platform/mica2/HPLUART0M.nc"
static inline result_t HPLUART0M$UART$put(uint8_t data)
#line 117
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 118
    {
      * (volatile uint8_t *)(0x0C + 0x20) = data;
      * (volatile uint8_t *)(0x0B + 0x20) |= 1 << 6;
    }
#line 121
    __nesc_atomic_end(__nesc_atomic); }

  return SUCCESS;
}

# 58 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t UARTM$HPLUART$put(uint8_t data){
#line 58
  unsigned char __nesc_result;
#line 58

#line 58
  __nesc_result = HPLUART0M$UART$put(data);
#line 58

#line 58
  return __nesc_result;
#line 58
}
#line 58
# 186 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$UARTSend$sendDone(TOS_MsgPtr msg, result_t success)
#line 186
{
  return AMStandard$reportSendDone(msg, success);
}

# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
inline static result_t FramerM$BareSendMsg$sendDone(TOS_MsgPtr msg, result_t success){
#line 45
  unsigned char __nesc_result;
#line 45

#line 45
  __nesc_result = AMStandard$UARTSend$sendDone(msg, success);
#line 45

#line 45
  return __nesc_result;
#line 45
}
#line 45
# 34 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t FramerM$ByteComm$txByte(uint8_t data){
#line 34
  unsigned char __nesc_result;
#line 34

#line 34
  __nesc_result = UARTM$ByteComm$txByte(data);
#line 34

#line 34
  return __nesc_result;
#line 34
}
#line 34
# 510 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
static inline result_t FramerM$ByteComm$txByteReady(bool LastByteSuccess)
#line 510
{
  result_t TxResult = SUCCESS;
  uint8_t nextByte;

  if (LastByteSuccess != 1) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 515
        FramerM$gTxState = FramerM$TXSTATE_ERROR;
#line 515
        __nesc_atomic_end(__nesc_atomic); }
      TOS_post(FramerM$PacketSent);
      return SUCCESS;
    }

  switch (FramerM$gTxState) {

      case FramerM$TXSTATE_PROTO: 
        FramerM$gTxState = FramerM$TXSTATE_INFO;
      FramerM$gTxRunningCRC = crcByte(FramerM$gTxRunningCRC, FramerM$gTxProto);
      TxResult = FramerM$ByteComm$txByte(FramerM$gTxProto);
      break;

      case FramerM$TXSTATE_INFO: 
        if (FramerM$gTxProto == FramerM$PROTO_ACK) {
          nextByte = FramerM$gpTxBuf[0];
          }
        else {
#line 532
          nextByte = FramerM$gpTxBuf[FramerM$gTxByteCnt];
          }
#line 533
      FramerM$gTxRunningCRC = crcByte(FramerM$gTxRunningCRC, nextByte);
      FramerM$gTxByteCnt++;

      if (FramerM$gTxByteCnt == 10) {
        FramerM$gTxByteCnt = 0;
        }
#line 538
      if (FramerM$gTxByteCnt == 1) {
        FramerM$gTxByteCnt = 10;
        }
      if (FramerM$gTxByteCnt >= FramerM$gTxLength) {
          FramerM$gTxState = FramerM$TXSTATE_FCS1;
        }

      TxResult = FramerM$TxArbitraryByte(nextByte);
      break;

      case FramerM$TXSTATE_ESC: 

        TxResult = FramerM$ByteComm$txByte(FramerM$gTxEscByte ^ 0x20);
      FramerM$gTxState = FramerM$gPrevTxState;
      break;

      case FramerM$TXSTATE_FCS1: 
        nextByte = (uint8_t )(FramerM$gTxRunningCRC & 0xff);
      FramerM$gTxState = FramerM$TXSTATE_FCS2;
      TxResult = FramerM$TxArbitraryByte(nextByte);
      break;

      case FramerM$TXSTATE_FCS2: 
        nextByte = (uint8_t )((FramerM$gTxRunningCRC >> 8) & 0xff);
      FramerM$gTxState = FramerM$TXSTATE_ENDFLAG;
      TxResult = FramerM$TxArbitraryByte(nextByte);
      break;

      case FramerM$TXSTATE_ENDFLAG: 
        FramerM$gTxState = FramerM$TXSTATE_FINISH;
      TxResult = FramerM$ByteComm$txByte(FramerM$HDLC_FLAG_BYTE);

      break;

      case FramerM$TXSTATE_FINISH: 
        case FramerM$TXSTATE_ERROR: 

          default: 
            break;
    }


  if (TxResult != SUCCESS) {
      FramerM$gTxState = FramerM$TXSTATE_ERROR;
      TOS_post(FramerM$PacketSent);
    }

  return SUCCESS;
}

# 54 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t UARTM$ByteComm$txByteReady(bool success){
#line 54
  unsigned char __nesc_result;
#line 54

#line 54
  __nesc_result = FramerM$ByteComm$txByteReady(success);
#line 54

#line 54
  return __nesc_result;
#line 54
}
#line 54
# 588 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
static inline result_t FramerM$ByteComm$txDone(void )
#line 588
{

  if (FramerM$gTxState == FramerM$TXSTATE_FINISH) {
      FramerM$gTxState = FramerM$TXSTATE_IDLE;
      TOS_post(FramerM$PacketSent);
    }

  return SUCCESS;
}

# 62 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t UARTM$ByteComm$txDone(void ){
#line 62
  unsigned char __nesc_result;
#line 62

#line 62
  __nesc_result = FramerM$ByteComm$txDone();
#line 62

#line 62
  return __nesc_result;
#line 62
}
#line 62
# 67 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
static inline result_t UARTM$HPLUART$putDone(void )
#line 67
{
  bool oldState;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 70
    {
      {
      }
#line 71
      ;
      oldState = UARTM$state;
      UARTM$state = 0;
    }
#line 74
    __nesc_atomic_end(__nesc_atomic); }








  if (oldState) {
      UARTM$ByteComm$txDone();
      UARTM$ByteComm$txByteReady(1);
    }
  return SUCCESS;
}

# 74 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t HPLUART0M$UART$putDone(void ){
#line 74
  unsigned char __nesc_result;
#line 74

#line 74
  __nesc_result = UARTM$HPLUART$putDone();
#line 74

#line 74
  return __nesc_result;
#line 74
}
#line 74
# 82 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
 bool TOS_post(void (*tp)())
#line 82
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t tmp;



  fInterruptFlags = __nesc_atomic_start();

  tmp = TOSH_sched_free;

  if (TOSH_queue[tmp].tp == (void *)0) {
      TOSH_sched_free = (tmp + 1) & TOSH_TASK_BITMASK;
      TOSH_queue[tmp].tp = tp;
      __nesc_atomic_end(fInterruptFlags);

      return TRUE;
    }
  else {
      __nesc_atomic_end(fInterruptFlags);

      return FALSE;
    }
}

# 34 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
  int main(void )
#line 34
{


  uint8_t local_symbol_ref;

  local_symbol_ref = TOS_PLATFORM;
  local_symbol_ref = TOS_BASE_STATION;
  local_symbol_ref = TOS_DATA_LENGTH;

  local_symbol_ref = TOS_ROUTE_PROTOCOL;








  RealMain$hardwareInit();
  RealMain$Pot$init(10);
  TOSH_sched_init();

  RealMain$StdControl$init();
  RealMain$StdControl$start();
  __nesc_enable_interrupt();

  while (1) {
      TOSH_run_task();
    }
}

# 57 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static result_t TimerM$StdControl$init(void )
#line 57
{

  init_avrtime();
  TimerM$mState = 0;
  TimerM$setIntervalFlag = 0;
  TimerM$queue_head = TimerM$queue_tail = -1;
  TimerM$queue_size = 0;
  TimerM$mScale = 3;
  TimerM$mInterval = TimerM$maxTimerInterval;
  return TimerM$Clock$setRate(TimerM$mInterval, TimerM$mScale);
}

# 285 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
static void FramerM$HDLCInitialize(void )
#line 285
{
  int i;

#line 287
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 287
    {
      for (i = 0; i < FramerM$HDLC_QUEUESIZE; i++) {
          FramerM$gMsgRcvTbl[i].pMsg = &FramerM$gMsgRcvBuf[i];
          FramerM$gMsgRcvTbl[i].Length = 0;
          FramerM$gMsgRcvTbl[i].Token = 0;
        }
      FramerM$gTxState = FramerM$TXSTATE_IDLE;
      FramerM$gTxByteCnt = 0;
      FramerM$gTxLength = 0;
      FramerM$gTxRunningCRC = 0;
      FramerM$gpTxMsg = (void *)0;

      FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
      FramerM$gRxHeadIndex = 0;
      FramerM$gRxTailIndex = 0;
      FramerM$gRxByteCnt = 0;
      FramerM$gRxRunningCRC = 0;
      FramerM$gpRxBuf = (uint8_t *)FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].pMsg;
    }
#line 305
    __nesc_atomic_end(__nesc_atomic); }
}

# 381 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420ControlM.nc"
static result_t CC2420ControlM$CC2420Control$disableAutoAck(void )
#line 381
{
  CC2420ControlM$gCurrentParameters[CP_MDMCTRL0] &= ~(1 << 4);
  return CC2420ControlM$HPLChipcon$write(0x11, CC2420ControlM$gCurrentParameters[CP_MDMCTRL0]);
}

# 147 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLCC2420M.nc"
static result_t HPLCC2420M$HPLCC2420$write(uint8_t addr, uint16_t data)
#line 147
{
  uint8_t status;



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 152
    {
      HPLCC2420M$bSpiAvail = FALSE;
      TOSH_CLR_CC_CS_PIN();
      * (volatile uint8_t *)(0x0F + 0x20) = addr;
      while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
        }
#line 156
      ;
      status = * (volatile uint8_t *)(0x0F + 0x20);
      if (addr > 0x0E) {
          * (volatile uint8_t *)(0x0F + 0x20) = data >> 8;
          while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
            }
#line 160
          ;
          * (volatile uint8_t *)(0x0F + 0x20) = data & 0xff;
          while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
            }
#line 162
          ;
        }
      HPLCC2420M$bSpiAvail = TRUE;
    }
#line 165
    __nesc_atomic_end(__nesc_atomic); }
  TOSH_SET_CC_CS_PIN();
  return status;
}

#line 122
static uint8_t HPLCC2420M$HPLCC2420$cmd(uint8_t addr)
#line 122
{
  uint8_t status;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 125
    {
      TOSH_CLR_CC_CS_PIN();
      * (volatile uint8_t *)(0x0F + 0x20) = addr;
      while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
        }
#line 128
      ;
      status = * (volatile uint8_t *)(0x0F + 0x20);
    }
#line 130
    __nesc_atomic_end(__nesc_atomic); }
  TOSH_SET_CC_CS_PIN();
  return status;
}

#line 178
static uint16_t HPLCC2420M$HPLCC2420$read(uint8_t addr)
#line 178
{

  uint16_t data = 0;
  uint8_t status;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 184
    {
      HPLCC2420M$bSpiAvail = FALSE;
      TOSH_CLR_CC_CS_PIN();
      * (volatile uint8_t *)(0x0F + 0x20) = addr | 0x40;
      while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
        }
#line 188
      ;
      status = * (volatile uint8_t *)(0x0F + 0x20);
      * (volatile uint8_t *)(0x0F + 0x20) = 0;
      while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
        }
#line 191
      ;
      data = * (volatile uint8_t *)(0x0F + 0x20);
      * (volatile uint8_t *)(0x0F + 0x20) = 0;
      while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
        }
#line 194
      ;
      data = (data << 8) | * (volatile uint8_t *)(0x0F + 0x20);
      TOSH_SET_CC_CS_PIN();
      HPLCC2420M$bSpiAvail = TRUE;
    }
#line 198
    __nesc_atomic_end(__nesc_atomic); }
  return data;
}

#line 103
static result_t HPLCC2420M$HPLCC2420$enableFIFOP(void )
#line 103
{
  * (volatile uint8_t *)(0x3A + 0x20) &= ~(1 << 4);
  * (volatile uint8_t *)(0x3A + 0x20) &= ~(1 << 5);

  * (volatile uint8_t *)(0x39 + 0x20) |= 1 << 6;
  return SUCCESS;
}

# 121 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLPowerManagementM.nc"
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void )
#line 121
{
  uint8_t mcu;

#line 123
  if (!HPLPowerManagementM$disabled) {
    TOS_post(HPLPowerManagementM$doAdjustment);
    }
  else 
#line 125
    {
      mcu = * (volatile uint8_t *)(0x35 + 0x20);
      mcu &= 0xe3;
      mcu |= HPLPowerManagementM$IDLE;
      * (volatile uint8_t *)(0x35 + 0x20) = mcu;
      * (volatile uint8_t *)(0x35 + 0x20) |= 1 << 5;
    }
  return 0;
}

# 194 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
  TOS_MsgPtr received(TOS_MsgPtr packet)
#line 194
{
  uint16_t addr = TOS_LOCAL_ADDRESS;

#line 196
  AMStandard$counter++;
  {
  }
#line 197
  ;


  if (
#line 199
  packet->crc == 1 && 
  packet->group == TOS_AM_GROUP && (
  packet->addr == TOS_BCAST_ADDR || 
  packet->addr == addr)) 
    {

      uint8_t type = packet->type;
      TOS_MsgPtr tmp;

      {
      }
#line 208
      ;
      AMStandard$dbgPacket(packet);
      {
      }
#line 210
      ;


      tmp = AMStandard$ReceiveMsg$receive(type, packet);
      if (tmp) {
        packet = tmp;
        }
    }
#line 217
  return packet;
}

# 82 "/Users/wbennett/opt/MoteWorks/lib/SOdebug0.h"
static void UARTPutChar(char c)
#line 82
{
  if (c == '\n') {
      do {
        }
      while (
#line 84
      !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)(0x0B + 0x20) & (1 << 5)));

      * (volatile uint8_t *)(0x0C + 0x20) = 0xd;
      do {
        }
      while (
#line 87
      !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)(0x0B + 0x20) & (1 << 5)));
      * (volatile uint8_t *)(0x0C + 0x20) = 0xa;

      do {
        }
      while (
#line 90
      !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)(0x0B + 0x20) & (1 << 6)));

      TOSH_uwait(100);
      return;
    }
#line 94
  ;
  do {
    }
  while (
#line 95
  !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)(0x0B + 0x20) & (1 << 5)));
  * (volatile uint8_t *)(0x0C + 0x20) = c;
  do {
    }
  while (
#line 97
  !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)(0x0B + 0x20) & (1 << 6)));
  return;
}

# 202 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLCC2420M.nc"
  __attribute((signal)) void __vector_7(void )
#line 202
{

  HPLCC2420M$HPLCC2420$FIFOPIntr();
}

# 49 "/Users/wbennett/opt/MoteWorks/tos/system/RandomLFSR.nc"
static uint16_t RandomLFSR$Random$rand(void )
#line 49
{
  bool endbit;
  uint16_t tmpShiftReg;

#line 52
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 52
    {
      tmpShiftReg = RandomLFSR$shiftReg;
      endbit = (tmpShiftReg & 0x8000) != 0;
      tmpShiftReg <<= 1;
      if (endbit) {
        tmpShiftReg ^= 0x100b;
        }
#line 58
      tmpShiftReg++;
      RandomLFSR$shiftReg = tmpShiftReg;
      tmpShiftReg = tmpShiftReg ^ RandomLFSR$mask;
    }
#line 61
    __nesc_atomic_end(__nesc_atomic); }
  return tmpShiftReg;
}

# 93 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/TimerJiffyAsyncM.nc"
static result_t TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(uint32_t _jiffy)
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 95
    {
      TimerJiffyAsyncM$jiffy = _jiffy;
      TimerJiffyAsyncM$bSet = 1;
    }
#line 98
    __nesc_atomic_end(__nesc_atomic); }
  if (_jiffy > 0xFF) {
      TimerJiffyAsyncM$Timer$setIntervalAndScale(0xFF, 0x4);
    }
  else {
      TimerJiffyAsyncM$Timer$setIntervalAndScale(_jiffy, 0x4);
    }

  TimerJiffyAsyncM$PowerManagement$adjustPower();
  return SUCCESS;
}

# 114 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLTimer2.nc"
static result_t HPLTimer2$Timer2$setIntervalAndScale(uint8_t interval, uint8_t scale)
#line 114
{

  if (scale > 7) {
#line 116
    return FAIL;
    }
#line 117
  scale |= 0x8;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 118
    {
      * (volatile uint8_t *)(0x25 + 0x20) = 0;
      * (volatile uint8_t *)(0x37 + 0x20) &= ~(1 << 7);
      * (volatile uint8_t *)(0x37 + 0x20) &= ~(1 << 6);
      HPLTimer2$mscale = scale;
      HPLTimer2$minterval = interval;
      * (volatile uint8_t *)(0x24 + 0x20) = 0;
      * (volatile uint8_t *)(0x23 + 0x20) = interval;
      * (volatile uint8_t *)(0x36 + 0x20) |= 1 << 7;
      * (volatile uint8_t *)(0x37 + 0x20) |= 1 << 7;
      * (volatile uint8_t *)(0x25 + 0x20) = scale;
    }
#line 129
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 125 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLCC2420FIFOM.nc"
static result_t HPLCC2420FIFOM$HPLCC2420FIFO$readRXFIFO(uint8_t len, uint8_t *msg)
#line 125
{
  uint8_t status;
#line 126
  uint8_t i;

  /* atomic removed: atomic calls only */

  {
    HPLCC2420FIFOM$bSpiAvail = FALSE;
    HPLCC2420FIFOM$rxbuf = msg;
    HPLCC2420FIFOM$rxlength = len;
    TOSH_CLR_CC_CS_PIN();
    * (volatile uint8_t *)(0x0F + 0x20) = 0x3F | 0x40;
    while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
      }
#line 136
    ;
    status = * (volatile uint8_t *)(0x0F + 0x20);

    i = 0;
    while (TOSH_READ_CC_FIFO_PIN() && i < HPLCC2420FIFOM$rxlength) {
        * (volatile uint8_t *)(0x0F + 0x20) = 0;
        while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
          }
#line 142
        ;
        HPLCC2420FIFOM$rxbuf[i] = * (volatile uint8_t *)(0x0F + 0x20);
        i++;
      }
    HPLCC2420FIFOM$rxlength = i;
    HPLCC2420FIFOM$bSpiAvail = TRUE;
  }
  TOSH_SET_CC_CS_PIN();








  return HPLCC2420FIFOM$rxlength;
}

# 122 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static result_t AMStandard$reportSendDone(TOS_MsgPtr msg, result_t success)
#line 122
{
  AMStandard$state = FALSE;
  AMStandard$SendMsg$sendDone(msg->type, msg, success);
  AMStandard$sendDone();

  return SUCCESS;
}

# 146 "/Users/wbennett/opt/MoteWorks/tos/platform/atm128/HPLClock.nc"
  __attribute((interrupt)) void __vector_15(void )
#line 146
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 147
    {
      if (HPLClock$set_flag) {
          HPLClock$mscale = HPLClock$nextScale;
          HPLClock$nextScale |= 0x8;
          * (volatile uint8_t *)(0x33 + 0x20) = HPLClock$nextScale;

          * (volatile uint8_t *)(0x31 + 0x20) = HPLClock$minterval;
          HPLClock$set_flag = 0;
        }
    }
#line 156
    __nesc_atomic_end(__nesc_atomic); }
  HPLClock$Clock$fire();
}

# 169 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLTimer2.nc"
  __attribute((interrupt)) void __vector_9(void )
#line 169
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 170
    {
      if (HPLTimer2$set_flag) {
          HPLTimer2$mscale = HPLTimer2$nextScale;
          HPLTimer2$nextScale |= 0x8;
          * (volatile uint8_t *)(0x25 + 0x20) = HPLTimer2$nextScale;
          * (volatile uint8_t *)(0x23 + 0x20) = HPLTimer2$minterval;
          HPLTimer2$set_flag = 0;
        }
    }
#line 178
    __nesc_atomic_end(__nesc_atomic); }
  HPLTimer2$Timer2$fire();
}

# 71 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/HPLCC2420FIFOM.nc"
static result_t HPLCC2420FIFOM$HPLCC2420FIFO$writeTXFIFO(uint8_t len, uint8_t *msg)
#line 71
{
  uint8_t i = 0;
  uint8_t status;



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 77
    {
      HPLCC2420FIFOM$bSpiAvail = FALSE;
      HPLCC2420FIFOM$txlength = len;
      HPLCC2420FIFOM$txbuf = msg;
      TOSH_CLR_CC_CS_PIN();
      * (volatile uint8_t *)(0x0F + 0x20) = 0x3E;
      while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
        }
#line 83
      ;
      status = * (volatile uint8_t *)(0x0F + 0x20);
      for (i = 0; i < HPLCC2420FIFOM$txlength; i++) {
          * (volatile uint8_t *)(0x0F + 0x20) = *HPLCC2420FIFOM$txbuf;
          HPLCC2420FIFOM$txbuf++;
          while (!(* (volatile uint8_t *)(0x0E + 0x20) & 0x80)) {
            }
#line 88
          ;
        }
      HPLCC2420FIFOM$bSpiAvail = TRUE;
    }
#line 91
    __nesc_atomic_end(__nesc_atomic); }
  TOSH_SET_CC_CS_PIN();




  if (!status) {
    HPLCC2420FIFOM$txlength = status;
    }
#line 99
  return HPLCC2420FIFOM$txlength;
}

# 415 "/Users/wbennett/opt/MoteWorks/tos/radio/cc2420/CC2420RadioM.nc"
static void CC2420RadioM$fSendAborted(void )
#line 415
{
  TOS_MsgPtr pBuf;
  uint8_t currentstate;


  CC2420RadioM$HPLChipcon$read(0x3F);
  CC2420RadioM$HPLChipcon$cmd(0x08);
  CC2420RadioM$HPLChipcon$read(0x3F);
  CC2420RadioM$HPLChipcon$cmd(0x08);


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 426
    currentstate = CC2420RadioM$RadioState;
#line 426
    __nesc_atomic_end(__nesc_atomic); }
  if (currentstate >= CC2420RadioM$PRE_TX_STATE && currentstate <= CC2420RadioM$POST_TX_STATE) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 428
        {
          CC2420RadioM$txbufptr->time = 0;
          pBuf = CC2420RadioM$txbufptr;

          pBuf->length = pBuf->length - MSG_HEADER_SIZE - MSG_FOOTER_SIZE;
          CC2420RadioM$RadioState = CC2420RadioM$IDLE_STATE;
        }
#line 434
        __nesc_atomic_end(__nesc_atomic); }
      CC2420RadioM$Send$sendDone(pBuf, FAIL);
    }








  CC2420RadioM$HPLChipcon$enableFIFOP();
  return;
}

#line 286
static result_t CC2420RadioM$sendPacket(void )
#line 286
{
  uint8_t status;
  uint16_t fail_count = 0;
  uint8_t currentstate;
  int16_t backoffValue;
  uint8_t offset;

  CC2420RadioM$HPLChipcon$cmd(0x05);


  ;

  status = CC2420RadioM$HPLChipcon$cmd(0x00);
  if ((status >> 3) & 0x01) {

      while (!TOSH_READ_CC_SFD_PIN()) {
          fail_count++;
          TOSH_uwait(5);
          if (fail_count > 1000) {
              CC2420RadioM$fSendAborted();

              ;

              return FAIL;
            }
        }
#line 311
      ;


      CC2420RadioM$RadioSendCoordinator$startSymbol(8, 0, CC2420RadioM$txbufptr);
      offset = (unsigned short )& ((TOS_Msg *)0)->data;


      CC2420RadioM$HPLChipconFIFO$writeTXFIFO(CC2420RadioM$txlength + 1 - offset, (uint8_t *)CC2420RadioM$txbufptr + offset);

      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 320
        currentstate = CC2420RadioM$RadioState;
#line 320
        __nesc_atomic_end(__nesc_atomic); }
      switch (currentstate) {
          case CC2420RadioM$PRE_TX_STATE: 
            case CC2420RadioM$TX_STATE: 
              { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 324
                CC2420RadioM$RadioState = CC2420RadioM$POST_TX_STATE;
#line 324
                __nesc_atomic_end(__nesc_atomic); }
          CC2420RadioM$HPLChipcon$enableFIFOP();
          CC2420RadioM$txbufptr->ack = 1;


          if ((CC2420RadioM$bAckEnable || CC2420RadioM$bAckManual) && CC2420RadioM$txbufptr->addr != TOS_BCAST_ADDR) {
              CC2420RadioM$txbufptr->ack = 0;
              while (TOSH_READ_CC_SFD_PIN()) {
                }
#line 331
              ;
              if (CC2420RadioM$setAckTimer(2 * 20)) {
                return SUCCESS;
                }
            }





          if (CC2420RadioM$gImmedSendDone) {
              CC2420RadioM$immedPacketSent();
            }
          else 
#line 343
            {
              if (!TOS_post(CC2420RadioM$PacketSent)) {
                  CC2420RadioM$fSendAborted();
                  return FAIL;
                }
            }
          break;

          default: 
            { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 352
              CC2420RadioM$RadioState = CC2420RadioM$IDLE_STATE;
#line 352
              __nesc_atomic_end(__nesc_atomic); }
          CC2420RadioM$HPLChipcon$enableFIFOP();
          return FAIL;
          break;
        }

      return SUCCESS;
    }
  else {

      backoffValue = CC2420RadioM$MacBackoff$congestionBackoff(CC2420RadioM$txbufptr) * 10;
      if (!CC2420RadioM$setBackoffTimer(backoffValue)) {
#line 363
        CC2420RadioM$fSendAborted();
        }
    }
  return SUCCESS;
}

# 102 "/Users/wbennett/opt/MoteWorks/tos/platform/mica2/HPLUART0M.nc"
  __attribute((signal)) void __vector_18(void )
#line 102
{
  if (* (volatile uint8_t *)(0x0B + 0x20) & (1 << 7)) {
    HPLUART0M$UART$get(* (volatile uint8_t *)(0x0C + 0x20));
    }
}

# 141 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
static uint8_t FramerM$fRemapRxPos(uint8_t InPos)
#line 141
{


  if (InPos < 4) {
    return InPos + (unsigned short )& ((struct TOS_Msg *)0)->addr;
    }
  else {
#line 146
    if (InPos == 4) {
      return (unsigned short )& ((struct TOS_Msg *)0)->length;
      }
    else {
#line 149
      return InPos + (unsigned short )& ((struct TOS_Msg *)0)->addr - 1;
      }
    }
}



static result_t FramerM$StartTx(void )
#line 156
{
  result_t Result = SUCCESS;
  bool fInitiate = 0;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 160
    {
      if (FramerM$gTxState == FramerM$TXSTATE_IDLE) {
          if (FramerM$gFlags & FramerM$FLAGS_TOKENPEND) {

              FramerM$gpTxBuf = (uint8_t *)&FramerM$gTxTokenBuf;




              FramerM$gTxProto = FramerM$PROTO_ACK;
              FramerM$gTxLength = sizeof FramerM$gTxTokenBuf;
              fInitiate = 1;
              FramerM$gTxState = FramerM$TXSTATE_PROTO;
            }
          else {
#line 174
            if (FramerM$gFlags & FramerM$FLAGS_DATAPEND) {
                FramerM$gpTxBuf = (uint8_t *)FramerM$gpTxMsg;
                FramerM$gTxProto = FramerM$PROTO_PACKET_NOACK;


                FramerM$gTxLength = FramerM$gpTxMsg->length + TOS_HEADER_SIZE + 2 + 3;



                fInitiate = 1;
                FramerM$gTxState = FramerM$TXSTATE_PROTO;
              }
            else {
#line 186
              if (FramerM$gFlags & FramerM$FLAGS_UNKNOWN) {
                  FramerM$gpTxBuf = (uint8_t *)&FramerM$gTxUnknownBuf;
                  FramerM$gTxProto = FramerM$PROTO_UNKNOWN;
                  FramerM$gTxLength = sizeof FramerM$gTxUnknownBuf;
                  fInitiate = 1;
                  FramerM$gTxState = FramerM$TXSTATE_PROTO;
                }
              }
            }
        }
    }
#line 196
    __nesc_atomic_end(__nesc_atomic); }
#line 196
  if (fInitiate) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 197
        {

          FramerM$gTxRunningCRC = 0;
          FramerM$gTxByteCnt = (unsigned short )& ((struct TOS_Msg *)0)->addr;
        }
#line 201
        __nesc_atomic_end(__nesc_atomic); }




      Result = FramerM$ByteComm$txByte(FramerM$HDLC_FLAG_BYTE);
      if (Result != SUCCESS) {
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 208
            FramerM$gTxState = FramerM$TXSTATE_ERROR;
#line 208
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(FramerM$PacketSent);
        }
    }

  return Result;
}

# 90 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
static result_t UARTM$ByteComm$txByte(uint8_t data)
#line 90
{
  bool oldState;

  {
  }
#line 93
  ;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 95
    {
      oldState = UARTM$state;
      UARTM$state = 1;
    }
#line 98
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState) {
    return FAIL;
    }
  UARTM$HPLUART$put(data);

  return SUCCESS;
}

# 263 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
static void FramerM$PacketSent(void )
#line 263
{
  result_t TxResult = SUCCESS;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 266
    {
      if (FramerM$gTxState == FramerM$TXSTATE_ERROR) {
          TxResult = FAIL;
          FramerM$gTxState = FramerM$TXSTATE_IDLE;
        }
    }
#line 271
    __nesc_atomic_end(__nesc_atomic); }
  if (FramerM$gTxProto == FramerM$PROTO_ACK) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 273
        FramerM$gFlags ^= FramerM$FLAGS_TOKENPEND;
#line 273
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 276
        FramerM$gFlags ^= FramerM$FLAGS_DATAPEND;
#line 276
        __nesc_atomic_end(__nesc_atomic); }
      FramerM$BareSendMsg$sendDone((TOS_MsgPtr )FramerM$gpTxMsg, TxResult);
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 278
        FramerM$gpTxMsg = (void *)0;
#line 278
        __nesc_atomic_end(__nesc_atomic); }
    }


  FramerM$StartTx();
}

# 112 "/Users/wbennett/opt/MoteWorks/tos/platform/mica2/HPLUART0M.nc"
  __attribute((interrupt)) void __vector_20(void )
#line 112
{
  HPLUART0M$UART$putDone();
}

# 497 "/Users/wbennett/opt/MoteWorks/tos/platform/micaz/FramerM.nc"
static result_t FramerM$TxArbitraryByte(uint8_t inByte)
#line 497
{
  if (inByte == FramerM$HDLC_FLAG_BYTE || inByte == FramerM$HDLC_CTLESC_BYTE) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 499
        {
          FramerM$gPrevTxState = FramerM$gTxState;
          FramerM$gTxState = FramerM$TXSTATE_ESC;
          FramerM$gTxEscByte = inByte;
        }
#line 503
        __nesc_atomic_end(__nesc_atomic); }
      inByte = FramerM$HDLC_CTLESC_BYTE;
    }

  return FramerM$ByteComm$txByte(inByte);
}


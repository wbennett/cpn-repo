#!/usr/bin/ruby
require 'rubygems'
require 'date'
require 'time'
require 'serialport'

def check_and_print(char) 
	if char == 9 || char == 10 || char == 13
		print char.chr;
	elsif char < 0x20 or char > 0x7E
		print " 0x#{char.to_s(16)} ";
	else
		print char.chr;
	end
end

if ARGV.size < 3
	STDERR.print <<EOF
	Usage: ruby #{$0} port bps file.to.output.data
EOF
	exit(1)
end
puts ARGV[0], ARGV[1], ARGV[2]
sp = SerialPort.new("#{ARGV[0]}", ARGV[1].to_i, 8,1,SerialPort::NONE)
queue = Queue.new
#consumer = Thread.new {
#str = ""
#loop do
#	str = ""
#	val = STDIN.gets()
#	if val == nil
#	break
#	else
#	str = val.gsub(/\r/,"").gsub(/\n/,"");
#	sp.puts("#{str}\r")
#	puts("wrote:#{str}");
#	end
#end
#}

file = File.open("#{ARGV[2]}",'w')

producer = Thread.new {
str = ""
pms2x = 0;
pms2y = 0;
pms2z = 0;
ms2x = 0.0;
ms2y = 0.0;
ms2z = 0.0;
pt = nil;
loop  do
	val = sp.gets()
	if val == nil
		break
	else
		#compute the jerk
		#the readings are m/s^2 jerk is m/s^3
		
		#check_and_print(val);
		readings = val.split(',');
		t = Time.now.to_f;
		ms2x = readings[6].to_i;
		ms2y = readings[7].to_i;
		ms2z = readings[8].to_i;
		if pt == nil 
			pt = t
			pms2x = ms2x;
			pms2y = ms2y;
			pms2z = ms2z;
		else
			ms3x = (ms2x-pms2x)/(t-pt);
			ms3y = (ms2y-pms2y)/(t-pt);
			ms3z = (ms2z-pms2z)/(t-pt);
			pms2x = ms2x;
			pms2y = ms2y;
			pms2z = ms2z;
			pt = t;
			#puts "#{ms3x}\t#{ms3y}\t#{ms3z}";
			if ms3z > 500000.0 or ms3z < -500000.0
				puts "FLAP! #{ms3z}"
			end
			if ms3x > 500000.0 or ms3x < -500000.0
				puts "FORAGE! #{ms3z}"
			end
			if ms3y > 500000.0 or ms3y < -500000.0
				puts "DANCE! #{ms3z}"
			end
			file.write "#{ms3x}\t#{ms3y}\t#{ms3z}";
		end
		#puts "#{Time.now.to_f}\t#{val}"
	end
end
}
producer.join
puts "closing socket.."
sp.close

/*
 * Base.nc
 *
 * Sends an 8-bit global struct orientation
 * <header>Data<header>
 *
 **/
#include "Tracker/compass_msg.h" 
configuration Base {	
}

implementation {
// COMPONENTS
  components Main, BaseM, LedsC, GenericComm;

// WIRING 
	
	// CounterRfmM
	Main.StdControl -> BaseM.StdControl;
	
	// LedsC
  	BaseM.Leds -> LedsC.Leds;
  	
  	// GenericComm
  	Main.StdControl -> GenericComm.Control;  	  	
  	BaseM.ReceiveMsg -> GenericComm.ReceiveMsg[AM_COMPASS_MSG];
  	 
}

/*
 * BaseM.nc
 *
 * Sends an 8-bit global struct orientation
 * <header>Data<header>
 *
 **/
#include "Tracker/compass_msg.h"
#include "debug_constants.h"
module BaseM {
  provides interface StdControl;
  uses {
    interface Leds;
    interface ReceiveMsg;
  }
}
implementation {

	
	command result_t StdControl.init() {
    call Leds.init();     
		call Leds.redToggle();
    return SUCCESS;
  }


  command result_t StdControl.start() {
    return SUCCESS;
  }

  command result_t StdControl.stop() {
    return SUCCESS;
  }
  event TOS_MsgPtr ReceiveMsg.receive(TOS_MsgPtr m) {
    compass_msg_t *message = (compass_msg_t*)m->data;
		uint8_t rssi,lqi;

		rssi = m->strength & 0x1F;
		if(rssi > 28) {
		rssi = 28;
		}
		lqi = m->lqi;
		call Leds.yellowToggle();
		dprintf("%u,%u,%u,%u,%i,%i,%i,%i,%i\n",
			rssi,
			lqi,
			message->seqnum,
			message->heading,
			message->pitch,
			message->roll,
			message->ax,
			message->ay,
			message->az);
    return m;
  }
   
}


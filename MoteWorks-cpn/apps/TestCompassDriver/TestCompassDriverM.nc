/*
 *
 *
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
#include "Tracker/compass_msg.h"
module TestCompassDriverM 
{
  provides 
	{
		interface StdControl;
  }
  uses 
	{
		#ifdef I2CDBG
		I2CDBG_CONFIG();
		#elif USERADIODBG
		RADIODBG_CONFIG();
		#endif
    interface SplitControl as CompassSplitControl;
		interface Compass as Compass;
		interface StdControl as TimerControl;
		interface Timer;
		interface Leds;
		#ifdef SEND_OVER_RADIO
		interface StdControl as RadioControl;
		interface SendMsg;
		#endif
  }
}
implementation 
{
//	#define I2CDBG	
	enum {
		S_INIT,
		S_SAMPLE,
		S_SEND_DATA
	};

	uint8_t state;
	bool radio_busy;
	
	compass_msg_t sMessage;
	TOS_Msg tempMsg;

	void transition();
	task void transition_task();
	
	command result_t StdControl.init()	{
		call TimerControl.init();
		call Leds.init();
		#ifdef SEND_OVER_RADIO
		call RadioControl.init();
		#endif
		sMessage.seqnum = 0;
		//SODbg(1,"init\r");
		//SODbg(1,"FAIL:%d,SUCCESS:%d\n",FAIL,SUCCESS);
		return SUCCESS;
	}
	
	command result_t StdControl.start() {
		call TimerControl.start();
		call Leds.redToggle();
		call Leds.greenToggle();
		#ifdef SEND_OVER_RADIO
		call RadioControl.start();
		call CompassSplitControl.init();
		#endif
		return SUCCESS;
	}
	
	command result_t StdControl.stop()	{
		call TimerControl.stop();
		call Timer.stop();
		call CompassSplitControl.stop();
		#ifdef SEND_OVER_RADIO
		call RadioControl.stop();
		#endif
		return SUCCESS;
	}
	
	event result_t CompassSplitControl.initDone() {
		call CompassSplitControl.start();
		return SUCCESS;
	}
	
	event result_t CompassSplitControl.startDone() {
		state = S_INIT;
		call Timer.start(TIMER_ONE_SHOT,10);
		return SUCCESS;
	}
	
	event result_t CompassSplitControl.stopDone() {
		return SUCCESS;
	}
	
	event void Compass.getAccelsDone(result_t result, uint16_t * data) {
		sMessage.ax = *data;
		sMessage.ay = *(data+1);
		sMessage.az = *(data+2);
		post transition_task();
	}
	#ifdef SEND_OVER_RADIO
	task void sendResults() {
		if(radio_busy == FALSE) {
			radio_busy = TRUE;
			//pack the message and send it 
			memcpy(tempMsg.data,&sMessage,sizeof(compass_msg_t));
			if(call SendMsg.send(AM_COMPASS_BASE_ADDR,
					sizeof(compass_msg_t),
					&tempMsg) == FAIL){
				call Leds.yellowToggle();
				radio_busy = FALSE;
				post sendResults();
			}
			sMessage.seqnum += 1;
		}
	}
	#endif
	task void printResults() {
		#ifndef SEND_OVER_RADIO
		call Leds.redToggle();
		#endif
	}
	event void Compass.getHeadingsDone(result_t result, uint16_t * data) {
		sMessage.heading = data[0];
		sMessage.pitch = data[1];
		sMessage.roll = data[2];
		call Leds.yellowToggle();
		call Compass.getAccels();
		call Timer.start(TIMER_ONE_SHOT,2000);
	}
	
	event void Compass.getTiltsDone(result_t result, uint16_t * data) {
	}

	event void Compass.calibrateDone(result_t result) {
	}
	
	event result_t Timer.fired() {
		post transition_task();
		return SUCCESS;
	}

	task void transition_task() {
		transition();
	}
	
	void transition() {
		uint8_t lstate = state;
		switch(lstate) {
			case S_INIT:
				state = S_SAMPLE;
				call Compass.getHeadings();
				break;
			case S_SAMPLE:
				state = S_SEND_DATA;
				post sendResults();
				break;
			case S_SEND_DATA:
				state = S_INIT;
				post transition_task();
				break;
			default:
				break;
		}
	}

	event result_t SendMsg.sendDone(TOS_MsgPtr msg, result_t success) {
		radio_busy = FALSE;
		if(success == SUCCESS) {
			call Leds.redToggle();
		}
		//post transition_task();
		call Timer.start(TIMER_ONE_SHOT,10);
		return SUCCESS;
	}
	
}


/*
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
#include "Tracker/compass_msg.h"
configuration TestCompassDriverC
{
}
implementation
{
	components Main, TestCompassDriverM, CompassC, TimerC, LedsC;
	#ifdef SEND_OVER_RADIO
	components GenericComm as Comm;
	TestCompassDriverM.RadioControl -> Comm;
	TestCompassDriverM.SendMsg -> Comm.SendMsg[AM_COMPASS_MSG];
	#endif
	#ifdef I2CDBG
	I2CDBG_WIRING(Main,TestCompassDriverM);
	#elif USERADIODBG
	RADIODBG_WIRING(TestCompassDriverM);
	#endif
	
	Main.StdControl -> TestCompassDriverM.StdControl;
	
	TestCompassDriverM.Compass -> CompassC.Compass;
	TestCompassDriverM.CompassSplitControl -> CompassC.SplitControl;
	
	TestCompassDriverM.TimerControl -> TimerC.StdControl;
	TestCompassDriverM.Timer -> TimerC.Timer[unique("Timer")];
	TestCompassDriverM.Leds -> LedsC.Leds;
	
}


/*
 *
 *
 *
 */
#include <stdio.h>
#include "avrtime.h"
#include "debug_constants.h"
#include "Tracker/gps.h"
#include "Tracker/config.h"
module TestGpsDriverM 
{
  provides 
	{
		interface StdControl;
  }
  uses 
	{

    interface SplitControl as GpsSplitControl;
		interface ReceiveMsg as GpsGGARecv;
		interface ReceiveMsg as GpsRMCRecv;
		interface StdControl as TimerControl;
		interface Timer;
		interface Leds;
		#ifdef I2CDBG
		I2CDBG_CONFIG();
		#endif

		#ifdef USERADIODBG
		RADIODBG_CONFIG();
		#endif
  }
}
implementation 
{
	GGAMsg nmeaMsgGga;
	RMCMsg nmeaMsgRmc;
	
	command result_t StdControl.init()	{
		call TimerControl.init();
		call Leds.init();
		call GpsSplitControl.init();
		return SUCCESS;
	}
	
	command result_t StdControl.start() {
		call TimerControl.start();
		//call Leds.redToggle();
		//call Leds.greenToggle();
		return SUCCESS;
	}
	
	command result_t StdControl.stop()	{
		call TimerControl.stop();
		call Timer.stop();
		call GpsSplitControl.stop();
		return SUCCESS;
	}
	
	event result_t GpsSplitControl.initDone() {
		call GpsSplitControl.start();
		return SUCCESS;
	}
	
	event result_t GpsSplitControl.startDone() {
		dprintf("starting...\n",NULL);
		return SUCCESS;
	}
	
	event result_t GpsSplitControl.stopDone() {
		return SUCCESS;
	}
	
	event TOS_MsgPtr GpsGGARecv.receive(TOS_MsgPtr msgPtr)
	{
		call Leds.redToggle();
		memcpy(&nmeaMsgGga,(GGAMsg*)msgPtr,sizeof(nmeaMsgGga));
		dprintf("Valid:%i,Hrs:%i,Mins:%i,Secs:%i,LatDeg:%i,LatDecMin:%li,LonDeg:%i,LonDecMin:%li,Alt:%i\n",
					nmeaMsgGga.valid,nmeaMsgGga.TimeHrs,nmeaMsgGga.TimeMin,nmeaMsgGga.TimeSec,nmeaMsgGga.Lat_deg,
					nmeaMsgGga.Lat_dec_min, nmeaMsgGga.Lon_deg,nmeaMsgGga.Lon_dec_min,nmeaMsgGga.Alt);
		return msgPtr;
	}
	
	event TOS_MsgPtr GpsRMCRecv.receive(TOS_MsgPtr msgPtr)
	{
		call Leds.greenToggle();
		memcpy(&nmeaMsgRmc,(RMCMsg*)msgPtr,sizeof(nmeaMsgRmc));
		dprintf("Valid:%i,Speed:%i,Tcourse:%i,FixDate:%li\n",nmeaMsgRmc.valid,nmeaMsgRmc.Speed,
					nmeaMsgRmc.Tcourse,nmeaMsgRmc.FixDate);
		return msgPtr;
	}
	
	event result_t Timer.fired() {
		return SUCCESS;
	}
	
}


/*
 *
 *
 *
 *
 */
#include "debug_constants.h"
configuration TestGpsDriverC
{
}
implementation
{
	components Main, TestGpsDriverM, GpsDriver, TimerC, LedsC;
	#ifdef I2CDBG
	I2CDBG_WIRING(Main, TestGpsDriverM);
	#endif
	#ifdef USERADIODBG
	RADIODBG_WIRING(TestGpsDriverM);
	#endif
	
	
	Main.StdControl -> TestGpsDriverM.StdControl;
	
	TestGpsDriverM.GpsGGARecv -> GpsDriver.GpsGGARecv;
	TestGpsDriverM.GpsRMCRecv -> GpsDriver.GpsRMCRecv;
	TestGpsDriverM.GpsSplitControl -> GpsDriver.GpsSplitControl;
	
	TestGpsDriverM.TimerControl -> TimerC.StdControl;
	TestGpsDriverM.Timer -> TimerC.Timer[unique("Timer")];
	TestGpsDriverM.Leds -> LedsC.Leds;
	
}

/*
 *
 *
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
module TestCompassDriverM 
{
  provides 
	{
		interface StdControl;
  }
  uses 
	{
		#ifdef I2CDBG
		I2CDBG_CONFIG();
		#elif USERADIODBG
		RADIODBG_CONFIG();
		#endif
    interface SplitControl as CompassSplitControl;
		interface Compass as Compass;
		interface StdControl as TimerControl;
		interface Timer;
		interface Leds;
  }
}
implementation 
{
//	#define I2CDBG	
	enum {
		READY,
		BUSY,
	};
	uint8_t state;
	
	uint16_t readings[3];
	
	void transition();
	
	command result_t StdControl.init()	{
		call TimerControl.init();
		call Leds.init();
		return SUCCESS;
	}
	
	command result_t StdControl.start() {
		call TimerControl.start();
		call CompassSplitControl.init();
		return SUCCESS;
	}
	
	command result_t StdControl.stop()	{
		call TimerControl.stop();
		call Timer.stop();
		call CompassSplitControl.stop();
		return SUCCESS;
	}
	
	event result_t CompassSplitControl.initDone() {
		call CompassSplitControl.start();
		return SUCCESS;
	}
	
	event result_t CompassSplitControl.startDone() {
		state = READY;
		call Compass.calibrate();
		call Leds.redOn();
		return SUCCESS;
	}
	
	event result_t CompassSplitControl.stopDone() {
		return SUCCESS;
	}
	
	event void Compass.getAccelsDone(result_t result, uint16_t * data) {
	}

	event void Compass.getHeadingsDone(result_t result, uint16_t * data) {
	}
	
	event void Compass.getTiltsDone(result_t result, uint16_t * data) {
	}

	event void Compass.calibrateDone(result_t result) {
		if(result == SUCCESS) {
			call Leds.redOff();
			call Leds.greenOn();
		}
	}
	
	event result_t Timer.fired() {
		return SUCCESS;
	}
}


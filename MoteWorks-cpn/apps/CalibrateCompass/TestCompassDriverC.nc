/*
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
configuration TestCompassDriverC
{
}
implementation
{
	components Main, TestCompassDriverM, CompassC, TimerC, LedsC;
	#ifdef I2CDBG
	I2CDBG_WIRING(Main,TestCompassDriverM);
	#elif USERADIODBG
	RADIODBG_WIRING(TestCompassDriverM);
	#endif
	
	Main.StdControl -> TestCompassDriverM.StdControl;
	
	TestCompassDriverM.Compass -> CompassC.Compass;
	TestCompassDriverM.CompassSplitControl -> CompassC.SplitControl;
	
	TestCompassDriverM.TimerControl -> TimerC.StdControl;
	TestCompassDriverM.Timer -> TimerC.Timer[unique("Timer")];
	TestCompassDriverM.Leds -> LedsC.Leds;
	
}


#include "crane.h";

configuration craneLeg {
}
implementation {
  components Main, craneLegM, PredictionM,TimerC, LedsC, GenericComm as Comm;
	components CompassC;

  
  Main.StdControl -> craneLegM.StdControl;
  Main.StdControl -> TimerC.StdControl;
	
	//compass
	craneLegM.Compass -> CompassC.Compass;
	craneLegM.CompassSplitControl -> CompassC.SplitControl;;
  
	craneLegM.Timer -> TimerC.Timer[unique("Timer")];
  craneLegM.Leds -> LedsC.Leds;
	
	craneLegM.RadioControl -> Comm.Control;  	
  craneLegM.SendMsg -> Comm.SendMsg[AM_CRANE];

	//Power Control
	components HPLPowerManagementM as PM;
	craneLegM.PowerManagement -> PM;
	craneLegM.Enable -> PM.Enable;
	craneLegM.Disable -> PM.Disable;
}

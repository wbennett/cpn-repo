/*
 *
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
#include "Tracker/gsm.h";
configuration TestGSMDriverC
{
}
implementation
{
	components Main, TestGSMDriverM, GSMDriverC, TimerC, LedsC;
	#ifdef I2CDBG
	I2CDBG_WIRING(Main,TestGSMDriverM);
	#endif
	
	Main.StdControl -> TestGSMDriverM.StdControl;
	
	TestGSMDriverM.GSM_Modem -> GSMDriverC.GSM_Modem;
	TestGSMDriverM.GsmControl -> GSMDriverC.SplitControlStatus;
	
	TestGSMDriverM.TimerControl -> TimerC.StdControl;
	TestGSMDriverM.Timer -> TimerC.Timer[unique("Timer")];
	TestGSMDriverM.Leds -> LedsC.Leds;
	
}


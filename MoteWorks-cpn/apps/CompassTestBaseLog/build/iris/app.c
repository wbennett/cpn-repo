#define nx_struct struct
#define nx_union union
#define dbg(mode, format, ...) ((void)0)
#define dbg_clear(mode, format, ...) ((void)0)
#define dbg_active(mode) 0
# 152 "/opt/local/lib/gcc/avr/4.1.2/include/stddef.h" 3
typedef int ptrdiff_t;
#line 214
typedef unsigned int size_t;
#line 326
typedef int wchar_t;
# 8 "/opt/local/lib/ncc/deputy_nodeputy.h"
struct __nesc_attr_nonnull {
#line 8
  int dummy;
}  ;
#line 9
struct __nesc_attr_bnd {
#line 9
  void *lo, *hi;
}  ;
#line 10
struct __nesc_attr_bnd_nok {
#line 10
  void *lo, *hi;
}  ;
#line 11
struct __nesc_attr_count {
#line 11
  int n;
}  ;
#line 12
struct __nesc_attr_count_nok {
#line 12
  int n;
}  ;
#line 13
struct __nesc_attr_one {
#line 13
  int dummy;
}  ;
#line 14
struct __nesc_attr_one_nok {
#line 14
  int dummy;
}  ;
#line 15
struct __nesc_attr_dmemset {
#line 15
  int a1, a2, a3;
}  ;
#line 16
struct __nesc_attr_dmemcpy {
#line 16
  int a1, a2, a3;
}  ;
#line 17
struct __nesc_attr_nts {
#line 17
  int dummy;
}  ;
# 121 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdint.h" 3
typedef int int8_t __attribute((__mode__(__QI__))) ;
typedef unsigned int uint8_t __attribute((__mode__(__QI__))) ;
typedef int int16_t __attribute((__mode__(__HI__))) ;
typedef unsigned int uint16_t __attribute((__mode__(__HI__))) ;
typedef int int32_t __attribute((__mode__(__SI__))) ;
typedef unsigned int uint32_t __attribute((__mode__(__SI__))) ;

typedef int int64_t __attribute((__mode__(__DI__))) ;
typedef unsigned int uint64_t __attribute((__mode__(__DI__))) ;
#line 142
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
#line 159
typedef int8_t int_least8_t;




typedef uint8_t uint_least8_t;




typedef int16_t int_least16_t;




typedef uint16_t uint_least16_t;




typedef int32_t int_least32_t;




typedef uint32_t uint_least32_t;







typedef int64_t int_least64_t;






typedef uint64_t uint_least64_t;
#line 213
typedef int8_t int_fast8_t;




typedef uint8_t uint_fast8_t;




typedef int16_t int_fast16_t;




typedef uint16_t uint_fast16_t;




typedef int32_t int_fast32_t;




typedef uint32_t uint_fast32_t;







typedef int64_t int_fast64_t;






typedef uint64_t uint_fast64_t;
#line 273
typedef int64_t intmax_t;




typedef uint64_t uintmax_t;
# 77 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/inttypes.h" 3
typedef int32_t int_farptr_t;



typedef uint32_t uint_farptr_t;
# 431 "/opt/local/lib/ncc/nesc_nx.h"
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nx_int8_t;typedef int8_t __nesc_nxbase_nx_int8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nx_int16_t;typedef int16_t __nesc_nxbase_nx_int16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nx_int32_t;typedef int32_t __nesc_nxbase_nx_int32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nx_int64_t;typedef int64_t __nesc_nxbase_nx_int64_t  ;
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nx_uint8_t;typedef uint8_t __nesc_nxbase_nx_uint8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nx_uint16_t;typedef uint16_t __nesc_nxbase_nx_uint16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nx_uint32_t;typedef uint32_t __nesc_nxbase_nx_uint32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nx_uint64_t;typedef uint64_t __nesc_nxbase_nx_uint64_t  ;


typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nxle_int8_t;typedef int8_t __nesc_nxbase_nxle_int8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nxle_int16_t;typedef int16_t __nesc_nxbase_nxle_int16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nxle_int32_t;typedef int32_t __nesc_nxbase_nxle_int32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nxle_int64_t;typedef int64_t __nesc_nxbase_nxle_int64_t  ;
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nxle_uint8_t;typedef uint8_t __nesc_nxbase_nxle_uint8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nxle_uint16_t;typedef uint16_t __nesc_nxbase_nxle_uint16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nxle_uint32_t;typedef uint32_t __nesc_nxbase_nxle_uint32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nxle_uint64_t;typedef uint64_t __nesc_nxbase_nxle_uint64_t  ;
# 71 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdlib.h" 3
#line 68
typedef struct __nesc_unnamed4242 {
  int quot;
  int rem;
} div_t;





#line 74
typedef struct __nesc_unnamed4243 {
  long quot;
  long rem;
} ldiv_t;


typedef int (*__compar_fn_t)(const void *arg_0x100771370, const void *arg_0x100771648);
# 71 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
typedef unsigned char bool;






enum __nesc_unnamed4244 {
  FALSE = 0, 
  TRUE = 1
};


uint16_t TOS_LOCAL_ADDRESS = 1;





uint8_t TOS_ROUTE_PROTOCOL = 0x90;
#line 104
uint8_t TOS_BASE_STATION = 0;





const uint8_t TOS_DATA_LENGTH = 36;
#line 132
uint8_t TOS_PLATFORM = 7;










enum __nesc_unnamed4245 {
  FAIL = 0, 
  SUCCESS = 1
};


static inline uint8_t rcombine(uint8_t r1, uint8_t r2);
typedef uint8_t result_t  ;







static inline result_t rcombine(result_t r1, result_t r2);
#line 171
static inline result_t rcombine4(result_t r1, result_t r2, result_t r3, 
result_t r4);





enum __nesc_unnamed4246 {
  NULL = 0x0
};
# 210 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/avr/pgmspace.h" 3
typedef void prog_void __attribute((__progmem__)) ;
typedef char prog_char __attribute((__progmem__)) ;
typedef unsigned char prog_uchar __attribute((__progmem__)) ;

typedef int8_t prog_int8_t __attribute((__progmem__)) ;
typedef uint8_t prog_uint8_t __attribute((__progmem__)) ;
typedef int16_t prog_int16_t __attribute((__progmem__)) ;
typedef uint16_t prog_uint16_t __attribute((__progmem__)) ;
typedef int32_t prog_int32_t __attribute((__progmem__)) ;
typedef uint32_t prog_uint32_t __attribute((__progmem__)) ;

typedef int64_t prog_int64_t __attribute((__progmem__)) ;
typedef uint64_t prog_uint64_t __attribute((__progmem__)) ;
# 118 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
enum __nesc_unnamed4247 {
  TOSH_period16 = 0x00, 
  TOSH_period32 = 0x01, 
  TOSH_period64 = 0x02, 
  TOSH_period128 = 0x03, 
  TOSH_period256 = 0x04, 
  TOSH_period512 = 0x05, 
  TOSH_period1024 = 0x06, 
  TOSH_period2048 = 0x07
};

static inline void TOSH_wait();







typedef uint8_t __nesc_atomic_t;

__nesc_atomic_t __nesc_atomic_start(void );
void __nesc_atomic_end(__nesc_atomic_t oldSreg);



__inline __nesc_atomic_t __nesc_atomic_start(void )  ;






__inline void __nesc_atomic_end(__nesc_atomic_t oldSreg)  ;






static __inline void __nesc_atomic_sleep();







static __inline void __nesc_enable_interrupt();



static __inline void __nesc_disable_interrupt();
# 67 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Const.h"
uint8_t TOS_RF230_TXPOWER __attribute((section(".data")))  = 0;
volatile uint8_t TOS_RF230_CHANNEL = 11;
# 39 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_uwait(int u_sec);
#line 55
static __inline void TOSH_uwait2(int u_sec);







static __inline void TOSH_SET_RED_LED_PIN();
#line 63
static __inline void TOSH_CLR_RED_LED_PIN();
#line 63
static __inline void TOSH_MAKE_RED_LED_OUTPUT();
static __inline void TOSH_SET_GREEN_LED_PIN();
#line 64
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT();
static __inline void TOSH_SET_YELLOW_LED_PIN();
#line 65
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT();

static __inline void TOSH_CLR_SERIAL_ID_PIN();
#line 67
static __inline void TOSH_MAKE_SERIAL_ID_INPUT();





static __inline void TOSH_SET_RF230_SLP_TR_PIN();
#line 73
static __inline void TOSH_CLR_RF230_SLP_TR_PIN();
#line 73
static __inline void TOSH_MAKE_RF230_SLP_TR_OUTPUT();
static __inline void TOSH_SET_RF230_RSTN_PIN();
#line 74
static __inline void TOSH_CLR_RF230_RSTN_PIN();
#line 74
static __inline void TOSH_MAKE_RF230_RSTN_OUTPUT();
static __inline void TOSH_CLR_RF230_IRQ_PIN();
#line 75
static __inline void TOSH_MAKE_RF230_IRQ_INPUT();
static __inline void TOSH_CLR_RF230_CLKM_PIN();
#line 76
static __inline void TOSH_MAKE_RF230_CLKM_INPUT();


static __inline void TOSH_SET_FLASH_SELECT_PIN();
#line 79
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT();
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT();
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT();










static __inline void TOSH_SET_SPI_CS_PIN();
#line 92
static __inline void TOSH_CLR_SPI_CS_PIN();
#line 92
static __inline void TOSH_MAKE_SPI_CS_OUTPUT();
static __inline void TOSH_MAKE_SPI_SCK_OUTPUT();
static __inline void TOSH_MAKE_MOSI_OUTPUT();
static __inline void TOSH_MAKE_MISO_INPUT();


static __inline void TOSH_MAKE_PW0_OUTPUT();
static __inline void TOSH_MAKE_PW1_OUTPUT();
static __inline void TOSH_MAKE_PW2_OUTPUT();
static __inline void TOSH_MAKE_PW3_OUTPUT();
static __inline void TOSH_MAKE_PW4_OUTPUT();
static __inline void TOSH_MAKE_PW5_OUTPUT();
static __inline void TOSH_MAKE_PW6_OUTPUT();
static __inline void TOSH_MAKE_PW7_OUTPUT();
#line 134
static inline void TOSH_SET_PIN_DIRECTIONS(void );
#line 187
enum __nesc_unnamed4248 {
  TOSH_ADC_PORTMAPSIZE = 12
};

enum __nesc_unnamed4249 {


  TOSH_ACTUAL_VOLTAGE_PORT = 30, 
  TOSH_ACTUAL_BANDGAP_PORT = 30, 
  TOSH_ACTUAL_GND_PORT = 31
};

enum __nesc_unnamed4250 {


  TOS_ADC_VOLTAGE_PORT = 7, 
  TOS_ADC_BANDGAP_PORT = 10, 
  TOS_ADC_GND_PORT = 11
};




uint32_t TOS_UART0_BAUDRATE = 57600u;
# 33 "/Users/wbennett/opt/MoteWorks/tos/types/dbg_modes.h"
typedef long long TOS_dbg_mode;



enum __nesc_unnamed4251 {
  DBG_ALL = ~0ULL, 


  DBG_BOOT = 1ULL << 0, 
  DBG_CLOCK = 1ULL << 1, 
  DBG_TASK = 1ULL << 2, 
  DBG_SCHED = 1ULL << 3, 
  DBG_SENSOR = 1ULL << 4, 
  DBG_LED = 1ULL << 5, 
  DBG_CRYPTO = 1ULL << 6, 


  DBG_ROUTE = 1ULL << 7, 
  DBG_AM = 1ULL << 8, 
  DBG_CRC = 1ULL << 9, 
  DBG_PACKET = 1ULL << 10, 
  DBG_ENCODE = 1ULL << 11, 
  DBG_RADIO = 1ULL << 12, 


  DBG_LOG = 1ULL << 13, 
  DBG_ADC = 1ULL << 14, 
  DBG_I2C = 1ULL << 15, 
  DBG_UART = 1ULL << 16, 
  DBG_PROG = 1ULL << 17, 
  DBG_SOUNDER = 1ULL << 18, 
  DBG_TIME = 1ULL << 19, 
  DBG_POWER = 1ULL << 20, 



  DBG_SIM = 1ULL << 21, 
  DBG_QUEUE = 1ULL << 22, 
  DBG_SIMRADIO = 1ULL << 23, 
  DBG_HARD = 1ULL << 24, 
  DBG_MEM = 1ULL << 25, 



  DBG_USR1 = 1ULL << 27, 
  DBG_USR2 = 1ULL << 28, 
  DBG_USR3 = 1ULL << 29, 
  DBG_TEMP = 1ULL << 30, 

  DBG_ERROR = 1ULL << 31, 
  DBG_NONE = 0, 

  DBG_DEFAULT = DBG_ALL
};
# 41 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
#line 39
typedef struct __nesc_unnamed4252 {
  void (*tp)();
} TOSH_sched_entry_T;

enum __nesc_unnamed4253 {






  TOSH_MAX_TASKS = 32, 

  TOSH_TASK_BITMASK = TOSH_MAX_TASKS - 1
};

volatile TOSH_sched_entry_T TOSH_queue[TOSH_MAX_TASKS];
uint8_t TOSH_sched_full;
volatile uint8_t TOSH_sched_free;

static inline void TOSH_sched_init(void );








bool TOS_post(void (*tp)());
#line 82
bool TOS_post(void (*tp)())  ;
#line 116
static inline bool TOSH_run_next_task();
#line 139
static inline void TOSH_run_task();
# 187 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline void *nmemcpy(void *to, const void *from, size_t n);
# 14 "/Users/wbennett/opt/MoteWorks/tos/system/Ident.h"
enum __nesc_unnamed4254 {

  IDENT_MAX_PROGRAM_NAME_LENGTH = 17
};






#line 19
typedef struct __nesc_unnamed4255 {

  uint32_t unix_time;
  uint32_t user_hash;
  char program_name[IDENT_MAX_PROGRAM_NAME_LENGTH];
} Ident_t;
# 14 "crane.h"
#line 7
typedef struct Compass_Readings {
  uint16_t heading;
  int16_t pitch;
  int16_t roll;
  uint16_t accelx;
  uint16_t accely;
  uint16_t accelz;
} __attribute((packed))  Compass_Readings;
#line 26
#line 16
typedef struct OrientationMessage {
  uint32_t seqnum;
  uint16_t heading;
  int16_t pitch;
  int16_t roll;
  uint16_t accelx;
  uint16_t accely;
  uint16_t accelz;
  uint8_t color;
  uint16_t source;
} OrientationMessage;

enum __nesc_unnamed4256 {
  AM_CRANE = 55
};

enum __nesc_unnamed4257 {
  NORTH = 0, 
  EAST = 1, 
  SOUTH = 2, 
  WEST = 3, 
  DERROR = 4
};

enum __nesc_unnamed4258 {
  UPDATE_PRD = 500
};
# 43 "/opt/local/lib/gcc/avr/4.1.2/include/stdarg.h" 3
typedef __builtin_va_list __gnuc_va_list;
#line 105
typedef __gnuc_va_list va_list;
# 242 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdio.h" 3
struct __file {
  char *buf;
  unsigned char unget;
  uint8_t flags;
#line 261
  int size;
  int len;
  int (*put)(char arg_0x10128f0c8, struct __file *arg_0x10128f408);
  int (*get)(struct __file *arg_0x10128faf8);
  void *udata;
};
#line 405
struct __file;
#line 417
struct __file;
#line 656
extern int sprintf(char *__s, const char *__fmt, ...);
# 42 "/Users/wbennett/opt/MoteWorks/lib/SOdebug0.h"
uint8_t debugStarted;
static const char hex[16] = "0123456789ABCDEF";



inline static void init_debug();
#line 82
static void UARTPutChar(char c);
#line 102
static inline int so_printf(const uint8_t *format, ...);
# 91 "/Users/wbennett/opt/MoteWorks/lib/debug_constants.h"
static char dbg_buffer[256];
# 89 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/AM.h"
enum __nesc_unnamed4259 {
  TOS_BCAST_ADDR = 0xffff, 
  TOS_UART_ADDR = 0x007e
};









enum __nesc_unnamed4260 {
  TOS_DEFAULT_AM_GROUP = 0x7d
};


uint8_t TOS_AM_GROUP = TOS_DEFAULT_AM_GROUP;
#line 156
#line 132
typedef struct TOS_Msg {


  uint8_t length;
  uint8_t fcfhi;
  uint8_t fcflo;
  uint8_t dsn;
  uint16_t destpan;
  uint16_t addr;
  uint8_t type;
  uint8_t group;
  int8_t data[29 + 3];







  uint8_t strength;
  uint8_t lqi;
  bool crc;
  uint8_t ack;
  uint16_t time;
} TOS_Msg;




#line 158
typedef struct TinySec_Msg {

  uint8_t invalid;
} TinySec_Msg;









#line 163
typedef struct Ack_Msg {

  uint8_t length;
  uint8_t fcfhi;
  uint8_t fcflo;
  uint8_t dsn;
  uint8_t fcshi;
  uint8_t fcslo;
} Ack_Msg;

enum __nesc_unnamed4261 {

  MSG_HEADER_SIZE = (unsigned short )& ((struct TOS_Msg *)0)->data - 1, 

  MSG_FOOTER_SIZE = 2, 

  MSG_DATA_SIZE = (unsigned short )& ((struct TOS_Msg *)0)->strength + sizeof(uint16_t ), 

  DATA_LENGTH = 29, 

  LENGTH_BYTE_NUMBER = (unsigned short )& ((struct TOS_Msg *)0)->length + 1, 

  TOS_HEADER_SIZE = 5
};

typedef TOS_Msg *TOS_MsgPtr;
# 18 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.h"
enum __nesc_unnamed4262 {
  TIMER_REPEAT = 0, 
  TIMER_ONE_SHOT = 1, 
  NUM_TIMERS = 4U
};
# 19 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/byteorder.h"
static __inline int is_host_lsb();










static __inline uint16_t fromLSB16(uint16_t a);
# 11 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLTimer1.h"
enum __nesc_unnamed4263 {
  TCLK_CPU_OFF = 0, 
  TCLK_CPU_DIV1 = 1, 
  TCLK_CPU_DIV8 = 2, 
  TCLK_CPU_DIV64 = 3, 
  TCLK_CPU_DIV256 = 4, 
  TCLK_CPU_DIV1024 = 5
};
enum __nesc_unnamed4264 {
  TIMER1_DEFAULT_SCALE = TCLK_CPU_DIV64, 
  TIMER1_DEFAULT_INTERVAL = 255
};
# 13 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/Clock.h"
enum __nesc_unnamed4265 {
  TOS_I1000PS = 32, TOS_S1000PS = 1, 
  TOS_I100PS = 40, TOS_S100PS = 2, 
  TOS_I10PS = 101, TOS_S10PS = 3, 
  TOS_I1024PS = 0, TOS_S1024PS = 3, 
  TOS_I512PS = 1, TOS_S512PS = 3, 
  TOS_I256PS = 3, TOS_S256PS = 3, 
  TOS_I128PS = 7, TOS_S128PS = 3, 
  TOS_I64PS = 15, TOS_S64PS = 3, 
  TOS_I32PS = 31, TOS_S32PS = 3, 
  TOS_I16PS = 63, TOS_S16PS = 3, 
  TOS_I8PS = 127, TOS_S8PS = 3, 
  TOS_I4PS = 255, TOS_S4PS = 3, 
  TOS_I2PS = 15, TOS_S2PS = 7, 
  TOS_I1PS = 31, TOS_S1PS = 7, 
  TOS_I0PS = 0, TOS_S0PS = 0
};
enum __nesc_unnamed4266 {
  DEFAULT_SCALE = 3, DEFAULT_INTERVAL = 127
};
# 30 "/Users/wbennett/opt/MoteWorks/lib/avrtime.h"
struct avr_tm {
  int8_t sec;
  int8_t min;
  int8_t hour;
  int8_t day;
  int8_t mon;
  int16_t year;
  int8_t wday;
  int16_t day_of_year;
  uint8_t is_dst;
  uint8_t hundreth;
};

typedef uint64_t avrtime_t;


static inline void set_time_millis(avrtime_t set_time);

static inline void add_time_millis(uint32_t time_to_add);
static inline void init_avrtime();

static inline avrtime_t get_time_millis();





static inline void reset_start_time();



struct avr_tm;

struct avr_tm;
# 15 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static avrtime_t start_time;
static avrtime_t sys_time;
static bool isInit = 0;


static inline void set_time_millis(avrtime_t set_time);








static inline void init_avrtime();








static inline void add_time_millis(uint32_t time_to_add);







static inline avrtime_t get_time_millis();
#line 70
static inline void reset_start_time();
#line 133
struct avr_tm;
# 12 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/crc.h"
uint16_t crcTable[256] __attribute((__progmem__))  = { 
0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 
0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef, 
0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6, 
0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de, 
0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485, 
0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d, 
0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4, 
0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc, 
0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823, 
0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b, 
0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12, 
0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a, 
0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41, 
0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49, 
0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70, 
0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78, 
0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f, 
0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067, 
0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e, 
0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256, 
0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d, 
0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 
0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c, 
0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634, 
0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab, 
0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3, 
0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a, 
0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92, 
0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9, 
0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1, 
0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8, 
0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0 };





static inline uint16_t crcByte(uint16_t oldCrc, uint8_t byte);
# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
static result_t PotM$Pot$init(uint8_t initialSetting);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
static result_t HPLPotC$Pot$finalise(void );
#line 38
static result_t HPLPotC$Pot$decrease(void );







static result_t HPLPotC$Pot$increase(void );
# 32 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static result_t HPLInit$init(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr BaseM$ReceiveMsg$receive(TOS_MsgPtr m);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t BaseM$StdControl$init(void );






static result_t BaseM$StdControl$start(void );
# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t LedsC$Leds$init(void );
#line 51
static result_t LedsC$Leds$redOff(void );








static result_t LedsC$Leds$redToggle(void );
#line 43
static result_t LedsC$Leds$redOn(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr AMStandard$ReceiveMsg$default$receive(
# 35 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
uint8_t arg_0x101346968, 
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr m);
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t AMStandard$ActivityTimer$fired(void );
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
static result_t AMStandard$UARTSend$sendDone(TOS_MsgPtr msg, result_t success);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr AMStandard$RadioReceive$receive(TOS_MsgPtr m);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t AMStandard$Control$init(void );






static result_t AMStandard$Control$start(void );
# 44 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static result_t AMStandard$default$sendDone(void );
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
static result_t AMStandard$RadioSend$sendDone(TOS_MsgPtr msg, result_t success);
# 27 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendMsg.nc"
static result_t AMStandard$SendMsg$default$sendDone(
# 34 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
uint8_t arg_0x101347cb8, 
# 27 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendMsg.nc"
TOS_MsgPtr msg, result_t success);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr AMStandard$UARTReceive$receive(TOS_MsgPtr m);
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
static result_t RF230ControlM$SplitControl$init(void );
#line 55
static result_t RF230ControlM$SplitControl$start(void );
# 104 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Control.nc"
static void RF230ControlM$RF230Control$force_TRX_OFF(void );
#line 116
static void RF230ControlM$RF230Control$set_RX_ON(void );
#line 74
static void RF230ControlM$RF230Control$getRSSIandCRC(bool *crcValid, uint8_t *rssi);
#line 98
static void RF230ControlM$RF230Control$set_TRX_OFF(void );
#line 17
static void RF230ControlM$RF230Control$resetRadio(void );
#line 110
static void RF230ControlM$RF230Control$set_PLL_ON(void );
#line 90
static bool RF230ControlM$RF230Control$CCA(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t RF230ControlM$initTimer$fired(void );
# 29 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/WakeSequence.nc"
static void RF230RadioM$WakeSequence$sendWakeDone(result_t succ);
#line 46
static void RF230RadioM$WakeSequence$IncomingPacket(void );








static void RF230RadioM$WakeSequence$sniffExpired(bool channelClear);
# 63 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
static result_t RF230RadioM$RadioSplitControl$startDone(void );
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
static result_t RF230RadioM$PHYSend$sendDone(TOS_MsgPtr msg, result_t success);
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t RF230RadioM$SleepTimer$fired(void );
#line 51
static result_t RF230RadioM$IntervalTimer$fired(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr RF230RadioM$PHYRecv$receive(TOS_MsgPtr m);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t RF230RadioM$StdControl$init(void );






static result_t RF230RadioM$StdControl$start(void );
# 25 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/MacBackoff.nc"
static int16_t RF230RadioM$MacBackoff$congestionBackoff(TOS_MsgPtr m);
# 16 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Interrupts.nc"
static void RF230RadioRawM$RF230Interrupts$INT_RX_Start(void );



static void RF230RadioRawM$RF230Interrupts$INT_TRX_UnderRun(void );
#line 18
static void RF230RadioRawM$RF230Interrupts$INT_TRX_Done(void );
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
static result_t RF230RadioRawM$RadioSplitControl$init(void );
#line 55
static result_t RF230RadioRawM$RadioSplitControl$start(void );
# 15 "/Users/wbennett/opt/MoteWorks/tos/interfaces/RadioCoordinator.nc"
static void RF230RadioRawM$sendCoordinator$default$startSymbol(uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff);
# 22 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerJiffyAsync.nc"
static result_t RF230RadioRawM$TimerJiffyAsync$fired(void );
# 15 "/Users/wbennett/opt/MoteWorks/tos/interfaces/RadioCoordinator.nc"
static void RF230RadioRawM$recvCoordinator$default$startSymbol(uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff);
# 63 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
static result_t RF230RadioRawM$RF230ControlSplitControl$startDone(void );
# 12 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Interrupts.nc"
static void RF230InterruptsM$RF230Interrupts$enable(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t RF230InterruptsM$InterruptControl$init(void );






static result_t RF230InterruptsM$InterruptControl$start(void );
# 60 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerCapture.nc"
static void RF230InterruptsM$TimerCapture$captured(uint16_t time);
# 157 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/Clock16.nc"
static result_t HPLTimer1M$Timer1$default$fire(void );
# 19 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerCapture.nc"
static uint16_t HPLTimer1M$CaptureT1$getEvent(void );
#line 50
static void HPLTimer1M$CaptureT1$enableEvents(void );
#line 28
static void HPLTimer1M$CaptureT1$setEdge(uint8_t cm);
# 34 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.nc"
static void HPLRF230M$HPLRF230$writeFrame(uint8_t len, uint8_t *buff);
#line 46
static uint8_t HPLRF230M$HPLRF230$readFrameCRC(uint8_t *buff);
#line 14
static void HPLRF230M$HPLRF230$writeReg(uint8_t addr, uint8_t value);
#line 29
static void HPLRF230M$HPLRF230$bitWrite(uint8_t addr, uint8_t mask, uint8_t pos, uint8_t value);










static void HPLRF230M$HPLRF230$addCRC(uint8_t *buff);
#line 19
static uint8_t HPLRF230M$HPLRF230$readReg(uint8_t addr);
# 14 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230Init.nc"
static void HPLRF230M$HPLRF230Init$init(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t TimerM$Clock$fire(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t TimerM$StdControl$init(void );






static result_t TimerM$StdControl$start(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TimerM$Timer$default$fired(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x1016dfd38);
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TimerM$Timer$start(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x1016dfd38, 
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
char type, uint32_t interval);
# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static void HPLClock$Clock$setInterval(uint8_t value);
#line 127
static result_t HPLClock$Clock$setIntervalAndScale(uint8_t interval, uint8_t scale);




static uint8_t HPLClock$Clock$readCounter(void );
#line 75
static result_t HPLClock$Clock$setRate(char interval, char scale);
#line 100
static uint8_t HPLClock$Clock$getInterval(void );
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void );
# 16 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerJiffyAsync.nc"
static result_t TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(uint32_t jiffy);

static result_t TimerJiffyAsyncM$TimerJiffyAsync$stop(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t TimerJiffyAsyncM$Timer$fire(void );
#line 127
static result_t HPLTimer0$Timer0$setIntervalAndScale(uint8_t interval, uint8_t scale);
#line 147
static void HPLTimer0$Timer0$intDisable(void );
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Random.nc"
static uint16_t RandomLFSR$Random$rand(void );
#line 36
static result_t RandomLFSR$Random$init(void );
# 62 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t FramerM$ByteComm$txDone(void );
#line 54
static result_t FramerM$ByteComm$txByteReady(bool success);
#line 45
static result_t FramerM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t FramerM$StdControl$init(void );






static result_t FramerM$StdControl$start(void );
# 59 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
static result_t FramerM$TokenReceiveMsg$ReflectToken(uint8_t Token);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr FramerAckM$ReceiveMsg$receive(TOS_MsgPtr m);
# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
static TOS_MsgPtr FramerAckM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t Token);
# 66 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t UARTM$HPLUART$get(uint8_t data);







static result_t UARTM$HPLUART$putDone(void );
# 34 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t UARTM$ByteComm$txByte(uint8_t data);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t UARTM$Control$init(void );






static result_t UARTM$Control$start(void );
# 40 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t HPLUART0M$UART$init(void );
#line 58
static result_t HPLUART0M$UART$put(uint8_t data);
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
static result_t HPLUART0M$Setbaud(uint32_t baud_rate);
# 27 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
static result_t RealMain$hardwareInit(void );
# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
static result_t RealMain$Pot$init(uint8_t initialSetting);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t RealMain$StdControl$init(void );






static result_t RealMain$StdControl$start(void );
# 34 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
int main(void )   ;
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
static result_t PotM$HPLPot$finalise(void );
#line 38
static result_t PotM$HPLPot$decrease(void );







static result_t PotM$HPLPot$increase(void );
# 70 "/Users/wbennett/opt/MoteWorks/tos/system/PotM.nc"
uint8_t PotM$potSetting;

static inline void PotM$setPot(uint8_t value);
#line 85
static inline result_t PotM$Pot$init(uint8_t initialSetting);
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$decrease(void );








static inline result_t HPLPotC$Pot$increase(void );








static inline result_t HPLPotC$Pot$finalise(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static inline result_t HPLInit$init(void );
# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t BaseM$Leds$init(void );
#line 60
static result_t BaseM$Leds$redToggle(void );
# 23 "BaseM.nc"
uint16_t BaseM$counter;
TOS_Msg BaseM$Tmessage;

static inline result_t BaseM$StdControl$init(void );






static inline result_t BaseM$StdControl$start(void );







static inline TOS_MsgPtr BaseM$ReceiveMsg$receive(TOS_MsgPtr m);
# 28 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
uint8_t LedsC$ledsOn;

enum LedsC$__nesc_unnamed4267 {
  LedsC$RED_BIT = 1, 
  LedsC$GREEN_BIT = 2, 
  LedsC$YELLOW_BIT = 4
};

static result_t LedsC$Leds$init(void );
#line 50
static inline result_t LedsC$Leds$redOn(void );








static inline result_t LedsC$Leds$redOff(void );








static inline result_t LedsC$Leds$redToggle(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr AMStandard$ReceiveMsg$receive(
# 35 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
uint8_t arg_0x101346968, 
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr m);
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t AMStandard$ActivityTimer$start(char type, uint32_t interval);
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t AMStandard$PowerManagement$adjustPower(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t AMStandard$RadioControl$init(void );






static result_t AMStandard$RadioControl$start(void );
#line 41
static result_t AMStandard$TimerControl$init(void );






static result_t AMStandard$TimerControl$start(void );
#line 41
static result_t AMStandard$UARTControl$init(void );






static result_t AMStandard$UARTControl$start(void );
# 44 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static result_t AMStandard$sendDone(void );
# 27 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendMsg.nc"
static result_t AMStandard$SendMsg$sendDone(
# 34 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
uint8_t arg_0x101347cb8, 
# 27 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendMsg.nc"
TOS_MsgPtr msg, result_t success);
# 60 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
bool AMStandard$state;

uint16_t AMStandard$lastCount;
uint16_t AMStandard$counter;


static inline result_t AMStandard$Control$init(void );
#line 82
static inline result_t AMStandard$Control$start(void );
#line 111
static inline void AMStandard$dbgPacket(TOS_MsgPtr data);










static result_t AMStandard$reportSendDone(TOS_MsgPtr msg, result_t success);







static inline result_t AMStandard$ActivityTimer$fired(void );





static inline result_t AMStandard$SendMsg$default$sendDone(uint8_t id, TOS_MsgPtr msg, result_t success);


static inline result_t AMStandard$default$sendDone(void );
#line 186
static inline result_t AMStandard$UARTSend$sendDone(TOS_MsgPtr msg, result_t success);


static inline result_t AMStandard$RadioSend$sendDone(TOS_MsgPtr msg, result_t success);




TOS_MsgPtr received(TOS_MsgPtr packet)   ;
#line 221
static inline TOS_MsgPtr AMStandard$ReceiveMsg$default$receive(uint8_t id, TOS_MsgPtr msg);



static inline TOS_MsgPtr AMStandard$UARTReceive$receive(TOS_MsgPtr packet);





static inline TOS_MsgPtr AMStandard$RadioReceive$receive(TOS_MsgPtr packet);
# 63 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
static result_t RF230ControlM$SplitControl$startDone(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t RF230ControlM$InterruptControl$init(void );






static result_t RF230ControlM$InterruptControl$start(void );
#line 41
static result_t RF230ControlM$TimerControl$init(void );
# 14 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.nc"
static void RF230ControlM$HPLRF230$writeReg(uint8_t addr, uint8_t value);
#line 29
static void RF230ControlM$HPLRF230$bitWrite(uint8_t addr, uint8_t mask, uint8_t pos, uint8_t value);
#line 19
static uint8_t RF230ControlM$HPLRF230$readReg(uint8_t addr);
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t RF230ControlM$initTimer$start(char type, uint32_t interval);
# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t RF230ControlM$Leds$init(void );
# 14 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230Init.nc"
static void RF230ControlM$HPLRF230Init$init(void );
# 46 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
enum RF230ControlM$__nesc_unnamed4268 {
  RF230ControlM$RF230_CTL_POWERON, 


  RF230ControlM$RF230_CTL_START, 
  RF230ControlM$RF230_CTL_START_DONE
};



uint8_t RF230ControlM$controlState = RF230ControlM$RF230_CTL_POWERON;



static inline result_t RF230ControlM$powerOnInitial(void );
static inline result_t RF230ControlM$powerOnSleep(void );
static inline void RF230ControlM$initRegisters(void );







static inline void RF230ControlM$sigStartDone(void );
#line 85
static inline result_t RF230ControlM$powerOnInitial(void );
#line 118
static inline result_t RF230ControlM$powerOnSleep(void );
#line 133
static inline void RF230ControlM$initRegisters(void );
#line 158
static inline result_t RF230ControlM$SplitControl$init(void );
#line 190
static inline result_t RF230ControlM$SplitControl$start(void );
#line 264
static inline result_t RF230ControlM$initTimer$fired(void );
#line 315
static void RF230ControlM$RF230Control$resetRadio(void );
#line 408
static inline void RF230ControlM$RF230Control$getRSSIandCRC(bool *crcValid, uint8_t *rssi);
#line 439
static inline bool RF230ControlM$RF230Control$CCA(void );
#line 475
static inline void RF230ControlM$RF230Control$set_TRX_OFF(void );







static inline void RF230ControlM$RF230Control$force_TRX_OFF(void );







static void RF230ControlM$RF230Control$set_PLL_ON(void );







static void RF230ControlM$RF230Control$set_RX_ON(void );
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
static result_t RF230RadioM$RadioSplitControl$init(void );
#line 55
static result_t RF230RadioM$RadioSplitControl$start(void );
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
static result_t RF230RadioM$Send$sendDone(TOS_MsgPtr msg, result_t success);
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Random.nc"
static uint16_t RF230RadioM$Random$rand(void );
#line 36
static result_t RF230RadioM$Random$init(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr RF230RadioM$Recv$receive(TOS_MsgPtr m);
# 48 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioM.nc"
static inline result_t RF230RadioM$StdControl$init(void );







static inline result_t RF230RadioM$StdControl$start(void );
#line 80
static inline result_t RF230RadioM$RadioSplitControl$startDone(void );










static inline result_t RF230RadioM$PHYSend$sendDone(TOS_MsgPtr msg, result_t succ);






static inline TOS_MsgPtr RF230RadioM$PHYRecv$receive(TOS_MsgPtr msg);



static inline result_t RF230RadioM$IntervalTimer$fired(void );





static inline result_t RF230RadioM$SleepTimer$fired(void );
#line 125
static inline int16_t RF230RadioM$MacBackoff$congestionBackoff(TOS_MsgPtr m);






static inline void RF230RadioM$WakeSequence$IncomingPacket(void );

static inline void RF230RadioM$WakeSequence$sniffExpired(bool channelClear);

static inline void RF230RadioM$WakeSequence$sendWakeDone(result_t succ);
# 29 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/WakeSequence.nc"
static void RF230RadioRawM$WakeSequence$sendWakeDone(result_t succ);
#line 46
static void RF230RadioRawM$WakeSequence$IncomingPacket(void );








static void RF230RadioRawM$WakeSequence$sniffExpired(bool channelClear);
# 12 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Interrupts.nc"
static void RF230RadioRawM$RF230Interrupts$enable(void );
# 63 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
static result_t RF230RadioRawM$RadioSplitControl$startDone(void );
# 15 "/Users/wbennett/opt/MoteWorks/tos/interfaces/RadioCoordinator.nc"
static void RF230RadioRawM$sendCoordinator$startSymbol(uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff);
# 104 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Control.nc"
static void RF230RadioRawM$RF230Control$force_TRX_OFF(void );
#line 116
static void RF230RadioRawM$RF230Control$set_RX_ON(void );
#line 74
static void RF230RadioRawM$RF230Control$getRSSIandCRC(bool *crcValid, uint8_t *rssi);
#line 110
static void RF230RadioRawM$RF230Control$set_PLL_ON(void );
#line 90
static bool RF230RadioRawM$RF230Control$CCA(void );
# 16 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerJiffyAsync.nc"
static result_t RF230RadioRawM$TimerJiffyAsync$setOneShot(uint32_t jiffy);

static result_t RF230RadioRawM$TimerJiffyAsync$stop(void );
# 15 "/Users/wbennett/opt/MoteWorks/tos/interfaces/RadioCoordinator.nc"
static void RF230RadioRawM$recvCoordinator$startSymbol(uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff);
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
static result_t RF230RadioRawM$Send$sendDone(TOS_MsgPtr msg, result_t success);
# 36 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Random.nc"
static result_t RF230RadioRawM$Random$init(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr RF230RadioRawM$Receive$receive(TOS_MsgPtr m);
# 34 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.nc"
static void RF230RadioRawM$HPLRF230$writeFrame(uint8_t len, uint8_t *buff);
#line 46
static uint8_t RF230RadioRawM$HPLRF230$readFrameCRC(uint8_t *buff);
#line 40
static void RF230RadioRawM$HPLRF230$addCRC(uint8_t *buff);
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
static result_t RF230RadioRawM$RF230ControlSplitControl$init(void );
#line 55
static result_t RF230RadioRawM$RF230ControlSplitControl$start(void );
# 25 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/MacBackoff.nc"
static int16_t RF230RadioRawM$MacBackoff$congestionBackoff(TOS_MsgPtr m);
# 64 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
enum RF230RadioRawM$__nesc_unnamed4269 {
  RF230RadioRawM$RF230_TIMER_IDLE, 
  RF230RadioRawM$RF230_TIMER_BACKOFF, 
  RF230RadioRawM$RF230_TIMER_ACK, 
  RF230RadioRawM$RF230_TIMER_SEND, 
  RF230RadioRawM$RF230_TIMER_SNIFF
};

uint8_t RF230RadioRawM$timerState = RF230RadioRawM$RF230_TIMER_IDLE;



enum RF230RadioRawM$__nesc_unnamed4270 {

  RF230RadioRawM$RF230_RADIO_DISABLED, 




  RF230RadioRawM$RF230_RADIO_START, 
  RF230RadioRawM$RF230_RADIO_STOP, 


  RF230RadioRawM$RF230_RADIO_IDLE, 





  RF230RadioRawM$RF230_RADIO_SEND_SETUP, 
  RF230RadioRawM$RF230_RADIO_SEND_BACKOFF, 
  RF230RadioRawM$RF230_RADIO_SEND_TX, 
  RF230RadioRawM$RF230_RADIO_WAIT_ACK, 
  RF230RadioRawM$RF230_RADIO_SEND_DONE, 
  RF230RadioRawM$RF230_RADIO_SEND_FAIL, 


  RF230RadioRawM$RF230_RADIO_SEND_WAKEUP
};





uint8_t RF230RadioRawM$radioState;




bool RF230RadioRawM$bAckEnabled;

bool RF230RadioRawM$bRxBufLocked;
bool RF230RadioRawM$bTxBufLocked;


uint8_t RF230RadioRawM$sendRetries;
uint8_t RF230RadioRawM$currentDSN;

uint16_t RF230RadioRawM$wakePacketCount;



uint8_t RF230RadioRawM$txlength;

TOS_MsgPtr RF230RadioRawM$txbufptr;
TOS_MsgPtr RF230RadioRawM$rxbufptr;
TOS_Msg RF230RadioRawM$RxBuf;






static __inline void RF230RadioRawM$initLocalState(void );


static __inline void RF230RadioRawM$tryToSend(void );
static __inline void RF230RadioRawM$doCongestionBackOff(void );
static __inline void RF230RadioRawM$completeSend(void );

static __inline result_t RF230RadioRawM$setBackoffTimer(uint16_t jiffy);
static __inline result_t RF230RadioRawM$setAckTimer(uint16_t jiffy);

static __inline result_t RF230RadioRawM$setSendTimer(uint16_t jiffy);


static __inline void RF230RadioRawM$stopAllTimers(void );

static __inline void RF230RadioRawM$handleRxInt(void );
static __inline void RF230RadioRawM$handleTxInt(result_t status);
static __inline void RF230RadioRawM$handleSendAck(uint8_t dsn);
static __inline void RF230RadioRawM$handleWakeInt(void );
#line 176
static inline void RF230RadioRawM$handleSendDone(void );









static inline void RF230RadioRawM$handleRecv(void );
#line 204
static inline void RF230RadioRawM$sigJiffyTimer(void );










static inline result_t RF230RadioRawM$RadioSplitControl$init(void );
#line 234
static __inline void RF230RadioRawM$initLocalState(void );
#line 266
static inline result_t RF230RadioRawM$RadioSplitControl$start(void );
#line 299
static result_t RF230RadioRawM$RF230ControlSplitControl$startDone(void );
#line 470
static __inline void RF230RadioRawM$tryToSend(void );
#line 552
static __inline void RF230RadioRawM$doCongestionBackOff(void );
#line 577
static __inline void RF230RadioRawM$completeSend(void );
#line 611
static inline void RF230RadioRawM$RF230Interrupts$INT_RX_Start(void );
#line 638
static inline void RF230RadioRawM$RF230Interrupts$INT_TRX_UnderRun(void );
#line 682
static inline void RF230RadioRawM$RF230Interrupts$INT_TRX_Done(void );
#line 726
static __inline void RF230RadioRawM$handleTxInt(result_t status);
#line 763
static __inline void RF230RadioRawM$handleRxInt(void );
#line 898
static __inline void RF230RadioRawM$handleSendAck(uint8_t dsn);
#line 944
static __inline result_t RF230RadioRawM$setBackoffTimer(uint16_t jiffy);
#line 973
static __inline result_t RF230RadioRawM$setAckTimer(uint16_t jiffy);
#line 1014
static __inline result_t RF230RadioRawM$setSendTimer(uint16_t jiffy);
#line 1034
static __inline void RF230RadioRawM$stopAllTimers(void );







static result_t RF230RadioRawM$TimerJiffyAsync$fired(void );
#line 1183
static __inline void RF230RadioRawM$handleWakeInt(void );
#line 1248
static inline 
#line 1247
void RF230RadioRawM$sendCoordinator$default$startSymbol(
uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff);









static inline 
#line 1257
void RF230RadioRawM$recvCoordinator$default$startSymbol(
uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff);
# 16 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Interrupts.nc"
static void RF230InterruptsM$RF230Interrupts$INT_RX_Start(void );



static void RF230InterruptsM$RF230Interrupts$INT_TRX_UnderRun(void );
#line 18
static void RF230InterruptsM$RF230Interrupts$INT_TRX_Done(void );
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t RF230InterruptsM$PowerManagement$adjustPower(void );
# 50 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerCapture.nc"
static void RF230InterruptsM$TimerCapture$enableEvents(void );
#line 28
static void RF230InterruptsM$TimerCapture$setEdge(uint8_t cm);
# 19 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.nc"
static uint8_t RF230InterruptsM$HPLRF230$readReg(uint8_t addr);
# 41 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230InterruptsM.nc"
static inline result_t RF230InterruptsM$InterruptControl$init(void );




static inline result_t RF230InterruptsM$InterruptControl$start(void );
#line 74
static inline void RF230InterruptsM$TimerCapture$captured(uint16_t time);
#line 117
static inline void RF230InterruptsM$RF230Interrupts$enable(void );
# 157 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/Clock16.nc"
static result_t HPLTimer1M$Timer1$fire(void );
# 60 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerCapture.nc"
static void HPLTimer1M$CaptureT1$captured(uint16_t time);
# 44 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLTimer1M.nc"
volatile uint8_t HPLTimer1M$set_flag;
volatile uint8_t HPLTimer1M$mscale;
volatile uint8_t HPLTimer1M$nextScale;
volatile uint16_t HPLTimer1M$minterval;
#line 179
static inline result_t HPLTimer1M$Timer1$default$fire(void );

void __vector_17(void )   __attribute((interrupt)) ;
#line 208
static void HPLTimer1M$CaptureT1$setEdge(uint8_t LowToHigh);
#line 240
static inline uint16_t HPLTimer1M$CaptureT1$getEvent(void );
#line 274
static inline void HPLTimer1M$CaptureT1$enableEvents(void );
#line 307
void __vector_16(void )   __attribute((signal)) ;
# 251 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/util/crc16.h" 3
#line 250
static __inline uint16_t 
HPLRF230M$_crc_ccitt_update(uint16_t __crc, uint8_t __data);
# 18 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.h"
static __inline void HPLRF230M$initSPIMaster(void );
#line 32
static __inline void HPLRF230M$initRadioGPIO(void );
#line 61
static __inline void HPLRF230M$bios_reg_write(uint8_t addr, uint8_t val);
#line 84
static __inline uint8_t HPLRF230M$bios_reg_read(uint8_t addr);
#line 148
static __inline void HPLRF230M$bios_bit_write(uint8_t addr, uint8_t mask, uint8_t pos, uint8_t value);
#line 173
static __inline void HPLRF230M$bios_frame_write(uint8_t length, uint8_t *data);
#line 252
static __inline uint8_t HPLRF230M$bios_frame_crc_read(uint8_t *d);
#line 319
static __inline void HPLRF230M$bios_gen_crc16(uint8_t *msg);
# 26 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230M.nc"
static inline void HPLRF230M$HPLRF230Init$init(void );
#line 42
static inline void HPLRF230M$HPLRF230$writeReg(uint8_t addr, uint8_t value);






static uint8_t HPLRF230M$HPLRF230$readReg(uint8_t addr);









static inline void HPLRF230M$HPLRF230$writeFrame(uint8_t len, uint8_t *buff);







static inline void HPLRF230M$HPLRF230$addCRC(uint8_t *buff);







static inline uint8_t HPLRF230M$HPLRF230$readFrameCRC(uint8_t *buff);
#line 114
static inline void HPLRF230M$HPLRF230$bitWrite(uint8_t addr, uint8_t mask, uint8_t pos, uint8_t value);
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t TimerM$PowerManagement$adjustPower(void );
# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static void TimerM$Clock$setInterval(uint8_t value);
#line 132
static uint8_t TimerM$Clock$readCounter(void );
#line 75
static result_t TimerM$Clock$setRate(char interval, char scale);
#line 100
static uint8_t TimerM$Clock$getInterval(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TimerM$Timer$fired(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x1016dfd38);









extern int sprintf(char *arg_0x1016f5020, const char *arg_0x1016f52f8, ...)  ;
uint32_t TimerM$mState;
uint8_t TimerM$setIntervalFlag;
uint8_t TimerM$mScale;
#line 41
uint8_t TimerM$mInterval;
int8_t TimerM$queue_head;
int8_t TimerM$queue_tail;
uint8_t TimerM$queue_size;
uint8_t TimerM$queue[NUM_TIMERS];
volatile uint16_t TimerM$interval_outstanding;





#line 48
struct TimerM$timer_s {
  uint8_t type;
  int32_t ticks;
  int32_t ticksLeft;
} TimerM$mTimerList[NUM_TIMERS];

enum TimerM$__nesc_unnamed4271 {
  TimerM$maxTimerInterval = 230
};
static result_t TimerM$StdControl$init(void );
#line 69
static inline result_t TimerM$StdControl$start(void );










static result_t TimerM$Timer$start(uint8_t id, char type, 
uint32_t interval);
#line 111
inline static void TimerM$adjustInterval(void );
#line 164
static inline result_t TimerM$Timer$default$fired(uint8_t id);



static inline void TimerM$enqueue(uint8_t value);







static inline uint8_t TimerM$dequeue(void );









static inline void TimerM$signalOneTimer(void );





static inline void TimerM$HandleFire(void );
#line 237
static inline result_t TimerM$Clock$fire(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t HPLClock$Clock$fire(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
uint8_t HPLClock$set_flag;
uint8_t HPLClock$mscale;
#line 38
uint8_t HPLClock$nextScale;
#line 38
uint8_t HPLClock$minterval;
#line 70
static inline void HPLClock$Clock$setInterval(uint8_t value);









static inline uint8_t HPLClock$Clock$getInterval(void );
#line 108
static inline result_t HPLClock$Clock$setIntervalAndScale(uint8_t interval, uint8_t scale);
#line 136
static inline uint8_t HPLClock$Clock$readCounter(void );
#line 157
static inline result_t HPLClock$Clock$setRate(char interval, char scale);






void __vector_13(void )   __attribute((interrupt)) ;
# 48 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/HPLPowerManagementM.nc"
bool HPLPowerManagementM$disabled = 1;
#line 61
static inline uint8_t HPLPowerManagementM$getPowerLevel(void );
#line 114
static inline void HPLPowerManagementM$doAdjustment(void );
#line 134
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void );
# 22 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerJiffyAsync.nc"
static result_t TimerJiffyAsyncM$TimerJiffyAsync$fired(void );
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t TimerJiffyAsyncM$PowerManagement$adjustPower(void );
# 127 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t TimerJiffyAsyncM$Timer$setIntervalAndScale(uint8_t interval, uint8_t scale);
#line 147
static void TimerJiffyAsyncM$Timer$intDisable(void );
# 40 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerJiffyAsyncM.nc"
uint32_t TimerJiffyAsyncM$jiffy;
bool TimerJiffyAsyncM$bSet;
#line 68
static inline result_t TimerJiffyAsyncM$Timer$fire(void );
#line 88
static result_t TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(uint32_t _jiffy);
#line 110
static inline result_t TimerJiffyAsyncM$TimerJiffyAsync$stop(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t HPLTimer0$Timer0$fire(void );
# 78 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLTimer0.nc"
uint8_t HPLTimer0$set_flag;
uint8_t HPLTimer0$mscale;
#line 79
uint8_t HPLTimer0$nextScale;
#line 79
uint8_t HPLTimer0$minterval;
#line 148
static result_t HPLTimer0$Timer0$setIntervalAndScale(uint8_t interval, uint8_t scale);
#line 202
static inline void HPLTimer0$Timer0$intDisable(void );






void __vector_21(void )   __attribute((interrupt)) ;
# 33 "/Users/wbennett/opt/MoteWorks/tos/system/RandomLFSR.nc"
uint16_t RandomLFSR$shiftReg;
uint16_t RandomLFSR$initSeed;
uint16_t RandomLFSR$mask;


static result_t RandomLFSR$Random$init(void );










static inline uint16_t RandomLFSR$Random$rand(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr FramerM$ReceiveMsg$receive(TOS_MsgPtr m);
# 34 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t FramerM$ByteComm$txByte(uint8_t data);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t FramerM$ByteControl$init(void );






static result_t FramerM$ByteControl$start(void );
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
static result_t FramerM$BareSendMsg$sendDone(TOS_MsgPtr msg, result_t success);
# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
static TOS_MsgPtr FramerM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t Token);
# 55 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
enum FramerM$__nesc_unnamed4272 {
  FramerM$HDLC_QUEUESIZE = 2, 

  FramerM$HDLC_MTU = sizeof(TOS_Msg ) - 5, 



  FramerM$HDLC_FLAG_BYTE = 0x7e, 
  FramerM$HDLC_CTLESC_BYTE = 0x7d, 
  FramerM$PROTO_ACK = 64, 
  FramerM$PROTO_PACKET_ACK = 65, 
  FramerM$PROTO_PACKET_NOACK = 66, 
  FramerM$PROTO_UNKNOWN = 255
};

enum FramerM$__nesc_unnamed4273 {
  FramerM$RXSTATE_NOSYNC, 
  FramerM$RXSTATE_PROTO, 
  FramerM$RXSTATE_TOKEN, 
  FramerM$RXSTATE_INFO, 
  FramerM$RXSTATE_ESC
};

enum FramerM$__nesc_unnamed4274 {
  FramerM$TXSTATE_IDLE, 
  FramerM$TXSTATE_PROTO, 
  FramerM$TXSTATE_INFO, 
  FramerM$TXSTATE_ESC, 
  FramerM$TXSTATE_FCS1, 
  FramerM$TXSTATE_FCS2, 
  FramerM$TXSTATE_ENDFLAG, 
  FramerM$TXSTATE_FINISH, 
  FramerM$TXSTATE_ERROR
};

enum FramerM$__nesc_unnamed4275 {
  FramerM$FLAGS_TOKENPEND = 0x2, 
  FramerM$FLAGS_DATAPEND = 0x4, 
  FramerM$FLAGS_UNKNOWN = 0x8
};

TOS_Msg FramerM$gMsgRcvBuf[FramerM$HDLC_QUEUESIZE];






#line 98
typedef struct FramerM$_MsgRcvEntry {
  uint8_t Proto;
  uint8_t Token;
  uint16_t Length;
  TOS_MsgPtr pMsg;
} FramerM$MsgRcvEntry_t;

FramerM$MsgRcvEntry_t FramerM$gMsgRcvTbl[FramerM$HDLC_QUEUESIZE];

uint8_t *FramerM$gpRxBuf;
uint8_t *FramerM$gpTxBuf;

uint8_t FramerM$gFlags;


uint8_t FramerM$gTxState;
uint8_t FramerM$gPrevTxState;
uint8_t FramerM$gTxProto;
uint16_t FramerM$gTxByteCnt;
uint16_t FramerM$gTxLength;
uint16_t FramerM$gTxRunningCRC;


uint8_t FramerM$gRxState;
uint8_t FramerM$gRxHeadIndex;
uint8_t FramerM$gRxTailIndex;
uint16_t FramerM$gRxByteCnt;

uint16_t FramerM$gRxRunningCRC;

TOS_MsgPtr FramerM$gpTxMsg;
uint8_t FramerM$gTxTokenBuf;
uint8_t FramerM$gTxUnknownBuf;
uint8_t FramerM$gTxEscByte;

static void FramerM$PacketSent(void );

static uint8_t FramerM$fRemapRxPos(uint8_t InPos);






static uint8_t FramerM$fRemapRxPos(uint8_t InPos);
#line 157
static result_t FramerM$StartTx(void );
#line 217
static inline void FramerM$PacketUnknown(void );







static inline void FramerM$PacketRcvd(void );
#line 264
static void FramerM$PacketSent(void );
#line 286
static void FramerM$HDLCInitialize(void );
#line 309
static inline result_t FramerM$StdControl$init(void );




static inline result_t FramerM$StdControl$start(void );
#line 346
static inline result_t FramerM$TokenReceiveMsg$ReflectToken(uint8_t Token);
#line 366
static inline result_t FramerM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength);
#line 498
static result_t FramerM$TxArbitraryByte(uint8_t inByte);
#line 511
static inline result_t FramerM$ByteComm$txByteReady(bool LastByteSuccess);
#line 589
static inline result_t FramerM$ByteComm$txDone(void );
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr FramerAckM$ReceiveCombined$receive(TOS_MsgPtr m);
# 59 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
static result_t FramerAckM$TokenReceiveMsg$ReflectToken(uint8_t Token);
# 37 "/Users/wbennett/opt/MoteWorks/tos/system/FramerAckM.nc"
uint8_t FramerAckM$gTokenBuf;

static inline void FramerAckM$SendAckTask(void );




static inline TOS_MsgPtr FramerAckM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t token);
#line 56
static inline TOS_MsgPtr FramerAckM$ReceiveMsg$receive(TOS_MsgPtr Msg);
# 40 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t UARTM$HPLUART$init(void );
#line 58
static result_t UARTM$HPLUART$put(uint8_t data);
# 62 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t UARTM$ByteComm$txDone(void );
#line 54
static result_t UARTM$ByteComm$txByteReady(bool success);
#line 45
static result_t UARTM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength);
# 38 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
bool UARTM$state;

static inline result_t UARTM$Control$init(void );







static inline result_t UARTM$Control$start(void );








static inline result_t UARTM$HPLUART$get(uint8_t data);









static inline result_t UARTM$HPLUART$putDone(void );
#line 90
static result_t UARTM$ByteComm$txByte(uint8_t data);
# 66 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t HPLUART0M$UART$get(uint8_t data);







static result_t HPLUART0M$UART$putDone(void );
# 49 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
static inline result_t HPLUART0M$Setbaud(uint32_t baud_rate);
#line 83
static inline result_t HPLUART0M$UART$init(void );
#line 98
void __vector_25(void )   __attribute((signal)) ;









void __vector_27(void )   __attribute((interrupt)) ;




static inline result_t HPLUART0M$UART$put(uint8_t data);
# 64 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_SET_GREEN_LED_PIN()
#line 64
{
#line 64
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 1;
}

#line 65
static __inline void TOSH_SET_YELLOW_LED_PIN()
#line 65
{
#line 65
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 0;
}

#line 63
static __inline void TOSH_SET_RED_LED_PIN()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 2;
}

#line 79
static __inline void TOSH_SET_FLASH_SELECT_PIN()
#line 79
{
#line 79
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 3;
}

#line 80
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT()
#line 80
{
#line 80
  * (volatile uint8_t *)(0x0A + 0x20) |= 1 << 5;
}

#line 81
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT()
#line 81
{
#line 81
  * (volatile uint8_t *)(0x0A + 0x20) |= 1 << 3;
}

#line 79
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT()
#line 79
{
#line 79
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 3;
}

#line 67
static __inline void TOSH_CLR_SERIAL_ID_PIN()
#line 67
{
#line 67
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 4);
}

#line 67
static __inline void TOSH_MAKE_SERIAL_ID_INPUT()
#line 67
{
#line 67
  * (volatile uint8_t *)(0X01 + 0x20) &= ~(1 << 4);
}

#line 98
static __inline void TOSH_MAKE_PW0_OUTPUT()
#line 98
{
#line 98
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 0;
}

#line 99
static __inline void TOSH_MAKE_PW1_OUTPUT()
#line 99
{
#line 99
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 1;
}

#line 100
static __inline void TOSH_MAKE_PW2_OUTPUT()
#line 100
{
#line 100
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 2;
}

#line 101
static __inline void TOSH_MAKE_PW3_OUTPUT()
#line 101
{
#line 101
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 3;
}

#line 102
static __inline void TOSH_MAKE_PW4_OUTPUT()
#line 102
{
#line 102
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 4;
}

#line 103
static __inline void TOSH_MAKE_PW5_OUTPUT()
#line 103
{
#line 103
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 5;
}

#line 104
static __inline void TOSH_MAKE_PW6_OUTPUT()
#line 104
{
#line 104
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 6;
}

#line 105
static __inline void TOSH_MAKE_PW7_OUTPUT()
#line 105
{
#line 105
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 7;
}

#line 64
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT()
#line 64
{
#line 64
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 1;
}

#line 65
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT()
#line 65
{
#line 65
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 0;
}

#line 63
static __inline void TOSH_MAKE_RED_LED_OUTPUT()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 2;
}

#line 134
static inline void TOSH_SET_PIN_DIRECTIONS(void )
{







  TOSH_MAKE_RED_LED_OUTPUT();
  TOSH_MAKE_YELLOW_LED_OUTPUT();
  TOSH_MAKE_GREEN_LED_OUTPUT();


  TOSH_MAKE_PW7_OUTPUT();
  TOSH_MAKE_PW6_OUTPUT();
  TOSH_MAKE_PW5_OUTPUT();
  TOSH_MAKE_PW4_OUTPUT();
  TOSH_MAKE_PW3_OUTPUT();
  TOSH_MAKE_PW2_OUTPUT();
  TOSH_MAKE_PW1_OUTPUT();
  TOSH_MAKE_PW0_OUTPUT();
#line 173
  TOSH_MAKE_SERIAL_ID_INPUT();
  TOSH_CLR_SERIAL_ID_PIN();

  TOSH_MAKE_FLASH_SELECT_OUTPUT();
  TOSH_MAKE_FLASH_OUT_OUTPUT();
  TOSH_MAKE_FLASH_CLK_OUTPUT();
  TOSH_SET_FLASH_SELECT_PIN();

  TOSH_SET_RED_LED_PIN();
  TOSH_SET_YELLOW_LED_PIN();
  TOSH_SET_GREEN_LED_PIN();
}

# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static inline result_t HPLInit$init(void )
#line 37
{
  TOSH_SET_PIN_DIRECTIONS();
  return SUCCESS;
}

# 27 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
inline static result_t RealMain$hardwareInit(void ){
#line 27
  unsigned char __nesc_result;
#line 27

#line 27
  __nesc_result = HPLInit$init();
#line 27

#line 27
  return __nesc_result;
#line 27
}
#line 27
# 55 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$finalise(void )
#line 55
{


  return SUCCESS;
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$finalise(void ){
#line 53
  unsigned char __nesc_result;
#line 53

#line 53
  __nesc_result = HPLPotC$Pot$finalise();
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 46 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$increase(void )
#line 46
{





  return SUCCESS;
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$increase(void ){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = HPLPotC$Pot$increase();
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$decrease(void )
#line 37
{





  return SUCCESS;
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$decrease(void ){
#line 38
  unsigned char __nesc_result;
#line 38

#line 38
  __nesc_result = HPLPotC$Pot$decrease();
#line 38

#line 38
  return __nesc_result;
#line 38
}
#line 38
# 72 "/Users/wbennett/opt/MoteWorks/tos/system/PotM.nc"
static inline void PotM$setPot(uint8_t value)
#line 72
{
  uint8_t i;

#line 74
  for (i = 0; i < 151; i++) 
    PotM$HPLPot$decrease();

  for (i = 0; i < value; i++) 
    PotM$HPLPot$increase();

  PotM$HPLPot$finalise();

  PotM$potSetting = value;
}

static inline result_t PotM$Pot$init(uint8_t initialSetting)
#line 85
{
  PotM$setPot(initialSetting);
  return SUCCESS;
}

# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
inline static result_t RealMain$Pot$init(uint8_t initialSetting){
#line 57
  unsigned char __nesc_result;
#line 57

#line 57
  __nesc_result = PotM$Pot$init(initialSetting);
#line 57

#line 57
  return __nesc_result;
#line 57
}
#line 57
# 59 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
static inline void TOSH_sched_init(void )
{
  int i;

#line 62
  TOSH_sched_free = 0;
  TOSH_sched_full = 0;
  for (i = 0; i < TOSH_MAX_TASKS; i++) 
    TOSH_queue[i].tp = (void *)0;
}

# 158 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline result_t rcombine(result_t r1, result_t r2)



{
  return r1 == FAIL ? FAIL : r2;
}

# 36 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Random.nc"
inline static result_t RF230RadioM$Random$init(void ){
#line 36
  unsigned char __nesc_result;
#line 36

#line 36
  __nesc_result = RandomLFSR$Random$init();
#line 36

#line 36
  return __nesc_result;
#line 36
}
#line 36
# 234 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static __inline void RF230RadioRawM$initLocalState(void )
#line 234
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 235
    {
      RF230RadioRawM$timerState = RF230RadioRawM$RF230_TIMER_IDLE;
      RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_DISABLED;
      RF230RadioRawM$bAckEnabled = TRUE;

      RF230RadioRawM$bRxBufLocked = FALSE;
      RF230RadioRawM$bTxBufLocked = FALSE;

      RF230RadioRawM$sendRetries = 0;
      RF230RadioRawM$currentDSN = 0;

      RF230RadioRawM$rxbufptr = &RF230RadioRawM$RxBuf;
    }
#line 247
    __nesc_atomic_end(__nesc_atomic); }
}

# 36 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Random.nc"
inline static result_t RF230RadioRawM$Random$init(void ){
#line 36
  unsigned char __nesc_result;
#line 36

#line 36
  __nesc_result = RandomLFSR$Random$init();
#line 36

#line 36
  return __nesc_result;
#line 36
}
#line 36
# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t RF230ControlM$Leds$init(void ){
#line 35
  unsigned char __nesc_result;
#line 35

#line 35
  __nesc_result = LedsC$Leds$init();
#line 35

#line 35
  return __nesc_result;
#line 35
}
#line 35
# 41 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230InterruptsM.nc"
static inline result_t RF230InterruptsM$InterruptControl$init(void )
#line 41
{

  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RF230ControlM$InterruptControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = RF230InterruptsM$InterruptControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 76 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_RF230_CLKM_PIN()
#line 76
{
#line 76
  * (volatile uint8_t *)(0x0B + 0x20) &= ~(1 << 6);
}

#line 75
static __inline void TOSH_CLR_RF230_IRQ_PIN()
#line 75
{
#line 75
  * (volatile uint8_t *)(0x0B + 0x20) &= ~(1 << 4);
}

#line 73
static __inline void TOSH_CLR_RF230_SLP_TR_PIN()
#line 73
{
#line 73
  * (volatile uint8_t *)(0x05 + 0x20) &= ~(1 << 7);
}

#line 74
static __inline void TOSH_SET_RF230_RSTN_PIN()
#line 74
{
#line 74
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 6;
}

#line 76
static __inline void TOSH_MAKE_RF230_CLKM_INPUT()
#line 76
{
#line 76
  * (volatile uint8_t *)(0x0A + 0x20) &= ~(1 << 6);
}

#line 75
static __inline void TOSH_MAKE_RF230_IRQ_INPUT()
#line 75
{
#line 75
  * (volatile uint8_t *)(0x0A + 0x20) &= ~(1 << 4);
}

#line 74
static __inline void TOSH_MAKE_RF230_RSTN_OUTPUT()
#line 74
{
#line 74
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 6;
}

#line 73
static __inline void TOSH_MAKE_RF230_SLP_TR_OUTPUT()
#line 73
{
#line 73
  * (volatile uint8_t *)(0x04 + 0x20) |= 1 << 7;
}

# 32 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.h"
static __inline void HPLRF230M$initRadioGPIO(void )
#line 32
{
  TOSH_MAKE_RF230_SLP_TR_OUTPUT();
  TOSH_MAKE_RF230_RSTN_OUTPUT();
  TOSH_MAKE_RF230_IRQ_INPUT();
  TOSH_MAKE_RF230_CLKM_INPUT();

  TOSH_SET_RF230_RSTN_PIN();
  TOSH_CLR_RF230_SLP_TR_PIN();
  TOSH_CLR_RF230_IRQ_PIN();
  TOSH_CLR_RF230_CLKM_PIN();
}

# 92 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_SET_SPI_CS_PIN()
#line 92
{
#line 92
  * (volatile uint8_t *)(0x05 + 0x20) |= 1 << 0;
}

#line 92
static __inline void TOSH_MAKE_SPI_CS_OUTPUT()
#line 92
{
#line 92
  * (volatile uint8_t *)(0x04 + 0x20) |= 1 << 0;
}

#line 93
static __inline void TOSH_MAKE_SPI_SCK_OUTPUT()
#line 93
{
#line 93
  * (volatile uint8_t *)(0x04 + 0x20) |= 1 << 1;
}

#line 94
static __inline void TOSH_MAKE_MOSI_OUTPUT()
#line 94
{
#line 94
  * (volatile uint8_t *)(0x04 + 0x20) |= 1 << 2;
}

#line 95
static __inline void TOSH_MAKE_MISO_INPUT()
#line 95
{
#line 95
  * (volatile uint8_t *)(0x04 + 0x20) &= ~(1 << 3);
}

# 18 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.h"
static __inline void HPLRF230M$initSPIMaster(void )
#line 18
{
  TOSH_MAKE_MISO_INPUT();
  TOSH_MAKE_MOSI_OUTPUT();
  TOSH_MAKE_SPI_SCK_OUTPUT();
  TOSH_MAKE_SPI_CS_OUTPUT();
  TOSH_SET_SPI_CS_PIN();

  * (volatile uint8_t *)(0x2D + 0x20) = 1 << 0;
  * (volatile uint8_t *)(0x2C + 0x20) = (1 << 6) | (1 << 4);
}

# 26 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230M.nc"
static inline void HPLRF230M$HPLRF230Init$init(void )
#line 26
{






  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 33
    {
      HPLRF230M$initSPIMaster();
      HPLRF230M$initRadioGPIO();
    }
#line 36
    __nesc_atomic_end(__nesc_atomic); }
}

# 14 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230Init.nc"
inline static void RF230ControlM$HPLRF230Init$init(void ){
#line 14
  HPLRF230M$HPLRF230Init$init();
#line 14
}
#line 14
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RF230ControlM$TimerControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = TimerM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 158 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static inline result_t RF230ControlM$SplitControl$init(void )
#line 158
{

  RF230ControlM$TimerControl$init();
  RF230ControlM$HPLRF230Init$init();
  RF230ControlM$InterruptControl$init();
  RF230ControlM$Leds$init();

  return SUCCESS;
}

# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
inline static result_t RF230RadioRawM$RF230ControlSplitControl$init(void ){
#line 42
  unsigned char __nesc_result;
#line 42

#line 42
  __nesc_result = RF230ControlM$SplitControl$init();
#line 42

#line 42
  return __nesc_result;
#line 42
}
#line 42
# 215 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static inline result_t RF230RadioRawM$RadioSplitControl$init(void )
#line 215
{
  result_t ok1;
#line 216
  result_t ok2;

  ok1 = RF230RadioRawM$RF230ControlSplitControl$init();
  ok2 = RF230RadioRawM$Random$init();
  RF230RadioRawM$initLocalState();

  return rcombine(ok1, ok2);
}

# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
inline static result_t RF230RadioM$RadioSplitControl$init(void ){
#line 42
  unsigned char __nesc_result;
#line 42

#line 42
  __nesc_result = RF230RadioRawM$RadioSplitControl$init();
#line 42

#line 42
  return __nesc_result;
#line 42
}
#line 42
# 48 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioM.nc"
static inline result_t RF230RadioM$StdControl$init(void )
#line 48
{
  result_t ok1;
#line 49
  result_t ok2;

  ok1 = RF230RadioM$RadioSplitControl$init();
  ok2 = RF230RadioM$Random$init();
  return rcombine(ok1, ok2);
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t AMStandard$RadioControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = RF230RadioM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 40 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
static inline result_t UARTM$Control$init(void )
#line 40
{
  {
  }
#line 41
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 42
    {
      UARTM$state = 0;
    }
#line 44
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t FramerM$ByteControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = UARTM$Control$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 309 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
static inline result_t FramerM$StdControl$init(void )
#line 309
{
  FramerM$HDLCInitialize();
  return FramerM$ByteControl$init();
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t AMStandard$UARTControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = FramerM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
inline static result_t AMStandard$TimerControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = TimerM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 66 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$Control$init(void )
#line 66
{
  result_t ok1;
#line 67
  result_t ok2;

  AMStandard$TimerControl$init();
  ok1 = AMStandard$UARTControl$init();
  ok2 = AMStandard$RadioControl$init();

  AMStandard$state = FALSE;
  AMStandard$lastCount = 0;
  AMStandard$counter = 0;
  {
  }
#line 76
  ;

  return rcombine(ok1, ok2);
}

# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t BaseM$Leds$init(void ){
#line 35
  unsigned char __nesc_result;
#line 35

#line 35
  __nesc_result = LedsC$Leds$init();
#line 35

#line 35
  return __nesc_result;
#line 35
}
#line 35
# 26 "BaseM.nc"
static inline result_t BaseM$StdControl$init(void )
#line 26
{
  BaseM$counter = 0;
  BaseM$Leds$init();
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RealMain$StdControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = BaseM$StdControl$init();
#line 41
  __nesc_result = rcombine(__nesc_result, AMStandard$Control$init());
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 46 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static inline avrtime_t get_time_millis()
#line 46
{



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 50
    {
      unsigned long long __nesc_temp = 
#line 50
      sys_time;

      {
#line 50
        __nesc_atomic_end(__nesc_atomic); 
#line 50
        return __nesc_temp;
      }
    }
#line 52
    __nesc_atomic_end(__nesc_atomic); }
}

#line 70
static inline void reset_start_time()
#line 70
{



  start_time = get_time_millis();
}

#line 20
static inline void set_time_millis(avrtime_t set_time)
#line 20
{



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 24
    sys_time = set_time;
#line 24
    __nesc_atomic_end(__nesc_atomic); }
}



static inline void init_avrtime()
#line 29
{
  if (isInit == 0) {
      avrtime_t t = 132489216000ULL;

#line 32
      isInit = 1;
      set_time_millis(t);
      reset_start_time();
    }
}

# 108 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline result_t HPLClock$Clock$setIntervalAndScale(uint8_t interval, uint8_t scale)
#line 108
{



  if (scale > 7) {
#line 112
    return FAIL;
    }
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 114
    {
      * (volatile uint8_t *)0x70 &= ~(1 << 0);
      * (volatile uint8_t *)0x70 &= ~(1 << 1);




      * (volatile uint8_t *)0xB6 |= 1 << 5;

      HPLClock$mscale = scale;
      HPLClock$minterval = interval;

      * (volatile uint8_t *)0xB0 = 2 << 0;
      * (volatile uint8_t *)0xB1 = scale;

      * (volatile uint8_t *)0xB2 = 0;
      * (volatile uint8_t *)0xB3 = interval;
      * (volatile uint8_t *)0x70 |= 1 << 1;
    }
#line 132
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

#line 157
static inline result_t HPLClock$Clock$setRate(char interval, char scale)
#line 157
{
  return HPLClock$Clock$setIntervalAndScale(interval, scale);
}

# 75 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static result_t TimerM$Clock$setRate(char interval, char scale){
#line 75
  unsigned char __nesc_result;
#line 75

#line 75
  __nesc_result = HPLClock$Clock$setRate(interval, scale);
#line 75

#line 75
  return __nesc_result;
#line 75
}
#line 75
# 171 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline result_t rcombine4(result_t r1, result_t r2, result_t r3, 
result_t r4)
{
  return rcombine(r1, rcombine(r2, rcombine(r3, r4)));
}

# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
inline static uint8_t AMStandard$PowerManagement$adjustPower(void ){
#line 19
  unsigned char __nesc_result;
#line 19

#line 19
  __nesc_result = HPLPowerManagementM$PowerManagement$adjustPower();
#line 19

#line 19
  return __nesc_result;
#line 19
}
#line 19
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
inline static result_t AMStandard$ActivityTimer$start(char type, uint32_t interval){
#line 37
  unsigned char __nesc_result;
#line 37

#line 37
  __nesc_result = TimerM$Timer$start(0U, type, interval);
#line 37

#line 37
  return __nesc_result;
#line 37
}
#line 37
inline static result_t RF230ControlM$initTimer$start(char type, uint32_t interval){
#line 37
  unsigned char __nesc_result;
#line 37

#line 37
  __nesc_result = TimerM$Timer$start(3U, type, interval);
#line 37

#line 37
  return __nesc_result;
#line 37
}
#line 37
# 118 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static inline result_t RF230ControlM$powerOnSleep(void )
#line 118
{


  TOSH_CLR_RF230_SLP_TR_PIN();

  return RF230ControlM$initTimer$start(TIMER_ONE_SHOT, 1);
}

# 63 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
inline static result_t RF230ControlM$SplitControl$startDone(void ){
#line 63
  unsigned char __nesc_result;
#line 63

#line 63
  __nesc_result = RF230RadioRawM$RF230ControlSplitControl$startDone();
#line 63

#line 63
  return __nesc_result;
#line 63
}
#line 63
# 70 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static inline void RF230ControlM$sigStartDone(void )
#line 70
{

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 72
    RF230ControlM$controlState = RF230ControlM$RF230_CTL_START_DONE;
#line 72
    __nesc_atomic_end(__nesc_atomic); }
  RF230ControlM$SplitControl$startDone();
}

# 55 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_uwait2(int u_sec)
#line 55
{
  while (u_sec > 0) {
       __asm volatile ("nop");
      u_sec--;
    }
}

# 28 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerCapture.nc"
inline static void RF230InterruptsM$TimerCapture$setEdge(uint8_t cm){
#line 28
  HPLTimer1M$CaptureT1$setEdge(cm);
#line 28
}
#line 28
# 46 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230InterruptsM.nc"
static inline result_t RF230InterruptsM$InterruptControl$start(void )
#line 46
{






  RF230InterruptsM$TimerCapture$setEdge(1);



  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RF230ControlM$InterruptControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = RF230InterruptsM$InterruptControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 92 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_SPI_CS_PIN()
#line 92
{
#line 92
  * (volatile uint8_t *)(0x05 + 0x20) &= ~(1 << 0);
}

# 61 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.h"
static __inline void HPLRF230M$bios_reg_write(uint8_t addr, uint8_t val)
{
  addr = ((1 << 7) | (1 << 6)) | (((((((((0 << 7) | (0 << 6)) | (1 << 5)) | (1 << 4)) | (1 << 3)) | (1 << 2)) | (1 << 1)) | (1 << 0)) & addr);


  TOSH_CLR_SPI_CS_PIN();

  * (volatile uint8_t *)(0X2E + 0x20) = addr;
  do {
#line 69
      while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
    }
  while (
#line 69
  0);
  * (volatile uint8_t *)(0X2E + 0x20) = val;
  do {
#line 71
      while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
    }
  while (
#line 71
  0);

  TOSH_SET_SPI_CS_PIN();
}









static __inline uint8_t HPLRF230M$bios_reg_read(uint8_t addr)
{

  uint8_t val;

  addr = ((1 << 7) | (0 << 6)) | (((((((((0 << 7) | (0 << 6)) | (1 << 5)) | (1 << 4)) | (1 << 3)) | (1 << 2)) | (1 << 1)) | (1 << 0)) & addr);


  TOSH_CLR_SPI_CS_PIN();


  * (volatile uint8_t *)(0X2E + 0x20) = addr;
  do {
#line 96
      while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
    }
  while (
#line 96
  0);
  * (volatile uint8_t *)(0X2E + 0x20) = addr;
  do {
#line 98
      while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
    }
  while (
#line 98
  0);
  val = * (volatile uint8_t *)(0X2E + 0x20);

  TOSH_SET_SPI_CS_PIN();

  return val;
}

#line 148
static __inline void HPLRF230M$bios_bit_write(uint8_t addr, uint8_t mask, uint8_t pos, uint8_t value)
{

  uint8_t tmp;

#line 152
  tmp = HPLRF230M$bios_reg_read(addr);
  tmp &= ~mask;
  value <<= pos;
  value &= mask;
  value |= tmp;
  HPLRF230M$bios_reg_write(addr, value);
  return;
}

# 114 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230M.nc"
static inline void HPLRF230M$HPLRF230$bitWrite(uint8_t addr, uint8_t mask, uint8_t pos, uint8_t value)
#line 114
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 115
    HPLRF230M$bios_bit_write(addr, mask, pos, value);
#line 115
    __nesc_atomic_end(__nesc_atomic); }
}

# 29 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.nc"
inline static void RF230ControlM$HPLRF230$bitWrite(uint8_t addr, uint8_t mask, uint8_t pos, uint8_t value){
#line 29
  HPLRF230M$HPLRF230$bitWrite(addr, mask, pos, value);
#line 29
}
#line 29
# 475 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static inline void RF230ControlM$RF230Control$set_TRX_OFF(void )
#line 475
{
  RF230ControlM$HPLRF230$bitWrite(0x02, 0x1f, 0, 8);
}

#line 85
static inline result_t RF230ControlM$powerOnInitial(void )
#line 85
{



  TOSH_uwait2(650);
  RF230ControlM$RF230Control$set_TRX_OFF();
  TOSH_uwait2(256);




  RF230ControlM$RF230Control$resetRadio();






  RF230ControlM$InterruptControl$start();


  RF230ControlM$RF230Control$set_RX_ON();
  TOSH_uwait2(200);



  if (TOS_post(RF230ControlM$sigStartDone)) {
#line 111
    return SUCCESS;
    }
  else {
#line 111
    return FAIL;
    }
}

#line 190
static inline result_t RF230ControlM$SplitControl$start(void )
#line 190
{
  bool bPowerOn = TRUE;
  bool result;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 195
    {
      if (RF230ControlM$controlState == RF230ControlM$RF230_CTL_START) {
          result = FALSE;
        }
      else 
#line 198
        {
          result = TRUE;
          if (RF230ControlM$controlState == RF230ControlM$RF230_CTL_POWERON) {
#line 200
            bPowerOn = TRUE;
            }
          else {
#line 201
            bPowerOn = FALSE;
            }
#line 202
          RF230ControlM$controlState = RF230ControlM$RF230_CTL_START;
        }
    }
#line 204
    __nesc_atomic_end(__nesc_atomic); }


  if (!result) {
#line 207
    return FAIL;
    }



  if (bPowerOn) {
      return RF230ControlM$powerOnInitial();
    }
  else 
#line 214
    {
      return RF230ControlM$powerOnSleep();
    }
}

# 55 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
inline static result_t RF230RadioRawM$RF230ControlSplitControl$start(void ){
#line 55
  unsigned char __nesc_result;
#line 55

#line 55
  __nesc_result = RF230ControlM$SplitControl$start();
#line 55

#line 55
  return __nesc_result;
#line 55
}
#line 55
# 266 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static inline result_t RF230RadioRawM$RadioSplitControl$start(void )
#line 266
{
  result_t result;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 270
    {
      if (RF230RadioRawM$radioState == RF230RadioRawM$RF230_RADIO_START) {



          result = FAIL;
        }
      else 
#line 276
        {
          result = SUCCESS;
          RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_START;
        }
    }
#line 280
    __nesc_atomic_end(__nesc_atomic); }

  if (result == FAIL) {
#line 282
    return FAIL;
    }

  result = RF230RadioRawM$RF230ControlSplitControl$start();

  return result;
}

# 55 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
inline static result_t RF230RadioM$RadioSplitControl$start(void ){
#line 55
  unsigned char __nesc_result;
#line 55

#line 55
  __nesc_result = RF230RadioRawM$RadioSplitControl$start();
#line 55

#line 55
  return __nesc_result;
#line 55
}
#line 55
# 56 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioM.nc"
static inline result_t RF230RadioM$StdControl$start(void )
#line 56
{
  return RF230RadioM$RadioSplitControl$start();
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t AMStandard$RadioControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = RF230RadioM$StdControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 49 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
static inline result_t HPLUART0M$Setbaud(uint32_t baud_rate)
#line 49
{

  switch (baud_rate) {
      case 9600u: 
        * (volatile uint8_t *)0xC5 = 0;
      * (volatile uint8_t *)0xC4 = 95;
      break;

      case 19200u: 
        * (volatile uint8_t *)0xC5 = 0;
      * (volatile uint8_t *)0xC4 = 47;
      break;

      case 57600u: 
        * (volatile uint8_t *)0xC5 = 0;
      * (volatile uint8_t *)0xC4 = 15;
      break;

      case 115200u: 
        * (volatile uint8_t *)0xC5 = 0;
      * (volatile uint8_t *)0xC4 = 7;
      break;

      default: 
        return FAIL;
    }
  * (volatile uint8_t *)0xC0 = 1 << 1;
  * (volatile uint8_t *)0xC2 = (1 << 2) | (1 << 1);
  * (volatile uint8_t *)0XC1 = (((1 << 7) | (1 << 6)) | (1 << 4)) | (1 << 3);
  return SUCCESS;
}



static inline result_t HPLUART0M$UART$init(void )
#line 83
{

  HPLUART0M$Setbaud(TOS_UART0_BAUDRATE);
  return SUCCESS;
}

# 40 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t UARTM$HPLUART$init(void ){
#line 40
  unsigned char __nesc_result;
#line 40

#line 40
  __nesc_result = HPLUART0M$UART$init();
#line 40

#line 40
  return __nesc_result;
#line 40
}
#line 40
# 48 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
static inline result_t UARTM$Control$start(void )
#line 48
{
  return UARTM$HPLUART$init();
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t FramerM$ByteControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = UARTM$Control$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 314 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
static inline result_t FramerM$StdControl$start(void )
#line 314
{
  FramerM$HDLCInitialize();
  return FramerM$ByteControl$start();
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t AMStandard$UARTControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = FramerM$StdControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 69 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline result_t TimerM$StdControl$start(void )
#line 69
{
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t AMStandard$TimerControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = TimerM$StdControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 82 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$Control$start(void )
#line 82
{
  result_t ok0 = AMStandard$TimerControl$start();
  result_t ok1 = AMStandard$UARTControl$start();
  result_t ok2 = AMStandard$RadioControl$start();
  result_t ok3 = AMStandard$ActivityTimer$start(TIMER_REPEAT, 1000);



  AMStandard$state = FALSE;

  AMStandard$PowerManagement$adjustPower();

  return rcombine4(ok0, ok1, ok2, ok3);
}

# 33 "BaseM.nc"
static inline result_t BaseM$StdControl$start(void )
#line 33
{
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RealMain$StdControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = BaseM$StdControl$start();
#line 48
  __nesc_result = rcombine(__nesc_result, AMStandard$Control$start());
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 74 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_RF230_RSTN_PIN()
#line 74
{
#line 74
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 6);
}

# 42 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230M.nc"
static inline void HPLRF230M$HPLRF230$writeReg(uint8_t addr, uint8_t value)
#line 42
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 43
    HPLRF230M$bios_reg_write(addr, value);
#line 43
    __nesc_atomic_end(__nesc_atomic); }
}

# 14 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.nc"
inline static void RF230ControlM$HPLRF230$writeReg(uint8_t addr, uint8_t value){
#line 14
  HPLRF230M$HPLRF230$writeReg(addr, value);
#line 14
}
#line 14
# 133 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static inline void RF230ControlM$initRegisters(void )
#line 133
{










  RF230ControlM$HPLRF230$bitWrite(0x05, 0x0f, 0, TOS_RF230_TXPOWER);
  RF230ControlM$HPLRF230$bitWrite(0x08, 0x1f, 0, TOS_RF230_CHANNEL);
  RF230ControlM$HPLRF230$writeReg(0x0e, (0x40 | 0x08) | 0x04);
  RF230ControlM$HPLRF230$writeReg(0x03, 0x08);
}

# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
inline static uint8_t RF230InterruptsM$PowerManagement$adjustPower(void ){
#line 19
  unsigned char __nesc_result;
#line 19

#line 19
  __nesc_result = HPLPowerManagementM$PowerManagement$adjustPower();
#line 19

#line 19
  return __nesc_result;
#line 19
}
#line 19
# 274 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLTimer1M.nc"
static inline void HPLTimer1M$CaptureT1$enableEvents(void )
#line 274
{

  * (volatile uint8_t *)0x81 &= ~(1 << 0);
  * (volatile uint8_t *)0x81 &= ~(1 << 1);
  * (volatile uint8_t *)0x81 &= ~(1 << 3);
  * (volatile uint8_t *)0x81 &= ~(1 << 4);
  * (volatile uint8_t *)0x6F |= 1 << 5;
}

# 50 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerCapture.nc"
inline static void RF230InterruptsM$TimerCapture$enableEvents(void ){
#line 50
  HPLTimer1M$CaptureT1$enableEvents();
#line 50
}
#line 50
# 117 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230InterruptsM.nc"
static inline void RF230InterruptsM$RF230Interrupts$enable(void )
#line 117
{
  RF230InterruptsM$TimerCapture$enableEvents();
  RF230InterruptsM$PowerManagement$adjustPower();
}

# 12 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Interrupts.nc"
inline static void RF230RadioRawM$RF230Interrupts$enable(void ){
#line 12
  RF230InterruptsM$RF230Interrupts$enable();
#line 12
}
#line 12
# 61 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/HPLPowerManagementM.nc"
static inline uint8_t HPLPowerManagementM$getPowerLevel(void )
#line 61
{

  if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0x6E & (1 << 1)) {

      return 0;
    }
  else {
#line 67
    if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0x6F & (1 << 5)) {

        return 0;
      }
    else {
#line 71
      if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)(0x2C + 0x20) & (1 << 7)) {

          return 0;
        }
      else {
#line 90
        if (* (volatile uint8_t *)0XC9 & ((((1 << 7) | (1 << 6)) | (1 << 4)) | (1 << 3))) {

            return 0;
          }
        else {
#line 94
          if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0x7A & (1 << 7)) {

              return 1 << 1;
            }
          else {
#line 98
            if (* (volatile uint8_t *)0x70 & ((1 << 1) | (1 << 0))) {

                uint8_t diff;

#line 101
                diff = * (volatile uint8_t *)0xB3 - * (volatile uint8_t *)0xB2;
                if (diff < 16) {
#line 102
                  return ((1 << 3) | (1 << 2)) | (1 << 1);
                  }
                else {
#line 103
                  return (1 << 2) | (1 << 1);
                  }
              }
            else 
#line 105
              {

                return 1 << 2;
              }
            }
          }
        }
      }
    }
}

#line 114
static inline void HPLPowerManagementM$doAdjustment(void )
#line 114
{
  uint8_t foo;
#line 115
  uint8_t mcu;

#line 116
  foo = HPLPowerManagementM$getPowerLevel();
  mcu = * (volatile uint8_t *)(0x33 + 0x20);
  mcu &= 0xf1;
  if (foo == (((1 << 3) | (1 << 2)) | (1 << 1)) || foo == ((1 << 2) | (1 << 1))) {
      mcu |= 0;
      while ((* (volatile uint8_t *)0xB6 & 0x1f) != 0) {
           __asm volatile ("nop");}

      mcu &= 0xf1;
    }
  mcu |= foo;
  * (volatile uint8_t *)(0x33 + 0x20) = mcu;
  * (volatile uint8_t *)(0x33 + 0x20) |= 1 << 0;
}

# 80 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioM.nc"
static inline result_t RF230RadioM$RadioSplitControl$startDone(void )
#line 80
{
  return SUCCESS;
}

# 63 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SplitControl.nc"
inline static result_t RF230RadioRawM$RadioSplitControl$startDone(void ){
#line 63
  unsigned char __nesc_result;
#line 63

#line 63
  __nesc_result = RF230RadioM$RadioSplitControl$startDone();
#line 63

#line 63
  return __nesc_result;
#line 63
}
#line 63
# 166 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
static __inline void __nesc_enable_interrupt()
#line 166
{
   __asm volatile ("sei");}

#line 151
__inline  void __nesc_atomic_end(__nesc_atomic_t oldSreg)
{
  * (volatile uint8_t *)(0x3F + 0x20) = oldSreg;
}

#line 129
static inline void TOSH_wait()
{
   __asm volatile ("nop");
   __asm volatile ("nop");}

#line 158
static __inline void __nesc_atomic_sleep()
{

   __asm volatile ("sei");
   __asm volatile ("sleep");
  TOSH_wait();
}

#line 144
__inline  __nesc_atomic_t __nesc_atomic_start(void )
{
  __nesc_atomic_t result = * (volatile uint8_t *)(0x3F + 0x20);

#line 147
   __asm volatile ("cli");
  return result;
}

# 116 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
static inline bool TOSH_run_next_task()
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t old_full;
  void (*func)(void );

  fInterruptFlags = __nesc_atomic_start();
  old_full = TOSH_sched_full;
  func = TOSH_queue[old_full].tp;
  if (func == (void *)0) 
    {
      __nesc_atomic_sleep();
      return 0;
    }

  TOSH_queue[old_full].tp = (void *)0;
  TOSH_sched_full = (old_full + 1) & TOSH_TASK_BITMASK;
  __nesc_atomic_end(fInterruptFlags);
  func();

  return 1;
}

static inline void TOSH_run_task()
#line 139
{
  for (; ; ) 
    TOSH_run_next_task();
}

# 111 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline void AMStandard$dbgPacket(TOS_MsgPtr data)
#line 111
{
  uint8_t i;

  for (i = 0; i < sizeof(TOS_Msg ); i++) 
    {
      {
      }
#line 116
      ;
    }
  {
  }
#line 118
  ;
}

# 47 "/Users/wbennett/opt/MoteWorks/lib/SOdebug0.h"
inline static void init_debug()
#line 47
{

  * (volatile uint8_t *)0xC5 = 0;



  * (volatile uint8_t *)0xC4 = 7;
  * (volatile uint8_t *)0xC0 = 1 << 1;







  * (volatile uint8_t *)0xC2 = (1 << 2) | (1 << 1);
  * (volatile uint8_t *)0XC6;
  * (volatile uint8_t *)0XC1 = 1 << 3;
}

#line 102
static inline int so_printf(const uint8_t *format, ...)
{
  uint8_t format_flag;
  uint32_t u_val = 0;
  uint32_t div_val;
  uint8_t base;
  uint8_t *ptr;
  bool longNumber = FALSE;
  va_list ap;

  __builtin_va_start(ap, format);
  if (!debugStarted) {
      init_debug();
      debugStarted = 1;
    }
#line 116
  ;
  if (format == (void *)0) {
#line 117
    format = "NULL\n";
    }
#line 118
  for (; ; ) 
    {
      if (!longNumber) {
          while ((format_flag = * format++) != '%') 
            {
              if (!format_flag) {
                  return 0;
                }
#line 125
              ;
              UARTPutChar(format_flag);
            }
#line 127
          ;
        }
#line 128
      ;
      switch ((format_flag = * format++)) {
          case 'c': 
            format_flag = (__builtin_va_arg(ap, int ));
          default: 
            UARTPutChar(format_flag);
          continue;

          case 'S': 
            case 's': 
              ptr = (uint8_t *)(__builtin_va_arg(ap, char *));
          while ((format_flag = * ptr++) != 0) {
              UARTPutChar(format_flag);
            }
#line 141
          ;
          continue;
#line 207
          case 'l': 
            longNumber = TRUE;
          continue;

          case 'o': 
            base = 8;
          if (!longNumber) {
            div_val = 0x8000;
            }
          else {
#line 216
            div_val = 0x40000000;
            }
#line 217
          goto CONVERSION_LOOP;

          case 'u': 
            case 'i': 
              case 'd': 
                base = 10;
          if (!longNumber) {
            div_val = 10000;
            }
          else {
#line 226
            div_val = 1000000000;
            }
#line 227
          goto CONVERSION_LOOP;

          case 'x': 
            base = 16;
          if (!longNumber) {
            div_val = 0x1000;
            }
          else {
#line 234
            div_val = 0x10000000;
            }
          CONVERSION_LOOP: 
            {
              if (!longNumber) {
                u_val = (__builtin_va_arg(ap, int ));
                }
              else {
#line 241
                u_val = (__builtin_va_arg(ap, long ));
                }
#line 242
              if (format_flag == 'd' || format_flag == 'i') {
                  bool isNegative;

#line 244
                  if (!longNumber) {
                    isNegative = (int )u_val < 0;
                    }
                  else {
#line 247
                    isNegative = (long )u_val < 0;
                    }
#line 248
                  if (isNegative) {
                      u_val = -u_val;
                      UARTPutChar('-');
                    }
#line 251
                  ;
                  while (div_val > 1 && div_val > u_val) {
                      div_val /= 10;
                    }
#line 254
                  ;
                }

              if (format_flag == 'x' && !longNumber) {
#line 257
                u_val &= 0xffff;
                }
#line 258
              do {
                  UARTPutChar(hex[u_val / div_val]);
                  u_val %= div_val;
                  div_val /= base;
                }
              while (div_val);
              longNumber = FALSE;
            }
#line 265
          ;
          break;
        }
#line 267
      ;
    }
#line 268
  ;
}

# 63 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_RED_LED_PIN()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 2);
}

# 50 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$redOn(void )
#line 50
{
  {
  }
#line 51
  ;
  /* atomic removed: atomic calls only */
#line 52
  {
    TOSH_CLR_RED_LED_PIN();
    LedsC$ledsOn |= LedsC$RED_BIT;
  }
  return SUCCESS;
}

static inline result_t LedsC$Leds$redOff(void )
#line 59
{
  {
  }
#line 60
  ;
  /* atomic removed: atomic calls only */
#line 61
  {
    TOSH_SET_RED_LED_PIN();
    LedsC$ledsOn &= ~LedsC$RED_BIT;
  }
  return SUCCESS;
}

static inline result_t LedsC$Leds$redToggle(void )
#line 68
{
  result_t rval;

#line 70
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 70
    {
      if (LedsC$ledsOn & LedsC$RED_BIT) {
        rval = LedsC$Leds$redOff();
        }
      else {
#line 74
        rval = LedsC$Leds$redOn();
        }
    }
#line 76
    __nesc_atomic_end(__nesc_atomic); }
#line 76
  return rval;
}

# 60 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t BaseM$Leds$redToggle(void ){
#line 60
  unsigned char __nesc_result;
#line 60

#line 60
  __nesc_result = LedsC$Leds$redToggle();
#line 60

#line 60
  return __nesc_result;
#line 60
}
#line 60
# 187 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline void *nmemcpy(void *to, const void *from, size_t n)
{
  char *cto = to;
  const char *cfrom = from;

  while (n--) * cto++ = * cfrom++;

  return to;
}

# 41 "BaseM.nc"
static inline TOS_MsgPtr BaseM$ReceiveMsg$receive(TOS_MsgPtr m)
#line 41
{

  OrientationMessage *Omessage;


  nmemcpy(&BaseM$Tmessage, m, sizeof(TOS_Msg ));

  Omessage = (OrientationMessage *)BaseM$Tmessage.data;

  BaseM$Leds$redToggle();
  sprintf(dbg_buffer, "%lu\t|\t%i\t%i\t%i\t|\t%i\t%i\t%i\t|\t%i\n", Omessage->seqnum, Omessage->accelx, Omessage->accely, Omessage->accelz, Omessage->heading, Omessage->pitch, Omessage->roll, Omessage->color);
#line 51
  {
#line 51
    if (1 != 0) {
#line 51
        so_printf("%s", dbg_buffer);
      }
#line 51
    ;
  }
#line 51
  ;








  return m;
}

# 221 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline TOS_MsgPtr AMStandard$ReceiveMsg$default$receive(uint8_t id, TOS_MsgPtr msg)
#line 221
{
  return msg;
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
inline static TOS_MsgPtr AMStandard$ReceiveMsg$receive(uint8_t arg_0x101346968, TOS_MsgPtr m){
#line 53
  struct TOS_Msg *__nesc_result;
#line 53

#line 53
  switch (arg_0x101346968) {
#line 53
    case AM_CRANE:
#line 53
      __nesc_result = BaseM$ReceiveMsg$receive(m);
#line 53
      break;
#line 53
    default:
#line 53
      __nesc_result = AMStandard$ReceiveMsg$default$receive(arg_0x101346968, m);
#line 53
      break;
#line 53
    }
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 179 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLTimer1M.nc"
static inline result_t HPLTimer1M$Timer1$default$fire(void )
#line 179
{
#line 179
  return SUCCESS;
}

# 157 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/Clock16.nc"
inline static result_t HPLTimer1M$Timer1$fire(void ){
#line 157
  unsigned char __nesc_result;
#line 157

#line 157
  __nesc_result = HPLTimer1M$Timer1$default$fire();
#line 157

#line 157
  return __nesc_result;
#line 157
}
#line 157
# 39 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_uwait(int u_sec)
#line 39
{
  while (u_sec > 0) {
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
      u_sec--;
    }
}

#line 73
static __inline void TOSH_SET_RF230_SLP_TR_PIN()
#line 73
{
#line 73
  * (volatile uint8_t *)(0x05 + 0x20) |= 1 << 7;
}

# 136 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioM.nc"
static inline void RF230RadioM$WakeSequence$sendWakeDone(result_t succ)
#line 136
{
}

# 29 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/WakeSequence.nc"
inline static void RF230RadioRawM$WakeSequence$sendWakeDone(result_t succ){
#line 29
  RF230RadioM$WakeSequence$sendWakeDone(succ);
#line 29
}
#line 29
# 116 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Control.nc"
inline static void RF230RadioRawM$RF230Control$set_RX_ON(void ){
#line 116
  RF230ControlM$RF230Control$set_RX_ON();
#line 116
}
#line 116
# 1183 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static __inline void RF230RadioRawM$handleWakeInt(void )
#line 1183
{
  bool bDone;

  /* atomic removed: atomic calls only */



  {
    if (--RF230RadioRawM$wakePacketCount == 0) {
        bDone = TRUE;
        RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_IDLE;
        RF230RadioRawM$bTxBufLocked = FALSE;
      }
    else 
#line 1195
      {
        bDone = FALSE;
      }
  }

  if (bDone) {


      RF230RadioRawM$RF230Control$set_RX_ON();
      RF230RadioRawM$WakeSequence$sendWakeDone(SUCCESS);
    }
  else {

      TOSH_SET_RF230_SLP_TR_PIN();
      TOSH_uwait(1);
      TOSH_CLR_RF230_SLP_TR_PIN();
    }
}

#line 638
static inline void RF230RadioRawM$RF230Interrupts$INT_TRX_UnderRun(void )
#line 638
{
  bool bWakeDone;

  /* atomic removed: atomic calls only */
#line 641
  {
    if (RF230RadioRawM$radioState == RF230RadioRawM$RF230_RADIO_SEND_WAKEUP) {
#line 642
      bWakeDone = TRUE;
      }
    else {
#line 643
      bWakeDone = FALSE;
      }
  }
  if (bWakeDone) {
#line 646
    RF230RadioRawM$handleWakeInt();
    }
}

# 20 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Interrupts.nc"
inline static void RF230InterruptsM$RF230Interrupts$INT_TRX_UnderRun(void ){
#line 20
  RF230RadioRawM$RF230Interrupts$INT_TRX_UnderRun();
#line 20
}
#line 20
# 231 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline TOS_MsgPtr AMStandard$RadioReceive$receive(TOS_MsgPtr packet)
#line 231
{
  return received(packet);
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
inline static TOS_MsgPtr RF230RadioM$Recv$receive(TOS_MsgPtr m){
#line 53
  struct TOS_Msg *__nesc_result;
#line 53

#line 53
  __nesc_result = AMStandard$RadioReceive$receive(m);
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 98 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioM.nc"
static inline TOS_MsgPtr RF230RadioM$PHYRecv$receive(TOS_MsgPtr msg)
#line 98
{
  return RF230RadioM$Recv$receive(msg);
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
inline static TOS_MsgPtr RF230RadioRawM$Receive$receive(TOS_MsgPtr m){
#line 53
  struct TOS_Msg *__nesc_result;
#line 53

#line 53
  __nesc_result = RF230RadioM$PHYRecv$receive(m);
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 186 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static inline void RF230RadioRawM$handleRecv(void )
#line 186
{
  TOS_MsgPtr ptr;

  ptr = RF230RadioRawM$Receive$receive(RF230RadioRawM$rxbufptr);
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 190
    {
      RF230RadioRawM$rxbufptr = ptr;
      RF230RadioRawM$bRxBufLocked = FALSE;
    }
#line 193
    __nesc_atomic_end(__nesc_atomic); }

  ;
}

# 19 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/byteorder.h"
static __inline int is_host_lsb()
{
  const uint8_t n[2] = { 1, 0 };

#line 22
  return * (uint16_t *)n == 1;
}






static __inline uint16_t fromLSB16(uint16_t a)
{
  return is_host_lsb() ? a : (a << 8) | (a >> 8);
}

# 173 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.h"
static __inline void HPLRF230M$bios_frame_write(uint8_t length, uint8_t *data)
{






  TOSH_CLR_SPI_CS_PIN();

  * (volatile uint8_t *)(0X2E + 0x20) = ((0 << 7) | (1 << 6)) | (1 << 5);
  do {
#line 184
      while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
    }
  while (
#line 184
  0);
  * (volatile uint8_t *)(0X2E + 0x20) = length;

  do 
    {
      do {
#line 189
          while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
        }
      while (
#line 189
      0);
      * (volatile uint8_t *)(0X2E + 0x20) = * data++;
    }
  while (--length > 0);

  do {
#line 194
      while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
    }
  while (
#line 194
  0);



  TOSH_SET_SPI_CS_PIN();
}

# 59 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230M.nc"
static inline void HPLRF230M$HPLRF230$writeFrame(uint8_t len, uint8_t *buff)
#line 59
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 60
    HPLRF230M$bios_frame_write(len, buff);
#line 60
    __nesc_atomic_end(__nesc_atomic); }
}

# 34 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.nc"
inline static void RF230RadioRawM$HPLRF230$writeFrame(uint8_t len, uint8_t *buff){
#line 34
  HPLRF230M$HPLRF230$writeFrame(len, buff);
#line 34
}
#line 34
# 251 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/util/crc16.h" 3
#line 250
static __inline uint16_t 
HPLRF230M$_crc_ccitt_update(uint16_t __crc, uint8_t __data)
{
  uint16_t __ret;

   __asm volatile (
  "eor    %A0,%1""\n\t"

  "mov    __tmp_reg__,%A0""\n\t"
  "swap   %A0""\n\t"
  "andi   %A0,0xf0""\n\t"
  "eor    %A0,__tmp_reg__""\n\t"

  "mov    __tmp_reg__,%B0""\n\t"

  "mov    %B0,%A0""\n\t"

  "swap   %A0""\n\t"
  "andi   %A0,0x0f""\n\t"
  "eor    __tmp_reg__,%A0""\n\t"

  "lsr    %A0""\n\t"
  "eor    %B0,%A0""\n\t"

  "eor    %A0,%B0""\n\t"
  "lsl    %A0""\n\t"
  "lsl    %A0""\n\t"
  "lsl    %A0""\n\t"
  "eor    %A0,__tmp_reg__" : 

  "=d"(__ret) : 
  "r"(__data), "0"(__crc) : 
  "r0");

  return __ret;
}

# 319 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.h"
static __inline void HPLRF230M$bios_gen_crc16(uint8_t *msg)
{
  uint16_t taps;
  uint8_t j;
#line 322
  uint8_t end;
  uint8_t *octet = &msg[1];

  end = *msg;
  taps = 0x0000;

  if (end >= 2) 
    {
      end = end - 2;
      for (j = 0; j < end; j++) 
        {
          taps = HPLRF230M$_crc_ccitt_update(taps, octet[j]);
        }


      octet[j] = (uint8_t )(taps & 0x00FF);
      j++;
      octet[j] = (uint8_t )((taps >> 8) & 0x00FF);
    }
}

# 67 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230M.nc"
static inline void HPLRF230M$HPLRF230$addCRC(uint8_t *buff)
#line 67
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 68
    HPLRF230M$bios_gen_crc16(buff);
#line 68
    __nesc_atomic_end(__nesc_atomic); }
}

# 40 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.nc"
inline static void RF230RadioRawM$HPLRF230$addCRC(uint8_t *buff){
#line 40
  HPLRF230M$HPLRF230$addCRC(buff);
#line 40
}
#line 40
# 110 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Control.nc"
inline static void RF230RadioRawM$RF230Control$set_PLL_ON(void ){
#line 110
  RF230ControlM$RF230Control$set_PLL_ON();
#line 110
}
#line 110
# 483 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static inline void RF230ControlM$RF230Control$force_TRX_OFF(void )
#line 483
{
  RF230ControlM$HPLRF230$bitWrite(0x02, 0x1f, 0, 3);
}

# 104 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Control.nc"
inline static void RF230RadioRawM$RF230Control$force_TRX_OFF(void ){
#line 104
  RF230ControlM$RF230Control$force_TRX_OFF();
#line 104
}
#line 104
# 898 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static __inline void RF230RadioRawM$handleSendAck(uint8_t dsn)
#line 898
{
  Ack_Msg msg;


  RF230RadioRawM$RF230Control$force_TRX_OFF();
  RF230RadioRawM$RF230Control$set_PLL_ON();
  TOSH_uwait2(100);


  msg.length = 5;
  msg.fcfhi = 0x02;
  msg.fcflo = 0x00;
  msg.dsn = dsn;


  RF230RadioRawM$HPLRF230$addCRC((uint8_t *)&msg);

  RF230RadioRawM$HPLRF230$writeFrame(5, (uint8_t *)&msg + 1);


  TOSH_SET_RF230_SLP_TR_PIN();
  TOSH_uwait2(1);
  TOSH_CLR_RF230_SLP_TR_PIN();

  ;


  TOSH_uwait2(400);

  ;


  RF230RadioRawM$RF230Control$set_RX_ON();

  return;
}

# 19 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.nc"
inline static uint8_t RF230ControlM$HPLRF230$readReg(uint8_t addr){
#line 19
  unsigned char __nesc_result;
#line 19

#line 19
  __nesc_result = HPLRF230M$HPLRF230$readReg(addr);
#line 19

#line 19
  return __nesc_result;
#line 19
}
#line 19
# 408 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static inline void RF230ControlM$RF230Control$getRSSIandCRC(bool *crcValid, uint8_t *rssi)
#line 408
{
  uint8_t value;

  value = RF230ControlM$HPLRF230$readReg(0x06);

  if (value & 0x80) {
#line 413
    *crcValid = TRUE;
    }
  else {
#line 413
    *crcValid = FALSE;
    }
  *rssi = RF230ControlM$HPLRF230$readReg(0x07);
}

# 74 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Control.nc"
inline static void RF230RadioRawM$RF230Control$getRSSIandCRC(bool *crcValid, uint8_t *rssi){
#line 74
  RF230ControlM$RF230Control$getRSSIandCRC(crcValid, rssi);
#line 74
}
#line 74
# 189 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$RadioSend$sendDone(TOS_MsgPtr msg, result_t success)
#line 189
{
  return AMStandard$reportSendDone(msg, success);
}

# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
inline static result_t RF230RadioM$Send$sendDone(TOS_MsgPtr msg, result_t success){
#line 45
  unsigned char __nesc_result;
#line 45

#line 45
  __nesc_result = AMStandard$RadioSend$sendDone(msg, success);
#line 45

#line 45
  return __nesc_result;
#line 45
}
#line 45
# 91 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioM.nc"
static inline result_t RF230RadioM$PHYSend$sendDone(TOS_MsgPtr msg, result_t succ)
#line 91
{

  return RF230RadioM$Send$sendDone(msg, succ);
}

# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
inline static result_t RF230RadioRawM$Send$sendDone(TOS_MsgPtr msg, result_t success){
#line 45
  unsigned char __nesc_result;
#line 45

#line 45
  __nesc_result = RF230RadioM$PHYSend$sendDone(msg, success);
#line 45

#line 45
  return __nesc_result;
#line 45
}
#line 45
# 577 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static __inline void RF230RadioRawM$completeSend(void )
#line 577
{
  TOS_MsgPtr oldPtr;
  result_t result;

  ;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 583
    {

      if (RF230RadioRawM$radioState == RF230RadioRawM$RF230_RADIO_SEND_DONE) {
          result = SUCCESS;
        }
      else 
#line 587
        {
          result = FAIL;
        }


      RF230RadioRawM$bTxBufLocked = FALSE;
      if (RF230RadioRawM$radioState != RF230RadioRawM$RF230_RADIO_STOP) {
#line 593
        RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_IDLE;
        }
#line 594
      oldPtr = RF230RadioRawM$txbufptr;


      RF230RadioRawM$txbufptr->length = RF230RadioRawM$txbufptr->length - MSG_HEADER_SIZE - MSG_FOOTER_SIZE;
    }
#line 598
    __nesc_atomic_end(__nesc_atomic); }


  RF230RadioRawM$Send$sendDone(oldPtr, result);
}

#line 176
static inline void RF230RadioRawM$handleSendDone(void )
#line 176
{
  RF230RadioRawM$completeSend();
}

# 202 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLTimer0.nc"
static inline void HPLTimer0$Timer0$intDisable(void )
#line 202
{
  * (volatile uint8_t *)0x6E &= ~(1 << 1);
}

# 147 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static void TimerJiffyAsyncM$Timer$intDisable(void ){
#line 147
  HPLTimer0$Timer0$intDisable();
#line 147
}
#line 147
# 110 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerJiffyAsyncM.nc"
static inline result_t TimerJiffyAsyncM$TimerJiffyAsync$stop(void )
{
  /* atomic removed: atomic calls only */
#line 112
  {
    TimerJiffyAsyncM$bSet = 0;
    TimerJiffyAsyncM$Timer$intDisable();
  }
  return SUCCESS;
}

# 18 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerJiffyAsync.nc"
inline static result_t RF230RadioRawM$TimerJiffyAsync$stop(void ){
#line 18
  unsigned char __nesc_result;
#line 18

#line 18
  __nesc_result = TimerJiffyAsyncM$TimerJiffyAsync$stop();
#line 18

#line 18
  return __nesc_result;
#line 18
}
#line 18
# 1034 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static __inline void RF230RadioRawM$stopAllTimers(void )
#line 1034
{
  /* atomic removed: atomic calls only */
#line 1035
  RF230RadioRawM$timerState = RF230RadioRawM$RF230_TIMER_IDLE;
  RF230RadioRawM$TimerJiffyAsync$stop();
}

# 252 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.h"
static __inline uint8_t HPLRF230M$bios_frame_crc_read(uint8_t *d)
{
  uint8_t *data = d;
  uint8_t length = 0;
  uint8_t i = 0;
  uint16_t crc = 0;


  TOSH_CLR_SPI_CS_PIN();

  * (volatile uint8_t *)(0X2E + 0x20) = ((0 << 7) | (0 << 6)) | (1 << 5);
  do {
#line 263
      while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
    }
  while (
#line 263
  0);
  * (volatile uint8_t *)(0X2E + 0x20) = 0;
  do {
#line 265
      while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
    }
  while (
#line 265
  0);
  *data = length = * (volatile uint8_t *)(0X2E + 0x20) + 1;
  i = length - 1;



  if (length > 3 && length <= MSG_HEADER_SIZE + 29 + MSG_FOOTER_SIZE + 1) 
    {

      data++;
      * (volatile uint8_t *)(0X2E + 0x20) = 0;
      do {
#line 276
          while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
        }
      while (
#line 276
      0);
      *data = * (volatile uint8_t *)(0X2E + 0x20);
      i--;
      do 
        {
          * (volatile uint8_t *)(0X2E + 0x20) = 0;
          crc = HPLRF230M$_crc_ccitt_update(crc, *data);
          data++;
          do {
#line 284
              while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
            }
          while (
#line 284
          0);
          *data = * (volatile uint8_t *)(0X2E + 0x20);
        }
      while (--i > 0);
      * (volatile uint8_t *)(0X2E + 0x20) = 0;
      crc = HPLRF230M$_crc_ccitt_update(crc, *data);
      data++;
      do {
#line 291
          while ((* (volatile uint8_t *)(0x2D + 0x20) & (1 << 7)) == 0) ;
        }
      while (
#line 291
      0);
      *data = * (volatile uint8_t *)(0X2E + 0x20);
      if (crc != 0) 
        {
          length = 0;
          *d = 0;
        }
    }
  else 
    {
      length = 0;
      *d = 0;
    }


  TOSH_SET_SPI_CS_PIN();

  return length;
}

# 75 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230M.nc"
static inline uint8_t HPLRF230M$HPLRF230$readFrameCRC(uint8_t *buff)
#line 75
{
  uint8_t length;

  /* atomic removed: atomic calls only */
#line 78
  length = HPLRF230M$bios_frame_crc_read(buff);
  return length;
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.nc"
inline static uint8_t RF230RadioRawM$HPLRF230$readFrameCRC(uint8_t *buff){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = HPLRF230M$HPLRF230$readFrameCRC(buff);
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 1258 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static inline 
#line 1257
void RF230RadioRawM$recvCoordinator$default$startSymbol(
uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff)
#line 1258
{
}

# 15 "/Users/wbennett/opt/MoteWorks/tos/interfaces/RadioCoordinator.nc"
inline static void RF230RadioRawM$recvCoordinator$startSymbol(uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff){
#line 15
  RF230RadioRawM$recvCoordinator$default$startSymbol(bitsPerBlock, offset, msgBuff);
#line 15
}
#line 15
# 763 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static __inline void RF230RadioRawM$handleRxInt(void )
#line 763
{
  bool bBuffLocked = FALSE;
  bool bFinishSend = FALSE;
  bool crcValid;
  uint8_t len;

  /* atomic removed: atomic calls only */
  {
    if ((RF230RadioRawM$bTxBufLocked || RF230RadioRawM$bRxBufLocked) || 
    RF230RadioRawM$radioState == RF230RadioRawM$RF230_RADIO_STOP) {
      bBuffLocked = TRUE;
      }
    else {
#line 774
      RF230RadioRawM$bRxBufLocked = TRUE;
      }
  }
  if (bBuffLocked) {
#line 777
    return;
    }
  ;








  RF230RadioRawM$recvCoordinator$startSymbol(8, 0, RF230RadioRawM$rxbufptr);






  len = RF230RadioRawM$HPLRF230$readFrameCRC((uint8_t *)RF230RadioRawM$rxbufptr);


  if (len == 0 || len < 6) {
      /* atomic removed: atomic calls only */
#line 799
      RF230RadioRawM$bRxBufLocked = FALSE;

      ;

      return;
    }

  ;
  ;


  if ((RF230RadioRawM$rxbufptr->fcfhi & 0x07) == 0x02) {
      /* atomic removed: atomic calls only */





      {
        if (RF230RadioRawM$radioState == RF230RadioRawM$RF230_RADIO_WAIT_ACK && 
        RF230RadioRawM$rxbufptr->dsn == RF230RadioRawM$currentDSN) 
          {
            RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_SEND_DONE;
            RF230RadioRawM$txbufptr->ack = 1;
            RF230RadioRawM$stopAllTimers();
            bFinishSend = TRUE;
            RF230RadioRawM$bRxBufLocked = FALSE;
          }
      }
    }


  if (bFinishSend) {
      if (TOS_post(RF230RadioRawM$handleSendDone) == FAIL) {
#line 832
        RF230RadioRawM$completeSend();
        }
      ;

      return;
    }





  if ((RF230RadioRawM$rxbufptr->fcfhi & 0x07) != 0x01) {
      /* atomic removed: atomic calls only */
#line 844
      RF230RadioRawM$bRxBufLocked = FALSE;

      ;

      return;
    }
#line 864
  RF230RadioRawM$RF230Control$getRSSIandCRC(&crcValid, & RF230RadioRawM$rxbufptr->strength);


  if (RF230RadioRawM$rxbufptr->addr == TOS_LOCAL_ADDRESS && 
  RF230RadioRawM$rxbufptr->group == TOS_AM_GROUP) {

      RF230RadioRawM$handleSendAck(RF230RadioRawM$rxbufptr->dsn);
    }







  RF230RadioRawM$rxbufptr->addr = fromLSB16(RF230RadioRawM$rxbufptr->addr);


  RF230RadioRawM$rxbufptr->crc = TRUE;


  RF230RadioRawM$rxbufptr->lqi = ((uint8_t *)RF230RadioRawM$rxbufptr)[len - 1];



  RF230RadioRawM$rxbufptr->length = RF230RadioRawM$rxbufptr->length - 1 - MSG_HEADER_SIZE - MSG_FOOTER_SIZE;

  TOS_post(RF230RadioRawM$handleRecv);
}

#line 682
static inline void RF230RadioRawM$RF230Interrupts$INT_TRX_Done(void )
#line 682
{
  bool txLocked;
  bool bWakeDone;

  /* atomic removed: atomic calls only */
#line 686
  {
    txLocked = RF230RadioRawM$bTxBufLocked;
    if (RF230RadioRawM$radioState == RF230RadioRawM$RF230_RADIO_SEND_WAKEUP) {
#line 688
      bWakeDone = TRUE;
      }
    else {
#line 689
      bWakeDone = FALSE;
      }
  }
  if (!txLocked) {
#line 692
    RF230RadioRawM$handleRxInt();
    }
  else {
#line 693
    if (bWakeDone) {
#line 693
      RF230RadioRawM$handleWakeInt();
      }
    }
}

# 18 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Interrupts.nc"
inline static void RF230InterruptsM$RF230Interrupts$INT_TRX_Done(void ){
#line 18
  RF230RadioRawM$RF230Interrupts$INT_TRX_Done();
#line 18
}
#line 18
# 132 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioM.nc"
static inline void RF230RadioM$WakeSequence$IncomingPacket(void )
#line 132
{
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/WakeSequence.nc"
inline static void RF230RadioRawM$WakeSequence$IncomingPacket(void ){
#line 46
  RF230RadioM$WakeSequence$IncomingPacket();
#line 46
}
#line 46
# 134 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioM.nc"
static inline void RF230RadioM$WakeSequence$sniffExpired(bool channelClear)
#line 134
{
}

# 55 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/WakeSequence.nc"
inline static void RF230RadioRawM$WakeSequence$sniffExpired(bool channelClear){
#line 55
  RF230RadioM$WakeSequence$sniffExpired(channelClear);
#line 55
}
#line 55
# 611 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static inline void RF230RadioRawM$RF230Interrupts$INT_RX_Start(void )
#line 611
{
  bool bSniffExpired = FALSE;

  /* atomic removed: atomic calls only */
#line 614
  {
    if (RF230RadioRawM$timerState == RF230RadioRawM$RF230_TIMER_SNIFF) {
        RF230RadioRawM$stopAllTimers();
        bSniffExpired = TRUE;
      }
  }

  if (bSniffExpired) {
#line 621
    RF230RadioRawM$WakeSequence$sniffExpired(FALSE);
    }
  else {
#line 622
    RF230RadioRawM$WakeSequence$IncomingPacket();
    }
}

# 16 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Interrupts.nc"
inline static void RF230InterruptsM$RF230Interrupts$INT_RX_Start(void ){
#line 16
  RF230RadioRawM$RF230Interrupts$INT_RX_Start();
#line 16
}
#line 16
# 19 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230.nc"
inline static uint8_t RF230InterruptsM$HPLRF230$readReg(uint8_t addr){
#line 19
  unsigned char __nesc_result;
#line 19

#line 19
  __nesc_result = HPLRF230M$HPLRF230$readReg(addr);
#line 19

#line 19
  return __nesc_result;
#line 19
}
#line 19
# 74 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230InterruptsM.nc"
static inline void RF230InterruptsM$TimerCapture$captured(uint16_t time)
#line 74
{









  volatile uint8_t irqStatus;

  irqStatus = RF230InterruptsM$HPLRF230$readReg(0x0f);
  irqStatus &= (0x40 | 0x08) | 0x04;






  if (irqStatus & 0x04) {
      RF230InterruptsM$RF230Interrupts$INT_RX_Start();
    }


  if (irqStatus & 0x08) {
      RF230InterruptsM$RF230Interrupts$INT_TRX_Done();
    }


  if (irqStatus & 0x40) {
      RF230InterruptsM$RF230Interrupts$INT_TRX_UnderRun();
    }





  return;
}

# 60 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerCapture.nc"
inline static void HPLTimer1M$CaptureT1$captured(uint16_t time){
#line 60
  RF230InterruptsM$TimerCapture$captured(time);
#line 60
}
#line 60
# 136 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$SendMsg$default$sendDone(uint8_t id, TOS_MsgPtr msg, result_t success)
#line 136
{
  return SUCCESS;
}

# 27 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendMsg.nc"
inline static result_t AMStandard$SendMsg$sendDone(uint8_t arg_0x101347cb8, TOS_MsgPtr msg, result_t success){
#line 27
  unsigned char __nesc_result;
#line 27

#line 27
    __nesc_result = AMStandard$SendMsg$default$sendDone(arg_0x101347cb8, msg, success);
#line 27

#line 27
  return __nesc_result;
#line 27
}
#line 27
# 139 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$default$sendDone(void )
#line 139
{
  return SUCCESS;
}

#line 44
inline static result_t AMStandard$sendDone(void ){
#line 44
  unsigned char __nesc_result;
#line 44

#line 44
  __nesc_result = AMStandard$default$sendDone();
#line 44

#line 44
  return __nesc_result;
#line 44
}
#line 44
# 240 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLTimer1M.nc"
static inline uint16_t HPLTimer1M$CaptureT1$getEvent(void )
#line 240
{



  return * (volatile uint16_t *)& * (volatile uint16_t *)0x86;
}

# 80 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline uint8_t HPLClock$Clock$getInterval(void )
#line 80
{
  return * (volatile uint8_t *)0xB3;
}

# 100 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static uint8_t TimerM$Clock$getInterval(void ){
#line 100
  unsigned char __nesc_result;
#line 100

#line 100
  __nesc_result = HPLClock$Clock$getInterval();
#line 100

#line 100
  return __nesc_result;
#line 100
}
#line 100
# 38 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static inline void add_time_millis(uint32_t time_to_add)
#line 38
{



  sys_time += time_to_add;
}

# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
inline static uint8_t TimerM$PowerManagement$adjustPower(void ){
#line 19
  unsigned char __nesc_result;
#line 19

#line 19
  __nesc_result = HPLPowerManagementM$PowerManagement$adjustPower();
#line 19

#line 19
  return __nesc_result;
#line 19
}
#line 19
# 70 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline void HPLClock$Clock$setInterval(uint8_t value)
#line 70
{
  * (volatile uint8_t *)0xB3 = value;
}

# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static void TimerM$Clock$setInterval(uint8_t value){
#line 84
  HPLClock$Clock$setInterval(value);
#line 84
}
#line 84
# 136 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline uint8_t HPLClock$Clock$readCounter(void )
#line 136
{
  return * (volatile uint8_t *)0xB2;
}

# 132 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static uint8_t TimerM$Clock$readCounter(void ){
#line 132
  unsigned char __nesc_result;
#line 132

#line 132
  __nesc_result = HPLClock$Clock$readCounter();
#line 132

#line 132
  return __nesc_result;
#line 132
}
#line 132
# 111 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
inline static void TimerM$adjustInterval(void )
#line 111
{
  uint8_t i;
#line 112
  uint8_t val = TimerM$maxTimerInterval;

#line 113
  if (TimerM$mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM$mState & (0x1L << i) && TimerM$mTimerList[i].ticksLeft < val) {
              val = TimerM$mTimerList[i].ticksLeft;
            }
        }
#line 130
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 130
        {
          i = TimerM$Clock$readCounter() + 3;
          if (val < i) {
              val = i;
            }
          TimerM$mInterval = val;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
        }
#line 138
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 141
        {
          TimerM$mInterval = TimerM$maxTimerInterval;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
        }
#line 145
        __nesc_atomic_end(__nesc_atomic); }
    }
  TimerM$PowerManagement$adjustPower();
}

#line 168
static inline void TimerM$enqueue(uint8_t value)
#line 168
{
  if (TimerM$queue_tail == NUM_TIMERS - 1) {
    TimerM$queue_tail = -1;
    }
#line 171
  TimerM$queue_tail++;
  TimerM$queue_size++;
  TimerM$queue[(uint8_t )TimerM$queue_tail] = value;
}

# 130 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$ActivityTimer$fired(void )
#line 130
{
  AMStandard$lastCount = AMStandard$counter;
  AMStandard$counter = 0;
  return SUCCESS;
}

# 264 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static inline result_t RF230ControlM$initTimer$fired(void )
#line 264
{









  RF230ControlM$RF230Control$resetRadio();









  RF230ControlM$InterruptControl$start();


  RF230ControlM$RF230Control$set_RX_ON();
  TOSH_uwait2(200);




  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 293
    RF230ControlM$controlState = RF230ControlM$RF230_CTL_START_DONE;
#line 293
    __nesc_atomic_end(__nesc_atomic); }


  RF230ControlM$SplitControl$startDone();

  return SUCCESS;
}

# 108 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioM.nc"
static inline result_t RF230RadioM$SleepTimer$fired(void )
#line 108
{
  return SUCCESS;
}

#line 102
static inline result_t RF230RadioM$IntervalTimer$fired(void )
#line 102
{
  return SUCCESS;
}

# 164 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline result_t TimerM$Timer$default$fired(uint8_t id)
#line 164
{
  return SUCCESS;
}

# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
inline static result_t TimerM$Timer$fired(uint8_t arg_0x1016dfd38){
#line 51
  unsigned char __nesc_result;
#line 51

#line 51
  switch (arg_0x1016dfd38) {
#line 51
    case 0U:
#line 51
      __nesc_result = AMStandard$ActivityTimer$fired();
#line 51
      break;
#line 51
    case 1U:
#line 51
      __nesc_result = RF230RadioM$IntervalTimer$fired();
#line 51
      break;
#line 51
    case 2U:
#line 51
      __nesc_result = RF230RadioM$SleepTimer$fired();
#line 51
      break;
#line 51
    case 3U:
#line 51
      __nesc_result = RF230ControlM$initTimer$fired();
#line 51
      break;
#line 51
    default:
#line 51
      __nesc_result = TimerM$Timer$default$fired(arg_0x1016dfd38);
#line 51
      break;
#line 51
    }
#line 51

#line 51
  return __nesc_result;
#line 51
}
#line 51
# 176 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline uint8_t TimerM$dequeue(void )
#line 176
{
  if (TimerM$queue_size == 0) {
    return NUM_TIMERS;
    }
#line 179
  if (TimerM$queue_head == NUM_TIMERS - 1) {
    TimerM$queue_head = -1;
    }
#line 181
  TimerM$queue_head++;
  TimerM$queue_size--;
  return TimerM$queue[(uint8_t )TimerM$queue_head];
}

static inline void TimerM$signalOneTimer(void )
#line 186
{
  uint8_t itimer = TimerM$dequeue();

#line 188
  if (itimer < NUM_TIMERS) {
    TimerM$Timer$fired(itimer);
    }
}

#line 192
static inline void TimerM$HandleFire(void )
#line 192
{
  uint8_t i;
  uint16_t int_out;


  TimerM$setIntervalFlag = 1;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 200
    {
      int_out = TimerM$interval_outstanding;
      TimerM$interval_outstanding = 0;
    }
#line 203
    __nesc_atomic_end(__nesc_atomic); }
  if (TimerM$mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM$mState & (0x1L << i)) {
              TimerM$mTimerList[i].ticksLeft -= int_out;
              if (TimerM$mTimerList[i].ticksLeft <= 2) {


                  if (TOS_post(TimerM$signalOneTimer)) {
                      if (TimerM$mTimerList[i].type == TIMER_REPEAT) {
                          TimerM$mTimerList[i].ticksLeft += TimerM$mTimerList[i].ticks;
                        }
                      else 
#line 214
                        {
                          TimerM$mState &= ~(0x1L << i);
                        }
                      TimerM$enqueue(i);
                    }
                  else {
                      {
                      }
#line 220
                      ;


                      TimerM$mTimerList[i].ticksLeft = TimerM$mInterval;
                    }
                }
            }
        }
    }


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 231
    int_out = TimerM$interval_outstanding;
#line 231
    __nesc_atomic_end(__nesc_atomic); }
  if (int_out == 0) {
    TimerM$adjustInterval();
    }
}

static inline result_t TimerM$Clock$fire(void )
#line 237
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 238
    {



      if (TimerM$interval_outstanding == 0) {
          TOS_post(TimerM$HandleFire);
        }
      else 
        {
        }
#line 246
      ;

      TimerM$interval_outstanding += TimerM$Clock$getInterval() + 1;


      add_time_millis(TimerM$Clock$getInterval());
    }
#line 252
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static result_t HPLClock$Clock$fire(void ){
#line 159
  unsigned char __nesc_result;
#line 159

#line 159
  __nesc_result = TimerM$Clock$fire();
#line 159

#line 159
  return __nesc_result;
#line 159
}
#line 159
#line 127
inline static result_t TimerJiffyAsyncM$Timer$setIntervalAndScale(uint8_t interval, uint8_t scale){
#line 127
  unsigned char __nesc_result;
#line 127

#line 127
  __nesc_result = HPLTimer0$Timer0$setIntervalAndScale(interval, scale);
#line 127

#line 127
  return __nesc_result;
#line 127
}
#line 127
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
inline static uint8_t TimerJiffyAsyncM$PowerManagement$adjustPower(void ){
#line 19
  unsigned char __nesc_result;
#line 19

#line 19
  __nesc_result = HPLPowerManagementM$PowerManagement$adjustPower();
#line 19

#line 19
  return __nesc_result;
#line 19
}
#line 19
# 22 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerJiffyAsync.nc"
inline static result_t TimerJiffyAsyncM$TimerJiffyAsync$fired(void ){
#line 22
  unsigned char __nesc_result;
#line 22

#line 22
  __nesc_result = RF230RadioRawM$TimerJiffyAsync$fired();
#line 22

#line 22
  return __nesc_result;
#line 22
}
#line 22
# 68 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerJiffyAsyncM.nc"
static inline result_t TimerJiffyAsyncM$Timer$fire(void )
#line 68
{
  uint16_t localjiffy;

#line 70
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 70
    localjiffy = TimerJiffyAsyncM$jiffy;
#line 70
    __nesc_atomic_end(__nesc_atomic); }
  if (localjiffy < 0xFF) {
      TimerJiffyAsyncM$Timer$intDisable();
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 73
        TimerJiffyAsyncM$bSet = 0;
#line 73
        __nesc_atomic_end(__nesc_atomic); }
      TimerJiffyAsyncM$TimerJiffyAsync$fired();
      TimerJiffyAsyncM$PowerManagement$adjustPower();
    }
  else {

      localjiffy = localjiffy >> 8;
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 80
        TimerJiffyAsyncM$jiffy = localjiffy;
#line 80
        __nesc_atomic_end(__nesc_atomic); }
      TimerJiffyAsyncM$Timer$setIntervalAndScale(localjiffy, 0x4);
    }
  return SUCCESS;
}

# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static result_t HPLTimer0$Timer0$fire(void ){
#line 159
  unsigned char __nesc_result;
#line 159

#line 159
  __nesc_result = TimerJiffyAsyncM$Timer$fire();
#line 159

#line 159
  return __nesc_result;
#line 159
}
#line 159
# 16 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerJiffyAsync.nc"
inline static result_t RF230RadioRawM$TimerJiffyAsync$setOneShot(uint32_t jiffy){
#line 16
  unsigned char __nesc_result;
#line 16

#line 16
  __nesc_result = TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(jiffy);
#line 16

#line 16
  return __nesc_result;
#line 16
}
#line 16
# 973 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static __inline result_t RF230RadioRawM$setAckTimer(uint16_t jiffy)
#line 973
{
  result_t result;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 977
    {
      if (RF230RadioRawM$timerState == RF230RadioRawM$RF230_TIMER_IDLE) {
          result = SUCCESS;
          RF230RadioRawM$timerState = RF230RadioRawM$RF230_TIMER_ACK;
          RF230RadioRawM$TimerJiffyAsync$setOneShot(jiffy);
        }
      else 
#line 982
        {
          result = FAIL;
        }
    }
#line 985
    __nesc_atomic_end(__nesc_atomic); }

  return result;
}

#line 726
static __inline void RF230RadioRawM$handleTxInt(result_t status)
#line 726
{


  RF230RadioRawM$RF230Control$set_RX_ON();


  if (status == FAIL) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 733
        RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_SEND_FAIL;
#line 733
        __nesc_atomic_end(__nesc_atomic); }
      if (TOS_post(RF230RadioRawM$handleSendDone) == FAIL) {
#line 734
        RF230RadioRawM$completeSend();
        }
#line 735
      return;
    }


  if (RF230RadioRawM$bAckEnabled && RF230RadioRawM$txbufptr->addr != TOS_BCAST_ADDR) {


      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 742
        {
          RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_WAIT_ACK;
          RF230RadioRawM$bTxBufLocked = FALSE;
        }
#line 745
        __nesc_atomic_end(__nesc_atomic); }

      if (RF230RadioRawM$setAckTimer(2 * 20) == FAIL) {

          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 749
            RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_SEND_DONE;
#line 749
            __nesc_atomic_end(__nesc_atomic); }
          if (TOS_post(RF230RadioRawM$handleSendDone) == FAIL) {
#line 750
            RF230RadioRawM$completeSend();
            }
        }
    }
  else 
#line 753
    {

      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 755
        RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_SEND_DONE;
#line 755
        __nesc_atomic_end(__nesc_atomic); }
      if (TOS_post(RF230RadioRawM$handleSendDone) == FAIL) {
#line 756
        RF230RadioRawM$completeSend();
        }
    }
}

#line 1014
static __inline result_t RF230RadioRawM$setSendTimer(uint16_t jiffy)
#line 1014
{
  result_t result;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 1018
    {
      if (RF230RadioRawM$timerState == RF230RadioRawM$RF230_TIMER_IDLE) {
          result = SUCCESS;
          RF230RadioRawM$timerState = RF230RadioRawM$RF230_TIMER_SEND;
          RF230RadioRawM$TimerJiffyAsync$setOneShot(jiffy);
        }
      else 
#line 1023
        {
          result = FAIL;
        }
    }
#line 1026
    __nesc_atomic_end(__nesc_atomic); }

  return result;
}

#line 1248
static inline 
#line 1247
void RF230RadioRawM$sendCoordinator$default$startSymbol(
uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff)
#line 1248
{
}

# 15 "/Users/wbennett/opt/MoteWorks/tos/interfaces/RadioCoordinator.nc"
inline static void RF230RadioRawM$sendCoordinator$startSymbol(uint8_t bitsPerBlock, uint8_t offset, TOS_MsgPtr msgBuff){
#line 15
  RF230RadioRawM$sendCoordinator$default$startSymbol(bitsPerBlock, offset, msgBuff);
#line 15
}
#line 15
# 204 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static inline void RF230RadioRawM$sigJiffyTimer(void )
#line 204
{
  RF230RadioRawM$TimerJiffyAsync$fired();
}

#line 944
static __inline result_t RF230RadioRawM$setBackoffTimer(uint16_t jiffy)
#line 944
{
  result_t result;

  if (jiffy == 0xffff) {
#line 947
    return FAIL;
    }

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 950
    {
      if (RF230RadioRawM$timerState == RF230RadioRawM$RF230_TIMER_IDLE) {

          RF230RadioRawM$timerState = RF230RadioRawM$RF230_TIMER_BACKOFF;


          if (jiffy == 0x0000) {
              result = TOS_post(RF230RadioRawM$sigJiffyTimer);
            }
          else 
#line 958
            {
              RF230RadioRawM$TimerJiffyAsync$setOneShot(jiffy * 16);
              result = SUCCESS;
            }
        }
      else 
#line 962
        {
          result = FAIL;
        }
    }
#line 965
    __nesc_atomic_end(__nesc_atomic); }

  return result;
}

# 49 "/Users/wbennett/opt/MoteWorks/tos/system/RandomLFSR.nc"
static inline uint16_t RandomLFSR$Random$rand(void )
#line 49
{
  bool endbit;
  uint16_t tmpShiftReg;

#line 52
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 52
    {
      tmpShiftReg = RandomLFSR$shiftReg;
      endbit = (tmpShiftReg & 0x8000) != 0;
      tmpShiftReg <<= 1;
      if (endbit) {
        tmpShiftReg ^= 0x100b;
        }
#line 58
      tmpShiftReg++;
      RandomLFSR$shiftReg = tmpShiftReg;
      tmpShiftReg = tmpShiftReg ^ RandomLFSR$mask;
    }
#line 61
    __nesc_atomic_end(__nesc_atomic); }
  return tmpShiftReg;
}

# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Random.nc"
inline static uint16_t RF230RadioM$Random$rand(void ){
#line 42
  unsigned short __nesc_result;
#line 42

#line 42
  __nesc_result = RandomLFSR$Random$rand();
#line 42

#line 42
  return __nesc_result;
#line 42
}
#line 42
# 125 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioM.nc"
static inline int16_t RF230RadioM$MacBackoff$congestionBackoff(TOS_MsgPtr m)
#line 125
{
  return (RF230RadioM$Random$rand() & 0xF) + 1;
}

# 25 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/MacBackoff.nc"
inline static int16_t RF230RadioRawM$MacBackoff$congestionBackoff(TOS_MsgPtr m){
#line 25
  short __nesc_result;
#line 25

#line 25
  __nesc_result = RF230RadioM$MacBackoff$congestionBackoff(m);
#line 25

#line 25
  return __nesc_result;
#line 25
}
#line 25
# 552 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static __inline void RF230RadioRawM$doCongestionBackOff(void )
#line 552
{
  int16_t backoffValue;
  result_t result;



  backoffValue = RF230RadioRawM$MacBackoff$congestionBackoff(RF230RadioRawM$txbufptr);
  result = RF230RadioRawM$setBackoffTimer(backoffValue);


  if (result == FAIL) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 563
        RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_SEND_FAIL;
#line 563
        __nesc_atomic_end(__nesc_atomic); }
      if (TOS_post(RF230RadioRawM$handleSendDone) == FAIL) {
#line 564
        RF230RadioRawM$completeSend();
        }
    }
  else 
#line 565
    {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 566
        RF230RadioRawM$sendRetries++;
#line 566
        __nesc_atomic_end(__nesc_atomic); }
    }
}

# 439 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static inline bool RF230ControlM$RF230Control$CCA(void )
#line 439
{
  uint8_t value;

  ;

  RF230ControlM$HPLRF230$bitWrite(0x08, 0x80, 7, 1);
  TOSH_uwait2(200);

  value = RF230ControlM$HPLRF230$readReg(0x01);

  ;

  if (value & 0x80) {

      if (value & 0x40) {
#line 453
        return TRUE;
        }
      else {
#line 453
        return FALSE;
        }
    }
  else 
#line 455
    {




      RF230ControlM$RF230Control$resetRadio();
      RF230ControlM$RF230Control$set_RX_ON();
      TOSH_uwait2(200);


      return FALSE;
    }
}

# 90 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230Control.nc"
inline static bool RF230RadioRawM$RF230Control$CCA(void ){
#line 90
  unsigned char __nesc_result;
#line 90

#line 90
  __nesc_result = RF230ControlM$RF230Control$CCA();
#line 90

#line 90
  return __nesc_result;
#line 90
}
#line 90
# 470 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static __inline void RF230RadioRawM$tryToSend(void )
#line 470
{

  bool bNoSend;
  bool bRadioStopped = FALSE;
  uint8_t transitTime;

  ;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 479
    {


      if (RF230RadioRawM$radioState == RF230RadioRawM$RF230_RADIO_STOP) {
#line 482
        bRadioStopped = TRUE;
        }

      if (RF230RadioRawM$bRxBufLocked) {

          bNoSend = TRUE;
        }
      else 
#line 488
        {

          RF230RadioRawM$bTxBufLocked = TRUE;
          bNoSend = FALSE;
        }
    }
#line 493
    __nesc_atomic_end(__nesc_atomic); }


  if (bRadioStopped) {
      if (TOS_post(RF230RadioRawM$handleSendDone) == FAIL) {
#line 497
        RF230RadioRawM$completeSend();
        }
#line 498
      return;
    }


  if (bNoSend) {
      RF230RadioRawM$doCongestionBackOff();
      return;
    }


  if (!RF230RadioRawM$RF230Control$CCA()) {
      ;
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 510
        RF230RadioRawM$bTxBufLocked = FALSE;
#line 510
        __nesc_atomic_end(__nesc_atomic); }
      RF230RadioRawM$doCongestionBackOff();
      return;
    }


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 516
    RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_SEND_TX;
#line 516
    __nesc_atomic_end(__nesc_atomic); }
  RF230RadioRawM$RF230Control$set_PLL_ON();


  RF230RadioRawM$sendCoordinator$startSymbol(8, 0, RF230RadioRawM$txbufptr);

  RF230RadioRawM$HPLRF230$addCRC((uint8_t *)RF230RadioRawM$txbufptr);
  RF230RadioRawM$HPLRF230$writeFrame(RF230RadioRawM$txlength, (uint8_t *)RF230RadioRawM$txbufptr + 1);


  TOSH_SET_RF230_SLP_TR_PIN();
  TOSH_uwait2(1);
  TOSH_CLR_RF230_SLP_TR_PIN();








  ;


  transitTime = RF230RadioRawM$txlength + 1 + 5 + 1;
  if (RF230RadioRawM$setSendTimer(transitTime) == FAIL) {
      TOSH_uwait2(transitTime << 5);
      RF230RadioRawM$handleTxInt(SUCCESS);
    }

  return;
}

# 50 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/crc.h"
static inline uint16_t crcByte(uint16_t oldCrc, uint8_t byte)
{

  uint16_t *table = crcTable;
  uint16_t newCrc;

   __asm ("eor %1,%B3\n"
  "\tlsl %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tadd %A2, %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tlpm\n"
  "\tmov %B0, %A3\n"
  "\tmov %A0, r0\n"
  "\tadiw r30,1\n"
  "\tlpm\n"
  "\teor %B0, r0" : 
  "=r"(newCrc), "+r"(byte), "+z"(table) : "r"(oldCrc));
  return newCrc;
}

# 217 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
static inline void FramerM$PacketUnknown(void )
#line 217
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 218
    {
      FramerM$gFlags |= FramerM$FLAGS_UNKNOWN;
    }
#line 220
    __nesc_atomic_end(__nesc_atomic); }

  FramerM$StartTx();
}

# 225 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline TOS_MsgPtr AMStandard$UARTReceive$receive(TOS_MsgPtr packet)
#line 225
{


  packet->group = TOS_AM_GROUP;
  return received(packet);
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
inline static TOS_MsgPtr FramerAckM$ReceiveCombined$receive(TOS_MsgPtr m){
#line 53
  struct TOS_Msg *__nesc_result;
#line 53

#line 53
  __nesc_result = AMStandard$UARTReceive$receive(m);
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 56 "/Users/wbennett/opt/MoteWorks/tos/system/FramerAckM.nc"
static inline TOS_MsgPtr FramerAckM$ReceiveMsg$receive(TOS_MsgPtr Msg)
#line 56
{
  TOS_MsgPtr pBuf;

  pBuf = FramerAckM$ReceiveCombined$receive(Msg);

  return pBuf;
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
inline static TOS_MsgPtr FramerM$ReceiveMsg$receive(TOS_MsgPtr m){
#line 53
  struct TOS_Msg *__nesc_result;
#line 53

#line 53
  __nesc_result = FramerAckM$ReceiveMsg$receive(m);
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 346 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
static inline result_t FramerM$TokenReceiveMsg$ReflectToken(uint8_t Token)
#line 346
{
  result_t Result = SUCCESS;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 349
    {
      if (!(FramerM$gFlags & FramerM$FLAGS_TOKENPEND)) {
          FramerM$gFlags |= FramerM$FLAGS_TOKENPEND;
          FramerM$gTxTokenBuf = Token;
        }
      else {
          Result = FAIL;
        }
    }
#line 357
    __nesc_atomic_end(__nesc_atomic); }

  if (Result == SUCCESS) {
      Result = FramerM$StartTx();
    }

  return Result;
}

# 59 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
inline static result_t FramerAckM$TokenReceiveMsg$ReflectToken(uint8_t Token){
#line 59
  unsigned char __nesc_result;
#line 59

#line 59
  __nesc_result = FramerM$TokenReceiveMsg$ReflectToken(Token);
#line 59

#line 59
  return __nesc_result;
#line 59
}
#line 59
# 39 "/Users/wbennett/opt/MoteWorks/tos/system/FramerAckM.nc"
static inline void FramerAckM$SendAckTask(void )
#line 39
{

  FramerAckM$TokenReceiveMsg$ReflectToken(FramerAckM$gTokenBuf);
}

static inline TOS_MsgPtr FramerAckM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t token)
#line 44
{
  TOS_MsgPtr pBuf;

  FramerAckM$gTokenBuf = token;

  TOS_post(FramerAckM$SendAckTask);

  pBuf = FramerAckM$ReceiveCombined$receive(Msg);

  return pBuf;
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/TokenReceiveMsg.nc"
inline static TOS_MsgPtr FramerM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t Token){
#line 46
  struct TOS_Msg *__nesc_result;
#line 46

#line 46
  __nesc_result = FramerAckM$TokenReceiveMsg$receive(Msg, Token);
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 225 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
static inline void FramerM$PacketRcvd(void )
#line 225
{
  FramerM$MsgRcvEntry_t *pRcv = &FramerM$gMsgRcvTbl[FramerM$gRxTailIndex];
  TOS_MsgPtr pBuf = pRcv->pMsg;



  if (pRcv->Length >= 5) {



      switch (pRcv->Proto) {
          case FramerM$PROTO_ACK: 
            break;
          case FramerM$PROTO_PACKET_ACK: 
            pBuf->crc = 1;
          pBuf = FramerM$TokenReceiveMsg$receive(pBuf, pRcv->Token);
          break;
          case FramerM$PROTO_PACKET_NOACK: 
            pBuf->crc = 1;
          pBuf = FramerM$ReceiveMsg$receive(pBuf);
          break;
          default: 
            FramerM$gTxUnknownBuf = pRcv->Proto;
          TOS_post(FramerM$PacketUnknown);
          break;
        }
    }

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 253
    {
      if (pBuf) {
          pRcv->pMsg = pBuf;
        }
      pRcv->Length = 0;
      pRcv->Token = 0;
      FramerM$gRxTailIndex++;
      FramerM$gRxTailIndex %= FramerM$HDLC_QUEUESIZE;
    }
#line 261
    __nesc_atomic_end(__nesc_atomic); }
}

#line 366
static inline result_t FramerM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength)
#line 366
{

  switch (FramerM$gRxState) {

      case FramerM$RXSTATE_NOSYNC: 
        if (data == FramerM$HDLC_FLAG_BYTE && FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length == 0) {

            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
            FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
            FramerM$gpRxBuf = (uint8_t *)FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].pMsg;
            FramerM$gRxState = FramerM$RXSTATE_PROTO;
          }
      break;

      case FramerM$RXSTATE_PROTO: 
        if (data == FramerM$HDLC_FLAG_BYTE) {
            break;
          }
      FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Proto = data;
      FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, data);
      switch (data) {
          case FramerM$PROTO_PACKET_ACK: 
            FramerM$gRxState = FramerM$RXSTATE_TOKEN;
          break;
          case FramerM$PROTO_PACKET_NOACK: 
            FramerM$gRxState = FramerM$RXSTATE_INFO;
          break;
          default: 
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          break;
        }
      break;

      case FramerM$RXSTATE_TOKEN: 
        if (data == FramerM$HDLC_FLAG_BYTE) {
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          }
        else {
#line 403
          if (data == FramerM$HDLC_CTLESC_BYTE) {
              FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0x20;
            }
          else {
              FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token ^= data;
              FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token);
              FramerM$gRxState = FramerM$RXSTATE_INFO;
            }
          }
#line 411
      break;


      case FramerM$RXSTATE_INFO: 
        if (FramerM$gRxByteCnt > FramerM$HDLC_MTU) {
            FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          }
        else {
#line 421
          if (data == FramerM$HDLC_CTLESC_BYTE) {
              FramerM$gRxState = FramerM$RXSTATE_ESC;
            }
          else {
#line 424
            if (data == FramerM$HDLC_FLAG_BYTE) {
                if (FramerM$gRxByteCnt >= 2) {

                    uint16_t usRcvdCRC = FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt - 1)] & 0xff;

#line 428
                    usRcvdCRC = (usRcvdCRC << 8) | (FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt - 2)] & 0xff);






                    if (usRcvdCRC == FramerM$gRxRunningCRC) {
                        FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = FramerM$gRxByteCnt - 2;
                        TOS_post(FramerM$PacketRcvd);
                        FramerM$gRxHeadIndex++;
#line 438
                        FramerM$gRxHeadIndex %= FramerM$HDLC_QUEUESIZE;
                      }
                    else {
                        FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
                        FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
                        FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
                        FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
                        break;
                      }
                    if (FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length == 0) {
                        FramerM$gpRxBuf = (uint8_t *)FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].pMsg;
                        FramerM$gRxState = FramerM$RXSTATE_PROTO;
                      }
                    else {
                        FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
                      }
                  }
                else {
                    FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
                    FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
                    FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
                  }
                FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
              }
            else {
                FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt)] = data;
                if (FramerM$gRxByteCnt >= 2) {
                    FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt - 2)]);
                  }
                FramerM$gRxByteCnt++;
              }
            }
          }
#line 469
      break;

      case FramerM$RXSTATE_ESC: 
        if (data == FramerM$HDLC_FLAG_BYTE) {

            FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          }
        else {
            data = data ^ 0x20;
            FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt)] = data;
            if (FramerM$gRxByteCnt >= 2) {
                FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt - 2)]);
              }
            FramerM$gRxByteCnt++;
            FramerM$gRxState = FramerM$RXSTATE_INFO;
          }
      break;

      default: 
        FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
      break;
    }

  return SUCCESS;
}

# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t UARTM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength){
#line 45
  unsigned char __nesc_result;
#line 45

#line 45
  __nesc_result = FramerM$ByteComm$rxByteReady(data, error, strength);
#line 45

#line 45
  return __nesc_result;
#line 45
}
#line 45
# 57 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
static inline result_t UARTM$HPLUART$get(uint8_t data)
#line 57
{




  UARTM$ByteComm$rxByteReady(data, 0, 0);
  {
  }
#line 63
  ;
  return SUCCESS;
}

# 66 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t HPLUART0M$UART$get(uint8_t data){
#line 66
  unsigned char __nesc_result;
#line 66

#line 66
  __nesc_result = UARTM$HPLUART$get(data);
#line 66

#line 66
  return __nesc_result;
#line 66
}
#line 66
# 113 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
static inline result_t HPLUART0M$UART$put(uint8_t data)
#line 113
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 114
    {
      * (volatile uint8_t *)0XC6 = data;
      * (volatile uint8_t *)0xC0 |= 1 << 6;
    }
#line 117
    __nesc_atomic_end(__nesc_atomic); }

  return SUCCESS;
}

# 58 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t UARTM$HPLUART$put(uint8_t data){
#line 58
  unsigned char __nesc_result;
#line 58

#line 58
  __nesc_result = HPLUART0M$UART$put(data);
#line 58

#line 58
  return __nesc_result;
#line 58
}
#line 58
# 186 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static inline result_t AMStandard$UARTSend$sendDone(TOS_MsgPtr msg, result_t success)
#line 186
{
  return AMStandard$reportSendDone(msg, success);
}

# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/BareSendMsg.nc"
inline static result_t FramerM$BareSendMsg$sendDone(TOS_MsgPtr msg, result_t success){
#line 45
  unsigned char __nesc_result;
#line 45

#line 45
  __nesc_result = AMStandard$UARTSend$sendDone(msg, success);
#line 45

#line 45
  return __nesc_result;
#line 45
}
#line 45
# 34 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t FramerM$ByteComm$txByte(uint8_t data){
#line 34
  unsigned char __nesc_result;
#line 34

#line 34
  __nesc_result = UARTM$ByteComm$txByte(data);
#line 34

#line 34
  return __nesc_result;
#line 34
}
#line 34
# 511 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
static inline result_t FramerM$ByteComm$txByteReady(bool LastByteSuccess)
#line 511
{
  result_t TxResult = SUCCESS;
  uint8_t nextByte;

  if (LastByteSuccess != 1) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 516
        FramerM$gTxState = FramerM$TXSTATE_ERROR;
#line 516
        __nesc_atomic_end(__nesc_atomic); }
      TOS_post(FramerM$PacketSent);
      return SUCCESS;
    }

  switch (FramerM$gTxState) {

      case FramerM$TXSTATE_PROTO: 
        FramerM$gTxState = FramerM$TXSTATE_INFO;
      FramerM$gTxRunningCRC = crcByte(FramerM$gTxRunningCRC, FramerM$gTxProto);
      TxResult = FramerM$ByteComm$txByte(FramerM$gTxProto);
      break;

      case FramerM$TXSTATE_INFO: 
        if (FramerM$gTxProto == FramerM$PROTO_ACK) {
          nextByte = FramerM$gpTxBuf[0];
          }
        else {
#line 533
          nextByte = FramerM$gpTxBuf[FramerM$gTxByteCnt];
          }
#line 534
      FramerM$gTxRunningCRC = crcByte(FramerM$gTxRunningCRC, nextByte);
      FramerM$gTxByteCnt++;

      if (FramerM$gTxByteCnt == 10) {
        FramerM$gTxByteCnt = 0;
        }
#line 539
      if (FramerM$gTxByteCnt == 1) {
        FramerM$gTxByteCnt = 10;
        }
      if (FramerM$gTxByteCnt >= FramerM$gTxLength) {
          FramerM$gTxState = FramerM$TXSTATE_FCS1;
        }

      TxResult = FramerM$TxArbitraryByte(nextByte);
      break;

      case FramerM$TXSTATE_ESC: 

        TxResult = FramerM$ByteComm$txByte(FramerM$gTxEscByte ^ 0x20);
      FramerM$gTxState = FramerM$gPrevTxState;
      break;

      case FramerM$TXSTATE_FCS1: 
        nextByte = (uint8_t )(FramerM$gTxRunningCRC & 0xff);
      FramerM$gTxState = FramerM$TXSTATE_FCS2;
      TxResult = FramerM$TxArbitraryByte(nextByte);
      break;

      case FramerM$TXSTATE_FCS2: 
        nextByte = (uint8_t )((FramerM$gTxRunningCRC >> 8) & 0xff);
      FramerM$gTxState = FramerM$TXSTATE_ENDFLAG;
      TxResult = FramerM$TxArbitraryByte(nextByte);
      break;

      case FramerM$TXSTATE_ENDFLAG: 
        FramerM$gTxState = FramerM$TXSTATE_FINISH;
      TxResult = FramerM$ByteComm$txByte(FramerM$HDLC_FLAG_BYTE);

      break;

      case FramerM$TXSTATE_FINISH: 
        case FramerM$TXSTATE_ERROR: 

          default: 
            break;
    }


  if (TxResult != SUCCESS) {
      FramerM$gTxState = FramerM$TXSTATE_ERROR;
      TOS_post(FramerM$PacketSent);
    }

  return SUCCESS;
}

# 54 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t UARTM$ByteComm$txByteReady(bool success){
#line 54
  unsigned char __nesc_result;
#line 54

#line 54
  __nesc_result = FramerM$ByteComm$txByteReady(success);
#line 54

#line 54
  return __nesc_result;
#line 54
}
#line 54
# 589 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
static inline result_t FramerM$ByteComm$txDone(void )
#line 589
{

  if (FramerM$gTxState == FramerM$TXSTATE_FINISH) {
      FramerM$gTxState = FramerM$TXSTATE_IDLE;
      TOS_post(FramerM$PacketSent);
    }

  return SUCCESS;
}

# 62 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t UARTM$ByteComm$txDone(void ){
#line 62
  unsigned char __nesc_result;
#line 62

#line 62
  __nesc_result = FramerM$ByteComm$txDone();
#line 62

#line 62
  return __nesc_result;
#line 62
}
#line 62
# 67 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
static inline result_t UARTM$HPLUART$putDone(void )
#line 67
{
  bool oldState;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 70
    {
      {
      }
#line 71
      ;
      oldState = UARTM$state;
      UARTM$state = 0;
    }
#line 74
    __nesc_atomic_end(__nesc_atomic); }








  if (oldState) {
      UARTM$ByteComm$txDone();
      UARTM$ByteComm$txByteReady(1);
    }
  return SUCCESS;
}

# 74 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t HPLUART0M$UART$putDone(void ){
#line 74
  unsigned char __nesc_result;
#line 74

#line 74
  __nesc_result = UARTM$HPLUART$putDone();
#line 74

#line 74
  return __nesc_result;
#line 74
}
#line 74
# 170 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
static __inline void __nesc_disable_interrupt()
#line 170
{
   __asm volatile ("cli");}

# 82 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
 bool TOS_post(void (*tp)())
#line 82
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t tmp;



  fInterruptFlags = __nesc_atomic_start();

  tmp = TOSH_sched_free;

  if (TOSH_queue[tmp].tp == (void *)0) {
      TOSH_sched_free = (tmp + 1) & TOSH_TASK_BITMASK;
      TOSH_queue[tmp].tp = tp;
      __nesc_atomic_end(fInterruptFlags);

      return TRUE;
    }
  else {
      __nesc_atomic_end(fInterruptFlags);

      return FALSE;
    }
}

# 34 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
  int main(void )
#line 34
{


  uint8_t local_symbol_ref;

  local_symbol_ref = TOS_PLATFORM;
  local_symbol_ref = TOS_BASE_STATION;
  local_symbol_ref = TOS_DATA_LENGTH;

  local_symbol_ref = TOS_ROUTE_PROTOCOL;








  RealMain$hardwareInit();
  RealMain$Pot$init(10);
  TOSH_sched_init();

  RealMain$StdControl$init();
  RealMain$StdControl$start();
  __nesc_enable_interrupt();

  while (1) {
      TOSH_run_task();
    }
}

# 36 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static result_t LedsC$Leds$init(void )
#line 36
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 37
    {
      LedsC$ledsOn = 0;
      {
      }
#line 39
      ;
      TOSH_MAKE_RED_LED_OUTPUT();
      TOSH_MAKE_YELLOW_LED_OUTPUT();
      TOSH_MAKE_GREEN_LED_OUTPUT();
      TOSH_SET_RED_LED_PIN();
      TOSH_SET_YELLOW_LED_PIN();
      TOSH_SET_GREEN_LED_PIN();
    }
#line 46
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 57 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static result_t TimerM$StdControl$init(void )
#line 57
{

  init_avrtime();
  TimerM$mState = 0;
  TimerM$setIntervalFlag = 0;
  TimerM$queue_head = TimerM$queue_tail = -1;
  TimerM$queue_size = 0;
  TimerM$mScale = 3;
  TimerM$mInterval = TimerM$maxTimerInterval;
  return TimerM$Clock$setRate(TimerM$mInterval, TimerM$mScale);
}

# 286 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
static void FramerM$HDLCInitialize(void )
#line 286
{
  int i;

#line 288
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 288
    {
      for (i = 0; i < FramerM$HDLC_QUEUESIZE; i++) {
          FramerM$gMsgRcvTbl[i].pMsg = &FramerM$gMsgRcvBuf[i];
          FramerM$gMsgRcvTbl[i].Length = 0;
          FramerM$gMsgRcvTbl[i].Token = 0;
        }
      FramerM$gTxState = FramerM$TXSTATE_IDLE;
      FramerM$gTxByteCnt = 0;
      FramerM$gTxLength = 0;
      FramerM$gTxRunningCRC = 0;
      FramerM$gpTxMsg = (void *)0;

      FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
      FramerM$gRxHeadIndex = 0;
      FramerM$gRxTailIndex = 0;
      FramerM$gRxByteCnt = 0;
      FramerM$gRxRunningCRC = 0;
      FramerM$gpRxBuf = (uint8_t *)FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].pMsg;
    }
#line 306
    __nesc_atomic_end(__nesc_atomic); }
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/system/RandomLFSR.nc"
static result_t RandomLFSR$Random$init(void )
#line 38
{
  {
  }
#line 39
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 40
    {
      RandomLFSR$shiftReg = 119 * 119 * (TOS_LOCAL_ADDRESS + 1);
      RandomLFSR$initSeed = RandomLFSR$shiftReg;
      RandomLFSR$mask = 137 * 29 * (TOS_LOCAL_ADDRESS + 1);
    }
#line 44
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 315 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static void RF230ControlM$RF230Control$resetRadio(void )
#line 315
{

  TOSH_CLR_RF230_RSTN_PIN();
  TOSH_uwait2(1);
  TOSH_SET_RF230_RSTN_PIN();
  TOSH_uwait2(120);


  RF230ControlM$initRegisters();

  return;
}

# 208 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLTimer1M.nc"
static void HPLTimer1M$CaptureT1$setEdge(uint8_t LowToHigh)
#line 208
{

  if (LowToHigh) {
    * (volatile uint8_t *)0x81 |= 1 << 6;
    }
  else {
#line 213
    * (volatile uint8_t *)0x81 &= ~(1 << 6);
    }
  * (volatile uint8_t *)(0x16 + 0x20) |= 1 << 5;
  return;
}

# 499 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static void RF230ControlM$RF230Control$set_RX_ON(void )
#line 499
{
  RF230ControlM$HPLRF230$bitWrite(0x02, 0x1f, 0, 6);
}

# 299 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static result_t RF230RadioRawM$RF230ControlSplitControl$startDone(void )
#line 299
{






  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 306
    {
      RF230RadioRawM$RF230Interrupts$enable();
      RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_IDLE;
    }
#line 309
    __nesc_atomic_end(__nesc_atomic); }

  RF230RadioRawM$RadioSplitControl$startDone();

  return SUCCESS;
}

# 134 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/HPLPowerManagementM.nc"
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void )
#line 134
{
  uint8_t mcu;

#line 136
  if (!HPLPowerManagementM$disabled) {
    TOS_post(HPLPowerManagementM$doAdjustment);
    }
  else 
#line 138
    {
      mcu = * (volatile uint8_t *)(0x33 + 0x20);
      mcu &= 0xf1;
      mcu |= 0;
      * (volatile uint8_t *)(0x33 + 0x20) = mcu;
      * (volatile uint8_t *)(0x33 + 0x20) |= 1 << 0;
    }
  return 0;
}

# 80 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static result_t TimerM$Timer$start(uint8_t id, char type, 
uint32_t interval)
#line 81
{
  uint8_t diff;

#line 83
  if (id >= NUM_TIMERS) {
#line 83
    return FAIL;
    }
#line 84
  if (type > TIMER_ONE_SHOT) {
#line 84
    return FAIL;
    }





  if (type == TIMER_REPEAT && interval <= 2) {
#line 91
    return FAIL;
    }
  TimerM$mTimerList[id].ticks = interval;
  TimerM$mTimerList[id].type = type;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 96
    {
      diff = TimerM$Clock$readCounter();
      interval += diff;
      TimerM$mTimerList[id].ticksLeft = interval;
      TimerM$mState |= 0x1L << id;
      if (interval < TimerM$mInterval) {
          TimerM$mInterval = interval;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
          TimerM$PowerManagement$adjustPower();
        }
    }
#line 107
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 194 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
  TOS_MsgPtr received(TOS_MsgPtr packet)
#line 194
{
  uint16_t addr = TOS_LOCAL_ADDRESS;

#line 196
  AMStandard$counter++;
  {
  }
#line 197
  ;


  if (
#line 199
  packet->crc == 1 && 
  packet->group == TOS_AM_GROUP && (
  packet->addr == TOS_BCAST_ADDR || 
  packet->addr == addr)) 
    {

      uint8_t type = packet->type;
      TOS_MsgPtr tmp;

      {
      }
#line 208
      ;
      AMStandard$dbgPacket(packet);
      {
      }
#line 210
      ;


      tmp = AMStandard$ReceiveMsg$receive(type, packet);
      if (tmp) {
        packet = tmp;
        }
    }
#line 217
  return packet;
}

# 82 "/Users/wbennett/opt/MoteWorks/lib/SOdebug0.h"
static void UARTPutChar(char c)
#line 82
{
  if (c == '\n') {
      do {
        }
      while (
#line 84
      !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0xC0 & (1 << 5)));

      * (volatile uint8_t *)0XC6 = 0xd;
      do {
        }
      while (
#line 87
      !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0xC0 & (1 << 5)));
      * (volatile uint8_t *)0XC6 = 0xa;

      do {
        }
      while (
#line 90
      !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0xC0 & (1 << 6)));

      TOSH_uwait(100);
      return;
    }
#line 94
  ;
  do {
    }
  while (
#line 95
  !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0xC0 & (1 << 5)));
  * (volatile uint8_t *)0XC6 = c;
  do {
    }
  while (
#line 97
  !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0xC0 & (1 << 6)));
  return;
}

# 181 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLTimer1M.nc"
  __attribute((interrupt)) void __vector_17(void )
#line 181
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 182
    {
      if (HPLTimer1M$set_flag) {
          HPLTimer1M$mscale = HPLTimer1M$nextScale;
          HPLTimer1M$nextScale |= 0x8;
          * (volatile uint8_t *)0x81 = HPLTimer1M$nextScale;
          * (volatile uint16_t *)0x88 = HPLTimer1M$minterval;
          HPLTimer1M$set_flag = 0;
        }
    }
#line 190
    __nesc_atomic_end(__nesc_atomic); }

  HPLTimer1M$Timer1$fire();
}

#line 307
  __attribute((signal)) void __vector_16(void )
{
  HPLTimer1M$CaptureT1$captured(HPLTimer1M$CaptureT1$getEvent());
}

# 49 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/HPLRF230M.nc"
static uint8_t HPLRF230M$HPLRF230$readReg(uint8_t addr)
#line 49
{
  uint8_t output;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 52
    output = HPLRF230M$bios_reg_read(addr);
#line 52
    __nesc_atomic_end(__nesc_atomic); }
  return output;
}

# 122 "/Users/wbennett/opt/MoteWorks/tos/system/AMStandard.nc"
static result_t AMStandard$reportSendDone(TOS_MsgPtr msg, result_t success)
#line 122
{
  AMStandard$state = FALSE;
  AMStandard$SendMsg$sendDone(msg->type, msg, success);
  AMStandard$sendDone();

  return SUCCESS;
}

# 491 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230ControlM.nc"
static void RF230ControlM$RF230Control$set_PLL_ON(void )
#line 491
{
  RF230ControlM$HPLRF230$bitWrite(0x02, 0x1f, 0, 9);
}

# 164 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
  __attribute((interrupt)) void __vector_13(void )
#line 164
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 165
    {
      if (HPLClock$set_flag) {
          HPLClock$mscale = HPLClock$nextScale;
          * (volatile uint8_t *)0xB1 = HPLClock$nextScale;
          * (volatile uint8_t *)0xB3 = HPLClock$minterval;
          HPLClock$set_flag = 0;
        }
    }
#line 172
    __nesc_atomic_end(__nesc_atomic); }
  HPLClock$Clock$fire();
}

# 209 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLTimer0.nc"
  __attribute((interrupt)) void __vector_21(void )
#line 209
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 210
    {
      if (HPLTimer0$set_flag) {
          HPLTimer0$mscale = HPLTimer0$nextScale;
          * (volatile uint8_t *)(0x25 + 0x20) = HPLTimer0$nextScale;
          * (volatile uint8_t *)(0X27 + 0x20) = HPLTimer0$minterval;
          HPLTimer0$set_flag = 0;
        }
    }
#line 217
    __nesc_atomic_end(__nesc_atomic); }
  HPLTimer0$Timer0$fire();
}

# 1042 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/RF230RadioRawM.nc"
static result_t RF230RadioRawM$TimerJiffyAsync$fired(void )
#line 1042
{

  uint8_t lTimerState;
  bool bFinishSend;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 1048
    {
      lTimerState = RF230RadioRawM$timerState;
      RF230RadioRawM$timerState = RF230RadioRawM$RF230_TIMER_IDLE;
    }
#line 1051
    __nesc_atomic_end(__nesc_atomic); }

  switch (lTimerState) {


      case RF230RadioRawM$RF230_TIMER_BACKOFF: 
        ;
      RF230RadioRawM$tryToSend();
      break;


      case RF230RadioRawM$RF230_TIMER_ACK: 


        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 1065
          {
            if (RF230RadioRawM$radioState == RF230RadioRawM$RF230_RADIO_WAIT_ACK) {
                RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_SEND_DONE;
                bFinishSend = TRUE;
              }
            else {
#line 1069
              if (RF230RadioRawM$radioState == RF230RadioRawM$RF230_RADIO_STOP) {
                  RF230RadioRawM$radioState = RF230RadioRawM$RF230_RADIO_SEND_FAIL;
                  bFinishSend = TRUE;
                }
              else 
#line 1072
                {
                  bFinishSend = FALSE;
                }
              }
          }
#line 1076
          __nesc_atomic_end(__nesc_atomic); }

      if (bFinishSend) {

          ;

          if (TOS_post(RF230RadioRawM$handleSendDone) == FAIL) {
#line 1082
            RF230RadioRawM$completeSend();
            }
        }
#line 1084
      break;

      case RF230RadioRawM$RF230_TIMER_SEND: 

        ;

      RF230RadioRawM$handleTxInt(SUCCESS);
      break;

      case RF230RadioRawM$RF230_TIMER_SNIFF: 

        RF230RadioRawM$WakeSequence$sniffExpired(TRUE);
      break;

      default: 
        break;
    }


  return SUCCESS;
}

# 88 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/TimerJiffyAsyncM.nc"
static result_t TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(uint32_t _jiffy)
{
  /* atomic removed: atomic calls only */
#line 90
  {
    TimerJiffyAsyncM$jiffy = _jiffy;
    TimerJiffyAsyncM$bSet = 1;
  }
  if (_jiffy > 0xFF) {
      TimerJiffyAsyncM$Timer$setIntervalAndScale(0xFF, 0x4);
    }
  else {
      TimerJiffyAsyncM$Timer$setIntervalAndScale(_jiffy, 0x4);
    }

  TimerJiffyAsyncM$PowerManagement$adjustPower();
  return SUCCESS;
}

# 148 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLTimer0.nc"
static result_t HPLTimer0$Timer0$setIntervalAndScale(uint8_t interval, uint8_t scale)
#line 148
{



  if (scale > 7) {
#line 152
    return FAIL;
    }
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 154
    {
      * (volatile uint8_t *)(0x25 + 0x20) = 0;

      * (volatile uint8_t *)0x6E &= ~(1 << 0);
      * (volatile uint8_t *)0x6E &= ~(1 << 1);

      HPLTimer0$mscale = scale;
      HPLTimer0$minterval = interval;

      * (volatile uint8_t *)(0X26 + 0x20) = 0;
      * (volatile uint8_t *)(0X27 + 0x20) = interval;

      * (volatile uint8_t *)(0x15 + 0x20) |= 1 << 1;
      * (volatile uint8_t *)0x6E |= 1 << 1;

      * (volatile uint8_t *)(0x24 + 0x20) = 0;
      * (volatile uint8_t *)(0x25 + 0x20) = scale;
    }
#line 171
    __nesc_atomic_end(__nesc_atomic); }

  return SUCCESS;
}

# 98 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
  __attribute((signal)) void __vector_25(void )
#line 98
{
  if (* (volatile uint8_t *)0xC0 & (1 << 7)) {
    HPLUART0M$UART$get(* (volatile uint8_t *)0XC6);
    }
}

# 142 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
static uint8_t FramerM$fRemapRxPos(uint8_t InPos)
#line 142
{


  if (InPos < 4) {
    return InPos + (unsigned short )& ((struct TOS_Msg *)0)->addr;
    }
  else {
#line 147
    if (InPos == 4) {
      return (unsigned short )& ((struct TOS_Msg *)0)->length;
      }
    else {
#line 150
      return InPos + (unsigned short )& ((struct TOS_Msg *)0)->addr - 1;
      }
    }
}



static result_t FramerM$StartTx(void )
#line 157
{
  result_t Result = SUCCESS;
  bool fInitiate = 0;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 161
    {
      if (FramerM$gTxState == FramerM$TXSTATE_IDLE) {
          if (FramerM$gFlags & FramerM$FLAGS_TOKENPEND) {

              FramerM$gpTxBuf = (uint8_t *)&FramerM$gTxTokenBuf;




              FramerM$gTxProto = FramerM$PROTO_ACK;
              FramerM$gTxLength = sizeof FramerM$gTxTokenBuf;
              fInitiate = 1;
              FramerM$gTxState = FramerM$TXSTATE_PROTO;
            }
          else {
#line 175
            if (FramerM$gFlags & FramerM$FLAGS_DATAPEND) {
                FramerM$gpTxBuf = (uint8_t *)FramerM$gpTxMsg;
                FramerM$gTxProto = FramerM$PROTO_PACKET_NOACK;


                FramerM$gTxLength = FramerM$gpTxMsg->length + TOS_HEADER_SIZE + 2 + 3;



                fInitiate = 1;
                FramerM$gTxState = FramerM$TXSTATE_PROTO;
              }
            else {
#line 187
              if (FramerM$gFlags & FramerM$FLAGS_UNKNOWN) {
                  FramerM$gpTxBuf = (uint8_t *)&FramerM$gTxUnknownBuf;
                  FramerM$gTxProto = FramerM$PROTO_UNKNOWN;
                  FramerM$gTxLength = sizeof FramerM$gTxUnknownBuf;
                  fInitiate = 1;
                  FramerM$gTxState = FramerM$TXSTATE_PROTO;
                }
              }
            }
        }
    }
#line 197
    __nesc_atomic_end(__nesc_atomic); }
#line 197
  if (fInitiate) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 198
        {

          FramerM$gTxRunningCRC = 0;
          FramerM$gTxByteCnt = (unsigned short )& ((struct TOS_Msg *)0)->addr;
        }
#line 202
        __nesc_atomic_end(__nesc_atomic); }




      Result = FramerM$ByteComm$txByte(FramerM$HDLC_FLAG_BYTE);
      if (Result != SUCCESS) {
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 209
            FramerM$gTxState = FramerM$TXSTATE_ERROR;
#line 209
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(FramerM$PacketSent);
        }
    }

  return Result;
}

# 90 "/Users/wbennett/opt/MoteWorks/tos/system/UARTM.nc"
static result_t UARTM$ByteComm$txByte(uint8_t data)
#line 90
{
  bool oldState;

  {
  }
#line 93
  ;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 95
    {
      oldState = UARTM$state;
      UARTM$state = 1;
    }
#line 98
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState) {
    return FAIL;
    }
  UARTM$HPLUART$put(data);

  return SUCCESS;
}

# 264 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
static void FramerM$PacketSent(void )
#line 264
{
  result_t TxResult = SUCCESS;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 267
    {
      if (FramerM$gTxState == FramerM$TXSTATE_ERROR) {
          TxResult = FAIL;
          FramerM$gTxState = FramerM$TXSTATE_IDLE;
        }
    }
#line 272
    __nesc_atomic_end(__nesc_atomic); }
  if (FramerM$gTxProto == FramerM$PROTO_ACK) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 274
        FramerM$gFlags ^= FramerM$FLAGS_TOKENPEND;
#line 274
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 277
        FramerM$gFlags ^= FramerM$FLAGS_DATAPEND;
#line 277
        __nesc_atomic_end(__nesc_atomic); }
      FramerM$BareSendMsg$sendDone((TOS_MsgPtr )FramerM$gpTxMsg, TxResult);
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 279
        FramerM$gpTxMsg = (void *)0;
#line 279
        __nesc_atomic_end(__nesc_atomic); }
    }


  FramerM$StartTx();
}

# 108 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
  __attribute((interrupt)) void __vector_27(void )
#line 108
{
  HPLUART0M$UART$putDone();
}

# 498 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/FramerM.nc"
static result_t FramerM$TxArbitraryByte(uint8_t inByte)
#line 498
{
  if (inByte == FramerM$HDLC_FLAG_BYTE || inByte == FramerM$HDLC_CTLESC_BYTE) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 500
        {
          FramerM$gPrevTxState = FramerM$gTxState;
          FramerM$gTxState = FramerM$TXSTATE_ESC;
          FramerM$gTxEscByte = inByte;
        }
#line 504
        __nesc_atomic_end(__nesc_atomic); }
      inByte = FramerM$HDLC_CTLESC_BYTE;
    }

  return FramerM$ByteComm$txByte(inByte);
}


/*
 * BaseM.nc
 *
 * Sends an 8-bit global struct orientation
 * <header>Data<header>
 *
 **/
#include "debug_constants.h"
#define DBG_PKT  1
#define SO_DEBUG  1
#include "crane.h"
 
module BaseM {
  provides interface StdControl;
  uses {
    interface Leds;
    interface ReceiveMsg;
  }
}
implementation {


	uint16_t counter;
	TOS_Msg Tmessage;
	 
  command result_t StdControl.init() {
	  counter = 0;
    call Leds.init();     
    return SUCCESS;
  }


  command result_t StdControl.start() {
    return SUCCESS;
  }

  command result_t StdControl.stop() {
    return SUCCESS;
  }
  
  event TOS_MsgPtr ReceiveMsg.receive(TOS_MsgPtr m) {
	
	  OrientationMessage * Omessage;
		
	  /***  Copy m to Tmessage ***/
	  memcpy(&Tmessage,m,sizeof(TOS_Msg));
	
    Omessage = (OrientationMessage*)Tmessage.data;

		call Leds.redToggle();
		dprintf("%lu\t|\t%i\t%i\t%i\t|\t%i\t%i\t%i\t|\t%i\n",
						Omessage->seqnum,
						Omessage->accelx,
						Omessage->accely,
						Omessage->accelz,
						Omessage->heading,
						Omessage->pitch,
						Omessage->roll,
						Omessage->color);
    return m;
  }
   
}


/*
 * BaseM.nc
 *
 * Sends an 8-bit global struct orientation
 * <header>Data<header>
 *
 **/
#include "comm_types.h"
#include "PowerMonitor/types.h"
#include "debug_constants.h"
module BaseM {
  provides interface StdControl;
  uses {
    interface Leds;
    interface ReceiveMsg;
  }
}
implementation {

	
	command result_t StdControl.init() {
    call Leds.init();     
		call Leds.redToggle();
    return SUCCESS;
  }


  command result_t StdControl.start() {
    return SUCCESS;
  }

  command result_t StdControl.stop() {
    return SUCCESS;
  }
  event TOS_MsgPtr ReceiveMsg.receive(TOS_MsgPtr m) {
    info_packet_t *message = (info_packet_t*)m->data;
		uint8_t rssi,lqi;
		power_monitor_t msg;
		memcpy(&msg,&message->msg,sizeof(power_monitor_t));

		/*
		rssi = m->strength & 0x1F;
		if(rssi > 28) {
		rssi = 28;
		}
		lqi = m->lqi;
		*/
		call Leds.yellowToggle();
		dprintf("battv:%u,batti:%u,solarv:%u,solari:%u,\n",
			msg.BattV,
			msg.BattI,
			msg.SolarV,
			msg.SolarI);
    return m;
  }
   
}


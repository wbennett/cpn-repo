#ifndef COMM_TYPES_H
#define COMM_TYPES_H
#include "PowerMonitor/types.h"
typedef struct info_packet {
	uint32_t seqnum;
	power_monitor_t msg;
}__attribute__ ((packed)) info_packet_t;

#endif

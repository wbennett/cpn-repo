#ifndef CRANE_LEG_H
#define CRANE_LEG_H

/**
 *  Structure for holding compass readings
 */
typedef struct Compass_Readings {
  uint16_t heading;
  int16_t pitch;
	int16_t roll;
	uint16_t accelx;
	uint16_t accely;
	uint16_t accelz;
} __attribute__ ((packed)) Compass_Readings;

typedef struct OrientationMessage {
  uint32_t seqnum;
  uint16_t heading;
  int16_t pitch;
  int16_t roll;
	uint16_t accelx;
	uint16_t accely;
	uint16_t accelz;
	uint8_t color;
  uint16_t source;
} OrientationMessage;

enum {
  AM_CRANE = 55
};

enum {
  NORTH = 0,
	EAST = 1,
	SOUTH = 2,
	WEST = 3,
	DERROR = 4
};

enum {
  UPDATE_PRD = 500
};

#endif /* CRANE_LEG_H */


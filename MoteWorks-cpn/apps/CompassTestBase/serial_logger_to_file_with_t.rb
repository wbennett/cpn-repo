#!/usr/bin/ruby
require 'rubygems'
require 'date'
require 'time'
require 'serialport'

def check_and_print(char) 
	if char == 9 || char == 10 || char == 13
		print char.chr;
	elsif char < 0x20 or char > 0x7E
		print " 0x#{char.to_s(16)} ";
	else
		print char.chr;
	end
end

if ARGV.size < 3
	STDERR.print <<EOF
	Usage: ruby #{$0} port bps file.to.output.data
EOF
	exit(1)
end
puts ARGV[0], ARGV[1], ARGV[2]
sp = SerialPort.new("#{ARGV[0]}", ARGV[1].to_i, 8,1,SerialPort::NONE)
queue = Queue.new
#consumer = Thread.new {
#str = ""
#loop do
#	str = ""
#	val = STDIN.gets()
#	if val == nil
#	break
#	else
#	str = val.gsub(/\r/,"").gsub(/\n/,"");
#	sp.puts("#{str}\r")
#	puts("wrote:#{str}");
#	end
#end
#}

file = File.open("#{ARGV[2]}",'w')

producer = Thread.new {
str = ""
loop  do
	val = sp.gets()
	if val == nil
	break
	else
#	check_and_print(val);
	puts "#{val.gsub(/\r\n?/, "")}  #{Time.now.to_f}\n"
	file.write("#{val.gsub(/\r\n?/,"")}  #{Time.now.to_f}\n");
	end
end
}
producer.join
puts "closing socket.."
sp.close

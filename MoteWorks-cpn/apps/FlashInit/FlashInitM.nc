module FlashInitM
	{
	provides
		{
		interface StdControl;
		}
	uses 
		{
		interface Leds;
		interface AllocationReq;
		interface WriteData;
		}
	}
	
implementation
	{
	#include "PageEEPROM.h"
	#define SO_DEBUG (1)
	#define DBG_PKT (1)
	#include "SOdebug.h"
	
	/* How much flash memory we request (all of it) */
	#define ALLOC_REQ	((uint32_t)((((uint32_t)TOS_EEPROM_MAX_PAGES) << ((uint32_t)TOS_EEPROM_PAGE_SIZE_LOG2))))
	#define BUF_SIZE	(((uint32_t)256))
	
	uint8_t		buf[BUF_SIZE];	/* where the read data is placed */
	uint32_t	index;
	
	task void writeNew()
		{
		SODbg( 1, (uint8_t *) "Writing to: %lu\n", index);
		call WriteData.write(index, buf, BUF_SIZE);
		}

	command result_t StdControl.init()
		{
		call Leds.init(); 
		memset(buf, INITVALUE, BUF_SIZE);
		index = 0;
		call AllocationReq.request(ALLOC_REQ);
	
		return SUCCESS;
		}

	command result_t StdControl.start()
		{
		return SUCCESS;
		}

	command result_t StdControl.stop()
		{
		return SUCCESS;
		}

	event result_t AllocationReq.requestProcessed(result_t success)
		{
		// Allocation must succeed
		if(success == SUCCESS)
			{
			call Leds.greenOn();
			post writeNew();
			}
		return SUCCESS;
		}
		
	/* Don't actually care about this as long as we're filling the entire flash */
	event result_t WriteData.flushDone(result_t success)
		{
		return SUCCESS;
		}
		
	event result_t WriteData.writeDone(uint8_t *data, uint32_t numBytesWrite, result_t success) 
		{
		index = index + numBytesWrite;
		call Leds.yellowToggle();
		if(index < ALLOC_REQ)
			{
			post writeNew();
			}
		else
			{
			call Leds.redOn();
			}
		return SUCCESS;
		}
	}

configuration FlashInit
	{
	}
implementation
	{
	enum
		{
		BYTE_EEPROM_ID = unique("ByteEEPROM"),
		};

	components Main, FlashInitM, LedsC, ByteEEPROM;
 
	Main.StdControl -> ByteEEPROM; 
	Main.StdControl -> FlashInitM.StdControl;
	
	FlashInitM.Leds				-> LedsC.Leds;
	FlashInitM.AllocationReq	-> ByteEEPROM.AllocationReq[BYTE_EEPROM_ID];
	FlashInitM.WriteData		-> ByteEEPROM.WriteData[BYTE_EEPROM_ID];
	}


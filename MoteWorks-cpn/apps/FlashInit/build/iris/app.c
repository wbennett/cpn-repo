#define nx_struct struct
#define nx_union union
#define dbg(mode, format, ...) ((void)0)
#define dbg_clear(mode, format, ...) ((void)0)
#define dbg_active(mode) 0
# 152 "/opt/local/lib/gcc/avr/4.1.2/include/stddef.h" 3
typedef int ptrdiff_t;
#line 214
typedef unsigned int size_t;
#line 326
typedef int wchar_t;
# 8 "/opt/local/lib/ncc/deputy_nodeputy.h"
struct __nesc_attr_nonnull {
#line 8
  int dummy;
}  ;
#line 9
struct __nesc_attr_bnd {
#line 9
  void *lo, *hi;
}  ;
#line 10
struct __nesc_attr_bnd_nok {
#line 10
  void *lo, *hi;
}  ;
#line 11
struct __nesc_attr_count {
#line 11
  int n;
}  ;
#line 12
struct __nesc_attr_count_nok {
#line 12
  int n;
}  ;
#line 13
struct __nesc_attr_one {
#line 13
  int dummy;
}  ;
#line 14
struct __nesc_attr_one_nok {
#line 14
  int dummy;
}  ;
#line 15
struct __nesc_attr_dmemset {
#line 15
  int a1, a2, a3;
}  ;
#line 16
struct __nesc_attr_dmemcpy {
#line 16
  int a1, a2, a3;
}  ;
#line 17
struct __nesc_attr_nts {
#line 17
  int dummy;
}  ;
# 121 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdint.h" 3
typedef int int8_t __attribute((__mode__(__QI__))) ;
typedef unsigned int uint8_t __attribute((__mode__(__QI__))) ;
typedef int int16_t __attribute((__mode__(__HI__))) ;
typedef unsigned int uint16_t __attribute((__mode__(__HI__))) ;
typedef int int32_t __attribute((__mode__(__SI__))) ;
typedef unsigned int uint32_t __attribute((__mode__(__SI__))) ;

typedef int int64_t __attribute((__mode__(__DI__))) ;
typedef unsigned int uint64_t __attribute((__mode__(__DI__))) ;
#line 142
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
#line 159
typedef int8_t int_least8_t;




typedef uint8_t uint_least8_t;




typedef int16_t int_least16_t;




typedef uint16_t uint_least16_t;




typedef int32_t int_least32_t;




typedef uint32_t uint_least32_t;







typedef int64_t int_least64_t;






typedef uint64_t uint_least64_t;
#line 213
typedef int8_t int_fast8_t;




typedef uint8_t uint_fast8_t;




typedef int16_t int_fast16_t;




typedef uint16_t uint_fast16_t;




typedef int32_t int_fast32_t;




typedef uint32_t uint_fast32_t;







typedef int64_t int_fast64_t;






typedef uint64_t uint_fast64_t;
#line 273
typedef int64_t intmax_t;




typedef uint64_t uintmax_t;
# 77 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/inttypes.h" 3
typedef int32_t int_farptr_t;



typedef uint32_t uint_farptr_t;
# 431 "/opt/local/lib/ncc/nesc_nx.h"
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nx_int8_t;typedef int8_t __nesc_nxbase_nx_int8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nx_int16_t;typedef int16_t __nesc_nxbase_nx_int16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nx_int32_t;typedef int32_t __nesc_nxbase_nx_int32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nx_int64_t;typedef int64_t __nesc_nxbase_nx_int64_t  ;
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nx_uint8_t;typedef uint8_t __nesc_nxbase_nx_uint8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nx_uint16_t;typedef uint16_t __nesc_nxbase_nx_uint16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nx_uint32_t;typedef uint32_t __nesc_nxbase_nx_uint32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nx_uint64_t;typedef uint64_t __nesc_nxbase_nx_uint64_t  ;


typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nxle_int8_t;typedef int8_t __nesc_nxbase_nxle_int8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nxle_int16_t;typedef int16_t __nesc_nxbase_nxle_int16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nxle_int32_t;typedef int32_t __nesc_nxbase_nxle_int32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nxle_int64_t;typedef int64_t __nesc_nxbase_nxle_int64_t  ;
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nxle_uint8_t;typedef uint8_t __nesc_nxbase_nxle_uint8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nxle_uint16_t;typedef uint16_t __nesc_nxbase_nxle_uint16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nxle_uint32_t;typedef uint32_t __nesc_nxbase_nxle_uint32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nxle_uint64_t;typedef uint64_t __nesc_nxbase_nxle_uint64_t  ;
# 71 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdlib.h" 3
#line 68
typedef struct __nesc_unnamed4242 {
  int quot;
  int rem;
} div_t;





#line 74
typedef struct __nesc_unnamed4243 {
  long quot;
  long rem;
} ldiv_t;


typedef int (*__compar_fn_t)(const void *arg_0x10076d370, const void *arg_0x10076d648);
# 71 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
typedef unsigned char bool;






enum __nesc_unnamed4244 {
  FALSE = 0, 
  TRUE = 1
};








uint8_t TOS_ROUTE_PROTOCOL = 0x90;
#line 104
uint8_t TOS_BASE_STATION = 0;





const uint8_t TOS_DATA_LENGTH = 36;
#line 132
uint8_t TOS_PLATFORM = 7;










enum __nesc_unnamed4245 {
  FAIL = 0, 
  SUCCESS = 1
};


static inline uint8_t rcombine(uint8_t r1, uint8_t r2);
typedef uint8_t result_t  ;







static inline result_t rcombine(result_t r1, result_t r2);
#line 178
enum __nesc_unnamed4246 {
  NULL = 0x0
};
# 210 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/avr/pgmspace.h" 3
typedef void prog_void __attribute((__progmem__)) ;
typedef char prog_char __attribute((__progmem__)) ;
typedef unsigned char prog_uchar __attribute((__progmem__)) ;

typedef int8_t prog_int8_t __attribute((__progmem__)) ;
typedef uint8_t prog_uint8_t __attribute((__progmem__)) ;
typedef int16_t prog_int16_t __attribute((__progmem__)) ;
typedef uint16_t prog_uint16_t __attribute((__progmem__)) ;
typedef int32_t prog_int32_t __attribute((__progmem__)) ;
typedef uint32_t prog_uint32_t __attribute((__progmem__)) ;

typedef int64_t prog_int64_t __attribute((__progmem__)) ;
typedef uint64_t prog_uint64_t __attribute((__progmem__)) ;
# 118 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
enum __nesc_unnamed4247 {
  TOSH_period16 = 0x00, 
  TOSH_period32 = 0x01, 
  TOSH_period64 = 0x02, 
  TOSH_period128 = 0x03, 
  TOSH_period256 = 0x04, 
  TOSH_period512 = 0x05, 
  TOSH_period1024 = 0x06, 
  TOSH_period2048 = 0x07
};

static inline void TOSH_wait();







typedef uint8_t __nesc_atomic_t;

__nesc_atomic_t __nesc_atomic_start(void );
void __nesc_atomic_end(__nesc_atomic_t oldSreg);



__inline __nesc_atomic_t __nesc_atomic_start(void )  ;






__inline void __nesc_atomic_end(__nesc_atomic_t oldSreg)  ;






static __inline void __nesc_atomic_sleep();







static __inline void __nesc_enable_interrupt();



static __inline void __nesc_disable_interrupt();
# 39 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_uwait(int u_sec);
#line 63
static __inline void TOSH_SET_RED_LED_PIN();
#line 63
static __inline void TOSH_CLR_RED_LED_PIN();
#line 63
static __inline void TOSH_MAKE_RED_LED_OUTPUT();
static __inline void TOSH_SET_GREEN_LED_PIN();
#line 64
static __inline void TOSH_CLR_GREEN_LED_PIN();
#line 64
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT();
static __inline void TOSH_SET_YELLOW_LED_PIN();
#line 65
static __inline void TOSH_CLR_YELLOW_LED_PIN();
#line 65
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT();

static __inline void TOSH_CLR_SERIAL_ID_PIN();
#line 67
static __inline void TOSH_MAKE_SERIAL_ID_INPUT();
#line 79
static __inline void TOSH_SET_FLASH_SELECT_PIN();
#line 79
static __inline void TOSH_CLR_FLASH_SELECT_PIN();
#line 79
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT();
static __inline void TOSH_SET_FLASH_CLK_PIN();
#line 80
static __inline void TOSH_CLR_FLASH_CLK_PIN();
#line 80
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT();
static __inline void TOSH_SET_FLASH_OUT_PIN();
#line 81
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT();
static __inline void TOSH_CLR_FLASH_IN_PIN();
#line 82
static __inline int TOSH_READ_FLASH_IN_PIN();
#line 82
static __inline void TOSH_MAKE_FLASH_IN_INPUT();
#line 98
static __inline void TOSH_MAKE_PW0_OUTPUT();
static __inline void TOSH_MAKE_PW1_OUTPUT();
static __inline void TOSH_MAKE_PW2_OUTPUT();
static __inline void TOSH_MAKE_PW3_OUTPUT();
static __inline void TOSH_MAKE_PW4_OUTPUT();
static __inline void TOSH_MAKE_PW5_OUTPUT();
static __inline void TOSH_MAKE_PW6_OUTPUT();
static __inline void TOSH_MAKE_PW7_OUTPUT();
#line 134
static inline void TOSH_SET_PIN_DIRECTIONS(void );
#line 187
enum __nesc_unnamed4248 {
  TOSH_ADC_PORTMAPSIZE = 12
};

enum __nesc_unnamed4249 {


  TOSH_ACTUAL_VOLTAGE_PORT = 30, 
  TOSH_ACTUAL_BANDGAP_PORT = 30, 
  TOSH_ACTUAL_GND_PORT = 31
};

enum __nesc_unnamed4250 {


  TOS_ADC_VOLTAGE_PORT = 7, 
  TOS_ADC_BANDGAP_PORT = 10, 
  TOS_ADC_GND_PORT = 11
};
# 33 "/Users/wbennett/opt/MoteWorks/tos/types/dbg_modes.h"
typedef long long TOS_dbg_mode;



enum __nesc_unnamed4251 {
  DBG_ALL = ~0ULL, 


  DBG_BOOT = 1ULL << 0, 
  DBG_CLOCK = 1ULL << 1, 
  DBG_TASK = 1ULL << 2, 
  DBG_SCHED = 1ULL << 3, 
  DBG_SENSOR = 1ULL << 4, 
  DBG_LED = 1ULL << 5, 
  DBG_CRYPTO = 1ULL << 6, 


  DBG_ROUTE = 1ULL << 7, 
  DBG_AM = 1ULL << 8, 
  DBG_CRC = 1ULL << 9, 
  DBG_PACKET = 1ULL << 10, 
  DBG_ENCODE = 1ULL << 11, 
  DBG_RADIO = 1ULL << 12, 


  DBG_LOG = 1ULL << 13, 
  DBG_ADC = 1ULL << 14, 
  DBG_I2C = 1ULL << 15, 
  DBG_UART = 1ULL << 16, 
  DBG_PROG = 1ULL << 17, 
  DBG_SOUNDER = 1ULL << 18, 
  DBG_TIME = 1ULL << 19, 
  DBG_POWER = 1ULL << 20, 



  DBG_SIM = 1ULL << 21, 
  DBG_QUEUE = 1ULL << 22, 
  DBG_SIMRADIO = 1ULL << 23, 
  DBG_HARD = 1ULL << 24, 
  DBG_MEM = 1ULL << 25, 



  DBG_USR1 = 1ULL << 27, 
  DBG_USR2 = 1ULL << 28, 
  DBG_USR3 = 1ULL << 29, 
  DBG_TEMP = 1ULL << 30, 

  DBG_ERROR = 1ULL << 31, 
  DBG_NONE = 0, 

  DBG_DEFAULT = DBG_ALL
};
# 41 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
#line 39
typedef struct __nesc_unnamed4252 {
  void (*tp)();
} TOSH_sched_entry_T;

enum __nesc_unnamed4253 {






  TOSH_MAX_TASKS = 32, 

  TOSH_TASK_BITMASK = TOSH_MAX_TASKS - 1
};

volatile TOSH_sched_entry_T TOSH_queue[TOSH_MAX_TASKS];
uint8_t TOSH_sched_full;
volatile uint8_t TOSH_sched_free;

static inline void TOSH_sched_init(void );








bool TOS_post(void (*tp)());
#line 82
bool TOS_post(void (*tp)())  ;
#line 116
static inline bool TOSH_run_next_task();
#line 139
static inline void TOSH_run_task();
# 197 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline void *nmemset(void *to, int val, size_t n);
# 14 "/Users/wbennett/opt/MoteWorks/tos/system/Ident.h"
enum __nesc_unnamed4254 {

  IDENT_MAX_PROGRAM_NAME_LENGTH = 17
};






#line 19
typedef struct __nesc_unnamed4255 {

  uint32_t unix_time;
  uint32_t user_hash;
  char program_name[IDENT_MAX_PROGRAM_NAME_LENGTH];
} Ident_t;
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.h"
enum __nesc_unnamed4256 {
  TOS_EEPROM_MAX_PAGES = 2048, 
  TOS_EEPROM_PAGE_SIZE = 264, 
  TOS_EEPROM_PAGE_SIZE_LOG2 = 8
};

enum __nesc_unnamed4257 {
  TOS_EEPROM_ERASE, 
  TOS_EEPROM_DONT_ERASE, 
  TOS_EEPROM_PREVIOUSLY_ERASED
};

typedef uint16_t eeprompage_t;
typedef uint16_t eeprompageoffset_t;
# 12 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/crc.h"
uint16_t crcTable[256] __attribute((__progmem__))  = { 
0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 
0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef, 
0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6, 
0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de, 
0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485, 
0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d, 
0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4, 
0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc, 
0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823, 
0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b, 
0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12, 
0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a, 
0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41, 
0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49, 
0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70, 
0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78, 
0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f, 
0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067, 
0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e, 
0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256, 
0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d, 
0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 
0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c, 
0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634, 
0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab, 
0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3, 
0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a, 
0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92, 
0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9, 
0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1, 
0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8, 
0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0 };





static inline uint16_t crcByte(uint16_t oldCrc, uint8_t byte);
# 14 "/Users/wbennett/opt/MoteWorks/tos/types/ByteEEPROMInternal.h"
#line 10
typedef struct RegionSpecifier_t {
  uint32_t startByte;
  uint32_t stopByte;
  struct RegionSpecifier_t *next;
} RegionSpecifier;
# 10 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/ByteEEPROM_platform.h"
enum __nesc_unnamed4258 {
  TOS_BYTEEEPROM_PAGESIZE = 1 << TOS_EEPROM_PAGE_SIZE_LOG2, 
  TOS_BYTEEEPROM_LASTBYTE = (long )TOS_EEPROM_MAX_PAGES << TOS_EEPROM_PAGE_SIZE_LOG2
};
enum FlashInit$__nesc_unnamed4259 {
  FlashInit$BYTE_EEPROM_ID = 0U
};
# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
static result_t PotM$Pot$init(uint8_t initialSetting);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
static result_t HPLPotC$Pot$finalise(void );
#line 38
static result_t HPLPotC$Pot$decrease(void );







static result_t HPLPotC$Pot$increase(void );
# 32 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static result_t HPLInit$init(void );
# 58 "/Users/wbennett/opt/MoteWorks/tos/interfaces/AllocationReq.nc"
static result_t FlashInitM$AllocationReq$requestProcessed(result_t success);
# 45 "WriteData.nc"
static result_t FlashInitM$WriteData$flushDone(result_t success);








static result_t FlashInitM$WriteData$writeDone(uint8_t *data, uint32_t numBytesWrite, result_t success);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t FlashInitM$StdControl$init(void );






static result_t FlashInitM$StdControl$start(void );
# 101 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t LedsC$Leds$yellowOff(void );
#line 93
static result_t LedsC$Leds$yellowOn(void );
#line 35
static result_t LedsC$Leds$init(void );
#line 110
static result_t LedsC$Leds$yellowToggle(void );
#line 43
static result_t LedsC$Leds$redOn(void );
#line 68
static result_t LedsC$Leds$greenOn(void );
# 28 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
static result_t PageEEPROMM$PageEEPROM$read(eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n);
#line 17
static result_t PageEEPROMM$PageEEPROM$erase(eeprompage_t page, uint8_t eraseKind);






static result_t PageEEPROMM$PageEEPROM$flush(eeprompage_t page);
static result_t PageEEPROMM$PageEEPROM$flushAll(void );
#line 13
static result_t PageEEPROMM$PageEEPROM$write(eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n);
# 45 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/SlavePin.nc"
static result_t PageEEPROMM$FlashSelect$notifyHigh(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t PageEEPROMM$StdControl$init(void );






static result_t PageEEPROMM$StdControl$start(void );
# 12 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Resource.nc"
static result_t PageEEPROMM$FlashIdle$available(void );
# 22 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
static result_t PageEEPROMShare$PageEEPROM$default$syncDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 22 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result);



static result_t PageEEPROMShare$PageEEPROM$default$flushDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 26 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result);

static result_t PageEEPROMShare$PageEEPROM$read(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 28 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n);
#line 15
static result_t PageEEPROMShare$PageEEPROM$default$writeDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result);

static result_t PageEEPROMShare$PageEEPROM$erase(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 17 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
eeprompage_t page, uint8_t eraseKind);
static result_t PageEEPROMShare$PageEEPROM$default$eraseDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 18 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result);
#line 34
static result_t PageEEPROMShare$PageEEPROM$default$computeCrcDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 34 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result, uint16_t crc);
#line 24
static result_t PageEEPROMShare$PageEEPROM$flush(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 24 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
eeprompage_t page);
static result_t PageEEPROMShare$PageEEPROM$flushAll(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020);
# 13 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
static result_t PageEEPROMShare$PageEEPROM$write(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 13 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n);
#line 30
static result_t PageEEPROMShare$PageEEPROM$default$readDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 30 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result);
#line 22
static result_t PageEEPROMShare$ActualEEPROM$syncDone(result_t result);



static result_t PageEEPROMShare$ActualEEPROM$flushDone(result_t result);
#line 15
static result_t PageEEPROMShare$ActualEEPROM$writeDone(result_t result);


static result_t PageEEPROMShare$ActualEEPROM$eraseDone(result_t result);
#line 34
static result_t PageEEPROMShare$ActualEEPROM$computeCrcDone(result_t result, uint16_t crc);
#line 30
static result_t PageEEPROMShare$ActualEEPROM$readDone(result_t result);
# 31 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLFlash.nc"
static bool HPLFlash$getCompareStatus(void );
# 30 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/SlavePin.nc"
static result_t HPLFlash$FlashSelect$low(void );









static result_t HPLFlash$FlashSelect$high(bool needEvent);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t HPLFlash$FlashControl$init(void );






static result_t HPLFlash$FlashControl$start(void );
# 38 "/Users/wbennett/opt/MoteWorks/tos/interfaces/FastSPI.nc"
static uint8_t HPLFlash$FlashSPI$txByte(uint8_t data);
# 11 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Resource.nc"
static result_t HPLFlash$FlashIdle$wait(void );
# 22 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
static result_t ByteEEPROMC$PageEEPROM$syncDone(result_t result);



static result_t ByteEEPROMC$PageEEPROM$flushDone(result_t result);
#line 15
static result_t ByteEEPROMC$PageEEPROM$writeDone(result_t result);


static result_t ByteEEPROMC$PageEEPROM$eraseDone(result_t result);
#line 34
static result_t ByteEEPROMC$PageEEPROM$computeCrcDone(result_t result, uint16_t crc);
#line 30
static result_t ByteEEPROMC$PageEEPROM$readDone(result_t result);
# 83 "/Users/wbennett/opt/MoteWorks/tos/interfaces/LogData.nc"
static result_t ByteEEPROMC$LogData$default$syncDone(
# 39 "ByteEEPROMC.nc"
uint8_t arg_0x10146ea90, 
# 83 "/Users/wbennett/opt/MoteWorks/tos/interfaces/LogData.nc"
result_t success);
#line 45
static result_t ByteEEPROMC$LogData$default$eraseDone(
# 39 "ByteEEPROMC.nc"
uint8_t arg_0x10146ea90, 
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/LogData.nc"
result_t success);
#line 61
static result_t ByteEEPROMC$LogData$default$appendDone(
# 39 "ByteEEPROMC.nc"
uint8_t arg_0x10146ea90, 
# 61 "/Users/wbennett/opt/MoteWorks/tos/interfaces/LogData.nc"
uint8_t *data, uint32_t numBytes, result_t success);
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReadData.nc"
static result_t ByteEEPROMC$ReadData$default$readDone(
# 40 "ByteEEPROMC.nc"
uint8_t arg_0x101469020, 
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReadData.nc"
uint8_t *buffer, uint32_t numBytesRead, result_t success);
# 45 "WriteData.nc"
static result_t ByteEEPROMC$WriteData$default$flushDone(
# 38 "ByteEEPROMC.nc"
uint8_t arg_0x10146aad8, 
# 45 "WriteData.nc"
result_t success);








static result_t ByteEEPROMC$WriteData$default$writeDone(
# 38 "ByteEEPROMC.nc"
uint8_t arg_0x10146aad8, 
# 54 "WriteData.nc"
uint8_t *data, uint32_t numBytesWrite, result_t success);
#line 39
static result_t ByteEEPROMC$WriteData$write(
# 38 "ByteEEPROMC.nc"
uint8_t arg_0x10146aad8, 
# 39 "WriteData.nc"
uint32_t offset, uint8_t *data, uint32_t numBytesWrite);
# 13 "/Users/wbennett/opt/MoteWorks/tos/system/ByteEEPROMAllocate.nc"
static RegionSpecifier *ByteEEPROMAllocate$getRegion(uint8_t id);
# 58 "/Users/wbennett/opt/MoteWorks/tos/interfaces/AllocationReq.nc"
static result_t ByteEEPROMAllocate$AllocationReq$default$requestProcessed(
# 12 "/Users/wbennett/opt/MoteWorks/tos/system/ByteEEPROMAllocate.nc"
uint8_t arg_0x10146f880, 
# 58 "/Users/wbennett/opt/MoteWorks/tos/interfaces/AllocationReq.nc"
result_t success);
#line 39
static result_t ByteEEPROMAllocate$AllocationReq$request(
# 12 "/Users/wbennett/opt/MoteWorks/tos/system/ByteEEPROMAllocate.nc"
uint8_t arg_0x10146f880, 
# 39 "/Users/wbennett/opt/MoteWorks/tos/interfaces/AllocationReq.nc"
uint32_t numBytesReq);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t ByteEEPROMAllocate$StdControl$init(void );






static result_t ByteEEPROMAllocate$StdControl$start(void );
# 27 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
static result_t RealMain$hardwareInit(void );
# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
static result_t RealMain$Pot$init(uint8_t initialSetting);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t RealMain$StdControl$init(void );






static result_t RealMain$StdControl$start(void );
# 34 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
int main(void )   ;
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
static result_t PotM$HPLPot$finalise(void );
#line 38
static result_t PotM$HPLPot$decrease(void );







static result_t PotM$HPLPot$increase(void );
# 70 "/Users/wbennett/opt/MoteWorks/tos/system/PotM.nc"
uint8_t PotM$potSetting;

static inline void PotM$setPot(uint8_t value);
#line 85
static inline result_t PotM$Pot$init(uint8_t initialSetting);
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$decrease(void );








static inline result_t HPLPotC$Pot$increase(void );








static inline result_t HPLPotC$Pot$finalise(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static inline result_t HPLInit$init(void );
# 39 "/Users/wbennett/opt/MoteWorks/tos/interfaces/AllocationReq.nc"
static result_t FlashInitM$AllocationReq$request(uint32_t numBytesReq);
# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t FlashInitM$Leds$init(void );
#line 110
static result_t FlashInitM$Leds$yellowToggle(void );
#line 43
static result_t FlashInitM$Leds$redOn(void );
#line 68
static result_t FlashInitM$Leds$greenOn(void );
# 39 "WriteData.nc"
static result_t FlashInitM$WriteData$write(uint32_t offset, uint8_t *data, uint32_t numBytesWrite);
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.h"
enum FlashInitM$__nesc_unnamed4260 {
  FlashInitM$TOS_EEPROM_MAX_PAGES = 2048, 
  FlashInitM$TOS_EEPROM_PAGE_SIZE = 264, 
  FlashInitM$TOS_EEPROM_PAGE_SIZE_LOG2 = 8
};

enum FlashInitM$__nesc_unnamed4261 {
  FlashInitM$TOS_EEPROM_ERASE, 
  FlashInitM$TOS_EEPROM_DONT_ERASE, 
  FlashInitM$TOS_EEPROM_PREVIOUSLY_ERASED
};

typedef uint16_t FlashInitM$eeprompage_t;
typedef uint16_t FlashInitM$eeprompageoffset_t;
# 43 "/opt/local/lib/gcc/avr/4.1.2/include/stdarg.h" 3
typedef __builtin_va_list FlashInitM$__gnuc_va_list;
#line 105
typedef FlashInitM$__gnuc_va_list FlashInitM$va_list;
# 42 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/SOdebug.h"
bool FlashInitM$debugStarted;
static const char FlashInitM$hex[16] = "0123456789ABCDEF";



inline static void FlashInitM$init_debug(void );
#line 82
static void FlashInitM$UARTPutChar(char c);
#line 102
static inline int FlashInitM$so_printf(const uint8_t *format, ...);
# 26 "FlashInitM.nc"
uint8_t FlashInitM$buf[(uint32_t )256];
uint32_t FlashInitM$index;

static void FlashInitM$writeNew(void );





static inline result_t FlashInitM$StdControl$init(void );









static inline result_t FlashInitM$StdControl$start(void );









static result_t FlashInitM$AllocationReq$requestProcessed(result_t success);
#line 67
static inline result_t FlashInitM$WriteData$flushDone(result_t success);




static inline result_t FlashInitM$WriteData$writeDone(uint8_t *data, uint32_t numBytesWrite, result_t success);
# 28 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
uint8_t LedsC$ledsOn;

enum LedsC$__nesc_unnamed4262 {
  LedsC$RED_BIT = 1, 
  LedsC$GREEN_BIT = 2, 
  LedsC$YELLOW_BIT = 4
};

static inline result_t LedsC$Leds$init(void );
#line 50
static inline result_t LedsC$Leds$redOn(void );
#line 79
static inline result_t LedsC$Leds$greenOn(void );
#line 108
static inline result_t LedsC$Leds$yellowOn(void );








static inline result_t LedsC$Leds$yellowOff(void );








static inline result_t LedsC$Leds$yellowToggle(void );
# 23 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static bool PageEEPROMM$getCompareStatus(void );
# 22 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
static result_t PageEEPROMM$PageEEPROM$syncDone(result_t result);



static result_t PageEEPROMM$PageEEPROM$flushDone(result_t result);
#line 15
static result_t PageEEPROMM$PageEEPROM$writeDone(result_t result);


static result_t PageEEPROMM$PageEEPROM$eraseDone(result_t result);
#line 34
static result_t PageEEPROMM$PageEEPROM$computeCrcDone(result_t result, uint16_t crc);
#line 30
static result_t PageEEPROMM$PageEEPROM$readDone(result_t result);
# 30 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/SlavePin.nc"
static result_t PageEEPROMM$FlashSelect$low(void );









static result_t PageEEPROMM$FlashSelect$high(bool needEvent);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t PageEEPROMM$FlashControl$init(void );






static result_t PageEEPROMM$FlashControl$start(void );
# 38 "/Users/wbennett/opt/MoteWorks/tos/interfaces/FastSPI.nc"
static uint8_t PageEEPROMM$FlashSPI$txByte(uint8_t data);
# 11 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Resource.nc"
static result_t PageEEPROMM$FlashIdle$wait(void );
# 44 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
enum PageEEPROMM$__nesc_unnamed4263 {
  PageEEPROMM$IDLE, 
  PageEEPROMM$R_READ, 
  PageEEPROMM$R_READCRC, 
  PageEEPROMM$R_WRITE, 
  PageEEPROMM$R_ERASE, 
  PageEEPROMM$R_SYNC, 
  PageEEPROMM$R_SYNCALL, 
  PageEEPROMM$R_FLUSH, 
  PageEEPROMM$R_FLUSHALL
};
uint8_t PageEEPROMM$request;
uint8_t *PageEEPROMM$reqBuf;
eeprompageoffset_t PageEEPROMM$reqOffset;
#line 57
eeprompageoffset_t PageEEPROMM$reqBytes;
eeprompage_t PageEEPROMM$reqPage;

bool PageEEPROMM$deselectRequested;

bool PageEEPROMM$broken;
bool PageEEPROMM$compareOk;

enum PageEEPROMM$__nesc_unnamed4264 {
  PageEEPROMM$P_SEND_CMD, 
  PageEEPROMM$P_READ, 
  PageEEPROMM$P_READCRC, 
  PageEEPROMM$P_WRITE, 
  PageEEPROMM$P_FLUSH, 
  PageEEPROMM$P_FILL, 
  PageEEPROMM$P_ERASE, 
  PageEEPROMM$P_COMPARE, 
  PageEEPROMM$P_COMPARE_CHECK
};
uint8_t PageEEPROMM$cmdPhase;
#line 76
uint8_t PageEEPROMM$waiting;
uint8_t *PageEEPROMM$data;
#line 77
uint8_t PageEEPROMM$cmd[4];
uint8_t PageEEPROMM$cmdCount;
eeprompageoffset_t PageEEPROMM$dataCount;
uint16_t PageEEPROMM$computedCrc;







#line 82
struct PageEEPROMM$__nesc_unnamed4265 {
  eeprompage_t page;
  bool busy : 1;
  bool clean : 1;
  bool erased : 1;
  uint8_t unchecked : 2;
} PageEEPROMM$buffer[2];
uint8_t PageEEPROMM$selected;
uint8_t PageEEPROMM$checking;
bool PageEEPROMM$flashBusy;

enum PageEEPROMM$__nesc_unnamed4266 {
  PageEEPROMM$C_READ_BUFFER1 = 0xd4, 
  PageEEPROMM$C_READ_BUFFER2 = 0xd6, 
  PageEEPROMM$C_WRITE_BUFFER1 = 0x84, 
  PageEEPROMM$C_WRITE_BUFFER2 = 0x87, 
  PageEEPROMM$C_FILL_BUFFER1 = 0x53, 
  PageEEPROMM$C_FILL_BUFFER2 = 0x55, 
  PageEEPROMM$C_FLUSH_BUFFER1 = 0x83, 
  PageEEPROMM$C_FLUSH_BUFFER2 = 0x86, 
  PageEEPROMM$C_QFLUSH_BUFFER1 = 0x88, 
  PageEEPROMM$C_QFLUSH_BUFFER2 = 0x89, 
  PageEEPROMM$C_COMPARE_BUFFER1 = 0x60, 
  PageEEPROMM$C_COMPARE_BUFFER2 = 0x61, 
  PageEEPROMM$C_REQ_STATUS = 0xd7, 
  PageEEPROMM$C_ERASE_PAGE = 0x81
};




static inline result_t PageEEPROMM$StdControl$init(void );
#line 128
static inline result_t PageEEPROMM$StdControl$start(void );








static inline void PageEEPROMM$selectFlash(void );




static inline void PageEEPROMM$requestDeselect(void );




static result_t PageEEPROMM$FlashIdle$available(void );






static void PageEEPROMM$requestFlashStatus(void );









static void PageEEPROMM$sendFlashCommand(void );
#line 229
static void PageEEPROMM$requestDone(result_t result);

static inline void PageEEPROMM$taskSuccess(void );



static inline void PageEEPROMM$taskFail(void );



static void PageEEPROMM$handleRWRequest(void );
static void PageEEPROMM$execCommand(bool wait, uint8_t reqCmd, uint8_t dontCare, 
eeprompage_t page, eeprompageoffset_t offset);

static void PageEEPROMM$checkBuffer(uint8_t buf);






static void PageEEPROMM$flushBuffer(void );







static inline void PageEEPROMM$flashCommandComplete(void );
#line 341
static result_t PageEEPROMM$FlashSelect$notifyHigh(void );








static void PageEEPROMM$execCommand(bool wait, uint8_t reqCmd, uint8_t dontCare, 
eeprompage_t page, eeprompageoffset_t offset);
#line 365
static inline void PageEEPROMM$execRWBuffer(uint8_t reqCmd, uint8_t dontCare, eeprompageoffset_t offset);



static result_t PageEEPROMM$syncOrFlushAll(uint8_t newReq);

static void PageEEPROMM$handleRWRequest(void );
#line 460
static void PageEEPROMM$requestDone(result_t result);
#line 475
static result_t PageEEPROMM$newRequest(uint8_t req, eeprompage_t page, 
eeprompageoffset_t offset, 
void *reqdata, eeprompageoffset_t n);
#line 511
static inline result_t PageEEPROMM$PageEEPROM$read(eeprompage_t page, eeprompageoffset_t offset, 
void *reqdata, eeprompageoffset_t n);
#line 530
static inline result_t PageEEPROMM$PageEEPROM$write(eeprompage_t page, eeprompageoffset_t offset, 
void *reqdata, eeprompageoffset_t n);
#line 566
static inline result_t PageEEPROMM$PageEEPROM$erase(eeprompage_t page, uint8_t eraseKind);



static inline result_t PageEEPROMM$syncOrFlush(eeprompage_t page, uint8_t newReq);
#line 600
static inline result_t PageEEPROMM$PageEEPROM$flush(eeprompage_t page);



static result_t PageEEPROMM$syncOrFlushAll(uint8_t newReq);
#line 634
static inline result_t PageEEPROMM$PageEEPROM$flushAll(void );
# 22 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
static result_t PageEEPROMShare$PageEEPROM$syncDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 22 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result);



static result_t PageEEPROMShare$PageEEPROM$flushDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 26 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result);
#line 15
static result_t PageEEPROMShare$PageEEPROM$writeDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result);


static result_t PageEEPROMShare$PageEEPROM$eraseDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 18 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result);
#line 34
static result_t PageEEPROMShare$PageEEPROM$computeCrcDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 34 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result, uint16_t crc);
#line 30
static result_t PageEEPROMShare$PageEEPROM$readDone(
# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
uint8_t arg_0x1013ea020, 
# 30 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
result_t result);
#line 28
static result_t PageEEPROMShare$ActualEEPROM$read(eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n);
#line 17
static result_t PageEEPROMShare$ActualEEPROM$erase(eeprompage_t page, uint8_t eraseKind);






static result_t PageEEPROMShare$ActualEEPROM$flush(eeprompage_t page);
static result_t PageEEPROMShare$ActualEEPROM$flushAll(void );
#line 13
static result_t PageEEPROMShare$ActualEEPROM$write(eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n);
# 19 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
enum PageEEPROMShare$__nesc_unnamed4267 {
  PageEEPROMShare$NCLIENTS = 1U
};
uint8_t PageEEPROMShare$lastClient;



static inline int PageEEPROMShare$setClient(uint8_t client);









static inline uint8_t PageEEPROMShare$getClient(void );
#line 49
static result_t PageEEPROMShare$check(result_t requestOk);








static __inline result_t PageEEPROMShare$PageEEPROM$write(uint8_t client, eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n);





static __inline result_t PageEEPROMShare$ActualEEPROM$writeDone(result_t result);



static __inline result_t PageEEPROMShare$PageEEPROM$erase(uint8_t client, eeprompage_t page, uint8_t eraseKind);





static __inline result_t PageEEPROMShare$ActualEEPROM$eraseDone(result_t result);
#line 91
static __inline result_t PageEEPROMShare$ActualEEPROM$syncDone(result_t result);



static __inline result_t PageEEPROMShare$PageEEPROM$flush(uint8_t client, eeprompage_t page);





static __inline result_t PageEEPROMShare$PageEEPROM$flushAll(uint8_t client);





static __inline result_t PageEEPROMShare$ActualEEPROM$flushDone(result_t result);



static __inline result_t PageEEPROMShare$PageEEPROM$read(uint8_t client, eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n);





static __inline result_t PageEEPROMShare$ActualEEPROM$readDone(result_t result);










static __inline result_t PageEEPROMShare$ActualEEPROM$computeCrcDone(result_t result, uint16_t crc);



static inline result_t PageEEPROMShare$PageEEPROM$default$writeDone(uint8_t client, result_t result);



static inline result_t PageEEPROMShare$PageEEPROM$default$eraseDone(uint8_t client, result_t result);



static inline result_t PageEEPROMShare$PageEEPROM$default$syncDone(uint8_t client, result_t result);



static inline result_t PageEEPROMShare$PageEEPROM$default$flushDone(uint8_t client, result_t result);



static inline result_t PageEEPROMShare$PageEEPROM$default$readDone(uint8_t client, result_t result);



static inline result_t PageEEPROMShare$PageEEPROM$default$computeCrcDone(uint8_t client, result_t result, uint16_t crc);
# 45 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/SlavePin.nc"
static result_t HPLFlash$FlashSelect$notifyHigh(void );
# 12 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Resource.nc"
static result_t HPLFlash$FlashIdle$available(void );
# 38 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLFlash.nc"
static inline result_t HPLFlash$FlashControl$init(void );
#line 51
static inline result_t HPLFlash$FlashControl$start(void );








static inline result_t HPLFlash$FlashSelect$low(void );





static inline void HPLFlash$sigHigh(void );



static inline result_t HPLFlash$FlashSelect$high(bool needEvent);
#line 90
static __inline uint8_t HPLFlash$FlashSPI$txByte(uint8_t spiOut);
#line 111
static void HPLFlash$idleWait(void );






static inline result_t HPLFlash$FlashIdle$wait(void );
#line 130
static inline bool HPLFlash$getCompareStatus(void );
# 43 "ByteEEPROMC.nc"
static RegionSpecifier *ByteEEPROMC$getRegion(uint8_t id);
# 28 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
static result_t ByteEEPROMC$PageEEPROM$read(eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n);
#line 17
static result_t ByteEEPROMC$PageEEPROM$erase(eeprompage_t page, uint8_t eraseKind);






static result_t ByteEEPROMC$PageEEPROM$flush(eeprompage_t page);
static result_t ByteEEPROMC$PageEEPROM$flushAll(void );
#line 13
static result_t ByteEEPROMC$PageEEPROM$write(eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n);
# 83 "/Users/wbennett/opt/MoteWorks/tos/interfaces/LogData.nc"
static result_t ByteEEPROMC$LogData$syncDone(
# 39 "ByteEEPROMC.nc"
uint8_t arg_0x10146ea90, 
# 83 "/Users/wbennett/opt/MoteWorks/tos/interfaces/LogData.nc"
result_t success);
#line 45
static result_t ByteEEPROMC$LogData$eraseDone(
# 39 "ByteEEPROMC.nc"
uint8_t arg_0x10146ea90, 
# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/LogData.nc"
result_t success);
#line 61
static result_t ByteEEPROMC$LogData$appendDone(
# 39 "ByteEEPROMC.nc"
uint8_t arg_0x10146ea90, 
# 61 "/Users/wbennett/opt/MoteWorks/tos/interfaces/LogData.nc"
uint8_t *data, uint32_t numBytes, result_t success);
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReadData.nc"
static result_t ByteEEPROMC$ReadData$readDone(
# 40 "ByteEEPROMC.nc"
uint8_t arg_0x101469020, 
# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReadData.nc"
uint8_t *buffer, uint32_t numBytesRead, result_t success);
# 45 "WriteData.nc"
static result_t ByteEEPROMC$WriteData$flushDone(
# 38 "ByteEEPROMC.nc"
uint8_t arg_0x10146aad8, 
# 45 "WriteData.nc"
result_t success);








static result_t ByteEEPROMC$WriteData$writeDone(
# 38 "ByteEEPROMC.nc"
uint8_t arg_0x10146aad8, 
# 54 "WriteData.nc"
uint8_t *data, uint32_t numBytesWrite, result_t success);
# 49 "ByteEEPROMC.nc"
enum ByteEEPROMC$__nesc_unnamed4268 {
  ByteEEPROMC$S_IDLE, 
  ByteEEPROMC$S_READ, 
  ByteEEPROMC$S_WRITE, 
  ByteEEPROMC$S_APPEND, 
  ByteEEPROMC$S_SYNC, 
  ByteEEPROMC$S_ERASE, 
  ByteEEPROMC$S_FLUSH
};

uint8_t ByteEEPROMC$state;

uint8_t ByteEEPROMC$appID;
uint32_t ByteEEPROMC$startAddr;
uint32_t ByteEEPROMC$stopAddr;
uint32_t ByteEEPROMC$numBytes;
uint32_t ByteEEPROMC$dataBufferOffset;
uint8_t *ByteEEPROMC$dataBuffer;
bool ByteEEPROMC$writesLastByte;

enum ByteEEPROMC$__nesc_unnamed4269 {
  ByteEEPROMC$NREGIONS = 1U, 
  ByteEEPROMC$PAGE_SIZE = 1 << TOS_EEPROM_PAGE_SIZE_LOG2, 
  ByteEEPROMC$PAGE_SIZE_MASK = ByteEEPROMC$PAGE_SIZE - 1
};



uint32_t ByteEEPROMC$appendOffset[ByteEEPROMC$NREGIONS];

static inline RegionSpecifier *ByteEEPROMC$newRequest(uint8_t clientId);








static inline result_t ByteEEPROMC$newBufferRequest(uint8_t clientId, uint32_t offset, 
uint8_t *buffer, uint32_t count);
#line 127
static void ByteEEPROMC$completeOp(result_t success);
#line 161
static inline void ByteEEPROMC$successTask(void );



static inline void ByteEEPROMC$failTask(void );



static inline void ByteEEPROMC$check(result_t success);




static void ByteEEPROMC$continueOp(void );
#line 216
static inline result_t ByteEEPROMC$PageEEPROM$readDone(result_t success);







static inline result_t ByteEEPROMC$PageEEPROM$writeDone(result_t success);
#line 244
static inline result_t ByteEEPROMC$PageEEPROM$flushDone(result_t result);
#line 286
static inline result_t ByteEEPROMC$WriteData$write(uint8_t id, uint32_t offset, uint8_t *buffer, uint32_t numBytesWrite);
#line 324
static inline result_t ByteEEPROMC$PageEEPROM$syncDone(result_t result);
#line 347
static inline result_t ByteEEPROMC$PageEEPROM$eraseDone(result_t success);
#line 365
static inline result_t ByteEEPROMC$WriteData$default$writeDone(uint8_t id, uint8_t *data, uint32_t numBytesWrite, result_t success);



static inline result_t ByteEEPROMC$WriteData$default$flushDone(uint8_t id, result_t succes);



static inline result_t ByteEEPROMC$ReadData$default$readDone(uint8_t id, uint8_t *buffer, uint32_t numBytesRead, result_t success);



static inline result_t ByteEEPROMC$LogData$default$appendDone(uint8_t id, uint8_t *data, uint32_t numBytesWrite, result_t success);



static inline result_t ByteEEPROMC$LogData$default$eraseDone(uint8_t id, result_t success);



static inline result_t ByteEEPROMC$LogData$default$syncDone(uint8_t id, result_t success);



static inline result_t ByteEEPROMC$PageEEPROM$computeCrcDone(result_t result, uint16_t crc);
# 58 "/Users/wbennett/opt/MoteWorks/tos/interfaces/AllocationReq.nc"
static result_t ByteEEPROMAllocate$AllocationReq$requestProcessed(
# 12 "/Users/wbennett/opt/MoteWorks/tos/system/ByteEEPROMAllocate.nc"
uint8_t arg_0x10146f880, 
# 58 "/Users/wbennett/opt/MoteWorks/tos/interfaces/AllocationReq.nc"
result_t success);
# 18 "/Users/wbennett/opt/MoteWorks/tos/system/ByteEEPROMAllocate.nc"
bool ByteEEPROMAllocate$allocated;

enum ByteEEPROMAllocate$__nesc_unnamed4270 {
  ByteEEPROMAllocate$NREGIONS = 1U
};

RegionSpecifier ByteEEPROMAllocate$regions[ByteEEPROMAllocate$NREGIONS];
RegionSpecifier *ByteEEPROMAllocate$allocatedHead;

static inline RegionSpecifier *ByteEEPROMAllocate$getRegion(uint8_t id);









static inline result_t ByteEEPROMAllocate$StdControl$init(void );



static inline void ByteEEPROMAllocate$addAllocatedRegion(RegionSpecifier *currentRequest, 
RegionSpecifier **allocatedRegion);






static __inline uint32_t ByteEEPROMAllocate$alignup(uint32_t value, uint32_t alignment);



static inline result_t ByteEEPROMAllocate$findFreeRegionAddrAndAlloc(RegionSpecifier *currentRequest);
#line 92
static inline result_t ByteEEPROMAllocate$findFreeRegionAndAlloc(RegionSpecifier *currentRequest);
#line 129
static inline result_t ByteEEPROMAllocate$StdControl$start(void );
#line 172
static inline result_t ByteEEPROMAllocate$AllocationReq$request(uint8_t id, uint32_t numBytesReq);
#line 200
static inline result_t ByteEEPROMAllocate$AllocationReq$default$requestProcessed(uint8_t id, result_t success);
# 64 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_SET_GREEN_LED_PIN()
#line 64
{
#line 64
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 1;
}

#line 65
static __inline void TOSH_SET_YELLOW_LED_PIN()
#line 65
{
#line 65
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 0;
}

#line 63
static __inline void TOSH_SET_RED_LED_PIN()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 2;
}

#line 79
static __inline void TOSH_SET_FLASH_SELECT_PIN()
#line 79
{
#line 79
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 3;
}

#line 80
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT()
#line 80
{
#line 80
  * (volatile uint8_t *)(0x0A + 0x20) |= 1 << 5;
}

#line 81
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT()
#line 81
{
#line 81
  * (volatile uint8_t *)(0x0A + 0x20) |= 1 << 3;
}

#line 79
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT()
#line 79
{
#line 79
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 3;
}

#line 67
static __inline void TOSH_CLR_SERIAL_ID_PIN()
#line 67
{
#line 67
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 4);
}

#line 67
static __inline void TOSH_MAKE_SERIAL_ID_INPUT()
#line 67
{
#line 67
  * (volatile uint8_t *)(0X01 + 0x20) &= ~(1 << 4);
}

#line 98
static __inline void TOSH_MAKE_PW0_OUTPUT()
#line 98
{
#line 98
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 0;
}

#line 99
static __inline void TOSH_MAKE_PW1_OUTPUT()
#line 99
{
#line 99
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 1;
}

#line 100
static __inline void TOSH_MAKE_PW2_OUTPUT()
#line 100
{
#line 100
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 2;
}

#line 101
static __inline void TOSH_MAKE_PW3_OUTPUT()
#line 101
{
#line 101
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 3;
}

#line 102
static __inline void TOSH_MAKE_PW4_OUTPUT()
#line 102
{
#line 102
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 4;
}

#line 103
static __inline void TOSH_MAKE_PW5_OUTPUT()
#line 103
{
#line 103
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 5;
}

#line 104
static __inline void TOSH_MAKE_PW6_OUTPUT()
#line 104
{
#line 104
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 6;
}

#line 105
static __inline void TOSH_MAKE_PW7_OUTPUT()
#line 105
{
#line 105
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 7;
}

#line 64
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT()
#line 64
{
#line 64
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 1;
}

#line 65
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT()
#line 65
{
#line 65
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 0;
}

#line 63
static __inline void TOSH_MAKE_RED_LED_OUTPUT()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 2;
}

#line 134
static inline void TOSH_SET_PIN_DIRECTIONS(void )
{







  TOSH_MAKE_RED_LED_OUTPUT();
  TOSH_MAKE_YELLOW_LED_OUTPUT();
  TOSH_MAKE_GREEN_LED_OUTPUT();


  TOSH_MAKE_PW7_OUTPUT();
  TOSH_MAKE_PW6_OUTPUT();
  TOSH_MAKE_PW5_OUTPUT();
  TOSH_MAKE_PW4_OUTPUT();
  TOSH_MAKE_PW3_OUTPUT();
  TOSH_MAKE_PW2_OUTPUT();
  TOSH_MAKE_PW1_OUTPUT();
  TOSH_MAKE_PW0_OUTPUT();
#line 173
  TOSH_MAKE_SERIAL_ID_INPUT();
  TOSH_CLR_SERIAL_ID_PIN();

  TOSH_MAKE_FLASH_SELECT_OUTPUT();
  TOSH_MAKE_FLASH_OUT_OUTPUT();
  TOSH_MAKE_FLASH_CLK_OUTPUT();
  TOSH_SET_FLASH_SELECT_PIN();

  TOSH_SET_RED_LED_PIN();
  TOSH_SET_YELLOW_LED_PIN();
  TOSH_SET_GREEN_LED_PIN();
}

# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static inline result_t HPLInit$init(void )
#line 37
{
  TOSH_SET_PIN_DIRECTIONS();
  return SUCCESS;
}

# 27 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
inline static result_t RealMain$hardwareInit(void ){
#line 27
  unsigned char __nesc_result;
#line 27

#line 27
  __nesc_result = HPLInit$init();
#line 27

#line 27
  return __nesc_result;
#line 27
}
#line 27
# 55 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$finalise(void )
#line 55
{


  return SUCCESS;
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$finalise(void ){
#line 53
  unsigned char __nesc_result;
#line 53

#line 53
  __nesc_result = HPLPotC$Pot$finalise();
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 46 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$increase(void )
#line 46
{





  return SUCCESS;
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$increase(void ){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = HPLPotC$Pot$increase();
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$decrease(void )
#line 37
{





  return SUCCESS;
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$decrease(void ){
#line 38
  unsigned char __nesc_result;
#line 38

#line 38
  __nesc_result = HPLPotC$Pot$decrease();
#line 38

#line 38
  return __nesc_result;
#line 38
}
#line 38
# 72 "/Users/wbennett/opt/MoteWorks/tos/system/PotM.nc"
static inline void PotM$setPot(uint8_t value)
#line 72
{
  uint8_t i;

#line 74
  for (i = 0; i < 151; i++) 
    PotM$HPLPot$decrease();

  for (i = 0; i < value; i++) 
    PotM$HPLPot$increase();

  PotM$HPLPot$finalise();

  PotM$potSetting = value;
}

static inline result_t PotM$Pot$init(uint8_t initialSetting)
#line 85
{
  PotM$setPot(initialSetting);
  return SUCCESS;
}

# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
inline static result_t RealMain$Pot$init(uint8_t initialSetting){
#line 57
  unsigned char __nesc_result;
#line 57

#line 57
  __nesc_result = PotM$Pot$init(initialSetting);
#line 57

#line 57
  return __nesc_result;
#line 57
}
#line 57
# 59 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
static inline void TOSH_sched_init(void )
{
  int i;

#line 62
  TOSH_sched_free = 0;
  TOSH_sched_full = 0;
  for (i = 0; i < TOSH_MAX_TASKS; i++) 
    TOSH_queue[i].tp = (void *)0;
}

# 158 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline result_t rcombine(result_t r1, result_t r2)



{
  return r1 == FAIL ? FAIL : r2;
}

# 172 "/Users/wbennett/opt/MoteWorks/tos/system/ByteEEPROMAllocate.nc"
static inline result_t ByteEEPROMAllocate$AllocationReq$request(uint8_t id, uint32_t numBytesReq)
#line 172
{
  RegionSpecifier *allocate = &ByteEEPROMAllocate$regions[id];


  if (ByteEEPROMAllocate$allocated || numBytesReq == 0) {
    return FAIL;
    }
  allocate->startByte = 0xffffffff;
  allocate->stopByte = numBytesReq;

  return SUCCESS;
}

# 39 "/Users/wbennett/opt/MoteWorks/tos/interfaces/AllocationReq.nc"
inline static result_t FlashInitM$AllocationReq$request(uint32_t numBytesReq){
#line 39
  unsigned char __nesc_result;
#line 39

#line 39
  __nesc_result = ByteEEPROMAllocate$AllocationReq$request(FlashInit$BYTE_EEPROM_ID, numBytesReq);
#line 39

#line 39
  return __nesc_result;
#line 39
}
#line 39
# 197 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline void *nmemset(void *to, int val, size_t n)
{
  char *cto = to;

  while (n--) * cto++ = val;

  return to;
}

# 36 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$init(void )
#line 36
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 37
    {
      LedsC$ledsOn = 0;
      {
      }
#line 39
      ;
      TOSH_MAKE_RED_LED_OUTPUT();
      TOSH_MAKE_YELLOW_LED_OUTPUT();
      TOSH_MAKE_GREEN_LED_OUTPUT();
      TOSH_SET_RED_LED_PIN();
      TOSH_SET_YELLOW_LED_PIN();
      TOSH_SET_GREEN_LED_PIN();
    }
#line 46
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t FlashInitM$Leds$init(void ){
#line 35
  unsigned char __nesc_result;
#line 35

#line 35
  __nesc_result = LedsC$Leds$init();
#line 35

#line 35
  return __nesc_result;
#line 35
}
#line 35
# 35 "FlashInitM.nc"
static inline result_t FlashInitM$StdControl$init(void )
{
  FlashInitM$Leds$init();
  nmemset(FlashInitM$buf, 0x00, (uint32_t )256);
  FlashInitM$index = 0;
  FlashInitM$AllocationReq$request((uint32_t )((uint32_t )FlashInitM$TOS_EEPROM_MAX_PAGES << (uint32_t )FlashInitM$TOS_EEPROM_PAGE_SIZE_LOG2));

  return SUCCESS;
}

# 82 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_MAKE_FLASH_IN_INPUT()
#line 82
{
#line 82
  * (volatile uint8_t *)(0x0A + 0x20) &= ~(1 << 2);
}

#line 82
static __inline void TOSH_CLR_FLASH_IN_PIN()
#line 82
{
#line 82
  * (volatile uint8_t *)(0x0B + 0x20) &= ~(1 << 2);
}

#line 81
static __inline void TOSH_SET_FLASH_OUT_PIN()
#line 81
{
#line 81
  * (volatile uint8_t *)(0x0B + 0x20) |= 1 << 3;
}

#line 80
static __inline void TOSH_CLR_FLASH_CLK_PIN()
#line 80
{
#line 80
  * (volatile uint8_t *)(0x0B + 0x20) &= ~(1 << 5);
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLFlash.nc"
static inline result_t HPLFlash$FlashControl$init(void )
#line 38
{
  TOSH_MAKE_FLASH_SELECT_OUTPUT();
  TOSH_SET_FLASH_SELECT_PIN();
  TOSH_CLR_FLASH_CLK_PIN();
  TOSH_MAKE_FLASH_CLK_OUTPUT();
  TOSH_SET_FLASH_OUT_PIN();
  TOSH_MAKE_FLASH_OUT_OUTPUT();
  TOSH_CLR_FLASH_IN_PIN();
  TOSH_MAKE_FLASH_IN_INPUT();

  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t PageEEPROMM$FlashControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = HPLFlash$FlashControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 113 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static inline result_t PageEEPROMM$StdControl$init(void )
#line 113
{
  PageEEPROMM$request = PageEEPROMM$IDLE;
  PageEEPROMM$waiting = PageEEPROMM$deselectRequested = FALSE;
  PageEEPROMM$flashBusy = TRUE;


  PageEEPROMM$buffer[0].page = PageEEPROMM$buffer[1].page = TOS_EEPROM_MAX_PAGES;
  PageEEPROMM$buffer[0].busy = PageEEPROMM$buffer[1].busy = FALSE;
  PageEEPROMM$buffer[0].clean = PageEEPROMM$buffer[1].clean = TRUE;
  PageEEPROMM$buffer[0].unchecked = PageEEPROMM$buffer[1].unchecked = 0;
  PageEEPROMM$buffer[0].erased = PageEEPROMM$buffer[1].erased = FALSE;

  return PageEEPROMM$FlashControl$init();
}

# 37 "/Users/wbennett/opt/MoteWorks/tos/system/ByteEEPROMAllocate.nc"
static inline result_t ByteEEPROMAllocate$StdControl$init(void )
#line 37
{
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RealMain$StdControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = ByteEEPROMAllocate$StdControl$init();
#line 41
  __nesc_result = rcombine(__nesc_result, PageEEPROMM$StdControl$init());
#line 41
  __nesc_result = rcombine(__nesc_result, FlashInitM$StdControl$init());
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 45 "FlashInitM.nc"
static inline result_t FlashInitM$StdControl$start(void )
{
  return SUCCESS;
}

# 51 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLFlash.nc"
static inline result_t HPLFlash$FlashControl$start(void )
#line 51
{
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t PageEEPROMM$FlashControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = HPLFlash$FlashControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 128 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static inline result_t PageEEPROMM$StdControl$start(void )
#line 128
{
  return PageEEPROMM$FlashControl$start();
}

# 200 "/Users/wbennett/opt/MoteWorks/tos/system/ByteEEPROMAllocate.nc"
static inline result_t ByteEEPROMAllocate$AllocationReq$default$requestProcessed(uint8_t id, result_t success)
#line 200
{
  return SUCCESS;
}

# 58 "/Users/wbennett/opt/MoteWorks/tos/interfaces/AllocationReq.nc"
inline static result_t ByteEEPROMAllocate$AllocationReq$requestProcessed(uint8_t arg_0x10146f880, result_t success){
#line 58
  unsigned char __nesc_result;
#line 58

#line 58
  switch (arg_0x10146f880) {
#line 58
    case FlashInit$BYTE_EEPROM_ID:
#line 58
      __nesc_result = FlashInitM$AllocationReq$requestProcessed(success);
#line 58
      break;
#line 58
    default:
#line 58
      __nesc_result = ByteEEPROMAllocate$AllocationReq$default$requestProcessed(arg_0x10146f880, success);
#line 58
      break;
#line 58
    }
#line 58

#line 58
  return __nesc_result;
#line 58
}
#line 58
# 41 "/Users/wbennett/opt/MoteWorks/tos/system/ByteEEPROMAllocate.nc"
static inline void ByteEEPROMAllocate$addAllocatedRegion(RegionSpecifier *currentRequest, 
RegionSpecifier **allocatedRegion)
#line 42
{
  currentRequest->next = *allocatedRegion;
  *allocatedRegion = currentRequest;
}



static __inline uint32_t ByteEEPROMAllocate$alignup(uint32_t value, uint32_t alignment)
#line 49
{
  return (value + (alignment - 1)) & ~(alignment - 1);
}

#line 92
static inline result_t ByteEEPROMAllocate$findFreeRegionAndAlloc(RegionSpecifier *currentRequest)
#line 92
{
  RegionSpecifier **allocatedRegion = &ByteEEPROMAllocate$allocatedHead;
  uint32_t startByte = 0;
  uint32_t stopByte;

  while ((void *)0 != *allocatedRegion) {
      stopByte = (*allocatedRegion)->startByte;

      if (stopByte - startByte >= currentRequest->stopByte) {
          currentRequest->startByte = startByte;
          currentRequest->stopByte = startByte + currentRequest->stopByte;

          ByteEEPROMAllocate$addAllocatedRegion(currentRequest, allocatedRegion);

          return SUCCESS;
        }
      startByte = ByteEEPROMAllocate$alignup((*allocatedRegion)->stopByte, TOS_BYTEEEPROM_PAGESIZE);
      allocatedRegion = & (*allocatedRegion)->next;
    }

  stopByte = TOS_BYTEEEPROM_LASTBYTE;

  if (stopByte - startByte >= currentRequest->stopByte) {
      currentRequest->startByte = startByte;
      currentRequest->stopByte = startByte + currentRequest->stopByte;

      ByteEEPROMAllocate$addAllocatedRegion(currentRequest, allocatedRegion);

      return SUCCESS;
    }


  currentRequest->startByte = currentRequest->stopByte = 0;

  return FAIL;
}

#line 53
static inline result_t ByteEEPROMAllocate$findFreeRegionAddrAndAlloc(RegionSpecifier *currentRequest)
#line 53
{
  RegionSpecifier **allocatedRegion = &ByteEEPROMAllocate$allocatedHead;
  uint32_t startByte = 0;
  uint32_t stopByte;

  while ((void *)0 != *allocatedRegion) {
      stopByte = (*allocatedRegion)->startByte;


      if (
#line 61
      currentRequest->startByte >= startByte && 
      currentRequest->startByte < stopByte && (
      currentRequest->stopByte >= startByte && 
      currentRequest->stopByte <= stopByte)) {
          ByteEEPROMAllocate$addAllocatedRegion(currentRequest, allocatedRegion);

          return SUCCESS;
        }

      startByte = ByteEEPROMAllocate$alignup((*allocatedRegion)->stopByte, TOS_BYTEEEPROM_PAGESIZE);
      allocatedRegion = & (*allocatedRegion)->next;
    }

  stopByte = TOS_BYTEEEPROM_LASTBYTE;


  if (
#line 76
  currentRequest->startByte >= startByte && 
  currentRequest->startByte < stopByte && (
  currentRequest->stopByte >= startByte && 
  currentRequest->stopByte <= stopByte)) {
      ByteEEPROMAllocate$addAllocatedRegion(currentRequest, allocatedRegion);

      return SUCCESS;
    }


  currentRequest->startByte = currentRequest->stopByte = 0;
  return FAIL;
}

#line 129
static inline result_t ByteEEPROMAllocate$StdControl$start(void )
#line 129
{
  if (!ByteEEPROMAllocate$allocated) 
    {
      result_t success;
      uint8_t i;


      for (i = 0; i < ByteEEPROMAllocate$NREGIONS; i++) 
        {
          RegionSpecifier *region = &ByteEEPROMAllocate$regions[i];



          if (region->next) 
            {
              success = ByteEEPROMAllocate$findFreeRegionAddrAndAlloc(region);
              ByteEEPROMAllocate$AllocationReq$requestProcessed(i, success);
            }
        }


      for (i = 0; i < ByteEEPROMAllocate$NREGIONS; i++) 
        {
          RegionSpecifier *region = &ByteEEPROMAllocate$regions[i];



          if (region->startByte == 0xffffffff) 
            {
              success = ByteEEPROMAllocate$findFreeRegionAndAlloc(region);
              ByteEEPROMAllocate$AllocationReq$requestProcessed(i, success);
            }
        }

      ByteEEPROMAllocate$allocated = TRUE;
    }
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RealMain$StdControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = ByteEEPROMAllocate$StdControl$start();
#line 48
  __nesc_result = rcombine(__nesc_result, PageEEPROMM$StdControl$start());
#line 48
  __nesc_result = rcombine(__nesc_result, FlashInitM$StdControl$start());
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 64 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_GREEN_LED_PIN()
#line 64
{
#line 64
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 1);
}

# 79 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$greenOn(void )
#line 79
{
  {
  }
#line 80
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 81
    {
      TOSH_CLR_GREEN_LED_PIN();
      LedsC$ledsOn |= LedsC$GREEN_BIT;
    }
#line 84
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 68 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t FlashInitM$Leds$greenOn(void ){
#line 68
  unsigned char __nesc_result;
#line 68

#line 68
  __nesc_result = LedsC$Leds$greenOn();
#line 68

#line 68
  return __nesc_result;
#line 68
}
#line 68
# 47 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/SOdebug.h"
inline static void FlashInitM$init_debug(void )
#line 47
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 48
    {
      * (volatile uint8_t *)0xC5 = 0;



      * (volatile uint8_t *)0xC4 = 7;
      * (volatile uint8_t *)0xC0 = 1 << 1;







      * (volatile uint8_t *)0xC2 = (1 << 2) | (1 << 1);
      * (volatile uint8_t *)0XC6;
      * (volatile uint8_t *)0XC1 = 1 << 3;
    }
#line 65
    __nesc_atomic_end(__nesc_atomic); }
}

#line 102
static inline int FlashInitM$so_printf(const uint8_t *format, ...)
{
  uint8_t format_flag;
  uint32_t u_val = 0;
  uint32_t div_val;
  uint8_t base;
  uint8_t *ptr;
  bool longNumber = FALSE;
  FlashInitM$va_list ap;

  __builtin_va_start(ap, format);
  if (!FlashInitM$debugStarted) {
      FlashInitM$init_debug();
      FlashInitM$debugStarted = 1;
    }
#line 116
  ;
  if (format == (void *)0) {
#line 117
    format = "NULL\n";
    }
#line 118
  for (; ; ) 
    {
      if (!longNumber) {
          while ((format_flag = * format++) != '%') 
            {
              if (!format_flag) {
                  return 0;
                }
#line 125
              ;
              FlashInitM$UARTPutChar(format_flag);
            }
#line 127
          ;
        }
#line 128
      ;
      switch ((format_flag = * format++)) {
          case 'c': 
            format_flag = (__builtin_va_arg(ap, int ));
          default: 
            FlashInitM$UARTPutChar(format_flag);
          continue;

          case 'S': 
            case 's': 
              ptr = (uint8_t *)(__builtin_va_arg(ap, char *));
          while ((format_flag = * ptr++) != 0) {
              FlashInitM$UARTPutChar(format_flag);
            }
#line 141
          ;
          continue;
#line 207
          case 'l': 
            longNumber = TRUE;
          continue;

          case 'o': 
            base = 8;
          if (!longNumber) {
            div_val = 0x8000;
            }
          else {
#line 216
            div_val = 0x40000000;
            }
#line 217
          goto CONVERSION_LOOP;

          case 'u': 
            case 'i': 
              case 'd': 
                base = 10;
          if (!longNumber) {
            div_val = 10000;
            }
          else {
#line 226
            div_val = 1000000000;
            }
#line 227
          goto CONVERSION_LOOP;

          case 'x': 
            base = 16;
          if (!longNumber) {
            div_val = 0x1000;
            }
          else {
#line 234
            div_val = 0x10000000;
            }
          CONVERSION_LOOP: 
            {
              if (!longNumber) {
                u_val = (__builtin_va_arg(ap, int ));
                }
              else {
#line 241
                u_val = (__builtin_va_arg(ap, long ));
                }
#line 242
              if (format_flag == 'd' || format_flag == 'i') {
                  bool isNegative;

#line 244
                  if (!longNumber) {
                    isNegative = (int )u_val < 0;
                    }
                  else {
#line 247
                    isNegative = (long )u_val < 0;
                    }
#line 248
                  if (isNegative) {
                      u_val = -u_val;
                      FlashInitM$UARTPutChar('-');
                    }
#line 251
                  ;
                  while (div_val > 1 && div_val > u_val) {
                      div_val /= 10;
                    }
#line 254
                  ;
                }

              if (format_flag == 'x' && !longNumber) {
#line 257
                u_val &= 0xffff;
                }
#line 258
              do {
                  FlashInitM$UARTPutChar(FlashInitM$hex[u_val / div_val]);
                  u_val %= div_val;
                  div_val /= base;
                }
              while (div_val);
              longNumber = FALSE;
            }
#line 265
          ;
          break;
        }
#line 267
      ;
    }
#line 268
  ;
}

# 27 "/Users/wbennett/opt/MoteWorks/tos/system/ByteEEPROMAllocate.nc"
static inline RegionSpecifier *ByteEEPROMAllocate$getRegion(uint8_t id)
#line 27
{


  if (!ByteEEPROMAllocate$allocated || id >= ByteEEPROMAllocate$NREGIONS) {
    return (void *)0;
    }
#line 32
  if (ByteEEPROMAllocate$regions[id].startByte == ByteEEPROMAllocate$regions[id].stopByte) {
    return (void *)0;
    }
#line 34
  return &ByteEEPROMAllocate$regions[id];
}

# 43 "ByteEEPROMC.nc"
inline static RegionSpecifier *ByteEEPROMC$getRegion(uint8_t id){
#line 43
  struct RegionSpecifier_t *__nesc_result;
#line 43

#line 43
  __nesc_result = ByteEEPROMAllocate$getRegion(id);
#line 43

#line 43
  return __nesc_result;
#line 43
}
#line 43
#line 79
static inline RegionSpecifier *ByteEEPROMC$newRequest(uint8_t clientId)
#line 79
{
  if (ByteEEPROMC$S_IDLE != ByteEEPROMC$state) {
    return (void *)0;
    }
  ByteEEPROMC$appID = clientId;

  return ByteEEPROMC$getRegion(clientId);
}

static inline result_t ByteEEPROMC$newBufferRequest(uint8_t clientId, uint32_t offset, 
uint8_t *buffer, uint32_t count)
#line 89
{
  RegionSpecifier *mappedRegion = ByteEEPROMC$newRequest(clientId);

  if (mappedRegion == (void *)0) {
    return FAIL;
    }

  ByteEEPROMC$startAddr = mappedRegion->startByte + offset;

  ByteEEPROMC$stopAddr = mappedRegion->startByte + offset + count;


  if (ByteEEPROMC$startAddr < mappedRegion->startByte || 
  ByteEEPROMC$startAddr >= mappedRegion->stopByte) 
    {

      return FAIL;
    }
  if (ByteEEPROMC$stopAddr <= mappedRegion->startByte || 
  ByteEEPROMC$stopAddr > mappedRegion->stopByte) 
    {

      return FAIL;
    }




  ByteEEPROMC$writesLastByte = ByteEEPROMC$stopAddr == mappedRegion->stopByte;


  ByteEEPROMC$numBytes = count;
  ByteEEPROMC$dataBuffer = buffer;
  ByteEEPROMC$dataBufferOffset = 0;

  return SUCCESS;
}

#line 286
static inline result_t ByteEEPROMC$WriteData$write(uint8_t id, uint32_t offset, uint8_t *buffer, uint32_t numBytesWrite)
#line 286
{
  if (ByteEEPROMC$newBufferRequest(id, offset, buffer, numBytesWrite) == FAIL) {
    return FAIL;
    }
  ByteEEPROMC$state = ByteEEPROMC$S_WRITE;
  ByteEEPROMC$continueOp();

  return SUCCESS;
}

# 39 "WriteData.nc"
inline static result_t FlashInitM$WriteData$write(uint32_t offset, uint8_t *data, uint32_t numBytesWrite){
#line 39
  unsigned char __nesc_result;
#line 39

#line 39
  __nesc_result = ByteEEPROMC$WriteData$write(FlashInit$BYTE_EEPROM_ID, offset, data, numBytesWrite);
#line 39

#line 39
  return __nesc_result;
#line 39
}
#line 39
# 161 "ByteEEPROMC.nc"
static inline void ByteEEPROMC$successTask(void )
#line 161
{
  ByteEEPROMC$completeOp(SUCCESS);
}

#line 373
static inline result_t ByteEEPROMC$ReadData$default$readDone(uint8_t id, uint8_t *buffer, uint32_t numBytesRead, result_t success)
#line 373
{
  return SUCCESS;
}

# 42 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReadData.nc"
inline static result_t ByteEEPROMC$ReadData$readDone(uint8_t arg_0x101469020, uint8_t *buffer, uint32_t numBytesRead, result_t success){
#line 42
  unsigned char __nesc_result;
#line 42

#line 42
    __nesc_result = ByteEEPROMC$ReadData$default$readDone(arg_0x101469020, buffer, numBytesRead, success);
#line 42

#line 42
  return __nesc_result;
#line 42
}
#line 42
# 63 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_RED_LED_PIN()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 2);
}

# 50 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$redOn(void )
#line 50
{
  {
  }
#line 51
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 52
    {
      TOSH_CLR_RED_LED_PIN();
      LedsC$ledsOn |= LedsC$RED_BIT;
    }
#line 55
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 43 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t FlashInitM$Leds$redOn(void ){
#line 43
  unsigned char __nesc_result;
#line 43

#line 43
  __nesc_result = LedsC$Leds$redOn();
#line 43

#line 43
  return __nesc_result;
#line 43
}
#line 43
# 65 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_YELLOW_LED_PIN()
#line 65
{
#line 65
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 0);
}

# 108 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$yellowOn(void )
#line 108
{
  {
  }
#line 109
  ;
  /* atomic removed: atomic calls only */
#line 110
  {
    TOSH_CLR_YELLOW_LED_PIN();
    LedsC$ledsOn |= LedsC$YELLOW_BIT;
  }
  return SUCCESS;
}

static inline result_t LedsC$Leds$yellowOff(void )
#line 117
{
  {
  }
#line 118
  ;
  /* atomic removed: atomic calls only */
#line 119
  {
    TOSH_SET_YELLOW_LED_PIN();
    LedsC$ledsOn &= ~LedsC$YELLOW_BIT;
  }
  return SUCCESS;
}

static inline result_t LedsC$Leds$yellowToggle(void )
#line 126
{
  result_t rval;

#line 128
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 128
    {
      if (LedsC$ledsOn & LedsC$YELLOW_BIT) {
        rval = LedsC$Leds$yellowOff();
        }
      else {
#line 132
        rval = LedsC$Leds$yellowOn();
        }
    }
#line 134
    __nesc_atomic_end(__nesc_atomic); }
#line 134
  return rval;
}

# 110 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t FlashInitM$Leds$yellowToggle(void ){
#line 110
  unsigned char __nesc_result;
#line 110

#line 110
  __nesc_result = LedsC$Leds$yellowToggle();
#line 110

#line 110
  return __nesc_result;
#line 110
}
#line 110
# 72 "FlashInitM.nc"
static inline result_t FlashInitM$WriteData$writeDone(uint8_t *data, uint32_t numBytesWrite, result_t success)
{
  FlashInitM$index = FlashInitM$index + numBytesWrite;
  FlashInitM$Leds$yellowToggle();
  if (FlashInitM$index < (uint32_t )((uint32_t )FlashInitM$TOS_EEPROM_MAX_PAGES << (uint32_t )FlashInitM$TOS_EEPROM_PAGE_SIZE_LOG2)) 
    {
      TOS_post(FlashInitM$writeNew);
    }
  else 
    {
      FlashInitM$Leds$redOn();
    }
  return SUCCESS;
}

# 365 "ByteEEPROMC.nc"
static inline result_t ByteEEPROMC$WriteData$default$writeDone(uint8_t id, uint8_t *data, uint32_t numBytesWrite, result_t success)
#line 365
{
  return SUCCESS;
}

# 54 "WriteData.nc"
inline static result_t ByteEEPROMC$WriteData$writeDone(uint8_t arg_0x10146aad8, uint8_t *data, uint32_t numBytesWrite, result_t success){
#line 54
  unsigned char __nesc_result;
#line 54

#line 54
  switch (arg_0x10146aad8) {
#line 54
    case FlashInit$BYTE_EEPROM_ID:
#line 54
      __nesc_result = FlashInitM$WriteData$writeDone(data, numBytesWrite, success);
#line 54
      break;
#line 54
    default:
#line 54
      __nesc_result = ByteEEPROMC$WriteData$default$writeDone(arg_0x10146aad8, data, numBytesWrite, success);
#line 54
      break;
#line 54
    }
#line 54

#line 54
  return __nesc_result;
#line 54
}
#line 54
# 377 "ByteEEPROMC.nc"
static inline result_t ByteEEPROMC$LogData$default$appendDone(uint8_t id, uint8_t *data, uint32_t numBytesWrite, result_t success)
#line 377
{
  return SUCCESS;
}

# 61 "/Users/wbennett/opt/MoteWorks/tos/interfaces/LogData.nc"
inline static result_t ByteEEPROMC$LogData$appendDone(uint8_t arg_0x10146ea90, uint8_t *data, uint32_t numBytes, result_t success){
#line 61
  unsigned char __nesc_result;
#line 61

#line 61
    __nesc_result = ByteEEPROMC$LogData$default$appendDone(arg_0x10146ea90, data, numBytes, success);
#line 61

#line 61
  return __nesc_result;
#line 61
}
#line 61
# 385 "ByteEEPROMC.nc"
static inline result_t ByteEEPROMC$LogData$default$syncDone(uint8_t id, result_t success)
#line 385
{
  return SUCCESS;
}

# 83 "/Users/wbennett/opt/MoteWorks/tos/interfaces/LogData.nc"
inline static result_t ByteEEPROMC$LogData$syncDone(uint8_t arg_0x10146ea90, result_t success){
#line 83
  unsigned char __nesc_result;
#line 83

#line 83
    __nesc_result = ByteEEPROMC$LogData$default$syncDone(arg_0x10146ea90, success);
#line 83

#line 83
  return __nesc_result;
#line 83
}
#line 83
# 381 "ByteEEPROMC.nc"
static inline result_t ByteEEPROMC$LogData$default$eraseDone(uint8_t id, result_t success)
#line 381
{
  return SUCCESS;
}

# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/LogData.nc"
inline static result_t ByteEEPROMC$LogData$eraseDone(uint8_t arg_0x10146ea90, result_t success){
#line 45
  unsigned char __nesc_result;
#line 45

#line 45
    __nesc_result = ByteEEPROMC$LogData$default$eraseDone(arg_0x10146ea90, success);
#line 45

#line 45
  return __nesc_result;
#line 45
}
#line 45
# 67 "FlashInitM.nc"
static inline result_t FlashInitM$WriteData$flushDone(result_t success)
{
  return SUCCESS;
}

# 369 "ByteEEPROMC.nc"
static inline result_t ByteEEPROMC$WriteData$default$flushDone(uint8_t id, result_t succes)
#line 369
{
  return SUCCESS;
}

# 45 "WriteData.nc"
inline static result_t ByteEEPROMC$WriteData$flushDone(uint8_t arg_0x10146aad8, result_t success){
#line 45
  unsigned char __nesc_result;
#line 45

#line 45
  switch (arg_0x10146aad8) {
#line 45
    case FlashInit$BYTE_EEPROM_ID:
#line 45
      __nesc_result = FlashInitM$WriteData$flushDone(success);
#line 45
      break;
#line 45
    default:
#line 45
      __nesc_result = ByteEEPROMC$WriteData$default$flushDone(arg_0x10146aad8, success);
#line 45
      break;
#line 45
    }
#line 45

#line 45
  return __nesc_result;
#line 45
}
#line 45
# 511 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static inline result_t PageEEPROMM$PageEEPROM$read(eeprompage_t page, eeprompageoffset_t offset, 
void *reqdata, eeprompageoffset_t n)
#line 512
{
  return PageEEPROMM$newRequest(PageEEPROMM$R_READ, page, offset, reqdata, n);
}

# 28 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMShare$ActualEEPROM$read(eeprompage_t page, eeprompageoffset_t offset, void *data, eeprompageoffset_t n){
#line 28
  unsigned char __nesc_result;
#line 28

#line 28
  __nesc_result = PageEEPROMM$PageEEPROM$read(page, offset, data, n);
#line 28

#line 28
  return __nesc_result;
#line 28
}
#line 28
# 26 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static inline int PageEEPROMShare$setClient(uint8_t client)
#line 26
{
  if (PageEEPROMShare$NCLIENTS != 1) 
    {
      if (PageEEPROMShare$lastClient) {
        return FALSE;
        }
#line 31
      PageEEPROMShare$lastClient = client + 1;
    }
  return TRUE;
}

#line 111
static __inline result_t PageEEPROMShare$PageEEPROM$read(uint8_t client, eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n)
#line 112
{
  if (!PageEEPROMShare$setClient(client)) {
    return FAIL;
    }
#line 115
  return PageEEPROMShare$check(PageEEPROMShare$ActualEEPROM$read(page, offset, data, n));
}

# 28 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t ByteEEPROMC$PageEEPROM$read(eeprompage_t page, eeprompageoffset_t offset, void *data, eeprompageoffset_t n){
#line 28
  unsigned char __nesc_result;
#line 28

#line 28
  __nesc_result = PageEEPROMShare$PageEEPROM$read(0U, page, offset, data, n);
#line 28

#line 28
  return __nesc_result;
#line 28
}
#line 28
# 36 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static inline uint8_t PageEEPROMShare$getClient(void )
#line 36
{
  uint8_t id = 0;

  if (PageEEPROMShare$NCLIENTS != 1) 
    {
      id = PageEEPROMShare$lastClient - 1;
      PageEEPROMShare$lastClient = 0;
    }

  return id;
}

# 216 "ByteEEPROMC.nc"
static inline result_t ByteEEPROMC$PageEEPROM$readDone(result_t success)
#line 216
{
  if (success == FAIL) {
    ByteEEPROMC$completeOp(FAIL);
    }
  else {
#line 220
    ByteEEPROMC$continueOp();
    }
#line 221
  return SUCCESS;
}

# 149 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static inline result_t PageEEPROMShare$PageEEPROM$default$readDone(uint8_t client, result_t result)
#line 149
{
  return FAIL;
}

# 30 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMShare$PageEEPROM$readDone(uint8_t arg_0x1013ea020, result_t result){
#line 30
  unsigned char __nesc_result;
#line 30

#line 30
  switch (arg_0x1013ea020) {
#line 30
    case 0U:
#line 30
      __nesc_result = ByteEEPROMC$PageEEPROM$readDone(result);
#line 30
      break;
#line 30
    default:
#line 30
      __nesc_result = PageEEPROMShare$PageEEPROM$default$readDone(arg_0x1013ea020, result);
#line 30
      break;
#line 30
    }
#line 30

#line 30
  return __nesc_result;
#line 30
}
#line 30
# 118 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static __inline result_t PageEEPROMShare$ActualEEPROM$readDone(result_t result)
#line 118
{
  return PageEEPROMShare$PageEEPROM$readDone(PageEEPROMShare$getClient(), result);
}

# 30 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMM$PageEEPROM$readDone(result_t result){
#line 30
  unsigned char __nesc_result;
#line 30

#line 30
  __nesc_result = PageEEPROMShare$ActualEEPROM$readDone(result);
#line 30

#line 30
  return __nesc_result;
#line 30
}
#line 30
# 389 "ByteEEPROMC.nc"
static inline result_t ByteEEPROMC$PageEEPROM$computeCrcDone(result_t result, uint16_t crc)
#line 389
{
  return SUCCESS;
}

# 153 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static inline result_t PageEEPROMShare$PageEEPROM$default$computeCrcDone(uint8_t client, result_t result, uint16_t crc)
#line 153
{
  return FAIL;
}

# 34 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMShare$PageEEPROM$computeCrcDone(uint8_t arg_0x1013ea020, result_t result, uint16_t crc){
#line 34
  unsigned char __nesc_result;
#line 34

#line 34
  switch (arg_0x1013ea020) {
#line 34
    case 0U:
#line 34
      __nesc_result = ByteEEPROMC$PageEEPROM$computeCrcDone(result, crc);
#line 34
      break;
#line 34
    default:
#line 34
      __nesc_result = PageEEPROMShare$PageEEPROM$default$computeCrcDone(arg_0x1013ea020, result, crc);
#line 34
      break;
#line 34
    }
#line 34

#line 34
  return __nesc_result;
#line 34
}
#line 34
# 129 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static __inline result_t PageEEPROMShare$ActualEEPROM$computeCrcDone(result_t result, uint16_t crc)
#line 129
{
  return PageEEPROMShare$PageEEPROM$computeCrcDone(PageEEPROMShare$getClient(), result, crc);
}

# 34 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMM$PageEEPROM$computeCrcDone(result_t result, uint16_t crc){
#line 34
  unsigned char __nesc_result;
#line 34

#line 34
  __nesc_result = PageEEPROMShare$ActualEEPROM$computeCrcDone(result, crc);
#line 34

#line 34
  return __nesc_result;
#line 34
}
#line 34
# 231 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static inline void PageEEPROMM$taskSuccess(void )
#line 231
{
  PageEEPROMM$requestDone(SUCCESS);
}

static inline void PageEEPROMM$taskFail(void )
#line 235
{
  PageEEPROMM$requestDone(FAIL);
}

#line 570
static inline result_t PageEEPROMM$syncOrFlush(eeprompage_t page, uint8_t newReq)
#line 570
{
  if (PageEEPROMM$request != PageEEPROMM$IDLE) {
    return FAIL;
    }
#line 573
  PageEEPROMM$request = newReq;

  if (PageEEPROMM$broken) 
    {
      TOS_post(PageEEPROMM$taskFail);
      return SUCCESS;
    }
  else {
#line 580
    if (PageEEPROMM$buffer[0].page == page) {
      PageEEPROMM$selected = 0;
      }
    else {
#line 582
      if (PageEEPROMM$buffer[1].page == page) {
        PageEEPROMM$selected = 1;
        }
      else {
          TOS_post(PageEEPROMM$taskSuccess);
          return SUCCESS;
        }
      }
    }
#line 590
  PageEEPROMM$buffer[PageEEPROMM$selected].unchecked = 0;
  PageEEPROMM$handleRWRequest();

  return SUCCESS;
}





static inline result_t PageEEPROMM$PageEEPROM$flush(eeprompage_t page)
#line 600
{
  return PageEEPROMM$syncOrFlush(page, PageEEPROMM$R_FLUSH);
}

# 24 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMShare$ActualEEPROM$flush(eeprompage_t page){
#line 24
  unsigned char __nesc_result;
#line 24

#line 24
  __nesc_result = PageEEPROMM$PageEEPROM$flush(page);
#line 24

#line 24
  return __nesc_result;
#line 24
}
#line 24
# 95 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static __inline result_t PageEEPROMShare$PageEEPROM$flush(uint8_t client, eeprompage_t page)
#line 95
{
  if (!PageEEPROMShare$setClient(client)) {
    return FAIL;
    }
#line 98
  return PageEEPROMShare$check(PageEEPROMShare$ActualEEPROM$flush(page));
}

# 24 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t ByteEEPROMC$PageEEPROM$flush(eeprompage_t page){
#line 24
  unsigned char __nesc_result;
#line 24

#line 24
  __nesc_result = PageEEPROMShare$PageEEPROM$flush(0U, page);
#line 24

#line 24
  return __nesc_result;
#line 24
}
#line 24
# 165 "ByteEEPROMC.nc"
static inline void ByteEEPROMC$failTask(void )
#line 165
{
  ByteEEPROMC$completeOp(FAIL);
}

static inline void ByteEEPROMC$check(result_t success)
#line 169
{
  if (!success) {
    TOS_post(ByteEEPROMC$failTask);
    }
}

#line 224
static inline result_t ByteEEPROMC$PageEEPROM$writeDone(result_t success)
#line 224
{
  if (success == FAIL) {
    ByteEEPROMC$completeOp(FAIL);
    }
  else {
      if (ByteEEPROMC$state == ByteEEPROMC$S_APPEND && (ByteEEPROMC$startAddr & ByteEEPROMC$PAGE_SIZE_MASK) == 0 && 
      !(ByteEEPROMC$writesLastByte && ByteEEPROMC$startAddr == ByteEEPROMC$stopAddr)) {






        ByteEEPROMC$check(ByteEEPROMC$PageEEPROM$flush((ByteEEPROMC$startAddr >> TOS_EEPROM_PAGE_SIZE_LOG2) - 1));
        }
      else {
#line 239
        ByteEEPROMC$continueOp();
        }
    }
#line 241
  return SUCCESS;
}

# 133 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static inline result_t PageEEPROMShare$PageEEPROM$default$writeDone(uint8_t client, result_t result)
#line 133
{
  return FAIL;
}

# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMShare$PageEEPROM$writeDone(uint8_t arg_0x1013ea020, result_t result){
#line 15
  unsigned char __nesc_result;
#line 15

#line 15
  switch (arg_0x1013ea020) {
#line 15
    case 0U:
#line 15
      __nesc_result = ByteEEPROMC$PageEEPROM$writeDone(result);
#line 15
      break;
#line 15
    default:
#line 15
      __nesc_result = PageEEPROMShare$PageEEPROM$default$writeDone(arg_0x1013ea020, result);
#line 15
      break;
#line 15
    }
#line 15

#line 15
  return __nesc_result;
#line 15
}
#line 15
# 65 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static __inline result_t PageEEPROMShare$ActualEEPROM$writeDone(result_t result)
#line 65
{
  return PageEEPROMShare$PageEEPROM$writeDone(PageEEPROMShare$getClient(), result);
}

# 15 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMM$PageEEPROM$writeDone(result_t result){
#line 15
  unsigned char __nesc_result;
#line 15

#line 15
  __nesc_result = PageEEPROMShare$ActualEEPROM$writeDone(result);
#line 15

#line 15
  return __nesc_result;
#line 15
}
#line 15
# 79 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_FLASH_SELECT_PIN()
#line 79
{
#line 79
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 3);
}

# 60 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLFlash.nc"
static inline result_t HPLFlash$FlashSelect$low(void )
#line 60
{
  TOSH_CLR_FLASH_CLK_PIN();
  TOSH_CLR_FLASH_SELECT_PIN();
  return SUCCESS;
}

# 30 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/SlavePin.nc"
inline static result_t PageEEPROMM$FlashSelect$low(void ){
#line 30
  unsigned char __nesc_result;
#line 30

#line 30
  __nesc_result = HPLFlash$FlashSelect$low();
#line 30

#line 30
  return __nesc_result;
#line 30
}
#line 30
# 137 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static inline void PageEEPROMM$selectFlash(void )
#line 137
{
  PageEEPROMM$FlashSelect$low();
}

# 90 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLFlash.nc"
static __inline uint8_t HPLFlash$FlashSPI$txByte(uint8_t spiOut)
#line 90
{
  uint8_t spiIn = 0;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      uint8_t clrClkAndData = * (volatile uint8_t *)(0x0B + 0x20) & ~0x28;

      * (volatile uint8_t *)(0x0B + 0x20) = clrClkAndData;
#line 98
       __asm volatile ("sbrc %2,""7""\n""\tsbi 11,3\n""\tsbi 11,5\n""\tsbic 9,2\n""\tori %0,1<<""7""\n" : "=d"(spiIn) : "0"(spiIn), "r"(spiOut));
      * (volatile uint8_t *)(0x0B + 0x20) = clrClkAndData;
#line 99
       __asm volatile ("sbrc %2,""6""\n""\tsbi 11,3\n""\tsbi 11,5\n""\tsbic 9,2\n""\tori %0,1<<""6""\n" : "=d"(spiIn) : "0"(spiIn), "r"(spiOut));
      * (volatile uint8_t *)(0x0B + 0x20) = clrClkAndData;
#line 100
       __asm volatile ("sbrc %2,""5""\n""\tsbi 11,3\n""\tsbi 11,5\n""\tsbic 9,2\n""\tori %0,1<<""5""\n" : "=d"(spiIn) : "0"(spiIn), "r"(spiOut));
      * (volatile uint8_t *)(0x0B + 0x20) = clrClkAndData;
#line 101
       __asm volatile ("sbrc %2,""4""\n""\tsbi 11,3\n""\tsbi 11,5\n""\tsbic 9,2\n""\tori %0,1<<""4""\n" : "=d"(spiIn) : "0"(spiIn), "r"(spiOut));
      * (volatile uint8_t *)(0x0B + 0x20) = clrClkAndData;
#line 102
       __asm volatile ("sbrc %2,""3""\n""\tsbi 11,3\n""\tsbi 11,5\n""\tsbic 9,2\n""\tori %0,1<<""3""\n" : "=d"(spiIn) : "0"(spiIn), "r"(spiOut));
      * (volatile uint8_t *)(0x0B + 0x20) = clrClkAndData;
#line 103
       __asm volatile ("sbrc %2,""2""\n""\tsbi 11,3\n""\tsbi 11,5\n""\tsbic 9,2\n""\tori %0,1<<""2""\n" : "=d"(spiIn) : "0"(spiIn), "r"(spiOut));
      * (volatile uint8_t *)(0x0B + 0x20) = clrClkAndData;
#line 104
       __asm volatile ("sbrc %2,""1""\n""\tsbi 11,3\n""\tsbi 11,5\n""\tsbic 9,2\n""\tori %0,1<<""1""\n" : "=d"(spiIn) : "0"(spiIn), "r"(spiOut));
      * (volatile uint8_t *)(0x0B + 0x20) = clrClkAndData;
#line 105
       __asm volatile ("sbrc %2,""0""\n""\tsbi 11,3\n""\tsbi 11,5\n""\tsbic 9,2\n""\tori %0,1<<""0""\n" : "=d"(spiIn) : "0"(spiIn), "r"(spiOut));}
#line 105
    __nesc_atomic_end(__nesc_atomic); }


  return spiIn;
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/interfaces/FastSPI.nc"
inline static uint8_t PageEEPROMM$FlashSPI$txByte(uint8_t data){
#line 38
  unsigned char __nesc_result;
#line 38

#line 38
  __nesc_result = HPLFlash$FlashSPI$txByte(data);
#line 38

#line 38
  return __nesc_result;
#line 38
}
#line 38
# 82 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline int TOSH_READ_FLASH_IN_PIN()
#line 82
{
#line 82
  return (* (volatile uint8_t *)(0x09 + 0x20) & (1 << 2)) != 0;
}

#line 39
static __inline void TOSH_uwait(int u_sec)
#line 39
{
  while (u_sec > 0) {
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
      u_sec--;
    }
}

# 118 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLFlash.nc"
static inline result_t HPLFlash$FlashIdle$wait(void )
#line 118
{
  TOSH_CLR_FLASH_CLK_PIN();


  TOSH_uwait(1);
  if (TOSH_READ_FLASH_IN_PIN()) {
    return FAIL;
    }
  TOS_post(HPLFlash$idleWait);
  return SUCCESS;
}

# 11 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Resource.nc"
inline static result_t PageEEPROMM$FlashIdle$wait(void ){
#line 11
  unsigned char __nesc_result;
#line 11

#line 11
  __nesc_result = HPLFlash$FlashIdle$wait();
#line 11

#line 11
  return __nesc_result;
#line 11
}
#line 11

inline static result_t HPLFlash$FlashIdle$available(void ){
#line 12
  unsigned char __nesc_result;
#line 12

#line 12
  __nesc_result = PageEEPROMM$FlashIdle$available();
#line 12

#line 12
  return __nesc_result;
#line 12
}
#line 12
# 80 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_SET_FLASH_CLK_PIN()
#line 80
{
#line 80
  * (volatile uint8_t *)(0x0B + 0x20) |= 1 << 5;
}

# 130 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLFlash.nc"
static inline bool HPLFlash$getCompareStatus(void )
#line 130
{
  TOSH_SET_FLASH_CLK_PIN();
  TOSH_CLR_FLASH_CLK_PIN();

   __asm volatile ("nop");
   __asm volatile ("nop");
  return !TOSH_READ_FLASH_IN_PIN();
}

# 23 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
inline static bool PageEEPROMM$getCompareStatus(void ){
#line 23
  unsigned char __nesc_result;
#line 23

#line 23
  __nesc_result = HPLFlash$getCompareStatus();
#line 23

#line 23
  return __nesc_result;
#line 23
}
#line 23
# 45 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/SlavePin.nc"
inline static result_t HPLFlash$FlashSelect$notifyHigh(void ){
#line 45
  unsigned char __nesc_result;
#line 45

#line 45
  __nesc_result = PageEEPROMM$FlashSelect$notifyHigh();
#line 45

#line 45
  return __nesc_result;
#line 45
}
#line 45
# 66 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLFlash.nc"
static inline void HPLFlash$sigHigh(void )
#line 66
{
  HPLFlash$FlashSelect$notifyHigh();
}

static inline result_t HPLFlash$FlashSelect$high(bool needEvent)
#line 70
{
  TOSH_SET_FLASH_SELECT_PIN();
  if (needEvent) {
    TOS_post(HPLFlash$sigHigh);
    }
#line 74
  return SUCCESS;
}

# 40 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/SlavePin.nc"
inline static result_t PageEEPROMM$FlashSelect$high(bool needEvent){
#line 40
  unsigned char __nesc_result;
#line 40

#line 40
  __nesc_result = HPLFlash$FlashSelect$high(needEvent);
#line 40

#line 40
  return __nesc_result;
#line 40
}
#line 40
# 142 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static inline void PageEEPROMM$requestDeselect(void )
#line 142
{
  PageEEPROMM$deselectRequested = TRUE;
  PageEEPROMM$FlashSelect$high(TRUE);
}

#line 258
static inline void PageEEPROMM$flashCommandComplete(void )
#line 258
{
  if (PageEEPROMM$waiting) 
    {
      PageEEPROMM$waiting = PageEEPROMM$flashBusy = PageEEPROMM$buffer[0].busy = PageEEPROMM$buffer[1].busy = FALSE;

      if (PageEEPROMM$cmdPhase == PageEEPROMM$P_COMPARE_CHECK) 
        {
          if (PageEEPROMM$compareOk) {
            PageEEPROMM$buffer[PageEEPROMM$checking].unchecked = 0;
            }
          else {
#line 267
            if (PageEEPROMM$buffer[PageEEPROMM$checking].unchecked < 2) {
              PageEEPROMM$buffer[PageEEPROMM$checking].clean = FALSE;
              }
            else {
                PageEEPROMM$broken = TRUE;
                PageEEPROMM$requestDone(FAIL);
                return;
              }
            }
#line 275
          PageEEPROMM$handleRWRequest();
        }
      else 
        {


          if ((PageEEPROMM$buffer[0].unchecked || PageEEPROMM$buffer[1].unchecked) && 
          !(PageEEPROMM$cmdPhase == PageEEPROMM$P_COMPARE || PageEEPROMM$cmdPhase == PageEEPROMM$P_COMPARE_CHECK)) {
            PageEEPROMM$checkBuffer(PageEEPROMM$buffer[0].unchecked ? 0 : 1);
            }
          else {
            PageEEPROMM$sendFlashCommand();
            }
        }
#line 288
      return;
    }
  switch (PageEEPROMM$cmdPhase) 
    {
      default: 
        PageEEPROMM$requestDone(FAIL);
      break;

      case PageEEPROMM$P_READ: case PageEEPROMM$P_READCRC: case PageEEPROMM$P_WRITE: 
            PageEEPROMM$requestDone(SUCCESS);
      break;

      case PageEEPROMM$P_FLUSH: 
        PageEEPROMM$flashBusy = TRUE;
      PageEEPROMM$buffer[PageEEPROMM$selected].clean = PageEEPROMM$buffer[PageEEPROMM$selected].busy = TRUE;
      PageEEPROMM$buffer[PageEEPROMM$selected].unchecked++;
      PageEEPROMM$buffer[PageEEPROMM$selected].erased = FALSE;
      PageEEPROMM$handleRWRequest();
      break;

      case PageEEPROMM$P_COMPARE: 
        PageEEPROMM$cmdPhase = PageEEPROMM$P_COMPARE_CHECK;
      PageEEPROMM$flashBusy = TRUE;
      PageEEPROMM$buffer[PageEEPROMM$checking].busy = TRUE;




      TOSH_uwait(10);
      PageEEPROMM$requestFlashStatus();
      break;

      case PageEEPROMM$P_FILL: 
        PageEEPROMM$flashBusy = TRUE;
      PageEEPROMM$buffer[PageEEPROMM$selected].page = PageEEPROMM$reqPage;
      PageEEPROMM$buffer[PageEEPROMM$selected].clean = PageEEPROMM$buffer[PageEEPROMM$selected].busy = TRUE;
      PageEEPROMM$buffer[PageEEPROMM$selected].erased = FALSE;
      PageEEPROMM$handleRWRequest();
      break;

      case PageEEPROMM$P_ERASE: 
        PageEEPROMM$flashBusy = TRUE;



      PageEEPROMM$buffer[PageEEPROMM$selected].page = PageEEPROMM$reqPage;
      PageEEPROMM$buffer[PageEEPROMM$selected].clean = TRUE;
      PageEEPROMM$buffer[PageEEPROMM$selected].erased = TRUE;
      PageEEPROMM$requestDone(SUCCESS);
      break;
    }
}

# 50 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/crc.h"
static inline uint16_t crcByte(uint16_t oldCrc, uint8_t byte)
{

  uint16_t *table = crcTable;
  uint16_t newCrc;

   __asm ("eor %1,%B3\n"
  "\tlsl %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tadd %A2, %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tlpm\n"
  "\tmov %B0, %A3\n"
  "\tmov %A0, r0\n"
  "\tadiw r30,1\n"
  "\tlpm\n"
  "\teor %B0, r0" : 
  "=r"(newCrc), "+r"(byte), "+z"(table) : "r"(oldCrc));
  return newCrc;
}

# 365 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static inline void PageEEPROMM$execRWBuffer(uint8_t reqCmd, uint8_t dontCare, eeprompageoffset_t offset)
#line 365
{
  PageEEPROMM$execCommand(PageEEPROMM$buffer[PageEEPROMM$selected].busy, reqCmd, dontCare, 0, offset);
}

# 324 "ByteEEPROMC.nc"
static inline result_t ByteEEPROMC$PageEEPROM$syncDone(result_t result)
#line 324
{
  ByteEEPROMC$completeOp(result);
  return SUCCESS;
}

# 141 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static inline result_t PageEEPROMShare$PageEEPROM$default$syncDone(uint8_t client, result_t result)
#line 141
{
  return FAIL;
}

# 22 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMShare$PageEEPROM$syncDone(uint8_t arg_0x1013ea020, result_t result){
#line 22
  unsigned char __nesc_result;
#line 22

#line 22
  switch (arg_0x1013ea020) {
#line 22
    case 0U:
#line 22
      __nesc_result = ByteEEPROMC$PageEEPROM$syncDone(result);
#line 22
      break;
#line 22
    default:
#line 22
      __nesc_result = PageEEPROMShare$PageEEPROM$default$syncDone(arg_0x1013ea020, result);
#line 22
      break;
#line 22
    }
#line 22

#line 22
  return __nesc_result;
#line 22
}
#line 22
# 91 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static __inline result_t PageEEPROMShare$ActualEEPROM$syncDone(result_t result)
#line 91
{
  return PageEEPROMShare$PageEEPROM$syncDone(PageEEPROMShare$getClient(), result);
}

# 22 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMM$PageEEPROM$syncDone(result_t result){
#line 22
  unsigned char __nesc_result;
#line 22

#line 22
  __nesc_result = PageEEPROMShare$ActualEEPROM$syncDone(result);
#line 22

#line 22
  return __nesc_result;
#line 22
}
#line 22
# 566 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static inline result_t PageEEPROMM$PageEEPROM$erase(eeprompage_t page, uint8_t eraseKind)
#line 566
{
  return PageEEPROMM$newRequest(PageEEPROMM$R_ERASE, page, eraseKind, (void *)0, 0);
}

# 17 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMShare$ActualEEPROM$erase(eeprompage_t page, uint8_t eraseKind){
#line 17
  unsigned char __nesc_result;
#line 17

#line 17
  __nesc_result = PageEEPROMM$PageEEPROM$erase(page, eraseKind);
#line 17

#line 17
  return __nesc_result;
#line 17
}
#line 17
# 69 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static __inline result_t PageEEPROMShare$PageEEPROM$erase(uint8_t client, eeprompage_t page, uint8_t eraseKind)
#line 69
{
  if (!PageEEPROMShare$setClient(client)) {
    return FAIL;
    }
#line 72
  return PageEEPROMShare$check(PageEEPROMShare$ActualEEPROM$erase(page, eraseKind));
}

# 17 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t ByteEEPROMC$PageEEPROM$erase(eeprompage_t page, uint8_t eraseKind){
#line 17
  unsigned char __nesc_result;
#line 17

#line 17
  __nesc_result = PageEEPROMShare$PageEEPROM$erase(0U, page, eraseKind);
#line 17

#line 17
  return __nesc_result;
#line 17
}
#line 17
# 244 "ByteEEPROMC.nc"
static inline result_t ByteEEPROMC$PageEEPROM$flushDone(result_t result)
#line 244
{




  if (ByteEEPROMC$state == ByteEEPROMC$S_FLUSH) 
    {
      ByteEEPROMC$completeOp(result);
    }
  else 
    {
      ByteEEPROMC$check(ByteEEPROMC$PageEEPROM$erase(ByteEEPROMC$startAddr >> TOS_EEPROM_PAGE_SIZE_LOG2, 
      TOS_EEPROM_PREVIOUSLY_ERASED));
    }
  return SUCCESS;
}

# 145 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static inline result_t PageEEPROMShare$PageEEPROM$default$flushDone(uint8_t client, result_t result)
#line 145
{
  return FAIL;
}

# 26 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMShare$PageEEPROM$flushDone(uint8_t arg_0x1013ea020, result_t result){
#line 26
  unsigned char __nesc_result;
#line 26

#line 26
  switch (arg_0x1013ea020) {
#line 26
    case 0U:
#line 26
      __nesc_result = ByteEEPROMC$PageEEPROM$flushDone(result);
#line 26
      break;
#line 26
    default:
#line 26
      __nesc_result = PageEEPROMShare$PageEEPROM$default$flushDone(arg_0x1013ea020, result);
#line 26
      break;
#line 26
    }
#line 26

#line 26
  return __nesc_result;
#line 26
}
#line 26
# 107 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static __inline result_t PageEEPROMShare$ActualEEPROM$flushDone(result_t result)
#line 107
{
  return PageEEPROMShare$PageEEPROM$flushDone(PageEEPROMShare$getClient(), result);
}

# 26 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMM$PageEEPROM$flushDone(result_t result){
#line 26
  unsigned char __nesc_result;
#line 26

#line 26
  __nesc_result = PageEEPROMShare$ActualEEPROM$flushDone(result);
#line 26

#line 26
  return __nesc_result;
#line 26
}
#line 26
# 347 "ByteEEPROMC.nc"
static inline result_t ByteEEPROMC$PageEEPROM$eraseDone(result_t success)
#line 347
{
  if (!success) {
    ByteEEPROMC$completeOp(success);
    }
  else {
#line 350
    if (ByteEEPROMC$state == ByteEEPROMC$S_APPEND) {
      ByteEEPROMC$continueOp();
      }
    else {
        if (ByteEEPROMC$startAddr == ByteEEPROMC$stopAddr) {
          ByteEEPROMC$completeOp(SUCCESS);
          }
        else {
            ByteEEPROMC$stopAddr--;
            ByteEEPROMC$check(ByteEEPROMC$PageEEPROM$erase(ByteEEPROMC$stopAddr, TOS_EEPROM_ERASE));
          }
      }
    }
#line 362
  return SUCCESS;
}

# 137 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static inline result_t PageEEPROMShare$PageEEPROM$default$eraseDone(uint8_t client, result_t result)
#line 137
{
  return FAIL;
}

# 18 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMShare$PageEEPROM$eraseDone(uint8_t arg_0x1013ea020, result_t result){
#line 18
  unsigned char __nesc_result;
#line 18

#line 18
  switch (arg_0x1013ea020) {
#line 18
    case 0U:
#line 18
      __nesc_result = ByteEEPROMC$PageEEPROM$eraseDone(result);
#line 18
      break;
#line 18
    default:
#line 18
      __nesc_result = PageEEPROMShare$PageEEPROM$default$eraseDone(arg_0x1013ea020, result);
#line 18
      break;
#line 18
    }
#line 18

#line 18
  return __nesc_result;
#line 18
}
#line 18
# 75 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static __inline result_t PageEEPROMShare$ActualEEPROM$eraseDone(result_t result)
#line 75
{
  return PageEEPROMShare$PageEEPROM$eraseDone(PageEEPROMShare$getClient(), result);
}

# 18 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMM$PageEEPROM$eraseDone(result_t result){
#line 18
  unsigned char __nesc_result;
#line 18

#line 18
  __nesc_result = PageEEPROMShare$ActualEEPROM$eraseDone(result);
#line 18

#line 18
  return __nesc_result;
#line 18
}
#line 18
# 530 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static inline result_t PageEEPROMM$PageEEPROM$write(eeprompage_t page, eeprompageoffset_t offset, 
void *reqdata, eeprompageoffset_t n)
#line 531
{
#line 562
  return PageEEPROMM$newRequest(PageEEPROMM$R_WRITE, page, offset, reqdata, n);
}

# 13 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMShare$ActualEEPROM$write(eeprompage_t page, eeprompageoffset_t offset, void *data, eeprompageoffset_t n){
#line 13
  unsigned char __nesc_result;
#line 13

#line 13
  __nesc_result = PageEEPROMM$PageEEPROM$write(page, offset, data, n);
#line 13

#line 13
  return __nesc_result;
#line 13
}
#line 13
# 58 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static __inline result_t PageEEPROMShare$PageEEPROM$write(uint8_t client, eeprompage_t page, eeprompageoffset_t offset, 
void *data, eeprompageoffset_t n)
#line 59
{
  if (!PageEEPROMShare$setClient(client)) {
    return FAIL;
    }
#line 62
  return PageEEPROMShare$check(PageEEPROMShare$ActualEEPROM$write(page, offset, data, n));
}

# 13 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t ByteEEPROMC$PageEEPROM$write(eeprompage_t page, eeprompageoffset_t offset, void *data, eeprompageoffset_t n){
#line 13
  unsigned char __nesc_result;
#line 13

#line 13
  __nesc_result = PageEEPROMShare$PageEEPROM$write(0U, page, offset, data, n);
#line 13

#line 13
  return __nesc_result;
#line 13
}
#line 13
# 634 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static inline result_t PageEEPROMM$PageEEPROM$flushAll(void )
#line 634
{
  return PageEEPROMM$syncOrFlushAll(PageEEPROMM$R_FLUSHALL);
}

# 25 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t PageEEPROMShare$ActualEEPROM$flushAll(void ){
#line 25
  unsigned char __nesc_result;
#line 25

#line 25
  __nesc_result = PageEEPROMM$PageEEPROM$flushAll();
#line 25

#line 25
  return __nesc_result;
#line 25
}
#line 25
# 101 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static __inline result_t PageEEPROMShare$PageEEPROM$flushAll(uint8_t client)
#line 101
{
  if (!PageEEPROMShare$setClient(client)) {
    return FAIL;
    }
#line 104
  return PageEEPROMShare$check(PageEEPROMShare$ActualEEPROM$flushAll());
}

# 25 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROM.nc"
inline static result_t ByteEEPROMC$PageEEPROM$flushAll(void ){
#line 25
  unsigned char __nesc_result;
#line 25

#line 25
  __nesc_result = PageEEPROMShare$PageEEPROM$flushAll(0U);
#line 25

#line 25
  return __nesc_result;
#line 25
}
#line 25
# 166 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
static __inline void __nesc_enable_interrupt()
#line 166
{
   __asm volatile ("sei");}

#line 151
__inline  void __nesc_atomic_end(__nesc_atomic_t oldSreg)
{
  * (volatile uint8_t *)(0x3F + 0x20) = oldSreg;
}

#line 129
static inline void TOSH_wait()
{
   __asm volatile ("nop");
   __asm volatile ("nop");}

#line 158
static __inline void __nesc_atomic_sleep()
{

   __asm volatile ("sei");
   __asm volatile ("sleep");
  TOSH_wait();
}

#line 144
__inline  __nesc_atomic_t __nesc_atomic_start(void )
{
  __nesc_atomic_t result = * (volatile uint8_t *)(0x3F + 0x20);

#line 147
   __asm volatile ("cli");
  return result;
}

# 116 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
static inline bool TOSH_run_next_task()
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t old_full;
  void (*func)(void );

  fInterruptFlags = __nesc_atomic_start();
  old_full = TOSH_sched_full;
  func = TOSH_queue[old_full].tp;
  if (func == (void *)0) 
    {
      __nesc_atomic_sleep();
      return 0;
    }

  TOSH_queue[old_full].tp = (void *)0;
  TOSH_sched_full = (old_full + 1) & TOSH_TASK_BITMASK;
  __nesc_atomic_end(fInterruptFlags);
  func();

  return 1;
}

static inline void TOSH_run_task()
#line 139
{
  for (; ; ) 
    TOSH_run_next_task();
}

# 170 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
static __inline void __nesc_disable_interrupt()
#line 170
{
   __asm volatile ("cli");}

# 82 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
 bool TOS_post(void (*tp)())
#line 82
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t tmp;



  fInterruptFlags = __nesc_atomic_start();

  tmp = TOSH_sched_free;

  if (TOSH_queue[tmp].tp == (void *)0) {
      TOSH_sched_free = (tmp + 1) & TOSH_TASK_BITMASK;
      TOSH_queue[tmp].tp = tp;
      __nesc_atomic_end(fInterruptFlags);

      return TRUE;
    }
  else {
      __nesc_atomic_end(fInterruptFlags);

      return FALSE;
    }
}

# 34 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
  int main(void )
#line 34
{


  uint8_t local_symbol_ref;

  local_symbol_ref = TOS_PLATFORM;
  local_symbol_ref = TOS_BASE_STATION;
  local_symbol_ref = TOS_DATA_LENGTH;

  local_symbol_ref = TOS_ROUTE_PROTOCOL;








  RealMain$hardwareInit();
  RealMain$Pot$init(10);
  TOSH_sched_init();

  RealMain$StdControl$init();
  RealMain$StdControl$start();
  __nesc_enable_interrupt();

  while (1) {
      TOSH_run_task();
    }
}

# 55 "FlashInitM.nc"
static result_t FlashInitM$AllocationReq$requestProcessed(result_t success)
{

  if (success == SUCCESS) 
    {
      FlashInitM$Leds$greenOn();
      TOS_post(FlashInitM$writeNew);
    }
  return SUCCESS;
}

#line 29
static void FlashInitM$writeNew(void )
{
  {
#line 31
    if (1 != 0) {
#line 31
        FlashInitM$so_printf((uint8_t *)"Writing to: %lu\n", FlashInitM$index);
      }
#line 31
    ;
  }
#line 31
  ;
  FlashInitM$WriteData$write(FlashInitM$index, FlashInitM$buf, (uint32_t )256);
}

# 82 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/SOdebug.h"
static void FlashInitM$UARTPutChar(char c)
#line 82
{
  if (c == '\n') {
      do {
        }
      while (
#line 84
      !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0xC0 & (1 << 5)));

      * (volatile uint8_t *)0XC6 = 0xd;
      do {
        }
      while (
#line 87
      !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0xC0 & (1 << 5)));
      * (volatile uint8_t *)0XC6 = 0xa;

      do {
        }
      while (
#line 90
      !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0xC0 & (1 << 6)));

      TOSH_uwait(100);
      return;
    }
#line 94
  ;
  do {
    }
  while (
#line 95
  !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0xC0 & (1 << 5)));
  * (volatile uint8_t *)0XC6 = c;
  do {
    }
  while (
#line 97
  !(* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0xC0 & (1 << 6)));
  return;
}

# 174 "ByteEEPROMC.nc"
static void ByteEEPROMC$continueOp(void )
#line 174
{
  eeprompage_t sPage = ByteEEPROMC$startAddr >> TOS_EEPROM_PAGE_SIZE_LOG2;
  eeprompage_t ePage = ByteEEPROMC$stopAddr >> TOS_EEPROM_PAGE_SIZE_LOG2;
  eeprompageoffset_t offset;
#line 177
  eeprompageoffset_t count;

  if (ByteEEPROMC$startAddr == ByteEEPROMC$stopAddr) 
    {
      TOS_post(ByteEEPROMC$successTask);
      return;
    }

  offset = ByteEEPROMC$startAddr & ByteEEPROMC$PAGE_SIZE_MASK;
  if (sPage == ePage) 
    {
      count = ByteEEPROMC$stopAddr - ByteEEPROMC$startAddr;
    }
  else 
    {
      count = ByteEEPROMC$PAGE_SIZE - offset;
    }

  switch (ByteEEPROMC$state) 
    {
      case ByteEEPROMC$S_READ: 
        ByteEEPROMC$check(ByteEEPROMC$PageEEPROM$read(sPage, offset, ByteEEPROMC$dataBuffer + ByteEEPROMC$dataBufferOffset, count));
      break;

      case ByteEEPROMC$S_WRITE: 

        case ByteEEPROMC$S_APPEND: 
          ByteEEPROMC$check(ByteEEPROMC$PageEEPROM$write(sPage, offset, ByteEEPROMC$dataBuffer + ByteEEPROMC$dataBufferOffset, count));
      break;

      case ByteEEPROMC$S_FLUSH: 
        ByteEEPROMC$check(ByteEEPROMC$PageEEPROM$flushAll());
      break;
    }

  ByteEEPROMC$dataBufferOffset += count;
  ByteEEPROMC$startAddr += count;
}

#line 127
static void ByteEEPROMC$completeOp(result_t success)
#line 127
{
  uint8_t op = ByteEEPROMC$state;

  ByteEEPROMC$state = ByteEEPROMC$S_IDLE;
  switch (op) 
    {
      case ByteEEPROMC$S_READ: 
        ByteEEPROMC$ReadData$readDone(ByteEEPROMC$appID, ByteEEPROMC$dataBuffer, ByteEEPROMC$numBytes, success);
      break;

      case ByteEEPROMC$S_WRITE: 
        ByteEEPROMC$WriteData$writeDone(ByteEEPROMC$appID, ByteEEPROMC$dataBuffer, ByteEEPROMC$numBytes, success);
      break;

      case ByteEEPROMC$S_APPEND: 
        if (success) {
          ByteEEPROMC$appendOffset[ByteEEPROMC$appID] += ByteEEPROMC$numBytes;
          }
#line 144
      ByteEEPROMC$LogData$appendDone(ByteEEPROMC$appID, ByteEEPROMC$dataBuffer, ByteEEPROMC$numBytes, success);
      break;

      case ByteEEPROMC$S_SYNC: 
        ByteEEPROMC$LogData$syncDone(ByteEEPROMC$appID, success);
      break;

      case ByteEEPROMC$S_ERASE: 
        ByteEEPROMC$LogData$eraseDone(ByteEEPROMC$appID, success);
      break;

      case ByteEEPROMC$S_FLUSH: 
        ByteEEPROMC$WriteData$flushDone(ByteEEPROMC$appID, success);
      break;
    }
}

# 49 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMShare.nc"
static result_t PageEEPROMShare$check(result_t requestOk)
#line 49
{
  if (requestOk != FAIL) {
    return requestOk;
    }
#line 52
  PageEEPROMShare$lastClient = 0;
  return FAIL;
}

# 475 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static result_t PageEEPROMM$newRequest(uint8_t req, eeprompage_t page, 
eeprompageoffset_t offset, 
void *reqdata, eeprompageoffset_t n)
#line 477
{


  if (((
#line 479
  page >= TOS_EEPROM_MAX_PAGES || offset >= TOS_EEPROM_PAGE_SIZE) || 
  n > TOS_EEPROM_PAGE_SIZE) || offset + n > TOS_EEPROM_PAGE_SIZE) {
    return FAIL;
    }

  if (PageEEPROMM$request != PageEEPROMM$IDLE) {
    return FAIL;
    }
#line 486
  PageEEPROMM$request = req;

  if (PageEEPROMM$broken) 
    {
      TOS_post(PageEEPROMM$taskFail);
      return SUCCESS;
    }

  PageEEPROMM$reqBuf = reqdata;
  PageEEPROMM$reqBytes = n;
  PageEEPROMM$reqPage = page;
  PageEEPROMM$reqOffset = offset;

  if (page == PageEEPROMM$buffer[0].page) {
    PageEEPROMM$selected = 0;
    }
  else {
#line 501
    if (page == PageEEPROMM$buffer[1].page) {
      PageEEPROMM$selected = 1;
      }
    else {
#line 504
      PageEEPROMM$selected = !PageEEPROMM$selected;
      }
    }
#line 506
  PageEEPROMM$handleRWRequest();

  return SUCCESS;
}

#line 460
static void PageEEPROMM$requestDone(result_t result)
#line 460
{
  uint8_t orequest = PageEEPROMM$request;

  PageEEPROMM$request = PageEEPROMM$IDLE;
  switch (orequest) 
    {
      case PageEEPROMM$R_READ: PageEEPROMM$PageEEPROM$readDone(result);
#line 466
      break;
      case PageEEPROMM$R_READCRC: PageEEPROMM$PageEEPROM$computeCrcDone(result, PageEEPROMM$computedCrc);
#line 467
      break;
      case PageEEPROMM$R_WRITE: PageEEPROMM$PageEEPROM$writeDone(result);
#line 468
      break;
      case PageEEPROMM$R_SYNC: case PageEEPROMM$R_SYNCALL: PageEEPROMM$PageEEPROM$syncDone(result);
#line 469
      break;
      case PageEEPROMM$R_FLUSH: case PageEEPROMM$R_FLUSHALL: PageEEPROMM$PageEEPROM$flushDone(result);
#line 470
      break;
      case PageEEPROMM$R_ERASE: PageEEPROMM$PageEEPROM$eraseDone(result);
#line 471
      break;
    }
}

#line 371
static void PageEEPROMM$handleRWRequest(void )
#line 371
{
  if (PageEEPROMM$reqPage == PageEEPROMM$buffer[PageEEPROMM$selected].page) {
    switch (PageEEPROMM$request) 
      {
        case PageEEPROMM$R_ERASE: 
          switch (PageEEPROMM$reqOffset) 
            {
              case TOS_EEPROM_ERASE: 
                PageEEPROMM$cmdPhase = PageEEPROMM$P_ERASE;
              PageEEPROMM$execCommand(TRUE, PageEEPROMM$C_ERASE_PAGE, 0, PageEEPROMM$reqPage, 0);
              break;
              case TOS_EEPROM_PREVIOUSLY_ERASED: 

                PageEEPROMM$buffer[PageEEPROMM$selected].erased = TRUE;

              case TOS_EEPROM_DONT_ERASE: 



                PageEEPROMM$buffer[PageEEPROMM$selected].clean = TRUE;
              PageEEPROMM$requestDone(SUCCESS);
              break;
            }
        break;

        case PageEEPROMM$R_SYNC: case PageEEPROMM$R_SYNCALL: 
            if (PageEEPROMM$buffer[PageEEPROMM$selected].clean && PageEEPROMM$buffer[PageEEPROMM$selected].unchecked) 
              {
                PageEEPROMM$checkBuffer(PageEEPROMM$selected);
                return;
              }

        case PageEEPROMM$R_FLUSH: case PageEEPROMM$R_FLUSHALL: 
            if (! PageEEPROMM$buffer[PageEEPROMM$selected].clean) {
              PageEEPROMM$flushBuffer();
              }
            else {
#line 406
              if (PageEEPROMM$request == PageEEPROMM$R_FLUSH || PageEEPROMM$request == PageEEPROMM$R_SYNC) {
                TOS_post(PageEEPROMM$taskSuccess);
                }
              else {

                  uint8_t oreq = PageEEPROMM$request;

                  PageEEPROMM$request = PageEEPROMM$IDLE;
                  PageEEPROMM$syncOrFlushAll(oreq);
                }
              }
#line 416
        break;

        case PageEEPROMM$R_READ: 
          PageEEPROMM$data = PageEEPROMM$reqBuf;
        PageEEPROMM$dataCount = PageEEPROMM$reqBytes;
        PageEEPROMM$cmdPhase = PageEEPROMM$P_READ;
        PageEEPROMM$execRWBuffer(PageEEPROMM$selected ? PageEEPROMM$C_READ_BUFFER1 : PageEEPROMM$C_READ_BUFFER2, 2, PageEEPROMM$reqOffset);
        break;

        case PageEEPROMM$R_READCRC: 
          PageEEPROMM$dataCount = PageEEPROMM$reqBytes;
        PageEEPROMM$cmdPhase = PageEEPROMM$P_READCRC;
        PageEEPROMM$execRWBuffer(PageEEPROMM$selected ? PageEEPROMM$C_READ_BUFFER1 : PageEEPROMM$C_READ_BUFFER2, 2, 0);
        break;

        case PageEEPROMM$R_WRITE: 
          PageEEPROMM$data = PageEEPROMM$reqBuf;
        PageEEPROMM$dataCount = PageEEPROMM$reqBytes;
        PageEEPROMM$cmdPhase = PageEEPROMM$P_WRITE;
        PageEEPROMM$buffer[PageEEPROMM$selected].clean = FALSE;
        PageEEPROMM$buffer[PageEEPROMM$selected].unchecked = 0;
        PageEEPROMM$execRWBuffer(PageEEPROMM$selected ? PageEEPROMM$C_WRITE_BUFFER1 : PageEEPROMM$C_WRITE_BUFFER2, 0, PageEEPROMM$reqOffset);
        break;
      }
    }
  else {
#line 440
    if (! PageEEPROMM$buffer[PageEEPROMM$selected].clean) {
      PageEEPROMM$flushBuffer();
      }
    else {
#line 442
      if (PageEEPROMM$buffer[PageEEPROMM$selected].unchecked) {
        PageEEPROMM$checkBuffer(PageEEPROMM$selected);
        }
      else {

          if (PageEEPROMM$request == PageEEPROMM$R_ERASE) 
            {
              PageEEPROMM$buffer[PageEEPROMM$selected].page = PageEEPROMM$reqPage;
              PageEEPROMM$handleRWRequest();
            }
          else 
            {
              PageEEPROMM$cmdPhase = PageEEPROMM$P_FILL;
              PageEEPROMM$execCommand(TRUE, PageEEPROMM$selected ? PageEEPROMM$C_FILL_BUFFER1 : PageEEPROMM$C_FILL_BUFFER2, 0, PageEEPROMM$reqPage, 0);
            }
        }
      }
    }
}

#line 350
static void PageEEPROMM$execCommand(bool wait, uint8_t reqCmd, uint8_t dontCare, 
eeprompage_t page, eeprompageoffset_t offset)
#line 351
{

  PageEEPROMM$cmd[0] = reqCmd;
  PageEEPROMM$cmd[1] = page >> 7;
  PageEEPROMM$cmd[2] = (page << 1) | (offset >> 8);
  PageEEPROMM$cmd[3] = offset;
  PageEEPROMM$cmdCount = 4 + dontCare;

  if (wait && PageEEPROMM$flashBusy) {
    PageEEPROMM$requestFlashStatus();
    }
  else {
#line 362
    PageEEPROMM$sendFlashCommand();
    }
}

#line 154
static void PageEEPROMM$requestFlashStatus(void )
#line 154
{
  PageEEPROMM$waiting = TRUE;
  PageEEPROMM$selectFlash();

  ;
  PageEEPROMM$FlashSPI$txByte(PageEEPROMM$C_REQ_STATUS);
  if (PageEEPROMM$FlashIdle$wait() == FAIL) {
    PageEEPROMM$FlashIdle$available();
    }
}

# 111 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLFlash.nc"
static void HPLFlash$idleWait(void )
#line 111
{
  if (TOSH_READ_FLASH_IN_PIN()) {
    HPLFlash$FlashIdle$available();
    }
  else {
#line 115
    TOS_post(HPLFlash$idleWait);
    }
}

# 147 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/PageEEPROMM.nc"
static result_t PageEEPROMM$FlashIdle$available(void )
#line 147
{
  if (PageEEPROMM$cmdPhase == PageEEPROMM$P_COMPARE_CHECK) {
    PageEEPROMM$compareOk = PageEEPROMM$getCompareStatus();
    }
#line 150
  PageEEPROMM$requestDeselect();
  return SUCCESS;
}

#line 341
static result_t PageEEPROMM$FlashSelect$notifyHigh(void )
#line 341
{
  if (PageEEPROMM$deselectRequested) 
    {
      PageEEPROMM$deselectRequested = FALSE;
      PageEEPROMM$flashCommandComplete();
    }
  return SUCCESS;
}

#line 243
static void PageEEPROMM$checkBuffer(uint8_t buf)
#line 243
{
  PageEEPROMM$cmdPhase = PageEEPROMM$P_COMPARE;
  PageEEPROMM$checking = buf;
  PageEEPROMM$execCommand(TRUE, buf ? PageEEPROMM$C_COMPARE_BUFFER1 : PageEEPROMM$C_COMPARE_BUFFER2, 0, 
  PageEEPROMM$buffer[buf].page, 0);
}

#line 164
static void PageEEPROMM$sendFlashCommand(void )
#line 164
{
  uint8_t in = 0;
#line 165
  uint8_t out = 0;
  uint8_t *ptr = PageEEPROMM$cmd;
  eeprompageoffset_t count = PageEEPROMM$cmdCount;
  uint16_t crc = 0;
  uint8_t lphase = PageEEPROMM$P_SEND_CMD;







  ;

  PageEEPROMM$selectFlash();

  for (; ; ) 
    {
      if (lphase == PageEEPROMM$P_READCRC) 
        {
          crc = crcByte(crc, in);

          --count;
          if (!count) 
            {
              PageEEPROMM$computedCrc = crc;
              break;
            }
        }
      else {
#line 194
        if (lphase == PageEEPROMM$P_SEND_CMD) 
          {
            out = * ptr++;
            count--;
            if (!count) 
              {
                lphase = PageEEPROMM$cmdPhase;
                ptr = PageEEPROMM$data;
                count = PageEEPROMM$dataCount;
              }
          }
        else {
#line 205
          if (lphase == PageEEPROMM$P_READ) 
            {
              * ptr++ = in;
              --count;
              if (!count) {
                break;
                }
            }
          else {
#line 212
            if (lphase == PageEEPROMM$P_WRITE) 
              {
                if (!count) {
                  break;
                  }
                out = * ptr++;
                --count;
              }
            else {
              break;
              }
            }
          }
        }
#line 223
      in = PageEEPROMM$FlashSPI$txByte(out);
    }

  PageEEPROMM$requestDeselect();
}

#line 250
static void PageEEPROMM$flushBuffer(void )
#line 250
{
  PageEEPROMM$cmdPhase = PageEEPROMM$P_FLUSH;
  PageEEPROMM$execCommand(TRUE, PageEEPROMM$buffer[PageEEPROMM$selected].erased ? 
  PageEEPROMM$selected ? PageEEPROMM$C_QFLUSH_BUFFER1 : PageEEPROMM$C_QFLUSH_BUFFER2 : 
  PageEEPROMM$selected ? PageEEPROMM$C_FLUSH_BUFFER1 : PageEEPROMM$C_FLUSH_BUFFER2, 0, 
  PageEEPROMM$buffer[PageEEPROMM$selected].page, 0);
}

#line 604
static result_t PageEEPROMM$syncOrFlushAll(uint8_t newReq)
#line 604
{
  if (PageEEPROMM$request != PageEEPROMM$IDLE) {
    return FAIL;
    }
#line 607
  PageEEPROMM$request = newReq;

  if (PageEEPROMM$broken) 
    {
      TOS_post(PageEEPROMM$taskFail);
      return SUCCESS;
    }
  else {
#line 614
    if (! PageEEPROMM$buffer[0].clean) {
      PageEEPROMM$selected = 0;
      }
    else {
#line 616
      if (! PageEEPROMM$buffer[1].clean) {
        PageEEPROMM$selected = 1;
        }
      else {
          TOS_post(PageEEPROMM$taskSuccess);
          return SUCCESS;
        }
      }
    }
#line 624
  PageEEPROMM$buffer[PageEEPROMM$selected].unchecked = 0;
  PageEEPROMM$handleRWRequest();

  return SUCCESS;
}


#define nx_struct struct
#define nx_union union
#define dbg(mode, format, ...) ((void)0)
#define dbg_clear(mode, format, ...) ((void)0)
#define dbg_active(mode) 0
# 152 "/opt/local/lib/gcc/avr/4.1.2/include/stddef.h" 3
typedef int ptrdiff_t;
#line 214
typedef unsigned int size_t;
#line 326
typedef int wchar_t;
# 8 "/opt/local/lib/ncc/deputy_nodeputy.h"
struct __nesc_attr_nonnull {
#line 8
  int dummy;
}  ;
#line 9
struct __nesc_attr_bnd {
#line 9
  void *lo, *hi;
}  ;
#line 10
struct __nesc_attr_bnd_nok {
#line 10
  void *lo, *hi;
}  ;
#line 11
struct __nesc_attr_count {
#line 11
  int n;
}  ;
#line 12
struct __nesc_attr_count_nok {
#line 12
  int n;
}  ;
#line 13
struct __nesc_attr_one {
#line 13
  int dummy;
}  ;
#line 14
struct __nesc_attr_one_nok {
#line 14
  int dummy;
}  ;
#line 15
struct __nesc_attr_dmemset {
#line 15
  int a1, a2, a3;
}  ;
#line 16
struct __nesc_attr_dmemcpy {
#line 16
  int a1, a2, a3;
}  ;
#line 17
struct __nesc_attr_nts {
#line 17
  int dummy;
}  ;
# 121 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdint.h" 3
typedef int int8_t __attribute((__mode__(__QI__))) ;
typedef unsigned int uint8_t __attribute((__mode__(__QI__))) ;
typedef int int16_t __attribute((__mode__(__HI__))) ;
typedef unsigned int uint16_t __attribute((__mode__(__HI__))) ;
typedef int int32_t __attribute((__mode__(__SI__))) ;
typedef unsigned int uint32_t __attribute((__mode__(__SI__))) ;

typedef int int64_t __attribute((__mode__(__DI__))) ;
typedef unsigned int uint64_t __attribute((__mode__(__DI__))) ;
#line 142
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
#line 159
typedef int8_t int_least8_t;




typedef uint8_t uint_least8_t;




typedef int16_t int_least16_t;




typedef uint16_t uint_least16_t;




typedef int32_t int_least32_t;




typedef uint32_t uint_least32_t;







typedef int64_t int_least64_t;






typedef uint64_t uint_least64_t;
#line 213
typedef int8_t int_fast8_t;




typedef uint8_t uint_fast8_t;




typedef int16_t int_fast16_t;




typedef uint16_t uint_fast16_t;




typedef int32_t int_fast32_t;




typedef uint32_t uint_fast32_t;







typedef int64_t int_fast64_t;






typedef uint64_t uint_fast64_t;
#line 273
typedef int64_t intmax_t;




typedef uint64_t uintmax_t;
# 77 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/inttypes.h" 3
typedef int32_t int_farptr_t;



typedef uint32_t uint_farptr_t;
# 431 "/opt/local/lib/ncc/nesc_nx.h"
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nx_int8_t;typedef int8_t __nesc_nxbase_nx_int8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nx_int16_t;typedef int16_t __nesc_nxbase_nx_int16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nx_int32_t;typedef int32_t __nesc_nxbase_nx_int32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nx_int64_t;typedef int64_t __nesc_nxbase_nx_int64_t  ;
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nx_uint8_t;typedef uint8_t __nesc_nxbase_nx_uint8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nx_uint16_t;typedef uint16_t __nesc_nxbase_nx_uint16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nx_uint32_t;typedef uint32_t __nesc_nxbase_nx_uint32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nx_uint64_t;typedef uint64_t __nesc_nxbase_nx_uint64_t  ;


typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nxle_int8_t;typedef int8_t __nesc_nxbase_nxle_int8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nxle_int16_t;typedef int16_t __nesc_nxbase_nxle_int16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nxle_int32_t;typedef int32_t __nesc_nxbase_nxle_int32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nxle_int64_t;typedef int64_t __nesc_nxbase_nxle_int64_t  ;
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nxle_uint8_t;typedef uint8_t __nesc_nxbase_nxle_uint8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nxle_uint16_t;typedef uint16_t __nesc_nxbase_nxle_uint16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nxle_uint32_t;typedef uint32_t __nesc_nxbase_nxle_uint32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nxle_uint64_t;typedef uint64_t __nesc_nxbase_nxle_uint64_t  ;
# 117 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/string.h" 3
extern char *strcat(char *arg_0x100757db0, const char *arg_0x1007560c8);
extern char *strchr(const char *arg_0x100756970, int arg_0x100756bd8) __attribute((__pure__)) ;


extern char *strcpy(char *arg_0x10075be28, const char *arg_0x10075a138);





extern size_t strlen(const char *arg_0x10075ce60) __attribute((__pure__)) ;

extern char *strncat(char *arg_0x100761060, const char *arg_0x100761338, size_t arg_0x1007615e0);

extern char *strncpy(char *arg_0x100767020, const char *arg_0x1007672f8, size_t arg_0x1007675a0);







extern char *strstr(const char *arg_0x10076e780, const char *arg_0x10076ea58) __attribute((__pure__)) ;
extern char *strtok_r(char *arg_0x10076d408, const char *arg_0x10076d6e0, char **arg_0x10076d9b8);
# 71 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdlib.h" 3
#line 68
typedef struct __nesc_unnamed4242 {
  int quot;
  int rem;
} div_t;





#line 74
typedef struct __nesc_unnamed4243 {
  long quot;
  long rem;
} ldiv_t;


typedef int (*__compar_fn_t)(const void *arg_0x100771370, const void *arg_0x100771648);
#line 271
extern int atoi(const char *s) __attribute((__pure__)) ;
# 71 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
typedef unsigned char bool;






enum __nesc_unnamed4244 {
  FALSE = 0, 
  TRUE = 1
};








uint8_t TOS_ROUTE_PROTOCOL = 0x90;
#line 104
uint8_t TOS_BASE_STATION = 0;





const uint8_t TOS_DATA_LENGTH = 36;
#line 132
uint8_t TOS_PLATFORM = 7;










enum __nesc_unnamed4245 {
  FAIL = 0, 
  SUCCESS = 1
};


static inline uint8_t rcombine(uint8_t r1, uint8_t r2);
typedef uint8_t result_t  ;







static inline result_t rcombine(result_t r1, result_t r2);
#line 178
enum __nesc_unnamed4246 {
  NULL = 0x0
};
# 210 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/avr/pgmspace.h" 3
typedef void prog_void __attribute((__progmem__)) ;
typedef char prog_char __attribute((__progmem__)) ;
typedef unsigned char prog_uchar __attribute((__progmem__)) ;

typedef int8_t prog_int8_t __attribute((__progmem__)) ;
typedef uint8_t prog_uint8_t __attribute((__progmem__)) ;
typedef int16_t prog_int16_t __attribute((__progmem__)) ;
typedef uint16_t prog_uint16_t __attribute((__progmem__)) ;
typedef int32_t prog_int32_t __attribute((__progmem__)) ;
typedef uint32_t prog_uint32_t __attribute((__progmem__)) ;

typedef int64_t prog_int64_t __attribute((__progmem__)) ;
typedef uint64_t prog_uint64_t __attribute((__progmem__)) ;
# 118 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
enum __nesc_unnamed4247 {
  TOSH_period16 = 0x00, 
  TOSH_period32 = 0x01, 
  TOSH_period64 = 0x02, 
  TOSH_period128 = 0x03, 
  TOSH_period256 = 0x04, 
  TOSH_period512 = 0x05, 
  TOSH_period1024 = 0x06, 
  TOSH_period2048 = 0x07
};

static inline void TOSH_wait();







typedef uint8_t __nesc_atomic_t;

__nesc_atomic_t __nesc_atomic_start(void );
void __nesc_atomic_end(__nesc_atomic_t oldSreg);



__inline __nesc_atomic_t __nesc_atomic_start(void )  ;






__inline void __nesc_atomic_end(__nesc_atomic_t oldSreg)  ;






static __inline void __nesc_atomic_sleep();







static __inline void __nesc_enable_interrupt();



static __inline void __nesc_disable_interrupt();
# 39 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_uwait(int u_sec);
#line 63
static __inline void TOSH_SET_RED_LED_PIN();
#line 63
static __inline void TOSH_CLR_RED_LED_PIN();
#line 63
static __inline void TOSH_MAKE_RED_LED_OUTPUT();
static __inline void TOSH_SET_GREEN_LED_PIN();
#line 64
static __inline void TOSH_CLR_GREEN_LED_PIN();
#line 64
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT();
static __inline void TOSH_SET_YELLOW_LED_PIN();
#line 65
static __inline void TOSH_CLR_YELLOW_LED_PIN();
#line 65
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT();

static __inline void TOSH_CLR_SERIAL_ID_PIN();
#line 67
static __inline void TOSH_MAKE_SERIAL_ID_INPUT();
#line 79
static __inline void TOSH_SET_FLASH_SELECT_PIN();
#line 79
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT();
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT();
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT();
#line 98
static __inline void TOSH_SET_PW0_PIN();
#line 98
static __inline void TOSH_CLR_PW0_PIN();
#line 98
static __inline void TOSH_MAKE_PW0_OUTPUT();
static __inline void TOSH_SET_PW1_PIN();
#line 99
static __inline void TOSH_CLR_PW1_PIN();
#line 99
static __inline void TOSH_MAKE_PW1_OUTPUT();
static __inline void TOSH_SET_PW2_PIN();
#line 100
static __inline void TOSH_MAKE_PW2_OUTPUT();
static __inline void TOSH_MAKE_PW3_OUTPUT();
static __inline void TOSH_MAKE_PW4_OUTPUT();
static __inline void TOSH_MAKE_PW5_OUTPUT();
static __inline void TOSH_MAKE_PW6_OUTPUT();
static __inline void TOSH_MAKE_PW7_OUTPUT();
#line 134
static inline void TOSH_SET_PIN_DIRECTIONS(void );
#line 187
enum __nesc_unnamed4248 {
  TOSH_ADC_PORTMAPSIZE = 12
};

enum __nesc_unnamed4249 {


  TOSH_ACTUAL_VOLTAGE_PORT = 30, 
  TOSH_ACTUAL_BANDGAP_PORT = 30, 
  TOSH_ACTUAL_GND_PORT = 31
};

enum __nesc_unnamed4250 {


  TOS_ADC_VOLTAGE_PORT = 7, 
  TOS_ADC_BANDGAP_PORT = 10, 
  TOS_ADC_GND_PORT = 11
};


uint32_t TOS_UART0_BAUDRATE = 57600;
# 33 "/Users/wbennett/opt/MoteWorks/tos/types/dbg_modes.h"
typedef long long TOS_dbg_mode;



enum __nesc_unnamed4251 {
  DBG_ALL = ~0ULL, 


  DBG_BOOT = 1ULL << 0, 
  DBG_CLOCK = 1ULL << 1, 
  DBG_TASK = 1ULL << 2, 
  DBG_SCHED = 1ULL << 3, 
  DBG_SENSOR = 1ULL << 4, 
  DBG_LED = 1ULL << 5, 
  DBG_CRYPTO = 1ULL << 6, 


  DBG_ROUTE = 1ULL << 7, 
  DBG_AM = 1ULL << 8, 
  DBG_CRC = 1ULL << 9, 
  DBG_PACKET = 1ULL << 10, 
  DBG_ENCODE = 1ULL << 11, 
  DBG_RADIO = 1ULL << 12, 


  DBG_LOG = 1ULL << 13, 
  DBG_ADC = 1ULL << 14, 
  DBG_I2C = 1ULL << 15, 
  DBG_UART = 1ULL << 16, 
  DBG_PROG = 1ULL << 17, 
  DBG_SOUNDER = 1ULL << 18, 
  DBG_TIME = 1ULL << 19, 
  DBG_POWER = 1ULL << 20, 



  DBG_SIM = 1ULL << 21, 
  DBG_QUEUE = 1ULL << 22, 
  DBG_SIMRADIO = 1ULL << 23, 
  DBG_HARD = 1ULL << 24, 
  DBG_MEM = 1ULL << 25, 



  DBG_USR1 = 1ULL << 27, 
  DBG_USR2 = 1ULL << 28, 
  DBG_USR3 = 1ULL << 29, 
  DBG_TEMP = 1ULL << 30, 

  DBG_ERROR = 1ULL << 31, 
  DBG_NONE = 0, 

  DBG_DEFAULT = DBG_ALL
};
# 41 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
#line 39
typedef struct __nesc_unnamed4252 {
  void (*tp)();
} TOSH_sched_entry_T;

enum __nesc_unnamed4253 {






  TOSH_MAX_TASKS = 32, 

  TOSH_TASK_BITMASK = TOSH_MAX_TASKS - 1
};

volatile TOSH_sched_entry_T TOSH_queue[TOSH_MAX_TASKS];
uint8_t TOSH_sched_full;
volatile uint8_t TOSH_sched_free;

static inline void TOSH_sched_init(void );








bool TOS_post(void (*tp)());
#line 82
bool TOS_post(void (*tp)())  ;
#line 116
static inline bool TOSH_run_next_task();
#line 139
static inline void TOSH_run_task();
# 187 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static void *nmemcpy(void *to, const void *from, size_t n);









static inline void *nmemset(void *to, int val, size_t n);
# 14 "/Users/wbennett/opt/MoteWorks/tos/system/Ident.h"
enum __nesc_unnamed4254 {

  IDENT_MAX_PROGRAM_NAME_LENGTH = 17
};






#line 19
typedef struct __nesc_unnamed4255 {

  uint32_t unix_time;
  uint32_t user_hash;
  char program_name[IDENT_MAX_PROGRAM_NAME_LENGTH];
} Ident_t;
# 43 "/opt/local/lib/gcc/avr/4.1.2/include/stdarg.h" 3
typedef __builtin_va_list __gnuc_va_list;
#line 105
typedef __gnuc_va_list va_list;
# 242 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdio.h" 3
struct __file {
  char *buf;
  unsigned char unget;
  uint8_t flags;
#line 261
  int size;
  int len;
  int (*put)(char arg_0x1007cba50, struct __file *arg_0x1007cbd90);
  int (*get)(struct __file *arg_0x10111b4d0);
  void *udata;
};
#line 405
struct __file;
#line 417
struct __file;
#line 1052
extern int sscanf(const char *__buf, const char *__fmt, ...);
# 12 "/Users/wbennett/opt/MoteWorks/lib/debug_constants.h"
static char dbg_buffer[256];
static uint16_t dbg_read_pointer;
static uint16_t dbg_write_pointer;
static int16_t dbg_data_size;
void (*full_event)(void ) = (void *)0;
#line 42
static inline char twi_pull_char(void );
#line 56
static inline void twi_attach_full_event(void (*handler)(void ));



static inline int8_t twi_buffer_empty(void );
# 49 "/Users/wbennett/opt/MoteWorks/include/Tracker/gsm.h"
#line 45
typedef struct __nesc_unnamed4256 {
  uint8_t length;
  int8_t data[200];
} 
__attribute((packed))  General_Msg;
typedef General_Msg *General_MsgPtr;






#line 52
typedef struct __nesc_unnamed4257 {
  uint8_t contents[4];
  uint8_t front;
  uint8_t count;
} 
RecvQueue_t;
typedef RecvQueue_t recvQueue_t;

typedef uint8_t gsm_result_t;
enum __nesc_unnamed4258 {

  ERROR = 0, 
  OK = 1, 
  TIMEOUT_ERROR = 3
};

typedef uint8_t gsm_assoc_status_t;
enum __nesc_unnamed4259 {


  NOT_REGISTERED_NO_LOOK = 0, 
  REGISTERED_HOME = 1, 
  NOT_REGISTERED_LOOK = 2, 
  REGISTRATION_DENIED = 3, 
  UNKNOWN = 4, 
  REGISTERED_ROAMING = 5
};

typedef uint8_t gsm_assoc_mode_t;
enum __nesc_unnamed4260 {

  DISABLE_NETWORK_REGISTRATION = 0, 
  ENABLE_NETWORK_REGISTRATION_RESULT_CODE = 1, 
  ENABLE_NETWORK_REGISTRATION_WITH_CELL_ID = 2
};







#line 88
typedef struct __nesc_unnamed4261 {

  uint8_t status;
  uint16_t cellid;
  uint16_t areacode;
} 
__attribute((packed))  Gsm_cellid_areacode_data_t;
typedef Gsm_cellid_areacode_data_t gsm_cellid_areacode_data_t;






#line 97
typedef struct __nesc_unnamed4262 {

  uint8_t rssi;
  uint8_t bit_err_rate;
} 
__attribute((packed))  Gsm_signal_quality_data_t;
typedef Gsm_signal_quality_data_t gsm_signal_quality_data_t;









#line 105
typedef struct __nesc_unnamed4263 {

  char netname[8];
  uint8_t bsic;
  uint16_t lac;
  uint16_t id;
  uint16_t arfcn;
} 
__attribute((packed))  Gsm_cellmon_record_t;
typedef Gsm_cellmon_record_t gsm_cellmon_record_t;







#line 116
typedef struct __nesc_unnamed4264 {

  Gsm_signal_quality_data_t signal_quality_data;
  Gsm_cellmon_record_t gsm_cellmon_record;
  uint8_t seqnum;
} 
__attribute((packed))  Gsm_header_t;
typedef Gsm_header_t gsm_header_t;







#line 127
typedef struct __nesc_unnamed4265 {

  Gsm_cellmon_record_t towers[3];
} 
__attribute((packed))  Gsm_cellmon_data_t;
typedef Gsm_cellmon_data_t gsm_cellmon_data_t;

typedef uint16_t gsm_error_t;
enum __nesc_unnamed4266 {


  MS_OPERATION_NOT_SUPPORTED = 303u, 
  MS_SIM_NOT_INSERTED = 310u, 
  MS_SIM_PIN_REQUIRED = 311u, 
  MS_NO_NETWORK_SERVICE = 331u, 
  MS_NETWORK_TIMEOUT = 332u, 

  NO_MODEM_RESPONSE = 1000u, 
  GSM_UART_HANDLER_SEND_CMD_FAIL = 1001u, 
  SMS_PROMPT_NOT_RECVD = 1002u, 
  GSM_NOT_ASSOCIATED = 1003u, 
  GSM_BUSY = 1004u, 
  INVALID_PARAM = 1005u, 
  WATCHDOG_TIMED_OUT = 1006u, 
  UNSUPPORTED_FEATURE = 1007u, 
  NO_ERROR = 65535u
};
# 18 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.h"
enum __nesc_unnamed4267 {
  TIMER_REPEAT = 0, 
  TIMER_ONE_SHOT = 1, 
  NUM_TIMERS = 4U
};
# 89 "/Users/wbennett/opt/MoteWorks/tos/radio/rf230/AM.h"
enum __nesc_unnamed4268 {
  TOS_BCAST_ADDR = 0xffff, 
  TOS_UART_ADDR = 0x007e
};









enum __nesc_unnamed4269 {
  TOS_DEFAULT_AM_GROUP = 0x7d
};
#line 156
#line 132
typedef struct TOS_Msg {


  uint8_t length;
  uint8_t fcfhi;
  uint8_t fcflo;
  uint8_t dsn;
  uint16_t destpan;
  uint16_t addr;
  uint8_t type;
  uint8_t group;
  int8_t data[29 + 3];







  uint8_t strength;
  uint8_t lqi;
  bool crc;
  uint8_t ack;
  uint16_t time;
} TOS_Msg;




#line 158
typedef struct TinySec_Msg {

  uint8_t invalid;
} TinySec_Msg;









#line 163
typedef struct Ack_Msg {

  uint8_t length;
  uint8_t fcfhi;
  uint8_t fcflo;
  uint8_t dsn;
  uint8_t fcshi;
  uint8_t fcslo;
} Ack_Msg;

enum __nesc_unnamed4270 {

  MSG_HEADER_SIZE = (unsigned short )& ((struct TOS_Msg *)0)->data - 1, 

  MSG_FOOTER_SIZE = 2, 

  MSG_DATA_SIZE = (unsigned short )& ((struct TOS_Msg *)0)->strength + sizeof(uint16_t ), 

  DATA_LENGTH = 29, 

  LENGTH_BYTE_NUMBER = (unsigned short )& ((struct TOS_Msg *)0)->length + 1, 

  TOS_HEADER_SIZE = 5
};

typedef TOS_Msg *TOS_MsgPtr;
# 30 "/Users/wbennett/opt/MoteWorks/lib/avrtime.h"
struct avr_tm {
  int8_t sec;
  int8_t min;
  int8_t hour;
  int8_t day;
  int8_t mon;
  int16_t year;
  int8_t wday;
  int16_t day_of_year;
  uint8_t is_dst;
  uint8_t hundreth;
};

typedef uint64_t avrtime_t;


static inline void set_time_millis(avrtime_t set_time);

static inline void add_time_millis(uint32_t time_to_add);
static inline void init_avrtime();

static inline avrtime_t get_time_millis();





static inline void reset_start_time();



struct avr_tm;

struct avr_tm;
# 15 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static avrtime_t start_time;
static avrtime_t sys_time;
static bool isInit = 0;


static inline void set_time_millis(avrtime_t set_time);








static inline void init_avrtime();








static inline void add_time_millis(uint32_t time_to_add);







static inline avrtime_t get_time_millis();
#line 70
static inline void reset_start_time();
#line 133
struct avr_tm;
# 13 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/Clock.h"
enum __nesc_unnamed4271 {
  TOS_I1000PS = 32, TOS_S1000PS = 1, 
  TOS_I100PS = 40, TOS_S100PS = 2, 
  TOS_I10PS = 101, TOS_S10PS = 3, 
  TOS_I1024PS = 0, TOS_S1024PS = 3, 
  TOS_I512PS = 1, TOS_S512PS = 3, 
  TOS_I256PS = 3, TOS_S256PS = 3, 
  TOS_I128PS = 7, TOS_S128PS = 3, 
  TOS_I64PS = 15, TOS_S64PS = 3, 
  TOS_I32PS = 31, TOS_S32PS = 3, 
  TOS_I16PS = 63, TOS_S16PS = 3, 
  TOS_I8PS = 127, TOS_S8PS = 3, 
  TOS_I4PS = 255, TOS_S4PS = 3, 
  TOS_I2PS = 15, TOS_S2PS = 7, 
  TOS_I1PS = 31, TOS_S1PS = 7, 
  TOS_I0PS = 0, TOS_S0PS = 0
};
enum __nesc_unnamed4272 {
  DEFAULT_SCALE = 3, DEFAULT_INTERVAL = 127
};
# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
static result_t PotM$Pot$init(uint8_t initialSetting);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
static result_t HPLPotC$Pot$finalise(void );
#line 38
static result_t HPLPotC$Pot$decrease(void );







static result_t HPLPotC$Pot$increase(void );
# 32 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static result_t HPLInit$init(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TestGSMDriverM$Timer$fired(void );
# 24 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/SplitControlStatus.nc"
static result_t TestGSMDriverM$GsmControl$initDone(void );
#line 39
static result_t TestGSMDriverM$GsmControl$startDone(gsm_error_t result);
#line 53
static result_t TestGSMDriverM$GsmControl$stopDone(gsm_error_t result);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t TestGSMDriverM$StdControl$init(void );






static result_t TestGSMDriverM$StdControl$start(void );
# 52 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSM_ModemI.nc"
static result_t TestGSMDriverM$GSM_Modem$sendSMSDone(gsm_error_t result, uint8_t *endpoint, uint8_t *data);
#line 34
static result_t TestGSMDriverM$GSM_Modem$setCellMonitorReportValueDone(gsm_error_t result);





static result_t TestGSMDriverM$GSM_Modem$cellMonitorReportReady(gsm_error_t result, gsm_cellmon_data_t *data);





static result_t TestGSMDriverM$GSM_Modem$signalQualityReady(gsm_error_t result, gsm_signal_quality_data_t *data);
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t GSMDriverM$Timer$fired(void );
# 36 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
static void GSMDriverM$GsmUartHandler$ErrorReceived(gsm_error_t errorCode);
#line 61
static result_t GSMDriverM$GsmUartHandler$SendCommandDone(uint8_t *cmd, result_t result);
#line 30
static void GSMDriverM$GsmUartHandler$OkReceived(void );
#line 43
static void GSMDriverM$GsmUartHandler$NetworkRegistrationReceived(gsm_cellid_areacode_data_t *ptr, uint8_t dataLength);
#line 66
static void GSMDriverM$GsmUartHandler$SignalQualityReceived(gsm_signal_quality_data_t *ptr, uint8_t dataLength);
#line 56
static void GSMDriverM$GsmUartHandler$PromptReceived(void );
#line 50
static void GSMDriverM$GsmUartHandler$CellMonitorReportReceived(gsm_cellmon_data_t *ptr, uint16_t dataLength);
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t GSMDriverM$CommandTimer$fired(void );
# 18 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/SplitControlStatus.nc"
static result_t GSMDriverM$SplitControlStatus$init(void );
#line 31
static result_t GSMDriverM$SplitControlStatus$start(void );
#line 47
static result_t GSMDriverM$SplitControlStatus$stop(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t GSMDriverM$WatchdogTimer$fired(void );
# 22 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSM_ModemI.nc"
static result_t GSMDriverM$GSM_Modem$getSignalQuality(void );
#line 21
static result_t GSMDriverM$GSM_Modem$getCellMonitorReport(void );






static result_t GSMDriverM$GSM_Modem$sendSMS(uint8_t *endpoint, uint8_t *data);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendVarLenPacket.nc"
static result_t GsmUartHandlerM$SendVarLenPacketGsm$sendDone(uint8_t *packet, result_t success);
# 22 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
static result_t GsmUartHandlerM$GsmUartHandler$sendCommand(char *cmd, uint8_t length);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr GsmUartHandlerM$Receive$receive(TOS_MsgPtr m);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t GsmUartHandlerM$StdControl$init(void );






static result_t GsmUartHandlerM$StdControl$start(void );







static result_t GsmUartHandlerM$StdControl$stop(void );
# 33 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendVarLenPacket.nc"
static result_t UARTPacket$SendVarLenPacket$send(uint8_t *packet, uint8_t numBytes);
# 62 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t UARTPacket$ByteComm$txDone(void );
#line 54
static result_t UARTPacket$ByteComm$txByteReady(bool success);
#line 45
static result_t UARTPacket$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t UARTPacket$Control$init(void );






static result_t UARTPacket$Control$start(void );







static result_t UARTPacket$Control$stop(void );
# 18 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static result_t UARTPacket$txBytes(uint8_t *bytes, uint8_t numBytes);
# 66 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t UART0M$HPLUART$get(uint8_t data);







static result_t UART0M$HPLUART$putDone(void );
# 34 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t UART0M$ByteComm$txByte(uint8_t data);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t UART0M$Control$init(void );






static result_t UART0M$Control$start(void );







static result_t UART0M$Control$stop(void );
# 40 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t HPLUART0M$UART$init(void );
#line 58
static result_t HPLUART0M$UART$put(uint8_t data);
#line 48
static result_t HPLUART0M$UART$stop(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
static result_t HPLUART0M$Setbaud(uint32_t baud_rate);
# 101 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t LedsC$Leds$yellowOff(void );
#line 93
static result_t LedsC$Leds$yellowOn(void );
#line 35
static result_t LedsC$Leds$init(void );
#line 110
static result_t LedsC$Leds$yellowToggle(void );
#line 128
static result_t LedsC$Leds$set(uint8_t value);
#line 43
static result_t LedsC$Leds$redOn(void );
#line 68
static result_t LedsC$Leds$greenOn(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t TimerM$Clock$fire(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t TimerM$StdControl$init(void );






static result_t TimerM$StdControl$start(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TimerM$Timer$default$fired(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x1016511a0);
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TimerM$Timer$start(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x1016511a0, 
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
char type, uint32_t interval);








static result_t TimerM$Timer$stop(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x1016511a0);
# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static void HPLClock$Clock$setInterval(uint8_t value);
#line 127
static result_t HPLClock$Clock$setIntervalAndScale(uint8_t interval, uint8_t scale);




static uint8_t HPLClock$Clock$readCounter(void );
#line 75
static result_t HPLClock$Clock$setRate(char interval, char scale);
#line 100
static uint8_t HPLClock$Clock$getInterval(void );
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void );
# 14 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static result_t DebugI2CM$flush(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t DebugI2CM$StdControl$init(void );






static result_t DebugI2CM$StdControl$start(void );
# 3 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
static void HighLevelTwiM$HighLevelTwiInterface$begin(void );







static void HighLevelTwiM$HighLevelTwiInterface$send(uint8_t data);
#line 7
static void HighLevelTwiM$HighLevelTwiInterface$beginTransmission(uint8_t address);

static uint8_t HighLevelTwiM$HighLevelTwiInterface$endTransmission(void );
# 7 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiInterface.nc"
static uint8_t LowLevelTwiM$LowLevelTwiInterface$twi_writeTo(uint8_t address, uint8_t *data, uint8_t length, uint8_t wait);
#line 3
static void LowLevelTwiM$LowLevelTwiInterface$twi_init(void );
# 27 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
static result_t RealMain$hardwareInit(void );
# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
static result_t RealMain$Pot$init(uint8_t initialSetting);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t RealMain$StdControl$init(void );






static result_t RealMain$StdControl$start(void );
# 34 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
int main(void )   ;
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
static result_t PotM$HPLPot$finalise(void );
#line 38
static result_t PotM$HPLPot$decrease(void );







static result_t PotM$HPLPot$increase(void );
# 70 "/Users/wbennett/opt/MoteWorks/tos/system/PotM.nc"
uint8_t PotM$potSetting;

static inline void PotM$setPot(uint8_t value);
#line 85
static inline result_t PotM$Pot$init(uint8_t initialSetting);
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$decrease(void );








static inline result_t HPLPotC$Pot$increase(void );








static inline result_t HPLPotC$Pot$finalise(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static inline result_t HPLInit$init(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TestGSMDriverM$Timer$start(char type, uint32_t interval);
# 18 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/SplitControlStatus.nc"
static result_t TestGSMDriverM$GsmControl$init(void );
#line 31
static result_t TestGSMDriverM$GsmControl$start(void );
#line 47
static result_t TestGSMDriverM$GsmControl$stop(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t TestGSMDriverM$TimerControl$init(void );






static result_t TestGSMDriverM$TimerControl$start(void );
# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t TestGSMDriverM$Leds$init(void );
#line 110
static result_t TestGSMDriverM$Leds$yellowToggle(void );
#line 128
static result_t TestGSMDriverM$Leds$set(uint8_t value);
#line 43
static result_t TestGSMDriverM$Leds$redOn(void );
#line 68
static result_t TestGSMDriverM$Leds$greenOn(void );
# 22 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSM_ModemI.nc"
static result_t TestGSMDriverM$GSM_Modem$getSignalQuality(void );
#line 21
static result_t TestGSMDriverM$GSM_Modem$getCellMonitorReport(void );






static result_t TestGSMDriverM$GSM_Modem$sendSMS(uint8_t *endpoint, uint8_t *data);
# 34 "TestGSMDriverM.nc"
uint8_t TestGSMDriverM$state;
enum TestGSMDriverM$__nesc_unnamed4273 {
  TestGSMDriverM$NONE, 
  TestGSMDriverM$STARTING, 
  TestGSMDriverM$READY, 
  TestGSMDriverM$GET_SIG_QUAL, 
  TestGSMDriverM$GET_CELL_MON, 
  TestGSMDriverM$SEND_SMS
};

static inline void TestGSMDriverM$transition(void );

static inline result_t TestGSMDriverM$StdControl$init(void );







static inline result_t TestGSMDriverM$StdControl$start(void );
#line 66
static inline result_t TestGSMDriverM$GsmControl$initDone(void );






static inline result_t TestGSMDriverM$GsmControl$startDone(gsm_error_t result);
#line 85
static inline result_t TestGSMDriverM$GsmControl$stopDone(gsm_error_t result);





static inline result_t TestGSMDriverM$GSM_Modem$setCellMonitorReportValueDone(gsm_error_t result);



static inline result_t TestGSMDriverM$GSM_Modem$cellMonitorReportReady(gsm_error_t result, gsm_cellmon_data_t *data);
#line 109
static inline result_t TestGSMDriverM$GSM_Modem$signalQualityReady(gsm_error_t result, gsm_signal_quality_data_t *data);









static result_t TestGSMDriverM$GSM_Modem$sendSMSDone(gsm_error_t result, uint8_t *endpoint, uint8_t *data);









static inline result_t TestGSMDriverM$Timer$fired(void );




static inline void TestGSMDriverM$transition(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t GSMDriverM$Timer$start(char type, uint32_t interval);








static result_t GSMDriverM$Timer$stop(void );
# 22 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
static result_t GSMDriverM$GsmUartHandler$sendCommand(char *cmd, uint8_t length);
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t GSMDriverM$CommandTimer$start(char type, uint32_t interval);








static result_t GSMDriverM$CommandTimer$stop(void );
# 24 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/SplitControlStatus.nc"
static result_t GSMDriverM$SplitControlStatus$initDone(void );
#line 39
static result_t GSMDriverM$SplitControlStatus$startDone(gsm_error_t result);
#line 53
static result_t GSMDriverM$SplitControlStatus$stopDone(gsm_error_t result);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t GSMDriverM$UARTControl$init(void );






static result_t GSMDriverM$UARTControl$start(void );







static result_t GSMDriverM$UARTControl$stop(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t GSMDriverM$WatchdogTimer$start(char type, uint32_t interval);








static result_t GSMDriverM$WatchdogTimer$stop(void );
# 52 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSM_ModemI.nc"
static result_t GSMDriverM$GSM_Modem$sendSMSDone(gsm_error_t result, uint8_t *endpoint, uint8_t *data);
#line 34
static result_t GSMDriverM$GSM_Modem$setCellMonitorReportValueDone(gsm_error_t result);





static result_t GSMDriverM$GSM_Modem$cellMonitorReportReady(gsm_error_t result, gsm_cellmon_data_t *data);





static result_t GSMDriverM$GSM_Modem$signalQualityReady(gsm_error_t result, gsm_signal_quality_data_t *data);
# 33 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
enum GSMDriverM$__nesc_unnamed4274 {

  GSMDriverM$NONE = 0, 

  GSMDriverM$GSM_START_READY, 
  GSMDriverM$GSM_VCC_ENABLE, 
  GSMDriverM$GSM_POWER_ON_PHASE1, 
  GSMDriverM$GSM_POWER_ON_PHASE2, 
  GSMDriverM$GSM_SET_BAUD_RATE, 
  GSMDriverM$GSM_SET_REGISTER_WITH_EXTRA_INFO, 
  GSMDriverM$GSM_SET_VERBOSE_MODE, 
  GSMDriverM$GSM_SET_NORTH_AMERICA_BAND, 
  GSMDriverM$GSM_SET_ENERGY_SAVE_MODE, 
  GSMDriverM$GSM_SET_TEXT_FORMAT, 
  GSMDriverM$GSM_CHECK_IF_ASSOC, 

  GSMDriverM$GSM_SET_CELL_MONITOR_RPT_VAL, 
  GSMDriverM$GSM_GET_CELL_MONITOR_RPT, 
  GSMDriverM$GSM_GET_SIGNAL_QUALITY, 
  GSMDriverM$SMS_REQUEST_PROMPT, 
  GSMDriverM$SMS_SEND_PAYLOAD
};
#line 79
gsm_error_t GSMDriverM$gsm_error_code;

gsm_cellid_areacode_data_t GSMDriverM$currentAssocData;
gsm_cellmon_data_t GSMDriverM$currentCellMonitorReportData;
gsm_signal_quality_data_t GSMDriverM$currentSignalQualityData;


uint8_t GSMDriverM$currentPhoneNumber[20];
uint8_t GSMDriverM$currentTextData[165];
uint32_t GSMDriverM$timeoutPeriod;
bool GSMDriverM$isAssociated;
bool GSMDriverM$isStartDone;
bool GSMDriverM$isConfigDone;

uint8_t GSMDriverM$currentCmdState;









uint8_t GSMDriverM$commandRetryCount;
uint8_t *GSMDriverM$CURRENT_COMMAND;

uint8_t *GSMDriverM$SET_BAUD_RATE_CMD = (uint8_t *)"AT+IPR=57600\r";
uint8_t *GSMDriverM$SET_REGISTER_MORE_INFO_CMD = (uint8_t *)"AT+CREG=2\r";
uint8_t *GSMDriverM$SET_VERBOSE_MODE_CMD = (uint8_t *)"AT+CMEE=2\r";
uint8_t *GSMDriverM$SET_NORTH_AMERICA_BAND_CMD = (uint8_t *)"AT#BND=3\r";
uint8_t *GSMDriverM$SET_ENERGY_SAVE_CMD = (uint8_t *)"AT+CFUN=1\r";
uint8_t *GSMDriverM$SET_TEXT_FORMAT_CMD = (uint8_t *)"AT+CMGF=1\r";
uint8_t *GSMDriverM$CHECK_IF_ASSOC_CMD = (uint8_t *)"AT+CREG?\r";



uint8_t *GSMDriverM$GET_CELL_MONITOR_RPT_CMD = (uint8_t *)"AT#MONI\r";

uint8_t *GSMDriverM$GET_SIGNAL_QUALITY_CMD = (uint8_t *)"AT+CSQ\r";


uint8_t *GSMDriverM$SMS_REQUEST_PROMPT_P1_CMD = (uint8_t *)"AT+CMGS=\"";
uint8_t *GSMDriverM$SMS_REQUEST_PROMPT_P2_CMD = (uint8_t *)"\"\r";
uint8_t GSMDriverM$SMS_REQUEST_PROMPT_WHOLE_CMD[30];
uint8_t GSMDriverM$SMS_SEND_DATA_BUFFER[165];


static __inline void GSMDriverM$initVars(void );
static __inline void GSMDriverM$initPins(void );
static __inline void GSMDriverM$enableGroundLine(void );
static __inline void GSMDriverM$disableGroundLine(void );
static __inline void GSMDriverM$enablePowerLine(void );
static __inline void GSMDriverM$disablePowerLine(void );
static __inline result_t GSMDriverM$sendCurrentCommand(void );

static inline void GSMDriverM$initDoneTask(void );
static inline void GSMDriverM$startDoneTask(void );
static void GSMDriverM$startFailTask(void );
static void GSMDriverM$stopDoneTask(void );
static inline void GSMDriverM$OkReceivedTask(void );
static inline void GSMDriverM$ErrorReceivedTask(void );
static inline void GSMDriverM$NetworkRegistrationReceivedTask(void );
static inline void GSMDriverM$setCellMonitorReportValueDoneTask(void );
static void GSMDriverM$setCellMonitorReportValueFailTask(void );
static inline void GSMDriverM$cellMonitorReportReadyTask(void );
static void GSMDriverM$cellMonitorReportFailTask(void );
static inline void GSMDriverM$signalQualityDataReadyTask(void );
static void GSMDriverM$signalQualityDataFailTask(void );
static inline void GSMDriverM$sendSMSDoneTask(void );
static void GSMDriverM$sendSMSFailTask(void );
static __inline void GSMDriverM$transition(void );



static __inline void GSMDriverM$initPins(void );









static __inline void GSMDriverM$enableGroundLine(void );







static __inline void GSMDriverM$disableGroundLine(void );








static __inline void GSMDriverM$enablePowerLine(void );








static __inline void GSMDriverM$disablePowerLine(void );





static __inline void GSMDriverM$setErrorCodeOnImedFail(void );
#line 213
static inline result_t GSMDriverM$SplitControlStatus$init(void );










static inline void GSMDriverM$initDoneTask(void );








static inline result_t GSMDriverM$SplitControlStatus$start(void );
#line 246
static __inline void GSMDriverM$initVars(void );
#line 258
static inline void GSMDriverM$startDoneTask(void );




static void GSMDriverM$startFailTask(void );
#line 278
static inline void GSMDriverM$gsmConfigurationDoneTask(void );
#line 295
static inline result_t GSMDriverM$SplitControlStatus$stop(void );








static void GSMDriverM$stopDoneTask(void );
#line 366
static inline result_t GSMDriverM$GSM_Modem$getCellMonitorReport(void );
#line 396
static inline result_t GSMDriverM$GSM_Modem$getSignalQuality(void );
#line 428
static inline result_t GSMDriverM$GSM_Modem$sendSMS(uint8_t *endpoint, uint8_t *data);
#line 466
static __inline result_t GSMDriverM$sendCurrentCommand(void );
#line 501
static __inline void GSMDriverM$transition(void );
#line 630
static inline result_t GSMDriverM$Timer$fired(void );







static inline result_t GSMDriverM$CommandTimer$fired(void );
#line 716
static inline result_t GSMDriverM$WatchdogTimer$fired(void );








static inline result_t GSMDriverM$GsmUartHandler$SendCommandDone(uint8_t *cmd, result_t result);





static inline void GSMDriverM$GsmUartHandler$OkReceived(void );





static void GSMDriverM$GsmUartHandler$ErrorReceived(gsm_error_t errorCode);






static inline void GSMDriverM$GsmUartHandler$NetworkRegistrationReceived(gsm_cellid_areacode_data_t *ptr, uint8_t dataLength);






static inline void GSMDriverM$GsmUartHandler$CellMonitorReportReceived(gsm_cellmon_data_t *ptr, uint16_t dataLength);








static inline void GSMDriverM$GsmUartHandler$SignalQualityReceived(gsm_signal_quality_data_t *ptr, uint8_t dataLength);








static inline void GSMDriverM$GsmUartHandler$PromptReceived(void );
#line 794
static inline void GSMDriverM$OkReceivedTask(void );
#line 826
static inline void GSMDriverM$ErrorReceivedTask(void );
#line 855
static inline void GSMDriverM$NetworkRegistrationReceivedTask(void );
#line 882
static inline void GSMDriverM$setCellMonitorReportValueDoneTask(void );






static void GSMDriverM$setCellMonitorReportValueFailTask(void );










static inline void GSMDriverM$cellMonitorReportReadyTask(void );






static void GSMDriverM$cellMonitorReportFailTask(void );










static inline void GSMDriverM$signalQualityDataReadyTask(void );





static void GSMDriverM$signalQualityDataFailTask(void );










static inline void GSMDriverM$sendSMSDoneTask(void );





static void GSMDriverM$sendSMSFailTask(void );
# 33 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendVarLenPacket.nc"
static result_t GsmUartHandlerM$SendVarLenPacketGsm$send(uint8_t *packet, uint8_t numBytes);
# 36 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
static void GsmUartHandlerM$GsmUartHandler$ErrorReceived(gsm_error_t errorCode);
#line 61
static result_t GsmUartHandlerM$GsmUartHandler$SendCommandDone(uint8_t *cmd, result_t result);
#line 30
static void GsmUartHandlerM$GsmUartHandler$OkReceived(void );
#line 43
static void GsmUartHandlerM$GsmUartHandler$NetworkRegistrationReceived(gsm_cellid_areacode_data_t *ptr, uint8_t dataLength);
#line 66
static void GsmUartHandlerM$GsmUartHandler$SignalQualityReceived(gsm_signal_quality_data_t *ptr, uint8_t dataLength);
#line 56
static void GsmUartHandlerM$GsmUartHandler$PromptReceived(void );
#line 50
static void GsmUartHandlerM$GsmUartHandler$CellMonitorReportReceived(gsm_cellmon_data_t *ptr, uint16_t dataLength);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t GsmUartHandlerM$UARTControl$init(void );






static result_t GsmUartHandlerM$UARTControl$start(void );







static result_t GsmUartHandlerM$UARTControl$stop(void );
# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t GsmUartHandlerM$Leds$init(void );
# 32 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
extern int sscanf(const char *__buf, const char *__fmt, ...)  ;



char GsmUartHandlerM$uartBuffer[4][256];

uint8_t GsmUartHandlerM$curBufIndex = 0;

gsm_cellid_areacode_data_t GsmUartHandlerM$regData;

uint8_t GsmUartHandlerM$gsmCellMonParamVal = 0;
uint8_t GsmUartHandlerM$gsmCellMonCount = 0;

gsm_cellmon_data_t GsmUartHandlerM$cellMonData;

gsm_signal_quality_data_t GsmUartHandlerM$sigQualData;

recvQueue_t GsmUartHandlerM$recvQueue;
char GsmUartHandlerM$trimBuf[50];
char GsmUartHandlerM$consBuf1[30];
char GsmUartHandlerM$consBuf2[30];
uint16_t GsmUartHandlerM$ec;



static inline void GsmUartHandlerM$initTask(void );
static inline void GsmUartHandlerM$startTask(void );
static inline void GsmUartHandlerM$stopTask(void );
static inline void GsmUartHandlerM$handleDataReceived(void );

static inline void GsmUartHandlerM$handleSendMoniFail(void );
static __inline void GsmUartHandlerM$checkForMessages(uint8_t index);
static __inline bool GsmUartHandlerM$checkForOkReceived(uint8_t index);
static __inline bool GsmUartHandlerM$checkForErrorReceived(uint8_t index);
static __inline bool GsmUartHandlerM$checkForNetworkRegistrationReceived(uint8_t index);
static __inline bool GsmUartHandlerM$checkForSignalQualityReceived(uint8_t index);
static __inline bool GsmUartHandlerM$checkForCellMonitorReceived(uint8_t index);
static __inline bool GsmUartHandlerM$checkForPromptReceived(uint8_t index);
static __inline uint8_t GsmUartHandlerM$countFieldsByDelimiter(char *src, const char delim);
static __inline result_t GsmUartHandlerM$enterRecvQueue(uint8_t element);
static __inline uint8_t GsmUartHandlerM$removeRecvQueue(void );
static __inline bool GsmUartHandlerM$isWhiteSpace(char c);
static __inline char *GsmUartHandlerM$trim(char *s);



static __inline result_t GsmUartHandlerM$enterRecvQueue(uint8_t element);
#line 95
static __inline uint8_t GsmUartHandlerM$removeRecvQueue(void );
#line 112
static __inline void GsmUartHandlerM$checkForMessages(uint8_t index);
#line 161
static __inline bool GsmUartHandlerM$checkForOkReceived(uint8_t index);
#line 181
static __inline bool GsmUartHandlerM$checkForErrorReceived(uint8_t index);
#line 203
static __inline bool GsmUartHandlerM$checkForNetworkRegistrationReceived(uint8_t index);
#line 277
static __inline bool GsmUartHandlerM$checkForSignalQualityReceived(uint8_t index);
#line 309
static __inline uint8_t GsmUartHandlerM$countFieldsByDelimiter(char *src, const char delim);
#line 323
static __inline bool GsmUartHandlerM$isWhiteSpace(char c);




static __inline char *GsmUartHandlerM$trim(char *s);
#line 349
static __inline char *GsmUartHandlerM$consolidate(char *s);
#line 393
static __inline bool GsmUartHandlerM$checkForCellMonitorReceived(uint8_t index);
#line 546
static __inline bool GsmUartHandlerM$checkForPromptReceived(uint8_t index);
#line 571
static inline result_t GsmUartHandlerM$StdControl$init(void );






static inline void GsmUartHandlerM$initTask(void );









static inline result_t GsmUartHandlerM$StdControl$start(void );






static inline void GsmUartHandlerM$startTask(void );








static inline result_t GsmUartHandlerM$StdControl$stop(void );






static inline void GsmUartHandlerM$stopTask(void );





static inline TOS_MsgPtr GsmUartHandlerM$Receive$receive(TOS_MsgPtr bufferPtr);
#line 634
static inline void GsmUartHandlerM$handleDataReceived(void );
#line 652
static inline void GsmUartHandlerM$handleSendMoniFail(void );
#line 667
static result_t GsmUartHandlerM$GsmUartHandler$sendCommand(char *cmd, uint8_t length);
#line 699
static inline result_t GsmUartHandlerM$SendVarLenPacketGsm$sendDone(uint8_t *sentdata, result_t result);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendVarLenPacket.nc"
static result_t UARTPacket$SendVarLenPacket$sendDone(uint8_t *packet, result_t success);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
static TOS_MsgPtr UARTPacket$Receive$receive(TOS_MsgPtr m);
# 34 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t UARTPacket$ByteComm$txByte(uint8_t data);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t UARTPacket$ByteControl$init(void );






static result_t UARTPacket$ByteControl$start(void );







static result_t UARTPacket$ByteControl$stop(void );
# 39 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
enum UARTPacket$__nesc_unnamed4275 {
  UARTPacket$NO_GSM_START_BYTE = 0, 
  UARTPacket$GSM_START_BYTE = 1, 
  UARTPacket$GSM_BUF_NOT_AVAIL = 2
};
uint8_t UARTPacket$gpsRecvState;



enum UARTPacket$__nesc_unnamed4276 {
  UARTPacket$IDLE, 
  UARTPacket$PACKET, 
  UARTPacket$BYTES
};
uint8_t UARTPacket$state;





uint16_t UARTPacket$rxCount;
#line 59
uint16_t UARTPacket$txCount;
#line 59
uint16_t UARTPacket$txLength;
uint8_t *UARTPacket$recPtr;
uint8_t *UARTPacket$sendPtr;



General_Msg UARTPacket$bufferSend;
General_Msg UARTPacket$bufferRecv[4];
uint8_t UARTPacket$curBufferRecvIndex = 0;

recvQueue_t UARTPacket$recvQueue;

static __inline result_t UARTPacket$enterRecvQueue(uint8_t element);
#line 88
static __inline uint8_t UARTPacket$removeRecvQueue(void );
#line 118
static __inline void UARTPacket$initState(void );
#line 136
static inline result_t UARTPacket$Control$init(void );







static inline result_t UARTPacket$Control$start(void );






static inline result_t UARTPacket$Control$stop(void );






static result_t UARTPacket$txBytes(uint8_t *bytes, uint8_t numBytes);
#line 178
static inline result_t UARTPacket$SendVarLenPacket$send(uint8_t *packet, uint8_t numBytes);








static inline void UARTPacket$sendVarLenFailTask(void );







static inline void UARTPacket$sendVarLenSuccessTask(void );







static void UARTPacket$sendComplete(result_t success);
#line 228
static inline result_t UARTPacket$ByteComm$txByteReady(bool success);
#line 244
static inline result_t UARTPacket$ByteComm$txDone(void );
#line 257
static inline void UARTPacket$receiveTask(void );
#line 275
static inline result_t UARTPacket$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength);
# 40 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t UART0M$HPLUART$init(void );
#line 58
static result_t UART0M$HPLUART$put(uint8_t data);
#line 48
static result_t UART0M$HPLUART$stop(void );
# 62 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
static result_t UART0M$ByteComm$txDone(void );
#line 54
static result_t UART0M$ByteComm$txByteReady(bool success);
#line 45
static result_t UART0M$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength);
# 38 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UART0M.nc"
bool UART0M$state;

static inline result_t UART0M$Control$init(void );







static inline result_t UART0M$Control$start(void );



static inline result_t UART0M$Control$stop(void );




static inline result_t UART0M$HPLUART$get(uint8_t data);









static inline result_t UART0M$HPLUART$putDone(void );
#line 90
static result_t UART0M$ByteComm$txByte(uint8_t data);
# 66 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
static result_t HPLUART0M$UART$get(uint8_t data);







static result_t HPLUART0M$UART$putDone(void );
# 49 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
static inline result_t HPLUART0M$Setbaud(uint32_t baud_rate);
#line 83
static inline result_t HPLUART0M$UART$init(void );





static inline result_t HPLUART0M$UART$stop(void );








void __vector_25(void )   __attribute((signal)) ;









void __vector_27(void )   __attribute((interrupt)) ;




static inline result_t HPLUART0M$UART$put(uint8_t data);
# 28 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
uint8_t LedsC$ledsOn;

enum LedsC$__nesc_unnamed4277 {
  LedsC$RED_BIT = 1, 
  LedsC$GREEN_BIT = 2, 
  LedsC$YELLOW_BIT = 4
};

static result_t LedsC$Leds$init(void );
#line 50
static result_t LedsC$Leds$redOn(void );
#line 79
static inline result_t LedsC$Leds$greenOn(void );
#line 108
static inline result_t LedsC$Leds$yellowOn(void );








static inline result_t LedsC$Leds$yellowOff(void );








static result_t LedsC$Leds$yellowToggle(void );
#line 145
static inline result_t LedsC$Leds$set(uint8_t ledsNum);
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t TimerM$PowerManagement$adjustPower(void );
# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static void TimerM$Clock$setInterval(uint8_t value);
#line 132
static uint8_t TimerM$Clock$readCounter(void );
#line 75
static result_t TimerM$Clock$setRate(char interval, char scale);
#line 100
static uint8_t TimerM$Clock$getInterval(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TimerM$Timer$fired(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x1016511a0);










uint32_t TimerM$mState;
uint8_t TimerM$setIntervalFlag;
uint8_t TimerM$mScale;
#line 41
uint8_t TimerM$mInterval;
int8_t TimerM$queue_head;
int8_t TimerM$queue_tail;
uint8_t TimerM$queue_size;
uint8_t TimerM$queue[NUM_TIMERS];
volatile uint16_t TimerM$interval_outstanding;





#line 48
struct TimerM$timer_s {
  uint8_t type;
  int32_t ticks;
  int32_t ticksLeft;
} TimerM$mTimerList[NUM_TIMERS];

enum TimerM$__nesc_unnamed4278 {
  TimerM$maxTimerInterval = 230
};
static inline result_t TimerM$StdControl$init(void );
#line 69
static inline result_t TimerM$StdControl$start(void );










static result_t TimerM$Timer$start(uint8_t id, char type, 
uint32_t interval);
#line 111
inline static void TimerM$adjustInterval(void );
#line 150
static result_t TimerM$Timer$stop(uint8_t id);
#line 164
static inline result_t TimerM$Timer$default$fired(uint8_t id);



static inline void TimerM$enqueue(uint8_t value);







static inline uint8_t TimerM$dequeue(void );









static inline void TimerM$signalOneTimer(void );





static inline void TimerM$HandleFire(void );
#line 237
static inline result_t TimerM$Clock$fire(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t HPLClock$Clock$fire(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
uint8_t HPLClock$set_flag;
uint8_t HPLClock$mscale;
#line 38
uint8_t HPLClock$nextScale;
#line 38
uint8_t HPLClock$minterval;
#line 70
static inline void HPLClock$Clock$setInterval(uint8_t value);









static inline uint8_t HPLClock$Clock$getInterval(void );
#line 108
static inline result_t HPLClock$Clock$setIntervalAndScale(uint8_t interval, uint8_t scale);
#line 136
static inline uint8_t HPLClock$Clock$readCounter(void );
#line 157
static inline result_t HPLClock$Clock$setRate(char interval, char scale);






void __vector_13(void )   __attribute((interrupt)) ;
# 48 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/HPLPowerManagementM.nc"
bool HPLPowerManagementM$disabled = 1;
#line 61
static inline uint8_t HPLPowerManagementM$getPowerLevel(void );
#line 114
static inline void HPLPowerManagementM$doAdjustment(void );
#line 134
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void );
# 3 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
static void DebugI2CM$HighLevelTwiInterface$begin(void );







static void DebugI2CM$HighLevelTwiInterface$send(uint8_t data);
#line 7
static void DebugI2CM$HighLevelTwiInterface$beginTransmission(uint8_t address);

static uint8_t DebugI2CM$HighLevelTwiInterface$endTransmission(void );
# 26 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static inline void DebugI2CM$full_event_handler(void );



static inline result_t DebugI2CM$StdControl$init(void );







static inline result_t DebugI2CM$StdControl$start(void );
#line 52
static void DebugI2CM$printBufferTask(void );
#line 74
static inline result_t DebugI2CM$flush(void );
# 7 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiInterface.nc"
static uint8_t HighLevelTwiM$LowLevelTwiInterface$twi_writeTo(uint8_t address, uint8_t *data, uint8_t length, uint8_t wait);
#line 3
static void HighLevelTwiM$LowLevelTwiInterface$twi_init(void );
# 16 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiM.nc"
static uint8_t HighLevelTwiM$rxBufferIndex = 0;
static uint8_t HighLevelTwiM$rxBufferLength = 0;

static uint8_t HighLevelTwiM$txAddress;
static uint8_t HighLevelTwiM$txBuffer[32];
static uint8_t HighLevelTwiM$txBufferIndex = 0;
static uint8_t HighLevelTwiM$txBufferLength = 0;

static uint8_t HighLevelTwiM$transmitting;

static inline void HighLevelTwiM$HighLevelTwiInterface$begin(void );
#line 54
static inline void HighLevelTwiM$HighLevelTwiInterface$beginTransmission(uint8_t address);










static uint8_t HighLevelTwiM$HighLevelTwiInterface$endTransmission(void );
#line 80
static inline void HighLevelTwiM$HighLevelTwiInterface$send(uint8_t data);
# 13 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiM.nc"
static volatile uint8_t LowLevelTwiM$twi_state;
static uint8_t LowLevelTwiM$twi_slarw;




static uint8_t LowLevelTwiM$twi_masterBuffer[32];
static volatile uint8_t LowLevelTwiM$twi_masterBufferIndex;
static uint8_t LowLevelTwiM$twi_masterBufferLength;








static volatile uint8_t LowLevelTwiM$twi_error;







static inline void LowLevelTwiM$LowLevelTwiInterface$twi_init(void );
#line 134
static inline uint8_t LowLevelTwiM$LowLevelTwiInterface$twi_writeTo(uint8_t address, uint8_t *data, uint8_t length, uint8_t wait);
#line 188
static inline void LowLevelTwiM$twi_reply(uint8_t ack);
#line 204
static void LowLevelTwiM$twi_stop(void );
#line 225
static inline void LowLevelTwiM$twi_releaseBus(void );








void __vector_39(void )   __attribute((signal)) ;
# 64 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_SET_GREEN_LED_PIN()
#line 64
{
#line 64
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 1;
}

#line 65
static __inline void TOSH_SET_YELLOW_LED_PIN()
#line 65
{
#line 65
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 0;
}

#line 63
static __inline void TOSH_SET_RED_LED_PIN()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 2;
}

#line 79
static __inline void TOSH_SET_FLASH_SELECT_PIN()
#line 79
{
#line 79
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 3;
}

#line 80
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT()
#line 80
{
#line 80
  * (volatile uint8_t *)(0x0A + 0x20) |= 1 << 5;
}

#line 81
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT()
#line 81
{
#line 81
  * (volatile uint8_t *)(0x0A + 0x20) |= 1 << 3;
}

#line 79
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT()
#line 79
{
#line 79
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 3;
}

#line 67
static __inline void TOSH_CLR_SERIAL_ID_PIN()
#line 67
{
#line 67
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 4);
}

#line 67
static __inline void TOSH_MAKE_SERIAL_ID_INPUT()
#line 67
{
#line 67
  * (volatile uint8_t *)(0X01 + 0x20) &= ~(1 << 4);
}

#line 98
static __inline void TOSH_MAKE_PW0_OUTPUT()
#line 98
{
#line 98
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 0;
}

#line 99
static __inline void TOSH_MAKE_PW1_OUTPUT()
#line 99
{
#line 99
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 1;
}

#line 100
static __inline void TOSH_MAKE_PW2_OUTPUT()
#line 100
{
#line 100
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 2;
}

#line 101
static __inline void TOSH_MAKE_PW3_OUTPUT()
#line 101
{
#line 101
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 3;
}

#line 102
static __inline void TOSH_MAKE_PW4_OUTPUT()
#line 102
{
#line 102
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 4;
}

#line 103
static __inline void TOSH_MAKE_PW5_OUTPUT()
#line 103
{
#line 103
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 5;
}

#line 104
static __inline void TOSH_MAKE_PW6_OUTPUT()
#line 104
{
#line 104
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 6;
}

#line 105
static __inline void TOSH_MAKE_PW7_OUTPUT()
#line 105
{
#line 105
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 7;
}

#line 64
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT()
#line 64
{
#line 64
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 1;
}

#line 65
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT()
#line 65
{
#line 65
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 0;
}

#line 63
static __inline void TOSH_MAKE_RED_LED_OUTPUT()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 2;
}

#line 134
static inline void TOSH_SET_PIN_DIRECTIONS(void )
{







  TOSH_MAKE_RED_LED_OUTPUT();
  TOSH_MAKE_YELLOW_LED_OUTPUT();
  TOSH_MAKE_GREEN_LED_OUTPUT();


  TOSH_MAKE_PW7_OUTPUT();
  TOSH_MAKE_PW6_OUTPUT();
  TOSH_MAKE_PW5_OUTPUT();
  TOSH_MAKE_PW4_OUTPUT();
  TOSH_MAKE_PW3_OUTPUT();
  TOSH_MAKE_PW2_OUTPUT();
  TOSH_MAKE_PW1_OUTPUT();
  TOSH_MAKE_PW0_OUTPUT();
#line 173
  TOSH_MAKE_SERIAL_ID_INPUT();
  TOSH_CLR_SERIAL_ID_PIN();

  TOSH_MAKE_FLASH_SELECT_OUTPUT();
  TOSH_MAKE_FLASH_OUT_OUTPUT();
  TOSH_MAKE_FLASH_CLK_OUTPUT();
  TOSH_SET_FLASH_SELECT_PIN();

  TOSH_SET_RED_LED_PIN();
  TOSH_SET_YELLOW_LED_PIN();
  TOSH_SET_GREEN_LED_PIN();
}

# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static inline result_t HPLInit$init(void )
#line 37
{
  TOSH_SET_PIN_DIRECTIONS();
  return SUCCESS;
}

# 27 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
inline static result_t RealMain$hardwareInit(void ){
#line 27
  unsigned char __nesc_result;
#line 27

#line 27
  __nesc_result = HPLInit$init();
#line 27

#line 27
  return __nesc_result;
#line 27
}
#line 27
# 55 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$finalise(void )
#line 55
{


  return SUCCESS;
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$finalise(void ){
#line 53
  unsigned char __nesc_result;
#line 53

#line 53
  __nesc_result = HPLPotC$Pot$finalise();
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 46 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$increase(void )
#line 46
{





  return SUCCESS;
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$increase(void ){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = HPLPotC$Pot$increase();
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$decrease(void )
#line 37
{





  return SUCCESS;
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$decrease(void ){
#line 38
  unsigned char __nesc_result;
#line 38

#line 38
  __nesc_result = HPLPotC$Pot$decrease();
#line 38

#line 38
  return __nesc_result;
#line 38
}
#line 38
# 72 "/Users/wbennett/opt/MoteWorks/tos/system/PotM.nc"
static inline void PotM$setPot(uint8_t value)
#line 72
{
  uint8_t i;

#line 74
  for (i = 0; i < 151; i++) 
    PotM$HPLPot$decrease();

  for (i = 0; i < value; i++) 
    PotM$HPLPot$increase();

  PotM$HPLPot$finalise();

  PotM$potSetting = value;
}

static inline result_t PotM$Pot$init(uint8_t initialSetting)
#line 85
{
  PotM$setPot(initialSetting);
  return SUCCESS;
}

# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
inline static result_t RealMain$Pot$init(uint8_t initialSetting){
#line 57
  unsigned char __nesc_result;
#line 57

#line 57
  __nesc_result = PotM$Pot$init(initialSetting);
#line 57

#line 57
  return __nesc_result;
#line 57
}
#line 57
# 59 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
static inline void TOSH_sched_init(void )
{
  int i;

#line 62
  TOSH_sched_free = 0;
  TOSH_sched_full = 0;
  for (i = 0; i < TOSH_MAX_TASKS; i++) 
    TOSH_queue[i].tp = (void *)0;
}

# 158 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline result_t rcombine(result_t r1, result_t r2)



{
  return r1 == FAIL ? FAIL : r2;
}

# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t TestGSMDriverM$Leds$init(void ){
#line 35
  unsigned char __nesc_result;
#line 35

#line 35
  __nesc_result = LedsC$Leds$init();
#line 35

#line 35
  return __nesc_result;
#line 35
}
#line 35
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
inline static result_t TestGSMDriverM$Timer$start(char type, uint32_t interval){
#line 37
  unsigned char __nesc_result;
#line 37

#line 37
  __nesc_result = TimerM$Timer$start(0U, type, interval);
#line 37

#line 37
  return __nesc_result;
#line 37
}
#line 37
# 64 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_GREEN_LED_PIN()
#line 64
{
#line 64
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 1);
}

# 79 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$greenOn(void )
#line 79
{
  {
  }
#line 80
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 81
    {
      TOSH_CLR_GREEN_LED_PIN();
      LedsC$ledsOn |= LedsC$GREEN_BIT;
    }
#line 84
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 68 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t TestGSMDriverM$Leds$greenOn(void ){
#line 68
  unsigned char __nesc_result;
#line 68

#line 68
  __nesc_result = LedsC$Leds$greenOn();
#line 68

#line 68
  return __nesc_result;
#line 68
}
#line 68
# 66 "TestGSMDriverM.nc"
static inline result_t TestGSMDriverM$GsmControl$initDone(void )
#line 66
{
  TestGSMDriverM$Leds$greenOn();
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 68
    TestGSMDriverM$state = TestGSMDriverM$STARTING;
#line 68
    __nesc_atomic_end(__nesc_atomic); }
  TestGSMDriverM$Timer$start(TIMER_ONE_SHOT, 100);
  return SUCCESS;
}

# 24 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/SplitControlStatus.nc"
inline static result_t GSMDriverM$SplitControlStatus$initDone(void ){
#line 24
  unsigned char __nesc_result;
#line 24

#line 24
  __nesc_result = TestGSMDriverM$GsmControl$initDone();
#line 24

#line 24
  return __nesc_result;
#line 24
}
#line 24
# 224 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline void GSMDriverM$initDoneTask(void )
{

  GSMDriverM$SplitControlStatus$initDone();
}

# 40 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UART0M.nc"
static inline result_t UART0M$Control$init(void )
#line 40
{
  {
  }
#line 41
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 42
    {
      UART0M$state = FALSE;
    }
#line 44
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t UARTPacket$ByteControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = UART0M$Control$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 118 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static __inline void UARTPacket$initState(void )
#line 118
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 119
    {
      UARTPacket$gpsRecvState = UARTPacket$NO_GSM_START_BYTE;
      UARTPacket$state = UARTPacket$IDLE;

      UARTPacket$txCount = UARTPacket$rxCount = 0;
      UARTPacket$curBufferRecvIndex = 0;
      UARTPacket$recPtr = (uint8_t *)&UARTPacket$bufferRecv[UARTPacket$curBufferRecvIndex];
      UARTPacket$recvQueue.front = 0;
      UARTPacket$recvQueue.count = 0;
      UARTPacket$sendPtr = (uint8_t *)&UARTPacket$bufferSend;
    }
#line 129
    __nesc_atomic_end(__nesc_atomic); }
}





static inline result_t UARTPacket$Control$init(void )
#line 136
{
  UARTPacket$initState();
  return UARTPacket$ByteControl$init();
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t GsmUartHandlerM$UARTControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = UARTPacket$Control$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t GsmUartHandlerM$Leds$init(void ){
#line 35
  unsigned char __nesc_result;
#line 35

#line 35
  __nesc_result = LedsC$Leds$init();
#line 35

#line 35
  return __nesc_result;
#line 35
}
#line 35
# 578 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static inline void GsmUartHandlerM$initTask(void )
{

  GsmUartHandlerM$Leds$init();
  GsmUartHandlerM$UARTControl$init();
}

#line 571
static inline result_t GsmUartHandlerM$StdControl$init(void )
{

  TOS_post(GsmUartHandlerM$initTask);
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t GSMDriverM$UARTControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = GsmUartHandlerM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 154 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static __inline void GSMDriverM$initPins(void )
{

  TOSH_MAKE_PW0_OUTPUT();
  TOSH_MAKE_PW1_OUTPUT();
}

#line 213
static inline result_t GSMDriverM$SplitControlStatus$init(void )
{

  GSMDriverM$initPins();
  GSMDriverM$UARTControl$init();
  TOS_post(GSMDriverM$initDoneTask);
  return SUCCESS;
}

# 18 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/SplitControlStatus.nc"
inline static result_t TestGSMDriverM$GsmControl$init(void ){
#line 18
  unsigned char __nesc_result;
#line 18

#line 18
  __nesc_result = GSMDriverM$SplitControlStatus$init();
#line 18

#line 18
  return __nesc_result;
#line 18
}
#line 18
# 108 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline result_t HPLClock$Clock$setIntervalAndScale(uint8_t interval, uint8_t scale)
#line 108
{



  if (scale > 7) {
#line 112
    return FAIL;
    }
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 114
    {
      * (volatile uint8_t *)0x70 &= ~(1 << 0);
      * (volatile uint8_t *)0x70 &= ~(1 << 1);




      * (volatile uint8_t *)0xB6 |= 1 << 5;

      HPLClock$mscale = scale;
      HPLClock$minterval = interval;

      * (volatile uint8_t *)0xB0 = 2 << 0;
      * (volatile uint8_t *)0xB1 = scale;

      * (volatile uint8_t *)0xB2 = 0;
      * (volatile uint8_t *)0xB3 = interval;
      * (volatile uint8_t *)0x70 |= 1 << 1;
    }
#line 132
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

#line 157
static inline result_t HPLClock$Clock$setRate(char interval, char scale)
#line 157
{
  return HPLClock$Clock$setIntervalAndScale(interval, scale);
}

# 75 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static result_t TimerM$Clock$setRate(char interval, char scale){
#line 75
  unsigned char __nesc_result;
#line 75

#line 75
  __nesc_result = HPLClock$Clock$setRate(interval, scale);
#line 75

#line 75
  return __nesc_result;
#line 75
}
#line 75
# 46 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static inline avrtime_t get_time_millis()
#line 46
{



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 50
    {
      unsigned long long __nesc_temp = 
#line 50
      sys_time;

      {
#line 50
        __nesc_atomic_end(__nesc_atomic); 
#line 50
        return __nesc_temp;
      }
    }
#line 52
    __nesc_atomic_end(__nesc_atomic); }
}

#line 70
static inline void reset_start_time()
#line 70
{



  start_time = get_time_millis();
}

#line 20
static inline void set_time_millis(avrtime_t set_time)
#line 20
{



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 24
    sys_time = set_time;
#line 24
    __nesc_atomic_end(__nesc_atomic); }
}



static inline void init_avrtime()
#line 29
{
  if (isInit == 0) {
      avrtime_t t = 132489216000ULL;

#line 32
      isInit = 1;
      set_time_millis(t);
      reset_start_time();
    }
}

# 57 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline result_t TimerM$StdControl$init(void )
#line 57
{

  init_avrtime();
  TimerM$mState = 0;
  TimerM$setIntervalFlag = 0;
  TimerM$queue_head = TimerM$queue_tail = -1;
  TimerM$queue_size = 0;
  TimerM$mScale = 3;
  TimerM$mInterval = TimerM$maxTimerInterval;
  return TimerM$Clock$setRate(TimerM$mInterval, TimerM$mScale);
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t TestGSMDriverM$TimerControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = TimerM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 46 "TestGSMDriverM.nc"
static inline result_t TestGSMDriverM$StdControl$init(void )
#line 46
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 47
    TestGSMDriverM$state = TestGSMDriverM$NONE;
#line 47
    __nesc_atomic_end(__nesc_atomic); }
  TestGSMDriverM$TimerControl$init();
  TestGSMDriverM$GsmControl$init();
  TestGSMDriverM$Leds$init();
  return SUCCESS;
}

# 74 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static inline result_t DebugI2CM$flush(void )
#line 74
{
  TOS_post(DebugI2CM$printBufferTask);
  return SUCCESS;
}

#line 26
static inline void DebugI2CM$full_event_handler(void )
#line 26
{
  DebugI2CM$flush();
}

# 56 "/Users/wbennett/opt/MoteWorks/lib/debug_constants.h"
static inline void twi_attach_full_event(void (*handler)(void ))
#line 56
{
  full_event = handler;
}

# 30 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static inline result_t DebugI2CM$StdControl$init(void )
{

  TOSH_MAKE_PW2_OUTPUT();
  twi_attach_full_event(DebugI2CM$full_event_handler);
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RealMain$StdControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = DebugI2CM$StdControl$init();
#line 41
  __nesc_result = rcombine(__nesc_result, TestGSMDriverM$StdControl$init());
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 54 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiM.nc"
static inline void HighLevelTwiM$HighLevelTwiInterface$beginTransmission(uint8_t address)
{

  HighLevelTwiM$transmitting = 1;

  HighLevelTwiM$txAddress = address;

  HighLevelTwiM$txBufferIndex = 0;
  HighLevelTwiM$txBufferLength = 0;
}

# 7 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
inline static void DebugI2CM$HighLevelTwiInterface$beginTransmission(uint8_t address){
#line 7
  HighLevelTwiM$HighLevelTwiInterface$beginTransmission(address);
#line 7
}
#line 7
# 60 "/Users/wbennett/opt/MoteWorks/lib/debug_constants.h"
static inline int8_t twi_buffer_empty(void )
#line 60
{
  return dbg_read_pointer == dbg_write_pointer && 
  dbg_data_size == 0;
}

# 9 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
inline static uint8_t DebugI2CM$HighLevelTwiInterface$endTransmission(void ){
#line 9
  unsigned char __nesc_result;
#line 9

#line 9
  __nesc_result = HighLevelTwiM$HighLevelTwiInterface$endTransmission();
#line 9

#line 9
  return __nesc_result;
#line 9
}
#line 9
# 134 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiM.nc"
static inline uint8_t LowLevelTwiM$LowLevelTwiInterface$twi_writeTo(uint8_t address, uint8_t *data, uint8_t length, uint8_t wait)
{
  uint8_t i;


  if (32 < length) {
      return 1;
    }


  while (0 != LowLevelTwiM$twi_state) {
      continue;
    }
  LowLevelTwiM$twi_state = 2;

  LowLevelTwiM$twi_error = 0xFF;


  LowLevelTwiM$twi_masterBufferIndex = 0;
  LowLevelTwiM$twi_masterBufferLength = length;


  for (i = 0; i < length; ++i) {
      LowLevelTwiM$twi_masterBuffer[i] = data[i];
    }


  LowLevelTwiM$twi_slarw = 0;
  LowLevelTwiM$twi_slarw |= address << 1;


  * (volatile uint8_t *)0xBC = ((((1 << 2) | (1 << 0)) | (1 << 6)) | (1 << 7)) | (1 << 5);


  while (wait && 2 == LowLevelTwiM$twi_state) {
      continue;
    }

  if (LowLevelTwiM$twi_error == 0xFF) {
    return 0;
    }
  else {
#line 174
    if (LowLevelTwiM$twi_error == 0x20) {
      return 2;
      }
    else {
#line 176
      if (LowLevelTwiM$twi_error == 0x30) {
        return 3;
        }
      else {
#line 179
        return 4;
        }
      }
    }
}

# 7 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiInterface.nc"
inline static uint8_t HighLevelTwiM$LowLevelTwiInterface$twi_writeTo(uint8_t address, uint8_t *data, uint8_t length, uint8_t wait){
#line 7
  unsigned char __nesc_result;
#line 7

#line 7
  __nesc_result = LowLevelTwiM$LowLevelTwiInterface$twi_writeTo(address, data, length, wait);
#line 7

#line 7
  return __nesc_result;
#line 7
}
#line 7
# 80 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiM.nc"
static inline void HighLevelTwiM$HighLevelTwiInterface$send(uint8_t data)
{
  if (HighLevelTwiM$transmitting) {


      if (HighLevelTwiM$txBufferLength >= 32) {
          return;
        }

      HighLevelTwiM$txBuffer[HighLevelTwiM$txBufferIndex] = data;
      ++HighLevelTwiM$txBufferIndex;

      HighLevelTwiM$txBufferLength = HighLevelTwiM$txBufferIndex;
    }
}

# 11 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
inline static void DebugI2CM$HighLevelTwiInterface$send(uint8_t data){
#line 11
  HighLevelTwiM$HighLevelTwiInterface$send(data);
#line 11
}
#line 11
# 42 "/Users/wbennett/opt/MoteWorks/lib/debug_constants.h"
static inline char twi_pull_char(void )
#line 42
{
  char rval;

#line 44
  if (++dbg_read_pointer >= 256) {
#line 44
    dbg_read_pointer = 0;
    }
#line 45
  rval = dbg_buffer[dbg_read_pointer];
  dbg_buffer[dbg_read_pointer] = 0x20;
  dbg_data_size--;
  return rval;
}

# 61 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/HPLPowerManagementM.nc"
static inline uint8_t HPLPowerManagementM$getPowerLevel(void )
#line 61
{

  if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0x6E & (1 << 1)) {

      return 0;
    }
  else {
#line 67
    if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0x6F & (1 << 5)) {

        return 0;
      }
    else {
#line 71
      if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)(0x2C + 0x20) & (1 << 7)) {

          return 0;
        }
      else {
#line 90
        if (* (volatile uint8_t *)0XC9 & ((((1 << 7) | (1 << 6)) | (1 << 4)) | (1 << 3))) {

            return 0;
          }
        else {
#line 94
          if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0x7A & (1 << 7)) {

              return 1 << 1;
            }
          else {
#line 98
            if (* (volatile uint8_t *)0x70 & ((1 << 1) | (1 << 0))) {

                uint8_t diff;

#line 101
                diff = * (volatile uint8_t *)0xB3 - * (volatile uint8_t *)0xB2;
                if (diff < 16) {
#line 102
                  return ((1 << 3) | (1 << 2)) | (1 << 1);
                  }
                else {
#line 103
                  return (1 << 2) | (1 << 1);
                  }
              }
            else 
#line 105
              {

                return 1 << 2;
              }
            }
          }
        }
      }
    }
}

#line 114
static inline void HPLPowerManagementM$doAdjustment(void )
#line 114
{
  uint8_t foo;
#line 115
  uint8_t mcu;

#line 116
  foo = HPLPowerManagementM$getPowerLevel();
  mcu = * (volatile uint8_t *)(0x33 + 0x20);
  mcu &= 0xf1;
  if (foo == (((1 << 3) | (1 << 2)) | (1 << 1)) || foo == ((1 << 2) | (1 << 1))) {
      mcu |= 0;
      while ((* (volatile uint8_t *)0xB6 & 0x1f) != 0) {
           __asm volatile ("nop");}

      mcu &= 0xf1;
    }
  mcu |= foo;
  * (volatile uint8_t *)(0x33 + 0x20) = mcu;
  * (volatile uint8_t *)(0x33 + 0x20) |= 1 << 0;
}

# 69 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline result_t TimerM$StdControl$start(void )
#line 69
{
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t TestGSMDriverM$TimerControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = TimerM$StdControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 54 "TestGSMDriverM.nc"
static inline result_t TestGSMDriverM$StdControl$start(void )
#line 54
{
  TestGSMDriverM$TimerControl$start();
  return SUCCESS;
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiM.nc"
static inline void LowLevelTwiM$LowLevelTwiInterface$twi_init(void )
{

  LowLevelTwiM$twi_state = 0;



  * (volatile uint8_t *)(0x0B + 0x20) |= 1 << 0;
  * (volatile uint8_t *)(0x0B + 0x20) |= 1 << 1;


  * (volatile uint8_t *)0xB9 &= ~(1 << 0);
  * (volatile uint8_t *)0xB9 &= ~(1 << 1);
  * (volatile uint8_t *)0xB8 = (16000000L / 100000L - 16) / 2;







  * (volatile uint8_t *)0xBC = ((1 << 2) | (1 << 0)) | (1 << 6);
}

# 3 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiInterface.nc"
inline static void HighLevelTwiM$LowLevelTwiInterface$twi_init(void ){
#line 3
  LowLevelTwiM$LowLevelTwiInterface$twi_init();
#line 3
}
#line 3
# 26 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiM.nc"
static inline void HighLevelTwiM$HighLevelTwiInterface$begin(void )
{
  HighLevelTwiM$rxBufferIndex = 0;
  HighLevelTwiM$rxBufferLength = 0;

  HighLevelTwiM$txBufferIndex = 0;
  HighLevelTwiM$txBufferLength = 0;

  HighLevelTwiM$LowLevelTwiInterface$twi_init();
}

# 3 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
inline static void DebugI2CM$HighLevelTwiInterface$begin(void ){
#line 3
  HighLevelTwiM$HighLevelTwiInterface$begin();
#line 3
}
#line 3
# 39 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_uwait(int u_sec)
#line 39
{
  while (u_sec > 0) {
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
      u_sec--;
    }
}

#line 100
static __inline void TOSH_SET_PW2_PIN()
#line 100
{
#line 100
  * (volatile uint8_t *)(0x08 + 0x20) |= 1 << 2;
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static inline result_t DebugI2CM$StdControl$start(void )
{
  TOSH_SET_PW2_PIN();
  TOSH_uwait(10);
  DebugI2CM$HighLevelTwiInterface$begin();
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RealMain$StdControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = DebugI2CM$StdControl$start();
#line 48
  __nesc_result = rcombine(__nesc_result, TestGSMDriverM$StdControl$start());
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 166 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
static __inline void __nesc_enable_interrupt()
#line 166
{
   __asm volatile ("sei");}

#line 151
__inline  void __nesc_atomic_end(__nesc_atomic_t oldSreg)
{
  * (volatile uint8_t *)(0x3F + 0x20) = oldSreg;
}

#line 129
static inline void TOSH_wait()
{
   __asm volatile ("nop");
   __asm volatile ("nop");}

#line 158
static __inline void __nesc_atomic_sleep()
{

   __asm volatile ("sei");
   __asm volatile ("sleep");
  TOSH_wait();
}

#line 144
__inline  __nesc_atomic_t __nesc_atomic_start(void )
{
  __nesc_atomic_t result = * (volatile uint8_t *)(0x3F + 0x20);

#line 147
   __asm volatile ("cli");
  return result;
}

# 116 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
static inline bool TOSH_run_next_task()
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t old_full;
  void (*func)(void );

  fInterruptFlags = __nesc_atomic_start();
  old_full = TOSH_sched_full;
  func = TOSH_queue[old_full].tp;
  if (func == (void *)0) 
    {
      __nesc_atomic_sleep();
      return 0;
    }

  TOSH_queue[old_full].tp = (void *)0;
  TOSH_sched_full = (old_full + 1) & TOSH_TASK_BITMASK;
  __nesc_atomic_end(fInterruptFlags);
  func();

  return 1;
}

static inline void TOSH_run_task()
#line 139
{
  for (; ; ) 
    TOSH_run_next_task();
}

# 95 "TestGSMDriverM.nc"
static inline result_t TestGSMDriverM$GSM_Modem$cellMonitorReportReady(gsm_error_t result, gsm_cellmon_data_t *data)
#line 95
{
  if (result == NO_ERROR) {





      TestGSMDriverM$Timer$start(TIMER_ONE_SHOT, 500);
    }
  else {
    }
  return SUCCESS;
}

# 40 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSM_ModemI.nc"
inline static result_t GSMDriverM$GSM_Modem$cellMonitorReportReady(gsm_error_t result, gsm_cellmon_data_t *data){
#line 40
  unsigned char __nesc_result;
#line 40

#line 40
  __nesc_result = TestGSMDriverM$GSM_Modem$cellMonitorReportReady(result, data);
#line 40

#line 40
  return __nesc_result;
#line 40
}
#line 40
# 900 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline void GSMDriverM$cellMonitorReportReadyTask(void )
{

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 903
    GSMDriverM$currentCmdState = GSMDriverM$NONE;
#line 903
    __nesc_atomic_end(__nesc_atomic); }
  GSMDriverM$GSM_Modem$cellMonitorReportReady(NO_ERROR, &GSMDriverM$currentCellMonitorReportData);
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
inline static result_t GSMDriverM$CommandTimer$stop(void ){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = TimerM$Timer$stop(2U);
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 751 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline void GSMDriverM$GsmUartHandler$CellMonitorReportReceived(gsm_cellmon_data_t *ptr, uint16_t dataLength)
{
  nmemcpy(&GSMDriverM$currentCellMonitorReportData, ptr, sizeof GSMDriverM$currentCellMonitorReportData);
  GSMDriverM$CommandTimer$stop();
  /* atomic removed: atomic calls only */
#line 755
  GSMDriverM$commandRetryCount = 0;
  TOS_post(GSMDriverM$cellMonitorReportReadyTask);
  return;
}

# 50 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
inline static void GsmUartHandlerM$GsmUartHandler$CellMonitorReportReceived(gsm_cellmon_data_t *ptr, uint16_t dataLength){
#line 50
  GSMDriverM$GsmUartHandler$CellMonitorReportReceived(ptr, dataLength);
#line 50
}
#line 50
# 323 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static __inline bool GsmUartHandlerM$isWhiteSpace(char c)
#line 323
{
  bool b = ((c == ' ' || c == '\t') || c == '\r') || c == '\n';

#line 325
  return b;
}

static __inline char *GsmUartHandlerM$trim(char *s)
#line 328
{
  uint8_t start = 0;
  uint8_t end = strlen(s) - 1;
  uint8_t l;

#line 332
  while (start <= end && GsmUartHandlerM$isWhiteSpace(s[start])) {
      start++;
    }
  if (start <= end) {
      while (end && GsmUartHandlerM$isWhiteSpace(s[end])) {
          end--;
        }
    }
  l = end - start + 1;
  if (l <= 0) {
      return "";
    }
  strncpy(GsmUartHandlerM$trimBuf, s + start, l);
  GsmUartHandlerM$trimBuf[l] = '\0';
  return GsmUartHandlerM$trimBuf;
}

static __inline char *GsmUartHandlerM$consolidate(char *s)
#line 349
{
  char *CcPtr;
  char *NcPtr;
  char *endPtr;
  uint8_t i;
#line 353
  uint8_t k;

#line 354
  strcpy(GsmUartHandlerM$consBuf1, s);
  CcPtr = strstr(s, "Cc:");
  if (CcPtr != (void *)0) {
      NcPtr = strstr(CcPtr, "Nc:");
      if (NcPtr != (void *)0) {
          CcPtr += strlen("Cc:");
          GsmUartHandlerM$consBuf1[0] = 'C';
          strncpy(GsmUartHandlerM$consBuf1 + 1, CcPtr, NcPtr - CcPtr);
          GsmUartHandlerM$consBuf1[NcPtr - CcPtr + 1] = '\0';
          strcpy(GsmUartHandlerM$consBuf1, GsmUartHandlerM$trim(GsmUartHandlerM$consBuf1));
          strcat(GsmUartHandlerM$consBuf1, "N");
          NcPtr += strlen("Nc:");
          endPtr = strstr(NcPtr, "BSIC:");
          if (endPtr != (void *)0) {
              i = strlen(GsmUartHandlerM$consBuf1);
              strncat(GsmUartHandlerM$consBuf1, NcPtr, endPtr - NcPtr);
              GsmUartHandlerM$consBuf1[i + endPtr - NcPtr] = '\0';
            }
          else {
              strcat(GsmUartHandlerM$consBuf1, NcPtr);
            }
        }
    }
  k = 0;
  for (i = 0; i <= strlen(GsmUartHandlerM$consBuf1); i++) {
      if (!GsmUartHandlerM$isWhiteSpace(GsmUartHandlerM$consBuf1[i])) {
          GsmUartHandlerM$consBuf2[k] = GsmUartHandlerM$consBuf1[i];
          k++;
        }
    }
  GsmUartHandlerM$consBuf2[9] = '\0';
  return GsmUartHandlerM$consBuf2;
}






static __inline bool GsmUartHandlerM$checkForCellMonitorReceived(uint8_t index)
{

  char *buf = GsmUartHandlerM$uartBuffer[index];

  if (strstr(buf, "#MONI:") == (void *)0) {
      return FALSE;
    }
  else {

      if (GsmUartHandlerM$gsmCellMonParamVal == 0) 
        {
          char *begPtr;
          char *endPtr;
          char tempString[20];
          char *tempPtr;
          uint16_t bsic;
          uint16_t rxQual;
          uint16_t lac;
          uint16_t id;
          uint16_t arfcn;
          int16_t dBm;
          uint16_t timadv;


          begPtr = strchr(buf, ':');
          begPtr++;

          endPtr = strstr(begPtr, "BSIC:");
          if (endPtr == (void *)0) {
              return FALSE;
            }
          strncpy(tempString, begPtr, endPtr - begPtr);
          tempString[endPtr - begPtr] = '\0';

          tempPtr = GsmUartHandlerM$trim(tempString);

          tempPtr = GsmUartHandlerM$consolidate(tempPtr);

          strcpy(GsmUartHandlerM$cellMonData.towers[0].netname, tempPtr);



          begPtr = endPtr + strlen("BSIC:");
          endPtr = strstr(begPtr, "RxQual:");
          if (endPtr == (void *)0) {
              return FALSE;
            }
          strncpy(tempString, begPtr, endPtr - begPtr);
          tempString[endPtr - begPtr] = '\0';

          sscanf(tempString, "%x", &bsic);
          GsmUartHandlerM$cellMonData.towers[0].bsic = (uint8_t )bsic;



          begPtr = endPtr + strlen("RxQual:");
          endPtr = strstr(begPtr, "LAC:");
          if (endPtr == (void *)0) {
              return FALSE;
            }
          strncpy(tempString, begPtr, endPtr - begPtr);
          tempString[endPtr - begPtr] = '\0';

          sscanf(tempString, "%d", &rxQual);





          begPtr = endPtr + strlen("LAC:");
          endPtr = strstr(begPtr, "Id:");
          if (endPtr == (void *)0) {
              return FALSE;
            }
          strncpy(tempString, begPtr, endPtr - begPtr);
          tempString[endPtr - begPtr] = '\0';

          sscanf(tempString, "%x", &lac);
          GsmUartHandlerM$cellMonData.towers[0].lac = lac;



          begPtr = endPtr + strlen("Id:");
          endPtr = strstr(begPtr, "ARFCN:");
          if (endPtr == (void *)0) {
              return FALSE;
            }
          strncpy(tempString, begPtr, endPtr - begPtr);
          tempString[endPtr - begPtr] = '\0';

          sscanf(tempString, "%x", &id);
          GsmUartHandlerM$cellMonData.towers[0].id = id;



          begPtr = endPtr + strlen("ARFCN:");
          endPtr = strstr(begPtr, "PWR:");
          if (endPtr == (void *)0) {
              return FALSE;
            }
          strncpy(tempString, begPtr, endPtr - begPtr);
          tempString[endPtr - begPtr] = '\0';

          sscanf(tempString, "%d", &arfcn);
          GsmUartHandlerM$cellMonData.towers[0].arfcn = arfcn;



          begPtr = endPtr + strlen("PWR:");
          endPtr = strstr(begPtr, "dbm");
          if (endPtr == (void *)0) {
              return FALSE;
            }
          strncpy(tempString, begPtr, endPtr - begPtr);
          tempString[endPtr - begPtr] = '\0';

          sscanf(tempString, "%d", &dBm);






          endPtr = strstr(begPtr, "TA:");
          if (endPtr == (void *)0) {
              return FALSE;
            }
          begPtr = endPtr + strlen("TA:");
          endPtr = begPtr + strlen(begPtr);

          strncpy(tempString, begPtr, endPtr - begPtr);
          tempString[endPtr - begPtr] = '\0';

          sscanf(tempString, "%d", &timadv);







          return TRUE;
        }
      else {
          return FALSE;
        }
    }
}

# 109 "TestGSMDriverM.nc"
static inline result_t TestGSMDriverM$GSM_Modem$signalQualityReady(gsm_error_t result, gsm_signal_quality_data_t *data)
#line 109
{
  if (result == NO_ERROR) {

      TestGSMDriverM$Timer$start(TIMER_ONE_SHOT, 500);
    }
  else {
    }
  return SUCCESS;
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSM_ModemI.nc"
inline static result_t GSMDriverM$GSM_Modem$signalQualityReady(gsm_error_t result, gsm_signal_quality_data_t *data){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = TestGSMDriverM$GSM_Modem$signalQualityReady(result, data);
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 918 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline void GSMDriverM$signalQualityDataReadyTask(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 920
    GSMDriverM$currentCmdState = GSMDriverM$NONE;
#line 920
    __nesc_atomic_end(__nesc_atomic); }
  GSMDriverM$GSM_Modem$signalQualityReady(NO_ERROR, &GSMDriverM$currentSignalQualityData);
}

#line 760
static inline void GSMDriverM$GsmUartHandler$SignalQualityReceived(gsm_signal_quality_data_t *ptr, uint8_t dataLength)
{
  nmemcpy(&GSMDriverM$currentSignalQualityData, ptr, sizeof GSMDriverM$currentSignalQualityData);
  GSMDriverM$CommandTimer$stop();
  /* atomic removed: atomic calls only */
#line 764
  GSMDriverM$commandRetryCount = 0;
  TOS_post(GSMDriverM$signalQualityDataReadyTask);
  return;
}

# 66 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
inline static void GsmUartHandlerM$GsmUartHandler$SignalQualityReceived(gsm_signal_quality_data_t *ptr, uint8_t dataLength){
#line 66
  GSMDriverM$GsmUartHandler$SignalQualityReceived(ptr, dataLength);
#line 66
}
#line 66
# 277 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static __inline bool GsmUartHandlerM$checkForSignalQualityReceived(uint8_t index)
{

  char *buf = GsmUartHandlerM$uartBuffer[index];



  if (strstr(buf, "+CSQ:") == (void *)0) {

      return FALSE;
    }
  else {
      char *dataSegment;
      char *prog;
      char *tok;

      dataSegment = strtok_r(buf, ":", &prog);

      tok = strtok_r((void *)0, ",", &prog);
      GsmUartHandlerM$sigQualData.rssi = atoi(tok);

      tok = strtok_r((void *)0, "\r", &prog);
      GsmUartHandlerM$sigQualData.bit_err_rate = atoi(tok);

      return TRUE;
    }
}

# 43 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t TestGSMDriverM$Leds$redOn(void ){
#line 43
  unsigned char __nesc_result;
#line 43

#line 43
  __nesc_result = LedsC$Leds$redOn();
#line 43

#line 43
  return __nesc_result;
#line 43
}
#line 43
# 73 "TestGSMDriverM.nc"
static inline result_t TestGSMDriverM$GsmControl$startDone(gsm_error_t result)
#line 73
{
  TestGSMDriverM$Leds$redOn();







  return SUCCESS;
}

# 39 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/SplitControlStatus.nc"
inline static result_t GSMDriverM$SplitControlStatus$startDone(gsm_error_t result){
#line 39
  unsigned char __nesc_result;
#line 39

#line 39
  __nesc_result = TestGSMDriverM$GsmControl$startDone(result);
#line 39

#line 39
  return __nesc_result;
#line 39
}
#line 39
# 258 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline void GSMDriverM$startDoneTask(void )
{
  GSMDriverM$SplitControlStatus$startDone(NO_ERROR);
}

# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
inline static result_t GSMDriverM$Timer$start(char type, uint32_t interval){
#line 37
  unsigned char __nesc_result;
#line 37

#line 37
  __nesc_result = TimerM$Timer$start(1U, type, interval);
#line 37

#line 37
  return __nesc_result;
#line 37
}
#line 37
# 855 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline void GSMDriverM$NetworkRegistrationReceivedTask(void )
{
  bool wasTriggeredByConfigRoutine;

#line 858
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 858
    wasTriggeredByConfigRoutine = GSMDriverM$currentCmdState == GSMDriverM$GSM_CHECK_IF_ASSOC;
#line 858
    __nesc_atomic_end(__nesc_atomic); }
  if (wasTriggeredByConfigRoutine) {
      GSMDriverM$CommandTimer$stop();
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 861
        GSMDriverM$commandRetryCount = 0;
#line 861
        __nesc_atomic_end(__nesc_atomic); }
      GSMDriverM$Timer$start(TIMER_ONE_SHOT, 500);
    }
  if (GSMDriverM$currentAssocData.status == REGISTERED_HOME || GSMDriverM$currentAssocData.status == REGISTERED_ROAMING) 
    {
      GSMDriverM$isAssociated = TRUE;



      if (GSMDriverM$isConfigDone && !GSMDriverM$isStartDone) 
        {
          GSMDriverM$isStartDone = TRUE;
          TOS_post(GSMDriverM$startDoneTask);
        }
    }
  else 
    {
      GSMDriverM$isAssociated = FALSE;
    }
}

#line 744
static inline void GSMDriverM$GsmUartHandler$NetworkRegistrationReceived(gsm_cellid_areacode_data_t *ptr, uint8_t dataLength)
{
  nmemcpy(&GSMDriverM$currentAssocData, ptr, sizeof GSMDriverM$currentAssocData);
  TOS_post(GSMDriverM$NetworkRegistrationReceivedTask);
  return;
}

# 43 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
inline static void GsmUartHandlerM$GsmUartHandler$NetworkRegistrationReceived(gsm_cellid_areacode_data_t *ptr, uint8_t dataLength){
#line 43
  GSMDriverM$GsmUartHandler$NetworkRegistrationReceived(ptr, dataLength);
#line 43
}
#line 43
# 309 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static __inline uint8_t GsmUartHandlerM$countFieldsByDelimiter(char *src, const char delim)
#line 309
{
  uint8_t count = 0;
  uint8_t i;

#line 312
  if (strlen(src) == 0) {
      return 0;
    }
  for (i = 0; i < strlen(src) - 1; i++) {
      if (src[i] == delim) {
          count++;
        }
    }
  return count;
}

#line 203
static __inline bool GsmUartHandlerM$checkForNetworkRegistrationReceived(uint8_t index)
{
  char *buf = GsmUartHandlerM$uartBuffer[index];

  if (strstr(buf, "+CREG:") == (void *)0) {
      return FALSE;
    }
  else {
      char *dataSegment;
      char *prog;
      char *tok;
      uint8_t numberOfFields;

      uint16_t areacode;
      uint16_t cellid;

      dataSegment = strchr(buf, ':') + 1;

      numberOfFields = GsmUartHandlerM$countFieldsByDelimiter(dataSegment, ',');

      switch (numberOfFields) {
          case 1: 

            tok = strtok_r(dataSegment, ",", &prog);

          tok = strtok_r((void *)0, ",", &prog);
          GsmUartHandlerM$regData.status = atoi(tok);
          GsmUartHandlerM$regData.areacode = 0;
          GsmUartHandlerM$regData.cellid = 0;
          break;
          case 2: 
            tok = strtok_r(dataSegment, ",", &prog);

          GsmUartHandlerM$regData.status = atoi(tok);

          tok = strtok_r((void *)0, ",", &prog);

          sscanf(tok, "%x", &areacode);
          GsmUartHandlerM$regData.areacode = areacode;

          tok = strtok_r((void *)0, ",", &prog);

          sscanf(tok, "%x", &cellid);
          GsmUartHandlerM$regData.cellid = cellid;
          break;
          case 3: 

            tok = strtok_r(dataSegment, ",", &prog);
          tok = strtok_r((void *)0, ",", &prog);

          GsmUartHandlerM$regData.status = atoi(tok);
          tok = strtok_r((void *)0, ",", &prog);

          sscanf(tok, "%x", &areacode);
          GsmUartHandlerM$regData.areacode = areacode;
          tok = strtok_r((void *)0, ",", &prog);

          sscanf(tok, "%x", &cellid);
          GsmUartHandlerM$regData.cellid = cellid;
          break;
          default: 
            GsmUartHandlerM$regData.status = 0;
          GsmUartHandlerM$regData.areacode = 0;
          GsmUartHandlerM$regData.cellid = 0;
          break;
        }
      return TRUE;
    }
}

# 22 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
inline static result_t GSMDriverM$GsmUartHandler$sendCommand(char *cmd, uint8_t length){
#line 22
  unsigned char __nesc_result;
#line 22

#line 22
  __nesc_result = GsmUartHandlerM$GsmUartHandler$sendCommand(cmd, length);
#line 22

#line 22
  return __nesc_result;
#line 22
}
#line 22
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
inline static result_t GSMDriverM$CommandTimer$start(char type, uint32_t interval){
#line 37
  unsigned char __nesc_result;
#line 37

#line 37
  __nesc_result = TimerM$Timer$start(2U, type, interval);
#line 37

#line 37
  return __nesc_result;
#line 37
}
#line 37
# 769 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline void GSMDriverM$GsmUartHandler$PromptReceived(void )
{
  if (GSMDriverM$currentCmdState == GSMDriverM$SMS_REQUEST_PROMPT) 
    {
      result_t status;

#line 774
      GSMDriverM$CommandTimer$stop();
      /* atomic removed: atomic calls only */
#line 775
      GSMDriverM$currentCmdState = GSMDriverM$SMS_SEND_PAYLOAD;
      strcpy((char *)GSMDriverM$SMS_SEND_DATA_BUFFER, (char *)GSMDriverM$currentTextData);
      strcat((char *)GSMDriverM$SMS_SEND_DATA_BUFFER, "\x1A");
      /* atomic removed: atomic calls only */
      GSMDriverM$CURRENT_COMMAND = GSMDriverM$SMS_SEND_DATA_BUFFER;
      GSMDriverM$CommandTimer$start(TIMER_ONE_SHOT, 60000u);
      /* atomic removed: atomic calls only */



      status = GSMDriverM$GsmUartHandler$sendCommand((char *)GSMDriverM$SMS_SEND_DATA_BUFFER, 
      strlen((char *)GSMDriverM$SMS_SEND_DATA_BUFFER));
    }
  else 
    {
      return;
    }
}

# 56 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
inline static void GsmUartHandlerM$GsmUartHandler$PromptReceived(void ){
#line 56
  GSMDriverM$GsmUartHandler$PromptReceived();
#line 56
}
#line 56
# 546 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static __inline bool GsmUartHandlerM$checkForPromptReceived(uint8_t index)
{

  char *buf = GsmUartHandlerM$uartBuffer[index];



  if (strstr(buf, "> ") == (void *)0) {

      return FALSE;
    }
  else {




      return TRUE;
    }
}

# 52 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSM_ModemI.nc"
inline static result_t GSMDriverM$GSM_Modem$sendSMSDone(gsm_error_t result, uint8_t *endpoint, uint8_t *data){
#line 52
  unsigned char __nesc_result;
#line 52

#line 52
  __nesc_result = TestGSMDriverM$GSM_Modem$sendSMSDone(result, endpoint, data);
#line 52

#line 52
  return __nesc_result;
#line 52
}
#line 52
# 935 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline void GSMDriverM$sendSMSDoneTask(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 937
    GSMDriverM$currentCmdState = GSMDriverM$NONE;
#line 937
    __nesc_atomic_end(__nesc_atomic); }
  GSMDriverM$GSM_Modem$sendSMSDone(NO_ERROR, GSMDriverM$currentPhoneNumber, GSMDriverM$currentTextData);
}

# 91 "TestGSMDriverM.nc"
static inline result_t TestGSMDriverM$GSM_Modem$setCellMonitorReportValueDone(gsm_error_t result)
#line 91
{
  return SUCCESS;
}

# 34 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSM_ModemI.nc"
inline static result_t GSMDriverM$GSM_Modem$setCellMonitorReportValueDone(gsm_error_t result){
#line 34
  unsigned char __nesc_result;
#line 34

#line 34
  __nesc_result = TestGSMDriverM$GSM_Modem$setCellMonitorReportValueDone(result);
#line 34

#line 34
  return __nesc_result;
#line 34
}
#line 34
# 882 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline void GSMDriverM$setCellMonitorReportValueDoneTask(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 884
    GSMDriverM$currentCmdState = GSMDriverM$NONE;
#line 884
    __nesc_atomic_end(__nesc_atomic); }
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 885
    GSMDriverM$commandRetryCount = 0;
#line 885
    __nesc_atomic_end(__nesc_atomic); }
  GSMDriverM$GSM_Modem$setCellMonitorReportValueDone(NO_ERROR);
}

#line 794
static inline void GSMDriverM$OkReceivedTask(void )
{
  uint8_t lState;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 798
    lState = GSMDriverM$currentCmdState;
#line 798
    __nesc_atomic_end(__nesc_atomic); }
  switch (lState) 
    {
      case GSMDriverM$GSM_SET_CELL_MONITOR_RPT_VAL: 
        GSMDriverM$CommandTimer$stop();
      TOS_post(GSMDriverM$setCellMonitorReportValueDoneTask);
      break;
      case GSMDriverM$GSM_GET_CELL_MONITOR_RPT: 
        case GSMDriverM$GSM_GET_SIGNAL_QUALITY: 
          case GSMDriverM$SMS_REQUEST_PROMPT: 
            break;
      case GSMDriverM$SMS_SEND_PAYLOAD: 
        GSMDriverM$CommandTimer$stop();
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 811
        GSMDriverM$commandRetryCount = 0;
#line 811
        __nesc_atomic_end(__nesc_atomic); }
      TOS_post(GSMDriverM$sendSMSDoneTask);
      break;
      case GSMDriverM$GSM_CHECK_IF_ASSOC: 
        break;
      default: 
        GSMDriverM$CommandTimer$stop();
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 818
        GSMDriverM$commandRetryCount = 0;
#line 818
        __nesc_atomic_end(__nesc_atomic); }
      GSMDriverM$Timer$start(TIMER_ONE_SHOT, 500);
      break;
    }
}

#line 731
static inline void GSMDriverM$GsmUartHandler$OkReceived(void )
{
  TOS_post(GSMDriverM$OkReceivedTask);
  return;
}

# 30 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
inline static void GsmUartHandlerM$GsmUartHandler$OkReceived(void ){
#line 30
  GSMDriverM$GsmUartHandler$OkReceived();
#line 30
}
#line 30
# 161 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static __inline bool GsmUartHandlerM$checkForOkReceived(uint8_t index)
{

  char *buf = GsmUartHandlerM$uartBuffer[index];



  if (strstr(buf, "OK") == (void *)0) {

      return FALSE;
    }
  else {

      return TRUE;
    }
}

# 36 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
inline static void GsmUartHandlerM$GsmUartHandler$ErrorReceived(gsm_error_t errorCode){
#line 36
  GSMDriverM$GsmUartHandler$ErrorReceived(errorCode);
#line 36
}
#line 36
# 181 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static __inline bool GsmUartHandlerM$checkForErrorReceived(uint8_t index)
{

  char *buf = GsmUartHandlerM$uartBuffer[index];

  char *startPtr = strstr(buf, "ERROR:");

  if (startPtr == (void *)0) {

      return FALSE;
    }
  else {

      startPtr += strlen("ERROR:");
      sscanf(startPtr, "%d", &GsmUartHandlerM$ec);
      return TRUE;
    }
}

#line 112
static __inline void GsmUartHandlerM$checkForMessages(uint8_t index)
{
  if (GsmUartHandlerM$checkForErrorReceived(index)) {
      /* atomic removed: atomic calls only */



      GsmUartHandlerM$GsmUartHandler$ErrorReceived(GsmUartHandlerM$ec);
    }
  else {
#line 121
    if (GsmUartHandlerM$checkForOkReceived(index)) {
        /* atomic removed: atomic calls only */

        GsmUartHandlerM$GsmUartHandler$OkReceived();
      }
    else {
#line 126
      if (GsmUartHandlerM$checkForPromptReceived(index)) {
          /* atomic removed: atomic calls only */

          GsmUartHandlerM$GsmUartHandler$PromptReceived();
        }
      else {
#line 131
        if (GsmUartHandlerM$checkForNetworkRegistrationReceived(index)) {
            /* atomic removed: atomic calls only */

            GsmUartHandlerM$GsmUartHandler$NetworkRegistrationReceived(&GsmUartHandlerM$regData, 
            sizeof GsmUartHandlerM$regData);
          }
        else {
#line 137
          if (GsmUartHandlerM$checkForSignalQualityReceived(index)) {
              /* atomic removed: atomic calls only */

              GsmUartHandlerM$GsmUartHandler$SignalQualityReceived(&GsmUartHandlerM$sigQualData, 
              sizeof GsmUartHandlerM$sigQualData);
            }
          else {
#line 143
            if (GsmUartHandlerM$checkForCellMonitorReceived(index)) {



                GsmUartHandlerM$gsmCellMonCount = 0;
                /* atomic removed: atomic calls only */
#line 148
                GsmUartHandlerM$GsmUartHandler$CellMonitorReportReceived(&GsmUartHandlerM$cellMonData, 
                sizeof GsmUartHandlerM$cellMonData);
              }
            else 
              {
              }
            }
          }
        }
      }
    }
}

#line 95
static __inline uint8_t GsmUartHandlerM$removeRecvQueue(void )
{
  uint8_t r_val;

  /* atomic removed: atomic calls only */
#line 98
  {
    r_val = GsmUartHandlerM$recvQueue.contents[GsmUartHandlerM$recvQueue.front];
    GsmUartHandlerM$recvQueue.front = (GsmUartHandlerM$recvQueue.front + 1) % 4;
    GsmUartHandlerM$recvQueue.count--;
  }
  return r_val;
}

#line 634
static inline void GsmUartHandlerM$handleDataReceived(void )
{


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 638
    {
      uint8_t nextIndex = GsmUartHandlerM$removeRecvQueue();

#line 640
      GsmUartHandlerM$checkForMessages(nextIndex);
    }
#line 641
    __nesc_atomic_end(__nesc_atomic); }
}

#line 78
static __inline result_t GsmUartHandlerM$enterRecvQueue(uint8_t element)
{
  if (GsmUartHandlerM$recvQueue.count >= 4) 
    {
      return FAIL;
    }
  else 
    {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 86
        {
          uint8_t newIndex = (GsmUartHandlerM$recvQueue.front + GsmUartHandlerM$recvQueue.count) % 4;

#line 88
          GsmUartHandlerM$recvQueue.contents[newIndex] = element;
          GsmUartHandlerM$recvQueue.count++;
          {
            unsigned char __nesc_temp = 
#line 90
            SUCCESS;

            {
#line 90
              __nesc_atomic_end(__nesc_atomic); 
#line 90
              return __nesc_temp;
            }
          }
        }
#line 93
        __nesc_atomic_end(__nesc_atomic); }
    }
}

#line 617
static inline TOS_MsgPtr GsmUartHandlerM$Receive$receive(TOS_MsgPtr bufferPtr)
{


  General_MsgPtr gen = (General_MsgPtr )bufferPtr;

#line 622
  strcpy((char *)GsmUartHandlerM$uartBuffer[GsmUartHandlerM$curBufIndex], (char *)gen->data);


  GsmUartHandlerM$enterRecvQueue(GsmUartHandlerM$curBufIndex);
  GsmUartHandlerM$curBufIndex = (GsmUartHandlerM$curBufIndex + 1) % 4;
  TOS_post(GsmUartHandlerM$handleDataReceived);
  return bufferPtr;
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ReceiveMsg.nc"
inline static TOS_MsgPtr UARTPacket$Receive$receive(TOS_MsgPtr m){
#line 53
  struct TOS_Msg *__nesc_result;
#line 53

#line 53
  __nesc_result = GsmUartHandlerM$Receive$receive(m);
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 88 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static __inline uint8_t UARTPacket$removeRecvQueue(void )
{
  uint8_t r_val;

#line 91
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 91
    {
      r_val = UARTPacket$recvQueue.contents[UARTPacket$recvQueue.front];
      UARTPacket$recvQueue.front = (UARTPacket$recvQueue.front + 1) % 4;
      UARTPacket$recvQueue.count--;
    }
#line 95
    __nesc_atomic_end(__nesc_atomic); }
  return r_val;
}

#line 257
static inline void UARTPacket$receiveTask(void )
#line 257
{




  uint8_t msgNumber = UARTPacket$removeRecvQueue();

  UARTPacket$Receive$receive((TOS_MsgPtr )&UARTPacket$bufferRecv[msgNumber]);
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 265
    UARTPacket$gpsRecvState = UARTPacket$NO_GSM_START_BYTE;
#line 265
    __nesc_atomic_end(__nesc_atomic); }
}

#line 71
static __inline result_t UARTPacket$enterRecvQueue(uint8_t element)
{
  if (UARTPacket$recvQueue.count >= 4) 
    {
      return FAIL;
    }
  else 
    {
      /* atomic removed: atomic calls only */
#line 79
      {
        uint8_t newIndex = (UARTPacket$recvQueue.front + UARTPacket$recvQueue.count) % 4;

#line 81
        UARTPacket$recvQueue.contents[newIndex] = element;
        UARTPacket$recvQueue.count++;
        {
          unsigned char __nesc_temp = 
#line 83
          SUCCESS;

#line 83
          return __nesc_temp;
        }
      }
    }
}

# 197 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline void *nmemset(void *to, int val, size_t n)
{
  char *cto = to;

  while (n--) * cto++ = val;

  return to;
}

# 275 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static inline result_t UARTPacket$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength)
#line 275
{
  uint8_t lState;
  result_t stat;

  /* atomic removed: atomic calls only */
#line 278
  lState = UARTPacket$gpsRecvState;

  if (data == '\0') {
      return SUCCESS;
    }

  if (error) {
      /* atomic removed: atomic calls only */
#line 285
      {
        UARTPacket$rxCount = 0;
        UARTPacket$gpsRecvState = UARTPacket$NO_GSM_START_BYTE;
      }

      return FAIL;
    }


  if (lState == UARTPacket$GSM_BUF_NOT_AVAIL) {
      return SUCCESS;
    }


  if (lState == UARTPacket$NO_GSM_START_BYTE) {

      if (data != 0x0D && data != 0x0A) 
        {
          /* atomic removed: atomic calls only */

          {
            UARTPacket$curBufferRecvIndex = (UARTPacket$curBufferRecvIndex + 1) % 4;
            UARTPacket$recPtr = (uint8_t *)&UARTPacket$bufferRecv[UARTPacket$curBufferRecvIndex];
            nmemset(UARTPacket$recPtr, 0, 200 + 1);
            UARTPacket$rxCount = 1;
            UARTPacket$recPtr[1] = data;
            UARTPacket$gpsRecvState = UARTPacket$GSM_START_BYTE;
          }
        }
      return SUCCESS;
    }


  if (lState == UARTPacket$GSM_START_BYTE) {


      if (UARTPacket$rxCount >= 200 - 1) {
          /* atomic removed: atomic calls only */
#line 322
          UARTPacket$gpsRecvState = UARTPacket$NO_GSM_START_BYTE;
          return SUCCESS;
        }
      else {
#line 325
        if (data == 0x0D || UARTPacket$recPtr[UARTPacket$rxCount] == '>') {
            /* atomic removed: atomic calls only */
            {
              UARTPacket$gpsRecvState = UARTPacket$GSM_BUF_NOT_AVAIL;
              UARTPacket$rxCount++;
              UARTPacket$recPtr[UARTPacket$rxCount] = data;
              UARTPacket$recPtr[0] = UARTPacket$rxCount;
              UARTPacket$recPtr[UARTPacket$rxCount + 1] = '\0';
              stat = UARTPacket$enterRecvQueue(UARTPacket$curBufferRecvIndex);
            }





            TOS_post(UARTPacket$receiveTask);
            /* atomic removed: atomic calls only */
#line 341
            UARTPacket$gpsRecvState = UARTPacket$NO_GSM_START_BYTE;

            return SUCCESS;
          }
        else {
            /* atomic removed: atomic calls only */
            {
              UARTPacket$rxCount++;
              UARTPacket$recPtr[UARTPacket$rxCount] = data;
            }
          }
        }
    }

  return SUCCESS;
}

# 45 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t UART0M$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength){
#line 45
  unsigned char __nesc_result;
#line 45

#line 45
  __nesc_result = UARTPacket$ByteComm$rxByteReady(data, error, strength);
#line 45

#line 45
  return __nesc_result;
#line 45
}
#line 45
# 57 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UART0M.nc"
static inline result_t UART0M$HPLUART$get(uint8_t data)
#line 57
{




  UART0M$ByteComm$rxByteReady(data, FALSE, 0);
  {
  }
#line 63
  ;
  return SUCCESS;
}

# 66 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t HPLUART0M$UART$get(uint8_t data){
#line 66
  unsigned char __nesc_result;
#line 66

#line 66
  __nesc_result = UART0M$HPLUART$get(data);
#line 66

#line 66
  return __nesc_result;
#line 66
}
#line 66
# 826 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline void GSMDriverM$ErrorReceivedTask(void )
{
  uint8_t lState;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 830
    lState = GSMDriverM$currentCmdState;
#line 830
    __nesc_atomic_end(__nesc_atomic); }
  switch (lState) 
    {
      case GSMDriverM$GSM_SET_CELL_MONITOR_RPT_VAL: 
        GSMDriverM$CommandTimer$stop();
      TOS_post(GSMDriverM$setCellMonitorReportValueFailTask);
      break;
      case GSMDriverM$GSM_GET_CELL_MONITOR_RPT: 
        GSMDriverM$CommandTimer$stop();
      TOS_post(GSMDriverM$cellMonitorReportFailTask);
      break;
      case GSMDriverM$GSM_GET_SIGNAL_QUALITY: 
        GSMDriverM$CommandTimer$stop();
      TOS_post(GSMDriverM$signalQualityDataFailTask);
      break;
      case GSMDriverM$SMS_REQUEST_PROMPT: 
        case GSMDriverM$SMS_SEND_PAYLOAD: 
          GSMDriverM$CommandTimer$stop();
      TOS_post(GSMDriverM$sendSMSFailTask);
      break;
      default: 
        break;
    }
}

# 178 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static inline result_t UARTPacket$SendVarLenPacket$send(uint8_t *packet, uint8_t numBytes)
#line 178
{
  /* atomic removed: atomic calls only */
#line 179
  {
    UARTPacket$state = UARTPacket$BYTES;
  }

  return UARTPacket$txBytes(packet, numBytes);
}

# 33 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendVarLenPacket.nc"
inline static result_t GsmUartHandlerM$SendVarLenPacketGsm$send(uint8_t *packet, uint8_t numBytes){
#line 33
  unsigned char __nesc_result;
#line 33

#line 33
  __nesc_result = UARTPacket$SendVarLenPacket$send(packet, numBytes);
#line 33

#line 33
  return __nesc_result;
#line 33
}
#line 33
# 113 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
static inline result_t HPLUART0M$UART$put(uint8_t data)
#line 113
{
  /* atomic removed: atomic calls only */
#line 114
  {
    * (volatile uint8_t *)0XC6 = data;
    * (volatile uint8_t *)0xC0 |= 1 << 6;
  }

  return SUCCESS;
}

# 58 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t UART0M$HPLUART$put(uint8_t data){
#line 58
  unsigned char __nesc_result;
#line 58

#line 58
  __nesc_result = HPLUART0M$UART$put(data);
#line 58

#line 58
  return __nesc_result;
#line 58
}
#line 58
# 725 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline result_t GSMDriverM$GsmUartHandler$SendCommandDone(uint8_t *cmd, result_t result)
{

  return SUCCESS;
}

# 61 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerI.nc"
inline static result_t GsmUartHandlerM$GsmUartHandler$SendCommandDone(uint8_t *cmd, result_t result){
#line 61
  unsigned char __nesc_result;
#line 61

#line 61
  __nesc_result = GSMDriverM$GsmUartHandler$SendCommandDone(cmd, result);
#line 61

#line 61
  return __nesc_result;
#line 61
}
#line 61
# 652 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static inline void GsmUartHandlerM$handleSendMoniFail(void )
#line 652
{
  GsmUartHandlerM$GsmUartHandler$SendCommandDone((void *)0, FAIL);
  GsmUartHandlerM$GsmUartHandler$ErrorReceived(UNSUPPORTED_FEATURE);
}

# 34 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t UARTPacket$ByteComm$txByte(uint8_t data){
#line 34
  unsigned char __nesc_result;
#line 34

#line 34
  __nesc_result = UART0M$ByteComm$txByte(data);
#line 34

#line 34
  return __nesc_result;
#line 34
}
#line 34
# 228 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static inline result_t UARTPacket$ByteComm$txByteReady(bool success)
#line 228
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 229
    {
      if (UARTPacket$txCount > 0) {
          if (!success) {

              UARTPacket$sendComplete(FAIL);
            }
          else {
#line 235
            if (UARTPacket$txCount < UARTPacket$txLength) {

                if (!UARTPacket$ByteComm$txByte(UARTPacket$sendPtr[UARTPacket$txCount++])) {
#line 237
                  UARTPacket$sendComplete(FAIL);
                  }
              }
            }
        }
    }
#line 242
    __nesc_atomic_end(__nesc_atomic); }
#line 241
  return SUCCESS;
}

# 54 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t UART0M$ByteComm$txByteReady(bool success){
#line 54
  unsigned char __nesc_result;
#line 54

#line 54
  __nesc_result = UARTPacket$ByteComm$txByteReady(success);
#line 54

#line 54
  return __nesc_result;
#line 54
}
#line 54
# 244 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static inline result_t UARTPacket$ByteComm$txDone(void )
#line 244
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 245
    {
      if (UARTPacket$txCount == UARTPacket$txLength) {
        UARTPacket$sendComplete(TRUE);
        }
    }
#line 249
    __nesc_atomic_end(__nesc_atomic); }
#line 249
  return SUCCESS;
}

# 62 "/Users/wbennett/opt/MoteWorks/tos/interfaces/ByteComm.nc"
inline static result_t UART0M$ByteComm$txDone(void ){
#line 62
  unsigned char __nesc_result;
#line 62

#line 62
  __nesc_result = UARTPacket$ByteComm$txDone();
#line 62

#line 62
  return __nesc_result;
#line 62
}
#line 62
# 67 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UART0M.nc"
static inline result_t UART0M$HPLUART$putDone(void )
#line 67
{
  bool oldState;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 70
    {
      {
      }
#line 71
      ;
      oldState = UART0M$state;
      UART0M$state = FALSE;
    }
#line 74
    __nesc_atomic_end(__nesc_atomic); }








  if (oldState) {
      UART0M$ByteComm$txDone();
      UART0M$ByteComm$txByteReady(TRUE);
    }
  return SUCCESS;
}

# 74 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t HPLUART0M$UART$putDone(void ){
#line 74
  unsigned char __nesc_result;
#line 74

#line 74
  __nesc_result = UART0M$HPLUART$putDone();
#line 74

#line 74
  return __nesc_result;
#line 74
}
#line 74
# 699 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static inline result_t GsmUartHandlerM$SendVarLenPacketGsm$sendDone(uint8_t *sentdata, result_t result)
{

  GsmUartHandlerM$GsmUartHandler$SendCommandDone(sentdata, result);
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/SendVarLenPacket.nc"
inline static result_t UARTPacket$SendVarLenPacket$sendDone(uint8_t *packet, result_t success){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = GsmUartHandlerM$SendVarLenPacketGsm$sendDone(packet, success);
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 195 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static inline void UARTPacket$sendVarLenSuccessTask(void )
#line 195
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 196
    {
      UARTPacket$txCount = 0;
      UARTPacket$state = UARTPacket$IDLE;
    }
#line 199
    __nesc_atomic_end(__nesc_atomic); }
  UARTPacket$SendVarLenPacket$sendDone((uint8_t *)UARTPacket$sendPtr, SUCCESS);
}

#line 187
static inline void UARTPacket$sendVarLenFailTask(void )
#line 187
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 188
    {
      UARTPacket$txCount = 0;
      UARTPacket$state = UARTPacket$IDLE;
    }
#line 191
    __nesc_atomic_end(__nesc_atomic); }
  UARTPacket$SendVarLenPacket$sendDone((uint8_t *)UARTPacket$sendPtr, FAIL);
}

# 80 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline uint8_t HPLClock$Clock$getInterval(void )
#line 80
{
  return * (volatile uint8_t *)0xB3;
}

# 100 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static uint8_t TimerM$Clock$getInterval(void ){
#line 100
  unsigned char __nesc_result;
#line 100

#line 100
  __nesc_result = HPLClock$Clock$getInterval();
#line 100

#line 100
  return __nesc_result;
#line 100
}
#line 100
# 38 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static inline void add_time_millis(uint32_t time_to_add)
#line 38
{



  sys_time += time_to_add;
}

# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
inline static uint8_t TimerM$PowerManagement$adjustPower(void ){
#line 19
  unsigned char __nesc_result;
#line 19

#line 19
  __nesc_result = HPLPowerManagementM$PowerManagement$adjustPower();
#line 19

#line 19
  return __nesc_result;
#line 19
}
#line 19
# 70 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline void HPLClock$Clock$setInterval(uint8_t value)
#line 70
{
  * (volatile uint8_t *)0xB3 = value;
}

# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static void TimerM$Clock$setInterval(uint8_t value){
#line 84
  HPLClock$Clock$setInterval(value);
#line 84
}
#line 84
# 136 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline uint8_t HPLClock$Clock$readCounter(void )
#line 136
{
  return * (volatile uint8_t *)0xB2;
}

# 132 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static uint8_t TimerM$Clock$readCounter(void ){
#line 132
  unsigned char __nesc_result;
#line 132

#line 132
  __nesc_result = HPLClock$Clock$readCounter();
#line 132

#line 132
  return __nesc_result;
#line 132
}
#line 132
# 111 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
inline static void TimerM$adjustInterval(void )
#line 111
{
  uint8_t i;
#line 112
  uint8_t val = TimerM$maxTimerInterval;

#line 113
  if (TimerM$mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM$mState & (0x1L << i) && TimerM$mTimerList[i].ticksLeft < val) {
              val = TimerM$mTimerList[i].ticksLeft;
            }
        }
#line 130
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 130
        {
          i = TimerM$Clock$readCounter() + 3;
          if (val < i) {
              val = i;
            }
          TimerM$mInterval = val;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
        }
#line 138
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 141
        {
          TimerM$mInterval = TimerM$maxTimerInterval;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
        }
#line 145
        __nesc_atomic_end(__nesc_atomic); }
    }
  TimerM$PowerManagement$adjustPower();
}

#line 168
static inline void TimerM$enqueue(uint8_t value)
#line 168
{
  if (TimerM$queue_tail == NUM_TIMERS - 1) {
    TimerM$queue_tail = -1;
    }
#line 171
  TimerM$queue_tail++;
  TimerM$queue_size++;
  TimerM$queue[(uint8_t )TimerM$queue_tail] = value;
}

# 63 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_RED_LED_PIN()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 2);
}

#line 65
static __inline void TOSH_CLR_YELLOW_LED_PIN()
#line 65
{
#line 65
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 0);
}

# 145 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$set(uint8_t ledsNum)
#line 145
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 146
    {
      LedsC$ledsOn = ledsNum & 0x7;
      if (LedsC$ledsOn & LedsC$GREEN_BIT) {
        TOSH_CLR_GREEN_LED_PIN();
        }
      else {
#line 151
        TOSH_SET_GREEN_LED_PIN();
        }
#line 152
      if (LedsC$ledsOn & LedsC$YELLOW_BIT) {
        TOSH_CLR_YELLOW_LED_PIN();
        }
      else {
#line 155
        TOSH_SET_YELLOW_LED_PIN();
        }
#line 156
      if (LedsC$ledsOn & LedsC$RED_BIT) {
        TOSH_CLR_RED_LED_PIN();
        }
      else {
#line 159
        TOSH_SET_RED_LED_PIN();
        }
    }
#line 161
    __nesc_atomic_end(__nesc_atomic); }
#line 161
  return SUCCESS;
}

# 128 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t TestGSMDriverM$Leds$set(uint8_t value){
#line 128
  unsigned char __nesc_result;
#line 128

#line 128
  __nesc_result = LedsC$Leds$set(value);
#line 128

#line 128
  return __nesc_result;
#line 128
}
#line 128
# 295 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline result_t GSMDriverM$SplitControlStatus$stop(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 297
    GSMDriverM$gsm_error_code = NO_ERROR;
#line 297
    __nesc_atomic_end(__nesc_atomic); }
  TOS_post(GSMDriverM$stopDoneTask);
  return SUCCESS;
}

# 47 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/SplitControlStatus.nc"
inline static result_t TestGSMDriverM$GsmControl$stop(void ){
#line 47
  unsigned char __nesc_result;
#line 47

#line 47
  __nesc_result = GSMDriverM$SplitControlStatus$stop();
#line 47

#line 47
  return __nesc_result;
#line 47
}
#line 47
# 110 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t TestGSMDriverM$Leds$yellowToggle(void ){
#line 110
  unsigned char __nesc_result;
#line 110

#line 110
  __nesc_result = LedsC$Leds$yellowToggle();
#line 110

#line 110
  return __nesc_result;
#line 110
}
#line 110
# 196 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static __inline void GSMDriverM$setErrorCodeOnImedFail(void )
{
  uint8_t lState;

#line 199
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 199
    lState = GSMDriverM$currentCmdState;
#line 199
    __nesc_atomic_end(__nesc_atomic); }
  if (!GSMDriverM$isAssociated) 
    {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 202
        GSMDriverM$gsm_error_code = GSM_NOT_ASSOCIATED;
#line 202
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
#line 204
    if (lState != GSMDriverM$NONE) 
      {
        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 206
          GSMDriverM$gsm_error_code = GSM_BUSY;
#line 206
          __nesc_atomic_end(__nesc_atomic); }
      }
    }
}

#line 466
static __inline result_t GSMDriverM$sendCurrentCommand(void )
{
  result_t status;
  uint8_t localState;

#line 470
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 470
    localState = GSMDriverM$currentCmdState;
#line 470
    __nesc_atomic_end(__nesc_atomic); }
  switch (localState) 
    {
      case GSMDriverM$SMS_REQUEST_PROMPT: 
        GSMDriverM$timeoutPeriod = 1000u;
      break;
      case GSMDriverM$SMS_SEND_PAYLOAD: 
        GSMDriverM$timeoutPeriod = 60000u;
      break;
      default: 
        GSMDriverM$timeoutPeriod = 2048u;
      break;
    }

  GSMDriverM$CommandTimer$start(TIMER_ONE_SHOT, GSMDriverM$timeoutPeriod);
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 485
    status = GSMDriverM$GsmUartHandler$sendCommand((char *)GSMDriverM$CURRENT_COMMAND, 
    strlen((char *)GSMDriverM$CURRENT_COMMAND));
#line 486
    __nesc_atomic_end(__nesc_atomic); }
  if (status == FAIL) 
    {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 489
        GSMDriverM$currentCmdState = GSMDriverM$NONE;
#line 489
        __nesc_atomic_end(__nesc_atomic); }
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 490
        GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
#line 490
        __nesc_atomic_end(__nesc_atomic); }
    }

  return status;
}

#line 428
static inline result_t GSMDriverM$GSM_Modem$sendSMS(uint8_t *endpoint, uint8_t *data)
{
  uint8_t lState;

#line 431
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 431
    lState = GSMDriverM$currentCmdState;
#line 431
    __nesc_atomic_end(__nesc_atomic); }

  if (lState == GSMDriverM$NONE && GSMDriverM$isAssociated) 
    {
      result_t status;

#line 436
      nmemset(GSMDriverM$currentPhoneNumber, 0, sizeof GSMDriverM$currentPhoneNumber);
      strcpy((char *)GSMDriverM$currentPhoneNumber, (char *)endpoint);
      nmemset(GSMDriverM$currentTextData, 0, sizeof GSMDriverM$currentTextData);
      strcpy((char *)GSMDriverM$currentTextData, (char *)data);

      strcpy((char *)GSMDriverM$SMS_REQUEST_PROMPT_WHOLE_CMD, (char *)GSMDriverM$SMS_REQUEST_PROMPT_P1_CMD);
      strcat((char *)GSMDriverM$SMS_REQUEST_PROMPT_WHOLE_CMD, (char *)GSMDriverM$currentPhoneNumber);
      strcat((char *)GSMDriverM$SMS_REQUEST_PROMPT_WHOLE_CMD, (char *)GSMDriverM$SMS_REQUEST_PROMPT_P2_CMD);
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 444
        GSMDriverM$currentCmdState = GSMDriverM$SMS_REQUEST_PROMPT;
#line 444
        __nesc_atomic_end(__nesc_atomic); }
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 445
        GSMDriverM$CURRENT_COMMAND = GSMDriverM$SMS_REQUEST_PROMPT_WHOLE_CMD;
#line 445
        __nesc_atomic_end(__nesc_atomic); }
      status = GSMDriverM$sendCurrentCommand();
      if (status == FAIL) 
        {
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
            {
              GSMDriverM$CommandTimer$stop();
              GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
              TOS_post(GSMDriverM$sendSMSFailTask);
            }
#line 454
            __nesc_atomic_end(__nesc_atomic); }
        }
      return status;
    }
  else 
    {
      GSMDriverM$setErrorCodeOnImedFail();
      TOS_post(GSMDriverM$sendSMSFailTask);
      return FAIL;
    }
}

# 28 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSM_ModemI.nc"
inline static result_t TestGSMDriverM$GSM_Modem$sendSMS(uint8_t *endpoint, uint8_t *data){
#line 28
  unsigned char __nesc_result;
#line 28

#line 28
  __nesc_result = GSMDriverM$GSM_Modem$sendSMS(endpoint, data);
#line 28

#line 28
  return __nesc_result;
#line 28
}
#line 28
# 366 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline result_t GSMDriverM$GSM_Modem$getCellMonitorReport(void )
{
  uint8_t lState;

#line 369
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 369
    lState = GSMDriverM$currentCmdState;
#line 369
    __nesc_atomic_end(__nesc_atomic); }

  if (lState == GSMDriverM$NONE && GSMDriverM$isAssociated) 
    {
      result_t status;

#line 374
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 374
        GSMDriverM$currentCmdState = GSMDriverM$GSM_GET_CELL_MONITOR_RPT;
#line 374
        __nesc_atomic_end(__nesc_atomic); }
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 375
        GSMDriverM$CURRENT_COMMAND = GSMDriverM$GET_CELL_MONITOR_RPT_CMD;
#line 375
        __nesc_atomic_end(__nesc_atomic); }
      status = GSMDriverM$sendCurrentCommand();
      if (status == FAIL) 
        {
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
            {
              GSMDriverM$CommandTimer$stop();
              GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
              TOS_post(GSMDriverM$cellMonitorReportFailTask);
            }
#line 384
            __nesc_atomic_end(__nesc_atomic); }
        }
      return status;
    }
  else 
    {
      GSMDriverM$setErrorCodeOnImedFail();
      TOS_post(GSMDriverM$cellMonitorReportFailTask);
      return FAIL;
    }
}

# 21 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSM_ModemI.nc"
inline static result_t TestGSMDriverM$GSM_Modem$getCellMonitorReport(void ){
#line 21
  unsigned char __nesc_result;
#line 21

#line 21
  __nesc_result = GSMDriverM$GSM_Modem$getCellMonitorReport();
#line 21

#line 21
  return __nesc_result;
#line 21
}
#line 21
# 396 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline result_t GSMDriverM$GSM_Modem$getSignalQuality(void )
{
  uint8_t lState;

#line 399
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 399
    lState = GSMDriverM$currentCmdState;
#line 399
    __nesc_atomic_end(__nesc_atomic); }



  if (lState == GSMDriverM$NONE && GSMDriverM$isAssociated) 
    {
      result_t status;

#line 406
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 406
        GSMDriverM$currentCmdState = GSMDriverM$GSM_GET_SIGNAL_QUALITY;
#line 406
        __nesc_atomic_end(__nesc_atomic); }
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 407
        GSMDriverM$CURRENT_COMMAND = GSMDriverM$GET_SIGNAL_QUALITY_CMD;
#line 407
        __nesc_atomic_end(__nesc_atomic); }
      status = GSMDriverM$sendCurrentCommand();
      if (status == FAIL) 
        {
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
            {
              GSMDriverM$CommandTimer$stop();
              GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
              TOS_post(GSMDriverM$signalQualityDataFailTask);
            }
#line 416
            __nesc_atomic_end(__nesc_atomic); }
        }
      return status;
    }
  else 
    {
      GSMDriverM$setErrorCodeOnImedFail();
      TOS_post(GSMDriverM$signalQualityDataFailTask);
      return FAIL;
    }
}

# 22 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSM_ModemI.nc"
inline static result_t TestGSMDriverM$GSM_Modem$getSignalQuality(void ){
#line 22
  unsigned char __nesc_result;
#line 22

#line 22
  __nesc_result = GSMDriverM$GSM_Modem$getSignalQuality();
#line 22

#line 22
  return __nesc_result;
#line 22
}
#line 22
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
inline static result_t GSMDriverM$WatchdogTimer$start(char type, uint32_t interval){
#line 37
  unsigned char __nesc_result;
#line 37

#line 37
  __nesc_result = TimerM$Timer$start(3U, type, interval);
#line 37

#line 37
  return __nesc_result;
#line 37
}
#line 37
# 246 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static __inline void GSMDriverM$initVars(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 248
    GSMDriverM$commandRetryCount = 0;
#line 248
    __nesc_atomic_end(__nesc_atomic); }
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 249
    GSMDriverM$gsm_error_code = NO_ERROR;
#line 249
    __nesc_atomic_end(__nesc_atomic); }
  GSMDriverM$isAssociated = FALSE;
  GSMDriverM$isStartDone = FALSE;
  GSMDriverM$isConfigDone = FALSE;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 253
    GSMDriverM$CURRENT_COMMAND = (void *)0;
#line 253
    __nesc_atomic_end(__nesc_atomic); }
}

# 49 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
static inline result_t HPLUART0M$Setbaud(uint32_t baud_rate)
#line 49
{

  switch (baud_rate) {
      case 9600u: 
        * (volatile uint8_t *)0xC5 = 0;
      * (volatile uint8_t *)0xC4 = 95;
      break;

      case 19200u: 
        * (volatile uint8_t *)0xC5 = 0;
      * (volatile uint8_t *)0xC4 = 47;
      break;

      case 57600u: 
        * (volatile uint8_t *)0xC5 = 0;
      * (volatile uint8_t *)0xC4 = 15;
      break;

      case 115200u: 
        * (volatile uint8_t *)0xC5 = 0;
      * (volatile uint8_t *)0xC4 = 7;
      break;

      default: 
        return FAIL;
    }
  * (volatile uint8_t *)0xC0 = 1 << 1;
  * (volatile uint8_t *)0xC2 = (1 << 2) | (1 << 1);
  * (volatile uint8_t *)0XC1 = (((1 << 7) | (1 << 6)) | (1 << 4)) | (1 << 3);
  return SUCCESS;
}



static inline result_t HPLUART0M$UART$init(void )
#line 83
{

  HPLUART0M$Setbaud(TOS_UART0_BAUDRATE);
  return SUCCESS;
}

# 40 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t UART0M$HPLUART$init(void ){
#line 40
  unsigned char __nesc_result;
#line 40

#line 40
  __nesc_result = HPLUART0M$UART$init();
#line 40

#line 40
  return __nesc_result;
#line 40
}
#line 40
# 48 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UART0M.nc"
static inline result_t UART0M$Control$start(void )
#line 48
{
  return UART0M$HPLUART$init();
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t UARTPacket$ByteControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = UART0M$Control$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 144 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static inline result_t UARTPacket$Control$start(void )
#line 144
{
  return UARTPacket$ByteControl$start();
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t GsmUartHandlerM$UARTControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = UARTPacket$Control$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 595 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static inline void GsmUartHandlerM$startTask(void )
{

  GsmUartHandlerM$UARTControl$start();
}

#line 588
static inline result_t GsmUartHandlerM$StdControl$start(void )
{

  TOS_post(GsmUartHandlerM$startTask);
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t GSMDriverM$UARTControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = GsmUartHandlerM$StdControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 233 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline result_t GSMDriverM$SplitControlStatus$start(void )
{

  GSMDriverM$UARTControl$start();
  GSMDriverM$initVars();

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 239
    GSMDriverM$currentCmdState = GSMDriverM$GSM_START_READY;
#line 239
    __nesc_atomic_end(__nesc_atomic); }

  GSMDriverM$Timer$start(TIMER_ONE_SHOT, 100);
  GSMDriverM$WatchdogTimer$start(TIMER_ONE_SHOT, 120000u);
  return SUCCESS;
}

# 31 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/SplitControlStatus.nc"
inline static result_t TestGSMDriverM$GsmControl$start(void ){
#line 31
  unsigned char __nesc_result;
#line 31

#line 31
  __nesc_result = GSMDriverM$SplitControlStatus$start();
#line 31

#line 31
  return __nesc_result;
#line 31
}
#line 31
# 134 "TestGSMDriverM.nc"
static inline void TestGSMDriverM$transition(void )
#line 134
{
  uint8_t lstate = TestGSMDriverM$state;

#line 136
  switch (lstate) {
      case TestGSMDriverM$STARTING: 
        TestGSMDriverM$Leds$yellowToggle();
      TestGSMDriverM$GsmControl$start();
      break;
      case TestGSMDriverM$READY: 
        TestGSMDriverM$Leds$yellowToggle();
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 143
        TestGSMDriverM$state = TestGSMDriverM$GET_SIG_QUAL;
#line 143
        __nesc_atomic_end(__nesc_atomic); }
      TestGSMDriverM$GSM_Modem$getSignalQuality();
      break;
      case TestGSMDriverM$GET_SIG_QUAL: 
        TestGSMDriverM$Leds$yellowToggle();
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 148
        TestGSMDriverM$state = TestGSMDriverM$GET_CELL_MON;
#line 148
        __nesc_atomic_end(__nesc_atomic); }
      TestGSMDriverM$GSM_Modem$getCellMonitorReport();
      break;
      case TestGSMDriverM$GET_CELL_MON: 
        TestGSMDriverM$Leds$yellowToggle();
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 153
        TestGSMDriverM$state = TestGSMDriverM$SEND_SMS;
#line 153
        __nesc_atomic_end(__nesc_atomic); }
      TestGSMDriverM$GSM_Modem$sendSMS("+14026795568", "Testing GSMDriver");
      break;
      case TestGSMDriverM$SEND_SMS: 
        TestGSMDriverM$Leds$yellowToggle();
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 158
        TestGSMDriverM$state = TestGSMDriverM$NONE;
#line 158
        __nesc_atomic_end(__nesc_atomic); }
      TestGSMDriverM$GsmControl$stop();
      TestGSMDriverM$Leds$set(0);
      break;
      default: 
        break;
    }
}

#line 129
static inline result_t TestGSMDriverM$Timer$fired(void )
#line 129
{
  TestGSMDriverM$transition();
  return SUCCESS;
}

# 716 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static inline result_t GSMDriverM$WatchdogTimer$fired(void )
{


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 720
    GSMDriverM$gsm_error_code = WATCHDOG_TIMED_OUT;
#line 720
    __nesc_atomic_end(__nesc_atomic); }
  TOS_post(GSMDriverM$stopDoneTask);
  return SUCCESS;
}

#line 638
static inline result_t GSMDriverM$CommandTimer$fired(void )
{
  uint8_t localState;
  bool triedMaxNumTimes;

#line 642
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 642
    localState = GSMDriverM$currentCmdState;
#line 642
    __nesc_atomic_end(__nesc_atomic); }
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 643
    triedMaxNumTimes = GSMDriverM$commandRetryCount >= 3;
#line 643
    __nesc_atomic_end(__nesc_atomic); }

  if (triedMaxNumTimes) 
    {
      switch (localState) 
        {
          case GSMDriverM$GSM_SET_CELL_MONITOR_RPT_VAL: 
            { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 650
              GSMDriverM$gsm_error_code = NO_MODEM_RESPONSE;
#line 650
              __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$setCellMonitorReportValueFailTask);
          break;
          case GSMDriverM$GSM_GET_CELL_MONITOR_RPT: 
            { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 654
              GSMDriverM$gsm_error_code = NO_MODEM_RESPONSE;
#line 654
              __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$cellMonitorReportFailTask);
          break;
          case GSMDriverM$GSM_GET_SIGNAL_QUALITY: 
            { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 658
              GSMDriverM$gsm_error_code = NO_MODEM_RESPONSE;
#line 658
              __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$signalQualityDataFailTask);
          break;
          case GSMDriverM$SMS_REQUEST_PROMPT: 
            { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 662
              GSMDriverM$gsm_error_code = SMS_PROMPT_NOT_RECVD;
#line 662
              __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$sendSMSFailTask);
          break;
          case GSMDriverM$SMS_SEND_PAYLOAD: 
            { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 666
              GSMDriverM$gsm_error_code = NO_MODEM_RESPONSE;
#line 666
              __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$sendSMSFailTask);
          break;
          default: 

            { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 671
              GSMDriverM$gsm_error_code = NO_MODEM_RESPONSE;
#line 671
              __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$startFailTask);
          break;
        }
    }
  else 
    {
      result_t status;

      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 680
        GSMDriverM$commandRetryCount++;
#line 680
        __nesc_atomic_end(__nesc_atomic); }

      status = GSMDriverM$sendCurrentCommand();
      if (status == FAIL) 
        {
          GSMDriverM$CommandTimer$stop();
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 686
            GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
#line 686
            __nesc_atomic_end(__nesc_atomic); }
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 687
            GSMDriverM$currentCmdState = GSMDriverM$NONE;
#line 687
            __nesc_atomic_end(__nesc_atomic); }
          switch (localState) 
            {
              case GSMDriverM$GSM_SET_CELL_MONITOR_RPT_VAL: 
                TOS_post(GSMDriverM$setCellMonitorReportValueFailTask);
              break;
              case GSMDriverM$GSM_GET_CELL_MONITOR_RPT: 
                TOS_post(GSMDriverM$cellMonitorReportFailTask);
              break;
              case GSMDriverM$GSM_GET_SIGNAL_QUALITY: 
                TOS_post(GSMDriverM$signalQualityDataFailTask);
              break;
              case GSMDriverM$SMS_REQUEST_PROMPT: 
                TOS_post(GSMDriverM$sendSMSFailTask);
              break;
              case GSMDriverM$SMS_SEND_PAYLOAD: 
                TOS_post(GSMDriverM$sendSMSFailTask);
              break;
              default: 

                TOS_post(GSMDriverM$startFailTask);
              break;
            }
        }
    }
  return SUCCESS;
}

#line 278
static inline void GSMDriverM$gsmConfigurationDoneTask(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 280
    GSMDriverM$currentCmdState = GSMDriverM$NONE;
#line 280
    __nesc_atomic_end(__nesc_atomic); }
  GSMDriverM$isConfigDone = TRUE;


  if (GSMDriverM$isAssociated) 
    {
      GSMDriverM$isStartDone = TRUE;
      TOS_post(GSMDriverM$startDoneTask);
    }
}

# 99 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_PW1_PIN()
#line 99
{
#line 99
  * (volatile uint8_t *)(0x08 + 0x20) &= ~(1 << 1);
}

# 190 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static __inline void GSMDriverM$disablePowerLine(void )
{

  TOSH_CLR_PW1_PIN();
}

# 99 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_SET_PW1_PIN()
#line 99
{
#line 99
  * (volatile uint8_t *)(0x08 + 0x20) |= 1 << 1;
}

# 181 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static __inline void GSMDriverM$enablePowerLine(void )
{

  TOSH_SET_PW1_PIN();
}

# 98 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_SET_PW0_PIN()
#line 98
{
#line 98
  * (volatile uint8_t *)(0x08 + 0x20) |= 1 << 0;
}

# 164 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static __inline void GSMDriverM$enableGroundLine(void )
{

  TOSH_SET_PW0_PIN();
}

#line 501
static __inline void GSMDriverM$transition(void )
{
  result_t status;
  uint8_t localState;

#line 505
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 505
    localState = GSMDriverM$currentCmdState;
#line 505
    __nesc_atomic_end(__nesc_atomic); }
  switch (localState) 
    {
      case GSMDriverM$GSM_START_READY: 


        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 511
          GSMDriverM$currentCmdState = GSMDriverM$GSM_VCC_ENABLE;
#line 511
          __nesc_atomic_end(__nesc_atomic); }
      GSMDriverM$enableGroundLine();
      GSMDriverM$Timer$start(TIMER_ONE_SHOT, 50);
      break;
      case GSMDriverM$GSM_VCC_ENABLE: 

        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 517
          GSMDriverM$currentCmdState = GSMDriverM$GSM_POWER_ON_PHASE1;
#line 517
          __nesc_atomic_end(__nesc_atomic); }
      GSMDriverM$enablePowerLine();

      GSMDriverM$Timer$start(TIMER_ONE_SHOT, 2000);
      break;
      case GSMDriverM$GSM_POWER_ON_PHASE1: 

        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 524
          GSMDriverM$currentCmdState = GSMDriverM$GSM_POWER_ON_PHASE2;
#line 524
          __nesc_atomic_end(__nesc_atomic); }


      GSMDriverM$disablePowerLine();



      GSMDriverM$Timer$start(TIMER_ONE_SHOT, 5000);
      break;
      case GSMDriverM$GSM_POWER_ON_PHASE2: 


        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 536
          GSMDriverM$currentCmdState = GSMDriverM$GSM_SET_BAUD_RATE;
#line 536
          __nesc_atomic_end(__nesc_atomic); }


      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 539
        GSMDriverM$CURRENT_COMMAND = GSMDriverM$SET_BAUD_RATE_CMD;
#line 539
        __nesc_atomic_end(__nesc_atomic); }
      status = GSMDriverM$sendCurrentCommand();
      if (status == FAIL) 
        {


          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 545
            GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
#line 545
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$startFailTask);
        }
      break;
      case GSMDriverM$GSM_SET_BAUD_RATE: 

        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 551
          GSMDriverM$currentCmdState = GSMDriverM$GSM_SET_REGISTER_WITH_EXTRA_INFO;
#line 551
          __nesc_atomic_end(__nesc_atomic); }
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 552
        GSMDriverM$CURRENT_COMMAND = GSMDriverM$SET_REGISTER_MORE_INFO_CMD;
#line 552
        __nesc_atomic_end(__nesc_atomic); }
      status = GSMDriverM$sendCurrentCommand();
      if (status == FAIL) 
        {

          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 557
            GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
#line 557
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$startFailTask);
        }
      break;
      case GSMDriverM$GSM_SET_REGISTER_WITH_EXTRA_INFO: 

        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 563
          GSMDriverM$currentCmdState = GSMDriverM$GSM_SET_VERBOSE_MODE;
#line 563
          __nesc_atomic_end(__nesc_atomic); }
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 564
        GSMDriverM$CURRENT_COMMAND = GSMDriverM$SET_VERBOSE_MODE_CMD;
#line 564
        __nesc_atomic_end(__nesc_atomic); }
      status = GSMDriverM$sendCurrentCommand();
      if (status == FAIL) 
        {

          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 569
            GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
#line 569
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$startFailTask);
        }
      break;
      case GSMDriverM$GSM_SET_VERBOSE_MODE: 

        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 575
          GSMDriverM$currentCmdState = GSMDriverM$GSM_SET_NORTH_AMERICA_BAND;
#line 575
          __nesc_atomic_end(__nesc_atomic); }
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 576
        GSMDriverM$CURRENT_COMMAND = GSMDriverM$SET_NORTH_AMERICA_BAND_CMD;
#line 576
        __nesc_atomic_end(__nesc_atomic); }
      status = GSMDriverM$sendCurrentCommand();
      if (status == FAIL) 
        {

          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 581
            GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
#line 581
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$startFailTask);
        }
      break;
      case GSMDriverM$GSM_SET_NORTH_AMERICA_BAND: 

        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 587
          GSMDriverM$currentCmdState = GSMDriverM$GSM_SET_ENERGY_SAVE_MODE;
#line 587
          __nesc_atomic_end(__nesc_atomic); }
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 588
        GSMDriverM$CURRENT_COMMAND = GSMDriverM$SET_ENERGY_SAVE_CMD;
#line 588
        __nesc_atomic_end(__nesc_atomic); }
      status = GSMDriverM$sendCurrentCommand();
      if (status == FAIL) 
        {

          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 593
            GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
#line 593
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$startFailTask);
        }
      break;
      case GSMDriverM$GSM_SET_ENERGY_SAVE_MODE: 

        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 599
          GSMDriverM$currentCmdState = GSMDriverM$GSM_SET_TEXT_FORMAT;
#line 599
          __nesc_atomic_end(__nesc_atomic); }
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 600
        GSMDriverM$CURRENT_COMMAND = GSMDriverM$SET_TEXT_FORMAT_CMD;
#line 600
        __nesc_atomic_end(__nesc_atomic); }
      status = GSMDriverM$sendCurrentCommand();
      if (status == FAIL) 
        {

          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 605
            GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
#line 605
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$startFailTask);
        }
      break;
      case GSMDriverM$GSM_SET_TEXT_FORMAT: 
        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 610
          GSMDriverM$currentCmdState = GSMDriverM$GSM_CHECK_IF_ASSOC;
#line 610
          __nesc_atomic_end(__nesc_atomic); }
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 611
        GSMDriverM$CURRENT_COMMAND = GSMDriverM$CHECK_IF_ASSOC_CMD;
#line 611
        __nesc_atomic_end(__nesc_atomic); }
      status = GSMDriverM$sendCurrentCommand();
      if (status == FAIL) 
        {
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 615
            GSMDriverM$gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
#line 615
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(GSMDriverM$startFailTask);
        }
      break;
      case GSMDriverM$GSM_CHECK_IF_ASSOC: 



        TOS_post(GSMDriverM$gsmConfigurationDoneTask);
      break;
      default: 
        break;
    }
}

static inline result_t GSMDriverM$Timer$fired(void )
{

  GSMDriverM$transition();
  return SUCCESS;
}

# 164 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline result_t TimerM$Timer$default$fired(uint8_t id)
#line 164
{
  return SUCCESS;
}

# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
inline static result_t TimerM$Timer$fired(uint8_t arg_0x1016511a0){
#line 51
  unsigned char __nesc_result;
#line 51

#line 51
  switch (arg_0x1016511a0) {
#line 51
    case 0U:
#line 51
      __nesc_result = TestGSMDriverM$Timer$fired();
#line 51
      break;
#line 51
    case 1U:
#line 51
      __nesc_result = GSMDriverM$Timer$fired();
#line 51
      break;
#line 51
    case 2U:
#line 51
      __nesc_result = GSMDriverM$CommandTimer$fired();
#line 51
      break;
#line 51
    case 3U:
#line 51
      __nesc_result = GSMDriverM$WatchdogTimer$fired();
#line 51
      break;
#line 51
    default:
#line 51
      __nesc_result = TimerM$Timer$default$fired(arg_0x1016511a0);
#line 51
      break;
#line 51
    }
#line 51

#line 51
  return __nesc_result;
#line 51
}
#line 51
# 176 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline uint8_t TimerM$dequeue(void )
#line 176
{
  if (TimerM$queue_size == 0) {
    return NUM_TIMERS;
    }
#line 179
  if (TimerM$queue_head == NUM_TIMERS - 1) {
    TimerM$queue_head = -1;
    }
#line 181
  TimerM$queue_head++;
  TimerM$queue_size--;
  return TimerM$queue[(uint8_t )TimerM$queue_head];
}

static inline void TimerM$signalOneTimer(void )
#line 186
{
  uint8_t itimer = TimerM$dequeue();

#line 188
  if (itimer < NUM_TIMERS) {
    TimerM$Timer$fired(itimer);
    }
}

#line 192
static inline void TimerM$HandleFire(void )
#line 192
{
  uint8_t i;
  uint16_t int_out;


  TimerM$setIntervalFlag = 1;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 200
    {
      int_out = TimerM$interval_outstanding;
      TimerM$interval_outstanding = 0;
    }
#line 203
    __nesc_atomic_end(__nesc_atomic); }
  if (TimerM$mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM$mState & (0x1L << i)) {
              TimerM$mTimerList[i].ticksLeft -= int_out;
              if (TimerM$mTimerList[i].ticksLeft <= 2) {


                  if (TOS_post(TimerM$signalOneTimer)) {
                      if (TimerM$mTimerList[i].type == TIMER_REPEAT) {
                          TimerM$mTimerList[i].ticksLeft += TimerM$mTimerList[i].ticks;
                        }
                      else 
#line 214
                        {
                          TimerM$mState &= ~(0x1L << i);
                        }
                      TimerM$enqueue(i);
                    }
                  else {
                      {
                      }
#line 220
                      ;


                      TimerM$mTimerList[i].ticksLeft = TimerM$mInterval;
                    }
                }
            }
        }
    }


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 231
    int_out = TimerM$interval_outstanding;
#line 231
    __nesc_atomic_end(__nesc_atomic); }
  if (int_out == 0) {
    TimerM$adjustInterval();
    }
}

static inline result_t TimerM$Clock$fire(void )
#line 237
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 238
    {



      if (TimerM$interval_outstanding == 0) {
          TOS_post(TimerM$HandleFire);
        }
      else 
        {
        }
#line 246
      ;

      TimerM$interval_outstanding += TimerM$Clock$getInterval() + 1;


      add_time_millis(TimerM$Clock$getInterval());
    }
#line 252
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static result_t HPLClock$Clock$fire(void ){
#line 159
  unsigned char __nesc_result;
#line 159

#line 159
  __nesc_result = TimerM$Clock$fire();
#line 159

#line 159
  return __nesc_result;
#line 159
}
#line 159
# 89 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
static inline result_t HPLUART0M$UART$stop(void )
#line 89
{
  * (volatile uint8_t *)0xC0 = 0x00;
  * (volatile uint8_t *)0XC1 = 0x00;
  * (volatile uint8_t *)0xC2 = 0x00;
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLUART.nc"
inline static result_t UART0M$HPLUART$stop(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = HPLUART0M$UART$stop();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 52 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UART0M.nc"
static inline result_t UART0M$Control$stop(void )
#line 52
{

  return UART0M$HPLUART$stop();
}

# 56 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t UARTPacket$ByteControl$stop(void ){
#line 56
  unsigned char __nesc_result;
#line 56

#line 56
  __nesc_result = UART0M$Control$stop();
#line 56

#line 56
  return __nesc_result;
#line 56
}
#line 56
# 151 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static inline result_t UARTPacket$Control$stop(void )
#line 151
{
  return UARTPacket$ByteControl$stop();
}

# 56 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t GsmUartHandlerM$UARTControl$stop(void ){
#line 56
  unsigned char __nesc_result;
#line 56

#line 56
  __nesc_result = UARTPacket$Control$stop();
#line 56

#line 56
  return __nesc_result;
#line 56
}
#line 56
# 611 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static inline void GsmUartHandlerM$stopTask(void )
{

  GsmUartHandlerM$UARTControl$stop();
}

#line 604
static inline result_t GsmUartHandlerM$StdControl$stop(void )
{

  TOS_post(GsmUartHandlerM$stopTask);
  return SUCCESS;
}

# 56 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t GSMDriverM$UARTControl$stop(void ){
#line 56
  unsigned char __nesc_result;
#line 56

#line 56
  __nesc_result = GsmUartHandlerM$StdControl$stop();
#line 56

#line 56
  return __nesc_result;
#line 56
}
#line 56
# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
inline static result_t GSMDriverM$Timer$stop(void ){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = TimerM$Timer$stop(1U);
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
inline static result_t GSMDriverM$WatchdogTimer$stop(void ){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = TimerM$Timer$stop(3U);
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 98 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_PW0_PIN()
#line 98
{
#line 98
  * (volatile uint8_t *)(0x08 + 0x20) &= ~(1 << 0);
}

# 172 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static __inline void GSMDriverM$disableGroundLine(void )
{

  TOSH_CLR_PW0_PIN();
}

# 85 "TestGSMDriverM.nc"
static inline result_t TestGSMDriverM$GsmControl$stopDone(gsm_error_t result)
#line 85
{
  TestGSMDriverM$state = TestGSMDriverM$STARTING;
  TestGSMDriverM$Timer$start(TIMER_ONE_SHOT, 3000);
  return SUCCESS;
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/SplitControlStatus.nc"
inline static result_t GSMDriverM$SplitControlStatus$stopDone(gsm_error_t result){
#line 53
  unsigned char __nesc_result;
#line 53

#line 53
  __nesc_result = TestGSMDriverM$GsmControl$stopDone(result);
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 117 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$yellowOff(void )
#line 117
{
  {
  }
#line 118
  ;
  /* atomic removed: atomic calls only */
#line 119
  {
    TOSH_SET_YELLOW_LED_PIN();
    LedsC$ledsOn &= ~LedsC$YELLOW_BIT;
  }
  return SUCCESS;
}

#line 108
static inline result_t LedsC$Leds$yellowOn(void )
#line 108
{
  {
  }
#line 109
  ;
  /* atomic removed: atomic calls only */
#line 110
  {
    TOSH_CLR_YELLOW_LED_PIN();
    LedsC$ledsOn |= LedsC$YELLOW_BIT;
  }
  return SUCCESS;
}

# 188 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiM.nc"
static inline void LowLevelTwiM$twi_reply(uint8_t ack)
{

  if (ack) {
      * (volatile uint8_t *)0xBC = (((1 << 2) | (1 << 0)) | (1 << 7)) | (1 << 6);
    }
  else 
#line 193
    {
      * (volatile uint8_t *)0xBC = ((1 << 2) | (1 << 0)) | (1 << 7);
    }
}

#line 225
static inline void LowLevelTwiM$twi_releaseBus(void )
{

  * (volatile uint8_t *)0xBC = (((1 << 2) | (1 << 0)) | (1 << 6)) | (1 << 7);


  LowLevelTwiM$twi_state = 0;
}

# 170 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
static __inline void __nesc_disable_interrupt()
#line 170
{
   __asm volatile ("cli");}

# 82 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
 bool TOS_post(void (*tp)())
#line 82
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t tmp;



  fInterruptFlags = __nesc_atomic_start();

  tmp = TOSH_sched_free;

  if (TOSH_queue[tmp].tp == (void *)0) {
      TOSH_sched_free = (tmp + 1) & TOSH_TASK_BITMASK;
      TOSH_queue[tmp].tp = tp;
      __nesc_atomic_end(fInterruptFlags);

      return TRUE;
    }
  else {
      __nesc_atomic_end(fInterruptFlags);

      return FALSE;
    }
}

# 34 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
  int main(void )
#line 34
{


  uint8_t local_symbol_ref;

  local_symbol_ref = TOS_PLATFORM;
  local_symbol_ref = TOS_BASE_STATION;
  local_symbol_ref = TOS_DATA_LENGTH;

  local_symbol_ref = TOS_ROUTE_PROTOCOL;








  RealMain$hardwareInit();
  RealMain$Pot$init(10);
  TOSH_sched_init();

  RealMain$StdControl$init();
  RealMain$StdControl$start();
  __nesc_enable_interrupt();

  while (1) {
      TOSH_run_task();
    }
}

# 52 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static void DebugI2CM$printBufferTask(void )
#line 52
{
  static uint16_t i;





  DebugI2CM$HighLevelTwiInterface$beginTransmission(42 >> 1);
  for (i = 0; i < 32; i++) {
      if (twi_buffer_empty() == 1) {
          DebugI2CM$HighLevelTwiInterface$endTransmission();
          return;
        }
      else {
          DebugI2CM$HighLevelTwiInterface$send(twi_pull_char());
        }
    }
  DebugI2CM$HighLevelTwiInterface$endTransmission();
  if (twi_buffer_empty() == 0) {
      TOS_post(DebugI2CM$printBufferTask);
    }
}

# 65 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiM.nc"
static uint8_t HighLevelTwiM$HighLevelTwiInterface$endTransmission(void )
{

  int8_t ret = HighLevelTwiM$LowLevelTwiInterface$twi_writeTo(HighLevelTwiM$txAddress, HighLevelTwiM$txBuffer, HighLevelTwiM$txBufferLength, 1);

  HighLevelTwiM$txBufferIndex = 0;
  HighLevelTwiM$txBufferLength = 0;

  HighLevelTwiM$transmitting = 0;
  return ret;
}

# 36 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static result_t LedsC$Leds$init(void )
#line 36
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 37
    {
      LedsC$ledsOn = 0;
      {
      }
#line 39
      ;
      TOSH_MAKE_RED_LED_OUTPUT();
      TOSH_MAKE_YELLOW_LED_OUTPUT();
      TOSH_MAKE_GREEN_LED_OUTPUT();
      TOSH_SET_RED_LED_PIN();
      TOSH_SET_YELLOW_LED_PIN();
      TOSH_SET_GREEN_LED_PIN();
    }
#line 46
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 80 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static result_t TimerM$Timer$start(uint8_t id, char type, 
uint32_t interval)
#line 81
{
  uint8_t diff;

#line 83
  if (id >= NUM_TIMERS) {
#line 83
    return FAIL;
    }
#line 84
  if (type > TIMER_ONE_SHOT) {
#line 84
    return FAIL;
    }





  if (type == TIMER_REPEAT && interval <= 2) {
#line 91
    return FAIL;
    }
  TimerM$mTimerList[id].ticks = interval;
  TimerM$mTimerList[id].type = type;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 96
    {
      diff = TimerM$Clock$readCounter();
      interval += diff;
      TimerM$mTimerList[id].ticksLeft = interval;
      TimerM$mState |= 0x1L << id;
      if (interval < TimerM$mInterval) {
          TimerM$mInterval = interval;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
          TimerM$PowerManagement$adjustPower();
        }
    }
#line 107
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 134 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/HPLPowerManagementM.nc"
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void )
#line 134
{
  uint8_t mcu;

#line 136
  if (!HPLPowerManagementM$disabled) {
    TOS_post(HPLPowerManagementM$doAdjustment);
    }
  else 
#line 138
    {
      mcu = * (volatile uint8_t *)(0x33 + 0x20);
      mcu &= 0xf1;
      mcu |= 0;
      * (volatile uint8_t *)(0x33 + 0x20) = mcu;
      * (volatile uint8_t *)(0x33 + 0x20) |= 1 << 0;
    }
  return 0;
}

# 98 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
  __attribute((signal)) void __vector_25(void )
#line 98
{
  if (* (volatile uint8_t *)0xC0 & (1 << 7)) {
    HPLUART0M$UART$get(* (volatile uint8_t *)0XC6);
    }
}

# 737 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static void GSMDriverM$GsmUartHandler$ErrorReceived(gsm_error_t errorCode)
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 739
    GSMDriverM$gsm_error_code = errorCode;
#line 739
    __nesc_atomic_end(__nesc_atomic); }
  TOS_post(GSMDriverM$ErrorReceivedTask);
  return;
}

# 150 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static result_t TimerM$Timer$stop(uint8_t id)
#line 150
{

  if (id >= NUM_TIMERS) {
#line 152
    return FAIL;
    }
#line 153
  if (TimerM$mState & (0x1L << id)) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 154
        TimerM$mState &= ~(0x1L << id);
#line 154
        __nesc_atomic_end(__nesc_atomic); }
      if (!TimerM$mState) {
          TimerM$setIntervalFlag = 1;
        }
      return SUCCESS;
    }
  return FAIL;
}

# 889 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static void GSMDriverM$setCellMonitorReportValueFailTask(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      GSMDriverM$currentCmdState = GSMDriverM$NONE;
      GSMDriverM$CURRENT_COMMAND = (void *)0;
      GSMDriverM$commandRetryCount = 0;
      GSMDriverM$GSM_Modem$setCellMonitorReportValueDone(GSMDriverM$gsm_error_code);
    }
#line 897
    __nesc_atomic_end(__nesc_atomic); }
}








static void GSMDriverM$cellMonitorReportFailTask(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      GSMDriverM$currentCmdState = GSMDriverM$NONE;
      GSMDriverM$CURRENT_COMMAND = (void *)0;
      GSMDriverM$commandRetryCount = 0;
      GSMDriverM$GSM_Modem$cellMonitorReportReady(GSMDriverM$gsm_error_code, (void *)0);
    }
#line 915
    __nesc_atomic_end(__nesc_atomic); }
}







static void GSMDriverM$signalQualityDataFailTask(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      GSMDriverM$currentCmdState = GSMDriverM$NONE;
      GSMDriverM$CURRENT_COMMAND = (void *)0;
      GSMDriverM$commandRetryCount = 0;
      GSMDriverM$GSM_Modem$signalQualityReady(GSMDriverM$gsm_error_code, (void *)0);
    }
#line 932
    __nesc_atomic_end(__nesc_atomic); }
}







static void GSMDriverM$sendSMSFailTask(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      GSMDriverM$currentCmdState = GSMDriverM$NONE;
      GSMDriverM$CURRENT_COMMAND = (void *)0;
      GSMDriverM$commandRetryCount = 0;
      GSMDriverM$GSM_Modem$sendSMSDone(GSMDriverM$gsm_error_code, GSMDriverM$currentPhoneNumber, GSMDriverM$currentTextData);
    }
#line 949
    __nesc_atomic_end(__nesc_atomic); }
}

# 119 "TestGSMDriverM.nc"
static result_t TestGSMDriverM$GSM_Modem$sendSMSDone(gsm_error_t result, uint8_t *endpoint, uint8_t *data)
#line 119
{
  if (result == NO_ERROR) {
      TestGSMDriverM$Timer$start(TIMER_ONE_SHOT, 500);
    }
  else {
      TestGSMDriverM$Timer$start(TIMER_ONE_SHOT, 500);
    }
  return SUCCESS;
}

# 667 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GsmUartHandlerM.nc"
static result_t GsmUartHandlerM$GsmUartHandler$sendCommand(char *cmd, uint8_t length)
{
  char tcmd[256];
  char *sp;
  char *tok;

  strcpy(tcmd, cmd);


  if (strstr(tcmd, "#MONI=") != (void *)0) 
    {


      tok = strtok_r(tcmd, "=", &sp);
      GsmUartHandlerM$gsmCellMonParamVal = atoi(tok);

      if (GsmUartHandlerM$gsmCellMonParamVal == 0) {
          return GsmUartHandlerM$SendVarLenPacketGsm$send((uint8_t *)cmd, length);
        }
      else {
          TOS_post(GsmUartHandlerM$handleSendMoniFail);
          return FAIL;
        }
    }
  else {
      return GsmUartHandlerM$SendVarLenPacketGsm$send((uint8_t *)cmd, length);
    }
}

# 158 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static result_t UARTPacket$txBytes(uint8_t *bytes, uint8_t numBytes)
#line 158
{
  /* atomic removed: atomic calls only */
  {
    if (UARTPacket$txCount == 0) 
      {
        UARTPacket$txCount = 1;
        UARTPacket$txLength = numBytes;
        UARTPacket$sendPtr = bytes;

        if (UARTPacket$ByteComm$txByte(UARTPacket$sendPtr[0])) 
          {
            unsigned char __nesc_temp = 
#line 168
            SUCCESS;

#line 168
            return __nesc_temp;
          }
        else {
#line 170
          UARTPacket$txCount = 0;
          }
      }
  }
#line 173
  return FAIL;
}

# 90 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UART0M.nc"
static result_t UART0M$ByteComm$txByte(uint8_t data)
#line 90
{
  bool oldState;

  {
  }
#line 93
  ;
  /* atomic removed: atomic calls only */
  {
    oldState = UART0M$state;
    UART0M$state = TRUE;
  }
  if (oldState) {
    return FAIL;
    }
  UART0M$HPLUART$put(data);

  return SUCCESS;
}

# 187 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static void *nmemcpy(void *to, const void *from, size_t n)
{
  char *cto = to;
  const char *cfrom = from;

  while (n--) * cto++ = * cfrom++;

  return to;
}

# 50 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static result_t LedsC$Leds$redOn(void )
#line 50
{
  {
  }
#line 51
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 52
    {
      TOSH_CLR_RED_LED_PIN();
      LedsC$ledsOn |= LedsC$RED_BIT;
    }
#line 55
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 108 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLUART0M.nc"
  __attribute((interrupt)) void __vector_27(void )
#line 108
{
  HPLUART0M$UART$putDone();
}

# 203 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/UARTPacket.nc"
static void UARTPacket$sendComplete(result_t success)
#line 203
{
  /* atomic removed: atomic calls only */
#line 204
  {
    if (UARTPacket$state == UARTPacket$BYTES) {
        if (success) {
            TOS_post(UARTPacket$sendVarLenSuccessTask);
          }
        else {
            TOS_post(UARTPacket$sendVarLenFailTask);
          }
      }
    else {
        UARTPacket$txCount = 0;
        UARTPacket$state = UARTPacket$IDLE;
      }
  }
}

# 164 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
  __attribute((interrupt)) void __vector_13(void )
#line 164
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 165
    {
      if (HPLClock$set_flag) {
          HPLClock$mscale = HPLClock$nextScale;
          * (volatile uint8_t *)0xB1 = HPLClock$nextScale;
          * (volatile uint8_t *)0xB3 = HPLClock$minterval;
          HPLClock$set_flag = 0;
        }
    }
#line 172
    __nesc_atomic_end(__nesc_atomic); }
  HPLClock$Clock$fire();
}

# 263 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/GSMDriverM.nc"
static void GSMDriverM$startFailTask(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      GSMDriverM$CommandTimer$stop();
      GSMDriverM$currentCmdState = GSMDriverM$NONE;
      GSMDriverM$CURRENT_COMMAND = (void *)0;
      GSMDriverM$isAssociated = FALSE;
      GSMDriverM$isStartDone = FALSE;
      GSMDriverM$isConfigDone = FALSE;
      GSMDriverM$commandRetryCount = 0;
      GSMDriverM$SplitControlStatus$startDone(GSMDriverM$gsm_error_code);
    }
#line 275
    __nesc_atomic_end(__nesc_atomic); }
}

#line 304
static void GSMDriverM$stopDoneTask(void )
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {

      GSMDriverM$UARTControl$stop();
      GSMDriverM$Timer$stop();
      GSMDriverM$CommandTimer$stop();
      GSMDriverM$WatchdogTimer$stop();
      GSMDriverM$disableGroundLine();
      GSMDriverM$disablePowerLine();
      GSMDriverM$currentCmdState = GSMDriverM$NONE;
      GSMDriverM$CURRENT_COMMAND = (void *)0;
      GSMDriverM$isAssociated = FALSE;
      GSMDriverM$isStartDone = FALSE;
      GSMDriverM$isConfigDone = FALSE;
      GSMDriverM$commandRetryCount = 0;
      GSMDriverM$SplitControlStatus$stopDone(GSMDriverM$gsm_error_code);
    }
#line 322
    __nesc_atomic_end(__nesc_atomic); }
}

# 126 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static result_t LedsC$Leds$yellowToggle(void )
#line 126
{
  result_t rval;

#line 128
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 128
    {
      if (LedsC$ledsOn & LedsC$YELLOW_BIT) {
        rval = LedsC$Leds$yellowOff();
        }
      else {
#line 132
        rval = LedsC$Leds$yellowOn();
        }
    }
#line 134
    __nesc_atomic_end(__nesc_atomic); }
#line 134
  return rval;
}

# 234 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiM.nc"
  __attribute((signal)) void __vector_39(void )
{
  switch (* (volatile uint8_t *)0xB9 & (((((1 << 7) | (1 << 6)) | (1 << 5)) | (1 << 4)) | (1 << 3))) {

      case 0x08: 
        case 0x10: 

          * (volatile uint8_t *)0xBB = LowLevelTwiM$twi_slarw;
      LowLevelTwiM$twi_reply(1);
      break;


      case 0x18: 
        case 0x28: 

          if (LowLevelTwiM$twi_masterBufferIndex < LowLevelTwiM$twi_masterBufferLength) {

              * (volatile uint8_t *)0xBB = LowLevelTwiM$twi_masterBuffer[LowLevelTwiM$twi_masterBufferIndex++];
              LowLevelTwiM$twi_reply(1);
            }
          else 
#line 253
            {
              LowLevelTwiM$twi_stop();
            }
      break;
      case 0x20: 
        LowLevelTwiM$twi_error = 0x20;
      LowLevelTwiM$twi_stop();
      break;
      case 0x30: 
        LowLevelTwiM$twi_error = 0x30;
      LowLevelTwiM$twi_stop();
      break;
      case 0x38: 
        LowLevelTwiM$twi_error = 0x38;
      LowLevelTwiM$twi_releaseBus();
      break;


      case 0x50: 

        LowLevelTwiM$twi_masterBuffer[LowLevelTwiM$twi_masterBufferIndex++] = * (volatile uint8_t *)0xBB;
      case 0x40: 

        if (LowLevelTwiM$twi_masterBufferIndex < LowLevelTwiM$twi_masterBufferLength) {
            LowLevelTwiM$twi_reply(1);
          }
        else 
#line 278
          {
            LowLevelTwiM$twi_reply(0);
          }
      break;
      case 0x58: 

        LowLevelTwiM$twi_masterBuffer[LowLevelTwiM$twi_masterBufferIndex++] = * (volatile uint8_t *)0xBB;
      case 0x48: 
        LowLevelTwiM$twi_stop();
      break;



      case 0xF8: 
        break;
      case 0x00: 
        LowLevelTwiM$twi_error = 0x00;
      LowLevelTwiM$twi_stop();
      break;
    }
}

#line 204
static void LowLevelTwiM$twi_stop(void )
{

  * (volatile uint8_t *)0xBC = ((((1 << 2) | (1 << 0)) | (1 << 6)) | (1 << 7)) | (1 << 4);



  while (* (volatile uint8_t *)0xBC & (1 << 4)) {
      continue;
    }


  LowLevelTwiM$twi_state = 0;
}


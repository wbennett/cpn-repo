/*
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
#include <stdio.h>
#include "Tracker/gsm.h"
module TestGSMDriverM 
{
  provides 
	{
		interface StdControl;
  }
  uses 
	{
		#ifdef I2CDBG
		I2CDBG_CONFIG();
		#endif
    interface SplitControlStatus as GsmControl;
		interface GSM_ModemI as GSM_Modem;
		interface StdControl as TimerControl;
		interface Timer;
		interface Leds;
  }
}
implementation 
{

	extern int sprintf(char *__s, const char *__fmt,...)__attribute__((C));
	
	uint8_t state;
	enum {
		NONE,
		STARTING,
		READY,
		GET_SIG_QUAL,
		GET_CELL_MON,
		SEND_SMS,
	};
	
	void transition();
	
	command result_t StdControl.init()	{
		atomic state = NONE;
		call TimerControl.init();
		call GsmControl.init();
		call Leds.init();
		return SUCCESS;
	}
	
	command result_t StdControl.start() {
		call TimerControl.start();
		return SUCCESS;
	}
	
	command result_t StdControl.stop()	{
		call TimerControl.stop();
		call GsmControl.stop();
		call Timer.stop();
		return SUCCESS;
	}
	
	event result_t GsmControl.initDone() {
		call Leds.greenOn();
		atomic state = STARTING;
		call Timer.start(TIMER_ONE_SHOT,100);
		return SUCCESS;
	}
	
	event result_t GsmControl.startDone(gsm_error_t result) {
		call Leds.redOn();
		/*
		if(result == NO_ERROR) {
			atomic state = READY;
			call Timer.start(TIMER_ONE_SHOT,100);
		}
		else {
		}*/
		return SUCCESS;
	}
	
	event result_t GsmControl.stopDone(gsm_error_t result) {
		state = STARTING;
		call Timer.start(TIMER_ONE_SHOT,3000);
		return SUCCESS;
	}
	
	event result_t GSM_Modem.setCellMonitorReportValueDone(gsm_error_t result) {
		return SUCCESS;
	}

	event result_t GSM_Modem.cellMonitorReportReady(gsm_error_t result, gsm_cellmon_data_t *data) {
		if(result==NO_ERROR) {
		/*		data->towers[0].netname,data->towers[0].bsic,
				data->towers[0].rxQual,data->towers[0].lac,
				data->towers[0].id,data->towers[0].arfcn,
				data->towers[0].dBm,data->towers[0].timadv
			);*/
			call Timer.start(TIMER_ONE_SHOT,500);
		}
		else {
		}
		return SUCCESS;
	}

	event result_t GSM_Modem.signalQualityReady(gsm_error_t result, gsm_signal_quality_data_t *data) {
		if(result==NO_ERROR) {
			
			call Timer.start(TIMER_ONE_SHOT,500);
		}
		else {
		}
		return SUCCESS;
	}
	
	event result_t GSM_Modem.sendSMSDone(gsm_error_t result, uint8_t *endpoint, uint8_t *data) {
		if(result==NO_ERROR) {
			call Timer.start(TIMER_ONE_SHOT,500);
		}
		else {
			call Timer.start(TIMER_ONE_SHOT,500);
		}
		return SUCCESS;
	}
	
	event result_t Timer.fired() {
		transition();
		return SUCCESS;
	}
	
	void transition() {
		uint8_t lstate = state;
		switch(lstate) {
			case STARTING:
				call Leds.yellowToggle();
				call GsmControl.start();
				break;
			case READY:
				call Leds.yellowToggle();
				atomic state = GET_SIG_QUAL;
				call GSM_Modem.getSignalQuality();
				break;
			case GET_SIG_QUAL:
				call Leds.yellowToggle();
				atomic state = GET_CELL_MON;
				call GSM_Modem.getCellMonitorReport();
				break;
			case GET_CELL_MON:
				call Leds.yellowToggle();
				atomic state = SEND_SMS;
				call GSM_Modem.sendSMS("+14026795568","Testing GSMDriver");
				break;
			case SEND_SMS:
				call Leds.yellowToggle();
				atomic state = NONE;
				call GsmControl.stop();
				call Leds.set(0);
				break;
			default:
				break;
		}
	}
	
}



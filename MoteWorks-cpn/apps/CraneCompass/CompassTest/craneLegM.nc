/*
 *
 *
 *
 *
 *
 *
 */


#include "crane.h"

#define SET_COMPASS_10HZ
#define SET_COMPASS_IIR

module craneLegM {
  provides {
    interface StdControl;
  }
  uses {
		interface Compass;
		interface SplitControl as CompassSplitControl;

		interface PowerManagement;
		command result_t Enable();
		command result_t Disable();
		interface Timer;
		interface Leds;
		interface StdControl as RadioControl;
		interface SendMsg;
  }
}
implementation {


  result_t status;
	Compass_Readings compVals;
	uint8_t compIndex;
	uint8_t compByteArr[6];
	char circleMessage[] = "You have circled!";
	TOS_Msg sendMsg;
	OrientationMessage *message;
	bool eventOccurred;
	bool radio_busy;
	bool sample_busy;
	uint8_t mState;
	uint16_t numOfSamples;
	uint32_t mSeqNum;
	#define MAX_COMPASS_SAMPLES (300)
	#define SLEEP_TIME (184320)

	//staties
	enum {
		S_INIT,
		S_SAMPLE_SEND,
		S_SLEEP
	};
 	
	inline void sleep() {
		call PowerManagement.adjustPower();	
	}
	

  command result_t StdControl.init() {
		compIndex=0;
		mSeqNum = 0;
		eventOccurred = FALSE;
		radio_busy = FALSE;
		sample_busy = FALSE;
		mState = S_INIT;
		call RadioControl.init();
    call Leds.init(); 
		return SUCCESS;
  }
  
  command result_t StdControl.start() {
		call CompassSplitControl.init();
		return SUCCESS;
  }
  
  command result_t StdControl.stop() {
		call Timer.stop();
		call RadioControl.stop();
		call CompassSplitControl.stop();
  }

	void transition() {
		uint8_t lstate = mState;
		switch(lstate) {
			case S_INIT:
				mState = S_SAMPLE_SEND;
				call RadioControl.start();
				call CompassSplitControl.start();
				break;
			case S_SAMPLE_SEND:
				mState = S_SLEEP;
				call RadioControl.stop();
				//the sleep method will happen in the stop done
				call CompassSplitControl.stop();
				break;
			case S_SLEEP:
				mState = S_INIT;
				transition();
				break;
			default:
				break;
		}
	}


	/*
	 *
	 *
	 */
 	inline void sampleAndSend() {
		
	  /***Timer will fire FIRST time after smsModeDone()***/
		if(radio_busy == FALSE) {
			call Timer.stop();
			radio_busy = TRUE;
			/********* Read Compass Data *******************/
			call Compass.getHeadings();
			
			/** Pack message->color a little early ***/
			if(compVals.heading >= 3150 || compVals.heading <= 450) {
				message->color = NORTH;
				#ifdef SHOW_DIRECTION
				call Leds.redOn();
				call Leds.greenOn();
				call Leds.yellowOn();
				#endif
			}
			else if (compVals.heading >= 450 && compVals.heading <= 1350) {
				message->color = EAST;
				#ifdef SHOW_DIRECTION
				call Leds.redOn();
				call Leds.greenOff();
				call Leds.yellowOff();
				#endif
			}
			else if (compVals.heading >= 1350 && compVals.heading <= 2250) {
				message->color = SOUTH;
				#ifdef SHOW_DIRECTION
				call Leds.redOff();
				call Leds.greenOn();
				call Leds.yellowOff();
				#endif
			}
			else if (compVals.heading >= 2250 && compVals.heading <= 3150) {
				message->color = WEST;
				#ifdef SHOW_DIRECTION
				call Leds.redOff();
				call Leds.greenOff();
				call Leds.yellowOn();
				#endif
			}
			else {
				message->color = DERROR;
			}
			
			/*********** Send values over the air to base *********/
			message = (OrientationMessage*) sendMsg.data;
			message->seqnum = mSeqNum;
			mSeqNum += 1;
			message->heading = compVals.heading;
			message->pitch = compVals.pitch;
			message->roll = compVals.roll;
			message->accelx = compVals.accelx;
			message->accely = compVals.accely;
			message->accelz = compVals.accelz;
			//already packed message->color
			message->source = TOS_LOCAL_ADDRESS;
			//call Leds.redToggle();
			if(call SendMsg.send(TOS_BCAST_ADDR, sizeof(OrientationMessage), &sendMsg) == FAIL) {
				//start up the time again so we can at least try
				call Timer.start(TIMER_ONE_SHOT,102);	
				call Leds.redToggle();
			}else {
				call Leds.yellowToggle();
			}
		}
	}

 	//evaluate the state machine...
  event result_t Timer.fired() {
    uint8_t lstate = mState;
		switch(lstate) {
			case S_SAMPLE_SEND:
				//we have sampled and completely sent lets do it again
				//send again
				sampleAndSend();
				break;
			case S_SLEEP://we are sleep and woke up
				call Disable();//disable sleep mode
				transition();
				break;
			default:
				call Leds.redToggle();
		}
		return SUCCESS;
  }
	/*
	 *keep
	 */
  event result_t SendMsg.sendDone(TOS_MsgPtr msg, result_t success)
  {
		radio_busy = FALSE;
		if(success == SUCCESS) {
			numOfSamples++;		
			call Leds.greenToggle();
		}else {
			call Leds.redToggle();
		}
   	if(numOfSamples > MAX_COMPASS_SAMPLES) {
			//transition
			transition();
		}else {
			//grab the next sample
			call Timer.start(TIMER_ONE_SHOT,102);
		}
		return SUCCESS;
  }
 	//============COMPASSS============== 
	//new compass methods...
	event void Compass.getHeadingsDone(result_t result, uint16_t *data) {
		compVals.heading = data[0];
		compVals.pitch = data[1];
		compVals.roll = data[2];
		call Compass.getAccels();
	}
	event void Compass.getTiltsDone(result_t result, uint16_t *data) {
		//shouldn't happen...
	}
	event void Compass.getAccelsDone(result_t result, uint16_t *data) {
		compVals.accelx = data[0];
		compVals.accely = data[1];
		compVals.accelz = data[2];
		//we are done now...
		sample_busy = FALSE;
	}
	event void Compass.calibrateDone(result_t result) {

	}
	
	event result_t CompassSplitControl.initDone() {
		transition();	
		return SUCCESS;
	}

	event result_t CompassSplitControl.startDone() {
		//reset the number of samples
		numOfSamples = 0;
		call Timer.start(TIMER_ONE_SHOT,102);
		return SUCCESS;
	}
	event result_t CompassSplitControl.stopDone() {
		/*
		 *
		 *
		 * Go to sleep
		 *
		 */
		call Leds.set(0);
		call Timer.start(TIMER_ONE_SHOT, SLEEP_TIME);
		call Enable();
		sleep();
		return SUCCESS;
	}
}

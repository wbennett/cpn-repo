includes crane;

interface PredictionInterface {

  command void init();

  command bool predict(Compass_Readings * compVals, uint8_t cardinalDir);
  
}


module PredictionM {
  provides {
    interface PredictionInterface;
  }
}
implementation {

  #include "crane.h"
	
	enum {
	  RIGHT_BANK_THRES = 150,
		LEFT_BANK_THRES = -150
	};
	
	enum {
	  NO_STATE,
		CIRCLE_RIGHT,
		CIRCLE_LEFT
	};
	uint8_t state;
	
	enum {
	  RIGHT_BANK,
		LEFT_BANK,
		NO_BANK
	};
	uint8_t oldBankDir;
	uint8_t newBankDir;

  Compass_Readings newCompVals;
	Compass_Readings oldCompVals;
	
	uint16_t numTimesCalled;
	uint16_t MAX_PREDIC_CNT;
	
	uint8_t cardinalCount;
	uint8_t prevCardinal;
	uint8_t curCardinal;
	
	void reset() {
	  numTimesCalled = 0;
		state = NO_STATE;
		cardinalCount = 0;
		return;
	}
	
	command void PredictionInterface.init() {
	  uint16_t temp = UPDATE_PRD/100;
	  MAX_PREDIC_CNT = 600/temp;
	  reset();
		return;
	}


  command bool PredictionInterface.predict(Compass_Readings * compVals, uint8_t cardinalDir) {
	  /*** Increment number of times this function has been called ***/
	  numTimesCalled++;
		
		/***  Copy newCompVals to oldCompVals ***/
	  memcpy(&oldCompVals,&newCompVals,sizeof(Compass_Readings));
		
		/*** Save old variables ***/
		oldBankDir = newBankDir;
		prevCardinal = curCardinal;
		
		/***  Copy compVals passed in to newCompVals  ***/
	  memcpy(&newCompVals,compVals,sizeof(Compass_Readings));
		curCardinal = cardinalDir;
		
		/*** Determine new Bank direction ***/
		if(newCompVals.roll > RIGHT_BANK_THRES) {
		  newBankDir = RIGHT_BANK;
		}
		else if(newCompVals.roll < LEFT_BANK_THRES) {
		  newBankDir = LEFT_BANK;
		}
		else {
		  newBankDir = NO_BANK;
		}
		
		/*** If timeout then no event ***/
		if(numTimesCalled >= MAX_PREDIC_CNT)
		{
		  reset();
		  return FALSE;
		}
		
		switch(state) {
		  case NO_STATE:
			  if(newBankDir == RIGHT_BANK) {
				  state = CIRCLE_RIGHT;
					cardinalCount++;
				}
				else if (newBankDir == LEFT_BANK) {
				  state = CIRCLE_LEFT;
					cardinalCount++;
				}
				break;
				
			case CIRCLE_RIGHT:
			  if(newBankDir == LEFT_BANK) {
				  reset();
				}
			  else if(curCardinal == prevCardinal) {
				  //Do nothing
				}
				else if (curCardinal == (prevCardinal+1) % 4) {
				  cardinalCount++;
				}
				else {
				  reset();
				}
			  break;
			
			case CIRCLE_LEFT:
			  if(newBankDir == RIGHT_BANK) {
				  reset();
				}
			  else if(curCardinal == prevCardinal) {
				  //Do nothing
				}
				else if (curCardinal == (prevCardinal-1) % 4) {
				  cardinalCount++;
				}
				else {
				  reset();
				}
			  break;
		}
		
		if(cardinalCount >= 5) {
		  reset();
		  return TRUE;
		}
		else {		
		  return FALSE;
		}
		
	}
	
}



// $Id: SimApp.nc,v 1.7 2003/10/07 21:44:45 idgay Exp $

/*									tab:4
 */
/**
 **/
configuration SimApp {
}
implementation {
  components Main, SimAppM, /*SingleTimer,*/ LedsC;
//  Main.StdControl -> SingleTimer.StdControl;
  Main.StdControl -> SimAppM.StdControl;
 // SimAppM.Timer -> SingleTimer.Timer;
  SimAppM.Leds -> LedsC;
	
	components UART0_SIM, Pins;
	SimAppM.uart_comm -> UART0_SIM.HPLUART;
	SimAppM.Uart0Control -> UART0_SIM.Control;
	
	SimAppM.PW0 -> Pins.PW0;
}


// $Id: SimAppM.nc,v 1.5 2003/10/07 21:44:45 idgay Exp $

/*									tab:4
 */

/**
 **/
module SimAppM {
  provides {
    interface StdControl;
  }
  uses {
//    interface Timer;
    interface Leds;
		interface HPLUART as uart_comm;
		interface StdControl as Uart0Control;
		interface GeneralIO as PW0;
  }
}
implementation {

  /**
   * Initialize the component.
   * 
   * @return Always returns <code>SUCCESS</code>
   **/
  command result_t StdControl.init() {
    
		//enable the uart...
		call Uart0Control.init();
		call PW0.makeOutput();
		//turn on the gps deveice....
		return SUCCESS;
  }

	async event result_t uart_comm.putDone() {
		//called when done...
		return SUCCESS;
	}

  /**
   * Start things up.  This just sets the rate for the clock component.
   * 
   * @return Always returns <code>SUCCESS</code>
   **/
  command result_t StdControl.start() {
		//start the gps
		call Uart0Control.start();
		call PW0.set();
		//start the timer...
		//every 5 seconds..
		//call Timer.start(TIMER_REPEAT,5000);
		return SUCCESS; 
  }

  /**
   * Halt execution of the application.
   * This just disables the clock component.
   * 
   * @return Always returns <code>SUCCESS</code>
   **/
  command result_t StdControl.stop() {
		call Uart0Control.stop();
		//stop the gps
		call PW0.clr();
    return SUCCESS;
  }

  /**
   * Toggle the red LED in response to the <code>Timer.fired</code> event.  
   *
   * @return Always returns <code>SUCCESS</code>
   **/
	 /*
  event result_t Timer.fired()
  {	
		dbg(DBG_USR1,"timer fired\n");
    return SUCCESS;
  }
*/
	async event result_t uart_comm.get(uint8_t data) {
		//we have recevied a byte... now what...
		//debug user
		dbg(DBG_USR1,"received: %c\n",(char)data);
		return SUCCESS;
	}
}



/*
 *
 *
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
#include "comm_types.h"
#include "PowerMonitor/comm.h"
module TestAdcDriverM 
{
  provides 
	{
		interface StdControl;
  }
  uses 
	{
    interface StdControl as RadioControl;
		interface SendMsg;
		interface StdControl as TimerControl;
		interface Timer;
		interface Leds;
  	interface PowerSample;
		interface StdControl as PowerMonControl;
	}
}
implementation 
{
	enum {
		S_INIT,
		S_SAMPLE,
		S_SEND_DATA,
	};
	uint8_t state;
	bool radio_busy;
	bool fresh_sample;
	TOS_Msg tempMsg;
	info_packet_t data;	
	void transition();
	task void transition_task();
	
	command result_t StdControl.init()	{
		call TimerControl.init();
		call Leds.init();
		call RadioControl.init();
		call PowerMonControl.init();
		radio_busy = FALSE;
		state = S_INIT;
		data.seqnum = 0;
		return SUCCESS;
	}
	
	command result_t StdControl.start() {
		call TimerControl.start();
		call RadioControl.start();
		post transition_task();
		return SUCCESS;
	}
	
	command result_t StdControl.stop()	{
		call TimerControl.stop();
		call RadioControl.stop();
		call Timer.stop();
		return SUCCESS;
	}

	task void sendResults() {
		if(radio_busy == FALSE) {
			radio_busy = TRUE;
			//pack the message and send it
			memcpy(tempMsg.data,&data,sizeof(info_packet_t));
			
			if(call SendMsg.send(AM_ADC_BASE_ADDR,
					sizeof(info_packet_t),
					&tempMsg) == FAIL) {
				
				call Leds.yellowToggle();
				radio_busy = FALSE;
				post sendResults();
			}
			data.seqnum += 1;
		}
	}
	
	event result_t Timer.fired() {
		return SUCCESS;
	}
	
	task void transition_task() {
		transition();
	}

	void transition() {
		uint8_t lstate = state;
		switch(lstate) {
			case S_INIT:
				state = S_SAMPLE;
				call PowerSample.getData();
				break;
			case S_SAMPLE:
				state = S_SEND_DATA;
				post sendResults();
				break;
			case S_SEND_DATA:
				state = S_INIT;
				post transition_task();
				break;
			default:
				break;
		}
	}

	event result_t SendMsg.sendDone(TOS_MsgPtr msg, result_t success) {
		radio_busy = FALSE;	
		if(success == SUCCESS) {
			call Leds.redToggle();
		}
		post transition_task();
		return SUCCESS;
	}

	event result_t PowerSample.dataReady(power_monitor_t ldata) {
		uint8_t bvat;
		data.msg = ldata;
		//testing the normalizing function...
		//bvat = normalize_batt_v(&power.BattV);
		//power.BattV = bvat;
		if(state == S_SAMPLE) {
			post transition_task();
		}
	}

}


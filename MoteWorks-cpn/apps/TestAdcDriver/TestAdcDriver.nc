/*
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
#include "PowerMonitor/comm.h"
configuration TestAdcDriver
{
}
implementation
{
	components Main, TestAdcDriverM, PowerMonitor, TimerC, LedsC;

	components GenericComm as Comm;

	Main.StdControl -> TestAdcDriverM.StdControl;
	TestAdcDriverM.RadioControl -> Comm;
	TestAdcDriverM.SendMsg -> Comm.SendMsg[AM_POWER_MON_MSG];
	
	TestAdcDriverM.TimerControl -> TimerC.StdControl;
	TestAdcDriverM.Timer -> TimerC.Timer[unique("Timer")];
	TestAdcDriverM.Leds -> LedsC.Leds;
	
	TestAdcDriverM.PowerMonControl -> PowerMonitor.StdControl;
	TestAdcDriverM.PowerSample -> PowerMonitor.PowerSample[unique("TestADCDriver")];
	
}


#define nx_struct struct
#define nx_union union
#define dbg(mode, format, ...) ((void)0)
#define dbg_clear(mode, format, ...) ((void)0)
#define dbg_active(mode) 0
# 152 "/opt/local/lib/gcc/avr/4.1.2/include/stddef.h" 3
typedef int ptrdiff_t;
#line 214
typedef unsigned int size_t;
#line 326
typedef int wchar_t;
# 8 "/opt/local/lib/ncc/deputy_nodeputy.h"
struct __nesc_attr_nonnull {
#line 8
  int dummy;
}  ;
#line 9
struct __nesc_attr_bnd {
#line 9
  void *lo, *hi;
}  ;
#line 10
struct __nesc_attr_bnd_nok {
#line 10
  void *lo, *hi;
}  ;
#line 11
struct __nesc_attr_count {
#line 11
  int n;
}  ;
#line 12
struct __nesc_attr_count_nok {
#line 12
  int n;
}  ;
#line 13
struct __nesc_attr_one {
#line 13
  int dummy;
}  ;
#line 14
struct __nesc_attr_one_nok {
#line 14
  int dummy;
}  ;
#line 15
struct __nesc_attr_dmemset {
#line 15
  int a1, a2, a3;
}  ;
#line 16
struct __nesc_attr_dmemcpy {
#line 16
  int a1, a2, a3;
}  ;
#line 17
struct __nesc_attr_nts {
#line 17
  int dummy;
}  ;
# 121 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdint.h" 3
typedef int int8_t __attribute((__mode__(__QI__))) ;
typedef unsigned int uint8_t __attribute((__mode__(__QI__))) ;
typedef int int16_t __attribute((__mode__(__HI__))) ;
typedef unsigned int uint16_t __attribute((__mode__(__HI__))) ;
typedef int int32_t __attribute((__mode__(__SI__))) ;
typedef unsigned int uint32_t __attribute((__mode__(__SI__))) ;

typedef int int64_t __attribute((__mode__(__DI__))) ;
typedef unsigned int uint64_t __attribute((__mode__(__DI__))) ;
#line 142
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
#line 159
typedef int8_t int_least8_t;




typedef uint8_t uint_least8_t;




typedef int16_t int_least16_t;




typedef uint16_t uint_least16_t;




typedef int32_t int_least32_t;




typedef uint32_t uint_least32_t;







typedef int64_t int_least64_t;






typedef uint64_t uint_least64_t;
#line 213
typedef int8_t int_fast8_t;




typedef uint8_t uint_fast8_t;




typedef int16_t int_fast16_t;




typedef uint16_t uint_fast16_t;




typedef int32_t int_fast32_t;




typedef uint32_t uint_fast32_t;







typedef int64_t int_fast64_t;






typedef uint64_t uint_fast64_t;
#line 273
typedef int64_t intmax_t;




typedef uint64_t uintmax_t;
# 77 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/inttypes.h" 3
typedef int32_t int_farptr_t;



typedef uint32_t uint_farptr_t;
# 431 "/opt/local/lib/ncc/nesc_nx.h"
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nx_int8_t;typedef int8_t __nesc_nxbase_nx_int8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nx_int16_t;typedef int16_t __nesc_nxbase_nx_int16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nx_int32_t;typedef int32_t __nesc_nxbase_nx_int32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nx_int64_t;typedef int64_t __nesc_nxbase_nx_int64_t  ;
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nx_uint8_t;typedef uint8_t __nesc_nxbase_nx_uint8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nx_uint16_t;typedef uint16_t __nesc_nxbase_nx_uint16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nx_uint32_t;typedef uint32_t __nesc_nxbase_nx_uint32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nx_uint64_t;typedef uint64_t __nesc_nxbase_nx_uint64_t  ;


typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nxle_int8_t;typedef int8_t __nesc_nxbase_nxle_int8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nxle_int16_t;typedef int16_t __nesc_nxbase_nxle_int16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nxle_int32_t;typedef int32_t __nesc_nxbase_nxle_int32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nxle_int64_t;typedef int64_t __nesc_nxbase_nxle_int64_t  ;
typedef struct { unsigned char nxdata[1]; } __attribute__((packed)) nxle_uint8_t;typedef uint8_t __nesc_nxbase_nxle_uint8_t  ;
typedef struct { unsigned char nxdata[2]; } __attribute__((packed)) nxle_uint16_t;typedef uint16_t __nesc_nxbase_nxle_uint16_t  ;
typedef struct { unsigned char nxdata[4]; } __attribute__((packed)) nxle_uint32_t;typedef uint32_t __nesc_nxbase_nxle_uint32_t  ;
typedef struct { unsigned char nxdata[8]; } __attribute__((packed)) nxle_uint64_t;typedef uint64_t __nesc_nxbase_nxle_uint64_t  ;
# 71 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdlib.h" 3
#line 68
typedef struct __nesc_unnamed4242 {
  int quot;
  int rem;
} div_t;





#line 74
typedef struct __nesc_unnamed4243 {
  long quot;
  long rem;
} ldiv_t;


typedef int (*__compar_fn_t)(const void *arg_0x100771370, const void *arg_0x100771648);
# 71 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
typedef unsigned char bool;






enum __nesc_unnamed4244 {
  FALSE = 0, 
  TRUE = 1
};








uint8_t TOS_ROUTE_PROTOCOL = 0x90;
#line 104
uint8_t TOS_BASE_STATION = 0;





const uint8_t TOS_DATA_LENGTH = 36;
#line 132
uint8_t TOS_PLATFORM = 7;










enum __nesc_unnamed4245 {
  FAIL = 0, 
  SUCCESS = 1
};


static inline uint8_t rcombine(uint8_t r1, uint8_t r2);
typedef uint8_t result_t  ;







static inline result_t rcombine(result_t r1, result_t r2);
#line 178
enum __nesc_unnamed4246 {
  NULL = 0x0
};
# 210 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/avr/pgmspace.h" 3
typedef void prog_void __attribute((__progmem__)) ;
typedef char prog_char __attribute((__progmem__)) ;
typedef unsigned char prog_uchar __attribute((__progmem__)) ;

typedef int8_t prog_int8_t __attribute((__progmem__)) ;
typedef uint8_t prog_uint8_t __attribute((__progmem__)) ;
typedef int16_t prog_int16_t __attribute((__progmem__)) ;
typedef uint16_t prog_uint16_t __attribute((__progmem__)) ;
typedef int32_t prog_int32_t __attribute((__progmem__)) ;
typedef uint32_t prog_uint32_t __attribute((__progmem__)) ;

typedef int64_t prog_int64_t __attribute((__progmem__)) ;
typedef uint64_t prog_uint64_t __attribute((__progmem__)) ;
# 118 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
enum __nesc_unnamed4247 {
  TOSH_period16 = 0x00, 
  TOSH_period32 = 0x01, 
  TOSH_period64 = 0x02, 
  TOSH_period128 = 0x03, 
  TOSH_period256 = 0x04, 
  TOSH_period512 = 0x05, 
  TOSH_period1024 = 0x06, 
  TOSH_period2048 = 0x07
};

static inline void TOSH_wait();







typedef uint8_t __nesc_atomic_t;

__nesc_atomic_t __nesc_atomic_start(void );
void __nesc_atomic_end(__nesc_atomic_t oldSreg);



__inline __nesc_atomic_t __nesc_atomic_start(void )  ;






__inline void __nesc_atomic_end(__nesc_atomic_t oldSreg)  ;






static __inline void __nesc_atomic_sleep();







static __inline void __nesc_enable_interrupt();



static __inline void __nesc_disable_interrupt();
# 39 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_uwait(int u_sec);
#line 63
static __inline void TOSH_SET_RED_LED_PIN();
#line 63
static __inline void TOSH_CLR_RED_LED_PIN();
#line 63
static __inline void TOSH_MAKE_RED_LED_OUTPUT();
static __inline void TOSH_SET_GREEN_LED_PIN();
#line 64
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT();
static __inline void TOSH_SET_YELLOW_LED_PIN();
#line 65
static __inline void TOSH_CLR_YELLOW_LED_PIN();
#line 65
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT();

static __inline void TOSH_CLR_SERIAL_ID_PIN();
#line 67
static __inline void TOSH_MAKE_SERIAL_ID_INPUT();
#line 79
static __inline void TOSH_SET_FLASH_SELECT_PIN();
#line 79
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT();
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT();
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT();
#line 98
static __inline void TOSH_SET_PW0_PIN();
#line 98
static __inline void TOSH_MAKE_PW0_OUTPUT();
static __inline void TOSH_SET_PW1_PIN();
#line 99
static __inline void TOSH_CLR_PW1_PIN();
#line 99
static __inline void TOSH_MAKE_PW1_OUTPUT();
static __inline void TOSH_SET_PW2_PIN();
#line 100
static __inline void TOSH_MAKE_PW2_OUTPUT();
static __inline void TOSH_MAKE_PW3_OUTPUT();
static __inline void TOSH_MAKE_PW4_OUTPUT();
static __inline void TOSH_MAKE_PW5_OUTPUT();
static __inline void TOSH_MAKE_PW6_OUTPUT();
static __inline void TOSH_MAKE_PW7_OUTPUT();
#line 134
static inline void TOSH_SET_PIN_DIRECTIONS(void );
#line 187
enum __nesc_unnamed4248 {
  TOSH_ADC_PORTMAPSIZE = 12
};

enum __nesc_unnamed4249 {


  TOSH_ACTUAL_VOLTAGE_PORT = 30, 
  TOSH_ACTUAL_BANDGAP_PORT = 30, 
  TOSH_ACTUAL_GND_PORT = 31
};

enum __nesc_unnamed4250 {


  TOS_ADC_VOLTAGE_PORT = 7, 
  TOS_ADC_BANDGAP_PORT = 10, 
  TOS_ADC_GND_PORT = 11
};
# 33 "/Users/wbennett/opt/MoteWorks/tos/types/dbg_modes.h"
typedef long long TOS_dbg_mode;



enum __nesc_unnamed4251 {
  DBG_ALL = ~0ULL, 


  DBG_BOOT = 1ULL << 0, 
  DBG_CLOCK = 1ULL << 1, 
  DBG_TASK = 1ULL << 2, 
  DBG_SCHED = 1ULL << 3, 
  DBG_SENSOR = 1ULL << 4, 
  DBG_LED = 1ULL << 5, 
  DBG_CRYPTO = 1ULL << 6, 


  DBG_ROUTE = 1ULL << 7, 
  DBG_AM = 1ULL << 8, 
  DBG_CRC = 1ULL << 9, 
  DBG_PACKET = 1ULL << 10, 
  DBG_ENCODE = 1ULL << 11, 
  DBG_RADIO = 1ULL << 12, 


  DBG_LOG = 1ULL << 13, 
  DBG_ADC = 1ULL << 14, 
  DBG_I2C = 1ULL << 15, 
  DBG_UART = 1ULL << 16, 
  DBG_PROG = 1ULL << 17, 
  DBG_SOUNDER = 1ULL << 18, 
  DBG_TIME = 1ULL << 19, 
  DBG_POWER = 1ULL << 20, 



  DBG_SIM = 1ULL << 21, 
  DBG_QUEUE = 1ULL << 22, 
  DBG_SIMRADIO = 1ULL << 23, 
  DBG_HARD = 1ULL << 24, 
  DBG_MEM = 1ULL << 25, 



  DBG_USR1 = 1ULL << 27, 
  DBG_USR2 = 1ULL << 28, 
  DBG_USR3 = 1ULL << 29, 
  DBG_TEMP = 1ULL << 30, 

  DBG_ERROR = 1ULL << 31, 
  DBG_NONE = 0, 

  DBG_DEFAULT = DBG_ALL
};
# 41 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
#line 39
typedef struct __nesc_unnamed4252 {
  void (*tp)();
} TOSH_sched_entry_T;

enum __nesc_unnamed4253 {






  TOSH_MAX_TASKS = 32, 

  TOSH_TASK_BITMASK = TOSH_MAX_TASKS - 1
};

volatile TOSH_sched_entry_T TOSH_queue[TOSH_MAX_TASKS];
uint8_t TOSH_sched_full;
volatile uint8_t TOSH_sched_free;

static inline void TOSH_sched_init(void );








bool TOS_post(void (*tp)());
#line 82
bool TOS_post(void (*tp)())  ;
#line 116
static inline bool TOSH_run_next_task();
#line 139
static inline void TOSH_run_task();
# 14 "/Users/wbennett/opt/MoteWorks/tos/system/Ident.h"
enum __nesc_unnamed4254 {

  IDENT_MAX_PROGRAM_NAME_LENGTH = 17
};






#line 19
typedef struct __nesc_unnamed4255 {

  uint32_t unix_time;
  uint32_t user_hash;
  char program_name[IDENT_MAX_PROGRAM_NAME_LENGTH];
} Ident_t;
# 43 "/opt/local/lib/gcc/avr/4.1.2/include/stdarg.h" 3
typedef __builtin_va_list __gnuc_va_list;
#line 105
typedef __gnuc_va_list va_list;
# 242 "/opt/local/lib/gcc/avr/4.1.2/../../../../avr/include/stdio.h" 3
struct __file {
  char *buf;
  unsigned char unget;
  uint8_t flags;
#line 261
  int size;
  int len;
  int (*put)(char arg_0x1007cba50, struct __file *arg_0x1007cbd90);
  int (*get)(struct __file *arg_0x10111b4d0);
  void *udata;
};
#line 405
struct __file;
#line 417
struct __file;
# 12 "/Users/wbennett/opt/MoteWorks/lib/debug_constants.h"
static char dbg_buffer[256];
static uint16_t dbg_read_pointer;
static uint16_t dbg_write_pointer;
static int16_t dbg_data_size;
void (*full_event)(void ) = (void *)0;
#line 42
static inline char twi_pull_char(void );
#line 56
static inline void twi_attach_full_event(void (*handler)(void ));



static inline int8_t twi_buffer_empty(void );
# 49 "/Users/wbennett/opt/MoteWorks/include/Tracker/gsm.h"
#line 45
typedef struct __nesc_unnamed4256 {
  uint8_t length;
  int8_t data[200];
} 
__attribute((packed))  General_Msg;
typedef General_Msg *General_MsgPtr;






#line 52
typedef struct __nesc_unnamed4257 {
  uint8_t contents[4];
  uint8_t front;
  uint8_t count;
} 
RecvQueue_t;
typedef RecvQueue_t recvQueue_t;

typedef uint8_t gsm_result_t;
enum __nesc_unnamed4258 {

  ERROR = 0, 
  OK = 1, 
  TIMEOUT_ERROR = 3
};

typedef uint8_t gsm_assoc_status_t;
enum __nesc_unnamed4259 {


  NOT_REGISTERED_NO_LOOK = 0, 
  REGISTERED_HOME = 1, 
  NOT_REGISTERED_LOOK = 2, 
  REGISTRATION_DENIED = 3, 
  UNKNOWN = 4, 
  REGISTERED_ROAMING = 5
};

typedef uint8_t gsm_assoc_mode_t;
enum __nesc_unnamed4260 {

  DISABLE_NETWORK_REGISTRATION = 0, 
  ENABLE_NETWORK_REGISTRATION_RESULT_CODE = 1, 
  ENABLE_NETWORK_REGISTRATION_WITH_CELL_ID = 2
};







#line 88
typedef struct __nesc_unnamed4261 {

  uint8_t status;
  uint16_t cellid;
  uint16_t areacode;
} 
__attribute((packed))  Gsm_cellid_areacode_data_t;
typedef Gsm_cellid_areacode_data_t gsm_cellid_areacode_data_t;






#line 97
typedef struct __nesc_unnamed4262 {

  uint8_t rssi;
  uint8_t bit_err_rate;
} 
__attribute((packed))  Gsm_signal_quality_data_t;
typedef Gsm_signal_quality_data_t gsm_signal_quality_data_t;









#line 105
typedef struct __nesc_unnamed4263 {

  char netname[8];
  uint8_t bsic;
  uint16_t lac;
  uint16_t id;
  uint16_t arfcn;
} 
__attribute((packed))  Gsm_cellmon_record_t;
typedef Gsm_cellmon_record_t gsm_cellmon_record_t;







#line 116
typedef struct __nesc_unnamed4264 {

  Gsm_signal_quality_data_t signal_quality_data;
  Gsm_cellmon_record_t gsm_cellmon_record;
  uint8_t seqnum;
} 
__attribute((packed))  Gsm_header_t;
typedef Gsm_header_t gsm_header_t;







#line 127
typedef struct __nesc_unnamed4265 {

  Gsm_cellmon_record_t towers[3];
} 
__attribute((packed))  Gsm_cellmon_data_t;
typedef Gsm_cellmon_data_t gsm_cellmon_data_t;

typedef uint16_t gsm_error_t;
enum __nesc_unnamed4266 {


  MS_OPERATION_NOT_SUPPORTED = 303u, 
  MS_SIM_NOT_INSERTED = 310u, 
  MS_SIM_PIN_REQUIRED = 311u, 
  MS_NO_NETWORK_SERVICE = 331u, 
  MS_NETWORK_TIMEOUT = 332u, 

  NO_MODEM_RESPONSE = 1000u, 
  GSM_UART_HANDLER_SEND_CMD_FAIL = 1001u, 
  SMS_PROMPT_NOT_RECVD = 1002u, 
  GSM_NOT_ASSOCIATED = 1003u, 
  GSM_BUSY = 1004u, 
  INVALID_PARAM = 1005u, 
  WATCHDOG_TIMED_OUT = 1006u, 
  UNSUPPORTED_FEATURE = 1007u, 
  NO_ERROR = 65535u
};
# 18 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.h"
enum __nesc_unnamed4267 {
  TIMER_REPEAT = 0, 
  TIMER_ONE_SHOT = 1, 
  NUM_TIMERS = 1U
};
# 30 "/Users/wbennett/opt/MoteWorks/lib/avrtime.h"
struct avr_tm {
  int8_t sec;
  int8_t min;
  int8_t hour;
  int8_t day;
  int8_t mon;
  int16_t year;
  int8_t wday;
  int16_t day_of_year;
  uint8_t is_dst;
  uint8_t hundreth;
};

typedef uint64_t avrtime_t;


static inline void set_time_millis(avrtime_t set_time);

static inline void add_time_millis(uint32_t time_to_add);
static inline void init_avrtime();

static inline avrtime_t get_time_millis();





static inline void reset_start_time();



struct avr_tm;

struct avr_tm;
# 15 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static avrtime_t start_time;
static avrtime_t sys_time;
static bool isInit = 0;


static inline void set_time_millis(avrtime_t set_time);








static inline void init_avrtime();








static inline void add_time_millis(uint32_t time_to_add);







static inline avrtime_t get_time_millis();
#line 70
static inline void reset_start_time();
#line 133
struct avr_tm;
# 13 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/Clock.h"
enum __nesc_unnamed4268 {
  TOS_I1000PS = 32, TOS_S1000PS = 1, 
  TOS_I100PS = 40, TOS_S100PS = 2, 
  TOS_I10PS = 101, TOS_S10PS = 3, 
  TOS_I1024PS = 0, TOS_S1024PS = 3, 
  TOS_I512PS = 1, TOS_S512PS = 3, 
  TOS_I256PS = 3, TOS_S256PS = 3, 
  TOS_I128PS = 7, TOS_S128PS = 3, 
  TOS_I64PS = 15, TOS_S64PS = 3, 
  TOS_I32PS = 31, TOS_S32PS = 3, 
  TOS_I16PS = 63, TOS_S16PS = 3, 
  TOS_I8PS = 127, TOS_S8PS = 3, 
  TOS_I4PS = 255, TOS_S4PS = 3, 
  TOS_I2PS = 15, TOS_S2PS = 7, 
  TOS_I1PS = 31, TOS_S1PS = 7, 
  TOS_I0PS = 0, TOS_S0PS = 0
};
enum __nesc_unnamed4269 {
  DEFAULT_SCALE = 3, DEFAULT_INTERVAL = 127
};
# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
static result_t PotM$Pot$init(uint8_t initialSetting);
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
static result_t HPLPotC$Pot$finalise(void );
#line 38
static result_t HPLPotC$Pot$decrease(void );







static result_t HPLPotC$Pot$increase(void );
# 32 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static result_t HPLInit$init(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t TestGSMDriverM$StdControl$init(void );






static result_t TestGSMDriverM$StdControl$start(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TestGSMDriverM$Timer$fired(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t TimerM$Clock$fire(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t TimerM$StdControl$init(void );






static result_t TimerM$StdControl$start(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TimerM$Timer$default$fired(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x101391808);
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TimerM$Timer$start(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x101391808, 
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
char type, uint32_t interval);
# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static void HPLClock$Clock$setInterval(uint8_t value);
#line 127
static result_t HPLClock$Clock$setIntervalAndScale(uint8_t interval, uint8_t scale);




static uint8_t HPLClock$Clock$readCounter(void );
#line 75
static result_t HPLClock$Clock$setRate(char interval, char scale);
#line 100
static uint8_t HPLClock$Clock$getInterval(void );
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void );
# 101 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t LedsC$Leds$yellowOff(void );
#line 93
static result_t LedsC$Leds$yellowOn(void );
#line 35
static result_t LedsC$Leds$init(void );







static result_t LedsC$Leds$redOn(void );
# 14 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static result_t DebugI2CM$flush(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t DebugI2CM$StdControl$init(void );






static result_t DebugI2CM$StdControl$start(void );
# 3 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
static void HighLevelTwiM$HighLevelTwiInterface$begin(void );







static void HighLevelTwiM$HighLevelTwiInterface$send(uint8_t data);
#line 7
static void HighLevelTwiM$HighLevelTwiInterface$beginTransmission(uint8_t address);

static uint8_t HighLevelTwiM$HighLevelTwiInterface$endTransmission(void );
# 7 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiInterface.nc"
static uint8_t LowLevelTwiM$LowLevelTwiInterface$twi_writeTo(uint8_t address, uint8_t *data, uint8_t length, uint8_t wait);
#line 3
static void LowLevelTwiM$LowLevelTwiInterface$twi_init(void );
# 27 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
static result_t RealMain$hardwareInit(void );
# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
static result_t RealMain$Pot$init(uint8_t initialSetting);
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t RealMain$StdControl$init(void );






static result_t RealMain$StdControl$start(void );
# 34 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
int main(void )   ;
# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
static result_t PotM$HPLPot$finalise(void );
#line 38
static result_t PotM$HPLPot$decrease(void );







static result_t PotM$HPLPot$increase(void );
# 70 "/Users/wbennett/opt/MoteWorks/tos/system/PotM.nc"
uint8_t PotM$potSetting;

static inline void PotM$setPot(uint8_t value);
#line 85
static inline result_t PotM$Pot$init(uint8_t initialSetting);
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$decrease(void );








static inline result_t HPLPotC$Pot$increase(void );








static inline result_t HPLPotC$Pot$finalise(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static inline result_t HPLInit$init(void );
# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
static result_t TestGSMDriverM$TimerControl$init(void );






static result_t TestGSMDriverM$TimerControl$start(void );
# 101 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
static result_t TestGSMDriverM$Leds$yellowOff(void );
#line 93
static result_t TestGSMDriverM$Leds$yellowOn(void );
#line 35
static result_t TestGSMDriverM$Leds$init(void );







static result_t TestGSMDriverM$Leds$redOn(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TestGSMDriverM$Timer$start(char type, uint32_t interval);
# 32 "TestGSMDriverM.nc"
uint8_t TestGSMDriverM$state;
enum TestGSMDriverM$__nesc_unnamed4270 {
  TestGSMDriverM$INIT, 
  TestGSMDriverM$HARD_TOGGLE, 
  TestGSMDriverM$SOFT_TOGGLE, 
  TestGSMDriverM$DONE
};

static void TestGSMDriverM$transition(void );

static inline result_t TestGSMDriverM$StdControl$init(void );






static inline result_t TestGSMDriverM$StdControl$start(void );
#line 67
static inline result_t TestGSMDriverM$Timer$fired(void );




static void TestGSMDriverM$transition(void );
# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
static uint8_t TimerM$PowerManagement$adjustPower(void );
# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static void TimerM$Clock$setInterval(uint8_t value);
#line 132
static uint8_t TimerM$Clock$readCounter(void );
#line 75
static result_t TimerM$Clock$setRate(char interval, char scale);
#line 100
static uint8_t TimerM$Clock$getInterval(void );
# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
static result_t TimerM$Timer$fired(
# 28 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
uint8_t arg_0x101391808);










uint32_t TimerM$mState;
uint8_t TimerM$setIntervalFlag;
uint8_t TimerM$mScale;
#line 41
uint8_t TimerM$mInterval;
int8_t TimerM$queue_head;
int8_t TimerM$queue_tail;
uint8_t TimerM$queue_size;
uint8_t TimerM$queue[NUM_TIMERS];
volatile uint16_t TimerM$interval_outstanding;





#line 48
struct TimerM$timer_s {
  uint8_t type;
  int32_t ticks;
  int32_t ticksLeft;
} TimerM$mTimerList[NUM_TIMERS];

enum TimerM$__nesc_unnamed4271 {
  TimerM$maxTimerInterval = 230
};
static inline result_t TimerM$StdControl$init(void );
#line 69
static inline result_t TimerM$StdControl$start(void );










static result_t TimerM$Timer$start(uint8_t id, char type, 
uint32_t interval);
#line 111
inline static void TimerM$adjustInterval(void );
#line 164
static inline result_t TimerM$Timer$default$fired(uint8_t id);



static inline void TimerM$enqueue(uint8_t value);







static inline uint8_t TimerM$dequeue(void );









static inline void TimerM$signalOneTimer(void );





static inline void TimerM$HandleFire(void );
#line 237
static inline result_t TimerM$Clock$fire(void );
# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
static result_t HPLClock$Clock$fire(void );
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
uint8_t HPLClock$set_flag;
uint8_t HPLClock$mscale;
#line 38
uint8_t HPLClock$nextScale;
#line 38
uint8_t HPLClock$minterval;
#line 70
static inline void HPLClock$Clock$setInterval(uint8_t value);









static inline uint8_t HPLClock$Clock$getInterval(void );
#line 108
static inline result_t HPLClock$Clock$setIntervalAndScale(uint8_t interval, uint8_t scale);
#line 136
static inline uint8_t HPLClock$Clock$readCounter(void );
#line 157
static inline result_t HPLClock$Clock$setRate(char interval, char scale);






void __vector_13(void )   __attribute((interrupt)) ;
# 48 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/HPLPowerManagementM.nc"
bool HPLPowerManagementM$disabled = 1;
#line 61
static inline uint8_t HPLPowerManagementM$getPowerLevel(void );
#line 114
static inline void HPLPowerManagementM$doAdjustment(void );
#line 134
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void );
# 28 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
uint8_t LedsC$ledsOn;

enum LedsC$__nesc_unnamed4272 {
  LedsC$RED_BIT = 1, 
  LedsC$GREEN_BIT = 2, 
  LedsC$YELLOW_BIT = 4
};

static inline result_t LedsC$Leds$init(void );
#line 50
static inline result_t LedsC$Leds$redOn(void );
#line 108
static inline result_t LedsC$Leds$yellowOn(void );








static inline result_t LedsC$Leds$yellowOff(void );
# 3 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
static void DebugI2CM$HighLevelTwiInterface$begin(void );







static void DebugI2CM$HighLevelTwiInterface$send(uint8_t data);
#line 7
static void DebugI2CM$HighLevelTwiInterface$beginTransmission(uint8_t address);

static uint8_t DebugI2CM$HighLevelTwiInterface$endTransmission(void );
# 26 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static inline void DebugI2CM$full_event_handler(void );



static inline result_t DebugI2CM$StdControl$init(void );







static inline result_t DebugI2CM$StdControl$start(void );
#line 52
static void DebugI2CM$printBufferTask(void );
#line 74
static inline result_t DebugI2CM$flush(void );
# 7 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiInterface.nc"
static uint8_t HighLevelTwiM$LowLevelTwiInterface$twi_writeTo(uint8_t address, uint8_t *data, uint8_t length, uint8_t wait);
#line 3
static void HighLevelTwiM$LowLevelTwiInterface$twi_init(void );
# 16 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiM.nc"
static uint8_t HighLevelTwiM$rxBufferIndex = 0;
static uint8_t HighLevelTwiM$rxBufferLength = 0;

static uint8_t HighLevelTwiM$txAddress;
static uint8_t HighLevelTwiM$txBuffer[32];
static uint8_t HighLevelTwiM$txBufferIndex = 0;
static uint8_t HighLevelTwiM$txBufferLength = 0;

static uint8_t HighLevelTwiM$transmitting;

static inline void HighLevelTwiM$HighLevelTwiInterface$begin(void );
#line 54
static inline void HighLevelTwiM$HighLevelTwiInterface$beginTransmission(uint8_t address);










static uint8_t HighLevelTwiM$HighLevelTwiInterface$endTransmission(void );
#line 80
static inline void HighLevelTwiM$HighLevelTwiInterface$send(uint8_t data);
# 13 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiM.nc"
static volatile uint8_t LowLevelTwiM$twi_state;
static uint8_t LowLevelTwiM$twi_slarw;




static uint8_t LowLevelTwiM$twi_masterBuffer[32];
static volatile uint8_t LowLevelTwiM$twi_masterBufferIndex;
static uint8_t LowLevelTwiM$twi_masterBufferLength;








static volatile uint8_t LowLevelTwiM$twi_error;







static inline void LowLevelTwiM$LowLevelTwiInterface$twi_init(void );
#line 134
static inline uint8_t LowLevelTwiM$LowLevelTwiInterface$twi_writeTo(uint8_t address, uint8_t *data, uint8_t length, uint8_t wait);
#line 188
static inline void LowLevelTwiM$twi_reply(uint8_t ack);
#line 204
static void LowLevelTwiM$twi_stop(void );
#line 225
static inline void LowLevelTwiM$twi_releaseBus(void );








void __vector_39(void )   __attribute((signal)) ;
# 64 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_SET_GREEN_LED_PIN()
#line 64
{
#line 64
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 1;
}

#line 65
static __inline void TOSH_SET_YELLOW_LED_PIN()
#line 65
{
#line 65
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 0;
}

#line 63
static __inline void TOSH_SET_RED_LED_PIN()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 2;
}

#line 79
static __inline void TOSH_SET_FLASH_SELECT_PIN()
#line 79
{
#line 79
  * (volatile uint8_t *)(0X02 + 0x20) |= 1 << 3;
}

#line 80
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT()
#line 80
{
#line 80
  * (volatile uint8_t *)(0x0A + 0x20) |= 1 << 5;
}

#line 81
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT()
#line 81
{
#line 81
  * (volatile uint8_t *)(0x0A + 0x20) |= 1 << 3;
}

#line 79
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT()
#line 79
{
#line 79
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 3;
}

#line 67
static __inline void TOSH_CLR_SERIAL_ID_PIN()
#line 67
{
#line 67
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 4);
}

#line 67
static __inline void TOSH_MAKE_SERIAL_ID_INPUT()
#line 67
{
#line 67
  * (volatile uint8_t *)(0X01 + 0x20) &= ~(1 << 4);
}

#line 98
static __inline void TOSH_MAKE_PW0_OUTPUT()
#line 98
{
#line 98
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 0;
}

#line 99
static __inline void TOSH_MAKE_PW1_OUTPUT()
#line 99
{
#line 99
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 1;
}

#line 100
static __inline void TOSH_MAKE_PW2_OUTPUT()
#line 100
{
#line 100
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 2;
}

#line 101
static __inline void TOSH_MAKE_PW3_OUTPUT()
#line 101
{
#line 101
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 3;
}

#line 102
static __inline void TOSH_MAKE_PW4_OUTPUT()
#line 102
{
#line 102
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 4;
}

#line 103
static __inline void TOSH_MAKE_PW5_OUTPUT()
#line 103
{
#line 103
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 5;
}

#line 104
static __inline void TOSH_MAKE_PW6_OUTPUT()
#line 104
{
#line 104
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 6;
}

#line 105
static __inline void TOSH_MAKE_PW7_OUTPUT()
#line 105
{
#line 105
  * (volatile uint8_t *)(0x07 + 0x20) |= 1 << 7;
}

#line 64
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT()
#line 64
{
#line 64
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 1;
}

#line 65
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT()
#line 65
{
#line 65
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 0;
}

#line 63
static __inline void TOSH_MAKE_RED_LED_OUTPUT()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X01 + 0x20) |= 1 << 2;
}

#line 134
static inline void TOSH_SET_PIN_DIRECTIONS(void )
{







  TOSH_MAKE_RED_LED_OUTPUT();
  TOSH_MAKE_YELLOW_LED_OUTPUT();
  TOSH_MAKE_GREEN_LED_OUTPUT();


  TOSH_MAKE_PW7_OUTPUT();
  TOSH_MAKE_PW6_OUTPUT();
  TOSH_MAKE_PW5_OUTPUT();
  TOSH_MAKE_PW4_OUTPUT();
  TOSH_MAKE_PW3_OUTPUT();
  TOSH_MAKE_PW2_OUTPUT();
  TOSH_MAKE_PW1_OUTPUT();
  TOSH_MAKE_PW0_OUTPUT();
#line 173
  TOSH_MAKE_SERIAL_ID_INPUT();
  TOSH_CLR_SERIAL_ID_PIN();

  TOSH_MAKE_FLASH_SELECT_OUTPUT();
  TOSH_MAKE_FLASH_OUT_OUTPUT();
  TOSH_MAKE_FLASH_CLK_OUTPUT();
  TOSH_SET_FLASH_SELECT_PIN();

  TOSH_SET_RED_LED_PIN();
  TOSH_SET_YELLOW_LED_PIN();
  TOSH_SET_GREEN_LED_PIN();
}

# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLInit.nc"
static inline result_t HPLInit$init(void )
#line 37
{
  TOSH_SET_PIN_DIRECTIONS();
  return SUCCESS;
}

# 27 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
inline static result_t RealMain$hardwareInit(void ){
#line 27
  unsigned char __nesc_result;
#line 27

#line 27
  __nesc_result = HPLInit$init();
#line 27

#line 27
  return __nesc_result;
#line 27
}
#line 27
# 55 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$finalise(void )
#line 55
{


  return SUCCESS;
}

# 53 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$finalise(void ){
#line 53
  unsigned char __nesc_result;
#line 53

#line 53
  __nesc_result = HPLPotC$Pot$finalise();
#line 53

#line 53
  return __nesc_result;
#line 53
}
#line 53
# 46 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$increase(void )
#line 46
{





  return SUCCESS;
}

# 46 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$increase(void ){
#line 46
  unsigned char __nesc_result;
#line 46

#line 46
  __nesc_result = HPLPotC$Pot$increase();
#line 46

#line 46
  return __nesc_result;
#line 46
}
#line 46
# 37 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLPotC.nc"
static inline result_t HPLPotC$Pot$decrease(void )
#line 37
{





  return SUCCESS;
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/interfaces/HPLPot.nc"
inline static result_t PotM$HPLPot$decrease(void ){
#line 38
  unsigned char __nesc_result;
#line 38

#line 38
  __nesc_result = HPLPotC$Pot$decrease();
#line 38

#line 38
  return __nesc_result;
#line 38
}
#line 38
# 72 "/Users/wbennett/opt/MoteWorks/tos/system/PotM.nc"
static inline void PotM$setPot(uint8_t value)
#line 72
{
  uint8_t i;

#line 74
  for (i = 0; i < 151; i++) 
    PotM$HPLPot$decrease();

  for (i = 0; i < value; i++) 
    PotM$HPLPot$increase();

  PotM$HPLPot$finalise();

  PotM$potSetting = value;
}

static inline result_t PotM$Pot$init(uint8_t initialSetting)
#line 85
{
  PotM$setPot(initialSetting);
  return SUCCESS;
}

# 57 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Pot.nc"
inline static result_t RealMain$Pot$init(uint8_t initialSetting){
#line 57
  unsigned char __nesc_result;
#line 57

#line 57
  __nesc_result = PotM$Pot$init(initialSetting);
#line 57

#line 57
  return __nesc_result;
#line 57
}
#line 57
# 59 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
static inline void TOSH_sched_init(void )
{
  int i;

#line 62
  TOSH_sched_free = 0;
  TOSH_sched_full = 0;
  for (i = 0; i < TOSH_MAX_TASKS; i++) 
    TOSH_queue[i].tp = (void *)0;
}

# 158 "/Users/wbennett/opt/MoteWorks/tos/system/tos.h"
static inline result_t rcombine(result_t r1, result_t r2)



{
  return r1 == FAIL ? FAIL : r2;
}

# 36 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$init(void )
#line 36
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 37
    {
      LedsC$ledsOn = 0;
      {
      }
#line 39
      ;
      TOSH_MAKE_RED_LED_OUTPUT();
      TOSH_MAKE_YELLOW_LED_OUTPUT();
      TOSH_MAKE_GREEN_LED_OUTPUT();
      TOSH_SET_RED_LED_PIN();
      TOSH_SET_YELLOW_LED_PIN();
      TOSH_SET_GREEN_LED_PIN();
    }
#line 46
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 35 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t TestGSMDriverM$Leds$init(void ){
#line 35
  unsigned char __nesc_result;
#line 35

#line 35
  __nesc_result = LedsC$Leds$init();
#line 35

#line 35
  return __nesc_result;
#line 35
}
#line 35
# 108 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline result_t HPLClock$Clock$setIntervalAndScale(uint8_t interval, uint8_t scale)
#line 108
{



  if (scale > 7) {
#line 112
    return FAIL;
    }
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 114
    {
      * (volatile uint8_t *)0x70 &= ~(1 << 0);
      * (volatile uint8_t *)0x70 &= ~(1 << 1);




      * (volatile uint8_t *)0xB6 |= 1 << 5;

      HPLClock$mscale = scale;
      HPLClock$minterval = interval;

      * (volatile uint8_t *)0xB0 = 2 << 0;
      * (volatile uint8_t *)0xB1 = scale;

      * (volatile uint8_t *)0xB2 = 0;
      * (volatile uint8_t *)0xB3 = interval;
      * (volatile uint8_t *)0x70 |= 1 << 1;
    }
#line 132
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

#line 157
static inline result_t HPLClock$Clock$setRate(char interval, char scale)
#line 157
{
  return HPLClock$Clock$setIntervalAndScale(interval, scale);
}

# 75 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static result_t TimerM$Clock$setRate(char interval, char scale){
#line 75
  unsigned char __nesc_result;
#line 75

#line 75
  __nesc_result = HPLClock$Clock$setRate(interval, scale);
#line 75

#line 75
  return __nesc_result;
#line 75
}
#line 75
# 46 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static inline avrtime_t get_time_millis()
#line 46
{



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 50
    {
      unsigned long long __nesc_temp = 
#line 50
      sys_time;

      {
#line 50
        __nesc_atomic_end(__nesc_atomic); 
#line 50
        return __nesc_temp;
      }
    }
#line 52
    __nesc_atomic_end(__nesc_atomic); }
}

#line 70
static inline void reset_start_time()
#line 70
{



  start_time = get_time_millis();
}

#line 20
static inline void set_time_millis(avrtime_t set_time)
#line 20
{



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 24
    sys_time = set_time;
#line 24
    __nesc_atomic_end(__nesc_atomic); }
}



static inline void init_avrtime()
#line 29
{
  if (isInit == 0) {
      avrtime_t t = 132489216000ULL;

#line 32
      isInit = 1;
      set_time_millis(t);
      reset_start_time();
    }
}

# 57 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline result_t TimerM$StdControl$init(void )
#line 57
{

  init_avrtime();
  TimerM$mState = 0;
  TimerM$setIntervalFlag = 0;
  TimerM$queue_head = TimerM$queue_tail = -1;
  TimerM$queue_size = 0;
  TimerM$mScale = 3;
  TimerM$mInterval = TimerM$maxTimerInterval;
  return TimerM$Clock$setRate(TimerM$mInterval, TimerM$mScale);
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t TestGSMDriverM$TimerControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = TimerM$StdControl$init();
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 42 "TestGSMDriverM.nc"
static inline result_t TestGSMDriverM$StdControl$init(void )
#line 42
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 43
    TestGSMDriverM$state = TestGSMDriverM$INIT;
#line 43
    __nesc_atomic_end(__nesc_atomic); }
  TestGSMDriverM$TimerControl$init();
  TestGSMDriverM$Leds$init();
  return SUCCESS;
}

# 74 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static inline result_t DebugI2CM$flush(void )
#line 74
{
  TOS_post(DebugI2CM$printBufferTask);
  return SUCCESS;
}

#line 26
static inline void DebugI2CM$full_event_handler(void )
#line 26
{
  DebugI2CM$flush();
}

# 56 "/Users/wbennett/opt/MoteWorks/lib/debug_constants.h"
static inline void twi_attach_full_event(void (*handler)(void ))
#line 56
{
  full_event = handler;
}

# 30 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static inline result_t DebugI2CM$StdControl$init(void )
{

  TOSH_MAKE_PW2_OUTPUT();
  twi_attach_full_event(DebugI2CM$full_event_handler);
  return SUCCESS;
}

# 41 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RealMain$StdControl$init(void ){
#line 41
  unsigned char __nesc_result;
#line 41

#line 41
  __nesc_result = DebugI2CM$StdControl$init();
#line 41
  __nesc_result = rcombine(__nesc_result, TestGSMDriverM$StdControl$init());
#line 41

#line 41
  return __nesc_result;
#line 41
}
#line 41
# 54 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiM.nc"
static inline void HighLevelTwiM$HighLevelTwiInterface$beginTransmission(uint8_t address)
{

  HighLevelTwiM$transmitting = 1;

  HighLevelTwiM$txAddress = address;

  HighLevelTwiM$txBufferIndex = 0;
  HighLevelTwiM$txBufferLength = 0;
}

# 7 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
inline static void DebugI2CM$HighLevelTwiInterface$beginTransmission(uint8_t address){
#line 7
  HighLevelTwiM$HighLevelTwiInterface$beginTransmission(address);
#line 7
}
#line 7
# 60 "/Users/wbennett/opt/MoteWorks/lib/debug_constants.h"
static inline int8_t twi_buffer_empty(void )
#line 60
{
  return dbg_read_pointer == dbg_write_pointer && 
  dbg_data_size == 0;
}

# 9 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
inline static uint8_t DebugI2CM$HighLevelTwiInterface$endTransmission(void ){
#line 9
  unsigned char __nesc_result;
#line 9

#line 9
  __nesc_result = HighLevelTwiM$HighLevelTwiInterface$endTransmission();
#line 9

#line 9
  return __nesc_result;
#line 9
}
#line 9
# 134 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiM.nc"
static inline uint8_t LowLevelTwiM$LowLevelTwiInterface$twi_writeTo(uint8_t address, uint8_t *data, uint8_t length, uint8_t wait)
{
  uint8_t i;


  if (32 < length) {
      return 1;
    }


  while (0 != LowLevelTwiM$twi_state) {
      continue;
    }
  LowLevelTwiM$twi_state = 2;

  LowLevelTwiM$twi_error = 0xFF;


  LowLevelTwiM$twi_masterBufferIndex = 0;
  LowLevelTwiM$twi_masterBufferLength = length;


  for (i = 0; i < length; ++i) {
      LowLevelTwiM$twi_masterBuffer[i] = data[i];
    }


  LowLevelTwiM$twi_slarw = 0;
  LowLevelTwiM$twi_slarw |= address << 1;


  * (volatile uint8_t *)0xBC = ((((1 << 2) | (1 << 0)) | (1 << 6)) | (1 << 7)) | (1 << 5);


  while (wait && 2 == LowLevelTwiM$twi_state) {
      continue;
    }

  if (LowLevelTwiM$twi_error == 0xFF) {
    return 0;
    }
  else {
#line 174
    if (LowLevelTwiM$twi_error == 0x20) {
      return 2;
      }
    else {
#line 176
      if (LowLevelTwiM$twi_error == 0x30) {
        return 3;
        }
      else {
#line 179
        return 4;
        }
      }
    }
}

# 7 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiInterface.nc"
inline static uint8_t HighLevelTwiM$LowLevelTwiInterface$twi_writeTo(uint8_t address, uint8_t *data, uint8_t length, uint8_t wait){
#line 7
  unsigned char __nesc_result;
#line 7

#line 7
  __nesc_result = LowLevelTwiM$LowLevelTwiInterface$twi_writeTo(address, data, length, wait);
#line 7

#line 7
  return __nesc_result;
#line 7
}
#line 7
# 80 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiM.nc"
static inline void HighLevelTwiM$HighLevelTwiInterface$send(uint8_t data)
{
  if (HighLevelTwiM$transmitting) {


      if (HighLevelTwiM$txBufferLength >= 32) {
          return;
        }

      HighLevelTwiM$txBuffer[HighLevelTwiM$txBufferIndex] = data;
      ++HighLevelTwiM$txBufferIndex;

      HighLevelTwiM$txBufferLength = HighLevelTwiM$txBufferIndex;
    }
}

# 11 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
inline static void DebugI2CM$HighLevelTwiInterface$send(uint8_t data){
#line 11
  HighLevelTwiM$HighLevelTwiInterface$send(data);
#line 11
}
#line 11
# 42 "/Users/wbennett/opt/MoteWorks/lib/debug_constants.h"
static inline char twi_pull_char(void )
#line 42
{
  char rval;

#line 44
  if (++dbg_read_pointer >= 256) {
#line 44
    dbg_read_pointer = 0;
    }
#line 45
  rval = dbg_buffer[dbg_read_pointer];
  dbg_buffer[dbg_read_pointer] = 0x20;
  dbg_data_size--;
  return rval;
}

# 37 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
inline static result_t TestGSMDriverM$Timer$start(char type, uint32_t interval){
#line 37
  unsigned char __nesc_result;
#line 37

#line 37
  __nesc_result = TimerM$Timer$start(0U, type, interval);
#line 37

#line 37
  return __nesc_result;
#line 37
}
#line 37
# 63 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_RED_LED_PIN()
#line 63
{
#line 63
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 2);
}

# 50 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$redOn(void )
#line 50
{
  {
  }
#line 51
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 52
    {
      TOSH_CLR_RED_LED_PIN();
      LedsC$ledsOn |= LedsC$RED_BIT;
    }
#line 55
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 43 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t TestGSMDriverM$Leds$redOn(void ){
#line 43
  unsigned char __nesc_result;
#line 43

#line 43
  __nesc_result = LedsC$Leds$redOn();
#line 43

#line 43
  return __nesc_result;
#line 43
}
#line 43
# 98 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_SET_PW0_PIN()
#line 98
{
#line 98
  * (volatile uint8_t *)(0x08 + 0x20) |= 1 << 0;
}

# 69 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline result_t TimerM$StdControl$start(void )
#line 69
{
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t TestGSMDriverM$TimerControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = TimerM$StdControl$start();
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 49 "TestGSMDriverM.nc"
static inline result_t TestGSMDriverM$StdControl$start(void )
#line 49
{
  TestGSMDriverM$TimerControl$start();

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 52
    TestGSMDriverM$state = TestGSMDriverM$HARD_TOGGLE;
#line 52
    __nesc_atomic_end(__nesc_atomic); }
  TOSH_SET_PW0_PIN();
  TestGSMDriverM$Leds$redOn();
  TestGSMDriverM$Timer$start(TIMER_ONE_SHOT, 1000);
  return SUCCESS;
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiM.nc"
static inline void LowLevelTwiM$LowLevelTwiInterface$twi_init(void )
{

  LowLevelTwiM$twi_state = 0;



  * (volatile uint8_t *)(0x0B + 0x20) |= 1 << 0;
  * (volatile uint8_t *)(0x0B + 0x20) |= 1 << 1;


  * (volatile uint8_t *)0xB9 &= ~(1 << 0);
  * (volatile uint8_t *)0xB9 &= ~(1 << 1);
  * (volatile uint8_t *)0xB8 = (16000000L / 100000L - 16) / 2;







  * (volatile uint8_t *)0xBC = ((1 << 2) | (1 << 0)) | (1 << 6);
}

# 3 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiInterface.nc"
inline static void HighLevelTwiM$LowLevelTwiInterface$twi_init(void ){
#line 3
  LowLevelTwiM$LowLevelTwiInterface$twi_init();
#line 3
}
#line 3
# 26 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiM.nc"
static inline void HighLevelTwiM$HighLevelTwiInterface$begin(void )
{
  HighLevelTwiM$rxBufferIndex = 0;
  HighLevelTwiM$rxBufferLength = 0;

  HighLevelTwiM$txBufferIndex = 0;
  HighLevelTwiM$txBufferLength = 0;

  HighLevelTwiM$LowLevelTwiInterface$twi_init();
}

# 3 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiInterface.nc"
inline static void DebugI2CM$HighLevelTwiInterface$begin(void ){
#line 3
  HighLevelTwiM$HighLevelTwiInterface$begin();
#line 3
}
#line 3
# 39 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_uwait(int u_sec)
#line 39
{
  while (u_sec > 0) {
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
      u_sec--;
    }
}

#line 100
static __inline void TOSH_SET_PW2_PIN()
#line 100
{
#line 100
  * (volatile uint8_t *)(0x08 + 0x20) |= 1 << 2;
}

# 38 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static inline result_t DebugI2CM$StdControl$start(void )
{
  TOSH_SET_PW2_PIN();
  TOSH_uwait(10);
  DebugI2CM$HighLevelTwiInterface$begin();
  return SUCCESS;
}

# 48 "/Users/wbennett/opt/MoteWorks/tos/interfaces/StdControl.nc"
inline static result_t RealMain$StdControl$start(void ){
#line 48
  unsigned char __nesc_result;
#line 48

#line 48
  __nesc_result = DebugI2CM$StdControl$start();
#line 48
  __nesc_result = rcombine(__nesc_result, TestGSMDriverM$StdControl$start());
#line 48

#line 48
  return __nesc_result;
#line 48
}
#line 48
# 61 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/HPLPowerManagementM.nc"
static inline uint8_t HPLPowerManagementM$getPowerLevel(void )
#line 61
{

  if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0x6E & (1 << 1)) {

      return 0;
    }
  else {
#line 67
    if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0x6F & (1 << 5)) {

        return 0;
      }
    else {
#line 71
      if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)(0x2C + 0x20) & (1 << 7)) {

          return 0;
        }
      else {
#line 90
        if (* (volatile uint8_t *)0XC9 & ((((1 << 7) | (1 << 6)) | (1 << 4)) | (1 << 3))) {

            return 0;
          }
        else {
#line 94
          if (* (volatile uint8_t *)(uint16_t )& * (volatile uint8_t *)0x7A & (1 << 7)) {

              return 1 << 1;
            }
          else {
#line 98
            if (* (volatile uint8_t *)0x70 & ((1 << 1) | (1 << 0))) {

                uint8_t diff;

#line 101
                diff = * (volatile uint8_t *)0xB3 - * (volatile uint8_t *)0xB2;
                if (diff < 16) {
#line 102
                  return ((1 << 3) | (1 << 2)) | (1 << 1);
                  }
                else {
#line 103
                  return (1 << 2) | (1 << 1);
                  }
              }
            else 
#line 105
              {

                return 1 << 2;
              }
            }
          }
        }
      }
    }
}

#line 114
static inline void HPLPowerManagementM$doAdjustment(void )
#line 114
{
  uint8_t foo;
#line 115
  uint8_t mcu;

#line 116
  foo = HPLPowerManagementM$getPowerLevel();
  mcu = * (volatile uint8_t *)(0x33 + 0x20);
  mcu &= 0xf1;
  if (foo == (((1 << 3) | (1 << 2)) | (1 << 1)) || foo == ((1 << 2) | (1 << 1))) {
      mcu |= 0;
      while ((* (volatile uint8_t *)0xB6 & 0x1f) != 0) {
           __asm volatile ("nop");}

      mcu &= 0xf1;
    }
  mcu |= foo;
  * (volatile uint8_t *)(0x33 + 0x20) = mcu;
  * (volatile uint8_t *)(0x33 + 0x20) |= 1 << 0;
}

# 166 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
static __inline void __nesc_enable_interrupt()
#line 166
{
   __asm volatile ("sei");}

#line 151
__inline  void __nesc_atomic_end(__nesc_atomic_t oldSreg)
{
  * (volatile uint8_t *)(0x3F + 0x20) = oldSreg;
}

#line 129
static inline void TOSH_wait()
{
   __asm volatile ("nop");
   __asm volatile ("nop");}

#line 158
static __inline void __nesc_atomic_sleep()
{

   __asm volatile ("sei");
   __asm volatile ("sleep");
  TOSH_wait();
}

#line 144
__inline  __nesc_atomic_t __nesc_atomic_start(void )
{
  __nesc_atomic_t result = * (volatile uint8_t *)(0x3F + 0x20);

#line 147
   __asm volatile ("cli");
  return result;
}

# 116 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
static inline bool TOSH_run_next_task()
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t old_full;
  void (*func)(void );

  fInterruptFlags = __nesc_atomic_start();
  old_full = TOSH_sched_full;
  func = TOSH_queue[old_full].tp;
  if (func == (void *)0) 
    {
      __nesc_atomic_sleep();
      return 0;
    }

  TOSH_queue[old_full].tp = (void *)0;
  TOSH_sched_full = (old_full + 1) & TOSH_TASK_BITMASK;
  __nesc_atomic_end(fInterruptFlags);
  func();

  return 1;
}

static inline void TOSH_run_task()
#line 139
{
  for (; ; ) 
    TOSH_run_next_task();
}

# 80 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline uint8_t HPLClock$Clock$getInterval(void )
#line 80
{
  return * (volatile uint8_t *)0xB3;
}

# 100 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static uint8_t TimerM$Clock$getInterval(void ){
#line 100
  unsigned char __nesc_result;
#line 100

#line 100
  __nesc_result = HPLClock$Clock$getInterval();
#line 100

#line 100
  return __nesc_result;
#line 100
}
#line 100
# 38 "/Users/wbennett/opt/MoteWorks/lib/avrtime.c"
static inline void add_time_millis(uint32_t time_to_add)
#line 38
{



  sys_time += time_to_add;
}

# 19 "/Users/wbennett/opt/MoteWorks/tos/interfaces/PowerManagement.nc"
inline static uint8_t TimerM$PowerManagement$adjustPower(void ){
#line 19
  unsigned char __nesc_result;
#line 19

#line 19
  __nesc_result = HPLPowerManagementM$PowerManagement$adjustPower();
#line 19

#line 19
  return __nesc_result;
#line 19
}
#line 19
# 70 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline void HPLClock$Clock$setInterval(uint8_t value)
#line 70
{
  * (volatile uint8_t *)0xB3 = value;
}

# 84 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static void TimerM$Clock$setInterval(uint8_t value){
#line 84
  HPLClock$Clock$setInterval(value);
#line 84
}
#line 84
# 136 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
static inline uint8_t HPLClock$Clock$readCounter(void )
#line 136
{
  return * (volatile uint8_t *)0xB2;
}

# 132 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static uint8_t TimerM$Clock$readCounter(void ){
#line 132
  unsigned char __nesc_result;
#line 132

#line 132
  __nesc_result = HPLClock$Clock$readCounter();
#line 132

#line 132
  return __nesc_result;
#line 132
}
#line 132
# 111 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
inline static void TimerM$adjustInterval(void )
#line 111
{
  uint8_t i;
#line 112
  uint8_t val = TimerM$maxTimerInterval;

#line 113
  if (TimerM$mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM$mState & (0x1L << i) && TimerM$mTimerList[i].ticksLeft < val) {
              val = TimerM$mTimerList[i].ticksLeft;
            }
        }
#line 130
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 130
        {
          i = TimerM$Clock$readCounter() + 3;
          if (val < i) {
              val = i;
            }
          TimerM$mInterval = val;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
        }
#line 138
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 141
        {
          TimerM$mInterval = TimerM$maxTimerInterval;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
        }
#line 145
        __nesc_atomic_end(__nesc_atomic); }
    }
  TimerM$PowerManagement$adjustPower();
}

#line 168
static inline void TimerM$enqueue(uint8_t value)
#line 168
{
  if (TimerM$queue_tail == NUM_TIMERS - 1) {
    TimerM$queue_tail = -1;
    }
#line 171
  TimerM$queue_tail++;
  TimerM$queue_size++;
  TimerM$queue[(uint8_t )TimerM$queue_tail] = value;
}

# 67 "TestGSMDriverM.nc"
static inline result_t TestGSMDriverM$Timer$fired(void )
#line 67
{
  TestGSMDriverM$transition();
  return SUCCESS;
}

# 164 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline result_t TimerM$Timer$default$fired(uint8_t id)
#line 164
{
  return SUCCESS;
}

# 51 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Timer.nc"
inline static result_t TimerM$Timer$fired(uint8_t arg_0x101391808){
#line 51
  unsigned char __nesc_result;
#line 51

#line 51
  switch (arg_0x101391808) {
#line 51
    case 0U:
#line 51
      __nesc_result = TestGSMDriverM$Timer$fired();
#line 51
      break;
#line 51
    default:
#line 51
      __nesc_result = TimerM$Timer$default$fired(arg_0x101391808);
#line 51
      break;
#line 51
    }
#line 51

#line 51
  return __nesc_result;
#line 51
}
#line 51
# 176 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static inline uint8_t TimerM$dequeue(void )
#line 176
{
  if (TimerM$queue_size == 0) {
    return NUM_TIMERS;
    }
#line 179
  if (TimerM$queue_head == NUM_TIMERS - 1) {
    TimerM$queue_head = -1;
    }
#line 181
  TimerM$queue_head++;
  TimerM$queue_size--;
  return TimerM$queue[(uint8_t )TimerM$queue_head];
}

static inline void TimerM$signalOneTimer(void )
#line 186
{
  uint8_t itimer = TimerM$dequeue();

#line 188
  if (itimer < NUM_TIMERS) {
    TimerM$Timer$fired(itimer);
    }
}

#line 192
static inline void TimerM$HandleFire(void )
#line 192
{
  uint8_t i;
  uint16_t int_out;


  TimerM$setIntervalFlag = 1;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 200
    {
      int_out = TimerM$interval_outstanding;
      TimerM$interval_outstanding = 0;
    }
#line 203
    __nesc_atomic_end(__nesc_atomic); }
  if (TimerM$mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM$mState & (0x1L << i)) {
              TimerM$mTimerList[i].ticksLeft -= int_out;
              if (TimerM$mTimerList[i].ticksLeft <= 2) {


                  if (TOS_post(TimerM$signalOneTimer)) {
                      if (TimerM$mTimerList[i].type == TIMER_REPEAT) {
                          TimerM$mTimerList[i].ticksLeft += TimerM$mTimerList[i].ticks;
                        }
                      else 
#line 214
                        {
                          TimerM$mState &= ~(0x1L << i);
                        }
                      TimerM$enqueue(i);
                    }
                  else {
                      {
                      }
#line 220
                      ;


                      TimerM$mTimerList[i].ticksLeft = TimerM$mInterval;
                    }
                }
            }
        }
    }


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 231
    int_out = TimerM$interval_outstanding;
#line 231
    __nesc_atomic_end(__nesc_atomic); }
  if (int_out == 0) {
    TimerM$adjustInterval();
    }
}

static inline result_t TimerM$Clock$fire(void )
#line 237
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 238
    {



      if (TimerM$interval_outstanding == 0) {
          TOS_post(TimerM$HandleFire);
        }
      else 
        {
        }
#line 246
      ;

      TimerM$interval_outstanding += TimerM$Clock$getInterval() + 1;


      add_time_millis(TimerM$Clock$getInterval());
    }
#line 252
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 159 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Clock.nc"
inline static result_t HPLClock$Clock$fire(void ){
#line 159
  unsigned char __nesc_result;
#line 159

#line 159
  __nesc_result = TimerM$Clock$fire();
#line 159

#line 159
  return __nesc_result;
#line 159
}
#line 159
# 99 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_SET_PW1_PIN()
#line 99
{
#line 99
  * (volatile uint8_t *)(0x08 + 0x20) |= 1 << 1;
}

#line 65
static __inline void TOSH_CLR_YELLOW_LED_PIN()
#line 65
{
#line 65
  * (volatile uint8_t *)(0X02 + 0x20) &= ~(1 << 0);
}

# 108 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$yellowOn(void )
#line 108
{
  {
  }
#line 109
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 110
    {
      TOSH_CLR_YELLOW_LED_PIN();
      LedsC$ledsOn |= LedsC$YELLOW_BIT;
    }
#line 113
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 93 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t TestGSMDriverM$Leds$yellowOn(void ){
#line 93
  unsigned char __nesc_result;
#line 93

#line 93
  __nesc_result = LedsC$Leds$yellowOn();
#line 93

#line 93
  return __nesc_result;
#line 93
}
#line 93
# 99 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/hardware.h"
static __inline void TOSH_CLR_PW1_PIN()
#line 99
{
#line 99
  * (volatile uint8_t *)(0x08 + 0x20) &= ~(1 << 1);
}

# 117 "/Users/wbennett/opt/MoteWorks/tos/system/LedsC.nc"
static inline result_t LedsC$Leds$yellowOff(void )
#line 117
{
  {
  }
#line 118
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 119
    {
      TOSH_SET_YELLOW_LED_PIN();
      LedsC$ledsOn &= ~LedsC$YELLOW_BIT;
    }
#line 122
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 101 "/Users/wbennett/opt/MoteWorks/tos/interfaces/Leds.nc"
inline static result_t TestGSMDriverM$Leds$yellowOff(void ){
#line 101
  unsigned char __nesc_result;
#line 101

#line 101
  __nesc_result = LedsC$Leds$yellowOff();
#line 101

#line 101
  return __nesc_result;
#line 101
}
#line 101
# 188 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiM.nc"
static inline void LowLevelTwiM$twi_reply(uint8_t ack)
{

  if (ack) {
      * (volatile uint8_t *)0xBC = (((1 << 2) | (1 << 0)) | (1 << 7)) | (1 << 6);
    }
  else 
#line 193
    {
      * (volatile uint8_t *)0xBC = ((1 << 2) | (1 << 0)) | (1 << 7);
    }
}

#line 225
static inline void LowLevelTwiM$twi_releaseBus(void )
{

  * (volatile uint8_t *)0xBC = (((1 << 2) | (1 << 0)) | (1 << 6)) | (1 << 7);


  LowLevelTwiM$twi_state = 0;
}

# 170 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/avrhardware.h"
static __inline void __nesc_disable_interrupt()
#line 170
{
   __asm volatile ("cli");}

# 82 "/Users/wbennett/opt/MoteWorks/tos/system/sched.c"
 bool TOS_post(void (*tp)())
#line 82
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t tmp;



  fInterruptFlags = __nesc_atomic_start();

  tmp = TOSH_sched_free;

  if (TOSH_queue[tmp].tp == (void *)0) {
      TOSH_sched_free = (tmp + 1) & TOSH_TASK_BITMASK;
      TOSH_queue[tmp].tp = tp;
      __nesc_atomic_end(fInterruptFlags);

      return TRUE;
    }
  else {
      __nesc_atomic_end(fInterruptFlags);

      return FALSE;
    }
}

# 34 "/Users/wbennett/opt/MoteWorks/tos/system/RealMain.nc"
  int main(void )
#line 34
{


  uint8_t local_symbol_ref;

  local_symbol_ref = TOS_PLATFORM;
  local_symbol_ref = TOS_BASE_STATION;
  local_symbol_ref = TOS_DATA_LENGTH;

  local_symbol_ref = TOS_ROUTE_PROTOCOL;








  RealMain$hardwareInit();
  RealMain$Pot$init(10);
  TOSH_sched_init();

  RealMain$StdControl$init();
  RealMain$StdControl$start();
  __nesc_enable_interrupt();

  while (1) {
      TOSH_run_task();
    }
}

# 52 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/DebugI2CM.nc"
static void DebugI2CM$printBufferTask(void )
#line 52
{
  static uint16_t i;





  DebugI2CM$HighLevelTwiInterface$beginTransmission(42 >> 1);
  for (i = 0; i < 32; i++) {
      if (twi_buffer_empty() == 1) {
          DebugI2CM$HighLevelTwiInterface$endTransmission();
          return;
        }
      else {
          DebugI2CM$HighLevelTwiInterface$send(twi_pull_char());
        }
    }
  DebugI2CM$HighLevelTwiInterface$endTransmission();
  if (twi_buffer_empty() == 0) {
      TOS_post(DebugI2CM$printBufferTask);
    }
}

# 65 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/HighLevelTwiM.nc"
static uint8_t HighLevelTwiM$HighLevelTwiInterface$endTransmission(void )
{

  int8_t ret = HighLevelTwiM$LowLevelTwiInterface$twi_writeTo(HighLevelTwiM$txAddress, HighLevelTwiM$txBuffer, HighLevelTwiM$txBufferLength, 1);

  HighLevelTwiM$txBufferIndex = 0;
  HighLevelTwiM$txBufferLength = 0;

  HighLevelTwiM$transmitting = 0;
  return ret;
}

# 80 "/Users/wbennett/opt/MoteWorks/lib/TimerM.nc"
static result_t TimerM$Timer$start(uint8_t id, char type, 
uint32_t interval)
#line 81
{
  uint8_t diff;

#line 83
  if (id >= NUM_TIMERS) {
#line 83
    return FAIL;
    }
#line 84
  if (type > TIMER_ONE_SHOT) {
#line 84
    return FAIL;
    }





  if (type == TIMER_REPEAT && interval <= 2) {
#line 91
    return FAIL;
    }
  TimerM$mTimerList[id].ticks = interval;
  TimerM$mTimerList[id].type = type;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 96
    {
      diff = TimerM$Clock$readCounter();
      interval += diff;
      TimerM$mTimerList[id].ticksLeft = interval;
      TimerM$mState |= 0x1L << id;
      if (interval < TimerM$mInterval) {
          TimerM$mInterval = interval;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
          TimerM$PowerManagement$adjustPower();
        }
    }
#line 107
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 134 "/Users/wbennett/opt/MoteWorks/tos/platform/micazc/HPLPowerManagementM.nc"
static uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void )
#line 134
{
  uint8_t mcu;

#line 136
  if (!HPLPowerManagementM$disabled) {
    TOS_post(HPLPowerManagementM$doAdjustment);
    }
  else 
#line 138
    {
      mcu = * (volatile uint8_t *)(0x33 + 0x20);
      mcu &= 0xf1;
      mcu |= 0;
      * (volatile uint8_t *)(0x33 + 0x20) = mcu;
      * (volatile uint8_t *)(0x33 + 0x20) |= 1 << 0;
    }
  return 0;
}

# 164 "/Users/wbennett/opt/MoteWorks/tos/platform/atm1281/HPLClock.nc"
  __attribute((interrupt)) void __vector_13(void )
#line 164
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 165
    {
      if (HPLClock$set_flag) {
          HPLClock$mscale = HPLClock$nextScale;
          * (volatile uint8_t *)0xB1 = HPLClock$nextScale;
          * (volatile uint8_t *)0xB3 = HPLClock$minterval;
          HPLClock$set_flag = 0;
        }
    }
#line 172
    __nesc_atomic_end(__nesc_atomic); }
  HPLClock$Clock$fire();
}

# 72 "TestGSMDriverM.nc"
static void TestGSMDriverM$transition(void )
#line 72
{
  uint8_t lstate = TestGSMDriverM$state;

#line 74
  switch (lstate) {
      case TestGSMDriverM$HARD_TOGGLE: 
        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 76
          TestGSMDriverM$state = TestGSMDriverM$SOFT_TOGGLE;
#line 76
          __nesc_atomic_end(__nesc_atomic); }
      TOSH_SET_PW1_PIN();
      TestGSMDriverM$Leds$yellowOn();
      TestGSMDriverM$Timer$start(TIMER_ONE_SHOT, 2000);
      break;
      case TestGSMDriverM$SOFT_TOGGLE: 
        { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 82
          TestGSMDriverM$state = TestGSMDriverM$DONE;
#line 82
          __nesc_atomic_end(__nesc_atomic); }
      TOSH_CLR_PW1_PIN();
      TestGSMDriverM$Leds$yellowOff();
      TestGSMDriverM$transition();
      break;
      case TestGSMDriverM$DONE: 
        break;
      default: 
        break;
    }
}

# 234 "/Users/wbennett/opt/MoteWorks/tos/sensorboards/CraneSensor300/LowLevelTwiM.nc"
  __attribute((signal)) void __vector_39(void )
{
  switch (* (volatile uint8_t *)0xB9 & (((((1 << 7) | (1 << 6)) | (1 << 5)) | (1 << 4)) | (1 << 3))) {

      case 0x08: 
        case 0x10: 

          * (volatile uint8_t *)0xBB = LowLevelTwiM$twi_slarw;
      LowLevelTwiM$twi_reply(1);
      break;


      case 0x18: 
        case 0x28: 

          if (LowLevelTwiM$twi_masterBufferIndex < LowLevelTwiM$twi_masterBufferLength) {

              * (volatile uint8_t *)0xBB = LowLevelTwiM$twi_masterBuffer[LowLevelTwiM$twi_masterBufferIndex++];
              LowLevelTwiM$twi_reply(1);
            }
          else 
#line 253
            {
              LowLevelTwiM$twi_stop();
            }
      break;
      case 0x20: 
        LowLevelTwiM$twi_error = 0x20;
      LowLevelTwiM$twi_stop();
      break;
      case 0x30: 
        LowLevelTwiM$twi_error = 0x30;
      LowLevelTwiM$twi_stop();
      break;
      case 0x38: 
        LowLevelTwiM$twi_error = 0x38;
      LowLevelTwiM$twi_releaseBus();
      break;


      case 0x50: 

        LowLevelTwiM$twi_masterBuffer[LowLevelTwiM$twi_masterBufferIndex++] = * (volatile uint8_t *)0xBB;
      case 0x40: 

        if (LowLevelTwiM$twi_masterBufferIndex < LowLevelTwiM$twi_masterBufferLength) {
            LowLevelTwiM$twi_reply(1);
          }
        else 
#line 278
          {
            LowLevelTwiM$twi_reply(0);
          }
      break;
      case 0x58: 

        LowLevelTwiM$twi_masterBuffer[LowLevelTwiM$twi_masterBufferIndex++] = * (volatile uint8_t *)0xBB;
      case 0x48: 
        LowLevelTwiM$twi_stop();
      break;



      case 0xF8: 
        break;
      case 0x00: 
        LowLevelTwiM$twi_error = 0x00;
      LowLevelTwiM$twi_stop();
      break;
    }
}

#line 204
static void LowLevelTwiM$twi_stop(void )
{

  * (volatile uint8_t *)0xBC = ((((1 << 2) | (1 << 0)) | (1 << 6)) | (1 << 7)) | (1 << 4);



  while (* (volatile uint8_t *)0xBC & (1 << 4)) {
      continue;
    }


  LowLevelTwiM$twi_state = 0;
}


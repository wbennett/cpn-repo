/*
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
#include <stdio.h>
#include "Tracker/gsm.h"
module TestGSMDriverM 
{
  provides 
	{
		interface StdControl;
  }
  uses 
	{
		#ifdef I2CDBG
		I2CDBG_CONFIG();
		#endif
		interface StdControl as TimerControl;
		interface Timer;
		interface Leds;
  }
}
implementation 
{

	extern int sprintf(char *__s, const char *__fmt,...)__attribute__((C));
	
	uint8_t state;
	enum {
		INIT,
		HARD_TOGGLE,
		SOFT_TOGGLE,
		DONE,
	};
	
	void transition();
	
	command result_t StdControl.init()	{
		atomic state = INIT;
		call TimerControl.init();
		call Leds.init();
		return SUCCESS;
	}
	
	command result_t StdControl.start() {
		call TimerControl.start();
		//enable the hardware line
		atomic state = HARD_TOGGLE;
		TOSH_SET_PW0_PIN();
		call Leds.redOn();
		call Timer.start(TIMER_ONE_SHOT, 1000);
		return SUCCESS;
	}
	
	command result_t StdControl.stop()	{
		call TimerControl.stop();
		call Timer.stop();
		return SUCCESS;
	}
	

	
	event result_t Timer.fired() {
		transition();
		return SUCCESS;
	}
	
	void transition() {
		uint8_t lstate = state;
		switch(lstate) {
			case HARD_TOGGLE:
				atomic state = SOFT_TOGGLE;
				TOSH_SET_PW1_PIN();
				call Leds.yellowOn();
				call Timer.start(TIMER_ONE_SHOT,2000);
				break;
			case SOFT_TOGGLE:
				atomic state = DONE;
				TOSH_CLR_PW1_PIN();
				call Leds.yellowOff();
				transition();
				break;
			case DONE:
				break;
			default:
				break;
		}
	}
}



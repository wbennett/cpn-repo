/*
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
#include "GpsToRadio.h"
configuration GpsToRadio
{
}
implementation
{
	components Main, GpsToRadioM, TimerC, LedsC;

	components GenericComm as Comm;

	Main.StdControl -> GpsToRadioM.StdControl;
	GpsToRadioM.RadioControl -> Comm;
	GpsToRadioM.SendMsg -> Comm.SendMsg[GPS_TO_RADIO_MSG];
	
	GpsToRadioM.TimerControl -> TimerC.StdControl;
	GpsToRadioM.Timer -> TimerC.Timer[unique("Timer")];
	GpsToRadioM.Leds -> LedsC.Leds;
	//for gps
	components GpsDriver;
	GpsToRadioM.GpsControl -> GpsDriver.GpsSplitControl;
	GpsToRadioM.GpsGGA -> GpsDriver.GpsGGARecv;
	GpsToRadioM.GpsRMC -> GpsDriver.GpsRMCRecv;
}


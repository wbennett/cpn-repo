/*
 *
 *
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
#include "Tracker/gps.h"
#include "GpsToRadio.h"
module GpsToRadioM 
{
  provides 
	{
		interface StdControl;
  }
  uses 
	{
    interface StdControl as RadioControl;
		interface SendMsg;
		interface StdControl as TimerControl;
		interface Timer;
		interface Leds;
		//gps
		interface SplitControl as GpsControl;
		interface ReceiveMsg as GpsGGA;
		interface ReceiveMsg as GpsRMC;
	}
}
implementation 
{
	enum {
		S_INIT,
		S_SAMPLE,
		S_SEND_DATA,
	};
	uint8_t state;
	bool radio_busy;
	bool fresh_sample;
	TOS_Msg tempMsg;

	bool gga_valid;
	GGAMsg gga_msg_buffer;
	gps_to_radio_t gps_radio_msg;


	task void sendResults();

	command result_t StdControl.init()	{
		call TimerControl.init();
		call Leds.init();
		call RadioControl.init();
		radio_busy = FALSE;
		state = S_INIT;
		return SUCCESS;
	}

	event result_t GpsControl.initDone() {
		gga_valid = FALSE;
		call GpsControl.start();
	}

	event result_t GpsControl.startDone() {
		call Leds.set(7);
	}

	event result_t GpsControl.stopDone() {

	}

	event TOS_MsgPtr GpsGGA.receive(TOS_MsgPtr msgPtr) {
		gga_msg_buffer = *((GGAMsg *)msgPtr);
			call Leds.redToggle();
		if(gga_msg_buffer.valid >= 1 && gga_msg_buffer.valid <=6) {
			gga_valid = TRUE;
			//send the data now
			//convert to senseable lat lon
			gps_radio_msg.lat = (double)gga_msg_buffer.Lat_deg + 
				((double)gga_msg_buffer.Lat_dec_min/10000.0f)/60.0f;
			gps_radio_msg.lon = (-1.0f)*((double)gga_msg_buffer.Lon_deg + 
				((double)gga_msg_buffer.Lon_dec_min/10000.0f)/60.0f);
			//send message
			post sendResults();
		}
		return msgPtr;
	}

	event TOS_MsgPtr GpsRMC.receive(TOS_MsgPtr msgPtr) {
		return msgPtr;
	}
	
	command result_t StdControl.start() {
		call TimerControl.start();
		call RadioControl.start();
		call GpsControl.init();
		return SUCCESS;
	}
	
	command result_t StdControl.stop()	{
		call TimerControl.stop();
		call RadioControl.stop();
		call Timer.stop();
		return SUCCESS;
	}

	task void sendResults() {
		if(radio_busy == FALSE) {
			radio_busy = TRUE;
			//pack the message and send it
			memcpy(tempMsg.data,&gps_radio_msg,sizeof(gps_to_radio_t));

			if(call SendMsg.send(GPS_TO_RADIO_BASE_ADDR,
					sizeof(gps_to_radio_t),
					&tempMsg) == FAIL) {
				
				call Leds.yellowToggle();
				radio_busy = FALSE;
				post sendResults();
			}
		}
	}
	
	event result_t Timer.fired() {
		return SUCCESS;
	}

	event result_t SendMsg.sendDone(TOS_MsgPtr msg, result_t success) {
		radio_busy = FALSE;	
		if(success == SUCCESS) {
			call Leds.greenToggle();
		}
		return SUCCESS;
	}

}


#ifndef GPS_TO_RADIO_INCLUDED
#define GPS_TO_RADIO_INCLUDED
enum {
	GPS_TO_RADIO_MSG = 0x77,
	GPS_TO_RADIO_BASE_ADDR = 0x00
	
};

typedef struct gps_to_radio {
	uint8_t sender_rssi;
 	double lat;
	double lon;
}__attribute__ ((packed)) gps_to_radio_t;

#endif

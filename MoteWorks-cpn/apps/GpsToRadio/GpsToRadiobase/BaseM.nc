/*
 * BaseM.nc
 *
 * Sends an 8-bit global struct orientation
 * <header>Data<header>
 *
 **/
#include "GpsToRadio.h"
#include "debug_constants.h"
module BaseM {
  provides interface StdControl;
  uses {
    interface Leds;
    interface ReceiveMsg;
  }
}
implementation {

	
	command result_t StdControl.init() {
    call Leds.init();     
		call Leds.redToggle();
    return SUCCESS;
  }


  command result_t StdControl.start() {
    return SUCCESS;
  }

  command result_t StdControl.stop() {
    return SUCCESS;
  }
  #define RSSI_BASE_VAL ((int8_t)-91)
  event TOS_MsgPtr ReceiveMsg.receive(TOS_MsgPtr m) {

		/*
		 * 8-bit register the value 
		 * //lowest 5 bits
		 * 0-28
		 *
		 */
		int prf;
		uint8_t rssi;
    gps_to_radio_t *message;
		message= (gps_to_radio_t*)m->data;
		rssi = (uint8_t)m->strength;
		prf = RSSI_BASE_VAL + (3*rssi-1);

		call Leds.yellowToggle();
		dprintf("rssi(dbm):%i\tlat:%f\tlon:%f\n",
			prf,
			message->lat,
			message->lon);
    return m;
  }
   
}


# To change this template, choose Tools | Templates
# and open the template in the editor.

puts "Fixing aldo dump..."

f = File.open(ARGV[0],"r")
o = File.open(ARGV[1],"w")

lines = []

f.each_line do |line|
  #line is one big string
  alltokens = line.split(",")
  currenttokens = []

  while !alltokens.empty? do
    #get the first 27 tokens
    puts "all tokens " + alltokens.length.to_s
    currenttokens = alltokens.shift(27)
    #checck if empty
    new_line = ""
    if alltokens.empty?
      temp = currenttokens.shift(26)
      puts "#{temp}\n"
      temp.each { |item|  
        new_line << item + ","
      }
      new_line << currenttokens.shift + "\n"
      o.write(new_line)
      break;
    end

    #concat the first 26 elements
    puts "current tokens " + currenttokens.length.to_s
    temp = currenttokens.shift(26)
    #puts "temp len" + temp.length.to_s
    #puts "#{temp}\n"
    temp.each do |element|
      #puts element
      new_line << element.to_s + ","
    end
    #badelement
    badelement = currenttokens.shift
    #puts badelement.to_s.class
    #puts "bad" + badelement
    #get the last char and tag on front of all tokens
    if badelement != "255255" 
      realfirstnode = badelement.slice!(badelement.length-1)
      char = "%c" % realfirstnode
    else
      realfirstnode = badelement.slice!(badelement.length-3..badelement.length-1)
      char = realfirstnode
    end
    
    #puts "first-ele:#{char}"
    #puts "new"+badelement
    alltokens.insert(0,char)
    #concat the rest onto the newline
    new_line << badelement + "\n"
    puts new_line
    o.write(new_line)
  end
end
f.close
o.close
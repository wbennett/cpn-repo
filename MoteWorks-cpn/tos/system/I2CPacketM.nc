/*
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: I2CPacketM.nc,v 1.1.4.1 2007/04/27 06:01:01 njain Exp $
 */

/*
 *
 * Authors:		Joe Polastre, Rob Szewczyk
 * Date last modified:  7/18/02
 * Modified on : 08/14/2003 by Mohammad Rahimi for some minor 
 * synchronization problems.
 */

/**
 * Provides functionality for writing and reading packets on the I2C bus
 */
module I2CPacketM
{
  provides {
    interface StdControl;
    interface I2CPacket[uint8_t id];
  }
  uses {
    interface I2C;
    interface StdControl as I2CStdControl;
    interface Leds;
  }
}

implementation
{
    #define SO_DEBUG 1
    #include "SOdebug.h"
  /* gstate of the i2c request  */
  enum {IDLE=99,
        I2C_START_COMMAND=1,
        I2C_STOP_COMMAND=2,
        I2C_STOP_COMMAND_SENT=3,
        I2C_WRITE_ADDRESS=10,
        I2C_WRITE_DATA=11,
        I2C_READ_ADDRESS=20,
        I2C_READ_DATA=21,
	I2C_READ_DONE=22};

  enum {STOP_FLAG=0x01, /* send stop command at the end of packet? */
        ACK_FLAG =0x02, /* send ack after recv a byte (except for last byte) */
        ACK_END_FLAG=0x04, /* send ack after last byte recv'd */ 
        ADDR_8BITS_FLAG=0x80, // the address is a full 8-bits with no terminating readflag
       };

  /**
   *  bytes to write to the i2c bus 
   */
  char* data;    

  /**
   * length in bytes of the request 
   */
  char length;   

  /**
   * current index of read/write byte 
   */
  char index;    

  /** 
   * current gstate of the i2c request 
   */
  static char gstate;   //wpb 

  /**
   * destination address 
   */
  char addr;     
  
  /**
   * store flags 
   */
  char flags;    

  /**
   * cache incoming bytes : 10 is a random number
   */
  char temp[10]; 

  /**
   * initialize the I2C bus and set initial gstate
   */

command result_t StdControl.init() {
    
    call I2CStdControl.init();
    atomic {
        gstate = IDLE;
        index = 0;
    }
    return SUCCESS;
  }
 
/**
   * start the component 
   **/
command result_t StdControl.start() {
     return SUCCESS;
 }
 
 /**
   * stop the component
   **/
command result_t StdControl.stop() {
     return SUCCESS;
  }
 
  /**
   * writes a series of bytes out to the I2C bus 
   *
   * @param in_length number of bytes to be written to the bus
   * @param in_data pointer to the data
   * @param in_flags bitmask of flags (see I2CPacket.ti interface)
   *
   * @return returns SUCCESS if the bus is free and the request is accepted.
   */
  command result_t I2CPacket.writePacket[uint8_t id](char in_length,char* in_data, char in_flags) {      
      
      uint8_t status;
      
      
      //if(gstate == 0)//wpb
        //atomic gstate = IDLE;//wpb
      atomic {
      status = FALSE;
      if (gstate == IDLE)
          {
            SODbg(1,"i2cpacket.writepacket:addr:%d,data:%d\n",id,*in_data);
              /*  reset variables  */
              addr = id;
              data = in_data;
              index = 0;
              length = in_length;
              flags = in_flags;
              gstate = I2C_WRITE_ADDRESS;
              SODbg(1,"i2c gstate_Change:%d\n",gstate);
              status = TRUE;
          }
      }
      if(status == FALSE ) {
            SODbg(1,"i2cpacket.writepacket:gstate:%c\n",gstate);
          SODbg(1,"i2cpacket.writepacket.status == false\n");
          return FAIL;
      }
      
      
      if (call I2C.sendStart())
          {
              return SUCCESS;
          }
      else
          {
              atomic { gstate = IDLE; }
              SODbg(1,"i2c gstate_Change:%d\n",gstate);
//                SODbg(1,"i2cpacket.writepacket.SENDSTART FAILED\n");
              return FAIL;
          }
  }
  
  /**
   * reads a series of bytes out to the I2C bus 
   *
   * @param in_length number of bytes to be read from the bus
   * @param in_flags bitmask of flags (see I2CPacket.ti interface)
   *
   * @return returns SUCCESS if the bus is free and the request is accepted.
   */
 command result_t I2CPacket.readPacket[uint8_t id](char in_length, 
                                                    char in_flags) {
      uint8_t status;
     // SODbg(1,"I2CP:readpacket:gstate:%c",gstate);
      atomic {
      status = FALSE;
      if (gstate == IDLE)
          {
               // SODbg(1,"I2CPacket:readpacket()\n");
              addr = id;
              index = 0;
              length = in_length;
              flags = in_flags;
              gstate = I2C_READ_ADDRESS;
              SODbg(1,"i2c gstate_Change:%d\n",gstate);
              status = TRUE;
          }
      }
      if(status == FALSE ) {
            SODbg(1,"I@Cpacket:readpacket():gstate==%c\n",gstate);
            SODbg(1,"I@Cpacket:readpacket():gstateus == false\n");
          return FAIL;
      }
      
      if (call I2C.sendStart())
          {
              return SUCCESS;
          }
      else
          {
              atomic { gstate = IDLE; }
               SODbg(1,"I@Cpacket:sendstart():fail\n");
              return FAIL;
          }
  }
  
  /**
   * notification that the start symbol was sent 
   **/
event result_t I2C.sendStartDone() {
        //SODbg(1,"i2cpacketm:sendstartdone\n");
        if(gstate == I2C_WRITE_ADDRESS){
            SODbg(1,"i2cpacketm:sendstartdone:i2cwriteaddress:addr:%d\n",addr); 
            gstate = I2C_WRITE_DATA;
   //         SODbg(1,"i2c gstate_Change:%d\n",gstate);
            SODbg(1,"writing:%d or %d\n",addr,((addr << 1)+0));
            call I2C.write( (flags & ADDR_8BITS_FLAG) ? addr : ((addr << 1) + 0) );
            //call I2C.write(addr);
        }
        else if (gstate == I2C_READ_ADDRESS){
            SODbg(1,"i2cpacketm:sendstartdone:i2creadaddress:addr:%d\n",addr);
            gstate = I2C_READ_DATA;
            SODbg(1,"i2c gstate_Change:%d\n",gstate);
            call I2C.write( (flags & ADDR_8BITS_FLAG) ? addr : ((addr << 1) + 1) );
            //call I2C.write(addr);
            index++;
        }
    return 1;
}
 
  /**
   * notification that the stop symbol was sent 
   **/
event result_t I2C.sendEndDone() {
    char* out_data;    
    char out_length;   
    char out_addr;     
    //SODbg(1,"i2c:sendEndDone");
    out_addr=addr;
    out_length=length;
    out_data=data;
    //SODbg(1,"SEND END DONE\n");
    if (gstate == I2C_STOP_COMMAND_SENT) {
        // success!
        gstate = IDLE;
        SODbg(1,"i2c gstate_Change:%d\n",gstate);
        signal I2CPacket.writePacketDone[out_addr](SUCCESS);
    }
    else if (gstate == I2C_READ_DONE) {
        gstate = IDLE;
        SODbg(1,"i2c gstate_Change:%d\n",gstate);
        signal I2CPacket.readPacketDone[out_addr](out_length, out_data);
    }

    return SUCCESS;
    
    /*
      if (gstate == I2C_STOP_COMMAND_SENT) {
      // success!
      gstate = IDLE;
      signal I2CPacket.writePacketDone[addr](SUCCESS);
      }
      else if (gstate == I2C_READ_DONE) {
      gstate = IDLE;
      signal I2CPacket.readPacketDone[addr](length, data);
      }
      return SUCCESS;
    */
}
 
  /**
   * notification of a byte sucessfully written to the bus 
   **/
event result_t I2C.writeDone(bool result) {
      //SODbg(1,"i2cpacketm:i2c:writedone\n");
      if(result == FAIL) {
          SODbg(1,"i2cpacketm:writedone:fail\n");
          gstate = IDLE;
          SODbg(1,"i2c gstate_Change:%d\n",gstate);
          signal I2CPacket.writePacketDone[addr](FAIL);
          return FAIL;
      }
      if ((gstate == I2C_WRITE_DATA) && (index < length))
          {
              index++;
              if (index == length) {
                  gstate = I2C_STOP_COMMAND;
                  SODbg(1,"i2c gstate_Change:%d\n",gstate);
              }
              return call I2C.write(data[index-1]);
          }
      else if (gstate == I2C_STOP_COMMAND)
          {
              gstate = I2C_STOP_COMMAND_SENT;
              SODbg(1,"i2c gstate_Change:%d\n",gstate);
              if (flags & STOP_FLAG)
                  {
                      return call I2C.sendEnd();
                  }
              else {
                 gstate = IDLE;
                 SODbg(1,"i2c gstate_Change:%d\n",gstate);
                  return signal I2CPacket.writePacketDone[addr](SUCCESS);
              }
          }
      else if (gstate == I2C_READ_DATA)
          {
              if (index == length)
                  {
                      return call I2C.read((flags & ACK_END_FLAG) == ACK_END_FLAG);
                  }
              else if (index < length)
                  return call I2C.read((flags & ACK_FLAG) == ACK_FLAG);
          }
      
      return SUCCESS;
  }


  /**
   * read a byte off the bus and add it to the packet 
   **/
event result_t I2C.readDone(char in_data) {
    temp[index-1] = in_data;
    index++;
    if (index == length)
        call I2C.read((flags & ACK_END_FLAG) == ACK_END_FLAG);
    else if (index < length)
        call I2C.read((flags & ACK_FLAG) == ACK_FLAG);
    else if (index > length)
          {
              gstate = I2C_READ_DONE;
              data = (char*)(&temp);
              if (flags & STOP_FLAG)
                  call I2C.sendEnd();
              else
                  {
                      //SODbg(1,"i2cpacket.gstate getting set idle\n");
                      gstate = IDLE;
                      SODbg(1,"i2c gstate_Change:%d\n",gstate);
                      signal I2CPacket.readPacketDone[addr](length, data);
                  }
          }
      return SUCCESS;
  }
  
  default event result_t I2CPacket.readPacketDone[uint8_t id](char in_length, char* in_data) {
    return SUCCESS;
  }

  default event result_t I2CPacket.writePacketDone[uint8_t id](bool result) {
    return SUCCESS;
  }

}


includes gps;

interface GSM_GPSI {

  command result_t init();
  
  command result_t start();
  
  command result_t stop();
  
  command result_t powerOn();
  
  command result_t powerOff();
  
  command result_t setBaud();
  
  command result_t verboseErrors();

  command result_t warmStartGps();
    
  command result_t smsMode();
  
  command result_t sendSms(uint8_t *number, uint8_t *text);//number and text are null-term strings
  
  command result_t signalStrength();
  
  command result_t getAqrdGpsPos();
  
  event result_t powerOnDone();
  
  event result_t powerOffDone();
  
  event result_t setBaudDone(General_MsgPtr generalMsgP);
  
  event result_t verboseErrorsDone(General_MsgPtr generalMsgP);

  event result_t warmStartGpsDone(General_MsgPtr generalMsgP);
    
  event result_t smsModeDone(General_MsgPtr generalMsgP);
  
  event result_t sendSmsDone(General_MsgPtr generalMsgP);
  
  event result_t signalStrengthDone(General_MsgPtr generalMsgP);
  
  event result_t getAqrdGpsPosDone(General_MsgPtr generalMsgP, result_t valid);
  
}

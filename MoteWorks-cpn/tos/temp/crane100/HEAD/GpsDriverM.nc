//57600 baud for gps
//115200 baud for iris/sodbg

module GpsDriverM {
  provides {
    interface GSM_GPSI as GSM_GPSI;
  }
  uses {
    interface StdControl as GpsControl;
    interface SendVarLenPacketGSM as SendVarLenPacketGSM;
    interface ReceiveMsg as Receive;
	interface Timer;
	interface Leds;
  }
}
implementation {

  #define DBG_PKT 1
  #define SO_DEBUG 1
  #include "SOdebug.h"
  
  #include "gps.h"
  
  
  enum {
    GSM_ON_BIT = 1,
	GSM_TRANSITION_BIT = 2
  };
  uint8_t state;
  
  enum {
    PW5_HIGH,
	PW5_LOW
  };
  uint8_t pinState;
  
  enum {
    NONE,
	SET_BAUD,
	VERBOSE_ERRORS,
    WARM_START_GPS,
	SMS_MODE,
	SEND_SMS,
	SIGNAL_STRENGTH,
	GET_AQRD_GPS_POS
  };
  uint8_t curCmdState;	

  result_t status;
  
  General_MsgPtr genPtr;
  General_Msg returnedMsg;
  
  General_Msg nextMsg;
  
  uint16_t i;
  
  uint8_t sendSmsCount;
  
  //==================== Commands ===========================//
  //Each string terminated by NULL char but don't want to send that
  uint8_t SET_BAUD_CMD[14] = "AT+IPR=57600\r";
  uint8_t VERBOSE_ERRORS_CMD[11] = "AT+CMEE=2\r"; //report errors in verbose format
  uint8_t WARM_START_GPS_CMD[11] = "AT$GPSR=2\r";
  uint8_t SMS_MODE_CMD[11] = "AT+CMGF=1\r";
  uint8_t SEND_SMS_CMD[22] = "AT+CMGS=\"";
  uint8_t SEND_SMS_CMD_FULL[200];
  uint8_t SMS_TEXT[200];
  uint8_t SIGNAL_STRENGTH_CMD[8] = "AT+CSQ\r";
  uint8_t GET_AQRD_GPS_POS_CMD[11] = "AT$GPSACP\r";
  
  
  void powerToggle() {
    pinState = PW5_HIGH;
	SODbg(DBG_PKT, "Setting PW5 High\n");
    TOSH_SET_PW5_PIN();
	call Timer.start(TIMER_ONE_SHOT,2000);
	return;
  }    

  command result_t GSM_GPSI.init() {
    status = call GpsControl.init();
	state = 0;
	curCmdState = NONE;
	pinState = PW5_LOW;
	call Leds.init();
	return SUCCESS;
  }
  
  command result_t GSM_GPSI.start() {
    status = call GpsControl.start();
	//SODbg(DBG_PKT, "StartStat: %i\n", status);
    return SUCCESS;
  }
  
  command result_t GSM_GPSI.stop() {
    call GpsControl.stop();
    return SUCCESS;
  }
  
  command result_t GSM_GPSI.powerOn() {
    if(state & GSM_ON_BIT) {
	  return FAIL;
	}
	else {
	  state |= GSM_TRANSITION_BIT;//GSM transitioning
      powerToggle();
	  return SUCCESS;
	}
  }
  
  command result_t GSM_GPSI.powerOff() {
    if(state & GSM_ON_BIT) {
	  state |= GSM_TRANSITION_BIT;//GSM transitioning
	  powerToggle();
	  return SUCCESS;
	}
	else {
	  return FAIL;
	}
  }
  
  void task gsmOffTask() {
    state &= ~GSM_ON_BIT;//GSM Off
	state &= ~GSM_TRANSITION_BIT;//GSM not transitioning
    signal GSM_GPSI.powerOffDone();
  }
  
  void task gsmOnTask() {
    state |= GSM_ON_BIT;//GSM On
	state &= ~GSM_TRANSITION_BIT;//GSM not transitioning
    signal GSM_GPSI.powerOnDone();
  }
  
  command result_t GSM_GPSI.setBaud() {
    if((state & GSM_ON_BIT) && (curCmdState == NONE)) {
	  curCmdState = SET_BAUD;
	  status = call SendVarLenPacketGSM.send(SET_BAUD_CMD,strlen(SET_BAUD_CMD),AT_GEN_MSG);
	  if(status == FAIL) {
	    curCmdState = NONE;
	  }
	  return status;
	}
	else {
	  return FAIL;
	}
  }
  
  command result_t GSM_GPSI.verboseErrors() {
    if((state & GSM_ON_BIT) && (curCmdState == NONE)) {
	  curCmdState = VERBOSE_ERRORS;
	  status = call SendVarLenPacketGSM.send(VERBOSE_ERRORS_CMD,strlen(VERBOSE_ERRORS_CMD),AT_GEN_MSG);
	  if(status == FAIL) {
	    curCmdState = NONE;
	  }
	  return status;
	}
	else {
	  return FAIL;
	}
  }

  command result_t GSM_GPSI.warmStartGps() {
    if((state & GSM_ON_BIT) && (curCmdState == NONE)) {
	  curCmdState = WARM_START_GPS;
	  status = call SendVarLenPacketGSM.send(WARM_START_GPS_CMD,strlen(WARM_START_GPS_CMD),AT_GEN_MSG);
	  if(status == FAIL) {
	    curCmdState = NONE;
	  }
	  return status;
	}
	else {
	  return FAIL;
	}
  }
    
  command result_t GSM_GPSI.smsMode() {
    if((state & GSM_ON_BIT) && (curCmdState == NONE)) {
	  curCmdState = SMS_MODE;
	  status = call SendVarLenPacketGSM.send(SMS_MODE_CMD,strlen(SMS_MODE_CMD),AT_GEN_MSG);
	  if(status == FAIL) {
	    curCmdState = NONE;
	  }
	  return status;
	}
	else {
	  return FAIL;
	}
  }
  
  command result_t GSM_GPSI.sendSms(uint8_t *number, uint8_t *text) {
    if((state & GSM_ON_BIT) && (curCmdState == NONE)) {
	  curCmdState = SEND_SMS;
	  strcpy(SEND_SMS_CMD_FULL,SEND_SMS_CMD);
	  strcat(SEND_SMS_CMD_FULL,number);
	  strcat(SEND_SMS_CMD_FULL,"\"\r");
	  
	  strcpy(SMS_TEXT,text);
	  strcat(SMS_TEXT,"\r\x1A");
	  
	  atomic {
	    sendSmsCount = 0;
	    status = call SendVarLenPacketGSM.send(SEND_SMS_CMD_FULL,strlen(SEND_SMS_CMD_FULL),AT_CARROT_MSG);
	  }
	  if(status == FAIL) {
	    curCmdState = NONE;
	  }
	  return status;
	}
	else {
	  return FAIL;
	}
  }
  
  command result_t GSM_GPSI.signalStrength() {
    if((state & GSM_ON_BIT) && (curCmdState == NONE)) {
	  curCmdState = SIGNAL_STRENGTH;
	  status = call SendVarLenPacketGSM.send(SIGNAL_STRENGTH_CMD,strlen(SIGNAL_STRENGTH_CMD),AT_GEN_MSG);
	  if(status == FAIL) {
	    curCmdState = NONE;
	  }
	  return status;
	}
	else {
	  return FAIL;
	}
  }
  
  command result_t GSM_GPSI.getAqrdGpsPos() {
    if((state & GSM_ON_BIT) && (curCmdState == NONE)) {
	  curCmdState = GET_AQRD_GPS_POS;
	  status = call SendVarLenPacketGSM.send(GET_AQRD_GPS_POS_CMD,strlen(GET_AQRD_GPS_POS_CMD),AT_GPS_MSG);
	  if(status == FAIL) {
	    curCmdState = NONE;
	  }
	  return status;
	}
	else {
	  return FAIL;
	}
  }
  
  event result_t Timer.fired()
  {
    if((state & GSM_TRANSITION_BIT) && pinState==PW5_HIGH) {
	  pinState = PW5_LOW;
	  SODbg(DBG_PKT, "Setting PW5 low\n");
	  TOSH_CLR_PW5_PIN();
	  if(state & GSM_ON_BIT) {
	    call Timer.start(TIMER_ONE_SHOT,1000);//wait for unit to shutdown
	  }
	  else {
	    call Timer.start(TIMER_ONE_SHOT,6000);//wait for unit to boot
	  }
	}
	else if((state & GSM_TRANSITION_BIT) && pinState==PW5_LOW) {
	  if(state & GSM_ON_BIT) {
		post gsmOffTask();
	  }
	  else {
		post gsmOnTask();
	  }
	}	    
	return SUCCESS;
  }
  
  event result_t SendVarLenPacketGSM.sendDone(uint8_t *sentData, uint8_t numBytes) {
	return SUCCESS;
  }
  
  void task setBaudDoneTask() {
	curCmdState = NONE;
	signal GSM_GPSI.setBaudDone(&returnedMsg);
  }
  
  void task verboseErrorsDoneTask() {    
	curCmdState = NONE;
	signal GSM_GPSI.verboseErrorsDone(&returnedMsg);
  }

  void task warmStartGpsDoneTask() {    
	curCmdState = NONE;
	signal GSM_GPSI.warmStartGpsDone(&returnedMsg);
  }
  
  void task smsModeDoneTask() {    
	curCmdState = NONE;
	signal GSM_GPSI.smsModeDone(&returnedMsg);
  }
  
  void task sendSmsDoneTask() {    
	curCmdState = NONE;
	signal GSM_GPSI.sendSmsDone(&returnedMsg);
  }
  
  void task signalStrengthDoneTask() {    
	curCmdState = NONE;
	signal GSM_GPSI.signalStrengthDone(&returnedMsg);
  }
  
  void task getAqrdGpsPosDoneTask() {
    result_t isValid = SUCCESS;
    for(i=1; i<returnedMsg.length; i++) {
	  if((returnedMsg.data[i-1] == ',') && (returnedMsg.data[i] == ',')) {
	    isValid = FAIL;
		break;
	  }
	}
	curCmdState = NONE;
	signal GSM_GPSI.getAqrdGpsPosDone(&returnedMsg,isValid);
  }
  
  event TOS_MsgPtr Receive.receive(TOS_MsgPtr bufferPtr) {
	genPtr = (General_MsgPtr) bufferPtr;
	//copy to new location so data doesn't get overwritten by another msg
	memcpy(&returnedMsg,genPtr,sizeof(returnedMsg));
	
	switch(curCmdState) {
		case SET_BAUD:			post setBaudDoneTask();
								break;
		case VERBOSE_ERRORS:	post verboseErrorsDoneTask();
								break;
		case WARM_START_GPS:	post warmStartGpsDoneTask();
								break;
		case SMS_MODE:			post smsModeDoneTask();
								break;
		case SEND_SMS:			if(sendSmsCount==1) {
								  SODbg(DBG_PKT, "SmsCount=1\n");
								  post sendSmsDoneTask();
								}
								else {
								  SODbg(DBG_PKT, "SmsCount=0\n");
								  atomic {
								    sendSmsCount = 1;
								    call SendVarLenPacketGSM.send(SMS_TEXT,strlen(SMS_TEXT),AT_ANY_MSG);
								  }
								}
								break;
		case SIGNAL_STRENGTH:	post signalStrengthDoneTask();
								break;
		case GET_AQRD_GPS_POS:	post getAqrdGpsPosDoneTask();
								break;
	}
	
	//SODbg(DBG_PKT, "Length: %i\n",returnedMsg.length);
	//for(i = 0; i < returnedMsg.length; i++)
	//{
	//  SODbg(DBG_PKT, "%c",returnedMsg.data[i]);
	//}
	
	return (TOS_MsgPtr) &nextMsg;
  }
  
}


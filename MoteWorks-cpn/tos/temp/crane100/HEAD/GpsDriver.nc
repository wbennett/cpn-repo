configuration GpsDriver {
  provides interface GSM_GPSI;
}
implementation {
  components GpsDriverM, UARTGpsPacket, TimerC, LedsC;
  
  GSM_GPSI = GpsDriverM.GSM_GPSI;  
  
  GpsDriverM.GpsControl -> UARTGpsPacket.Control;
  GpsDriverM.SendVarLenPacketGSM -> UARTGpsPacket.SendVarLenPacketGSM;
  GpsDriverM.Receive -> UARTGpsPacket.Receive;
  GpsDriverM.Timer -> TimerC.Timer[unique("Timer")];
  GpsDriverM.Leds -> LedsC.Leds;  
}


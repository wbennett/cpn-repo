/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: gps.h,v 1.1.2.4 2007/12/03 22:28:11 xyang Exp $
 */


#ifndef XBOW_GPS_H
#define XBOW_GPS_H

#define GEN_DATA_LENGTH  128             //max payload length of General_Msg
//#define GPS_PACKET_START 0x24            //start of gps packet
//#define GPS_PACKET_END1  0x0D            // <CR> end if gps packet
//#define GPS_PACKET_END2  0x0A            // <LF> end of gps packet

//#define GPS_MSG_LENGTH 100
//#define GPS_CHAR 11
#define GGA_FIELDS 8                    //these defines the temp buffer for GGA Parsing
#define GPS_CHAR_PER_FIELD 10
#define GPS_DELIMITER ','
#define GPS_END_MSG '*'

#define CR 0x0D
#define LF 0x0A

#define AT_GEN_MSG 0
#define AT_GPS_MSG 1
#define AT_CARROT_MSG 2
#define AT_ANY_MSG 3
		

//Set GPS unit baudrate here
//#define UART1_BAUDRATE 4800
//TODO: FIGURE OUT HOW TO SET BAUD RATE
//USES micazc hardware.h file


/**
 *  This packet is what the TOS_MSG is cast into when the gps signals
 *  the Raw Receive.
 */
typedef struct General_Msg {
  uint8_t length;  //does not include len or crc
  int8_t data[GEN_DATA_LENGTH];  //should be uint8_t
} __attribute__ ((packed)) General_Msg;

typedef General_Msg *General_MsgPtr;

/**
 *  This packet is what the TOS_MSG is cast into when the gps signals
 *  GGA receive.
 */
typedef struct GGAMsg 
{
  uint8_t  hours;
  uint8_t  minutes;
  uint8_t  Lat_deg;
  uint8_t  Long_deg;

  uint32_t dec_sec;
  uint32_t Lat_dec_min;
  uint32_t Long_dec_min;

  uint8_t  NSEWind;
  uint8_t  valid;
  // This can be used after the rest of it is tested.
  /*
  struct {
     uint8_t NS : 1;
     uint8_t EW : 1;
     uint8_t num_sats : 4;
   }
  */
} __attribute__ ((packed)) GGAMsg;


//NOTE:
//Should be taken out of the appFeatrures.h for MTS420CC

//NOTE:
//These macros of multiplying 0.01 and stuff might not be such a good idea.

//NMEA_EXTRA_STUFF
#ifndef NMEA_EXTRACTION_MACROS
#define NMEA_EXTRACTION_MACROS

#define extract_num_sats_m(data)     (10*(data[0]-'0') + (data[1]-'0'))

#define extract_hours_m(data)        (10*(data[0]-'0') + (data[1]-'0'))

#define extract_minutes_m(data)      (10*(data[2]-'0') + (data[3]-'0'))

#define extract_dec_sec_m(data)      (10*(data[4]-'0') +  (data[5]-'0') + 0.1*(data[7]-'0') \
                                      + 0.01*(data[8]-'0') + 0.001*(data[9]-'0'))
                                      
#define extract_Lat_deg_m(data)      (10*(data[0]-'0') + (data[1]-'0'))

#define extract_Lat_dec_min_m(data)  (10*(data[2]-'0') +  (data[3]-'0') + 0.1*(data[5]-'0') \
                                      + 0.01*(data[6]-'0') + 0.001*(data[7]-'0') + 0.0001*(data[8]-'0'))
                                      
#define extract_Long_deg_m(data)     (100*(data[0]-'0') + 10*(data[1]-'0') + (data[2]-'0'))

#define extract_Long_dec_min_m(data) (10*(data[3]-'0') +  (data[4]-'0') + 0.1*(data[6]-'0') \
                      + 0.01*(data[7]-'0') + 0.001*(data[8]-'0') + 0.0001*(data[9]-'0'))
                      
#define is_gga_string_m(ns) ((ns[3]=='G')&&(ns[4]=='G')&&(ns[5]=='A'))

//NOTE:
//We don't reall need this param, but the old sensor board apps do
#define GPS_MAX_WAIT 10

#endif  
//NMEA_EXTRA_STUFF


#endif /* XBOW_GPS_H */


/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: UARTGpsPacket.nc,v 1.1.2.4 2007/12/03 22:28:11 xyang Exp $
 */

/*
 *
 * Authors:		Jason Hill, David Gay, Philip Levis
 * Date last modified:  6/25/02
 *
 */

configuration UARTGpsPacket
{
  provides {
    interface StdControl as Control;
	interface SendVarLenPacketGSM as SendVarLenPacketGSM;
    interface ReceiveMsg as Receive;

  }
}
implementation
{
  components GpsPacket as Packet, UART1 as UART;

  Control = Packet.Control;

  SendVarLenPacketGSM=Packet.SendVarLenPacketGSM;
  Receive = Packet.Receive;
  
  Packet.ByteControl -> UART;
  Packet.ByteComm -> UART;
}

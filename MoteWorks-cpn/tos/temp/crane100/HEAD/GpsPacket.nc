/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: GpsPacket.nc,v 1.1.2.4 2007/12/03 22:28:10 xyang Exp $
 */


module GpsPacket {
    provides {
        interface StdControl as Control;
        interface BareSendMsg as Send;
        interface ReceiveMsg as Receive;
        interface SendVarLenPacketGSM;

        command result_t txBytes(uint8_t *bytes, uint8_t numBytes);
    }
    
    uses {
        interface ByteComm;
        interface StdControl as ByteControl;
        interface Leds;
    }
}

implementation {
    
/* === Preprocessor ======================================================== */

    #include "gps.h"
        
/* === Frame State ========================================================= */
 
    
    //RX States
    enum {
       NO_GPS_START_BYTE = 0,  //no start
       GPS_START_BYTE = 1,     //start byte found and some
       GPS_BUF_NOT_AVAIL = 2   //done
    };
    uint8_t gpsRecvState;        //detect gps pckt

    
    //State
    enum {
        IDLE,
        PACKET,
        BYTES,
    };
    uint8_t state;
	
	//	AT_GPS_MSG,	AT_GEN_MSG
	uint8_t msgType;

    
    //Buffer tracking
    uint16_t rxCount, txCount, txLength;
    uint8_t *recPtr;
    uint8_t *sendPtr;
    
    //Buffers
    General_Msg buffer;

  /*
    state == IDLE, nothing is being sent
    state == PACKET, this level is sending a packet
    state == BYTES, this level is just transferring bytes

    The purpose of adding the new state, to simply transfer bytes, is because
    certain applications may want to just send a sequence of bytes without the
    packet abstraction.  One such example is the UART.

  */
  
  /*
    Moved these from gps.h #defines to editable variables
	so we can get gps messages that start with '$' and end with <CR>
	or AT message responses that start with 'A' and end with OK/r/n
  */
  uint8_t PACKET_END_SEQ[5] = "OK\r\n";
  
/* === StdControl ========================================================== */

    inline void initState() {
        atomic {  
            gpsRecvState = NO_GPS_START_BYTE;
            state = IDLE;
            
            txCount = rxCount = 0;      //reset recv counts
            recPtr = (uint8_t *) &buffer;      //setup recv ptr
			
			msgType=AT_GEN_MSG;
            
        }
    }

  
    /**
     *
     */
    command result_t Control.init() {    
        initState();
        return call ByteControl.init();        
    }

    /**
     *
     */
    command result_t Control.start() {
        return call ByteControl.start();
    }

    /**
     *
     */
    command result_t Control.stop() {
        return call ByteControl.stop();
    }


/* === Send ================================================================ */

  command result_t txBytes(uint8_t *bytes, uint8_t numBytes) {
    if (txCount == 0)
      {
        txCount = 1;
        txLength = numBytes;
        sendPtr = bytes;
    /* send the first byte */
        if (call ByteComm.txByte(sendPtr[0]))
        return SUCCESS;
    else
        txCount = 0;
      }
    return FAIL;
  }

  /* Command to transmit a packet */
  command result_t Send.send(TOS_MsgPtr msg) {
    atomic state = PACKET;
    msg->crc = 1; /* Fake out the CRC as passed. */
    return call txBytes((uint8_t *)msg, TOS_MsgLength(msg->type));
  }

  /* Command to transfer a variable length packet */
  command result_t SendVarLenPacketGSM.send(uint8_t* packet, uint8_t numBytes, uint8_t atMsgType) {
    atomic{ 
	  state = BYTES;
	  msgType = atMsgType;
	}
	  
    return call txBytes(packet, numBytes);
  }

  
  task void sendDoneFailTask() {
    atomic{
      txCount = 0;
      state = IDLE;
    }
    signal Send.sendDone((TOS_MsgPtr)sendPtr, FAIL);
  }
  
  task void sendDoneSuccessTask() {
    atomic{
      txCount = 0;
      state = IDLE;
    }
    signal Send.sendDone((TOS_MsgPtr)sendPtr, SUCCESS);
  }

  task void sendVarLenFailTask() {
    atomic{
      txCount = 0;
      state = IDLE;
    }
    signal SendVarLenPacketGSM.sendDone((uint8_t*)sendPtr, FAIL);
  }

  task void sendVarLenSuccessTask() {
    atomic {
      txCount = 0;
      state = IDLE;
    }
    signal SendVarLenPacketGSM.sendDone((uint8_t*)sendPtr, SUCCESS);
  }
  
  void sendComplete(result_t success) {
   atomic{ 
    if (state == PACKET){
           TOS_MsgPtr msg = (TOS_MsgPtr)sendPtr;
        if (success) {           /* This is a non-ack based layer */
           msg->ack = TRUE;
           post sendDoneSuccessTask();
       }
       else {
           post sendDoneFailTask();
       }
      }
    else if (state == BYTES) {
      if (success) {
            post sendVarLenSuccessTask();
      }
      else {
            post sendVarLenFailTask();
      }
    }
    else {
      txCount = 0;
      state = IDLE;
    }
   } //atomic
  }

      
  default event result_t SendVarLenPacketGSM.sendDone(uint8_t* packet, result_t success) {
    return success;
  }

  default event result_t Send.sendDone(TOS_MsgPtr msg, result_t success){
    return success;
  }
  
  
  /* Byte level component signals it is ready to accept the next byte.
     Send the next byte if there are data pending to be sent */
 async event result_t ByteComm.txByteReady(bool success) {
   atomic{
    if (txCount > 0){
         if (!success){
            dbg(DBG_ERROR, "TX_packet failed, TX_byte_failed");
            sendComplete(FAIL);
         }
         else if (txCount < txLength){
            dbg(DBG_PACKET, "PACKET: byte sent: %x, COUNT: %d\n",
            sendPtr[txCount], txCount);
            if (!call ByteComm.txByte(sendPtr[txCount++])) sendComplete(FAIL);
         }
    }
   } //atomic
    return SUCCESS;
  }

 async  event result_t ByteComm.txDone() {
   atomic{
    if (txCount == txLength)
      sendComplete(TRUE);
    }
    return SUCCESS;
  }
  
/* === Receive ============================================================= */

    /**
     *  Signal receive + Reset State
     */
    task void receiveTask() {
        
        //NOTE:
        //Even though we use a ptr swap interface, we do not actually swap
        //ptrs
    
        signal Receive.receive((TOS_MsgPtr) recPtr);
        atomic gpsRecvState = NO_GPS_START_BYTE;
    }
	
	
	bool checkEndByteOrSeq(uint8_t data) {
	  if((msgType == AT_GEN_MSG) || (msgType == AT_ANY_MSG)) {
	    return ((data==PACKET_END_SEQ[3]) && (recPtr[rxCount]==PACKET_END_SEQ[2]) && (recPtr[rxCount-1]==PACKET_END_SEQ[1]) && (recPtr[rxCount-2]==PACKET_END_SEQ[0]));
	  }
	  else if(msgType == AT_GPS_MSG) {
	    return (data==LF);
	  }
	  else if(msgType == AT_CARROT_MSG) {
	    return TRUE;
	  }
	}
	
	bool checkStartByteOrSeq(uint8_t data) {
	  if(msgType == AT_GEN_MSG) {
	    return (data=='A'); 
	  }
	  else if(msgType == AT_GPS_MSG) {
	    return (data==':');
	  }
	  else if(msgType == AT_CARROT_MSG) {
	    return (data=='>');
	  }
	  else {
	    return TRUE;
	  }
	}
    
	/**
     * Byte received from GPS
     * First byte in gps packet is reserved to count number of bytes rcvd.
     * Gps messages start with '$' (0x24) and end with <cr><lf> (0x0D, 0x0A)
     *
     */
    async event result_t ByteComm.rxByteReady(uint8_t data, bool error, uint16_t strength) {		
		uint8_t lState;
        atomic lState = gpsRecvState;
        
        //TODO:
        //Handle this case better

        if (error) {
            atomic {
                rxCount = 0;
                atomic gpsRecvState = NO_GPS_START_BYTE;
            }
            
            return FAIL;   //Return Statements doesn't matter
        }
        
        //Buffer not available
        if (lState == GPS_BUF_NOT_AVAIL) {
            return SUCCESS;
        }
        
        //Looking for start byte
        if (lState == NO_GPS_START_BYTE) {
            
            //Start Byte found
            if ( checkStartByteOrSeq(data) ) {
                atomic {
                    memset(recPtr, 0, GEN_DATA_LENGTH+1);  //clean my buffer
                    rxCount = 1;                           //set length to 1
                    recPtr[1] = data;                      //insert start symbol
                    gpsRecvState = GPS_START_BYTE;
                }
            }
            
            return SUCCESS;
        }
        
        //Start byte already found
        if (lState == GPS_START_BYTE) {
            
            //Max Length Exceeded
            if (rxCount == GEN_DATA_LENGTH ) {
                atomic gpsRecvState = NO_GPS_START_BYTE;
                return SUCCESS;
            }
            
            //NOTE:
            //We are stopping on the 2nd end byte, so the first end byte
            //get processed, and the 2nd bytes doesn't get into the buffer
            
            //Stop byte found
            if ( checkEndByteOrSeq(data) ) {
                atomic {
                    gpsRecvState = GPS_BUF_NOT_AVAIL;
                    rxCount++;
                    recPtr[rxCount] = data;
                    recPtr[0] = rxCount;
                }
                post receiveTask(); 
                return SUCCESS;
            }
            
            //Payload portion
            atomic {
                rxCount++;
                recPtr[rxCount] = data;
            }
        }
        

        return SUCCESS;
    }


}






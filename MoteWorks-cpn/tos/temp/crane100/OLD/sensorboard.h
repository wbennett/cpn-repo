/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: sensorboard.h,v 1.1.4.1 2007/04/27 05:07:15 njain Exp $
 */

/*
 *
 * Authors:		PIPENG
 * Date last modified:  8/6/05
 *
 */

/**
 * @author pipeng
 */


enum {
  TOSH_ACTUAL_PHOTO_PORT = 1,
  TOSH_ACTUAL_TEMP_PORT = 1, 
};

enum {
  TOS_ADC_PHOTO_PORT = 1,
  TOS_ADC_TEMP_PORT = 2,
};



TOSH_ALIAS_PIN(PHOTO_CTL, INT1);             
TOSH_ALIAS_PIN(TEMP_CTL, INT2);              


//commands for the HMC6343
//alogrithm for using this device
/*
*   1. apply power to the device
*   2. wait for the device to init (500ms)-the device is in run mode
*   3. Send 0x32 and 0x50 to get the heading and tilt data (>>1 (shift by 1))
*   4. Wait 1 ms to allow HMC6343
*   5. Send 0x33 and wait six response bytes from the HMC6343
*       - there will be heading(2 bytes), pitch(2 bytes), and roll(2 bytes)
*   6. Repeat steps 3-5 and wait 200ms for updated results (default == 5hz update rate)
*/

enum {
    //power applied 500ms
    //note you have to shift by 1 (>>1
    //i2c address (slave)
    TOSH_HMC6343_ADDR = (0x32),
    //this will be called before writing
    HMC_CMD_WRITE_ADDR = ((0x32)>>1),
    //this will be called to get the next six bytes
    HMC_CMD_READ_ADDR = (0x32),
    //post accel data (1ms)
    HMC_CMD_ACCEL = (0x40),
    //post Magnometer data (1ms)
    HMC_CMD_MAG = (0x45),
    //post heading data (1ms)
    HMC_CMD_HEAD = (0x50),
    //post tilt data (1ms)
    HMC_CMD_TILT = (0x55),
    //post op mode 1 (1ms)
    HMC_CMD_OP_MODE = (0x65),
    //enter usr calibration mode (.3ms)
    HMC_CMD_USR_CAL = (0x71),
    //level orientation (.3ms)
    HMC_CMD_LVL_ORIENT = (0x72),
    //upright sideways orientation (.3ms)
    HMC_CMD_UPS_ORIENT = (0x73),
    //upright flat front orientation (.3ms)
    HMC_CMD_UPF_ORIENT = (0x74),
    //enter run mode (.3ms)
    HMC_CMD_RUN_MODE = (0x75),
    //enter standby mode (.3ms)
    HMC_CMD_STDBY_MODE = (0x76),
    //exit user calibration mode (50ms)
    HMC_CMD_EXT_CALIB = (0x7E),
    //reset the processor (500ms)
    HMC_CMD_RST_PROC = (0x82),
    //enter sleep mode (1ms)
    HMC_CMD_SLEEP = (0x83),
    //exit sleep mode (20ms)
    HMC_CMD_EXT_SLEEP = (0x84),
    //read from eeprom ram (10ms)
    HMC_CMD_R_EEPROM = (0xE1),
    //write to eeprom ram (10ms)
    HMC_CMD_W_EEPROM = (0xF1),
};


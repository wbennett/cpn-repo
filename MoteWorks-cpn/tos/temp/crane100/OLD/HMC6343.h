/*
*This header file will contain structs for the data from the sensorf
*/
//accel struct
typedef struct Accel
{
    int16_t X_VAL;
    int16_t Y_VAL;
    int16_t Z_VAL;
}Accel_t;
//magnometer struct
typedef struct Mag
{
    int16_t X_VAL;
    int16_t Y_VAL;
    int16_t Z_VAL;
}Mag_t;
//heading struct
typedef struct Head
{
    int16_t HEAD_VAL;
    int16_t PITCH_VAL;
    int16_t ROLL_VAL;
}Head_t;
//tilt struct
typedef struct Tilt
{
    int16_t PITCH_VAL;
    int16_t ROLL_VAL;
    int16_t TEMP_VAL;
}Tilt_t;

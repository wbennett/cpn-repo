/*
*This module will provide access to the HMC6343 device
*
*/
module HMC6343_SensorM
{
    provides
    {
        interface StdControl;
        interface HMC6343I;
    }
    uses
    {
        //for the i2c
        interface StdControl as I2CPacketControl;
        interface I2CPacket as I2CPacket;
        //timer for waiting on the sensor
        interface StdControl as TimerControl;
        interface Timer as WaitTimer;
        
    }
}
implementation
{
    #define SO_DEBUG 1
    //see the data types
    #include "HMC6343.h"
    #include "SOdebug.h" 
    //DEFINES FOR SLEEP TIMES
    #define START_TIME (500)
    #define POST_TIME (1)
    #define CALI_CMD_TIME (1)
    #define SET_ORIENT_TIME (1)
    #define CALIBRATION_TIME (15000)
    #define EXIT_CAL_TIME (50)
    #define CHANGEMODE_TIME (1)
    #define RESET_TIME (500)
    #define ENTER_SLEEP_TIME (1)
    #define EXIT_SLEEP_TIME (20)
    #define DO_EEPROM_TIME (10)
    #define SAMPLE_RATE (200) //default is 5hz
    
    enum
    {
        //i2c states
        IDLE = 0,
        SENDCMD_NR,
        SENDCMD,
        RECVDATA,
        //driver states
        START,
        RUN,
        STDBY,
        HMC_SLEEP,
        ENTER_CAL,
        CALIBRATION,
        EXIT_CAL,
        SET_ORIENT,
        POST,
        CHG_MODE,
        ENTER_SLEEP,
        EXIT_SLEEP,
        EEPROM,
        RESET,
    };
  
    uint8_t gState = START;
    uint8_t i2cState = IDLE;
    //i2c buffers
    //Accel_t accel_t_buf;
    //Mag_t mag_t_buf;
    //Head_t head_t_buf;
    //Tilt_t tilt_t_buf;
    int voidBuf[3];
    //state variables
    bool isCalibrated = FALSE;
    uint8_t i2cstate;//state of the i2c request
    //protoypes
    void DoNext();//this will be called after command is sent.

    
    /**************interface methods stdcontrol**********/
    command result_t StdControl.init()
    {
        //i2c init stuff here
        SODbg(1,"SENSOR:init();\n");
        return call I2CPacketControl.init();
    }

    command result_t StdControl.start()
    {
        SODbg(1,"SENSOR:start();\n");
        //i2c startup
        call I2CPacketControl.start();
        //start state
        atomic gState = START;
        return SUCCESS;
    }

    command result_t StdControl.stop()
    {
        SODbg(1,"SENSOR:stop()\n");
        //itc shutdown
        //just turn off the device
        return call I2CPacketControl.stop();
    }
    /***********interface methods Timer*************/
    /*
    * This will fired after any command is sent on the line
    * It is important to wait for the i2c communication to complete
    * before doing any other operation.
    */
    event result_t WaitTimer.fired()
    {
        SODbg(1,"SENSOR:waittimer:fired()\n");
        //go through the cases and react based on them
        DoNext();
        return SUCCESS;
    }
    //method called 
    void DoNext()
    {
        uint8_t lState;
        uint8_t lBusState;
        char cmd;
        SODbg(1,"SENSOR:DoNext()\n");
        //get the current state at this time
        //get the bus state (busy,etc....)
        atomic
        {
            lState = gState;
            lBusState = i2cState;
        }

        switch(lState)
        {
            uint8_t lI2cState;
            case START:
                //nothing to do here
                SODbg(1,"DONEXT:START\n");
                break;
            case RUN:
                //nothing to do here
                break;
            case ENTER_CAL:
                SODbg(1,"DONEXT:ENTER_CAL\n");
                //in calibration mode need wait until exit is called
                //give the user 15 seconds to calibrate
                //this will allow the sensor to adjust to iron in the env
                atomic gState = CALIBRATION;
                //wait 15 sec
                call WaitTimer.start(TIMER_ONE_SHOT,CALIBRATION_TIME);
                break;
            case CALIBRATION:
                SODbg(1,"DONEXT:CALIBRATION\n");
                cmd = HMC_CMD_EXT_CALIB;
                //just completed calibration, now we need to exit
                //send the exit command...
                atomic gState = EXIT_CAL;
                call I2CPacket.writePacket(1,&cmd,0x80);
                break;
            case EXIT_CAL:
                SODbg(1,"DONEXT:EXIT_CAL\n");
                //go into run mode...(need to adjust this to save power)
                //we need to open the bus line and change to run state
                atomic
                {
                    //open bus
                    i2cState = IDLE;
                    //ready to rumble
                    gState = RUN;
                    signal HMC6343I.calibrationDone(TRUE);
                }
                break;
            case POST:
                SODbg(1,"DONEXT:POST\n");
                //we just sent a command we need to read the incoming data...
                //update bus state
                atomic lI2cState = i2cState;
                switch(lI2cState)
                {
                    case SENDCMD:
                        SODbg(1,"DONEXT:POST:SENDDATA\n");
                        //cmd = HMC_CMD_RESPBYTES;
                        //just sent the command lets read the data
                        atomic i2cState = RECVDATA;
                        if(!(call I2CPacket.readPacket(6,0x80)))
                            SODbg(1,"DONEXT:POST:RECVDATA:readfail\n");
                        break;
                    case RECVDATA:
                        SODbg(1,"DONEXT:POST:RECVDATA\n");
                        //just received data...
                        atomic
                        {
                            //make bus available
                           // i2cState = IDLE;
                            //let us sample some other day
                            //gState = RUN;
                            //signal ok to sample
                        }
                        atomic i2cState = RECVDATA;
                        //signal HMC6343I.deviceReady(SUCCESS);
                        if(!(call I2CPacket.readPacket(6,0x80)))
                            SODbg(1,"DONEXT:POST:RECVDATA:readfail\n");
                        break;
                    default:
                        //bad
                        break;
                }
                //read 6 bytes
                break;
            case SET_ORIENT:
                atomic
                {
                    //open bus
                    i2cState = IDLE;
                    //ready to rumble
                    gState = RUN;
                }
                break;
            case CHG_MODE:
                //shouldn't happen
                break;
            case ENTER_SLEEP:
                //just got out of sleep mode
                //let the world know
                atomic gState = HMC_SLEEP;
                atomic i2cState = IDLE;
                break;
            case HMC_SLEEP:
                //do nothing
                break;
            case EXIT_SLEEP:
                //not implemented
                //should be in run mode
                atomic gState = RUN;
                atomic i2cState = IDLE;
                break;
            case STDBY:
                //do nothing....
                atomic gState = STDBY;
                atomic i2cState = IDLE;
                break;
            case EEPROM:
                //not implemented for now...
                break;
            case RESET:
                //just came out of reset we should be in run mode
                atomic gState = RUN;
                atomic i2cState = IDLE;
                break;
            default:
                //do nothing...
                break;
        }
    }
    
    /***********interface methods i2c packet*********/
    
    //fired when done writing a packet
    event result_t I2CPacket.writePacketDone(bool result)
    {
        SODbg(1,"SENSOR:I2CPacket.writePacketDone():r:%d\n",result);
        //make sure it worked
        if(result)
        {
            uint8_t lState;//,busState;
            //wrote packet
            //get the global state 
            //get the i2c bus state
            atomic
            {
                lState = gState;
                //busState = i2cState;
            }
            //Based on the command written go to sleep
            //for that time...(see the predefines)
            switch(lState)
            {
                case ENTER_CAL:
                    SODbg(1,"I2CPacket.writepacketdone:ENTER_CAL");
                    //wait for 1ms
                    return call WaitTimer.start(TIMER_ONE_SHOT,CALI_CMD_TIME);
                case EXIT_CAL:
                    //wait for 50ms
                    return call WaitTimer.start(TIMER_ONE_SHOT,EXIT_CAL_TIME);
                case RUN:
                    //wait for 1ms
                    return call WaitTimer.start(TIMER_ONE_SHOT,CHANGEMODE_TIME);
                case STDBY:
                    //wait for 1ms
                    return call WaitTimer.start(TIMER_ONE_SHOT,CHANGEMODE_TIME);
                case POST:
                    //sleep for 1ms
                    return call WaitTimer.start(TIMER_ONE_SHOT,POST_TIME);
                case SET_ORIENT:
                    return call WaitTimer.start(TIMER_ONE_SHOT,SET_ORIENT_TIME);
                case ENTER_SLEEP:
                    //sleep for 1ms
                    return call WaitTimer.start(TIMER_ONE_SHOT,ENTER_SLEEP_TIME);
                case EXIT_SLEEP:
                    //sleep for 20ms
                    return call WaitTimer.start(TIMER_ONE_SHOT,EXIT_SLEEP_TIME);
                case EEPROM:
                    //sleep for 10ms (not implemented yet)
                    return call WaitTimer.start(TIMER_ONE_SHOT,DO_EEPROM_TIME);
                case RESET:
                    //sleep for 500ms
                    return call WaitTimer.start(TIMER_ONE_SHOT,RESET_TIME);
                case CALIBRATION:
                    //should never happen
                    return FAIL;
                case START:
                    //should never happen
                    return FAIL;
                default:
                    //should never happen
                    return FAIL;
            }
        }
        else
            return FAIL;
    }
    event result_t I2CPacket.readPacketDone(char length, char* data)
    {
        uint8_t lState,busState;
        uint16_t temp;
        char dlen = 6;
        SODbg(1,"SENSOR:I2CPacket.readpacketdone\n");
        atomic
        {
            lState = gState;
            busState = i2cState;
        }
        //temp = ((((uint16_t)data[0]) << 8)|(uint16_t)data[1]);
        //testing
        SODbg(1,"r1:%u,r2:%u,r3:%u,r4:%u,r5:%u,r6:%u\n",
            data[0],data[1],data[2],data[3],data[4],data[5]);
        //SODbg(1,"dlen:%uHEADING:%lu\n",(uint8_t)length,temp);
        //end testing
        //read packet 
        if((length == dlen)&&(lState == POST)
            &&(i2cState == RECVDATA))
        {
            char tBuf[6];
            //uint8_t i = 0;
            
            //SODbg(1,"I2CPacket.readpacketdone:length:post:recvdata\n");
            
            //when we get the data signal sleep for 200ms 
            //(default 5hz update rate)
            //copy packet data to the buffer and signal
            //to <- from, size
            memcpy(tBuf,data,sizeof(voidBuf));
            //singal event
            //(it goes msb than lsb)
            /*for(i=0;i<sizeof(voidBuf);i++)
            {
                voidBuf[i] = 0;
                voidBuf[i] = tBuf[(i*2)]*256))+((uint16_t)tBuf[(i*2)+1]));
            }*/
            voidBuf[0] = tBuf[0]*256 + tBuf[1];
            voidBuf[1] = tBuf[2]*256 + tBuf[3];
            voidBuf[2] = tBuf[4]*256 + tBuf[5];
            signal HMC6343I.dataReady((int *)&voidBuf, SUCCESS);
            //wait 5hz (200ms)...
            return call WaitTimer.start(TIMER_ONE_SHOT,200);
        }
        else
            return FAIL;
        //nothing todo because write packet is done with command
        return SUCCESS;
    }
    /*
    //fired when done reading a packet
    //
    event result_t ReadPacket.writePacketDone(bool result)
    {   
        SODbg(1,"SENSOR:READPACKET:I2CPacket.one:r:%d\n",result);
        //shouldn't happen....
        return result;
    }
    event result_t ReadPacket.readPacketDone(char length, char* data)
    {
        uint8_t lState,busState;
        uint16_t temp;
        char dlen = 6;
        SODbg(1,"SENSOR:READPACKET.readpacketdone\n");
        atomic
        {
            lState = gState;
            busState = i2cState;
        }
        temp = ((((uint16_t)data[0]) << 8)|(uint16_t)data[1]);
        //testing
        //SODbg(1,"r1:%u,r2:%u,r3:%u,r4:%u,r5:%u,r6:%u\n",
        //    data[0],data[1],data[2],data[3],data[4],data[5]);
        //SODbg(1,"dlen:%uHEADING:%lu\n",(uint8_t)length,temp);
        //end testing
        //read packet 
        if((length == dlen)&&(lState == POST)
            &&(i2cState == RECVDATA))
        {
            uint8_t tBuf[6];
            uint8_t i = 0;
            SODbg(1,"Readpacket.readpacketdone:length:post:recvdata\n");
            //when we get the data signal sleep for 200ms 
            //(default 5hz update rate)
            //copy packet data to the buffer and signal
            //to <- from, size
            memcpy(tBuf,data,sizeof(voidBuf));
            //singal event
            //(it goes msb than lsb)
            for(i=0;i<sizeof(voidBuf);i++)
            {
                voidBuf[i] = 0;
                voidBuf[i] = (uint16_t)((((uint16_t)tBuf[(i*2)])>>8)|((uint16_t)tBuf[(i*2)+1]));
            }
            signal HMC6343I.dataReady((uint16_t *)&voidBuf, SUCCESS);
            //wait 5hz (200ms)...
            return call WaitTimer.start(TIMER_ONE_SHOT,200);
        }
        else
            return FAIL;
    }
    */
    /***********interface methods HMC6343*******/
    /**
    * Change the mode of the sensor...
    **/
    command result_t HMC6343I.changeMode(char mode)
    {
        SODbg(1,"SENSOR:changeMode\n");
        if(i2cState == IDLE)
        {
            atomic{
                i2cState = SENDCMD_NR;
                switch(mode)
                {
                    case HMC_CMD_SLEEP:
                        gState = ENTER_SLEEP;
                        break;
                    case HMC_CMD_EXT_SLEEP:
                        gState = EXIT_SLEEP;
                        break;
                    case HMC_CMD_RUN_MODE:
                        gState = RUN;
                        break;
                    case HMC_CMD_STDBY_MODE:
                        gState = STDBY;
                        break;
                    default:
                        return FAIL;
                }
            }
            //write one byte command to the slave device
            call I2CPacket.writePacket(1,&mode,0x80);
            
        }
        else
            return FAIL;

    }
    
    /**
    * Set orientation of the device
    **/
    command result_t HMC6343I.setOrientation(char orientation)
    {
        SODbg(1,"SENSOR:setOrientation\n");
        if(i2cState == IDLE)
        {
            atomic{
                i2cState = SENDCMD_NR;
                gState = SET_ORIENT;
            }
            //write one byte command to the slave device
            call I2CPacket.writePacket(1,&orientation,0x80);
        }
        else
            return FAIL;
    }
    
    /**
    * Command the sensor to go into calibrate mode
    **/
    command result_t HMC6343I.calibrateSensor()
    {
        SODbg(1,"SENSOR:calibrateSensor\n");
        //put the mode in calibration and calibrate sensor
        if(i2cState == IDLE)
        {
            char cmd = HMC_CMD_USR_CAL;
            //lets put in calibration mode and do next
            atomic{
            i2cState = SENDCMD_NR;//no response command
            gState = ENTER_CAL;//enter calibration mode
            }
            //write one byte command to the slave device
           return call I2CPacket.writePacket(1,&cmd,0x80);
            //allow them to calibrate for 15 seconds
        }
        else
            return FAIL;
    }
    /**
    * Set orientation of the device
    **/
    command result_t HMC6343I.getAccelData()
    {
        SODbg(1,"SENSOR:getAccelData\n");
        //retreive the accel data **check state**
        if(i2cState == IDLE)
        {
            char cmd = HMC_CMD_ACCEL;
            atomic{
            i2cState = SENDCMD;
            gState = POST;
            }
            return call I2CPacket.writePacket(1,&cmd,0x80);
        }
        else
            return FAIL;
    }
    
    /**
    * Command the sensor to retrieve mag data
    **/
    command result_t HMC6343I.getMagData()
    {
        SODbg(1,"SENSOR:getMagData()\n");
        //retreive the mag data **check state**
        if(i2cState == IDLE)
        {
            char cmd = HMC_CMD_MAG;
            atomic{
            i2cState = SENDCMD;
            gState = POST;
            }
            return call I2CPacket.writePacket(1,&cmd,0x80);
        }
        else
            return FAIL;
    }
    
    /**
    * Command the sensor to retrieve heading data
    **/
    command result_t HMC6343I.getHeadData()
    {
        SODbg(1,"SENSOR:getHeadData\n");
        //retrieve the head data **check state**
        if(i2cState == IDLE)
        {
            char cmd = HMC_CMD_HEAD;
            SODbg(1,"SENSOR:GETHEADDATA:I2CSTATE:IDLE\n");
            atomic{
            i2cState = SENDCMD;
            gState = POST;
            }
            if((call I2CPacket.writePacket(1,&cmd,0x80))==SUCCESS)
            {
                SODbg(1,"SENSOR:GETHEAD:I2CPacket.SUCCESS\n");
                return SUCCESS;
            }
            else
            {
                return FAIL;
            }
        }
        else
            return FAIL;
    }
    
    /**
    * Command the sensor to retrieve tilt data
    **/
    command result_t HMC6343I.getTiltData()
    {
        SODbg(1,"SENSOR:getTiltData\n");
        //retrieve the tilt data **check state**
        if(i2cState == IDLE)
        {
            char cmd = HMC_CMD_TILT;
            atomic{
            i2cState = SENDCMD;
            gState = POST;
            }

            return call I2CPacket.writePacket(1,&cmd,0x80);

        }
        else
            return FAIL;
    }
    
    /**
    * Event to let the user now the calibration is done
    **/
    default event result_t HMC6343I.calibrationDone(bool success)
    {
        return SUCCESS;
    }
    
    /**
    * Event signaled when the i2c bus receives data
    */
    default event result_t HMC6343I.dataReady(int* data, bool success)
    {
        return SUCCESS;
    }
    /**
    * Signaled when its ok to send command
    * (IN THE FUTURE WE SHOULD MAKE A QUEING SYSTEM)
    */
    default event result_t HMC6343I.deviceReady(bool success)
    {
        return SUCCESS;
    }
}

interface HMC6343I
{
    /**
    * Command to put the sensor into a different orientation
    **/
    command result_t setOrientation(char orientation);
    /**
    * Command to put the sensor into a different mode
    * (ex: run, standby, sleep, wakeup)
    **/
    command result_t changeMode(char mode);
    /**
    * Command the sensor to go into calibrate mode
    **/
    command result_t calibrateSensor();
    /**
    * Command the sensor to retrieve accel data
    **/
    command result_t getAccelData();
    
    /**
    * Command the sensor to retrieve mag data
    **/
    command result_t getMagData();
    
    /**
    * Command the sensor to retrieve heading data
    **/
    command result_t getHeadData();
    
    /**
    * Command the sensor to retrieve tilt data
    **/
    command result_t getTiltData();
    
    /**
    * Event to let the user now the calibration is done
    **/
    event result_t calibrationDone(bool success);
    
    /**
    * **REFACTORED**
    * The data that will be read from the i2c on any command
    * is the same size, so we will let the developer cast it
    * to whatever data he requested (mag,head,etc..)
    */
    event result_t dataReady(int* data, bool success); 
    event result_t deviceReady(bool success);
    /**
    * Command the sensor to retrieve tilt data
    **/
    //event result_t accelDone(Accel_t accel, bool success);
    
    /**
    * Command the sensor to retrieve tilt data
    **/
    //event result_t magDone(Mag_t mag, bool success);
    
    /**
    * Command the sensor to retrieve tilt data
    **/
    //event result_t headDone(Head_t head, bool success);
    
    /**
    * Command the sensor to retrieve tilt data
    **/
    //event result_t tiltDone(Tilt_t tilt, bool success);
    
}

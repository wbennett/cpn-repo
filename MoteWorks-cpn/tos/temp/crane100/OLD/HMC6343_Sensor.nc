includes sensorboard;

configuration HMC6343_Sensor
{
    provides
    {
        interface StdControl;
        interface HMC6343I as HMC6343;
    }
}
implementation
{
    components I2CPacketC,TimerC, HMC6343_SensorM as Sensor;
    StdControl = TimerC;
    StdControl = Sensor;
    HMC6343 = Sensor;
    Sensor.I2CPacketControl -> I2CPacketC.StdControl; 
    //Sensor.I2CPacket -> I2CPacketC.I2CPacket[TOSH_HMC6343_ADDR];
    Sensor.I2CPacket -> I2CPacketC.I2CPacket[HMC_CMD_WRITE_ADDR];
    Sensor.WaitTimer ->TimerC.Timer[unique("Timer")];
}

/*	
*	The different sleep modes and how to control them can be found in the Atmega
*	1281 datasheet, in section 11.
*
*	Based on MicaZ vesion of this file.
*
* 	See license.txt file included with the distribution.
*/
 
module HPLPowerManagementM
	{
	provides
		{
		interface PowerManagement;
		command result_t Enable();
		command result_t Disable();
		command bool isEnabled();
		}
	}

implementation
	{
	/*	SPCR Register
	*	----------------------------------------
	*	| x | x | x | x | SM2 | SM1 | SM0 | SE |
	*	----------------------------------------
	*/
	#define SLEEP_MODE_MASK		(0x0E) /* Mask to get the sleep mode bits from SPCR register	*/
	
	/* The next set of masks get the bits that indicate a timer has an output compare or overflow interrupt enabled */
	#define TIMER_0_RUN_MASK	((1 << TOIE0) | (1 << OCIE0A) | (1 << OCIE0B))
	#define TIMER_1_RUN_MASK	((1 << TOIE1) | (1 << OCIE1A) | (1 << OCIE1B) | (1 << OCIE1C) | (1 << OCIE1))
	#define TIMER_2_RUN_MASK	((1 << TOIE2) | (1 << OCIE2A) | (1 << OCIE2B))
	#define TIMER_3_RUN_MASK	((1 << TOIE3) | (1 << OCIE3A) | (1 << OCIE3B) | (1 << OCIE3C) | (1 << OCIE3))
	#define TIMER_4_RUN_MASK	((1 << TOIE4) | (1 << OCIE4A) | (1 << OCIE4B) | (1 << OCIE4C) | (1 << OCIE4))
	#define TIMER_5_RUN_MASK	((1 << TOIE5) | (1 << OCIE5A) | (1 << OCIE5B) | (1 << OCIE5C) | (1 << OCIE5))
	
	/* Mask to see if flash is waiting on interrupt */
	#define FLASH_INTERRUPT_MASK	((1 << RXCIE) | (1 << TXCIE) | (1 << RXEN)  | (1 << TXEN))
	
	#define IMMINENT_INTERRUPT	(16)	/* If Timer2 will interrupt in this many ticks don't say we're sleeping */
	
	/* 	These are the different sleep modes available to the Atmega 1281.  The
	*	bit patterns are used in the SMCR register to send the processor into
	*	that particular state
	*/
	enum
		{
		IDLE 		= 0,										/* 0x0				*/
		ADC_NR 		= (1 << SM0),								/* 0x1				*/
		POWER_DOWN	= (1 << SM1),								/* 0x2				*/
		POWER_SAVE 	= ((1 << SM0) | (1 << SM1)),				/* 0x3				*/
		RESERVED_1	= (1 << SM2),								/* 0x4, DO NOT USE	*/
		RESERVED_2	= ((1 << SM0) | (1 << SM2)),				/* 0x5, DO NOT USE	*/
		STANDBY 	= ((1 << SM1) | (1 << SM2)),				/* 0x6				*/
		EXT_STANDBY = ((1 << SM0) | (1 << SM1) | (1 << SM2))	/* 0x7				*/
		};
	
	/*===Local State ============================================================*/
	bool disabled = TRUE; /* Indicates whether power management is disabled */

	/*===Local Functions ========================================================*/
    
	/* Returns the current sleep mode of the processor */
	uint8_t getPowerLevel()
		{
		uint8_t diff_a;		/* ticks until output compare A	*/
		uint8_t diff_b;		/* ticks until output compare B	*/
		uint8_t	cur_level;	/* Return value 				*/
		
		cur_level = POWER_DOWN;

		/* 	Timer2 is the only timer that can wake the unit from sleep mode,
		*	so if any other timers are running, we must be idleing
		*/
		if((inp(TIMSK0) & TIMER_0_RUN_MASK) ||
			(inp(TIMSK1) & TIMER_1_RUN_MASK) ||
			(inp(TIMSK3) & TIMER_3_RUN_MASK) ||
			(inp(TIMSK4) & TIMER_4_RUN_MASK) ||
			(inp(TIMSK5) & TIMER_5_RUN_MASK))
			{ 
			cur_level = IDLE;
			}
		/* Is the SPI interrupt enabled? */
		else if(bit_is_set(SPCR, SPIE))
			{ 
			cur_level = IDLE;	   
			}
		/* Are we waiting to send or receive something from the flash? */
		else if(inp(UCSR1B) & FLASH_INTERRUPT_MASK)
			{
			cur_level = IDLE;
			}
		/* Is the ADC enabled? */
		else if(bit_is_set(ADCSRA, ADEN))
			{ 
			cur_level = ADC_NR;
			}
		/* Is Timer2 running? */
		else if(inp(TIMSK2) & TIMER_2_RUN_MASK)
			{
			/* Figure out how long until it goes off */
			diff_a = inp(OCR2A) - inp(TCNT2);
			diff_b = inp(OCR2B) - inp(TCNT2);
			
			/* Interrupt is about to go off */
			if((diff_a < IMMINENT_INTERRUPT) ||
				(diff_b < IMMINENT_INTERRUPT) )
				{
				cur_level = EXT_STANDBY;
				}
			else
				{
				cur_level = POWER_SAVE;
				}
			}
		else
			{
			cur_level = POWER_DOWN;
			}
			
		return cur_level;
		}

	/*===Tasks ==================================================================*/
	/* Figures out what sleep mode to go to, and sets the processor to that mode */
	task void doAdjustment()
		{
		uint8_t cur_power_level;
		uint8_t sm;
		
		cur_power_level = getPowerLevel();
		
		/* Get the current sleep mode register and clear the status bits */
		sm = inp(SMCR);
		sm = (sm & (~SLEEP_MODE_MASK));
		
		/* Special handling if Timer2 is running */
		if((cur_power_level == EXT_STANDBY) || (cur_power_level == POWER_SAVE))
			{
			sm |= IDLE;
			
			while((inp(ASSR) & 0xF) != 0)
				{
				asm volatile("nop");
				}
				
			sm = (sm & (~SLEEP_MODE_MASK));
			}

		sm |= cur_power_level;
		
		/* Write out new sleep mode */
		outp(sm, SMCR);
		sbi(SMCR, SE);
		}
    
	/*===Power Management ======================================================*/
	/* Sets a new sleep mode if needed */
	async command uint8_t PowerManagement.adjustPower()
		{
		uint8_t sm;
    
		if(disabled == FALSE)
			{
			/* Update the sleep mode */
			post doAdjustment();
			}
		else
			{
			/* Force the processor to idle sleep mode */
			sm = inp(SMCR);
			sm = (sm & (~SLEEP_MODE_MASK));
			sm |= IDLE;
			outp(sm, SMCR);
			sbi(SMCR, SE);
			}
			
		return 0;
		}

	/* Enable power management */
	command result_t Enable()
		{
		atomic disabled = FALSE;
		return SUCCESS;
		}

	/* Disable power management */
	command result_t Disable()
		{
		atomic disabled = TRUE;
		return SUCCESS;
		}
    
	/* Get power management mode */
	command bool isEnabled()
		{
		bool ldisabled;
	  
		atomic ldisabled = disabled;
		
		return ldisabled;
		}
	}

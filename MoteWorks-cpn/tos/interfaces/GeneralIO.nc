/*
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
interface GeneralIO {
	//make output
	async command result_t makeOutput();
	async command result_t makeInput();
	async command result_t set();
	async command result_t clr();
}

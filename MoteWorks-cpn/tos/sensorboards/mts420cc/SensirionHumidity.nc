/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: SensirionHumidity.nc,v 1.1.2.4 2007/12/03 22:28:11 xyang Exp $
 */

/*
 *
 * Authors:     Joe Polastre
 *repaired by M. Grimmer 5/15/07
 */

includes sensorboard;

configuration SensirionHumidity
{
  provides {
    interface ADC as Humidity;
    interface ADC as Temperature;
    interface SplitControl;
    
    //dummy
    interface ADCError as HumidityError;
    interface ADCError as TemperatureError;
  }
}
implementation
{
  components SensirionHumidityM, MicaWbSwitch, TempHum, LedsC;

  SplitControl = SensirionHumidityM;
  Humidity = SensirionHumidityM.Humidity;
  Temperature = SensirionHumidityM.Temperature;
  HumidityError = SensirionHumidityM.HumidityError;
  TemperatureError = SensirionHumidityM.TemperatureError;
  
  SensirionHumidityM.SensorControl -> TempHum.SplitControl;
  SensirionHumidityM.HumSensor -> TempHum.HumSensor;
  SensirionHumidityM.TempSensor -> TempHum.TempSensor;

  SensirionHumidityM.SwitchControl -> MicaWbSwitch.StdControl;
  SensirionHumidityM.PowerSwitch -> MicaWbSwitch.Switch[0];
  SensirionHumidityM.IOSwitch -> MicaWbSwitch.Switch[1];
  SensirionHumidityM.Leds -> LedsC;

}

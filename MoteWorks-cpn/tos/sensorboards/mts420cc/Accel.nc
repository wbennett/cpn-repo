/*
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: Accel.nc,v 1.1.2.4 2007/12/03 22:28:10 xyang Exp $
 */

includes sensorboard;

configuration Accel {
  provides interface ADC as AccelX;
  provides interface ADC as AccelY;
  provides interface StdControl;
  provides interface I2CSwitchCmds as AccelCmd;
}

implementation {
  components AccelM, ADCC, MicaWbSwitch, HPLPowerManagementM, TimerC;
  
  AccelCmd = AccelM;
  StdControl = AccelM;
  
  AccelM.Timer -> TimerC.Timer[unique("Timer")];
  
  AccelX = ADCC.ADC[ADC_ACCEL_X_PORT];
  AccelY = ADCC.ADC[ADC_ACCEL_Y_PORT];
 
  AccelM.ADCControl -> ADCC;
  AccelM.SwitchControl -> MicaWbSwitch.StdControl;
  AccelM.PowerSwitch -> MicaWbSwitch.Switch[0];
  AccelM.PowerManagement -> HPLPowerManagementM;
 
}

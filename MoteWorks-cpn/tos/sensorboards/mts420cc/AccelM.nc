/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: AccelM.nc,v 1.1.2.4 2007/12/03 22:28:10 xyang Exp $
 */
 

includes sensorboard;

module AccelM {
    provides  {
        interface StdControl;
        interface I2CSwitchCmds as AccelCmd;
    }
    
    uses {
        interface ADCControl;
        interface Timer;
        interface StdControl as SwitchControl;
        interface Switch as PowerSwitch;
        interface PowerManagement;
    }
}

implementation {
    
    //NOTE:
    //The adc interface is not adc RefM so sensitive to battery voltage
    
    //NOTE:
    //Should be okay with 20ms (no less than 2.6ms)
    //Wait time is determined by the duty cycle resistor
    
    #define ACCEL_SWITCH_POWER_ON_WAIT_MS 20
 
    enum {
        ACCEL_SWITCH_IDLE,          //switch operation complete
        ACCEL_SWITCH_WAIT_ON,       //switch operation busy wait for on
        ACCEL_SWITCH_WAIT_OFF,      //switch operation busy wait for off
    };

    uint8_t state_accel;

/* === StdControl ========================================================== */

    //TODO:
    //Make this split control so you don't have to seperate power switch from
    //Stdcontrol.
    
    //NOTE:
    //We init and start ADC but we never stop it.  Do we need to stop adc in
    //order for processor to sleep?  No, ADC enables and disables ADC on every 
    //sample

    command result_t StdControl.init() {
        
        state_accel = ACCEL_SWITCH_IDLE; 

        call SwitchControl.init();
        call ADCControl.init();
                
        return SUCCESS;
        
        //TODO:
        //Check return statements
    }

    command result_t StdControl.start() {
        
        //NOTE:
        //Should there be a need to start + stop the adc??
        
        call SwitchControl.start();
        return SUCCESS;
    }

    command result_t StdControl.stop() {
        
        //NOTE:
        //does adc need to be stopped?
        
        return SUCCESS;
    }
    
/* ==== AccelCmd =========================================================== */

    /**
     *  Turn Accel power on/off.  
     *
     *  @param PowerState   1=powerOn, 0=powerOff
     *  @return result_t
     *
     */
    command result_t AccelCmd.PowerSwitch(uint8_t PowerState) {
        result_t result;

        //Invalid Power State
        if ((PowerState != 0) && (PowerState != 1)) return FAIL;
        
        //Check if the I2C switch is already busy        
        if (state_accel != ACCEL_SWITCH_IDLE) return FAIL;
        
        //If not busy set the power switch
        result = call PowerSwitch.set(MICAWB_ACCEL_POWER, PowerState);
        
        //check if switch operation started correctly
        if (result == SUCCESS) {
            if (PowerState == 1) state_accel = ACCEL_SWITCH_WAIT_ON;
            else state_accel = ACCEL_SWITCH_WAIT_OFF;
            return SUCCESS;
        } else {
            return FAIL;
        }

    }

/* ==== PowerSwitch ======================================================== */

    /**
     *
     */
    event result_t PowerSwitch.setDone(bool local_result) {
      
        result_t result;

        if (state_accel == ACCEL_SWITCH_WAIT_ON) {
            
            //NOTE:
            //ADXL202E has turn on time of 160*Cflt+0.3 ms
            //With Cflt = 0.1 uF, Turn on time = ~16 msec.
            result = call Timer.start(TIMER_ONE_SHOT, ACCEL_SWITCH_POWER_ON_WAIT_MS);
            
            //TODO:
            //Check return values
            
            //Calibrate the to the batt voltage
            call ADCControl.manualCalibrate();
            
            //Note:
            //if the timer failed to start well just give the signal now.
            //the accel sample will be bad but at least this won't hang the code
            //nor cause a 16ms spin
            if (result == FAIL) signal AccelCmd.SwitchesSet(1);
            call PowerManagement.adjustPower();
            return SUCCESS;
            
        } else if (state_accel == ACCEL_SWITCH_WAIT_OFF) {
            
            state_accel = ACCEL_SWITCH_IDLE;
            signal AccelCmd.SwitchesSet(0);
            call PowerManagement.adjustPower();
            return SUCCESS;
            
        } else {
            
            //NOTE:
            //When the switch trips, every module that wires into the switch 
            //will trip.  So each module must know if they have control of the 
            //switch or not!!!
            
            //Do nothing
            return SUCCESS;
        }
    }

 
    /**
     *
     */
    event result_t PowerSwitch.setAllDone(bool local_result) {
        return SUCCESS;
    }
    
    
    /**
     *
     */
    event result_t PowerSwitch.getDone(char value) {
        return SUCCESS;
    }

/* ==== Time Fire ========================================================== */

    /**
     *
     */
    event result_t Timer.fired() {
        state_accel = ACCEL_SWITCH_IDLE;
        signal AccelCmd.SwitchesSet(1);
        return SUCCESS;
    } 

}

/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: TempHumM.nc,v 1.1.2.4 2007/12/03 22:28:11 xyang Exp $
 */

/*
 *
 * Authors:     Mohammad Rahmim, Joe Polastre
 *              M. Grimmer 5/15/07 rewritten by to reflect how the hardware actually works 
 *              Xin Yang 
 */
 
includes sensorboard;

module TempHumM {
    provides {
        //interface StdControl;
        interface SplitControl;
        interface ADC as TempSensor;
        interface ADC as HumSensor; 
    }
    uses {
        interface Timer;
        interface StdControl as TimerControl;
        interface Leds;
    }
}

implementation {
    
    //#include "uartMacros.h"
    
/* === Frame States ======================================================== */    
    
    ///Sensor States
    enum {
        TEMPHUM_SENSOR_READY,
        TEMPHUM_SENSOR_GET_TEMP,
        TEMPHUM_SENSOR_GET_HUMD,
        TEMPHUM_SENSOR_OFF,
    };
    
    uint8_t sensorState = TEMPHUM_SENSOR_OFF;
    
    ///Timer States
    enum {
        TEMPHUM_TIMER_IDLE,
        TEMPHUM_TIMER_START_WAIT,
        TEMPHUM_TIMER_RESET_WAIT,
        TEMPHUM_TIMER_SAMPLE_TEMP_WAIT,
        TEMPHUM_TIMER_SAMPLE_HUMD_WAIT,
    };
    
    uint8_t timerState = TEMPHUM_TIMER_IDLE;
        

/* === Prototypes ========================================================== */    

    inline void activePinState();
    inline void powerDownPinState();

    inline result_t setStartResetTimer();
    inline result_t setErrorResetTimer();
    inline result_t setSampleTempWaitTimer();
    inline result_t setSampleHumdWaitTimer();
    
    task void getData();
    task void signalDataError();
    inline void resetState();
    
    
/* === Bit Bang Functions ================================================== */

    /**
     *  Send start sequence.
     *  Default: Data Hi, Clk Lo
     *  Leave: Data Hi (pulled up), Clk Hi
     */
    static inline void initseq() {     
        
        HUMIDITY_SET_CLOCK();       
        TOSH_wait_250ns();
        HUMIDITY_MAKE_DATA_OUTPUT();
        HUMIDITY_CLEAR_DATA();
        TOSH_wait_250ns();  
        HUMIDITY_CLEAR_CLOCK();
        TOSH_wait_250ns();
        HUMIDITY_SET_CLOCK();       
        TOSH_wait_250ns();
        HUMIDITY_SET_DATA();
        TOSH_wait_250ns();
        HUMIDITY_CLEAR_CLOCK();
        TOSH_wait_250ns();  
        HUMIDITY_MAKE_DATA_INPUT();   
        HUMIDITY_SET_DATA();
        
    }

    /**
     *  Send reset sequence
     *  Leave: Data Hi (pulled up), Clk Hi
     */
    static inline void reset() {
        uint8_t i;
        
        //set Data Hi
        HUMIDITY_MAKE_DATA_OUTPUT();
        HUMIDITY_SET_DATA();

        //toggle clk for 9 cycles
        for (i=0;i<9;i++) {
            HUMIDITY_SET_CLOCK();
            TOSH_wait_250ns();
            HUMIDITY_CLEAR_CLOCK();
            TOSH_wait_250ns();
        }
        
        //leave Data Hi (pulled up), Clk Lo
        TOSH_wait_250ns();
        HUMIDITY_MAKE_DATA_INPUT(); 
        HUMIDITY_SET_DATA();
        TOSH_wait_250ns();  
    }
  

    /**
     *  Data changes on low clock, latched on rising edge of clock
     *  Data is msb first
     */
    static inline result_t writebyte(uint8_t cmd) {
        uint8_t ack;
        uint8_t i;
        
        HUMIDITY_MAKE_DATA_OUTPUT();
        for (i=0;i<8;i++) {
            if (cmd&0x80) HUMIDITY_SET_DATA();
            else HUMIDITY_CLEAR_DATA();
            
            cmd = cmd << 1;
            TOSH_wait_250ns();      
            HUMIDITY_SET_CLOCK();     
            TOSH_wait_250ns();
            HUMIDITY_CLEAR_CLOCK();
            TOSH_wait_250ns();
        }
        
        // read ack         
        HUMIDITY_MAKE_DATA_INPUT(); 
        HUMIDITY_SET_DATA();
        TOSH_wait_250ns();
        HUMIDITY_SET_CLOCK(); 
        TOSH_wait_250ns();
        ack = HUMIDITY_GET_DATA();
        TOSH_wait_250ns();
        HUMIDITY_CLEAR_CLOCK();     
                
        if (ack == 0) return SUCCESS;
        else return FAIL;      
    }

    /**
     *
     */
    static inline uint8_t readbyte() { 
        uint8_t temp = 0;
        uint8_t i;
        
        HUMIDITY_MAKE_DATA_INPUT(); 
        HUMIDITY_SET_DATA();
        
        for (i=0;i<8;i++) {
            TOSH_wait_250ns();
            TOSH_wait_250ns();    //added extra wait here???
            HUMIDITY_SET_CLOCK(); 
            temp |= HUMIDITY_GET_DATA();  

            //don't shift the last bit
            if (i != 7) temp = temp << 1;
            
            TOSH_wait_250ns();      
            HUMIDITY_CLEAR_CLOCK();
        }
        
        // send ack         
        HUMIDITY_MAKE_DATA_OUTPUT();    
        HUMIDITY_CLEAR_DATA();
        TOSH_wait_250ns();    
        HUMIDITY_SET_CLOCK();
        TOSH_wait_250ns();  
        HUMIDITY_CLEAR_CLOCK();
        HUMIDITY_MAKE_DATA_INPUT(); 
        HUMIDITY_SET_DATA();
        TOSH_wait_250ns();   

        return temp;
    }

    /**
     *
     */
    char calc_crc(char current, char in) {
        return crctable[current ^ in];
    }
    
/* === Pin State Functions ================================================= */

    /**
     *  Clk Out Hi, Data In Lo
     */    
    inline void activePinState() {
        HUMIDITY_MAKE_CLOCK_OUTPUT();       
        HUMIDITY_CLEAR_CLOCK();
        HUMIDITY_MAKE_DATA_INPUT();
        HUMIDITY_SET_DATA();
    }
    
    /**
     *  Clk In Lo (tristate), Data In Lo (tristate)
     */
    inline void powerDownPinState() {
        HUMIDITY_CLEAR_CLOCK();
        HUMIDITY_MAKE_CLOCK_INPUT();
        HUMIDITY_MAKE_DATA_INPUT();
        HUMIDITY_CLEAR_DATA();
    }
        
/* === Tasks =============================================================== */

    task void signalInitDone() {
        signal SplitControl.initDone();
    }
    
    task void signalStartDone() {
        signal SplitControl.startDone();
    }
    
    task void signalStopDone() {
        signal SplitControl.stopDone();
    }
    
/* === StdControl ========================================================== */

    /**
     *
     */
    command result_t SplitControl.init() {
        call TimerControl.init();
        atomic sensorState = TEMPHUM_SENSOR_OFF;
        
        if (post signalInitDone() == FAIL) return FAIL;
        
        return SUCCESS;
    }

    /**
     *
     */
    command result_t SplitControl.start() {
        call TimerControl.start();
        
        activePinState();                       //set pins to active state
        reset();                                //io line reset
        initseq();                              //chip rest
        writebyte(TOSH_HUMIDITY_RESET);
        
        //wait for reset to complete
        if (setStartResetTimer() == FAIL) return FAIL;

        return SUCCESS;
    }

    /**
     *
     */
    command result_t SplitControl.stop() {
        powerDownPinState();                       //set pins to sleep state
        atomic sensorState = TEMPHUM_SENSOR_OFF;  

        if (post signalStopDone() == FAIL) return FAIL;
        
        return SUCCESS;
    }

/* === TempSensor + HumSensor ============================================== */

    //NOTE:
    //Even though these are declared async, we should not use them in 
    //async fashion
    
    //TODO:
    //Make this async safe?

    /**
     *
     */
    async command result_t HumSensor.getData() {
        uint8_t lSensorState;
        bool bFail = FALSE;
        
        //State checking
        atomic {
            lSensorState = sensorState;
            if (lSensorState != TEMPHUM_SENSOR_READY) bFail = TRUE;
            else sensorState = TEMPHUM_SENSOR_GET_HUMD;
        }
        
        if (bFail) return FAIL;

        // Start the get data process
        reset();
        initseq();
        if (writebyte(TOSH_HUMIDITY_ADDR) == FAIL) {
          reset();          
          atomic sensorState = TEMPHUM_SENSOR_READY;
          return FAIL;
        }
        
        //set the wait timer
        if (setSampleHumdWaitTimer() == FAIL) {
            atomic sensorState = TEMPHUM_SENSOR_READY;
            return FAIL;
        }
        
        return SUCCESS;
    }

    /**
     *
     */
    async command result_t TempSensor.getData() {
        uint8_t lSensorState;
        bool bFail = FALSE;
        
        
        //State checking
        atomic {
            lSensorState = sensorState;
            if (lSensorState != TEMPHUM_SENSOR_READY) bFail = TRUE;
            else sensorState = TEMPHUM_SENSOR_GET_TEMP;
        }
        
        if (bFail) return FAIL;
        
        // Start the get data process
        reset();
        initseq();
        if (writebyte(TOSH_HUMIDTEMP_ADDR) == FAIL) {
          reset();          
          atomic sensorState = TEMPHUM_SENSOR_READY;
          return FAIL;
        }
        
        //set the wait timer
        if (setSampleTempWaitTimer() == FAIL) {
            atomic sensorState = TEMPHUM_SENSOR_READY;
            return FAIL;
        }
        
        return SUCCESS;
    }

    ///Not implemented
    async  command result_t TempSensor.getContinuousData() { return FAIL; }
    async  command result_t HumSensor.getContinuousData() { return FAIL; }
    

/* === Timer Functions ===================================================== */

    /**
     *
     */
    inline result_t setStartResetTimer() {
        uint8_t lTimerState;
        
        //check if timer already used by others
        atomic lTimerState = timerState;
        if (lTimerState != TEMPHUM_TIMER_IDLE) return FAIL;
        
        //set wait timer for > 11ms before using the sensor
        if (call Timer.start(TIMER_ONE_SHOT, 12) == SUCCESS)
            atomic timerState = TEMPHUM_TIMER_START_WAIT;
        else return FAIL;
    }
    
    /**
     *
     */
    inline result_t setErrorResetTimer() {
        uint8_t lTimerState;
        
        //check if timer already used by others
        atomic lTimerState = timerState;
        if (lTimerState != TEMPHUM_TIMER_IDLE) return FAIL;
        
        //set wait timer for > 11ms before using the sensor
        if (call Timer.start(TIMER_ONE_SHOT, 12) == SUCCESS)
            atomic timerState = TEMPHUM_TIMER_RESET_WAIT;
        else return FAIL;
    }
    
    
    task void setTempTimerTask() {
        //NOTE:
        //This call must succeed, cause there's not error interface
        call Timer.start(TIMER_ONE_SHOT, 250);
    }
    
    /**
     *
     */
    inline result_t setSampleTempWaitTimer() {
        uint8_t lTimerState;
        
        //check if timer already used by others
        atomic lTimerState = timerState;
        if (lTimerState != TEMPHUM_TIMER_IDLE) return FAIL;
        
        //14 bit needs 210 ms
        //if (call Timer.start(TIMER_ONE_SHOT, 250) == SUCCESS)
        if (post setTempTimerTask() == SUCCESS)
            atomic timerState = TEMPHUM_TIMER_SAMPLE_TEMP_WAIT;
        else return FAIL;
    }
    
    
    
    task void setHumdTimerTask() {
        //NOTE:
        //This call must succeed, cause there's not error interface
        call Timer.start(TIMER_ONE_SHOT, 75);
    }
    
    /**
     *
     */
    inline result_t setSampleHumdWaitTimer() {
        uint8_t lTimerState;
        
        //check if timer already used by others
        atomic lTimerState = timerState;
        if (lTimerState != TEMPHUM_TIMER_IDLE) return FAIL;
        
        //12 bit needs 65 ms
        //if (call Timer.start(TIMER_ONE_SHOT, 75) == SUCCESS)
        if (post setHumdTimerTask() == SUCCESS)
            atomic timerState = TEMPHUM_TIMER_SAMPLE_HUMD_WAIT;
        else return FAIL;           
    }
    

/* === Timer =============================================================== */

    /**
     *
     */
    event result_t Timer.fired() {        
        uint8_t lTimerState;

        atomic {
            lTimerState = timerState;
            timerState = TEMPHUM_TIMER_IDLE;      //reset timer state
        }

        switch (lTimerState) {

            //Initial reset done
            case TEMPHUM_TIMER_START_WAIT:
                atomic sensorState = TEMPHUM_SENSOR_READY;
                if (post signalStartDone() == FAIL) signal SplitControl.startDone();
                break;

            //Error reset done
            case TEMPHUM_TIMER_RESET_WAIT:
                if (post signalDataError() == FAIL) resetState();
                break;

            //Sample wait done  
            case TEMPHUM_TIMER_SAMPLE_TEMP_WAIT:
            case TEMPHUM_TIMER_SAMPLE_HUMD_WAIT:
                if (post getData() == FAIL) resetState();
                break;

            case TEMPHUM_TIMER_IDLE:
                break;

            default:
                break;
        }

        return SUCCESS;
    }
  
  
    /**
     *
     */
    task void getData() {
        uint8_t lSensorState;
        uint16_t data;
        atomic lSensorState = sensorState;
        
        
        
        // unit not responding
        if (HUMIDITY_GET_DATA()) {     
            
            //chip reset the sensor
            reset();
            initseq();
            writebyte(TOSH_HUMIDITY_RESET);
            if (setErrorResetTimer() == FAIL) resetState();
            
            return;
        }
        
        //TODO:
        //look at the crc stuff !!!

        //grab data
        data = readbyte()<<8;
        data = data | (readbyte()&0x00ff);
        readbyte();  // crc not used
        
//         #if 1
//         hi = readbyte();
//         lo = readbyte();
//         readbyte();
//         
//         data = hi;
//         data = data << 8;
//         data = data | lo;
//         
//         SODbg(SO_DEBUG_XMTS400_TEST, (uint8_t *) "Hi:%x ", hi);
//         SODbg(SO_DEBUG_XMTS400_TEST, (uint8_t *) "Lo:%x ", lo);
//         SODbg(SO_DEBUG_XMTS400_TEST, (uint8_t *) "Sum:%i\n", data);
//         #endif

        atomic sensorState = TEMPHUM_SENSOR_READY;
        
        //Signal the data
        if (lSensorState == TEMPHUM_SENSOR_GET_HUMD) {
            signal HumSensor.dataReady(data);
        } else if (lSensorState == TEMPHUM_SENSOR_GET_TEMP) {
            signal TempSensor.dataReady(data);
        }
    }
    
    /**
     *
     */
    task void signalDataError() {
        resetState();
    }
  
    /**
     *
     */
    inline void resetState() {
        uint8_t lSensorState;
        
        atomic {
            lSensorState = sensorState;
            sensorState = TEMPHUM_SENSOR_READY;        //reset sensor state
        }
        
        if (lSensorState == TEMPHUM_SENSOR_GET_HUMD) {
            signal HumSensor.dataReady(0);
        } else if (lSensorState == TEMPHUM_SENSOR_GET_TEMP) {
            signal TempSensor.dataReady(0);
        }
        
        //TODO:
        //create proper error interface and not just return 0
    }
}

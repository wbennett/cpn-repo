/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: TaosPhoto.nc,v 1.1.2.4 2007/12/03 22:28:11 xyang Exp $
 */

/*
 *
 * Authors:     Joe Polastre
 * rewrote to fix major logic flaws 5/16/07 M. Grimmer
 */

includes sensorboard;

configuration TaosPhoto
{
  provides {
    interface ADC[uint8_t id];
    interface SplitControl;
  }
}
implementation
{
  components TaosPhotoM
    , I2CPacketC
    , MicaWbSwitch
    , TimerC
    , LedsC
    ;

  SplitControl = TaosPhotoM;
  ADC = TaosPhotoM;

  TaosPhotoM.I2CPacketControl -> I2CPacketC.StdControl;
  TaosPhotoM.I2CPacket -> I2CPacketC.I2CPacket[TOSH_PHOTO_ADDR];

  TaosPhotoM.SwitchControl -> MicaWbSwitch.StdControl;
  TaosPhotoM.Switch -> MicaWbSwitch.Switch[0];
    
  TaosPhotoM.TimerControl -> TimerC;
  TaosPhotoM.Timer -> TimerC.Timer[unique("Timer")];
  TaosPhotoM.Leds -> LedsC;

}

/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: gps.h,v 1.1.2.4 2007/12/03 22:28:11 xyang Exp $
 */


#ifndef XBOW_GPS_H
#define XBOW_GPS_H

#define GPS_DATA_LENGTH  128             //max payload length of GPS_MSG
#define GPS_PACKET_START 0x24            //start of gps packet
#define GPS_PACKET_END1  0x0D            // <LF> end if gps packet
#define GPS_PACKET_END2  0x0A            // <CR> end of gps packet

//#define GPS_MSG_LENGTH 100
//#define GPS_CHAR 11
#define GGA_FIELDS 				( 10 )	//these define the temp buffer for GGA Parsing
#define RMC_FIELDS				( 10 )
#define GPS_CHAR_PER_FIELD		( 10 )
#define GPS_DELIMITER 		','
#define GPS_END_MSG 		'*'


/**
 *  This packet is what the TOS_MSG is cast into when the gps signals
 *  the Raw Receive.
 */
typedef struct GPS_Msg {
  uint8_t length;  //does not include len or crc
  int8_t data[GPS_DATA_LENGTH];  //should be uint8_t
  uint16_t crc;
} __attribute__ ((packed)) GPS_Msg;

typedef GPS_Msg *GPS_MsgPtr;

typedef union raw_storage_buffer {
	char parsed_gga[GGA_FIELDS][GPS_CHAR_PER_FIELD];
	char parsed_rmc[RMC_FIELDS][GPS_CHAR_PER_FIELD];
} raw_storage_buffer;

/**
 *  This packet is what the TOS_MSG is cast into when the gps signals
 *  GGA receive.
 */
typedef struct GGAMsg 
	{
	uint8_t  hours;
	uint8_t  minutes;
	uint8_t  Lat_deg;
	uint8_t  Long_deg;
	
	uint32_t dec_sec;
	uint32_t Lat_dec_min;
	uint32_t Long_dec_min;
	
	uint8_t  NSEWind;
	uint8_t  valid;
	  
	uint8_t  num_sats;
	
	uint16_t altitude;
	} __attribute__ ((packed)) GGAMsg;

typedef struct RMCMsg 
	{
	uint8_t		valid;
	uint16_t	sog;
	uint32_t	date;
	uint16_t	cog;
	} __attribute__ ((packed)) RMCMsg;

typedef union NMEAMsg
	{
	GGAMsg	GGAMsgBuffer;
	RMCMsg	RMCMsgBuffer;
	} NMEAMsg;

//NOTE:
//Should be taken out of the appFeatrures.h for MTS420CC

//NOTE:
//These macros of multiplying 0.01 and stuff might not be such a good idea.

//NMEA_EXTRA_STUFF
#ifndef NMEA_EXTRACTION_MACROS
#define NMEA_EXTRACTION_MACROS

#define extract_num_sats_m(data)     (10*(data[0]-'0') + (data[1]-'0'))

#define extract_hours_m(data)        (10*(data[0]-'0') + (data[1]-'0'))

#define extract_minutes_m(data)      (10*(data[2]-'0') + (data[3]-'0'))

#define extract_dec_sec_m(data)      (10*(data[4]-'0') +  (data[5]-'0') + 0.1*(data[7]-'0') \
                                      + 0.01*(data[8]-'0') + 0.001*(data[9]-'0'))
                                      
#define extract_Lat_deg_m(data)      (10*(data[0]-'0') + (data[1]-'0'))

#define extract_Lat_dec_min_m(data)  (10*(data[2]-'0') +  (data[3]-'0') + 0.1*(data[5]-'0') \
                                      + 0.01*(data[6]-'0') + 0.001*(data[7]-'0') + 0.0001*(data[8]-'0'))
                                      
#define extract_Long_deg_m(data)     (100*(data[0]-'0') + 10*(data[1]-'0') + (data[2]-'0'))

#define extract_Long_dec_min_m(data) (10*(data[3]-'0') +  (data[4]-'0') + 0.1*(data[6]-'0') \
                      + 0.01*(data[7]-'0') + 0.001*(data[8]-'0') + 0.0001*(data[9]-'0'))
						
#define is_gga_string_m(ns) ((ns[3]=='G')&&(ns[4]=='G')&&(ns[5]=='A'))
#define is_rmc_string_m(ns) ((ns[3]=='R')&&(ns[4]=='M')&&(ns[5]=='C'))

//NOTE:
//We don't reall need this param, but the old sensor board apps do
#define GPS_MAX_WAIT 10

#endif  
//NMEA_EXTRA_STUFF


#endif /* XBOW_GPS_H */


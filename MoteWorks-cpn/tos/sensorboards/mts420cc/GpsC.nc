/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: GpsC.nc,v 1.1.2.4 2007/12/03 22:28:10 xyang Exp $
 */

configuration GpsC
{
  provides interface Gps
  }
}
implementation
{
  components GpsPacketM;
  Gps = GpsPacketM.Gps;
}


interface EnableControl {
    
    command result_t enable();
    
    command result_t disable();
    
    command bool isEnabled();
    
}


configuration GpsUpperC {

    provides {
        interface SplitControl as GpsSplitControl;
        interface ReceiveMsg as GpsRecv;
        interface ReceiveMsg as GpsGGARecv;
		interface ReceiveMsg as GpsRMCRecv;
        interface EnableControl as PowerOffOnStop;
    }
}

implementation {
    
    components 
        GpsUpperM,
        GpsPacket as Packet, 
        UART1 as UART,
        MicaWbSwitch,
        TimerC,
        LedsC;

    //Provided        
    GpsSplitControl = GpsUpperM;
    GpsRecv = GpsUpperM.GpsRecv;
    GpsGGARecv = GpsUpperM.GpsGGARecv;
	GpsRMCRecv = GpsUpperM.GpsRMCRecv;
    PowerOffOnStop = GpsUpperM.PowerOffOnStop;
    
    //Upper
    GpsUpperM.GpsCmd -> Packet.GpsCmd;
    GpsUpperM.GpsRawControl -> Packet.Control;
    GpsUpperM.GpsRawRecv -> Packet.Receive;
    //GpsUpperM.GpsTimer -> TimerC.Timer[unique("Timer")];
    GpsUpperM.Leds -> LedsC;
    
    //Packet
    Packet.ByteControl -> UART;
    Packet.ByteComm -> UART;
    Packet.SwitchControl -> MicaWbSwitch.StdControl;
    Packet.Switch1 -> MicaWbSwitch.Switch[0];
    Packet.SwitchI2W -> MicaWbSwitch.Switch[1];
    
}

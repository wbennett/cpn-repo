/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: SensirionHumidityM.nc,v 1.1.2.4 2007/12/03 22:28:11 xyang Exp $
 */

/*
 *
 * Authors:     Joe Polastre
 *
 */

includes sensorboard;

module SensirionHumidityM {
  provides {
    interface SplitControl;
    interface ADC as Humidity;
    interface ADC as Temperature;
    
    //dummy
    interface ADCError as HumidityError;
    interface ADCError as TemperatureError;
  }
  uses {
    //Sensor
    interface SplitControl as SensorControl;
    interface ADC as HumSensor;
    interface ADC as TempSensor;
    
    //Switch
    interface StdControl as SwitchControl;
    interface Switch as PowerSwitch;
    interface Switch as IOSwitch;
    
    //Misc
    interface Leds;
  }
}

implementation {

/* === Frame State ========================================================= */

  enum { 
      SENSOR_OFF,                 //every thing off
      
      POWER_SWITCH_WAIT_ON,       //power switch busy wait for on
      POWER_SWITCH_WAIT_OFF,      //power switch busy wait for off
      
      IO_SWITCH_WAIT,             //io switch start
      IO_SWITCH_WAIT_IO1,         //io switch busy way for io1
      IO_SWITCH_WAIT_IO2,         //io switch busy way for io2
      
      SENSOR_READY,               //power on, io active, sensor ready
      SENSOR_BUSY,                //sensor busy
  };
  
  uint8_t state;
  
  
  enum {      
      IOON = 1, 
      IOOFF = 0 
  };
  
  uint8_t iostate;
  

/* === Tasks =============================================================== */

    /// SplitControl Stuff
    
    //NOTE:
    //This task never posted
    
    task void initDone() {
        signal SplitControl.initDone();
    }
    
    task void startDone() {
        atomic state = SENSOR_READY;
        signal SplitControl.startDone();
    }
    
    task void stopDone() {
        atomic state = SENSOR_OFF;
        signal SplitControl.stopDone();
    }

    
    //TODO:
    //Need to check return statments from the switch calls!!!
    
    /**
     *
     */
    task void IOBus() {
        uint8_t l_state, l_iostate;

        atomic {
            l_state = state;
            l_iostate = iostate;
        }

        if (l_state == IO_SWITCH_WAIT) {
            atomic state = IO_SWITCH_WAIT_IO1;
            call IOSwitch.set(MICAWB_HUMIDITY_SCLK, l_iostate);
        }
        else if (l_state == IO_SWITCH_WAIT_IO1) {
            atomic state = IO_SWITCH_WAIT_IO2;
            call IOSwitch.set(MICAWB_HUMIDITY_DATA, l_iostate);
        }
        else if (l_state == IO_SWITCH_WAIT_IO2) {
            
            //NOTE:
            //If the lower splitcontrol call fails, there is nothing we we
            //can do other than signal done.  There is no way to tell
            //the upper layer that this call failed.
            
            if (l_iostate == IOOFF) {
                
                //stop the lower layer
                if (call SensorControl.stop() == FAIL) {
                    post stopDone();  
                }
                
            }
            else {
                
                //start the lower layer
                if (call SensorControl.start() == FAIL) {
                    post startDone();
                }
            }
        }
    }

/* === StdControl ========================================================== */

    /**
     *
     */
    command result_t SplitControl.init() {
        atomic {
            state = SENSOR_OFF;
            iostate = IOOFF;
        }
        
        call SwitchControl.init();
        if (call SensorControl.init() == FAIL) return FAIL;
        
        return SUCCESS;
    }
    
    /**
     *
     */
    event result_t SensorControl.initDone() {
        signal SplitControl.initDone();
        return SUCCESS;
    }

    /**
     *
     */
    command result_t SplitControl.start() {
        
        //NOTE:
        //do i need protection for multiple start calls?
        
        call SwitchControl.start();
        if (call PowerSwitch.set(MICAWB_HUMIDITY_POWER,1) != SUCCESS) {
            return FAIL;
        }
        
        atomic state = POWER_SWITCH_WAIT_ON;
        return SUCCESS;
    }
    
    /**
     *
     */
    event result_t SensorControl.startDone() {
        atomic state = SENSOR_READY;
        signal SplitControl.startDone();
        return SUCCESS;
    }

    /**
     *
     */
    command result_t SplitControl.stop() {
        
        //NOTE:
        //do i need protection for multiple stop calls?
        
        if (call PowerSwitch.set(MICAWB_HUMIDITY_POWER,0) != SUCCESS) {
            return FAIL;
        }
        
        atomic state = POWER_SWITCH_WAIT_OFF;
        return SUCCESS;
    }
    
    /**
     *
     */
    event result_t SensorControl.stopDone() {
        atomic state = SENSOR_OFF;
        signal SplitControl.stopDone();
        return SUCCESS;
    }

/* === Power Swtich ======================================================== */
 
  event result_t PowerSwitch.setDone(bool l_result) {
    uint8_t l_state;
    
    atomic l_state = state;
    
    if (l_state == POWER_SWITCH_WAIT_ON) {
        atomic {
            state = IO_SWITCH_WAIT;
            iostate = IOON;
        }
        post IOBus();  //if this post fails, we hang
    } else if (l_state == POWER_SWITCH_WAIT_OFF) {
        atomic {
            state = IO_SWITCH_WAIT;
            iostate = IOOFF;
        }
        post IOBus();  //if this post fails, we hang
    } else {
        //DO NOTHING
    }
    
    return SUCCESS;
  }
  
  event result_t PowerSwitch.getDone(char value) {
    return SUCCESS;
  }

  event result_t PowerSwitch.setAllDone(bool l_result) {
    return SUCCESS;
  }
  
/* === I/O Switch ========================================================== */

  /**
   *
   */
  event result_t IOSwitch.setDone(bool result) {
      uint8_t l_state;
      
      atomic l_state = state;
      if ( (l_state == IO_SWITCH_WAIT_IO1) || 
           (l_state == IO_SWITCH_WAIT_IO2) ) 
      {
          post IOBus();
      }
      
      return SUCCESS;
      
      //TODO:
      //check result ?
  }

  event result_t IOSwitch.setAllDone(bool l_result) {
    return SUCCESS;
  }
  
  event result_t IOSwitch.getDone(char value) {
    return SUCCESS;
  }


/* === Temp + Pressure ===================================================== */


 //TODO:
 //Check these return statments?


 async command result_t Humidity.getData() {
    uint8_t l_state;
    
    atomic l_state = state;
    if (l_state == SENSOR_READY)
    {
      
      if (call HumSensor.getData() == FAIL) {
          atomic state = SENSOR_READY;
          return FAIL;
      } else {
          atomic state = SENSOR_BUSY;
          return SUCCESS;
      }
    }
    
    return FAIL;
  }

 async event result_t HumSensor.dataReady(uint16_t data) {
    atomic state = SENSOR_READY;
    signal Humidity.dataReady(data);
    return SUCCESS;
  }

 async command result_t Temperature.getData() {
    uint8_t l_state;
    
    atomic l_state = state;
    if (l_state == SENSOR_READY)
    {
      
      if (call TempSensor.getData() == FAIL) {
          atomic state = SENSOR_READY;
          return FAIL;
      } else {
          atomic state = SENSOR_BUSY;
          return SUCCESS;
      }
    }
    
    return FAIL;
  }

  async event result_t TempSensor.dataReady(uint16_t data) {
    atomic state = SENSOR_READY;
    signal Temperature.dataReady(data);
    return SUCCESS;
  }

  // no such thing
  async command result_t Humidity.getContinuousData() {
    return FAIL;
  }

  // no such thing
  async command result_t Temperature.getContinuousData() {
    return FAIL;
  }
  
/* === Dummy =============================================================== */

  command result_t HumidityError.enable() {
      return SUCCESS;
  }
  
  command result_t HumidityError.disable() {
      return SUCCESS;
  }
  
  command result_t TemperatureError.enable() {
      return SUCCESS;
  }
  
  command result_t TemperatureError.disable() {
      return SUCCESS;
  }

/* === Temp + Pressure ===================================================== */

  default async event result_t Humidity.dataReady(uint16_t data)
  {
    return SUCCESS;
  }

  default async event result_t Temperature.dataReady(uint16_t data)
  {
    return SUCCESS;
  }

 
}


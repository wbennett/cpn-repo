/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: IntersemaPressureM.nc,v 1.1.2.4 2007/12/03 22:28:10 xyang Exp $
 */

/*
 *
 * Authors:     Joe Polastre
 *
 */

includes sensorboard;

module IntersemaPressureM {
  provides {
    interface ADC as Temperature;
    interface ADC as Pressure;
    interface SplitControl;
    interface Calibration;
  }
  uses {
    interface StdControl as SwitchControl;
    interface StdControl as LowerControl;
    interface Calibration as LowerCalibrate;
    interface Switch as PowerSwitch;
    interface Switch as IOSwitch;
    interface ADC as LowerPressure;
    interface ADC as LowerTemp;
    interface Leds;
  }
}

implementation {

/* === Frame State ========================================================= */

  enum { 
      SENSOR_OFF,                 //every thing off
      
      POWER_SWITCH_WAIT_ON,       //power switch busy wait for on
      POWER_SWITCH_WAIT_OFF,      //power switch busy wait for off
      
      IO_SWITCH_WAIT,             //io switch start
      IO_SWITCH_WAIT_IO1,         //io switch busy way for io1
      IO_SWITCH_WAIT_IO2,         //io switch busy way for io2
      IO_SWITCH_WAIT_IO3,         //io switch busy way for io3
      
      SENSOR_READY,               //power on, io active, sensor ready
      SENSOR_BUSY,                //sensor busy
  };
  
  uint8_t state;
  
  
  enum {      
      IOON = 1, 
      IOOFF = 0 
  };
  
  uint8_t iostate;
  

/* === Tasks =============================================================== */

    /// SplitControl Stuff
    task void initDone() {
        signal SplitControl.initDone();
    }

    task void stopDone() {
        atomic state = SENSOR_OFF;
        signal SplitControl.stopDone();
    }

    task void startDone() {
        atomic state = SENSOR_READY;
        signal SplitControl.startDone();
    }

    /**
     *
     */
    task void IOBus() {
        uint8_t l_state, l_iostate;

        atomic {
            l_state = state;
            l_iostate = iostate;
        }

        if (l_state == IO_SWITCH_WAIT) {
            atomic state = IO_SWITCH_WAIT_IO1;
            call IOSwitch.set(MICAWB_PRESSURE_SCLK, l_iostate);
        }
        else if (l_state == IO_SWITCH_WAIT_IO1) {
            atomic state = IO_SWITCH_WAIT_IO2;
            call IOSwitch.set(MICAWB_PRESSURE_DIN, l_iostate);
        }
        else if (l_state == IO_SWITCH_WAIT_IO2) {
            atomic state = IO_SWITCH_WAIT_IO3;
            call IOSwitch.set(MICAWB_PRESSURE_DOUT, l_iostate);
        }
        else if (l_state == IO_SWITCH_WAIT_IO3) {
            
            if (l_iostate == IOOFF) {
                call LowerControl.stop();  //stop the lower layer
                post stopDone();
            }
            else {
                call LowerControl.start();  //start the lower layer
                post startDone();
            }
        }
    }

/* === StdControl ========================================================== */

    /**
     *
     */
    command result_t SplitControl.init() {
        atomic {
            state = SENSOR_OFF;
            iostate = IOOFF;
        }

        call LowerControl.init();
        call SwitchControl.init();
        if (post initDone() == FAIL) return FAIL;

        return SUCCESS;
    }

    /**
     *
     */
    command result_t SplitControl.start() {
        
        //NOTE:
        //do i need protection for multiple start calls?
        
        //NOTE:
        //I should probably think about how power management is handled here?
        
        call SwitchControl.start();
        if (call PowerSwitch.set(MICAWB_PRESSURE_POWER,1) != SUCCESS) {
            return FAIL;
        }
        
        atomic state = POWER_SWITCH_WAIT_ON;
        return SUCCESS;
    }

    /**
     *
     */
    command result_t SplitControl.stop() {
        
        //NOTE:
        //do i need protection for multiple stop calls?
        
        //NOTE:
        //I should probably think about how power management is handled here?
 
        
        if (call PowerSwitch.set(MICAWB_PRESSURE_POWER,0) != SUCCESS) {
            return FAIL;
        }
        
        atomic state = POWER_SWITCH_WAIT_OFF;
        return SUCCESS;
    }

/* === Power Swtich ======================================================== */
 
  event result_t PowerSwitch.setDone(bool l_result) {
    uint8_t l_state;
    
    atomic l_state = state;
    
    if (l_state == POWER_SWITCH_WAIT_ON) {
        atomic {
            state = IO_SWITCH_WAIT;
            iostate = IOON;
        }
        post IOBus();  //if this post fails, we hang
    } else if (l_state == POWER_SWITCH_WAIT_OFF) {
        atomic {
            state = IO_SWITCH_WAIT;
            iostate = IOOFF;
        }
        post IOBus();  //if this post fails, we hang
    } else {
        //DO NOTHING
    }
    
    return SUCCESS;
  }
  
  event result_t PowerSwitch.getDone(char value) {
    return SUCCESS;
  }

  event result_t PowerSwitch.setAllDone(bool l_result) {
    return SUCCESS;
  }
  
/* === I/O Switch ========================================================== */

  /**
   *
   */
  event result_t IOSwitch.setDone(bool result) {
      uint8_t l_state;
      
      atomic l_state = state;
      if ( (l_state == IO_SWITCH_WAIT_IO1) || 
           (l_state == IO_SWITCH_WAIT_IO2) || 
           (l_state == IO_SWITCH_WAIT_IO3) ) 
      {
          post IOBus();
      }
      
      return SUCCESS;
      
      //TODO:
      //check result ?
  }

  event result_t IOSwitch.setAllDone(bool l_result) {
    return SUCCESS;
  }
  
  event result_t IOSwitch.getDone(char value) {
    return SUCCESS;
  }


/* === Temp + Pressure ===================================================== */

    //TODO:
    //Error interface is not being wiried to, could be an issue if data
    //does not return

 async command result_t Temperature.getData() {
    uint8_t l_state;
    
    atomic l_state = state;
    if (l_state == SENSOR_READY)
    {
      
      if (call LowerTemp.getData() == FAIL) {
          atomic state = SENSOR_READY;
          return FAIL;
      } else {
          atomic state = SENSOR_BUSY;
          return SUCCESS;
      }
    }
    return FAIL;
  }

 async event result_t LowerTemp.dataReady(uint16_t data) {
    atomic state = SENSOR_READY;
    signal Temperature.dataReady(data);
    return SUCCESS;
  }
  
    //TODO:
    //Error interface is not being wiried to, could be an issue if data
    //does not return
  
 async command result_t Pressure.getData() {
    uint8_t l_state;
    
    atomic l_state = state;
    if (l_state == SENSOR_READY)
    {
      
      if (call LowerPressure.getData() == FAIL) {
          atomic state = SENSOR_READY;
          return FAIL;
      } else {
          atomic state = SENSOR_BUSY;
          return SUCCESS;
      }
    }
    return FAIL;
  }

  async event result_t LowerPressure.dataReady(uint16_t data) {
    atomic state = SENSOR_READY;
    signal Pressure.dataReady(data);
    return SUCCESS;
  }

  // no such thing
  async command result_t Temperature.getContinuousData() {
    return FAIL;
  }

  // no such thing
  async command result_t Pressure.getContinuousData() {
    return FAIL;
  }

  default async event result_t Temperature.dataReady(uint16_t data)
  {
    return SUCCESS;
  }

  default async event result_t Pressure.dataReady(uint16_t data)
  {
    return SUCCESS;
  }
  
/* === Calibration ========================================================= */

command result_t Calibration.getData() {
    uint8_t l_state;

    atomic l_state = state;
    if (l_state == SENSOR_READY)
    {
      atomic state = SENSOR_BUSY;
      call LowerCalibrate.getData();
      return SUCCESS;
    }
    return FAIL;
  }


  event result_t LowerCalibrate.dataReady(char word, uint16_t value) {
    
    if (word == 4) atomic state = SENSOR_READY;
    signal Calibration.dataReady(word, value);
    
    return SUCCESS;
  }

  default event result_t Calibration.dataReady(char word, uint16_t value) {
    return SUCCESS;
  }

 
}


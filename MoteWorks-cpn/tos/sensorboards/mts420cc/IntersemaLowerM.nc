/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: IntersemaLowerM.nc,v 1.1.2.4 2007/12/03 22:28:10 xyang Exp $
 */

/*
 *
 * Authors:     Joe Polastre
 *
 * $Id: IntersemaLowerM.nc,v 1.1.2.4 2007/12/03 22:28:10 xyang Exp $
 */

includes sensorboard;
includes hardware;   //why is this included?

module IntersemaLowerM {
  provides {
    interface ADC as Pressure;
    interface ADC as Temp;
    interface ADCError as PressError;
    interface ADCError as TempError;
    interface StdControl;
    interface Calibration;
  }
  uses {
    interface Timer;
    interface StdControl as TimerControl;
    interface Leds;
  }
}

implementation {

/* === Frame State ========================================================= */

  enum {
      //Module States
      IDLE=7, 
      RESET=8, 
      
      //Sensor States
      CALIBRATE=9, 
      TEMP=10, 
      PRESSURE=11 
  };
  
  uint8_t state;  //Module State
  uint8_t sensor;  //Sensor State
  
  uint16_t calibration[4];  //Cal values from the sensor

  uint8_t timeout;  //timeout counter for sensor waits
  uint8_t errornum;  //error values

  //enable or disable error events
  bool presserror, temperror;

/* === Prototypes ========================================================== */
    
    void pulse_clock();
    char read_bit();
    void write_bit(bool bit);
    void spi_reset();
    uint16_t adc_read();
    uint16_t spi_word(char num);
    void sense();
    
    void activePinState();
    void powerDownPinState();
    
    task void signalPressError();
    task void signalTempError();  
    task void gotInterrupt();
    task void SPITask();
    
    
/* === Local Functions ===================================================== */

  /**
   *  Pulse the clock one time.
   *  Wait 500ns, Go Hi, Wait 500ns, Go Lo
   *
   *  xx500xx --500-- __
   */
  void pulse_clock() {
    TOSH_wait_250ns(); TOSH_wait_250ns();  //Wait
    PRESSURE_SET_CLOCK();                  //Clk Hi
    TOSH_wait_250ns(); TOSH_wait_250ns();  //Wait
    PRESSURE_CLEAR_CLOCK();                //Clk Lo
  }

  /**
   *  Read one bit.  Assume clock is already Lo.
   *  Will clk hi once, wait, read then return clk to low
   */
  char read_bit() {
    char i;
    
    //TODO:
    //Need to take this out, cause its not doing anything!
    //PRESSURE_CLEAR_OUT_PIN();              //Clr Output pin?
    
    PRESSURE_SET_CLOCK();                  //Clk Hi
    TOSH_wait_250ns(); TOSH_wait_250ns();  //Wait
    i = PRESSURE_READ_IN_PIN();            //Input Read
    PRESSURE_CLEAR_CLOCK();                //Clk Lo
    return i;
  }

  /**
   *  Writes one bit.  Uses the pulse_clock function.  Assumes
   *  clock starts low.
   */
  void write_bit(bool bit) {
    if (bit)
      PRESSURE_SET_OUT_PIN();
    else
      PRESSURE_CLEAR_OUT_PIN();
    pulse_clock();
  }

  /**
   *  resets the intersema device
   */
  void spi_reset() {
    int i = 0;
    for (i = 0; i < 21; i++) {
      if (i < 16) {
    if ((i % 2) == 0) {
      write_bit(TRUE);
    }
    else {
      write_bit(FALSE);
    }
      }
      else {
        write_bit(FALSE);
      }
    } 
  }

  /**
   *  Reads out 16 bits
   */
  uint16_t adc_read() {
    uint16_t result = 0;
    uint16_t tresult = 0;
    char i;
    
    TOSH_wait_250ns();                  //finish the low edge wait
    for (i = 0; i < 16; i++) {
      tresult = (uint16_t)read_bit();
      tresult = tresult << (15-i);
      result += tresult;
    }
    return result;      
  }

  /**
   *  Grab the calibration word from the sensor
   */
  uint16_t spi_word(char num) {
    int i;
    TOSH_wait_250ns(); TOSH_wait_250ns();  //Wait
    
    // write first byte
    for (i = 0; i < 3; i++) {
      write_bit(TRUE);      //Write Start Seq
    }
    write_bit(FALSE);       //Write Lo
    write_bit(TRUE);        //Write Hi
    if (num == 1) {        //W1
      write_bit(FALSE);
      write_bit(TRUE);
      write_bit(FALSE);
      write_bit(TRUE);
    }
    else if (num == 2) {   //W2         
      write_bit(FALSE);
      write_bit(TRUE);
      write_bit(TRUE);
      write_bit(FALSE);
    }
    else if (num == 3) {   //W3
      write_bit(TRUE);
      write_bit(FALSE);
      write_bit(FALSE);
      write_bit(TRUE);
    }
    else if (num == 4) {   //W4
      write_bit(TRUE);
      write_bit(FALSE);
      write_bit(TRUE);
      write_bit(FALSE);
    }
    for (i = 0; i < 4; i++) 
      write_bit(FALSE);    //Write Stop Seq + 1 extra clk pulses
      
    TOSH_wait_250ns();     //do half the wait for the next clk cycle here
    
    return adc_read();     
  }
  
  /**
   *  Grab the temp/pressure data from sensor
   */
  void sense() {
    int i;
    bool l_sensor;
    
    TOSH_wait_250ns(); TOSH_wait_250ns();  //Wait
    
    // write first byte
    for (i = 0; i < 3; i++) {     //Start Seq
      write_bit(TRUE);
    }
    atomic l_sensor = sensor;
    if (l_sensor == PRESSURE) {   //read pressure
      write_bit(TRUE);
      write_bit(FALSE);
      write_bit(TRUE);
      write_bit(FALSE);
    }
    else if (l_sensor == TEMP) {  //read temp
      write_bit(TRUE);
      write_bit(FALSE);
      write_bit(FALSE);
      write_bit(TRUE);
    }
    for (i = 0; i < 5; i++) {     //Stop Seq + 2 extra clk pulses
      write_bit(FALSE);
    }

    timeout = 0;
    call Timer.start(TIMER_ONE_SHOT, 36);
    
  }

/* === Tasks =============================================================== */

    task void signalPressError() {
        signal PressError.error(errornum);
    }

    task void signalTempError() {
        signal TempError.error(errornum);
    }
  
  
    /**
     *  Handle SPI tasks.  Either grab cal values from sensor
     *  or initiate a sensor read.
     */
    task void SPITask() {
        char i, l_state, l_sensor;

        atomic {
            l_state = state;
            l_sensor = sensor;
        }

        if (l_state == RESET) {

            // if calibration is on, grab the calibration data
            if (l_sensor == CALIBRATE) {

                for (i = 0; i < 4; i++) {
                  // reset the device
                  spi_reset();
                  calibration[(int)i] = spi_word(i+1);
                }

                // we're done, so we can be idle
                atomic state = IDLE;

                // send the calibration data up to the application
                for (i = 0; i < 4; i++)
                    signal Calibration.dataReady(i+1, calibration[(int)i]);
                
                return;
                
            } else {
                
                //NOTE:
                //We always rests the device before we sense!
                
                // reset the device
                spi_reset();

                // grab the sensor reading and store it locally
                sense();
            }
        }
        
        //NOTE:
        //Only works in RESET STATE
        
    }
    
  
    /**
     *  Grab the data from sensor and signal the upper layer with
     *  data
     */  
    task void gotInterrupt() {
        uint16_t l_reading;
        uint8_t l_sensor;

        l_reading = adc_read();

        atomic {
            l_sensor = sensor;
            state = IDLE;
        }

        // give the application the sensor data
        if (l_sensor == TEMP) {
            signal Temp.dataReady(l_reading);
        } else if (l_sensor == PRESSURE) {
            signal Pressure.dataReady(l_reading);
        }
    }
  

  
/* === StdControl ========================================================== */

    command result_t StdControl.init() {
        atomic {
            state = IDLE;
            presserror = temperror = FALSE;
        }
        call TimerControl.init();
        return SUCCESS;
    }
    
    /**
     *
     */
    void activePinState() {
        PRESSURE_MAKE_CLOCK_OUTPUT();
        PRESSURE_MAKE_IN_INPUT();
        PRESSURE_MAKE_OUT_OUTPUT();
        
        //TODO:
        //verify these initial pin states are valid !
        
        PRESSURE_CLEAR_CLOCK();
        PRESSURE_SET_IN_PIN();  //why the weak pull up?
        PRESSURE_CLEAR_OUT_PIN();
    }

    command result_t StdControl.start() {
        
        activePinState();
        
        //TODO: !!!
        //WE WILL DISABLE THE UART ONE AT A HIGHER LEVEL 
        //UART1_DISABLE();   //outp(0 ,UCSR1B);

        
        return SUCCESS;
    }
    
    /**
     *
     */
    void powerDownPinState() {
        
        //TODO:
        //What are these states????
        
    }

    command result_t StdControl.stop() {
        
        powerDownPinState();
        
        return SUCCESS;
    }

/* === Calibration ========================================================= */

  /**
   *  Grab Cal data from the sensor
   */
  command result_t Calibration.getData() {
    char l_state;
    
    atomic l_state = state;
    
    if (l_state == IDLE) {
        atomic {
            state = RESET;
            sensor = CALIBRATE;
        }
        post SPITask();
        return SUCCESS;
    }
    return FAIL;
  }

/* === ADC ================================================================= */
  

    /**
     *  Grab the raw pressure data
     */
    async command result_t Pressure.getData() {
        char l_state;
        
        atomic l_state = state;
        
        if (l_state == IDLE) {
            atomic {
                state = RESET;
                sensor = PRESSURE;
            }
            post SPITask();
            return SUCCESS;
        }
        
        return FAIL;
    }

    /**
     *  Grab the raw temp data
     */
    async command result_t Temp.getData() {
        char l_state;

        atomic l_state = state;
        
        if (l_state == IDLE) {
            atomic {
                state = RESET;
                sensor = TEMP;
            }
            post SPITask();
            return SUCCESS;
        }
        return FAIL;
    }

    // not implemented 
    async command result_t Pressure.getContinuousData() { return FAIL; }
    async command result_t Temp.getContinuousData() { return FAIL; }
  
/* === Timer Fired ========================================================= */

    //TODO:
    //Error interface is not being wiried to by the upper layers

    /**
     *
     */
    event result_t Timer.fired() {
        char l_sensor;
        bool l_presserror,l_temperror;

        atomic {
            l_sensor = sensor;
            l_presserror = presserror;
            l_temperror = temperror;
        }
        
        if (PRESSURE_READ_IN_PIN() == 1) {
            timeout++;
            if (timeout > PRESSURE_TIMEOUT_TRIES) {
                if ((l_sensor == PRESSURE) && (l_presserror == TRUE)) {
                    atomic errornum = 1;
                    call Timer.stop();
                    atomic state = IDLE;
                    post signalPressError();
                    return SUCCESS; 
                }
                else if ((l_sensor == TEMP) && (l_temperror == TRUE)) {
                    atomic errornum = 1;
                    call Timer.stop();
                    atomic state = IDLE;
                    post signalTempError();
                    return SUCCESS; 
                }
            }
            call Timer.start(TIMER_ONE_SHOT, 20);
        }
        else {
            call Timer.stop();
            post gotInterrupt();
        }
        
        return SUCCESS;
    }

/* === ADC Error =========================================================== */

  


  command result_t PressError.enable() {
    bool l_presserror;
    atomic l_presserror = presserror;
    
    if (l_presserror == FALSE) {
      atomic presserror = TRUE;
      return SUCCESS;
    }
    return FAIL;
  }

  command result_t PressError.disable() {
    bool l_presserror;
    atomic l_presserror = presserror;
    
    if (l_presserror == TRUE) {
      atomic presserror = FALSE;
      return SUCCESS;
    }
    return FAIL;
  }

  command result_t TempError.enable() {
    bool l_temperror;
    atomic l_temperror = temperror;
    
    if (l_temperror == FALSE) {
      atomic temperror = TRUE;
      return SUCCESS;
    }
    return FAIL;
  }

  command result_t TempError.disable() {
    bool l_temperror;
    atomic l_temperror = temperror;
    
    if (l_temperror == TRUE) {
      atomic temperror = FALSE;
      return SUCCESS;
    }
    return FAIL;
  }

/* === Default Events ====================================================== */
  
  default event result_t PressError.error(uint8_t token) { return SUCCESS; }

  default event result_t TempError.error(uint8_t token) { return SUCCESS; }

  default async event result_t Pressure.dataReady(uint16_t data)
  {
    return SUCCESS;
  }

  default async event result_t Temp.dataReady(uint16_t data)
  {
    return SUCCESS;
  }

  default event result_t Calibration.dataReady(char word, uint16_t value)
  {
    return SUCCESS;
  }

}


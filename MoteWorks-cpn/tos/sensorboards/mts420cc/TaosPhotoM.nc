/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: TaosPhotoM.nc,v 1.1.2.4 2007/12/03 22:28:11 xyang Exp $
 */

/*
 *
 * Authors:     Joe Polastre
 * rewrote to fix major logic flaws 5/16/07 M. Grimmer
 *
 */

includes sensorboard;

module TaosPhotoM {
    provides {
        interface SplitControl;
        interface ADC[uint8_t id];
    }
    
    uses {
        interface StdControl as SwitchControl;
        interface StdControl as TimerControl;   
        interface StdControl as I2CPacketControl;
        interface Switch;
        interface I2CPacket;
        interface Timer;
        interface Leds;
    }
}

implementation {

/* === Frame States ======================================================== */

    enum {
        TAOS_OFF,
        TAOS_POWER_SWITCH_WAIT_ON,       //power switch busy wait for on
        TAOS_POWER_SWITCH_WAIT_OFF,      //power switch busy wait for off
        TAOS_WARMUP,                     //checking if Taos online
        TAOS_READY,                      //sensor ready
        TAOS_READ_0,
        TAOS_READ_1
    };

    uint8_t state;

    //Odd but necessary.    
    char tempvalue;
  
/* === Tasks =============================================================== */

///SPLIT CONTROL
  
  task void signalInitDone() {
      signal SplitControl.initDone();
  }
  
  //NOTE:
  //Not actually posted
  task void signalStartDone() {
      atomic state = TAOS_READY;
      signal SplitControl.startDone();
  }
  
  //NOTE:
  //Not actually posted
  task void signalStopDone() {
      atomic state = TAOS_OFF;
      signal SplitControl.stopDone();
  }
  
///I2C Read/Write
  
  task void writeI2C() {
       uint8_t l_state;
        
       atomic l_state = state;
       if (l_state == TAOS_READ_0) {
           
           atomic tempvalue = 0x43;
           if (call I2CPacket.writePacket(1,(char*)&tempvalue,0x01) == FAIL) {
               
               //TODO:
               //Need an error interface
               
               atomic state = TAOS_READY;
               signal ADC.dataReady[0](0);
           }
           
       } else if (l_state == TAOS_READ_1) {
           
           atomic tempvalue = 0x83;
           if (call I2CPacket.writePacket(1,(char*)&tempvalue,0x01) == FAIL) {
               
               //TODO:
               //Need an error interface
               
               atomic state = TAOS_READY;
               signal ADC.dataReady[1](0);
           }
       }
  }
     

/* === SplitControl ======================================================== */


    //TODO:
    //Need to check return stateuments!!
    
    //NOTE:
    //Will there be an issue with starting both the I2C and Switch?

    /**
     *
     */
    command result_t SplitControl.init() {
        call TimerControl.init();
        call I2CPacketControl.init();
        call SwitchControl.init();
        
        if (post signalInitDone() == FAIL) return FAIL;
        
        atomic state = TAOS_OFF;
        return SUCCESS;
    }

    /**
     *
     */
    command result_t SplitControl.start() {  
        
        call TimerControl.start();
        call I2CPacketControl.start();
        call SwitchControl.start();
        
        if (call Switch.set(MICAWB_LIGHT_POWER,1) == FAIL) return FAIL;
        
        atomic state = TAOS_POWER_SWITCH_WAIT_ON;
        return SUCCESS;
    }

    /**
     *
     */
    command result_t SplitControl.stop() {
        
        if (call Switch.set(MICAWB_LIGHT_POWER,0) == FAIL) return FAIL;
        
        atomic state = TAOS_POWER_SWITCH_WAIT_OFF;
        return SUCCESS;
    }

/* === SplitControl ======================================================== */

    /**
     *
     */
    async command result_t ADC.getData[uint8_t id]() {  
        uint8_t l_state;
        
        atomic l_state = state;
        if (l_state != TAOS_READY) return FAIL;     //sensor not ready
        if (id > 1) return FAIL;                    //sensor not exist
        
        if (id == 0) {        
            if (post writeI2C() == FAIL) return FAIL;
            atomic state = TAOS_READ_0;
            
        } else if (id == 1) {
            if (post writeI2C() == FAIL) return FAIL;
            atomic state = TAOS_READ_1;
            
        }
        
        return SUCCESS;
    }

    ///Not Implemetned
    async command result_t ADC.getContinuousData[uint8_t id]() { return FAIL; }

/* === Switch ============================================================== */

    //TODO:
    //Check the result of the power switches!!!!

    /**
     *  Power Switch Done
     */
    event result_t Switch.setDone(bool status) {
        uint8_t l_state;
        result_t result;

        atomic l_state = state;
        
        if (l_state == TAOS_POWER_SWITCH_WAIT_OFF) { 

            atomic state = TAOS_OFF;
            signal SplitControl.stopDone();
            
        } else if (l_state == TAOS_POWER_SWITCH_WAIT_ON) {
          
            atomic state = TAOS_WARMUP;
            
            //NOTE:
            //is this wait necessary on power up?
            //TOSH_uwait(1000);
            
            //NOTE:
            //Passsing pointers in this way is bad!
            
            atomic tempvalue = 0x03;
            result = call I2CPacket.writePacket(1,(char*)&tempvalue,0x01);
            
            //Note:
            //What do i do if this breaks?  Nothing, for now just assume it worked.
            if (result == FAIL) {
                atomic state = TAOS_READY;
                signal SplitControl.startDone();
            }
        }
        
        return SUCCESS;
    }

    event result_t Switch.getDone(char value) {
        return SUCCESS;
    }

    event result_t Switch.setAllDone(bool result) {
        return SUCCESS;
    }
    
/* === I2C ================================================================= */
    
    /**
     *
     */
    event result_t I2CPacket.writePacketDone(bool result) {
        uint8_t l_state;

        atomic l_state = state; 

        if (l_state == TAOS_WARMUP) {
            
            atomic state = TAOS_READY;
            signal SplitControl.startDone();
            
        } else if (l_state == TAOS_READ_0) {
            call Timer.start(TIMER_ONE_SHOT, 900);
            
        } else if (l_state == TAOS_READ_1) {
            call Timer.start(TIMER_ONE_SHOT, 900);
            
        } else {
            //bad state
        }
        
        return FAIL;
        
        //TODO:
        //Handle timer start failures
        
        //TODO:
        //Handle old values, should check what its returning and see that 
        //value is valid
    }


    /**
     *
     */
    event result_t I2CPacket.readPacketDone(char length, char* data) {
        
        uint8_t l_state;
        
        atomic {
            l_state = state;
            state = TAOS_READY;
        }
        
        if (l_state == TAOS_READ_0) {
            signal ADC.dataReady[0](data[0]);
            
        } else if (l_state == TAOS_READ_1) {
            signal ADC.dataReady[1](data[0]);
            
        } else {
            //didn't request this
        }
        
        return SUCCESS;
    }
  
/* === Timer Fires ========================================================= */

    /**
     *
     */
    event result_t Timer.fired() {
        uint8_t l_state;

        atomic l_state = state;

        if (l_state == TAOS_READ_0) {
            call I2CPacket.readPacket(1,0x01);
        } else if (l_state == TAOS_READ_1) {
            call I2CPacket.readPacket(1,0x01);
        }

        return SUCCESS;
    }
  
/* === Default Events ====================================================== */

    /**
     *
     */
    default async event result_t ADC.dataReady[uint8_t id](uint16_t data) {
        return SUCCESS;
    }

}


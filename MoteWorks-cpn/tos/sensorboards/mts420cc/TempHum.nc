/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: TempHum.nc,v 1.1.2.4 2007/12/03 22:28:11 xyang Exp $
 */

/*
 *
 * Authors:     Mohammad Rahmim, Joe Polastre
 *  rewritten by M. Grimmer to reflect how the hardware actually works 5/15/07
 */

configuration TempHum {
  provides {
    interface SplitControl;
    interface ADC as TempSensor;
    interface ADC as HumSensor;
  }
}

implementation {
  components TempHumM, TimerC, LedsC;
  
  SplitControl =  TempHumM.SplitControl;
  TempSensor = TempHumM.TempSensor;
  HumSensor = TempHumM.HumSensor;

  TempHumM.TimerControl -> TimerC;
  TempHumM.Timer -> TimerC.Timer[unique("Timer")];
  TempHumM.Leds -> LedsC;
}


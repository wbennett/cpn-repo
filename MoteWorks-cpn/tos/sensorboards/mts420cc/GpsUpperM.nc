
//includes gps;

module GpsUpperM {
    provides {
        interface SplitControl as GpsSplitControl;
        interface ReceiveMsg as GpsRecv;
        interface ReceiveMsg as GpsGGARecv;
		interface ReceiveMsg as GpsRMCRecv;
        interface EnableControl as PowerOffOnStop;
    }
    
    uses {
        interface GpsCmd;
        interface StdControl as GpsRawControl;
        interface ReceiveMsg as GpsRawRecv;
        //interface Timer as GpsTimer;  //necessary?
        interface Leds;
    }
}

implementation {

/* === Preprocessor ======================================================== */
  
    #include "gps.h"
    #include "uartMacros.h"
    
    //#define GPS_POWER_OFF_ON_STOP
    
    //Big TODO: !!!!!
    //Sometime the lower module will not signal a receive
    //This means that i have to power cycle this module and have a timer
    //which times out this module !!!! - but this won't be easy b/c 
    //there is not error interface for this.
    
/* === Frame States ======================================================== */

    enum {
        GPS_STATE_OFF,
        
        GPS_STATE_START,
        GPS_STATE_ACTIVE,
        GPS_STATE_STOP,
    };

    uint8_t gState = GPS_STATE_OFF;

    bool bPowerOffOnStop = TRUE;
	
	raw_storage_buffer	rawBuffer;
	NMEAMsg				genericMsgBuffer;
    
/* === Function Params ===================================================== */

    inline void parse_gga_message(GPS_MsgPtr gps_data);
	inline void parse_rmc_message(GPS_MsgPtr gps_data);
    inline void fillGGAMsg(GGAMsg * GGAPtr);
	inline void fillRMCMsg(RMCMsg * RMCPtr);
    
/* === Tasks =============================================================== */

    task void signalInitDone() {
        signal GpsSplitControl.initDone();
    }
    
    task void signalStartDone() {
        signal GpsSplitControl.startDone();
    }
    
    task void signalStopDone() {
        signal GpsSplitControl.stopDone();
    }
    

/* === SplitControl ======================================================== */

    /**
     *
     */
    inline void initState() {
    }

    /**
     *
     */
    command result_t GpsSplitControl.init() {
        result_t result;
        
        initState();
        result = call GpsRawControl.init();
        if (result == SUCCESS) {
            if (post signalInitDone() == FAIL) signal GpsSplitControl.initDone();
        } else {
            return FAIL;
        }
        
    }
    
    /**
     *
     */
    command result_t GpsSplitControl.start() {
        result_t result;
        
        //can't start if not already off
        if (gState != GPS_STATE_OFF) return FAIL;
                
        result = call GpsCmd.PowerSwitch(1);
        if (result == SUCCESS) gState = GPS_STATE_START;
        
        return result;
    }
    
    /**
     *
     */
    command result_t GpsSplitControl.stop() {
        result_t ok1,ok2,okAll;
        
        //NOTE:
        //For now, i will not stop the Power Pins, only i/o pins.
        //This way we should not need to reacquire.  However, will we screw up
        //the incoming packets?
        
        ok1 = call GpsRawControl.stop();      //stop the uart stack
        ok2 = call GpsCmd.TxRxSwitch(0);      //disconnect i/o pins
		call GpsCmd.PowerSwitch(0);
        
        okAll = rcombine(ok1,ok2);
        if (okAll == SUCCESS) gState = GPS_STATE_STOP;
        
        return okAll;
    }
    
/* === PowerOffOnStop ====================================================== */

    /**
     *
     */
    command result_t PowerOffOnStop.enable() {
        bPowerOffOnStop = TRUE;
        return SUCCESS;
    }
    
    /**
     *
     */
    command result_t PowerOffOnStop.disable() {
        bPowerOffOnStop = FALSE;
        return SUCCESS;
    }
    
    /**
     *
     */
    command bool PowerOffOnStop.isEnabled() {
        return bPowerOffOnStop;
    }
/* === GPS Command ========================================================= */

    /**
     *
     */
    event result_t GpsCmd.PowerSet(uint8_t PowerState) {
        
        //TODO:
        //We ought to check the power state        
        
        if (gState == GPS_STATE_START) {
            
            //NOTE:
            //If this returns fail, then we are hosed!!!
            
            //NOTE:
            //For some reason the previous driver added a 5 sec wait between
            //power up and I/O line activation?  We do not do so.
            
            //start I/O lines
            call GpsCmd.TxRxSwitch(1);
            
        } else if (gState == GPS_STATE_STOP) {
            //NOTE:
            //Start and stopping the module has different order of start/stop
            //i/o and power lines.  Fors start, start the power, then the io lines.
            //For stop, chop the io lines then the power.
            //CHH //if (bPowerOffOnStop) {
                gState = GPS_STATE_OFF;
                if (post signalStopDone() == FAIL) signal GpsSplitControl.stopDone();
            //}
            
        } else {
            //SYSTEM FAILURE
        }
        
        return SUCCESS;
    }

    /**
     *
     */
    event result_t GpsCmd.TxRxSet(uint8_t rtstate) {
        
        if (gState == GPS_STATE_START) {
            
            //NOTE:
            //If this call fails we are hosed
            call GpsRawControl.start();  //starts up the uart stack
            gState = GPS_STATE_ACTIVE;
            if (post signalStartDone() == FAIL) signal GpsSplitControl.startDone();
            
        } else if (gState == GPS_STATE_STOP) {
            
			/* CHH */
            //if (bPowerOffOnStop) {
                //NOTE:
                //If this call fails we are hosed
                call GpsCmd.PowerSwitch(0);
            //} else {
            //    gState = GPS_STATE_OFF;
            //    if (post signalStopDone() == FAIL) signal GpsSplitControl.stopDone();
            //}
            
        } else {
            //SYSTEM FAILURE
        }
        return SUCCESS;
    }

/* === GPS Raw Recv ======================================================== */

    /**
     *
     */
    event TOS_MsgPtr GpsRawRecv.receive(TOS_MsgPtr msgPtr) {
        
        //SODbg(SO_DEBUG_XMTS400_GPS_TEST, (uint8_t *) "    GPS_UPPER: RECV\n");
        
        //NOTE:
        //signal the raw receive.  
        //This ptr is not a TOS_Msg, it is really a GPS_MSG!!
        signal GpsRecv.receive(msgPtr);
        //SODbg(SO_DEBUG_XMTS400_GPS_TEST, (uint8_t *) "      GPS_UPPER: %i, %s", ((GPS_MsgPtr)msgPtr)->length, ((GPS_MsgPtr)msgPtr)->data);
        
        //SODbg(SO_DEBUG_XMTS400_GPS_TEST, (uint8_t *) "      GPS_UPPER: Check String\n");
        
        //NOTE:
        //process the packet into a gga pakcet then send it up.  Even though
        //the interface param is a TOS_Msg it is really a GGAMsg
        if (is_gga_string_m( ((GPS_MsgPtr)msgPtr)->data) ) {
            
            //SODbg(SO_DEBUG_XMTS400_GPS_TEST, (uint8_t *) "      GPS_UPPER: Parse Msg\n");
            parse_gga_message( (GPS_MsgPtr) msgPtr);
            //SODbg(SO_DEBUG_XMTS400_GPS_TEST, (uint8_t *) "      GPS_UPPER: Fill Msg\n");
            fillGGAMsg(&genericMsgBuffer.GGAMsgBuffer);
            //SODbg(SO_DEBUG_XMTS400_GPS_TEST, (uint8_t *) "      GPS_UPPER: Signal Recv\n");
            signal GpsGGARecv.receive( (TOS_MsgPtr)&genericMsgBuffer.GGAMsgBuffer);
        }
		else if(is_rmc_string_m( ((GPS_MsgPtr)msgPtr)->data) ) { 
			parse_rmc_message( (GPS_MsgPtr)msgPtr);
			fillRMCMsg(&genericMsgBuffer.RMCMsgBuffer);
			signal GpsRMCRecv.receive( (TOS_MsgPtr)&genericMsgBuffer.RMCMsgBuffer);
		}
		
        //SODbg(SO_DEBUG_XMTS400_GPS_TEST, (uint8_t *) "      GPS_UPPER: Check String Done\n");
        
        return msgPtr;
    }
    
    /**
     *  Parse the raw GPS GGA Packet into field, we stop at Satellites used
     *  
     */
    inline void parse_gga_message(GPS_MsgPtr gps_data) {
        uint8_t i,j,k;
        bool end_of_field;
        uint8_t length;

        // parse comma delimited fields to gga_filed[][]
        end_of_field = FALSE;
        i=0;
        k=0;
        length = gps_data->length;
        memset( rawBuffer.parsed_gga, '0', GGA_FIELDS*GPS_CHAR_PER_FIELD);
        
        while (i < GGA_FIELDS)
        {
          // assemble gga_fields array
          end_of_field = FALSE;
          j = 0;
          while ((!end_of_field) && ( k < length))
          {
            if (gps_data->data[k] == GPS_DELIMITER)
              {
                end_of_field = TRUE;
              }
              else
              {
                if (j<GPS_CHAR_PER_FIELD) {
                    rawBuffer.parsed_gga[i][j] = gps_data->data[k];
                }
              }
            j++;
            k++;
          }
          i++;
        }
        
        return;
    }
	
    inline void parse_rmc_message(GPS_MsgPtr gps_data) {
        uint8_t i,j,k;
        bool end_of_field;
        uint8_t length;

        // parse comma delimited fields to rmc_field[][]
        end_of_field = FALSE;
        i=0;
        k=0;
        length = gps_data->length;
        memset( rawBuffer.parsed_rmc, '0', RMC_FIELDS*GPS_CHAR_PER_FIELD);
        
        while (i < RMC_FIELDS)
        {
          // assemble rmc_fields array
          end_of_field = FALSE;
          j = 0;
          while ((!end_of_field) && ( k < length))
          {
            if (gps_data->data[k] == GPS_DELIMITER)
              {
                end_of_field = TRUE;
              }
              else
              {
                if (j<GPS_CHAR_PER_FIELD) {
                    rawBuffer.parsed_rmc[i][j] = gps_data->data[k];
                }
              }
            j++;
            k++;
          }
          i++;
        }
        
        return;
    }
    
    /**
     *
     */
    inline void fillGGAMsg(GGAMsg * GGAPtr) {
        char * pdata;
        uint8_t NS,EW;
		uint8_t alt_extract;
		
        //NOTE:
        //These ascii to decimal extraction macros are suspect.
        //Need to see if they match raw values.
        
        //NOTE:
        //These data structures do not hold the alititude information
        
        //Extract Time
        pdata=rawBuffer.parsed_gga[1];
        GGAPtr->hours = extract_hours_m(pdata);       
        GGAPtr->minutes = extract_minutes_m(pdata);        
        GGAPtr->dec_sec = (uint32_t)(1000*extract_dec_sec_m(pdata));  //milisec
        
        //Extract Latitude
        pdata=rawBuffer.parsed_gga[2];
        GGAPtr->Lat_deg = extract_Lat_deg_m(pdata);
        GGAPtr->Lat_dec_min = (uint32_t)(10000*extract_Lat_dec_min_m(pdata));  //mili min?
        
        //Extract Longitude
        pdata = rawBuffer.parsed_gga[4];
        GGAPtr->Long_deg = extract_Long_deg_m(pdata);
        GGAPtr->Long_dec_min =(uint32_t)(10000*extract_Long_dec_min_m(pdata));  //mili min?

        //Extra N,S,W,E inidicators for Long + Lat
        NS = (rawBuffer.parsed_gga[3][0] == 'N') ? 1 : 0;
        EW = (rawBuffer.parsed_gga[5][0] == 'W') ? 1 : 0;
        GGAPtr->NSEWind = EW | (NS<<4); // eg. Status = 000N000E = 00010000
        
        //Extra GPS Lock Valid
        GGAPtr->valid = (uint8_t)(rawBuffer.parsed_gga[6][0]-'0');

		//Number of tracked satellites
		pdata = rawBuffer.parsed_gga[7];
		GGAPtr->num_sats = extract_num_sats_m(pdata);
		
		//Altitude
		GGAPtr->altitude = 0;
		for( alt_extract = 0; alt_extract < GPS_CHAR_PER_FIELD && rawBuffer.parsed_gga[9][alt_extract] != '.'; alt_extract++ )
			{
			GGAPtr->altitude = GGAPtr->altitude * 10;
			GGAPtr->altitude = GGAPtr->altitude + ( rawBuffer.parsed_gga[9][alt_extract] - '0' );
			}
    }
	
    inline void fillRMCMsg(RMCMsg * RMCPtr)
		{
		uint8_t i;
	
		if( rawBuffer.parsed_rmc[2][0] == 'A' )
			{
			RMCPtr->valid = TRUE;
			}
		else
			{
			RMCPtr->valid = FALSE;
			}
			
		RMCPtr->sog = 0;
		for( i = 0; i < GPS_CHAR_PER_FIELD && rawBuffer.parsed_rmc[7][i] != '.'; i++ )
			{
			RMCPtr->sog = RMCPtr->sog * 10;
			RMCPtr->sog = RMCPtr->sog + ( rawBuffer.parsed_rmc[7][i] - '0' );
			}
			
		RMCPtr->cog = 0;
		for( i = 0; i < GPS_CHAR_PER_FIELD && rawBuffer.parsed_rmc[8][i] != '.'; i++ )
			{
			RMCPtr->cog = RMCPtr->cog * 10;
			RMCPtr->cog = RMCPtr->cog + ( rawBuffer.parsed_rmc[8][i] - '0' );
			}

		RMCPtr->date = 0;
		for( i = 0; i < GPS_CHAR_PER_FIELD && rawBuffer.parsed_rmc[9][i] != '.'; i++ )
			{
			RMCPtr->date = RMCPtr->date * 10;
			RMCPtr->date = RMCPtr->date + ( rawBuffer.parsed_rmc[9][i] - '0' );
			}
		}
	
/* === Timer Fire ========================================================== */


//     //TODO:
//     //GPS TimeOut not implemented!!!

//     /**
//      *
//      */
//     event result_t GpsTimer.fired() {
//         return SUCCESS;
//     }
    
/* === Default ============================================================= */

    default event TOS_MsgPtr GpsRecv.receive(TOS_MsgPtr msgPtr) {
        return msgPtr;
    }
    
    default event TOS_MsgPtr GpsGGARecv.receive(TOS_MsgPtr msgPtr) {
        return msgPtr;
    }
    
}

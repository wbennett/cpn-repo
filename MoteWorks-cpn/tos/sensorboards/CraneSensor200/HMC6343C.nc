configuration HMC6343C
{
	provides
	{
		interface HMC6343;
		interface SplitControl;
	}
}
implementation
{
	components HMC6343M, TimerC, HighLevelTwi;

	SplitControl = HMC6343M.SplitControl;
	HMC6343 = HMC6343M.HMC6343;

	HMC6343M.Timer -> TimerC.Timer[unique("Timer")];
	HMC6343M.HighLevelTwiInterface -> HighLevelTwi.HighLevelTwiInterface;

}

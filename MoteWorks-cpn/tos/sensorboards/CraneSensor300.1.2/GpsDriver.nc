/*
 *
 *
 *
 */
#include "debug_constants.h"
configuration GpsDriver
{
  provides
	{
		interface SplitControl as GpsSplitControl;
		interface ReceiveMsg as GpsGGARecv;
		interface ReceiveMsg as GpsRMCRecv;
	}
}
implementation
{
  components GpsDriverM, UARTGpsPacket;
  
  GpsSplitControl = GpsDriverM.GpsSplitControl;  
	GpsGGARecv = GpsDriverM.GpsGGARecv;
	GpsRMCRecv = GpsDriverM.GpsRMCRecv;
  
  GpsDriverM.GpsControl -> UARTGpsPacket.Control;
  GpsDriverM.SendVarLenPacket -> UARTGpsPacket.SendVarLenPacket;
  GpsDriverM.Receive -> UARTGpsPacket.Receive;
	#ifdef I2CDBG
	components Main;
	I2CDBG_WIRING(Main,GpsDriverM);
	#endif

	#ifdef USERADIODBG
	RADIODBG_WIRING(GpsDriverM);
	#endif

	components LedsC;
	GpsDriverM.Leds -> LedsC.Leds;
}


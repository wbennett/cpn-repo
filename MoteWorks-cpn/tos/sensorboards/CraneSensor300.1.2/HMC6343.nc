interface HMC6343
{
	//commands
	
	//retrieve 6bytes of data from the corresponding sensors...
	//stored into temp structure....which can be then be processed
	command result_t postAccelData();
	//command result_t postMagData();
	command result_t postHeadingData();
	command result_t postTiltData();

	command result_t enterUserCal();
	command result_t exitUserCal();

	//command result_t orientLevel();
	//command result_t orientSideWays();
	//command result_t orientFlatFront();

	command result_t reset();

	//command result_t postOpMode();

	command result_t StandToRun();
	command result_t RunToStand();
	command result_t RunToSleep();
	command result_t SleepToStand();

	//command result_t eepromRead(uint8_t addr);
	//command result_t eepromWrite(uint8_t addr, uint8_t data);
	
	//events
	event void postAccelDataDone(result_t result, uint8_t* data);
	//event void postMagDataDone(result_t result, uint8_t* data);
	event void postHeadingDataDone(result_t result, uint8_t* data);
	event void postTiltDataDone(result_t result, uint8_t* data);

	
	event void enterUserCalDone(result_t result);
	event void exitUserCalDone(result_t result);

	//event void orientLevelDone(result_t result);
	//event void orientSideWaysDone(result_t result);
	//event void orientFlatFrontDone(result_t result);

	event void resetDone(result_t result);

	//event void postOpModeDone(result_t result, uint8_t data);

	event void StandToRunDone(result_t result);
	event void RunToStandDone(result_t result);
	event void RunToSleepDone(result_t result);
	event void SleepToStandDone(result_t result);

	//event void eepromReadDone(result_t result, uint8_t data);
	//event void eepromWriteDone(result_t result);
}

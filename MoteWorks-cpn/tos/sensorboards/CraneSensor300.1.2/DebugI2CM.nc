/*
 *
 *
 *
 //Look in debug_constants.h file for the static function
 *
 */
#include "debug_constants.h"
module DebugI2CM
{
	provides
	{
		interface StdControl;
		command result_t flush();
	}
	uses
	{
		interface HighLevelTwiInterface;
	}
}

implementation
{
	#include "twi.h"
	#define DEBUG_ADDR (42 >> 1)
	void full_event_handler(void) {
		call flush();
	}

	command result_t StdControl.init()
	{
		//attach full event handler
		TOSH_MAKE_PW2_OUTPUT();
		twi_attach_full_event(full_event_handler);
		return SUCCESS;
	}

	command result_t StdControl.start()
	{
		TOSH_SET_PW2_PIN();
		TOSH_uwait(10);
		call HighLevelTwiInterface.begin();
		return SUCCESS;
	}

	command result_t StdControl.stop()
	{
		TOSH_CLR_PW2_PIN();
		return SUCCESS;
	}
	#define MAX_TWI_PRINTF_SIZE (256)
	task void printBufferTask() {
		static uint16_t i;
		//static uint16_t k;
		//static uint16_t max;
		//max = strlen(dbg_buffer);
		/** TWI_BUFFER is only 32 bytes so we have to split printed ***
		*** debug message into 32 byte chunks                       **/
		call HighLevelTwiInterface.beginTransmission(DEBUG_ADDR);
		for(i=0; i<TWI_BUFFER_LENGTH; i++) {
			if(twi_buffer_empty() == TRUE) {
				call HighLevelTwiInterface.endTransmission();
				return;
			}
			else {
				call HighLevelTwiInterface.send(twi_pull_char());
			}
		}
		call HighLevelTwiInterface.endTransmission();
		if(twi_buffer_empty() == FALSE) {
			post printBufferTask();	
		}
	}
	command result_t flush() {
		post printBufferTask();
		return SUCCESS;
	}
}

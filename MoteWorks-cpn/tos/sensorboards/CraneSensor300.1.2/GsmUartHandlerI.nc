/**
 * author: wbennett
 * note: this interface provides the events and commands required to create
 * GsmUartHandler component
 *
 * To see the requirements for this code see the cpn website. http://cse.unl.edu/~cpn
 *
 */

#include "Tracker/gsm.h"
interface GsmUartHandlerI
{
	/**
	 * ---------Commands------
	 *
	 */

	/**
	 * This command will send a command over UART line.
	 * It is the responsibility of the upper layers to make sure the device handles state changes and timeout errors for sending commands.
	 */
	async command result_t sendCommand(char *cmd, uint8_t length);
	/**
	 * ---------Events--------
	 */
	/**
	 * This event will be raised once the GUH realizes an OK was sent over the UART line.
	 * NOTE: It is the responsibility of the upper layers to ensure commands are sent synchronously.
	 */
	async event void OkReceived();

	/**
	 * This event will be raised once the GUH realizes an ERROR was received over the UART line.
	 * If the error includes an error code it will return the error code in the parameter “errorCode”.
	 */
	async event void ErrorReceived(gsm_error_t errorCode);
	
	/**
	 * This event will be raised once the GUH realizes an Network Registration Report was received.
	 * If the Network Registration Report includes data regarding the registration of the device, GUH will store this information in an internal structure. 
	 * This event will return a pointer to the structure and the size of the structure for listening components.
	 */
	async event void NetworkRegistrationReceived(gsm_cellid_areacode_data_t*ptr, uint8_t dataLength);

	/**
	 *This event will be raised once the GUH realizes a Cell Monitor Report was received on the UART line.
		If the Cell Monitor Report includes data regarding the neighboring cellular towers, GUH will store the information in an internal structure.
		This event will return a pointer to the structure and the size of the structure for listening components.i 
	 */
	async event void CellMonitorReportReceived(gsm_cellmon_data_t*ptr,uint16_t dataLength);
	
	/**
	 * This even will be raised once the GUH realizes a prompt(<cr><cl><greater than><space>) was 
	 * received over UART
	 */
	async event void PromptReceived();

	/**
	 * This event will be raised once the GUH successfully sends a command on the UART line.
	 */
	async event result_t SendCommandDone(uint8_t *cmd, result_t result);

	/**
	 * This event will be raised once the GUH successfully reads signal quality on the UART line.
	 */
	async event void SignalQualityReceived(gsm_signal_quality_data_t*ptr,uint8_t dataLength);
}


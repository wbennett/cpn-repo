/**
 * author: wbennett
 * note: this interface defines the functions of the GSM minus the control
 *
 * the GSM will offer the following:
 * 	- attempt to associate
 * 	- offer the cellid and area code
 * 	- offer the signal quality
 *  - attempt to send SMS
 *
 */

#include "Tracker/gsm.h"

interface GSM_ModemI
{
	/**
	 * --------Commands---------
	 */
	command result_t setCellMonitorReportValue(uint8_t value);
	command result_t getCellMonitorReport();
	command result_t getSignalQuality();
	/**
	 * sendSMS will attempt to send a message to the end point
	 * @param: uint8_t *endpoint is a null terminated phone number were the message is directed
	 * @param: uint8_t *data is a null terminated message sent to the endpoint
	 */
	command result_t sendSMS(uint8_t *endpoint, uint8_t *data);

	/**
	 * ---------Events--------
	 */
	 
	event result_t setCellMonitorReportValueDone(gsm_error_t result);

	/**
	 * cellIdAndAreaCodeReady event fires when the getCellidAndAreaCode process is complete.
	 * @param: gsm_error_t result will be NO_ERROR if successful
	 */	
	event result_t cellMonitorReportReady(gsm_error_t result, gsm_cellmon_data_t *data);
	
	/**
	 * signalQualityReady event fires when the getSignalQuality process is complete.
	 * @param: gsm_error_t result will be NO_ERROR if successful
	 */	
	event result_t signalQualityReady(gsm_error_t result, gsm_signal_quality_data_t *data);
	
	/**
	 * sendSMSDone event fires when the sendSMS process is complete.
	 * @param: gsm_error_t result will be NO_ERROR if successful
	 */	
	event result_t sendSMSDone(gsm_error_t result, uint8_t *endpoint, uint8_t *data);

}


configuration HighLevelTwi {
  provides interface HighLevelTwiInterface;
}
implementation {
  components HighLevelTwiM, LowLevelTwiM;
	
	HighLevelTwiInterface = HighLevelTwiM.HighLevelTwiInterface;
  
  HighLevelTwiM.LowLevelTwiInterface -> LowLevelTwiM.LowLevelTwiInterface;
}


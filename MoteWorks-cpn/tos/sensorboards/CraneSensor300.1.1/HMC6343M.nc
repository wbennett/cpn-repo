//3-axis compass HMC6343
/*
 *
 *
 *
 *
 */


#include "debug_constants.h"
module HMC6343M
{
	provides
	{
		interface HMC6343;
		interface SplitControl;
	}
	uses
	{
		interface Timer;
		interface HighLevelTwiInterface;
	}
}

implementation
{
	#include "HMC6343.h"

	#define DBG_VERBOSE (0)

	result_t performCommand();
	task void readDone();
	task void signalStartDone();
	task void signalStopDone();
	task void signalInitDone();
	
	uint8_t cmdBuffer;//[3];
	uint8_t length;
	uint8_t readLength;
	uint8_t readData[6];


	command result_t SplitControl.init()
	{
		#ifndef I2CDBG
		#warning "MAKING OUPUT IN HMCDRIVER"
		TOSH_MAKE_PW2_OUTPUT();
		#endif
		post signalInitDone();
		return SUCCESS;
	}

	command result_t SplitControl.start()
	{
		#ifndef I2CDBG
		#warning "SETTING OUPUT IN HMCDRIVER"
		TOSH_SET_PW2_PIN();
		TOSH_uwait(10);
		#endif
		call HighLevelTwiInterface.begin();
		call Timer.start(TIMER_ONE_SHOT, 500);
		return SUCCESS;
	}

	task void signalStartDone()
	{
		signal SplitControl.startDone();
	}
	
	task void signalStopDone()
	{
		signal SplitControl.stopDone();
	}
	
	task void signalInitDone()
	{
		signal SplitControl.initDone();
	}

	event result_t Timer.fired()
	{
		post signalStartDone();
		return SUCCESS;
	}

	command result_t SplitControl.stop()
	{
		#ifndef I2CDBG
		#warning "SETTING OUPUT IN HMCDRIVER"
		TOSH_CLR_PW2_PIN();
		#endif
		post signalStopDone();
		return SUCCESS;
	}

	result_t performCommand()//(uint8_t Command)
	{
		uint8_t i = 0;
		uint16_t read;
		call HighLevelTwiInterface.beginTransmission(HMC6343_ADDR);
		call HighLevelTwiInterface.send(cmdBuffer);
		call HighLevelTwiInterface.endTransmission();
		
		read = call HighLevelTwiInterface.requestFrom(HMC6343_ADDR,
													readLength);
		while(call HighLevelTwiInterface.available())
		{
			readData[i] = call HighLevelTwiInterface.receive();
			i++;
		}
		while(i < 6)
		{
			readData[i] = 0;
			i++;
		}
		post readDone();
		return SUCCESS;
	}
	
	
	task void readDone()
	{
		switch(cmdBuffer)
		{
			case POST_ACCEL:
				signal HMC6343.postAccelDataDone(SUCCESS, readData);
				break;
			case POST_HEADING:
				signal HMC6343.postHeadingDataDone(SUCCESS, readData);
				break;
			case POST_TILT:
				signal HMC6343.postTiltDataDone(SUCCESS, readData);
				break;
			case RESET_COMPASS:
				signal HMC6343.resetDone(SUCCESS);
				break;
				/*
			case ORIENT_LEVEL:
				signal HMC6343.orientLevelDone(SUCCESS);
				break;
				*/
			case ENTER_USER_CAL:
				signal HMC6343.enterUserCalDone(SUCCESS);
				break;
			case EXIT_USER_CAL:
				signal HMC6343.exitUserCalDone(SUCCESS);
				break;
			case STAND_TO_RUN:
				signal HMC6343.StandToRunDone(SUCCESS);
				break;
			case RUN_TO_STAND:
				signal HMC6343.RunToStandDone(SUCCESS);
				break;
			case RUN_TO_SLEEP:
				signal HMC6343.RunToSleepDone(SUCCESS);
				break;
			case SLEEP_TO_STAND:
				signal HMC6343.SleepToStandDone(SUCCESS);
				break;
		}
	}

	command result_t HMC6343.postAccelData()
	{
		cmdBuffer = POST_ACCEL;
		length = 1;
		readLength = 6;
		return performCommand();
	}

	command result_t HMC6343.postHeadingData()
	{
		cmdBuffer = POST_HEADING;
		length = 1;
		readLength = 6;
		return performCommand();
	}

	command result_t HMC6343.postTiltData()
	{
		cmdBuffer = POST_TILT;
		length = 1;
		readLength = 6;
		return performCommand();
	}

/*
	command result_t HMC6343.orientLevel()
	{
		cmdBuffer = ORIENT_LEVEL;
		length = 1;
		readLength = 0;
		return performCommand();
	}
*/
	command result_t HMC6343.enterUserCal()
	{
		//ENTER_USER_CAL = 0x71,
		cmdBuffer = ENTER_USER_CAL;
		length = 1;
		readLength = 0;
		return performCommand();
	}

	command result_t HMC6343.exitUserCal()
	{
		//EXIT_USER_CAL = 0x7E,
		cmdBuffer = EXIT_USER_CAL;
		length = 1;
		readLength = 0;
		return performCommand();
	}
	
	command result_t HMC6343.reset()
	{
		cmdBuffer = RESET_COMPASS;
		length = 1;
		readLength = 0;
		return performCommand();
	}

	command result_t HMC6343.StandToRun()
	{
		cmdBuffer = STAND_TO_RUN;
		length = 1;
		readLength = 0;
		return performCommand();
	}

	command result_t HMC6343.RunToStand()
	{
		cmdBuffer = RUN_TO_STAND;
		length = 1;
		readLength = 0;
		return performCommand();
	}

	command result_t HMC6343.RunToSleep()
	{
		cmdBuffer = RUN_TO_SLEEP;
		length = 1;
		readLength = 0;
		return performCommand();
	}

	command result_t HMC6343.SleepToStand()
	{
		cmdBuffer = SLEEP_TO_STAND;
		length = 1;
		readLength = 0;
		return performCommand();
	}


}

interface Compass
{
	command void getAccels();
	command void getHeadings();
	command void getTilts();
	command void calibrate();
	event void getAccelsDone(result_t result, uint16_t * data);
	event void getHeadingsDone(result_t result, uint16_t * data);
	event void getTiltsDone(result_t result, uint16_t * data);
	event void calibrateDone(result_t result);

}

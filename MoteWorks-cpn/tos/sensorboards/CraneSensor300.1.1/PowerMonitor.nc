/*
 *
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
#include "adc_mappings.h"
configuration PowerMonitor {
	provides interface StdControl;
	//this needs to fan out because we are going to have many 
	//calling this...
	provides interface PowerSample[uint8_t id];
/*	provides interface ADC as BatteryVoltage;
	provides interface ADC as BatteryCurrent;
	provides interface ADC as SolarVoltage;
	provides interface ADC as SolarCurrent;
	*/
} implementation {
	components PowerMonitorM, ADCC, TimerC;
	#ifdef I2CDBG
	components Main;
	I2CDBG_WIRING(Main,PowerMonitorM);
	#endif
	StdControl = PowerMonitorM;
	PowerSample = PowerMonitorM;
	PowerMonitorM.ADCControl -> ADCC;
	PowerMonitorM.BatteryVoltage -> ADCC.ADC[BATTERY_VOLTAGE_ADC_PORT];
	PowerMonitorM.BatteryCurrent -> ADCC.ADC[BATTERY_CURRENT_ADC_PORT];
	PowerMonitorM.SolarVoltage -> ADCC.ADC[SOLAR_VOLTAGE_ADC_PORT];
	PowerMonitorM.SolarCurrent -> ADCC.ADC[SOLAR_CURRENT_ADC_PORT];
}

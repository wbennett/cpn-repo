#ifndef twi_h
#define twi_h

  #include <inttypes.h>
  
  #include <compat/twi.h>
  
  #include "avrhardware.h"

  #ifndef CPU_FREQ
  #define CPU_FREQ 16000000L
  #endif

  #ifndef TWI_FREQ
  #define TWI_FREQ 100000L
  #endif

  #ifndef TWI_BUFFER_LENGTH
  #define TWI_BUFFER_LENGTH 32
  #endif

  #define TWI_READY 0
  #define TWI_MRX   1
  #define TWI_MTX   2
  #define TWI_SRX   3
  #define TWI_STX   4

#endif


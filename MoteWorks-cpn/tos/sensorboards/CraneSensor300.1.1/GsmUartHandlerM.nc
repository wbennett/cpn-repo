//57600 baud for gps
//115200 baud for iris/sodbg

module GsmUartHandlerM 
{
  provides 
	{
		interface StdControl;
    interface GsmUartHandlerI as GsmUartHandler;
  }
  uses 
	{
    interface StdControl as UARTControl;
		interface SendVarLenPacket as SendVarLenPacketGsm; 
    interface ReceiveMsg as Receive;
		interface Leds;
  }
}
implementation 
{
  //#define DBG_PKT 1
  //#define SO_DEBUG 1
	//#include "SOdebug1.h"
	//SOdebug uses UART0
	//SOdebug1 uses UART1
	//use uart1 because the gsm communicates over uart0
	#include "Tracker/gsm.h"
  #include <stdlib.h>
	#include <string.h>
	#include <stdio.h>
	
	extern int sscanf(const char *__buf, const char *__fmt,...)__attribute__((C));
	
	//============Module variables==========//
	//cyclic buffer for analyzing uart data
	char uartBuffer[4][256];
	//index for current buffer
	uint8_t curBufIndex = 0;
	//current buffer for registration data
	gsm_cellid_areacode_data_t regData;
	//assume that the cell mon is 0
	uint8_t gsmCellMonParamVal = 0;
	uint8_t gsmCellMonCount=0;
	//current buffer for cell monitor data
	gsm_cellmon_data_t cellMonData;
	//current buffer for signal quality
	gsm_signal_quality_data_t sigQualData;
	
	recvQueue_t recvQueue;
	char trimBuf[50];
	char consBuf1[30];
	char consBuf2[30];
	uint16_t ec;
	//======================================//
	
	//=============Prototypes===============//
	task void initTask();
	task void startTask();
	task void stopTask();
	task void handleDataReceived();
	task void checkBufferForValidResponse();
	task void handleSendMoniFail();
	inline void checkForMessages(uint8_t index);
	inline bool checkForOkReceived(uint8_t index);
	inline bool checkForErrorReceived(uint8_t index);
	inline bool checkForNetworkRegistrationReceived(uint8_t index);
	inline bool checkForSignalQualityReceived(uint8_t index);
	inline bool checkForCellMonitorReceived(uint8_t index);
	inline bool checkForPromptReceived(uint8_t index);
	inline uint8_t countFieldsByDelimiter(char*src, const char delim);
	inline result_t enterRecvQueue(uint8_t element);
	inline uint8_t removeRecvQueue();
	inline bool isWhiteSpace(char c);
	inline char* trim(char* s);
	//======================================//
	
	//============Queue methods=============//
	inline result_t enterRecvQueue(uint8_t element)
	{
		if(recvQueue.count >= QUEUE_SIZE)
		{
			return FAIL;
		}
		else
		{
			atomic{
				uint8_t newIndex = (recvQueue.front+recvQueue.count)%QUEUE_SIZE;
				recvQueue.contents[newIndex] = element;
				recvQueue.count++;
				return SUCCESS;
			}
		}
	}
	
	inline uint8_t removeRecvQueue()
	{
		uint8_t r_val;
		atomic{
			r_val = recvQueue.contents[recvQueue.front];
			recvQueue.front = (recvQueue.front+1)%QUEUE_SIZE;
			recvQueue.count--;
		}
		return r_val;
	}
	//======================================//

	//=============message parsing==========//
	
	/**
	 * Comments
	 */
	inline void checkForMessages(uint8_t index)
	{
		if(checkForErrorReceived(index)) {
			//we found an error signal that there was an error found
			//SODbg(1,"error received\n");
			//signal upper layers that we found a error
			//we should use a prpper type...see requirements
			atomic signal GsmUartHandler.ErrorReceived(ec);
		}
		else if(checkForOkReceived(index)) {
			//we found an ok message signal that we found it
			//SODbg(1,"ok received\n");
			atomic signal GsmUartHandler.OkReceived();
		}
		else if(checkForPromptReceived(index)) {
			//we found a prompt message
			//SODbg(1,"prompt received\n");
			atomic signal GsmUartHandler.PromptReceived();
		}
		else if(checkForNetworkRegistrationReceived(index)) {
			//we found a registration received
			//SODbg(1,"network registration received\n");
			atomic signal GsmUartHandler.NetworkRegistrationReceived(&regData,
																		sizeof(regData));
		}
		else if(checkForSignalQualityReceived(index)) {
			//we found a signal quality received
			//SODbg(1,"signal quality received\n");
			atomic signal GsmUartHandler.SignalQualityReceived(&sigQualData,
																		sizeof(sigQualData));
		}
		else if(checkForCellMonitorReceived(index)) {
			//we found a cell monitor received...
			//SODbg(1,"cell monitor received\n");
			//if((gsmCellMonCount >= MAXCELLMONRECORDS)||(gsmCellMonCount >= gsmCellMonParamVal)) {
				gsmCellMonCount = 0;
				atomic signal GsmUartHandler.CellMonitorReportReceived(&cellMonData,
																				sizeof(cellMonData));
			//}
		}
		else{
			//bad state...
			//SODbg(1,"Unknown data in the buffer\n");
		}
	}

	/**
	 * Comments
	 */
	inline bool checkForOkReceived(uint8_t index)
	{
		//current string for clarity...
		char *buf = uartBuffer[index];
		//SODbg(1,"checking :%s\n",buf);
		//SODbg(1,"check for ok received\n");
		//if the buffer contains <CR><LF>OK<CR><LF> it is good
		if(strstr(buf,"OK") == NULL) {
			//SODbg(1,"ok not found\n");
			return FALSE;
		}
		else {
			//SODbg(1,"ok found\n");
			return TRUE;
		}
	}

	/**
	 * Comments
	 */
	inline bool checkForErrorReceived(uint8_t index)
	{
		//current string for clarity...
		char *buf = uartBuffer[index];
		
		char *startPtr = strstr(buf,"ERROR:");
		
		if(startPtr == NULL) {
			//SODbg(1,"error not found\n");
			return FALSE;
		}
		else {
			//SODbg(1,"error found\n");
			startPtr += strlen("ERROR:");
			sscanf(startPtr,"%d",&ec);
			return TRUE;
		}
	}

	/**
	 * Comments
	 */
	inline bool checkForNetworkRegistrationReceived(uint8_t index)
	{
		char *buf = uartBuffer[index];
		
		if(strstr(buf,"+CREG:") == NULL) {
			return FALSE;
		}
		else {
			char *dataSegment;
			char *prog;
			char *tok;
			uint8_t numberOfFields;
			
			uint16_t areacode;
			uint16_t cellid;

			dataSegment = strchr(buf, ':')+1;
			//count the number fields
			numberOfFields = countFieldsByDelimiter(dataSegment,',');
			
			switch(numberOfFields) {
				case 1:
					//first field is mode "uint8_t"
					tok = strtok_r(dataSegment,",",&prog);
					//second field is current status "uint8_t"
					tok = strtok_r(NULL,",",&prog);
					regData.status = atoi(tok);
					regData.areacode = 0;
					regData.cellid = 0;
					break;
				case 2:
					tok = strtok_r(dataSegment,",",&prog);
					//first field is status "uint8_t"
					regData.status = atoi(tok);
					
					tok = strtok_r(NULL,",",&prog);	
					//second field is local area code "2-byte hex"
					sscanf(tok,"%x",&areacode);
					regData.areacode = areacode;
					
					tok = strtok_r(NULL,",",&prog);	
					//third field is cell id "string"
					sscanf(tok,"%x",&cellid);
					regData.cellid = cellid;
					break;
				case 3:
					//first field is mode "uint8_t"
					tok = strtok_r(dataSegment,",",&prog);
					tok = strtok_r(NULL,",",&prog);
					//second field is status "uint8_t"
					regData.status = atoi(tok);
					tok = strtok_r(NULL,",",&prog);
					//third field is area code "string"
					sscanf(tok,"%x",&areacode);
					regData.areacode = areacode;
					tok = strtok_r(NULL,",",&prog);
					//fourth field is cell id "string"
					sscanf(tok,"%x",&cellid);
					regData.cellid = cellid;
					break;
				default:
					regData.status = 0;
					regData.areacode = 0;
					regData.cellid = 0;
					break;
			}
			return TRUE;
		}
	}

	/**
	 * Check for a signal quality being received then it will 
	 * parse the values for the signal being received...
	 */
	inline bool checkForSignalQualityReceived(uint8_t index)
	{
		//current string for clarity...
		char *buf = uartBuffer[index];
		//SODbg(1,"checking :%s\n",buf);
		//SODbg(1,"check for signal quality received\n");
		//if the buffer contains +CSQ: it is good
		if(strstr(buf,"+CSQ:") == NULL) {
			//SODbg(1,"signal quality not found not found\n");
			return FALSE;
		}
		else {
			char *dataSegment;
			char *prog;
			char *tok;

			dataSegment = strtok_r(buf,":",&prog);

			tok = strtok_r(NULL,",",&prog);
			sigQualData.rssi = atoi(tok);
			
			tok = strtok_r(NULL,"\r",&prog);
			sigQualData.bit_err_rate = atoi(tok);
			
			return TRUE;
		}
	}
	
	/**
	 *	This method will count the number of fields between
	 *	the specified delimiter
	 */
	inline uint8_t countFieldsByDelimiter(char*src, const char delim) {
		uint8_t count = 0;
		uint8_t i;
		if(strlen(src) == 0) {
			return 0;
		}
		for(i=0;i<strlen(src)-1;i++) {
			if(src[i] == delim) {
				count++;
			}
		}
		return count;
	}
	
	inline bool isWhiteSpace(char c) {
		bool b = ( (c==' ') || (c=='\t') || (c=='\r') || (c=='\n'));
		return b;
	}
	
	inline char* trim(char* s) {
		uint8_t start = 0;
		uint8_t end = strlen(s)-1;
		uint8_t l;
		while(start<=end && isWhiteSpace(s[start])) {
			start++;
		}
		if(start<=end) {
			while(end && isWhiteSpace(s[end])) {
				end--;
			}
		}
		l = end-start+1;
		if(l<=0) {
			return "";
		}
		strncpy(trimBuf,s+start,l);
		trimBuf[l]='\0';
		return trimBuf;
	}
	
	inline char* consolidate(char* s) {
		char* CcPtr;
		char* NcPtr;
		char* endPtr;
		uint8_t i,k;
		strcpy(consBuf1,s);
		CcPtr = strstr(s,"Cc:");
		if(CcPtr != NULL) {
			NcPtr = strstr(CcPtr,"Nc:");
			if(NcPtr != NULL) {
				CcPtr += strlen("Cc:");
				consBuf1[0]='C';
				strncpy(consBuf1+1,CcPtr,NcPtr-CcPtr);
				consBuf1[NcPtr-CcPtr+1]='\0';
				strcpy(consBuf1,trim(consBuf1));
				strcat(consBuf1,"N");
				NcPtr += strlen("Nc:");
				endPtr = strstr(NcPtr,"BSIC:");
				if(endPtr != NULL) {
					i=strlen(consBuf1);
					strncat(consBuf1,NcPtr,endPtr-NcPtr);
					consBuf1[i+endPtr-NcPtr]='\0';
				}
				else {
					strcat(consBuf1,NcPtr);
				}
			}
		}
		k=0;
		for(i=0; i<=strlen(consBuf1);i++) {
			if( !isWhiteSpace(consBuf1[i]) ) {
				consBuf2[k] = consBuf1[i];
				k++;
			}
		}
		consBuf2[9]='\0';//truncate to size 10
		return consBuf2;
	}
	
	
	/**
	 * This will check for the Cell Monitor Received and then will
	 * parse the information to a local buffer.
	 */
	inline bool checkForCellMonitorReceived(uint8_t index)
	{
		//current string for clarity...
		char *buf = uartBuffer[index];

		if(strstr(buf,"#MONI:") == NULL) {
			return FALSE;
		}
		else {
			 
			if(gsmCellMonParamVal==0)
			{
				char *begPtr;
				char *endPtr;
				char tempString[20];
				char *tempPtr;
				uint16_t bsic;
				uint16_t rxQual;//0-7
				uint16_t lac;//hex
				uint16_t id;//hex
				uint16_t arfcn;//dec
				int16_t dBm;//negative number
				uint16_t timadv;//0-63

				//========Extract netname======//
				begPtr = strchr(buf, ':');
				begPtr++;
				
				endPtr = strstr(begPtr,"BSIC:");
				if(endPtr == NULL) {
					return FALSE;
				}
				strncpy( tempString,begPtr,endPtr-begPtr);
				tempString[endPtr-begPtr]='\0';
				
				tempPtr = trim(tempString);
				
				tempPtr = consolidate(tempPtr);
				
				strcpy(cellMonData.towers[0].netname,tempPtr);
				//=============================//
				
				//======Extract bsic===========//				
				begPtr = endPtr+strlen("BSIC:");
				endPtr = strstr(begPtr,"RxQual:");
				if(endPtr == NULL) {
					return FALSE;
				}
				strncpy(tempString,begPtr,endPtr-begPtr);
				tempString[endPtr-begPtr]='\0';
				
				sscanf(tempString,"%x",&bsic);				
				cellMonData.towers[0].bsic = (uint8_t) bsic;
				//==============================//
				
				//======Extract rxQual===========//				
				begPtr = endPtr+strlen("RxQual:");
				endPtr = strstr(begPtr,"LAC:");
				if(endPtr == NULL) {
					return FALSE;
				}
				strncpy(tempString,begPtr,endPtr-begPtr);
				tempString[endPtr-begPtr]='\0';
				
				sscanf(tempString,"%d",&rxQual);				
				//not supported anymore keep the sscanf for other purposes
				//cellMonData.towers[0].rxQual = (uint8_t) rxQual;
				//==============================//
				
				//======Extract LAC===========//				
				begPtr = endPtr+strlen("LAC:");
				endPtr = strstr(begPtr,"Id:");
				if(endPtr == NULL) {
					return FALSE;
				}
				strncpy(tempString,begPtr,endPtr-begPtr);
				tempString[endPtr-begPtr]='\0';
				
				sscanf(tempString,"%x",&lac);				
				cellMonData.towers[0].lac = lac;
				//==============================//
				
				//======Extract Id===========//				
				begPtr = endPtr+strlen("Id:");
				endPtr = strstr(begPtr,"ARFCN:");
				if(endPtr == NULL) {
					return FALSE;
				}
				strncpy(tempString,begPtr,endPtr-begPtr);
				tempString[endPtr-begPtr]='\0';
				
				sscanf(tempString,"%x",&id);				
				cellMonData.towers[0].id = id;
				//==============================//
				
				//======Extract ARFCN===========//				
				begPtr = endPtr+strlen("ARFCN:");
				endPtr = strstr(begPtr,"PWR:");
				if(endPtr == NULL) {
					return FALSE;
				}
				strncpy(tempString,begPtr,endPtr-begPtr);
				tempString[endPtr-begPtr]='\0';
				
				sscanf(tempString,"%d",&arfcn);				
				cellMonData.towers[0].arfcn = arfcn;
				//==============================//
				
				//======Extract PWR===========//				
				begPtr = endPtr+strlen("PWR:");
				endPtr = strstr(begPtr,"dbm");
				if(endPtr == NULL) {
					return FALSE;
				}
				strncpy(tempString,begPtr,endPtr-begPtr);
				tempString[endPtr-begPtr]='\0';
				
				sscanf(tempString,"%d",&dBm);				
				//this functionality is removed keep the
				//scan to eliminate alternative conditions
				//cellMonData.towers[0].dBm = (uint8_t) dBm;
				//==============================//
				
				//======Extract TA===========//	
				endPtr = strstr(begPtr,"TA:");
				if(endPtr==NULL) {
					return FALSE;
				}
				begPtr = endPtr+strlen("TA:");
				endPtr = begPtr+strlen(begPtr);

				strncpy(tempString,begPtr,endPtr-begPtr);
				tempString[endPtr-begPtr]='\0';
				
				sscanf(tempString,"%d",&timadv);				
				//this functionality is removed keep the scan
				//to eliminate alternative conditions
				//cellMonData.towers[0].timadv = (uint8_t) timadv;
				//==============================//

				//increment index...
				//gsmCellMonCount++;
				return TRUE;
			}
			else {
				return FALSE; //No support for >0 yet
			}
		}
	}

	/**
	 * Comments
	 */
	inline bool checkForPromptReceived(uint8_t index)
	{
		//current string for clarity...
		char *buf = uartBuffer[index];
		//SODbg(1,"checking :%s\n",buf);
		//SODbg(1,"check for prompt received\n");
		//if the buffer contains <CR><CL><greater than><space>: it is good
		if(strstr(buf,"> ") == NULL) {
			//SODbg(1,"prompt not found\n");
			return FALSE;
		}
		else {
			//SODbg(1,"prompt found\n");
			//we need to parse the string for the data values...
			//use strtok_r...
			//TODO
			return TRUE;
		}
	}
	//======================================//


	/**
	 *	Initialize 
	 */
	command result_t StdControl.init()
	{
		//SODbg(1,"StdControl init\n");
		post initTask();
		return SUCCESS;
	}
	
	task void initTask()
	{
		//SODbg(1,"init task\n");
		call Leds.init();
		call UARTControl.init();
	}

	/**
	 * Start procedure of the GSM
	 */
	command result_t StdControl.start()
	{
		//SODbg(1,"StdControl start\n");
		post startTask();
		return SUCCESS;
	}
	
	task void startTask()
	{
		//SODbg(1,"start task\n");
		call UARTControl.start();
	}

	/**
	 * Stop procedure of the GSM
	 */
	command result_t StdControl.stop()
	{
		//SODbg(1,"StdControl stop\n");
		post stopTask();
		return SUCCESS;
	}
	
	task void stopTask()
	{
		//SODbg(1,"stop task\n");
		call UARTControl.stop(); 
	}

	event TOS_MsgPtr Receive.receive(TOS_MsgPtr bufferPtr)
	{
		//we must copy the data to the currentBuffer then start data received task
		//copy the data to the uartBuffer[curBufIndex] pointer
		General_MsgPtr gen = (General_MsgPtr) bufferPtr;
		strcpy((char*)uartBuffer[curBufIndex],(char*)gen->data);
		//post task to handle current data received
		//SODbg(1,"Received:%s",gen->data);
		enterRecvQueue(curBufIndex);
		curBufIndex = (curBufIndex+1)%4;//increment cyclic buffer size buffer
		post handleDataReceived();
		return bufferPtr;	
	}
	
	/**
	 * Comments
	 */
	task void handleDataReceived()
	{
		//SODbg(1,"handle data received\n");
		//check for each of our messages
		atomic{
			uint8_t nextIndex = removeRecvQueue();
			checkForMessages(nextIndex);
		}
	}
	
	/**
	 * Comments
	 */
	task void checkBufferForValidResponse()
	{
		//SODbg(1,"check buffer for valid response\n");
	}
	
	task void handleSendMoniFail() {
		signal GsmUartHandler.SendCommandDone(NULL, FAIL);
		signal GsmUartHandler.ErrorReceived(UNSUPPORTED_FEATURE);
	}


	/**
	 * ---------Commands------
	 *
	 */

	/**
	 * This command will send a command over UART line.
	 * It is the responsibility of the upper layers to make sure the device handles state changes and timeout errors for sending commands.
	 */
	async command result_t GsmUartHandler.sendCommand(char *cmd, uint8_t length)
	{
		char tcmd[256];
		char *sp;
		char *tok;
		//copy command to the local variable
		strcpy(tcmd,cmd);
		//send message over the uart over the GsmUartHandler.sendCommand
		//check if it is a #MONI command
		if(strstr(tcmd,"#MONI=") != NULL)
		{
			//grab the param value to get the count
			//tokenize at the '=' then grab the last value
			tok = strtok_r(tcmd,"=",&sp);
			gsmCellMonParamVal = atoi(tok);
			//SODbg(1,"found #MONI= collecting count: %i",gsmCellMonParamVal);
			if(gsmCellMonParamVal == 0) {
				return call SendVarLenPacketGsm.send((uint8_t*)cmd,length);
			}
			else {
				post handleSendMoniFail();
				return FAIL;
			}
		}
		else {
			return call SendVarLenPacketGsm.send((uint8_t*)cmd,length);
		}
	}
	
	/**
	 * ---------Events--------
	 */
	event result_t SendVarLenPacketGsm.sendDone(uint8_t *sentdata, result_t result)
	{
		//SODbg(1,"sendvarlenpacketgsm send done\n");
		signal GsmUartHandler.SendCommandDone(sentdata, result);
		return SUCCESS;
	}
	
	/**
	 * This event will be raised once the GUH realizes an OK was sent over the UART line.
	 * NOTE: It is the responsibility of the upper layers to ensure commands are sent synchronously.
	 */
	async default event void GsmUartHandler.OkReceived(){;}

	/**
	 * This event will be raised once the GUH realizes an ERROR was received over the UART line.
	 * If the error includes an error code it will return the error code in the parameter “errorCode”.
	 */
	async default event void GsmUartHandler.ErrorReceived(gsm_error_t errorCode){;}
	
	/**
	 * This event will be raised once the GUH realizes an Network Registration Report was received.
	 * If the Network Registration Report includes data regarding the registration of the device, GUH will store this information in an internal structure. 
	 * This event will return a pointer to the structure and the size of the structure for listening components.
	 */
	async default event void GsmUartHandler.NetworkRegistrationReceived(gsm_cellid_areacode_data_t*ptr, uint8_t dataLength){;}

	/**
	 *This event will be raised once the GUH realizes a Cell Monitor Report was received on the UART line.
		If the Cell Monitor Report includes data regarding the neighboring cellular towers, GUH will store the information in an internal structure.
		This event will return a pointer to the structure and the size of the structure for listening components.i 
	 */
	async default event void GsmUartHandler.CellMonitorReportReceived(gsm_cellmon_data_t*ptr,uint16_t dataLength){;}
	
	/**
	 * This even will be raised once the GUH realizes a prompt(<cr><cl><greater than><space>) was 
	 * received over UART
	 */
	async default event void GsmUartHandler.PromptReceived(){;}

	/**
	 * This event will be raised once the GUH successfully sends a command on the UART line.
	 */
	async default	event result_t GsmUartHandler.SendCommandDone(uint8_t *cmd, result_t result){return SUCCESS;}
	
	/**
	 * This event will be raised once the GUH successfully reads signal quality on the UART line.
	 */
	async default event void GsmUartHandler.SignalQualityReceived(gsm_signal_quality_data_t*ptr,uint8_t dataLength){;}

}


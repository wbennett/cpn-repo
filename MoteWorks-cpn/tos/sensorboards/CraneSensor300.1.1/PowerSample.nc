/*
 *
 *
 *
 *
 *
 *
 */

#include "PowerMonitor/types.h"

interface PowerSample {
	command result_t getData();
	event result_t dataReady(power_monitor_t );
}

/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: GpsPacket.nc,v 1.1.2.4 2007/12/03 22:28:10 xyang Exp $
 */


module UARTPacket {
    provides {
        interface StdControl as Control;
        interface ReceiveMsg as Receive;
        interface SendVarLenPacket;

        command result_t txBytes(uint8_t *bytes, uint8_t numBytes);
    }
    
    uses {
        interface ByteComm;
        interface StdControl as ByteControl;
        interface Leds;
    }
}

implementation {
    
/* === Preprocessor ======================================================== */
    #include "Tracker/gsm.h"
		
		//#include "SOdebug1.h"
	
/* === Frame State ========================================================= */
 
    
    //RX States
    enum {
       NO_GSM_START_BYTE = 0,  //no start
       GSM_START_BYTE = 1,     //start byte found and some
       GSM_BUF_NOT_AVAIL = 2   //done
    };
    uint8_t gpsRecvState;        //detect gps pckt

    
    //State
    enum {
        IDLE,
        PACKET,
        BYTES,
    };
    uint8_t state;
	
	//	AT_GSM_MSG,	AT_GEN_MSG

    
    //Buffer tracking
    uint16_t rxCount, txCount, txLength;
    uint8_t *recPtr;
    uint8_t *sendPtr;
    
    //Buffers

		General_Msg bufferSend;
		General_Msg bufferRecv[4];
		uint8_t curBufferRecvIndex = 0;
		
		recvQueue_t recvQueue;
		
		inline result_t enterRecvQueue(uint8_t element)
		{
			if(recvQueue.count >= QUEUE_SIZE)
			{
				return FAIL;
			}
			else
			{
				atomic{
					uint8_t newIndex = (recvQueue.front+recvQueue.count)%QUEUE_SIZE;
					recvQueue.contents[newIndex] = element;
					recvQueue.count++;
					return SUCCESS;
				}
			}
		}
		
		inline uint8_t removeRecvQueue()
		{
			uint8_t r_val;
			atomic{
				r_val = recvQueue.contents[recvQueue.front];
				recvQueue.front = (recvQueue.front+1)%QUEUE_SIZE;
				recvQueue.count--;
			}
			return r_val;
		}
		
  /*
    state == IDLE, nothing is being sent
    state == PACKET, this level is sending a packet
    state == BYTES, this level is just transferring bytes

    The purpose of adding the new state, to simply transfer bytes, is because
    certain applications may want to just send a sequence of bytes without the
    packet abstraction.  One such example is the UART.

  */
  
  /*
    Moved these from gps.h #defines to editable variables
	so we can get gps messages that start with '$' and end with <CR>
	or AT message responses that start with 'A' and end with OK/r/n
  */
  
/* === StdControl ========================================================== */

    inline void initState() {
        atomic {  
            gpsRecvState = NO_GSM_START_BYTE;
            state = IDLE;
            
            txCount = rxCount = 0;      //reset recv counts
						curBufferRecvIndex = 0;
						recPtr = (uint8_t *) &bufferRecv[curBufferRecvIndex];
						recvQueue.front = 0;
						recvQueue.count = 0;
						sendPtr = (uint8_t *) &bufferSend;            
        }
    }

  
    /**
     *
     */
    command result_t Control.init() {    
        initState();
        return call ByteControl.init();        
    }

    /**
     *
     */
    command result_t Control.start() {
        return call ByteControl.start();
    }

    /**
     *
     */
    command result_t Control.stop() {
        return call ByteControl.stop();
    }


/* === Send ================================================================ */

  command result_t txBytes(uint8_t *bytes, uint8_t numBytes) {
    atomic
		{
		if (txCount == 0)
      {
        txCount = 1;
        txLength = numBytes;
        sendPtr = bytes;
    /* send the first byte */
        if (call ByteComm.txByte(sendPtr[0]))
        return SUCCESS;
    else
        txCount = 0;
      }
		}
    return FAIL;
  }


  /* Command to transfer a variable length packet */
  command result_t SendVarLenPacket.send(uint8_t* packet, uint8_t numBytes) {
    atomic{ 
	  state = BYTES;
	}
	  
    return call txBytes(packet, numBytes);
  }

  
  task void sendVarLenFailTask() {
    atomic{
      txCount = 0;
      state = IDLE;
    }
    signal SendVarLenPacket.sendDone((uint8_t*)sendPtr, FAIL);
  }

  task void sendVarLenSuccessTask() {
    atomic {
      txCount = 0;
      state = IDLE;
    }
    signal SendVarLenPacket.sendDone((uint8_t*)sendPtr, SUCCESS);
  }
  
  void sendComplete(result_t success) {
   atomic{ 
    if (state == BYTES) {
      if (success) {
            post sendVarLenSuccessTask();
      }
      else {
            post sendVarLenFailTask();
      }
    }
    else {
      txCount = 0;
      state = IDLE;
    }
   } //atomic
  }

      
  default event result_t SendVarLenPacket.sendDone(uint8_t* packet, result_t success) {
    return success;
  }

  
  /* Byte level component signals it is ready to accept the next byte.
     Send the next byte if there are data pending to be sent */
 async event result_t ByteComm.txByteReady(bool success) {
   atomic{
    if (txCount > 0){
         if (!success){
            //dbg(DBG_ERROR, "TX_packet failed, TX_byte_failed");
            sendComplete(FAIL);
         }
         else if (txCount < txLength){
            //dbg(DBG_PACKET, "PACKET: byte sent: %x, COUNT: %d\n",sendPtr[txCount], txCount);
            if (!call ByteComm.txByte(sendPtr[txCount++])) sendComplete(FAIL);
         }
    }
   } //atomic
    return SUCCESS;
  }

 async  event result_t ByteComm.txDone() {
   atomic{
    if (txCount == txLength)
      sendComplete(TRUE);
    }
    return SUCCESS;
  }
  
/* === Receive ============================================================= */

    /**
     *  Signal receive + Reset State
     */
    task void receiveTask() {
        
        //NOTE:
        //Even though we use a ptr swap interface, we do not actually swap
        //ptrs
				uint8_t msgNumber = removeRecvQueue();
				//SODbg(1,"receiveTask:%i,%s",bufferRecv[msgNumber].length,bufferRecv[msgNumber].data);
        signal Receive.receive((TOS_MsgPtr) &bufferRecv[msgNumber]);
        atomic gpsRecvState = NO_GSM_START_BYTE;
    }
	
	
	/**
     * Byte received from GSM
     * First byte in gps packet is reserved to count number of bytes rcvd.
     * Gps messages start with '$' (0x24) and end with <cr><lf> (0x0D, 0x0A)
     *
     */
	async event result_t ByteComm.rxByteReady(uint8_t data, bool error, uint16_t strength) {		
				uint8_t lState;
				result_t stat;
        atomic lState = gpsRecvState;
				
				if(data == '\0') {
					return SUCCESS;
				}
        
        if (error) {
            atomic {
                rxCount = 0;
                atomic gpsRecvState = NO_GSM_START_BYTE;
            }
            
            return FAIL;   //Return Statement doesn't matter
        }
        
        //Buffer not available
        if (lState == GSM_BUF_NOT_AVAIL) {
            return SUCCESS;
        }
        
        //Looking for start byte
        if (lState == NO_GSM_START_BYTE) {
            
						if(data != 0x0D && data != 0x0A) //CR=0x0D, LF=0x0A
						{
            //Start Byte found
								//SODbg(1,"START_BYTE:%c",data);
                atomic {
										curBufferRecvIndex = (curBufferRecvIndex+1)%4;
										recPtr = (uint8_t *) &bufferRecv[curBufferRecvIndex];
                    memset(recPtr, 0, GEN_DATA_LENGTH+1);  //clean my buffer
                    rxCount = 1;                           //set length to 1
                    recPtr[1] = data;                     //insert start symbol
                    gpsRecvState = GSM_START_BYTE;
            		}
            }
            return SUCCESS;
        }
        
        //Start byte already found
        if (lState == GSM_START_BYTE) {
            
            //Max Length Exceeded
            if (rxCount >= GEN_DATA_LENGTH - 1 ) {
                atomic gpsRecvState = NO_GSM_START_BYTE;
                return SUCCESS;
            }
            else if ( data  == 0x0D ||  recPtr[rxCount] == '>' ) {//CR=0x0D
                	//SODbg(1,"STOP_BYTE:%c",data);
									atomic {
                  	  gpsRecvState = GSM_BUF_NOT_AVAIL;
                    	rxCount++;
                   	 	recPtr[rxCount] = data;
                    	recPtr[0] = rxCount;//Set length in General_Msg
											recPtr[rxCount+1] = '\0';
											stat = enterRecvQueue(curBufferRecvIndex);
                	}
								//if(recPtr[1] != '\r' || ( data == ' ' && recPtr[rxCount] == '>')){
								//if(( data == ' ' && recPtr[rxCount] == '>')){
                	//post receiveTask(); 
								//}
								//else{
									post receiveTask();
        					atomic gpsRecvState = NO_GSM_START_BYTE;
								//}
                return SUCCESS;
            }
						else {
							//Payload portion
							atomic {
                rxCount++;
                recPtr[rxCount] = data;
							}
						}
        }
        

        return SUCCESS;
    }


}






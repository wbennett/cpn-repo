module CompassM
	{
	provides
		{
		interface Compass;
		interface SplitControl;
		}
	uses
		{
		interface HMC6343;
		interface StdControl as TimerControl;
		interface SplitControl as CompassControl;
		interface Timer as CalibrateTimer;
		}
	}

implementation
{
	uint16_t AccelData[3];		//x, y, z
	uint16_t HeadingData[3];
	uint16_t TiltData[3];
	uint8_t cmdBuffer;
	
	task void signalTiltsDone();
	task void signalAccelsDone();
	task void signalHeadingDone();
	task void signalTiltsDoneFail();
	task void signalAccelsDoneFail();
	task void signalHeadingDoneFail();
	task void signalCalDone(); 
	task void signalCalDoneFail();
	task void postStartDone();
	task void postStopDone();
	task void postInitDone();
	
	command result_t SplitControl.stop()
		{
		call CalibrateTimer.stop();	
		return call CompassControl.stop();
		}
	
	event result_t CompassControl.stopDone()
		{
		post postStopDone();
		return SUCCESS;
		}

	task void postStopDone()
		{
		signal SplitControl.stopDone();
		}
	task void postStartDone() {
		signal SplitControl.startDone();
	}
	task void postInitDone() {
		signal SplitControl.initDone();
	}

	command result_t SplitControl.start() 
		{
		call TimerControl.start();
		call CompassControl.start();
		return SUCCESS; 
		}
	
	event result_t CompassControl.startDone()
		{
		post postStartDone();
		return SUCCESS;
		}

	command result_t SplitControl.init()
		{
		call TimerControl.init();
		return call CompassControl.init();
		}

	event result_t CompassControl.initDone()
		{	
		post postInitDone();
		return SUCCESS;
		}
	inline void performOperation()
		{
		if(cmdBuffer == 0)
			{
			call HMC6343.postAccelData();
			}
		else if(cmdBuffer == 1)
			{
			result_t rval = FAIL;
			rval = call HMC6343.postHeadingData();
			}
		else if(cmdBuffer == 2)
			{
			call HMC6343.postTiltData();
			}
		else if(cmdBuffer == 4)
			{
			call HMC6343.enterUserCal();
			}
		cmdBuffer = 3;
		}
	command void Compass.calibrate() 
		{
			cmdBuffer = 4;
			performOperation();

		}
	
	command void Compass.getAccels()
		{
		cmdBuffer = 0;
		performOperation();
		}

	command void Compass.getHeadings()
		{
		cmdBuffer = 1;
		performOperation();
		}

	command void Compass.getTilts()
		{
		cmdBuffer = 2;
		performOperation();
		}
	event result_t CalibrateTimer.fired() 
		{
			//exit calibration mode
			call HMC6343.exitUserCal();
			return SUCCESS;
		}
	event void HMC6343.postAccelDataDone(result_t result, uint8_t* data)
		{
		if(result == SUCCESS)
			{
			AccelData[0] = (uint16_t)( data[0] << 8 | data[1] );
			AccelData[1] = (uint16_t)( data[2] << 8 | data[3] );
			AccelData[2] = (uint16_t)( data[4] << 8 | data[5] );
			post signalAccelsDone();
			}
		else
			{
			post signalAccelsDoneFail();
			}
		}

	task void signalAccelsDone()
		{
		signal Compass.getAccelsDone(SUCCESS, AccelData);
		}

	task void signalAccelsDoneFail()
		{
		signal Compass.getAccelsDone(FAIL, AccelData);
		}

	event void HMC6343.postHeadingDataDone(result_t result, uint8_t* data)
		{
		if(result == SUCCESS)
			{
			HeadingData[0] = (uint16_t)( data[0] << 8 | data[1] );	//heading
			HeadingData[1] = (uint16_t)( data[2] << 8 | data[3] );	//pitch
			HeadingData[2] = (uint16_t)( data[4] << 8 | data[5] );	//roll
			post signalHeadingDone();
			}
		else
			{
			post signalHeadingDoneFail();
			}
		}


	task void signalHeadingDone()
		{
		signal Compass.getHeadingsDone(SUCCESS, HeadingData);
		}
	
	task void signalHeadingDoneFail()
		{
		signal Compass.getHeadingsDone(FAIL, HeadingData);
		}
	
	event void HMC6343.enterUserCalDone(result_t result)
		{
			//start timer 
			//user should calibrate now
			if(result == FAIL) 
				{
				post signalCalDoneFail();
				}
			else
				{
				call CalibrateTimer.start(TIMER_ONE_SHOT,30000);
				}
		}

	event void HMC6343.exitUserCalDone(result_t result)
		{
			if(result == FAIL)
				{
					post signalCalDoneFail();
				}
			else
				{
					post signalCalDone();
				}
		}
	
	task void signalCalDone()
		{
		signal Compass.calibrateDone(SUCCESS);
		}
	
	task void signalCalDoneFail()
		{
		signal Compass.calibrateDone(FAIL);
		}

	event void HMC6343.postTiltDataDone(result_t result, uint8_t* data)
		{
		if(result == SUCCESS)
			{
			TiltData[0] = (uint16_t)( data[0] << 8 | data[1] );	//pitch
			TiltData[1] = (uint16_t)( data[2] << 8 | data[3] );	//roll
			TiltData[2] = (uint16_t)( data[4] << 8 | data[5] );	//temp
			post signalTiltsDone();
			}
		else
			{
			post signalTiltsDoneFail();
			}
		}
	
	task void signalTiltsDone()
		{
		signal Compass.getTiltsDone(SUCCESS, TiltData);
		}
	
	task void signalTiltsDoneFail()
		{
		signal Compass.getTiltsDone(FAIL, TiltData);
		}

	event void HMC6343.resetDone(result_t result)
		{
		}

	event void HMC6343.StandToRunDone(result_t result)
		{
		}
	
	event void HMC6343.RunToStandDone(result_t result)
		{
		}
	
	event void HMC6343.RunToSleepDone(result_t result)
		{
		}
	
	event void HMC6343.SleepToStandDone(result_t result)
		{
		}
	
	default event void Compass.getTiltsDone(result_t result, uint16_t * data)
		{
		}

	default event void Compass.getHeadingsDone(result_t result, uint16_t * data)
		{
		}

	default event void Compass.getAccelsDone(result_t result, uint16_t * data)
		{
		}
	}

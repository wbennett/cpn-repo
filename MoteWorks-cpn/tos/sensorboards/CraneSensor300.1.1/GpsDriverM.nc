/*
 *
 *
 *
 *
 */
//38400 baud for gps
//115200 baud for iris/sodbg

#include "debug_constants.h"

module GpsDriverM {
  provides {
    interface SplitControl as GpsSplitControl;
		interface ReceiveMsg as GpsGGARecv;
		interface ReceiveMsg as GpsRMCRecv;
  }
  uses {
    interface StdControl as GpsControl;
    interface SendVarLenPacket as SendVarLenPacket;
    interface ReceiveMsg as Receive;
		#ifdef I2CDBG
		I2CDBG_CONFIG();
		#endif
		#ifdef USERADIODBG
		RADIODBG_CONFIG();
		#endif
		interface Leds;
  }
}
implementation {

//preprocessor
  
  #include "Tracker/gps.h"
	/*
	 * UART1_BAUDRATE specifies the baudrate of the
	 * uart line set this to be the speed of the gps.
	 * The datasheet says otherwise but the baud is actually
	 * 38400, from sniffing the line.
	 */
 	//#define DBG_PKT 1
	//#define SO_DEBUG 1
	//#include "SOdebug1.h"

//frame states
  enum {
		GPS_STATE_OFF,
		GPS_STATE_ON,
  };
  uint8_t gState = GPS_STATE_OFF;
	
	
  
  result_t status;
  
  
  raw_storage_buffer rawBuffer;
	NMEAMsg genericMsgBuffer;

	raw_storage_buffer rawBuffer;	
//function params

	inline void parse_gga_message(GPS_MsgPtr gps_data);
	inline void parse_rmc_message(GPS_MsgPtr gps_data);
	inline void fillGGAMsg(GGAMsg * GGAPtr);
	inline void fillRMCMsg(RMCMsg * RMCPtr);
  

	//tasks
	
	task void signalInitDone()
	{
		signal GpsSplitControl.initDone();
	}
	
	task void signalStartDone()
	{
		signal GpsSplitControl.startDone();
	}
	
	task void signalStopDone()
	{
		signal GpsSplitControl.stopDone();
	}

	//splitcontrol
	inline void initState()
	{
		
	}


	inline result_t PowerSwitch(uint8_t blah)
	{
		//SODbg(1,"PS:%i\n",blah);
		if(gState == GPS_STATE_OFF && blah == 1)
		{
			TOSH_SET_PW5_PIN();
			gState = GPS_STATE_ON;
			return SUCCESS;
		}
		else if(blah == 0)
		{
			TOSH_CLR_PW5_PIN();
			gState = GPS_STATE_OFF;
			return SUCCESS;
		}
		return FAIL;
	}

	command result_t GpsSplitControl.init()
	{
		result_t result;
		//SODbg(1,"DM.init\n");
		TOSH_MAKE_PW5_OUTPUT();
		TOSH_CLR_PW5_PIN();
		initState();
		result = call GpsControl.init();
		if (result == SUCCESS)
		{
			post signalInitDone();
		}
		else
		{
			return FAIL;
		}
		return result;
	}

	command result_t GpsSplitControl.start()
	{
		result_t result;

		//SODbg(1,"DM.start\n");
	
		if(gState != GPS_STATE_OFF)
		{
			return FAIL;
		}
		//turn on power line here....
		result = PowerSwitch(1);
		call GpsControl.start();
		post signalStartDone();
		return result;
	}

	command result_t GpsSplitControl.stop()
	{
		result_t result1, result2, result3;
		//SODbg(1,"DM.stop\n");

		result1 = call GpsControl.stop();
		//turn off power line....
		result2 = PowerSwitch(0);	
		result3 = rcombine(result1, result2);
		post signalStopDone();
		return result3;
	}

	
	
//==================== Commands ===========================//
  //Each string terminated by NULL char but don't want to send that
  //uint8_t WARM_START_GPS_CMD[11] = "AT$GPSR=2\r";
  //uint8_t GET_AQRD_GPS_POS_CMD[11] = "AT$GPSACP\r";
	//uint8_t START[7] = "START\r\n";
  
  
  
  event TOS_MsgPtr Receive.receive(TOS_MsgPtr msgPtr)
	{
		GPS_MsgPtr blah;
		blah = ((GPS_MsgPtr)msgPtr);
	
		dprintf("receive:%s", ((GPS_MsgPtr)msgPtr)->data);
//		call Leds.yellowToggle();
		if(is_gga_string_m( ((GPS_MsgPtr)msgPtr)->data) )
		{
			parse_gga_message( (GPS_MsgPtr)msgPtr);
			//SODbg(DBG_PKT, "GGA:receive %s", ((GPS_MsgPtr)msgPtr)->data);
			if(rawBuffer.parsed_gga[6][0] == '1')//if valid fix....
			{
				//SODbg(DBG_PKT, "GGA0:%s\n",rawBuffer.parsed_gga[0]);
				fillGGAMsg(&genericMsgBuffer.GGAMsgBuffer);
				signal GpsGGARecv.receive( (TOS_MsgPtr)&genericMsgBuffer.GGAMsgBuffer);
			}
		}
		else if(is_rmc_string_m( ((GPS_MsgPtr)msgPtr)->data) )
		{
			parse_rmc_message( (GPS_MsgPtr)msgPtr);
			//SODbg(DBG_PKT, "RMC:receive %s", ((GPS_MsgPtr)msgPtr)->data);
			if(rawBuffer.parsed_rmc[2][0] == 'A')
			{
				//SODbg(DBG_PKT, "RMC0:%s\n",rawBuffer.parsed_rmc[0]);
				fillRMCMsg(&genericMsgBuffer.RMCMsgBuffer);
				signal GpsRMCRecv.receive( (TOS_MsgPtr)&genericMsgBuffer.RMCMsgBuffer);
			}
		}
		return msgPtr;
  }

	inline void parse_gga_message(GPS_MsgPtr gps_data)
	{
		uint8_t i,j,k;
		bool end_of_field;
		uint8_t length;

		end_of_field = FALSE;
		i = 0;
		k = 0;
		length = gps_data->length;
		memset(rawBuffer.parsed_gga, '0', GGA_FIELDS*GPS_CHAR_PER_FIELD);
		
		while( i < GGA_FIELDS)
		{
			end_of_field = FALSE;
			j = 0;
			while((!end_of_field)&&(k < length))
			{
				if(gps_data->data[k] == GPS_DELIMITER)
				{
					end_of_field = TRUE;
				}
				else
				{
					if(j<GPS_CHAR_PER_FIELD)
					{
						rawBuffer.parsed_gga[i][j] = gps_data->data[k];
					}
				}
				j++;
				k++;
			}
			i++;
		}
	}
	
	inline void parse_rmc_message(GPS_MsgPtr gps_data)
	{
		uint8_t i,j,k;
		bool end_of_field;
		uint8_t length;

		end_of_field = FALSE;
		i = 0;
		k = 0;
		length = gps_data->length;
		memset(rawBuffer.parsed_rmc, '0', RMC_FIELDS*GPS_CHAR_PER_FIELD);
		
		while( i < RMC_FIELDS)
		{
			end_of_field = FALSE;
			j = 0;
			while((!end_of_field)&&(k < length))
			{
				if(gps_data->data[k] == GPS_DELIMITER)
				{
					end_of_field = TRUE;
				}
				else
				{
					if(j<GPS_CHAR_PER_FIELD)
					{
						rawBuffer.parsed_rmc[i][j] = gps_data->data[k];
					}
				}
				j++;
				k++;
			}
			i++;
		}
		return;
	}
 
 	//MsgId 			0
	//FixTime 		1
	//Lat 				2
	//LatDir 			3
	//Lon 				4
	//LonDir 			5
	//FixQuality 	6
	//SatsUsed 		7
	//HDOP 				8
	//Alt 				9
	//AltUnit 		10
	//GeoSep 			11
	//GeoSepUnit 	12
	//DGPSage 		13
	//DGPSstaID 	14
	//ChkSum 			15
 	inline void fillGGAMsg(GGAMsg * GGAPtr)
	{
		//extract GGA
		char *pdata;
		uint16_t alt_extract;
		
		//GGAPtr->FixTime[0] = rawBuffer.parsed_gga[0][0];

		if(rawBuffer.parsed_gga[6][0] == '1')
		{
			GGAPtr->valid = 1;
		}
		else
		{
			GGAPtr->valid = 0;
		}
		
		pdata = rawBuffer.parsed_gga[1];
		//GGAPtr->TimeHrs = (rawBuffer.parsed_gga[1][0]-'0')*10+
		GGAPtr->TimeHrs = extract_hours_m(pdata);
		GGAPtr->TimeMin = extract_minutes_m(pdata);
		GGAPtr->TimeSec = extract_seconds_m(pdata);
		
		pdata=rawBuffer.parsed_gga[2];
		GGAPtr->Lat_deg = extract_lat_deg_m(pdata);
		GGAPtr->Lat_dec_min = (uint32_t)(10000*extract_lat_dec_min_m(pdata));
		
		pdata=rawBuffer.parsed_gga[4];
		GGAPtr->Lon_deg = extract_lon_deg_m(pdata);
		GGAPtr->Lon_dec_min = (uint32_t)(10000*extract_lon_dec_min_m(pdata));
		
		pdata=rawBuffer.parsed_gga[9];
		GGAPtr->Alt = 0;
		for(alt_extract = 0; alt_extract < GPS_CHAR_PER_FIELD && rawBuffer.parsed_gga[9][alt_extract] != '.'; alt_extract++)
		{
			GGAPtr->Alt = GGAPtr->Alt * 10;
			GGAPtr->Alt = GGAPtr->Alt + (rawBuffer.parsed_gga[9][alt_extract]-'0');
		}
		
	}

	event result_t SendVarLenPacket.sendDone(uint8_t *sentData, uint8_t numBytes)
	{
		return SUCCESS;
	}

 //MsgId 			0
 //FixTime 		1
 //FixStatus 	2
 //Lat 				3
 //LatDir 		4
 //Lon 				5
 //LonDir 		6
 //Speed 			7
 //Tcourse 		8
 //FixDate 		9
 //MagVar 		10
 //MVDir 			11
 //FixMode 		12
 //ChkSum 		13
 	inline void fillRMCMsg(RMCMsg * RMCPtr)
	{
		//extract RMC
		char *pdata;
		uint16_t i;
		
		RMCPtr->Speed = 0;
		for(i = 0; i < GPS_CHAR_PER_FIELD && rawBuffer.parsed_rmc[7][i] != '.'; i++)
		{
			RMCPtr->Speed = RMCPtr->Speed * 10;
			RMCPtr->Speed = RMCPtr->Speed + (rawBuffer.parsed_rmc[7][i] - '0');
		}


		
		RMCPtr->Tcourse = 0;
		for(i = 0; i < GPS_CHAR_PER_FIELD && rawBuffer.parsed_rmc[8][i] != '.'; i++)
		{
			RMCPtr->Tcourse = RMCPtr->Tcourse * 10;
			RMCPtr->Tcourse = RMCPtr->Tcourse + (rawBuffer.parsed_rmc[8][i] - '0');
		}
		
		pdata = rawBuffer.parsed_rmc[9];
		RMCPtr->FixDate = (uint32_t)(extract_date_m(pdata));

		if(rawBuffer.parsed_rmc[2][0] == 'A')
		{
			RMCPtr->valid = 1;
		}
		else
		{
			RMCPtr->valid = 0;
		}

	}
 
 	
}


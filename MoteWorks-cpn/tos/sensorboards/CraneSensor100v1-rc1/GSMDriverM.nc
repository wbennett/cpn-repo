//57600 baud for gps
//115200 baud for iris/sodbg

module GSMDriverM 
{
  provides 
	{
		interface SplitControlStatus;
    interface GSM_ModemI as GSM_Modem;
  }
  uses 
	{
    interface StdControl as UARTControl;
		interface GsmUartHandlerI as GsmUartHandler;
		interface StdControl as TimerControl;
		interface Timer;
		interface Timer as CommandTimer;
		interface Timer as WatchdogTimer;
		interface Leds;
  }
}
implementation 
{
  //#define DBG_PKT 1
  //#define SO_DEBUG 1
	//SOdebug uses UART0
	//SOdebug1 uses UART1
	//use uart0 because the gsm communicates over uart1
  //#include "SOdebug1.h"

	#include "gsm.h"
  #include <string.h>

	enum
	{
		NONE = 0,
		//Configuration generated states
		GSM_START_READY,
		GSM_VCC_ENABLE,
		GSM_POWER_ON_PHASE1,
		GSM_POWER_ON_PHASE2,
		GSM_SET_BAUD_RATE,
		GSM_SET_REGISTER_WITH_EXTRA_INFO,
		GSM_SET_VERBOSE_MODE,
		GSM_SET_NORTH_AMERICA_BAND,
		GSM_SET_ENERGY_SAVE_MODE,
		GSM_SET_TEXT_FORMAT,
		GSM_CHECK_IF_ASSOC,
		//Command generated states
		GSM_SET_CELL_MONITOR_RPT_VAL,
		GSM_GET_CELL_MONITOR_RPT,
		GSM_GET_SIGNAL_QUALITY,
		SMS_REQUEST_PROMPT,
		SMS_SEND_PAYLOAD,
	};
	/**
	 * Assocation process
	 * --------------
	 * 1. Set Baud Rate
	 * 2. Set Verbose Mode
	 * 3. Set North America Radio Band
	 * 4. Set Register with extra info (2) 
	 * 5.	Set Energy Saving Mode
	 * 6. Set Text Mode
	 * 7. Wait for unsolicited Association Notification (CREG)
	 * 8. Once configured and associated, signal listeners with startDone() event
	 *
	 * Commands
	 * --------------
	 * 9. Collect cell id and area code on command (if associated)
	 * 10. Collect rssi and bit error rate - signal quality - on command (if associated)
	 * (SMS Command)
	 * 11a. Send initial command with phone number
	 * 11b. Wait for reception of '>' (greater than) prompt or timeout
	 * 11c. Send text payload terminated with 0x1A when prompted, or retry if timeout
	 * 11d. Notify listeners with sendSMSDone event with either NO_ERROR or an error code on failure
	 */ 
	
	//status variables
	gsm_error_t gsm_error_code;
	//structs
	gsm_cellid_areacode_data_t currentAssocData;
	gsm_cellmon_data_t currentCellMonitorReportData;
	gsm_signal_quality_data_t currentSignalQualityData;
	
	//text structs
	uint8_t currentPhoneNumber[20];
	uint8_t currentTextData[165];
	uint32_t timeoutPeriod;
	bool isAssociated;
	bool isStartDone;
	bool isConfigDone;
	//command state
	uint8_t currentCmdState;

  //=====================watchdog============================//
	#define DEFAULTWATCHDOGTIMEOUT (120000u)// 2 minutes...
  //==================== Commands ===========================//
	#define SMS_REQUEST_PROMPT_TIMEOUT (1000u)  //1s to get '>' prompt
	#define SMS_SEND_PAYLOAD_TIMEOUT (60000u) //60s after CTRL-Z
	#define DEFAULTCOMMANDTIMEOUT (2048u) //1s for all other commands
	
	#define NUMBEROFCOMMANDRETRY (3)
	uint8_t commandRetryCount;
	uint8_t *CURRENT_COMMAND;//this will point to the current command being sent...
	//Each string terminated by a null terminator
	uint8_t *SET_BAUD_RATE_CMD = (uint8_t*)"AT+IPR=57600\r";
	uint8_t *SET_REGISTER_MORE_INFO_CMD =(uint8_t*) "AT+CREG=2\r";
	uint8_t *SET_VERBOSE_MODE_CMD = (uint8_t*)"AT+CMEE=2\r"; //report errors in verbose format
	uint8_t *SET_NORTH_AMERICA_BAND_CMD = (uint8_t*)"AT#BND=3\r";
	uint8_t *SET_ENERGY_SAVE_CMD = (uint8_t*)"AT+CFUN=1\r";
	uint8_t *SET_TEXT_FORMAT_CMD = (uint8_t*)"AT+CMGF=1\r";
	uint8_t *CHECK_IF_ASSOC_CMD = (uint8_t*)"AT+CREG?\r";
	uint8_t *SET_CELL_MONITOR_RPT_VAL_P1_CMD = (uint8_t*)"AT#MONI=";
	uint8_t SET_CELL_MONITOR_RPT_VAL_WHOLE_CMD[15];
    //FIXME firmware v7 needs no question mark v6 needs a question mark
	uint8_t *GET_CELL_MONITOR_RPT_CMD = (uint8_t*)"AT#MONI\r";
    //FIXME firmware v7 needs no question mark v6 needs a question mark
	uint8_t *GET_SIGNAL_QUALITY_CMD = (uint8_t*)"AT+CSQ\r";
	//sms commands
	uint8_t *SET_TEXT_MODE_CMD = (uint8_t*)"AT+CMGF=1\r";
	uint8_t *SMS_REQUEST_PROMPT_P1_CMD = (uint8_t*)"AT+CMGS=\"";
	uint8_t *SMS_REQUEST_PROMPT_P2_CMD = (uint8_t*)"\"\r";
	uint8_t SMS_REQUEST_PROMPT_WHOLE_CMD[30];
	uint8_t SMS_SEND_DATA_BUFFER[165];
  
	//=============prototypes===============//
	inline void initVars();
	inline void initPins();
	inline void enableGroundLine();
	inline void disableGroundLine();	
	inline void enablePowerLine();
	inline void disablePowerLine();
	inline result_t sendCurrentCommand();

	task void initDoneTask();
	task void startDoneTask();
	task void startFailTask();
	task void stopDoneTask();
	task void OkReceivedTask();
	task void ErrorReceivedTask();
	task void NetworkRegistrationReceivedTask();
	task void setCellMonitorReportValueDoneTask();
	task void setCellMonitorReportValueFailTask();
	task void cellMonitorReportReadyTask();
	task void cellMonitorReportFailTask();
	task void signalQualityDataReadyTask();
	task void signalQualityDataFailTask();
	task void sendSMSDoneTask();
	task void sendSMSFailTask();
	inline void transition();
	
	//======================================//

	inline void initPins()
	{
		//SODbg(1,"init pins\n");
		TOSH_MAKE_PW0_OUTPUT();
		TOSH_MAKE_PW1_OUTPUT();
	}

	/**
	 * This method will activate the transistor ground
	 */
	inline void enableGroundLine()
	{
		//SODbg(1,"enable ground line\n");
		TOSH_SET_PW0_PIN();
	}
	/**
	 * This method will deactivate the transistor ground
	 */
	inline void disableGroundLine()
	{
		//SODbg(1,"disable ground line\n");
		TOSH_CLR_PW0_PIN();
	}

	/**
	 * This method will enable power pin
	 */
	inline void enablePowerLine()
	{
		//SODbg(1,"enable power line\n");
		TOSH_SET_PW1_PIN();
	}

	/**
	 * This method will disable power pin
	 */ 
	inline void disablePowerLine()
	{
		//SODbg(1,"disable power line\n");
		TOSH_CLR_PW1_PIN();
	}
	
	inline void setErrorCodeOnImedFail()
	{
		uint8_t lState;
		atomic lState = currentCmdState;
		if(!isAssociated)
		{
			atomic gsm_error_code = GSM_NOT_ASSOCIATED;
		}
		else if(lState != NONE)
		{
			atomic gsm_error_code = GSM_BUSY;
		}
	}
	
	/**
	 *	Initialize 
	 */
	command result_t SplitControlStatus.init()
	{
		//SODbg(1,"SplitControlStatus init\n");
		call TimerControl.init();
		initPins();
		call UARTControl.init();
		post initDoneTask();
		return SUCCESS;
	}
	
	default event result_t SplitControlStatus.initDone(){return SUCCESS;}

	task void initDoneTask()
	{
		//SODbg(1,"initdonetask\n");
		signal SplitControlStatus.initDone();
	}

	/**
	 * Start procedure of the GSM
	 */
	command result_t SplitControlStatus.start()
	{
		//SODbg(1,"SplitControlStatus start\n");
		call TimerControl.start();
		call UARTControl.start();
		initVars();
		//start the startup procedure signal startdone
		atomic currentCmdState = GSM_START_READY;
		//start timer to go through startup procedure
		call Timer.start(TIMER_ONE_SHOT,100);
		call WatchdogTimer.start(TIMER_ONE_SHOT,DEFAULTWATCHDOGTIMEOUT);
		return SUCCESS;
	}
	
	inline void initVars()
	{
		atomic commandRetryCount = 0;
		atomic gsm_error_code = NO_ERROR;
		isAssociated = FALSE;
		isStartDone = FALSE;
		isConfigDone = FALSE;
		atomic CURRENT_COMMAND = NULL;
	}
 	
	default event result_t SplitControlStatus.startDone(gsm_error_t result){return SUCCESS;} 

	task void startDoneTask()
	{
		signal SplitControlStatus.startDone(NO_ERROR);
	}
	
	task void startFailTask()
	{
		atomic
		{
			call CommandTimer.stop();
			currentCmdState = NONE;
			CURRENT_COMMAND = NULL;
			isAssociated = FALSE;
			isStartDone = FALSE;
			isConfigDone = FALSE;
			commandRetryCount = 0;
			signal SplitControlStatus.startDone(gsm_error_code);
		}
	}
	
	task void gsmConfigurationDoneTask()
	{
		atomic currentCmdState = NONE;
		isConfigDone = TRUE;
		//If we received unsolicited assoc report before finishing configuration routine
		//then post startDone b/c we are already associated and configured
		if(isAssociated)
		{
			isStartDone = TRUE;
			post startDoneTask();
		}
		//Otherwise wait for association or Watchdog to timeout
	}

	/**
	 * Stop procedure of the GSM
	 */
	command result_t SplitControlStatus.stop()
	{
		atomic gsm_error_code = NO_ERROR;
		post stopDoneTask();
		return SUCCESS;
	}

	default event result_t SplitControlStatus.stopDone(gsm_error_t result){return SUCCESS;}

	task void stopDoneTask()
	{
		atomic
		{
			//call TimerControl.stop();Don't call this bc it kills all timers
			call UARTControl.stop();
			call Timer.stop();
			call CommandTimer.stop();
			call WatchdogTimer.stop();
			disableGroundLine();
			disablePowerLine();
			atomic currentCmdState =	NONE;
			CURRENT_COMMAND = NULL;
			isAssociated = FALSE;
			isStartDone = FALSE;
			isConfigDone = FALSE;
			commandRetryCount = 0;
			signal SplitControlStatus.stopDone(gsm_error_code);
		}
	}
	
	command result_t GSM_Modem.setCellMonitorReportValue(uint8_t value)
	{
		if(value > 7)
		{
			atomic gsm_error_code = INVALID_PARAM;
			post setCellMonitorReportValueFailTask();
			return FAIL;
		}
		else if( (currentCmdState == NONE) && isAssociated)
		{
			result_t status;
			uint8_t t_val = 48 + value; //convert int between 0-7 to ascii code
			uint8_t valueStr[] = { t_val, 0x0D, 0x00 }; //number,<CR>,NULL
			
			atomic currentCmdState = GSM_SET_CELL_MONITOR_RPT_VAL;
			
			memset(SET_CELL_MONITOR_RPT_VAL_WHOLE_CMD,0,sizeof(SET_CELL_MONITOR_RPT_VAL_WHOLE_CMD));
			strcpy(SET_CELL_MONITOR_RPT_VAL_WHOLE_CMD,SET_CELL_MONITOR_RPT_VAL_P1_CMD);
			strcat(SET_CELL_MONITOR_RPT_VAL_WHOLE_CMD, valueStr);
			
			atomic CURRENT_COMMAND = SET_CELL_MONITOR_RPT_VAL_WHOLE_CMD;
			status = sendCurrentCommand();
			if(status == FAIL)
			{
				atomic
				{
					call CommandTimer.stop();
					atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
					post setCellMonitorReportValueFailTask();
				}
			}
			return status;
		}
		else
		{
			setErrorCodeOnImedFail();
			post setCellMonitorReportValueFailTask();
			return FAIL;
		}
	}

	command result_t GSM_Modem.getCellMonitorReport()
	{
		uint8_t lState;
		atomic lState = currentCmdState;
		//SODbg(1,"Upper layer called get cell id and area code\n");
		if( (lState == NONE) && isAssociated)
		{
			result_t status;
			atomic currentCmdState = GSM_GET_CELL_MONITOR_RPT;
			atomic CURRENT_COMMAND = GET_CELL_MONITOR_RPT_CMD;
			status = sendCurrentCommand();
			if(status == FAIL)
			{
				atomic
				{
					call CommandTimer.stop();
					atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
					post cellMonitorReportFailTask();
				}
			}
			return status;
		}
		else
		{
			setErrorCodeOnImedFail();
			post cellMonitorReportFailTask();
			return FAIL;
		}
	}

	command result_t GSM_Modem.getSignalQuality()
	{
		uint8_t lState;
		atomic lState = currentCmdState;
		//SODbg(1,"Upper layer called get cell id and area code\n");
		//get the signal quality procedure
		//check system state first
		if( (lState == NONE) && isAssociated)
		{
			result_t status;
			atomic currentCmdState = GSM_GET_SIGNAL_QUALITY;
			atomic CURRENT_COMMAND = GET_SIGNAL_QUALITY_CMD;
			status = sendCurrentCommand();
			if(status == FAIL)
			{
				atomic
				{
					call CommandTimer.stop();
					atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
					post signalQualityDataFailTask();
				}
			}
			return status;
		}
		else
		{
			setErrorCodeOnImedFail();
			post signalQualityDataFailTask();
			return FAIL;
		}
	}

	command result_t GSM_Modem.sendSMS(uint8_t *endpoint, uint8_t *data)
	{
		uint8_t lState;
		atomic lState = currentCmdState;
		//SODbg(1,"Upper layer called sendSMS\n");
		if( (lState == NONE) && isAssociated)
		{
			result_t status;
			memset(currentPhoneNumber,0,sizeof(currentPhoneNumber));
			strcpy((char*)currentPhoneNumber,(char*)endpoint);
			memset(currentTextData,0,sizeof(currentTextData));
			strcpy((char*)currentTextData,(char*)data);
			//Put together AT+CMGS="0123456789"<CR>
			strcpy((char*)SMS_REQUEST_PROMPT_WHOLE_CMD, (char*)SMS_REQUEST_PROMPT_P1_CMD);
			strcat((char*)SMS_REQUEST_PROMPT_WHOLE_CMD, (char*)currentPhoneNumber);
			strcat((char*)SMS_REQUEST_PROMPT_WHOLE_CMD, (char*)SMS_REQUEST_PROMPT_P2_CMD);
			atomic currentCmdState = SMS_REQUEST_PROMPT;
			atomic CURRENT_COMMAND = SMS_REQUEST_PROMPT_WHOLE_CMD;
			status = sendCurrentCommand();
			if(status == FAIL)
			{
				atomic
				{
					call CommandTimer.stop();
					atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
					post sendSMSFailTask();
				}
			}
			return status;
		}
		else
		{
			setErrorCodeOnImedFail();
			post sendSMSFailTask();
			return FAIL;
		}
	}

	inline result_t sendCurrentCommand()
	{
		result_t status;
		uint8_t localState;
		atomic localState = currentCmdState;
		switch(localState)
		{
			case SMS_REQUEST_PROMPT:
				timeoutPeriod = SMS_REQUEST_PROMPT_TIMEOUT;
				break;
			case SMS_SEND_PAYLOAD:
				timeoutPeriod = SMS_SEND_PAYLOAD_TIMEOUT;
				break;
			default:
				timeoutPeriod = DEFAULTCOMMANDTIMEOUT;
				break;
		}
			
		call CommandTimer.start(TIMER_ONE_SHOT,timeoutPeriod);
		atomic status = call GsmUartHandler.sendCommand((char*)CURRENT_COMMAND,
																						strlen((char*)CURRENT_COMMAND));
		if(status == FAIL)
		{
			atomic currentCmdState = NONE;
			atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
		}

		return status;
	}

	default event result_t GSM_Modem.setCellMonitorReportValueDone(gsm_error_t result){return SUCCESS;}
	default event result_t GSM_Modem.cellMonitorReportReady(gsm_error_t result, gsm_cellmon_data_t *data){return SUCCESS;}
	default event result_t GSM_Modem.signalQualityReady(gsm_error_t result, gsm_signal_quality_data_t *data){return SUCCESS;}
	default event result_t GSM_Modem.sendSMSDone(gsm_error_t result, uint8_t *endpoint, uint8_t *data){return SUCCESS;}

	inline void transition()
	{
		result_t status;
		uint8_t localState;
		atomic localState = currentCmdState;
		switch(localState)
		{
			case GSM_START_READY:
				//SODbg(1,"GSM_VCC_ENABLE\n");
				//change state enable the ground line
				atomic currentCmdState = GSM_VCC_ENABLE;
				enableGroundLine();
				call Timer.start(TIMER_ONE_SHOT,50);
				break;
			case GSM_VCC_ENABLE:
				//SODbg(1,"GSM PHASE1\n");
				atomic currentCmdState = GSM_POWER_ON_PHASE1;
				enablePowerLine();
				//enable line for a second
				call Timer.start(TIMER_ONE_SHOT,2000);
				break;
			case GSM_POWER_ON_PHASE1:
				//SODbg(1,"GSM PHASE2\n");
				atomic currentCmdState = GSM_POWER_ON_PHASE2;
				//hack
				//atomic currentCmdState = GSM_SET_BAUD_RATE;
				disablePowerLine();
				//set on bit
				//SODbg(1,"Starting commands\n");
				//start setting configurating the device
				call Timer.start(TIMER_ONE_SHOT,5000);
				break;
			case GSM_POWER_ON_PHASE2:
				//SODbg(1,"GSM SET BAUD\n");
				//TODO this is skipped
				atomic currentCmdState = GSM_SET_BAUD_RATE;
				//leave it up to GsmUartHandler Events or GSM_Modem Commands to start the system timer from here on
				//leave it up to method calls to start CommandTimer
				atomic CURRENT_COMMAND = SET_BAUD_RATE_CMD;
				status = sendCurrentCommand();
				if(status == FAIL)
				{
					//SODbg(1,"GSM SET BAUD FAILED\n");
					//shutdown procedure...
					atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
					post startFailTask();
				}
				break;
			case GSM_SET_BAUD_RATE:
				//SODbg(1,"GSM SET REGISTER\n");
				atomic currentCmdState = GSM_SET_REGISTER_WITH_EXTRA_INFO;
				atomic CURRENT_COMMAND = SET_REGISTER_MORE_INFO_CMD;
				status = sendCurrentCommand();
				if(status == FAIL)
				{
					//shutdown procedure...
					atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
					post startFailTask();
				}
				break;
			case GSM_SET_REGISTER_WITH_EXTRA_INFO:
				//SODbg(1,"GSM VERBOSE\n");
				atomic currentCmdState = GSM_SET_VERBOSE_MODE;
				atomic CURRENT_COMMAND = SET_VERBOSE_MODE_CMD;
				status = sendCurrentCommand();
				if(status == FAIL)
				{
					//shutdown procedure...
					atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
					post startFailTask();
				}
				break;
			case GSM_SET_VERBOSE_MODE:
				//SODbg(1,"GSM NORTH AMERICA BAND\n");
				atomic currentCmdState = GSM_SET_NORTH_AMERICA_BAND;
				atomic CURRENT_COMMAND = SET_NORTH_AMERICA_BAND_CMD;
				status = sendCurrentCommand();
				if(status == FAIL)
				{
					//shutdown procedure...
					atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
					post startFailTask();
				}
				break;
			case GSM_SET_NORTH_AMERICA_BAND:
				//SODbg(1,"SET ENERGY MODE\n");
				atomic currentCmdState = GSM_SET_ENERGY_SAVE_MODE;
				atomic CURRENT_COMMAND = SET_ENERGY_SAVE_CMD;
				status = sendCurrentCommand();
				if(status == FAIL)
				{
					//shutdown procedure...
					atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
					post startFailTask();
				}
				break;
			case GSM_SET_ENERGY_SAVE_MODE:
				//SODbg(1,"SET TEXT FORMAT\n");
				atomic currentCmdState = GSM_SET_TEXT_FORMAT;
				atomic CURRENT_COMMAND = SET_TEXT_FORMAT_CMD;
				status = sendCurrentCommand();
				if(status == FAIL)
				{
					//shutdown procedure...
					atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
					post startFailTask();
				}
				break;
			case GSM_SET_TEXT_FORMAT:
				atomic currentCmdState = GSM_CHECK_IF_ASSOC;
				atomic CURRENT_COMMAND = CHECK_IF_ASSOC_CMD;
				status = sendCurrentCommand();
				if(status == FAIL)
				{
					atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
					post startFailTask();
				}
				break;
			case GSM_CHECK_IF_ASSOC:
				//SODbg(1,"WAITING FOR ASSOC\n");
				//done here wait for NetworkRegistrationReceived event for association status...
				//we signal startDone() event upon successful association w/ network
				post gsmConfigurationDoneTask();
				break;
			default:
				break;
		}
	}

	event result_t Timer.fired()
	{
		//SODbg(1,"GSMDRIVERM Timer fired\n");
		transition();
		return SUCCESS;
	}

	//if this timer fires we retry sending the previous command...and increment the count
	event result_t CommandTimer.fired()
	{
		uint8_t localState;
		bool triedMaxNumTimes;
		atomic localState = currentCmdState;
		atomic triedMaxNumTimes = (commandRetryCount >= NUMBEROFCOMMANDRETRY);
		
		if(triedMaxNumTimes)
		{
			switch(localState)
			{
				case GSM_SET_CELL_MONITOR_RPT_VAL:
					atomic gsm_error_code = NO_MODEM_RESPONSE;
					post setCellMonitorReportValueFailTask();
					break;
				case GSM_GET_CELL_MONITOR_RPT:
					atomic gsm_error_code = NO_MODEM_RESPONSE;
					post cellMonitorReportFailTask();
					break;
				case GSM_GET_SIGNAL_QUALITY:
					atomic gsm_error_code = NO_MODEM_RESPONSE;
					post signalQualityDataFailTask();
					break;
				case SMS_REQUEST_PROMPT:
					atomic gsm_error_code = SMS_PROMPT_NOT_RECVD;
					post sendSMSFailTask();
					break;
				case SMS_SEND_PAYLOAD:
					atomic gsm_error_code = NO_MODEM_RESPONSE;
					post sendSMSFailTask();
					break;
				default:
					//Configuration failed
					atomic gsm_error_code = NO_MODEM_RESPONSE;
					post startFailTask();
					break;
			}
		}
		else
		{
			result_t status;
			//increment then send previous command
			atomic commandRetryCount++;
			//send the previous command...
			status = sendCurrentCommand();
			if(status == FAIL)
			{
				call CommandTimer.stop();
				atomic gsm_error_code = GSM_UART_HANDLER_SEND_CMD_FAIL;
				atomic currentCmdState = NONE;
				switch(localState)
				{
					case GSM_SET_CELL_MONITOR_RPT_VAL:
						post setCellMonitorReportValueFailTask();
						break;
					case GSM_GET_CELL_MONITOR_RPT:
						post cellMonitorReportFailTask();
						break;
					case GSM_GET_SIGNAL_QUALITY:
						post signalQualityDataFailTask();
						break;
					case SMS_REQUEST_PROMPT:
						post sendSMSFailTask();
						break;
					case SMS_SEND_PAYLOAD:
						post sendSMSFailTask();
						break;
					default:
						//Configuration failed
						post startFailTask();
						break;
				}
			}
		}
		return SUCCESS;
	}
	
	//if this timer fires we notify upper layer that this component shutdown...
	event result_t WatchdogTimer.fired()
	{
		//SODbg(1,"Watchdog timer fired\n");
		//we quit here...
		atomic gsm_error_code = WATCHDOG_TIMED_OUT;
		post stopDoneTask();
		return SUCCESS;
	}
	
	async event result_t GsmUartHandler.SendCommandDone(uint8_t *cmd, result_t result)
	{
		//SODbg(1,"GsmUartHandler.SendCommandDone rval:%i\n",result);
		return SUCCESS;
	}
	
	async event void GsmUartHandler.OkReceived()
	{
		post OkReceivedTask();
		return;
	}
	
	async event void GsmUartHandler.ErrorReceived(gsm_error_t errorCode)
	{
		atomic gsm_error_code = errorCode;
		post ErrorReceivedTask();
		return;
	}
	
	async event void GsmUartHandler.NetworkRegistrationReceived(gsm_cellid_areacode_data_t*ptr, uint8_t dataLength)
	{
		memcpy(&currentAssocData,ptr,sizeof(currentAssocData));
		post NetworkRegistrationReceivedTask();
		return;
	}
	
	async event void GsmUartHandler.CellMonitorReportReceived(gsm_cellmon_data_t*ptr,uint16_t dataLength)
	{
		memcpy(&currentCellMonitorReportData, ptr,sizeof(currentCellMonitorReportData));
		call CommandTimer.stop();
		atomic commandRetryCount = 0;
		post cellMonitorReportReadyTask();
		return;
	}
	
	async event void GsmUartHandler.SignalQualityReceived(gsm_signal_quality_data_t*ptr,uint8_t dataLength)
	{
		memcpy(&currentSignalQualityData, ptr, sizeof(currentSignalQualityData));
		call CommandTimer.stop();
		atomic commandRetryCount = 0;
		post signalQualityDataReadyTask();
		return;
	}
	
	async event void GsmUartHandler.PromptReceived()
	{
		if(currentCmdState == SMS_REQUEST_PROMPT)
		{
			result_t status;
			call CommandTimer.stop();
			atomic currentCmdState = SMS_SEND_PAYLOAD;
			strcpy((char*)SMS_SEND_DATA_BUFFER,(char*)currentTextData);
			strcat((char*)SMS_SEND_DATA_BUFFER,"\x1A");
			
			atomic CURRENT_COMMAND = SMS_SEND_DATA_BUFFER;
			call CommandTimer.start(TIMER_ONE_SHOT,SMS_SEND_PAYLOAD_TIMEOUT);
			//Don't care about status. If we fail to send text with Ctrl+Z
			//appended, we could freeze GSM in '>' input mode
			//If that happens, we want to resend text payload to try to
			//end '>' input mode with the Ctrl+Z character
			atomic status = call GsmUartHandler.sendCommand((char*)SMS_SEND_DATA_BUFFER,
																			strlen((char*)SMS_SEND_DATA_BUFFER));
		}
		else
		{
			return;
		}
	}
	
	task void OkReceivedTask()
	{
		uint8_t lState;
		//SODbg(1,"OkReceived\n");
		atomic lState = currentCmdState;
		switch(lState)
		{
			case GSM_SET_CELL_MONITOR_RPT_VAL:
				call CommandTimer.stop();
				post setCellMonitorReportValueDoneTask();
				break;
			case GSM_GET_CELL_MONITOR_RPT:
			case GSM_GET_SIGNAL_QUALITY:
			case SMS_REQUEST_PROMPT:
				break;
			case SMS_SEND_PAYLOAD:
				call CommandTimer.stop();
				atomic commandRetryCount = 0;
				post sendSMSDoneTask();
				break;
			case GSM_CHECK_IF_ASSOC:
				break;
			default:
				call CommandTimer.stop();
				atomic commandRetryCount = 0;
				call Timer.start(TIMER_ONE_SHOT,500);
				break;
		}
	}
	
	
	
	task void ErrorReceivedTask()
	{
		uint8_t lState;
		//SODbg(1,"ErrorReceived\n");
		atomic lState = currentCmdState;
		switch(lState)
		{
			case GSM_SET_CELL_MONITOR_RPT_VAL:
				call CommandTimer.stop();
				post setCellMonitorReportValueFailTask();
				break;
			case GSM_GET_CELL_MONITOR_RPT:
				call CommandTimer.stop();
				post cellMonitorReportFailTask();
				break;
			case GSM_GET_SIGNAL_QUALITY:
				call CommandTimer.stop();
				post signalQualityDataFailTask();
				break;
			case SMS_REQUEST_PROMPT:
			case SMS_SEND_PAYLOAD:
				call CommandTimer.stop();
				post sendSMSFailTask();
				break;
			default:
				break;
		}
	}
	
	task void NetworkRegistrationReceivedTask()
	{
		bool wasTriggeredByConfigRoutine; 
		atomic wasTriggeredByConfigRoutine = (currentCmdState == GSM_CHECK_IF_ASSOC);
		if(wasTriggeredByConfigRoutine) {
			call CommandTimer.stop();
			atomic commandRetryCount = 0;
			call Timer.start(TIMER_ONE_SHOT,500);
		}
		if( (currentAssocData.status == REGISTERED_HOME) || (currentAssocData.status == REGISTERED_ROAMING) )
		{
			isAssociated = TRUE;
			//SODbg(1,"isAssoc=TRUE\n");
			//If we received unsolicited assoc report and configuration routine is finished
			//then post startDone b/c we are configured and associated
			if(isConfigDone && !isStartDone)
			{
				isStartDone = TRUE;
				post startDoneTask();
			}
		}
		else
		{
			isAssociated = FALSE;
		}
	}
	
	task void setCellMonitorReportValueDoneTask()
	{
		atomic currentCmdState = NONE;
		atomic commandRetryCount = 0;
		signal GSM_Modem.setCellMonitorReportValueDone(NO_ERROR);
	}
	
	task void setCellMonitorReportValueFailTask()
	{
		atomic
		{
			currentCmdState = NONE;
			CURRENT_COMMAND = NULL;
			commandRetryCount = 0;
			signal GSM_Modem.setCellMonitorReportValueDone(gsm_error_code);
		}
	}
	
	task void cellMonitorReportReadyTask()
	{
		//SODbg(1,"CellMonReceived\n");
		atomic currentCmdState = NONE;
		signal GSM_Modem.cellMonitorReportReady(NO_ERROR,&currentCellMonitorReportData);
	}
	
	task void cellMonitorReportFailTask()
	{
		atomic
		{
			currentCmdState = NONE;
			CURRENT_COMMAND = NULL;
			commandRetryCount = 0;
			signal GSM_Modem.cellMonitorReportReady(gsm_error_code,NULL);
		}
	}
	
	task void signalQualityDataReadyTask()
	{
		atomic currentCmdState = NONE;
		signal GSM_Modem.signalQualityReady(NO_ERROR,&currentSignalQualityData);
	}
	
	task void signalQualityDataFailTask()
	{
		atomic
		{
			currentCmdState = NONE;
			CURRENT_COMMAND = NULL;
			commandRetryCount = 0;
			signal GSM_Modem.signalQualityReady(gsm_error_code,NULL);
		}
	}
	
	task void sendSMSDoneTask()
	{
		atomic currentCmdState = NONE;
		signal GSM_Modem.sendSMSDone(NO_ERROR,currentPhoneNumber,currentTextData);
	}
	
	task void sendSMSFailTask()
	{
		atomic
		{
			currentCmdState = NONE;
			CURRENT_COMMAND = NULL;
			commandRetryCount = 0;
			signal GSM_Modem.sendSMSDone(gsm_error_code,currentPhoneNumber,currentTextData);
		}
	}
	
}

configuration GpsDriver
{
  provides
	{
		interface SplitControl as GpsSplitControl;
		interface ReceiveMsg as GpsGGARecv;
		interface ReceiveMsg as GpsRMCRecv;
	}
}
implementation
{
  components GpsDriverM, UARTGpsPacket;
  
  GpsSplitControl = GpsDriverM.GpsSplitControl;  
	GpsGGARecv = GpsDriverM.GpsGGARecv;
	GpsRMCRecv = GpsDriverM.GpsRMCRecv;
  
  GpsDriverM.GpsControl -> UARTGpsPacket.Control;
  GpsDriverM.SendVarLenPacket -> UARTGpsPacket.SendVarLenPacket;
  GpsDriverM.Receive -> UARTGpsPacket.Receive;
}


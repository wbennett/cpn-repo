configuration CompassC
{
	provides
	{
		interface Compass;
		interface SplitControl;
	}
}
implementation
{
	components CompassM, HMC6343C, TimerC;

	Compass = CompassM;
	SplitControl = CompassM.SplitControl;
	
	CompassM.HMC6343 -> HMC6343C.HMC6343;
	CompassM.TimerControl -> TimerC.StdControl;
	CompassM.CompassControl -> HMC6343C.SplitControl;
	CompassM.CalibrateTimer -> TimerC.Timer[unique("Timer")];

}

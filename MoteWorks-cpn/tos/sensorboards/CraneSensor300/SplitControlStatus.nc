/**
 * author: Collin Lutz
 * note: this interface provides the events and commands required for
 * a SplitControl interface that returns a status/error parameter
 * with the initDone(), startDone(), and stopDone() events
 *
 */

#include "Tracker/gsm.h"

interface SplitControlStatus
{
  /**
   * Initialize the component and its subcomponents.
   *
   * @return Whether initialization was successful.
   */
  command result_t init();

    /** 
     * Notify components that the component has been init
     *
     */
    event result_t initDone();

  /**
   * Start the component and its subcomponents.
   *
   * @return Whether starting was successful.
   */
  command result_t start();

    /** 
     * Notify components that the component has been started and is ready to
     * receive other commands
     *
     */

    event result_t startDone(gsm_error_t result);

  /**
   * Stop the component and pertinent subcomponents (not all
   * subcomponents may be turned off due to wakeup timers, etc.).
   *
   * @return Whether stopping was successful.
   */
  command result_t stop();

    /**
     * Notify components that the component has been stopped. 
     */

    event result_t stopDone(gsm_error_t result);
}

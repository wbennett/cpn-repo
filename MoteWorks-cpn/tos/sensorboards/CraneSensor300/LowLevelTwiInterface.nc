interface LowLevelTwiInterface {

  command void twi_init();
  
  command uint8_t twi_readFrom(uint8_t address, uint8_t* data, uint8_t length);
  
  command uint8_t twi_writeTo(uint8_t address, uint8_t* data, uint8_t length, uint8_t wait);
  
}


/**
 * File: GsmUartHandlerC.nc
 * Version: 0.00.1
 * Note: This file is the wiring between upper layers and layers that below the GsmUartHandler
 */ 
#include "Tracker/gsm.h"
configuration GsmUartHandlerC
{
	provides interface StdControl;
	provides interface GsmUartHandlerI as GsmUartHandler;
}
implementation
{
	//Leds is for debugging only 
	components GsmUartHandlerM, UARTGsmPacket, LedsC;

	//wire the upper interfaces
	GsmUartHandler = GsmUartHandlerM.GsmUartHandler;
	StdControl = GsmUartHandlerM.StdControl;

	//wire the UART components
	GsmUartHandlerM.UARTControl -> UARTGsmPacket.Control;
	GsmUartHandlerM.Receive -> UARTGsmPacket.Receive;
	GsmUartHandlerM.SendVarLenPacketGsm -> UARTGsmPacket.SendVarLenPacket;
	GsmUartHandlerM.Leds -> LedsC.Leds;
}

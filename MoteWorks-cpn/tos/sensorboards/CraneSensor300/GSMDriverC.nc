/*
 *
 *
 *
 */
#include "Tracker/gsm.h"
configuration GSMDriverC
{
	provides interface SplitControlStatus;
	provides interface GSM_ModemI as GSM_Modem;
}
implementation
{
	components GSMDriverM, GsmUartHandlerC, TimerC, LedsC;

	GSM_Modem = GSMDriverM.GSM_Modem;
	SplitControlStatus = GSMDriverM.SplitControlStatus;
	
	GSMDriverM.GsmUartHandler -> GsmUartHandlerC.GsmUartHandler;
	GSMDriverM.UARTControl -> GsmUartHandlerC.StdControl;

	GSMDriverM.Timer -> TimerC.Timer[unique("Timer")];
	GSMDriverM.CommandTimer -> TimerC.Timer[unique("Timer")];
	GSMDriverM.WatchdogTimer -> TimerC.Timer[unique("Timer")];
	GSMDriverM.Leds -> LedsC.Leds;

}

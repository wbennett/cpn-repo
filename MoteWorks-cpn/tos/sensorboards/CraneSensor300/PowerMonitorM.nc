/*
 *
 *
 *
 *
 *
 *
 */
#include "debug_constants.h"
#include "PowerMonitor/types.h"
#include "adc_mappings.h"
module PowerMonitorM {
	#ifdef I2CDBG
	uses {
	I2CDBG_CONFIG();	
	}
	#endif
	provides interface StdControl;
	provides interface PowerSample[uint8_t id];
	uses interface ADCControl;
	uses interface ADC as BatteryVoltage;
	uses interface ADC as BatteryCurrent;
	uses interface ADC as SolarVoltage;
	uses interface ADC as SolarCurrent;
} implementation {
	
	power_monitor_t _current_sample;
	uint8_t _state;
	uint8_t _current_id;
	bool _is_busy;
	bool _is_initd = FALSE;
	enum {
		S_INIT,
		S_SAMP_BATTV,
		S_SAMP_BATTI,
		S_SAMP_SOLARV,
		S_SAMP_SOLARI
	};
	task void transition_task();
	void transition();

	command result_t StdControl.init() {
		//map the logical ports to the physical ports...tinyos is dumb
		//also only init once...don't know the effects of binding and initing twice 
		if(_is_initd == FALSE) {
			call ADCControl.bindPort(BATTERY_VOLTAGE_ADC_PORT, ACTUAL_BATTERY_VOLTAGE_ADC_PORT);
			call ADCControl.bindPort(BATTERY_CURRENT_ADC_PORT, ACTUAL_BATTERY_CURRENT_ADC_PORT);
			call ADCControl.bindPort(SOLAR_VOLTAGE_ADC_PORT,ACTUAL_SOLAR_VOLTAGE_ADC_PORT);
			call ADCControl.bindPort(SOLAR_CURRENT_ADC_PORT,ACTUAL_SOLAR_CURRENT_ADC_PORT);
			call ADCControl.init();
			_is_initd = TRUE;
		}
		return SUCCESS;
	}
	command result_t StdControl.start() {
		return SUCCESS;
	}
	command result_t StdControl.stop() {
		return SUCCESS;
	}
	
	void transition() {
		uint8_t lstate;
		power_monitor_t t_sample;
		atomic lstate = _state;
		switch(lstate) {
			case S_INIT:
				atomic _state = S_SAMP_BATTV;
				call BatteryVoltage.getData();
				break;
			case S_SAMP_BATTV:
				atomic _state = S_SAMP_BATTI;
				call BatteryCurrent.getData();
				break;
			case S_SAMP_BATTI:
				atomic _state = S_SAMP_SOLARV;
				call SolarVoltage.getData();
				break;
			case S_SAMP_SOLARV:
				atomic _state = S_SAMP_SOLARI;
				call SolarCurrent.getData();
				break;
			case S_SAMP_SOLARI:
				//signal the readings are ready
				atomic t_sample = _current_sample;
				signal PowerSample.dataReady[_current_id](t_sample);
				atomic _is_busy = FALSE;
				break;
			default:
				break;
		}
	}

	task void transition_task() {
		transition();
	}
	
	/*
	 * Called when lines are to be sampled
	 */
	command result_t PowerSample.getData[uint8_t id]() {
		//check to see if we are currently busy
		bool l_is_busy;
		l_is_busy = _is_busy;
		if(l_is_busy == TRUE) {
			return FAIL;
		}
		_is_busy = TRUE;
		_current_id = id;
		//set to the init state
		_state = S_INIT;
		//post task to start sampling
		post transition_task();
		return SUCCESS;
	}

	async event result_t BatteryVoltage.dataReady(uint16_t data) {
		//store the value and then transition
		atomic _current_sample.BattV = data;
		//dprintf("bv:%i\n",data);
		post transition_task();
		return SUCCESS;
	}

	async event result_t BatteryCurrent.dataReady(uint16_t data) {
		//store the value and then transition
		atomic _current_sample.BattI = data;
		//dprintf("bi:%i\n",data);
		post transition_task();
		return SUCCESS;
	}

	async event result_t SolarVoltage.dataReady(uint16_t data) {
		//store the value and then transition
		atomic _current_sample.SolarV = data;

		//dprintf("sv:%i\n",data);
		post transition_task();
		return SUCCESS;
	}

	async event result_t SolarCurrent.dataReady(uint16_t data) {
		//store the value and then transition
		atomic _current_sample.SolarI = data;
		//dprintf("si:%i\n",data);
		post transition_task();
		return SUCCESS;
	}
	/*
	 * Performed before the other components get the data.
	 */
	default event result_t PowerSample.dataReady[uint8_t id](power_monitor_t data) {
		return SUCCESS;
	}

}

module HighLevelTwiM {
  provides {
    interface HighLevelTwiInterface as HighLevelTwiInterface;
  }
  uses {
	  interface LowLevelTwiInterface as LowLevelTwiInterface;
  }
}
implementation {

	#define DBG_VERBOSE (0)
	#define BUFFER_LENGTH 32
	//#include "SOdebug0.h"
	
	static uint8_t rxBuffer[BUFFER_LENGTH];
	static uint8_t rxBufferIndex = 0;
	static uint8_t rxBufferLength = 0;
	
	static uint8_t txAddress;
	static uint8_t txBuffer[BUFFER_LENGTH];
	static uint8_t txBufferIndex = 0;
	static uint8_t txBufferLength = 0;
	
	static uint8_t transmitting;
	
	command void HighLevelTwiInterface.begin()
	{
		rxBufferIndex = 0;
		rxBufferLength = 0;
	
		txBufferIndex = 0;
		txBufferLength = 0;
		
		call LowLevelTwiInterface.twi_init();
		//SODbg(DBG_VERBOSE,"After low level twi interface\n");
	}
	
	command uint8_t HighLevelTwiInterface.requestFrom(uint8_t address, uint8_t quantity)
	{
	  uint8_t read;
		// clamp to buffer length
		if(quantity > BUFFER_LENGTH){
			quantity = BUFFER_LENGTH;
		}
		// perform blocking read into buffer
		read = call LowLevelTwiInterface.twi_readFrom(address, rxBuffer, quantity);
		// set rx buffer iterator vars
		rxBufferIndex = 0;
		rxBufferLength = read;
	
		return read;
	}
	
	command void HighLevelTwiInterface.beginTransmission(uint8_t address)
	{
		// indicate that we are transmitting
		transmitting = 1;
		// set address of targeted slave
		txAddress = address;
		// reset tx buffer iterator vars
		txBufferIndex = 0;
		txBufferLength = 0;
	}
	
	command uint8_t HighLevelTwiInterface.endTransmission()
	{
		// transmit buffer (blocking)
		int8_t ret = call LowLevelTwiInterface.twi_writeTo(txAddress, txBuffer, txBufferLength, 1);
		// reset tx buffer iterator vars
		txBufferIndex = 0;
		txBufferLength = 0;
		// indicate that we are done transmitting
		transmitting = 0;
		return ret;
	}
	
	// must be called in:
	// slave tx event callback
	// or after beginTransmission(address)
	command void HighLevelTwiInterface.send(uint8_t data)
	{
		if(transmitting){
		// in master transmitter mode
			// don't bother if buffer is full
			if(txBufferLength >= BUFFER_LENGTH){
				return;
			}
			// put byte in tx buffer
			txBuffer[txBufferIndex] = data;
			++txBufferIndex;
			// update amount in buffer   
			txBufferLength = txBufferIndex;
		}
	}
	
	// must be called in:
	// slave rx event callback
	// or after requestFrom(address, numBytes)
	command uint8_t HighLevelTwiInterface.available()
	{
		return rxBufferLength - rxBufferIndex;
	}
	
	// must be called in:
	// slave rx event callback
	// or after requestFrom(address, numBytes)
	command uint8_t HighLevelTwiInterface.receive()
	{
		// default to returning null char
		// for people using with char strings
		uint8_t value = '\0';
		
		// get each successive byte on each call
		if(rxBufferIndex < rxBufferLength){
			value = rxBuffer[rxBufferIndex];
			++rxBufferIndex;
		}
	
		return value;
	}
}
	

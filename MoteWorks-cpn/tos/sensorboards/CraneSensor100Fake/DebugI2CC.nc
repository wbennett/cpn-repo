configuration DebugI2CC
{
	provides
	{
		interface StdControl;
		command result_t i2cPrint(const char* str);
	}
}
implementation
{
	components DebugI2CM, HighLevelTwi;

	StdControl = DebugI2CM.StdControl;
	i2cPrint = DebugI2CM.i2cPrint;

	DebugI2CM.HighLevelTwiInterface -> HighLevelTwi.HighLevelTwiInterface;

}

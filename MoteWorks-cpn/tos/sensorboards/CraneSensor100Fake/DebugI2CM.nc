
module DebugI2CM
{
	provides
	{
		interface StdControl;
		command result_t i2cPrint(const char* str);
	}
	uses
	{
		interface HighLevelTwiInterface;
	}
}

implementation
{
	#include "twi.h"
	#define DEBUG_ADDR (42 >> 1)

	command result_t StdControl.init()
	{
		return SUCCESS;
	}

	command result_t StdControl.start()
	{
		call HighLevelTwiInterface.begin();
		return SUCCESS;
	}

	command result_t StdControl.stop()
	{
		return SUCCESS;
	}
	
	command result_t i2cPrint(const char* str) {
		uint16_t i=0;
		uint16_t k=0;
		
		/** TWI_BUFFER is only 32 bytes so we have to split printed ***
		*** debug message into 32 byte chunks                       **/
		while(k<strlen(str)) {
			call HighLevelTwiInterface.beginTransmission(DEBUG_ADDR);
			for(i=0; (k<strlen(str) && i<TWI_BUFFER_LENGTH); i++) {
				call HighLevelTwiInterface.send(str[k]);
				k++;
			}
			call HighLevelTwiInterface.endTransmission();
		}
		return SUCCESS;
	}

}

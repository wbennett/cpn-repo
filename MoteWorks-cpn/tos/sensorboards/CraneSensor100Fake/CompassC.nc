configuration CompassC
{
	provides
	{
		interface Compass;
		interface SplitControl;
	}
}
implementation
{
	components CompassM;

	Compass = CompassM;
	SplitControl = CompassM.SplitControl;

}

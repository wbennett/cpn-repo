interface HighLevelTwiInterface {

  command void begin();
	
	command uint8_t requestFrom(uint8_t address, uint8_t quantity);
	
	command void beginTransmission(uint8_t address);
	
	command uint8_t endTransmission();
	
	command void send(uint8_t data);
	
	command uint8_t available();
	
	command uint8_t receive();
  
}


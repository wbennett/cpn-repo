/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: XSensor.h,v 1.1.2.3 2007/06/11 18:25:54 mturon Exp $
 */ 
 
/**
 * Global data structures for all sensor boards
 *
 * @author Xin Yang
 * @author Martin Turon
 */

 
#ifndef __XSENSOR__
#define __XSENSOR__
 
/*=== General Params ======================================================*/

#define MAX_SENSOR_PAYLOAD_SIZE 44  //derived from max gps pkt size

 
/*=== BOARD ID ============================================================*/

typedef enum {
  // surge packet
  SENSOR_BOARD_ID_SURGE = 0x00,

  // mica2dot sensorboards 
  SENSOR_BOARD_ID_MDA500 = 0x01,   
  SENSOR_BOARD_ID_MTS510,
  SENSOR_BOARD_ID_MEP500,
  SENSOR_BOARD_ID_MEP510,

  SENSOR_BOARD_ID_HEALTH = 0x10,
  
  SENSOR_BOARD_ID_TELOSB = 0x40,

  // mote boards
  SENSOR_BOARD_ID_MICA2 = 0x60,
  SENSOR_BOARD_ID_MICA2DOT,
  SENSOR_BOARD_ID_MICAZ,
  
  // mica2 sensorboards 
  SENSOR_BOARD_ID_MDA400 = 0x80,   
  SENSOR_BOARD_ID_MDA300,
  SENSOR_BOARD_ID_MTS101,
  SENSOR_BOARD_ID_MTS300,
  SENSOR_BOARD_ID_MTS310,
  SENSOR_BOARD_ID_MTS400,
  SENSOR_BOARD_ID_MTS420,
  SENSOR_BOARD_ID_MEP401,
  SENSOR_BOARD_ID_XTUTORIAL = 0x88,
  SENSOR_BOARD_ID_GGBACLTST,
  SENSOR_BOARD_ID_MEP410,
  
  SENSOR_BOARD_ID_MDA320 = 0x90,
  SENSOR_BOARD_ID_MDA100, 
  SENSOR_BOARD_ID_MTS450,
  SENSOR_BOARD_ID_MDA325 = 0x93,
    
  // mica2 integrated boards
  SENSOR_BOARD_ID_MSP410 = 0xA0,
  SENSOR_BOARD_ID_MTP400 = 0xA1,  
  SENSOR_BOARD_ID_MTS410 = 0xA2,
  SENSOR_BOARD_ID_MDA300_S3C= 0xA3

} XSensorBoardId;


/*=== XMesh APP ID ========================================================*/

enum {
    AM_XSENSOR_MSG   = 50,
    AM_XMULTIHOP_MSG = 51,         // xsensor multihop 
};

/*=== UART AM_TYPE ========================================================*/

enum {
    AM_XDEBUG_MSG    = 49,  
};

/*=== Sensor Packet =======================================================*/

/**
 *  Header
 */
typedef struct XSensorHeader{
  uint8_t  board_id;
  uint8_t  packet_id;
  uint16_t parent;
} __attribute__ ((packed)) XSensorHeader;

/**
 *  Packet
 */
typedef struct XSensorPacket {
  XSensorHeader header;
  char sensorPayload[MAX_SENSOR_PAYLOAD_SIZE];
  
} __attribute__ ((packed)) XSensorPacket;
 
  
#endif

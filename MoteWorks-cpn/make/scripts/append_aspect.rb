#!/usr/bin/ruby
symbol = '__STARTASPECT__'
outfile = File.open("#{ARGV[0]}",'a');
for i in (1..ARGV.length-1) do 
	isAppend = false;
	puts "Appending file....#{ARGV[i]}"
	File.open("#{ARGV[i]}",'r') do |infile|
		while(line = infile.gets)
			#if we are ready to append start
			if(isAppend == true)
				outfile.puts(line)
			end
			#check for symbol and that we haven't started
			if(line.include?(symbol) && isAppend == false)
				isAppend = true
			end
		end
	end
	puts "Done..."
end


/*
 *
 *
 *
 *
 *
 */

#include "debug_constants.h"
#include "PowerMonitor/types.h"
module GsmManagerM
	{
	provides
		{
		interface TrackerControl;
		}
	uses
		{
		#ifdef I2CDBG
		I2CDBG_CONFIG();	
		#endif
		/* Timers for control flow */
		interface Timer					as FailSafeTimer;
		interface Timer					as CommandTimer;
		
		/* Storage */
		interface FlashStorageRead		as Flash;
		
		/* GSM Control */
		interface GSM_ModemI			as GSM_Modem;
		interface SplitControlStatus	as GSMSplitControlStatus;
		
		/* Voltage Monitoring */
		//interface ADC 					as ADCBATT;
		//interface StdControl			as BattControl;
		interface StdControl as PowerMonControl;
		interface PowerSample;
		}
	}
	
implementation 
	{
	/****************************************************************
	*	Includes
	*****************************************************************/
	#include "Tracker/gsm.h"
	#include "Tracker/storage.h"

	/****************************************************************
	*	Defines
	*****************************************************************/
	#define CMND_CELL_MON_TIME	(205u)				/* How long to give the cell monitor command		*/
	#define CMND_SEND_SMS_TIME	(63488u)				/* How long to try and send an SMS	page 22 AT_COMMANDS_REF_GUIDE	60 seconds + 2 second padding			*/
	#define CMND_SIG_QUAL_TIME	(205u)				/* How long to give the signal quality command		*/
	#define NUMBER_PHONES		((uint8_t)1)		/* How many phones to call							*/
	//Voltage GSM min this value was tested using a multimeter for
	//the current to voltage circuit.
	#ifndef VOLTAGE_GSM_MIN_PARAM
	#define VOLTAGE_GSM_MIN_PARAM (200)
	#endif
	#define VOLTAGE_GSM_MIN		(VOLTAGE_GSM_MIN_PARAM)				/* 3400 mV											*/

	/****************************************************************
	*	Enums
	*****************************************************************/
	enum
		{
		GM_NONE,				/* what gm_state is initialized to (does nothing)										*/
		GM_START_COMPONENTS,	/* starts components (gsm, flash) (GM_SHUTDOWN if either fail) goes to GM_START_DONE	*/
		GM_START_DONE,			/* when startdones are received goes to GM_GET_SIG_QUAL									*/
		GM_GET_SIG_QUAL,		/* sends getSignalQuality command goes to GM_GET_CELL_MON after command succeeds.		*/
		GM_GET_CELL_MON,		/* sends getCellMonitorReport command. Goes to GM_QUEUE_MESSAGE after command succeeds.	*/
		GM_READ,				/* reads data from flash																*/
		GM_BUILD_MESSAGE,		/* parses the data from flash and forms the SMS messages to send						*/
		GM_SEND_MESSAGE,		/* attempts to send message all the numbers in the gm_phone_book						*/
		GM_ADVANCE_READ,		/* advance the read pointer																*/
		GM_SHUTDOWN				/* turns everything off																	*/
		};

	/****************************************************************
	*	Static Variables
	*****************************************************************/
	uint8_t	gm_state;				/* Used for high level control flow			*/
	uint8_t	gm_phone_nmbr_idx;		/* Which phone number we are sending to		*/
	uint8_t	gm_number_of_records;	/* How many records we got from flash		*/

	uint8_t						gm_sms_buffer[MAX_SMS_SIZE + 1];	/* What we send.  Extra char is for terminator	*/
	//gsm_header_t				gsmHeader;							/* Storage for the header						*/
	generic_record_t			gm_record_buffer[MAX_NUM_RECORDS];	/* Storage for flash data						*/
	sms_packet_t gsm_packet;
	gsm_cellmon_data_t			gm_gsm_cellmon_data;				/* Cell monitor data storage					*/
	gsm_signal_quality_data_t	gm_gsm_signal_data;					/* Signal quality data							*/
	uint32_t					gsm_run_time;						/* How long to run for							*/

	/* Where we want to send the data */
	uint8_t **gm_phone_book;
	uint8_t *gm_phone_numbers[1];
	uint8_t *GOOGLE	= (uint8_t*)"+19092765228";
//	uint8_t *GOOGLE = (uint8_t*)"+14026795568";
	
	/* The current system voltage level */
	power_monitor_t power;

	//uint16_t	voltage;
	bool		have_adc_control;

	/* sequence number for communication order */
	uint8_t gsm_seqnum;
	/****************************************************************
	*	Function Prototypes
	*****************************************************************/
	void initPhoneBook();
	task void shutDown();
	task void stopDone();
	task void transition();

	/****************************************************************
	*
	*	ALL TASKS GO HERE
	*
	*****************************************************************/
	task void advanceFlash()
		{
		if(gm_state == GM_ADVANCE_READ)
			{
			call Flash.advanceRead(gm_number_of_records);
			}
		}

	/*	This tasks converts the raw data into a format suitable for
	*	transmission through the GSM.  Because there are several special
	*	control characters, we split every byte into a high and low nibble,
	*	put them in separate characters, and add 65.  This guarantees that 
	*	none of the control characters are ever sent.
	*/
	task void encodeMessage()
		{
		uint8_t i;
		uint8_t *bytePtr;
		//uint8_t endIndex;

		if(gm_state == GM_BUILD_MESSAGE)
			{
			/* Initialize the buffer */
			memset(gm_sms_buffer, 0, sizeof(gm_sms_buffer));
			
			/* Start packing in the GSM header */
			//gsmHeader.seqnum = gsm_seqnum++;
			//gsmHeader.signal_quality_data	= gm_gsm_signal_data;
			//gsmHeader.gsm_cellmon_record	= gm_gsm_cellmon_data.towers[0];		
			gsm_packet.header.seqnum = gsm_seqnum++;
			gsm_packet.header.signal_quality_data = gm_gsm_signal_data;
			gsm_packet.header.gsm_cellmon_record = gm_gsm_cellmon_data.towers[0];
			//copy in` buffer to be sent
			memcpy(gsm_packet.payload,gm_record_buffer,sizeof(gm_record_buffer));
			//bytePtr = (uint8_t *)&gsmHeader;
			bytePtr = (uint8_t *)&gsm_packet;
			
			for(i=0;i < sizeof(sms_packet_t)*2;i = i+2) {
				gm_sms_buffer[i] = (*bytePtr >> 4) + 65;
				gm_sms_buffer[i+1] = (*bytePtr & 0x0f) + 65;
				bytePtr++;
			}

			/*
			for(i = 0; i < sizeof(gsmHeader) * 2; i = i + 2)
				{
				gm_sms_buffer[i]		= (*bytePtr >> 4) + 65;
				gm_sms_buffer[i + 1]	= (*bytePtr & 0x0F) + 65;
				bytePtr++;
				}
			*/
			/* Now start packing the the actual record */
			/*
			bytePtr = (uint8_t *)gm_record_buffer;
			endIndex = (sizeof(gm_record_buffer) * 2) + i;
			
			for(; i < endIndex; i = i + 2)
				{
				gm_sms_buffer[i]		= (*bytePtr >> 4) + 65;
				gm_sms_buffer[i + 1]	= (*bytePtr & 0x0F) + 65;
				bytePtr++;
				}
			*/
			/* Null terminate the buffer and send it */
			gm_sms_buffer[i] = '\0';
			gm_state = GM_SEND_MESSAGE;
			post transition();
			}
		}

	/* This gets the cell monitor report from the GSM driver */
	task void getCellMonitorReport()
		{
		if(gm_state == GM_GET_CELL_MON)
			{
			call GSM_Modem.getCellMonitorReport();
			call CommandTimer.start(TIMER_ONE_SHOT, CMND_CELL_MON_TIME);
			}
		}

	/* Get the signal quality */
	task void getSignalQuality()
		{
		if(gm_state == GM_GET_SIG_QUAL)
			{
			call GSM_Modem.getSignalQuality();
			call CommandTimer.start(TIMER_ONE_SHOT, CMND_SIG_QUAL_TIME);
			}
		}

	/* Done initializing, signal upper layers we're done */
	task void initDone()
		{
		signal TrackerControl.initDone();
		}

	/* Get a record from flash */
	task void read()
		{
		if(gm_state == GM_READ)
			{
			/* Initialize how many records we got */
			gm_number_of_records	= 0;
			/*	Since we're getting new records, always start sending to the first
			*	phone number
			*/
			gm_phone_nmbr_idx		= 0;
			/* If we can't get more records, bail out */
			if(call Flash.getRecords(MAX_NUM_RECORDS, gm_record_buffer) != SUCCESS)
				{
				gm_state = GM_SHUTDOWN;
				post shutDown();
				}
			}
		}

	/* Send an encoded message */
	task void sendMessage()
		{
		if(gm_state == GM_SEND_MESSAGE)
			{
			/* This should never be the case, but safety first */
			if(gm_phone_nmbr_idx >= NUMBER_PHONES)
				{
				gm_phone_nmbr_idx = 0;
				}
			call GSM_Modem.sendSMS(gm_phone_numbers[gm_phone_nmbr_idx], gm_sms_buffer);
			call CommandTimer.start(TIMER_ONE_SHOT, CMND_SEND_SMS_TIME);
			}
		}

	/* Kill the system */
	task void shutDown()
		{
		call CommandTimer.stop();
		call FailSafeTimer.stop();
		call GSMSplitControlStatus.stop();
		}

	/* Start everything up */
	task void startComponents()
		{
		if(gm_state == GM_START_COMPONENTS)
			{
			call GSMSplitControlStatus.start();
			}
		}

	/* Respond to everything starting up */
	task void startDone()
		{
		if(gm_state == GM_START_DONE)
			{
			gm_state = GM_GET_SIG_QUAL;
			post transition();
			}
		}
	
	/* Stop the battery and decide if we have enough power to keep going */
	task void checkBatt()
		{
		power_monitor_t temp_power;
		
		if(gm_state == GM_START_COMPONENTS)
			{
			atomic temp_power = power;
			//no point to calling this because it is not connected to anything
			//call BattControl.stop();
			//in the new version the condition is different larger value is higher voltage	
			if(temp_power.BattV >= VOLTAGE_GSM_MIN)
				{
				post transition();
				}
			else
				{
				debug_trace();
				post stopDone();
				}
			}
		}

	/* Driver has stopped */
	task void stopDone()
		{
		atomic have_adc_control = FALSE;
		call CommandTimer.stop();
		call FailSafeTimer.stop();
		signal TrackerControl.stopped();
		}

	/* Main decision making process */
	task void transition()
		{
		switch(gm_state)
			{
			case GM_START_COMPONENTS:
				post startComponents();
				break;
				
			case GM_START_DONE:
				post startDone();
				break;
				
			case GM_GET_SIG_QUAL:
				post getSignalQuality();
				break;
				
			case GM_GET_CELL_MON:
				post getCellMonitorReport();
				break;
				
			case GM_READ:
				post read();
				break;
				
			case GM_BUILD_MESSAGE:
				post encodeMessage();
				break;
				
			case GM_SEND_MESSAGE:
				post sendMessage();
				break;

			case GM_ADVANCE_READ:
				post advanceFlash();
				break;

			default:
				break;
			}
		}

	/****************************************************************
	*
	*	ALL COMMANDS GO HERE
	*
	*****************************************************************/
	/* Initialize static variables and all needed modules */
	command result_t TrackerControl.init()
		{
		gm_state			= GM_NONE;
		gm_phone_nmbr_idx	= 0;
		memset(gm_sms_buffer, 0, sizeof(gm_sms_buffer));

		initPhoneBook();
	
		/*Init the tracker control*/
		gsm_seqnum = 0;
		//init the power sampling **NOTE start and stop don't do anything
		call PowerMonControl.init();
		call GSMSplitControlStatus.init();
		return SUCCESS;
		}
	/*
	 *
	 * This could possibly turn into an infinte loop...
	 *
	 */
	task void sample_power() {
		if(call PowerSample.getData() == FAIL) {
			//someone else might be sampling
			post sample_power();
		}
	}
	/* Start all necessary modules */
	command result_t TrackerControl.start(uint32_t run_time)
		{
		gm_state				= GM_START_COMPONENTS;
		gm_phone_nmbr_idx		= 0;
		gsm_run_time			= run_time;
		atomic have_adc_control	= TRUE;

		if(call FailSafeTimer.start(TIMER_ONE_SHOT, run_time) == SUCCESS)
			{
			//this doesn't do anything...
			//call BattControl.start();
			post sample_power();
			}
		else
			{
			debug_trace();
			post stopDone();
			}

		return SUCCESS;
		}

	/****************************************************************
	*
	*	ALL EVENTS GO HERE
	*
	*****************************************************************/
	/* Power Management */
 event result_t PowerSample.dataReady(power_monitor_t data)
		{
		if(have_adc_control == TRUE)
			{
			//update the power
			power = data;
			post checkBatt();		
			}
		return SUCCESS;
		}

	/****************************************************************
	*	Flash events
	*****************************************************************/
	/* This is called when we have gotten records out of flash */
	event result_t Flash.recordsReady(uint8_t numRecords)
		{
		/* This should always be true */
		if(numRecords > 0)
			{
			gm_number_of_records = numRecords;
			gm_state = GM_BUILD_MESSAGE;
			post transition();
			}
		else
			{
			gm_state = GM_SHUTDOWN;
			post shutDown();
			}
		return SUCCESS;
		}

	/*	We have advanced the read pointer.  In this module, that means that
	*	we have successfully sent data via SMS and now need more records.
	*/
	event void Flash.advanced()
		{
		gm_state = GM_READ;
		post transition();
		}

	/****************************************************************
	*	Timer events
	*****************************************************************/
	/* Some command is taking too long */
	event result_t CommandTimer.fired()
		{
		debug_trace();
		gm_state = GM_SHUTDOWN;
		post shutDown();
		//post stopDone();
		return SUCCESS;
		}

	/* We have exceeded our run time */
	event result_t FailSafeTimer.fired()
		{
		debug_trace();
		gm_state = GM_SHUTDOWN;
		post shutDown();
		//post stopDone();
		return SUCCESS;
		}

	/****************************************************************
	*	GSM Split Control events
	*****************************************************************/
	/*	Initialization is done of the driver, tell the upper layer we're
	*	ready to go
	*/
	event result_t GSMSplitControlStatus.initDone()
		{
		post initDone();
		return SUCCESS;
		}

	/* The GSM is on and associated.  Start sending data */
	event result_t GSMSplitControlStatus.startDone(gsm_error_t result)
		{
		if(gm_state == GM_START_COMPONENTS)
			{
			if(result == NO_ERROR)
				{
				gm_state		= GM_START_DONE;
				post transition();
				}
			else
				{
				gm_state = GM_SHUTDOWN;
				post shutDown();
				}
			}
		return SUCCESS;
		}

	/* The GSM has stopped.  Tell the upper layer we're finished */
	event result_t GSMSplitControlStatus.stopDone(gsm_error_t result)
		{
		debug_trace();
		post stopDone();
		return SUCCESS;
		}

	/****************************************************************
	*	GSM Modem events
	*****************************************************************/
	/*	We got the cell monitor report.  Start getting data from flash
	*	to send via SMS.
	*/
	event result_t GSM_Modem.cellMonitorReportReady(gsm_error_t result, gsm_cellmon_data_t *data)
		{
		/*	If we got the cell monitor report, make a copy of it and fetch
		*	the stored sensor data.
		*/
		if(result == NO_ERROR)
			{
			memcpy(&gm_gsm_cellmon_data, data, sizeof(gm_gsm_cellmon_data));
			gm_state = GM_READ;
			post transition();
			}
		/* 	We failed to get the cell monitor report.  Bail out. */
		else
			{
			gm_state = GM_SHUTDOWN;
			post shutDown();
			}		
		return SUCCESS;
		}

	/* The SMS send attempt is done.  We may or may not have successfully sent the data */
	event result_t GSM_Modem.sendSMSDone(gsm_error_t result, uint8_t *endpoint, uint8_t *data)
		{
		/* If the send was successful go the the next phone number in the book */
		if(result == NO_ERROR)
			{
			/* Restart the failsafe timer on success */
			call FailSafeTimer.stop();
			call CommandTimer.stop();
			call FailSafeTimer.start(TIMER_ONE_SHOT, gsm_run_time);
			gm_phone_nmbr_idx = gm_phone_nmbr_idx + 1;
			}

		/*	If we have sent the data to all of the numbers in the phone book then
		*	advance the read pointer so we don't ever fetch the data again.
		*/
		if(gm_phone_nmbr_idx >= NUMBER_PHONES)
			{
			gm_state			= GM_ADVANCE_READ;
			gm_phone_nmbr_idx	= 0;
			}
		
		post transition();
		return SUCCESS;
		}

	event result_t GSM_Modem.setCellMonitorReportValueDone(gsm_error_t result)
		{
		return SUCCESS;
		}

	/*	We got the signal quality.  If successful, get the cell monitor data.  If not,
	*	bail out of the module.
	*/
	event result_t GSM_Modem.signalQualityReady(gsm_error_t result, gsm_signal_quality_data_t *data)
		{
		if(result == NO_ERROR)
			{
			memcpy(&gm_gsm_signal_data, data, sizeof(gm_gsm_signal_data));
			gm_state = GM_GET_CELL_MON;
			post transition();
			}
		else
			{
			gm_state = GM_SHUTDOWN;
			post shutDown();
			}

		return SUCCESS;
		}
	
	/****************************************************************
	*
	*	ALL FUNCTIONS GO HERE
	*
	*****************************************************************/
	/* Fill the phone book with the number we might want to call */
	void initPhoneBook()
		{
		gm_phone_numbers[0] = GOOGLE;
		gm_phone_book = gm_phone_numbers;
		}
	}

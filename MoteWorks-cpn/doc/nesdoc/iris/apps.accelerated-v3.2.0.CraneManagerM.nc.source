/*
 *
 *
 *
 */
#include "avrtime.h"
#include "Tracker/storage.h"
#include "PowerMonitor/types.h"
module CraneManagerM
	{
	provides
		{
		interface StdControl;
		}
	uses
		{
		#ifdef I2CDBG
		I2CDBG_CONFIG();	
		#endif
		#ifdef USERADIODBG
		RADIODBG_CONFIG();
		#endif
		/* Storage control */
		interface FlashStorageControl as StorageControl;
		
		/* LEDs for debugging and state confirmation */
		interface Leds;

		/* Timers for state control */
		interface StdControl	as TimerControl;
		interface Timer			as StateTimer;
		interface Timer			as FailsafeTimer;

		/* GPS */
		interface SplitControl	as GpsSplitControl;       
		interface ReceiveMsg	as GpsGGARecv;
		interface ReceiveMsg	as GpsRMCRecv;
		interface Timer			as GpsTimer;
		
		/* Power Control */
		interface PowerManagement;
		command result_t Enable();
		command result_t Disable();
		
		/* Sensors */
		//interface ADC 			as ADCBATT;
		interface PowerSample;
		interface StdControl as PowerMonControl;
		//interface StdControl	as BattControl;
		
		interface Compass;
		interface SplitControl	as CompassSplitControl;
		
		/* Radio */
		interface CraneCommunication as Radio;
		
		/* GSM */
		interface TrackerControl	as GSMControl;
		
		/* Storage */
		interface FlashStorageWrite	as FlashWrite;
		}
	}

implementation
	{
	#include "Tracker/gps.h"
	#include "Tracker/config.h"
        

	/* GPS variables*/
	GGAMsg		gga_msg_buffer;
	RMCMsg		rmc_msg_buffer;
	uint16_t	gps_count;
	bool		gga_valid;
	bool		rmc_valid;
	
	/* sample variables */
	uint8_t  sample_seqnum;

	/* Compass variables */
	uint8_t		compass_minor_count;	/* the number of times we have sampled in the current period	*/
	uint8_t		compass_major_count;	/* used to time the duty cycle									*/
	uint16_t	compass_total_count;	/* How many compass samples we have done						*/
	
	/* Radio variables */
	uint32_t 	comm_on_time;	/* How long the radio ended up running for		*/
	
	/* GSM Variables */
	uint32_t gsm_sleep_time;	/* How long to sleep after the GSM is stopped	*/
	avrtime_t gsm_start_time;
		
	//norace uint16_t voltage;
	power_monitor_t power;
	uint8_t valid_gps_count = 0;
	
	enum states
		{
		INIT,
		SAMPLE_VOLTAGE,
		SAMPLE_GPS,
		WRITE_GPS,
		GPS_SLEEP,
		SAMPLE_COMPASS_VOLTAGE,
		COMPASS_START,
		SAMPLE_ACCEL,
		SAMPLE_HEADING,
		SAMPLE_TILT,
		SAMPLE_SOLAR,
		COMPASS_STOP,
		WRITE_ACCEL_X,
		WRITE_ACCEL_Y,
		WRITE_ACCEL_Z,
		WRITE_HEADING,
		WRITE_PITCH,
		WRITE_ROLL,
		WRITE_TEMP,
		WRITE_SOLAR_I,
		WRITE_SOLAR_V,
		COMPASS_SLEEP_MINOR,
		SAMPLE_COMPASS_VOLTAGE_MINOR,
		COMPASS_SLEEP_MAJOR,
		RADIO_LISTEN,
		GSM_COMM,
		COMM_SLEEP,
		GSM_SLEEP
		} system_state;
	
	const char* state_to_str(uint8_t state) {
		char *ptr;
		switch(state) {
			case INIT:
				ptr = var_to_str(INIT);
				break;
			case SAMPLE_VOLTAGE:
				ptr = var_to_str(SAMPLE_VOLTAGE);
				break;
			case SAMPLE_GPS:
				ptr = var_to_str(SAMPLE_GPS);
				break;
			case WRITE_GPS:
				ptr = var_to_str(WRITE_GPS);
				break;
			case GPS_SLEEP:
				ptr = var_to_str(GPS_SLEEP);
				break;
			case SAMPLE_COMPASS_VOLTAGE:
				ptr = var_to_str(SAMPLE_COMPASS_VOLTAGE);
				break;
			case COMPASS_START:
				ptr = var_to_str(COMPASS_START);
				break;
			case SAMPLE_ACCEL:
				ptr = var_to_str(SAMPLE_ACCEL);
				break;
			case SAMPLE_HEADING:
				ptr = var_to_str(SAMPLE_HEADING);
				break;
			case SAMPLE_TILT:
				ptr = var_to_str(SAMPLE_TILT);
				break;
			case SAMPLE_SOLAR:
				ptr = var_to_str(SAMPLE_SOLAR);
				break;
			case COMPASS_STOP:
				ptr = var_to_str(COMPASS_STOP);
				break;
			case WRITE_ACCEL_X:
				ptr = var_to_str(WRITE_ACCEL_X);
				break;
			case WRITE_ACCEL_Y:
				ptr = var_to_str(WRITE_ACCEL_Y);
				break;
			case WRITE_ACCEL_Z:
				ptr = var_to_str(WRITE_ACCEL_Z);
				break;
			case WRITE_HEADING:
				ptr = var_to_str(WRITE_HEADING);
				break;
			case WRITE_PITCH:
				ptr = var_to_str(WRITE_PITCH);
				break;
			case WRITE_ROLL:
				ptr = var_to_str(WRITE_ROLL);
				break;
			case WRITE_TEMP:
				ptr = var_to_str(WRITE_TEMP);
				break;
			case WRITE_SOLAR_I:
				ptr = var_to_str(WRITE_SOLAR_I);
				break;
			case WRITE_SOLAR_V:
				ptr = var_to_str(WRITE_SOLAR_V);
			case COMPASS_SLEEP_MINOR:
				ptr = var_to_str(COMPASS_SLEEP_MINOR);
				break;
			case SAMPLE_COMPASS_VOLTAGE_MINOR:
				ptr = var_to_str(SAMPLE_COMPASS_VOLTAGE_MINOR);
				break;
			case COMPASS_SLEEP_MAJOR:
				ptr = var_to_str(COMPASS_SLEEP_MAJOR);
				break;
			case RADIO_LISTEN:
				ptr = var_to_str(RADIO_LISTEN);
				break;
			case GSM_COMM:
				ptr = var_to_str(GSM_COMM);
				break;
			case COMM_SLEEP:
				ptr = var_to_str(COMM_SLEEP);
				break;
			case GSM_SLEEP:
				ptr = var_to_str(GSM_SLEEP);
				break;
			default:
				ptr = "";
				break;
		}
		return ptr;
	}
	/* Storage variable */
	generic_record_t gps_data;
	generic_record_t accel_x_data;
	generic_record_t accel_y_data;
	generic_record_t accel_z_data;
	generic_record_t heading_data;
	generic_record_t pitch_data;
	generic_record_t roll_data;
	generic_record_t temp_data;
	generic_record_t solar_i_data;
	generic_record_t solar_v_data;

	/* Other functions */
	result_t stopGPS();
	
	/* Put system into deep sleep mode */
	task void sleep()
		{
		call PowerManagement.adjustPower();
		}
	/*
	 * This will convert the system time in ms since our epoch Sat Jan 01 00:00:00 UTC 2011
	 * to seconds in 32-bit format
	 */
	inline uint32_t t_ms_to_t_s_in_cpn_epoch(const avrtime_t *lsys_time) {
		avrtime_t ltime;
		ltime = (avrtime_t)*lsys_time;
		//convert to seconds
		ltime = ltime/(1000);
		//subtract the epoch seconds offset **binary time
		//129384000 seconds == 132489216 binary seconds
		ltime -= 132489216;
		//truncate to 128 years
		return (uint32_t)(ltime&0x00000000FFFFFFFF);	
	}

	/*
	 * This will update the power reading for the assigned record
	 */
	inline void update_header_with_power(generic_record_t *ptr) {
		uint8_t lv_bat;
		lv_bat = normalize_batt_v(&power.BattV);
		ptr->vref_solar = power.SolarV;
		ptr->iref_solar = power.SolarI;
		ptr->vref_bat = lv_bat;
	}
	
	/*
	 *
	 * This will inflate the record with the system time and increment record number
	 *
	 */
	inline void inflate_record_header(generic_record_t *ptr) {
		uint32_t ltime;
		ltime = t_ms_to_t_s_in_cpn_epoch(get_time_millis_ptr());
		ptr->sys_time = ltime;
		ptr->record_num = sample_seqnum++;

	}

	/*	This is how we change our overall state.  This function checks the overall system state
	*	and decides which of the specific modes we need to advance
	*/
	task void do_next_state()
		{
		uint32_t gps_sleep_time;
		uint32_t radio_sleep_time;
		
		//call Leds.yellowToggle();
		dprintf("Completed State:%s\n",state_to_str(system_state));
		
		switch(system_state)
			{
			case INIT:
				call Leds.set(0);		
				system_state = SAMPLE_VOLTAGE;
				
				/* Set up compass */
				compass_major_count = 0;
				compass_minor_count = 0;
				
				/* Prepare GPS */
				gps_count = 0;
				
				/* Get radio ready */
				comm_on_time = RADIO_LISTEN_TIME;
				radio_sleep_time = 0;
				
				/* Get all of the storage records ready for new values */
				memset((uint8_t *)&gps_data, 0xFF, sizeof(generic_record_t));
				gps_data.type = GPS_DATA;

				memset((uint8_t *)&accel_x_data, 0xFF, sizeof(generic_record_t));
				accel_x_data.type = ACCEL_X_DATA;

				memset((uint8_t *)&accel_y_data, 0xFF, sizeof(generic_record_t));
				accel_y_data.type = ACCEL_Y_DATA;

				memset((uint8_t *)&accel_z_data, 0xFF, sizeof(generic_record_t));
				accel_z_data.type = ACCEL_Z_DATA;
				
				memset((uint8_t *)&heading_data, 0xFF, sizeof(generic_record_t));
				heading_data.type = HEADING_DATA;

				memset((uint8_t *)&pitch_data, 0xFF, sizeof(generic_record_t));
				pitch_data.type = PITCH_DATA;

				memset((uint8_t *)&roll_data, 0xFF, sizeof(generic_record_t));
				roll_data.type = ROLL_DATA;

				memset((uint8_t *)&temp_data, 0xFF, sizeof(generic_record_t));
				temp_data.type = TEMP_DATA;

				
				memset((uint8_t *)&solar_i_data, 0xFF, sizeof(generic_record_t));
				solar_i_data.type = SOLAR_I_DATA;
				memset((uint8_t *)&solar_v_data, 0xFF, sizeof(generic_record_t));
				solar_v_data.type = SOLAR_V_DATA;
				/* Start by getting the current voltage */
				call PowerSample.getData();
				break;
				
			case SAMPLE_VOLTAGE:
				/* 	Sloppy, but didn't have room in GPS and didn't want an extra record type
				*	just for battery
				*/
				inflate_record_header(&gps_data);
				//condition is different than v2
				//higher value is larger voltage
				dprintf("%lu\n",gps_data.sys_time);
				dprintf("tbv%u\n",gps_data.vref_bat);
				dprintf("tbi%u\n",power.BattI);
				dprintf("tsv%u\n",gps_data.vref_solar);
				dprintf("tsi%u\n",gps_data.iref_solar);
				dprintf("test:%u\n",VOLTAGE_GPS_MIN);
				
				if(power.BattV >= VOLTAGE_GPS_MIN)
					{
					system_state = SAMPLE_GPS;
					/* Start the GPS */
					call GpsSplitControl.start();
					}
				else
					{
					/* not enough power for anything, go to sleep for LONG_SLEEP_TIME */
					system_state = INIT;
					//call Leds.yellowOff();
					//call Leds.redOff();
					//call Leds.greenOff();
					call StateTimer.start(TIMER_ONE_SHOT, (uint32_t)LONG_SLEEP_TIME);
					post sleep();
					}
				break;

			case SAMPLE_GPS:
				system_state = WRITE_GPS;
				call FlashWrite.writeRecord((uint8_t *)&gps_data);
				break;

			case WRITE_GPS:
				/*
				 * This will sleep the rest of the time not used, to elliminate 
				 * clock skew
				 */
				if(GPS_MAX_WAIT_CYCLES > gps_count)
					{
					gps_sleep_time = ((GPS_MAX_WAIT_CYCLES - gps_count) * GPS_TIMER_RATE);
					system_state = GPS_SLEEP;
					call StateTimer.start(TIMER_ONE_SHOT, gps_sleep_time);
					post sleep();
					}
				else
					{
					system_state = GPS_SLEEP;
					post do_next_state();
					}
				break;
				
			case GPS_SLEEP:
				system_state = SAMPLE_COMPASS_VOLTAGE;
				call PowerSample.getData();
				break;
			case SAMPLE_COMPASS_VOLTAGE:
				system_state = COMPASS_START;
				call FailsafeTimer.start(TIMER_ONE_SHOT, COMPASS_MAX_TIME);
				call CompassSplitControl.start();
				break;
				
			case COMPASS_START:
				system_state = SAMPLE_ACCEL;
				call Compass.getAccels();
				break;
				
			case SAMPLE_ACCEL:
				system_state = SAMPLE_HEADING;
				call Compass.getHeadings();
				break;
	
			case SAMPLE_HEADING:
				system_state = SAMPLE_TILT;
				call Compass.getTilts();
				break;
			case SAMPLE_TILT:
				system_state = SAMPLE_SOLAR;
				call PowerSample.getData();
				break;
			case SAMPLE_SOLAR:
				compass_minor_count = compass_minor_count + 1;
				call FailsafeTimer.stop();
				
				if(compass_minor_count < COMPASS_MINOR_COUNT)
					{
					system_state = COMPASS_SLEEP_MINOR;
					call StateTimer.start(TIMER_ONE_SHOT, COMPASS_MINOR_DELAY);
					post sleep();
					}
				else
					{
					system_state = COMPASS_STOP;
					call CompassSplitControl.stop();
					}
				break;
				
			case COMPASS_STOP:
				system_state = WRITE_ACCEL_X;
				inflate_record_header(&accel_x_data);
				//dprintf("tbv%u\n",accel_x_data.vref_bat);
				//dprintf("abv%u\n",power.BattV);
				//dprintf("test:%u\n",VOLTAGE_GPS_MIN);
				call FlashWrite.writeRecord((uint8_t *)&accel_x_data);
				break;
				
			case WRITE_ACCEL_X:
				system_state = WRITE_ACCEL_Y;
				inflate_record_header(&accel_y_data);
				//dprintf("tbv%u\n",accel_y_data.vref_bat);
				//dprintf("abv%u\n",power.BattV);
				call FlashWrite.writeRecord((uint8_t *)&accel_y_data);
				break;

			case WRITE_ACCEL_Y:
				system_state = WRITE_ACCEL_Z;
				inflate_record_header(&accel_z_data);
				//dprintf("tbv%u\n",accel_z_data.vref_bat);
				//dprintf("abv%u\n",power.BattV);
				call FlashWrite.writeRecord((uint8_t *)&accel_z_data);
				break;

			case WRITE_ACCEL_Z:
				system_state = WRITE_HEADING;
				inflate_record_header(&heading_data);
				//dprintf("tbv%u\n",heading_data.vref_bat);
				//dprintf("abv%u\n",power.BattV);
				call FlashWrite.writeRecord((uint8_t *)&heading_data);
				break;
				
			case WRITE_HEADING:
				system_state = WRITE_PITCH;
				inflate_record_header(&pitch_data);
				//dprintf("tbv%u\n",pitch_data.vref_bat);
				//dprintf("abv%u\n",power.BattV);
				call FlashWrite.writeRecord((uint8_t *)&pitch_data);
				break;

			case WRITE_PITCH:
				system_state = WRITE_ROLL;
				inflate_record_header(&roll_data);
				//dprintf("tbv%u\n",roll_data.vref_bat);
				//dprintf("abv%u\n",power.BattV);
				call FlashWrite.writeRecord((uint8_t *)&roll_data);
				break;

			case WRITE_ROLL:
				system_state = WRITE_TEMP;
				inflate_record_header(&temp_data);
				//dprintf("tbv%u\n",temp_data.vref_bat);
				//dprintf("abv%u\n",power.BattV);
				call FlashWrite.writeRecord((uint8_t *)&temp_data);
				break;
			case WRITE_TEMP:
				system_state = WRITE_SOLAR_I;
				inflate_record_header(&solar_i_data);
				//dprintf("tbv%u\n",solar_i_data.vref_bat);
				//dprintf("abv%u\n",power.BattV);
				call FlashWrite.writeRecord((uint8_t *)&solar_i_data);
				break;
			case WRITE_SOLAR_I:
				system_state = WRITE_SOLAR_V;
				inflate_record_header(&solar_v_data);
				//dprintf("tbv%u\n",solar_v_data.vref_bat);
				//dprintf("abv%u\n",power.BattV);
				call FlashWrite.writeRecord((uint8_t *)&solar_v_data);
				break;
			case WRITE_SOLAR_V:
				compass_major_count = compass_major_count + 1;
				if(compass_major_count < COMPASS_MAJOR_COUNT)
					{
					system_state = COMPASS_SLEEP_MAJOR;
					compass_minor_count = 0;
					call StateTimer.start(TIMER_ONE_SHOT, COMPASS_MAJOR_DELAY);
					post sleep();
					}
				else
					{
					system_state = RADIO_LISTEN;
					call Disable(); //Disable PW before starting Radio and Gsm
					//call Leds.greenOn();
					call Radio.start(RADIO_LISTEN_TIME);
					}
				break;
				
			case COMPASS_SLEEP_MINOR:
				system_state = SAMPLE_COMPASS_VOLTAGE_MINOR;
				call PowerSample.getData();
				break;
			
			case SAMPLE_COMPASS_VOLTAGE_MINOR:
				system_state = SAMPLE_ACCEL;
				call FailsafeTimer.start(TIMER_ONE_SHOT, COMPASS_MAX_TIME);
				call Compass.getAccels();
				break;
				
			case COMPASS_SLEEP_MAJOR:
				system_state = GPS_SLEEP;//need to start the compass again
				post do_next_state();
				break;

			case RADIO_LISTEN:
				system_state = GSM_SLEEP;
				//this sleep time is the large one
				//we need to disable the leds as they take about 1ma each when on
				call Leds.set(0);
				//on next transition the yellow should turn on
				call StateTimer.start(TIMER_ONE_SHOT, RADIO_SLEEP_TIME);
				call Enable();
				post sleep();
				break;
			
			case GSM_SLEEP:
				system_state = GSM_COMM;
				call Disable();
				gsm_start_time = 0;
				gsm_start_time = get_time_millis();
				call GSMControl.start(GSM_LISTEN_TIME);
				break;
				
			case GSM_COMM:
				//system_state = INIT;
				break;
	
			/*
			 *FIXME NOT USED
			 */
			case COMM_SLEEP:
				system_state = RADIO_LISTEN;
				call Disable();
				call Radio.start(RADIO_LISTEN_TIME);
				break;

			default:
				dbg(DBG_TEMP, "Got into unknown state\n");
				break;
			}
		}

	/* Main system initialization.  First thing ever called */
    command result_t StdControl.init()
		{
		call Leds.init();
		call TimerControl.init();
		call PowerMonControl.init();
		call Radio.init();
		
		system_state = INIT;
		
		/* Prepare seqnum */
		sample_seqnum = 0;
       	return SUCCESS;
		}
    
	/* Called after the init.  This really kicks things off */
    command result_t StdControl.start()
		{
		call Leds.set(7);
		compass_total_count = 0;
		/* Turn on power management */
		call Enable();
		call TimerControl.start();
		
		if(call StorageControl.setup() == SUCCESS)
			{
			}
        return SUCCESS;
		}
    
	/* 	Doubtful this will ever be called.  This would mean the system is
	*	shutting down
	*/
    command result_t StdControl.stop()
		{
		call TimerControl.stop();
		return SUCCESS;
		}
	
	event result_t StorageControl.setupComplete()
		{
		call GpsSplitControl.init();
		return SUCCESS;
		}

	event result_t FlashWrite.writeDone(result_t success)
		{
		debug_trace();
		post do_next_state();
		return SUCCESS;
		}
		
	event result_t GpsSplitControl.initDone()
		{
		call GSMControl.init();
		return SUCCESS;
		}
		
	event result_t GpsSplitControl.startDone()
		{
		atomic
			{
			rmc_valid = FALSE;
			gga_valid = FALSE;
			}
		gps_count = 0;
		call GpsTimer.start(TIMER_ONE_SHOT, GPS_TIMER_RATE);

		return SUCCESS;
		}
		
	event result_t GpsSplitControl.stopDone()
		{
		debug_trace();
		post do_next_state();
		return SUCCESS;
		}
		
	event TOS_MsgPtr GpsGGARecv.receive(TOS_MsgPtr msgPtr)
		{
		atomic gga_msg_buffer = *((GGAMsg*)msgPtr);

		if(gga_msg_buffer.valid >= 1 && gga_msg_buffer.valid <= 6)
			{
			atomic gga_valid = TRUE;
			//call Leds.greenToggle();
			}

        return msgPtr;
		}
		
	event TOS_MsgPtr GpsRMCRecv.receive(TOS_MsgPtr msgPtr)
		{
		atomic rmc_msg_buffer = *((RMCMsg*)msgPtr);

		if(rmc_msg_buffer.valid == TRUE)
			{
			atomic rmc_valid = TRUE;
			//call Leds.greenToggle();
			}
		return msgPtr;
		}
		
		#define MAX_VALID_GPS_COUNT (10)
    event result_t GpsTimer.fired()
		{
		bool done;
		/* See if we've gotten all the sentences we require */
		atomic done = (gga_valid && rmc_valid);
		
		gps_count = gps_count + 1;
		
		/* 	If we have, stop the GPS, otherwise log how many times
		*	we have unsuccessfully sampled so that we can shut down
		*	the GPS if we aren't going to get a fix
		*/
		if(done == TRUE)
		{
			//struct avr_tm ltime;
			//uint64_t lsys;
			valid_gps_count = valid_gps_count + 1;
			gps_data.gps_record.hours = gga_msg_buffer.TimeHrs;
			gps_data.gps_record.minutes = gga_msg_buffer.TimeMin;
			gps_data.gps_record.date = rmc_msg_buffer.FixDate;
			gps_data.gps_record.lat_deg = gga_msg_buffer.Lat_deg;
			gps_data.gps_record.lat_dec_min = gga_msg_buffer.Lat_dec_min;
			gps_data.gps_record.lon_deg = gga_msg_buffer.Lon_deg;
			gps_data.gps_record.lon_dec_min = gga_msg_buffer.Lon_dec_min;
			gps_data.gps_record.altitude = gga_msg_buffer.Alt;
			gps_data.gps_record.sog = rmc_msg_buffer.Speed;
			//gps_data.gps_record.cog = rmc_msg_buffer.Tcourse;
			//update with the time first after checking if it is valid
			/*
			lsys = get_time_millis()*(1000/1024);
			//paarse the date
			parse_date(gps_data.gps_record.date,&ltime);
			//update with time
			ltime.min = gps_data.gps_record.minutes;
			ltime.hour = gps_data.gps_record.hours;
			ltime.sec = gga_msg_buffer.TimeSec;
			//check if the time is valid
			if(is_time_valid_tm(&ltime) == TRUE) {
				//set the system time to the local time
				//set_time_millis(avr_mktime(&ltime));
			}
			*/
			//call Leds.redToggle();
			if(valid_gps_count >= MAX_VALID_GPS_COUNT) {
				//otherwise don't sync
				stopGPS();
				valid_gps_count = 0;
			}else {
				call GpsTimer.start(TIMER_ONE_SHOT, GPS_TIMER_RATE);
			}
		} 
		else
			{
        if(gps_count >= GPS_MAX_WAIT_CYCLES)
				{
        stopGPS();
				}
			else
				{
        call GpsTimer.start(TIMER_ONE_SHOT, GPS_TIMER_RATE);
				}
			}
        
      return SUCCESS;
		}
		
	result_t stopGPS()
		{
		atomic
			{
			gga_valid = FALSE;
			rmc_valid = FALSE;
			}
        return call GpsSplitControl.stop();
		}

	event result_t PowerSample.dataReady(power_monitor_t data)
		{
		//update the system power values
		memcpy(&power,&data,sizeof(power_monitor_t));
		//update the records
	  update_header_with_power(&gps_data);
	  update_header_with_power(&accel_x_data);
	  update_header_with_power(&accel_y_data);
	  update_header_with_power(&accel_z_data);
	  update_header_with_power(&heading_data);
	  update_header_with_power(&pitch_data);
	  update_header_with_power(&roll_data);
	  update_header_with_power(&temp_data);
	  update_header_with_power(&solar_v_data);
	  update_header_with_power(&solar_i_data);

		if(system_state == SAMPLE_SOLAR) {
			//check if we are an even sample
			solar_i_data.solar_record.sample[compass_minor_count] = data.SolarI;
			solar_v_data.solar_record.sample[compass_minor_count] = data.SolarV;
		}
		//transition
		post do_next_state();
    return SUCCESS;
		}
		
	event result_t CompassSplitControl.initDone() {
		call StateTimer.start(TIMER_ONE_SHOT, INIT_DELAY);
		return SUCCESS;
	}
	
	event result_t CompassSplitControl.startDone() {
		debug_trace();
		post do_next_state();
		return SUCCESS;
	}
	
	event result_t CompassSplitControl.stopDone() {
		debug_trace();
		post do_next_state();
		return SUCCESS;
	}
	
	event void Compass.getAccelsDone(result_t result, uint16_t *data)
		{

		compass_total_count = compass_total_count + 1;
		
		accel_x_data.compass_record.sample[compass_minor_count] = data[0];
		accel_y_data.compass_record.sample[compass_minor_count] = data[1];
		accel_z_data.compass_record.sample[compass_minor_count] = data[2];
		post do_next_state();
		}
		
	event void Compass.getHeadingsDone(result_t result, uint16_t *data)
		{

		heading_data.compass_record.sample[compass_minor_count] = data[0];
		pitch_data.compass_record.sample[compass_minor_count] = data[1];
		roll_data.compass_record.sample[compass_minor_count] = data[2];
		post do_next_state();
		}

	event void Compass.getTiltsDone(result_t result, uint16_t *data)
		{

		temp_data.compass_record.sample[compass_minor_count] = data[2];
		post do_next_state();
		}

	event result_t Radio.CommFinished(uint32_t on_time)
		{
		comm_on_time = comm_on_time + on_time;
		post do_next_state();
		return SUCCESS;
		}

	/*	Setting up all of the components is done, start the action	*/
	event result_t StateTimer.fired()
		{
		post do_next_state();
		return SUCCESS;
		}

	event result_t FailsafeTimer.fired()
		{
		atomic system_state = SAMPLE_SOLAR;
		post do_next_state();
		return SUCCESS;
		}
		
	event void Compass.calibrateDone(result_t result)
		{
		
		}
		
	event void GSMControl.initDone()
		{
		call CompassSplitControl.init();
		}
		
	event void GSMControl.stopped()
		{
		avrtime_t time_delta;
		//change state to init so stop done doesn't get called twice
		call Leds.set(0);
		atomic system_state = INIT;
		time_delta = (get_time_millis() - gsm_start_time);
		//check if we have run the entire time we want
		if(GSM_SLEEP_TIME > time_delta) {
			//if we have been on less time than we want sleep the rest of it
			call StateTimer.start(TIMER_ONE_SHOT, GSM_SLEEP_TIME + time_delta);
		}else {
			//otherwise just sleep regular
			call StateTimer.start(TIMER_ONE_SHOT, GSM_SLEEP_TIME);
		}
		call Enable(); //Re-enable PM mode after using UART0
		post sleep();
		}
	}

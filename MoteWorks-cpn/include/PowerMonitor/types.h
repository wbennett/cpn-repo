#ifndef POWER_MON_TYPES_H_INCLUDED
#define POWER_MON_TYPES_H_INCLUDED

typedef struct power_monitor_t {
	uint16_t BattV;
	uint16_t BattI;
	uint16_t SolarV;
	uint16_t SolarI;
}__attribute__ ((packed)) power_monitor_t;
#ifndef BATT_V_UPPER_BOUND
#define BATT_V_UPPER_BOUND (300) 
#endif 
#define BATT_V_LOWER_BOUND (BATT_V_UPPER_BOUND-255)

//functions to convert to the correct range
uint8_t normalize_batt_v(const uint16_t *ptr) {

	if(*ptr < BATT_V_LOWER_BOUND) {
		return 0;
	}else if(*ptr > BATT_V_UPPER_BOUND) {
		return 255;
	}else {
		return (*ptr-BATT_V_LOWER_BOUND) % 256;
	}

//	return (uint8_t)((*ptr)>>2);
}

uint8_t normalize_batt_i(uint16_t *ptr) {
#warning "NOT IMPLEMENTED"
}

uint8_t normalize_solar_v(uint16_t *ptr) {
#warning "NOT IMPLEMENTED"
}

uint8_t normalize_solar_i(uint16_t *ptr) {
#warning "NOT IMPLEMENTED"
}
#endif

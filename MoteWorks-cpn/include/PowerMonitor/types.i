%module PowerMonitor
%{
#include "types.h"
%}

typedef struct power_monitor_t {
	uint16_t BattV;
	uint16_t BattI;
	uint16_t SolarV;
	uint16_t SolarI;
}Power_monitor_t;



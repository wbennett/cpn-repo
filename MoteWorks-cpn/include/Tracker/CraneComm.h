#ifndef CRANECOMM_H
#define CRANECOMM_H

#include "storage.h"

typedef uint8_t 	board_id_t;
typedef uint16_t	record_count_t;

enum
	{
	INVALID_CMND		= 0,
	BEACON_OPEN			= 1,
	REQUEST_ACCESS		= 2,
	ACCESS_GRANT		= 3,
	
	/* Begin record types */
	TX_GENERIC_RECORD = 4,
	/* End record types */
	
	//leaving these as is since, i
	//am not entirely sure what the affects are of
	//changing theses enum vals
	RECORD_RCV_ACK		= 14,
	END_SESSION			= 15,
	
	};

typedef struct crane_header_t
	{
	board_id_t	board_id;
	uint8_t 	cmnd;
	} __attribute__ ((packed)) crane_header_t;
	
typedef struct beacon_data_payload_t
	{
	} __attribute__ ((packed)) beacon_data_payload_t;
	
typedef struct request_access_payload_t
	{
	record_count_t	record_count;
	} __attribute__ ((packed)) request_access_payload_t;
	
typedef struct access_grant_payload_t
	{
	board_id_t	id;
	} __attribute__ ((packed)) access_grant_payload_t;

typedef struct compass_record_payload_t
	{
	uint8_t 			seq_id;
	compass_record_t	data;
	} __attribute__ ((packed)) compass_record_payload_t;

typedef struct generic_record_payload_t {
	uint8_t seq_id;
	generic_record_t data;
} __attribute__ ((packed)) generic_record_payload_t;

typedef struct gps_record_payload_t
	{
	uint8_t 		seq_id;
	gps_record_t	data;
	} __attribute__ ((packed)) gps_record_payload_t;
	
typedef struct record_rcv_ack_payload_t
	{
	uint8_t 	seq_id;
	} __attribute__ ((packed)) record_rcv_ack_payload_t;

typedef struct end_session_payload_t
	{
	} __attribute__ ((packed)) end_session_payload_t;
	
typedef struct crane_msg_t
	{
	crane_header_t	header;
	union
		{
		beacon_data_payload_t		beacon;
		request_access_payload_t	request_access;
		access_grant_payload_t		access_grant;
		generic_record_payload_t 	generic_record;
		record_rcv_ack_payload_t	record_rcv_ack;
		end_session_payload_t		end_session;
		} payload;
	} __attribute__ ((packed)) crane_msg_t;
#endif

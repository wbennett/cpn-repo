#ifndef _GSM_H
#define _GSM_H
/**
 * Author: wbennett
 * 
 * Note:
 * This header file will includes structs used by the GSM_Modem
 *
 */

#ifdef __cplusplus
extern "C" {
#include <stdint.h>
#endif

#define GEN_DATA_LENGTH  200             //max payload length of General_Msg
#define AT_GEN_MSG 0
#define AT_GPS_MSG 1
#define AT_CARROT_MSG 2
#define AT_ANY_MSG 3
#define CR 0x0D
#define LF 0x0A

#define QUEUE_SIZE 4


#define HEADER_SIZE			((uint8_t)18 * 2)	/* SMS Header size									*/
/* How many records we can fit in an SMS 															*/
#define MAX_NUM_RECORDS		((uint8_t)(MAX_PAYLOAD_SIZE/sizeof(generic_record_t)))
/* How much data we can pack in an SMS after byte stuffing and header 								*/
#define MAX_PAYLOAD_SIZE	((uint8_t)((MAX_SMS_SIZE-HEADER_SIZE)/2))
#define MAX_SMS_SIZE		((uint8_t)160)		/* Size of SMS message								*/
		

//Set GPS unit baudrate here
//#define UART1_BAUDRATE 4800
//TODO: FIGURE OUT HOW TO SET BAUD RATE
//USES micazc hardware.h file


/**
 *  This packet is what the TOS_MSG is cast into when the gps signals
 *  the Raw Receive.
 */
typedef struct {
  uint8_t length;  //does not include len or crc
  int8_t data[GEN_DATA_LENGTH];  //should be uint8_t
}
__attribute__ ((packed)) General_Msg;
typedef General_Msg *General_MsgPtr;

typedef struct {
			uint8_t contents[QUEUE_SIZE];
			uint8_t front;
			uint8_t count;
}
	RecvQueue_t;
typedef RecvQueue_t recvQueue_t;

typedef uint8_t gsm_result_t;
enum
{
	ERROR = 0,
	OK = 1,
	TIMEOUT_ERROR = 3,
};

typedef uint8_t gsm_assoc_status_t;
enum
{
	//this means its not trying to associate and not registered
	NOT_REGISTERED_NO_LOOK = 0,
	REGISTERED_HOME = 1,
	NOT_REGISTERED_LOOK = 2,
	REGISTRATION_DENIED = 3,
	UNKNOWN = 4,
	REGISTERED_ROAMING = 5,
};

typedef uint8_t gsm_assoc_mode_t;
enum
{
	DISABLE_NETWORK_REGISTRATION = 0,
	ENABLE_NETWORK_REGISTRATION_RESULT_CODE = 1,
	ENABLE_NETWORK_REGISTRATION_WITH_CELL_ID = 2,//we want this one
};

typedef struct
{
	uint8_t status;//dec
	uint16_t cellid;//hex
	uint16_t areacode;//hex
} 
__attribute__ ((packed)) Gsm_cellid_areacode_data_t;
typedef Gsm_cellid_areacode_data_t gsm_cellid_areacode_data_t;

typedef struct
{
	uint8_t rssi;//0-255
	uint8_t bit_err_rate;//percentage
}
__attribute__ ((packed)) Gsm_signal_quality_data_t;
typedef Gsm_signal_quality_data_t gsm_signal_quality_data_t;

typedef struct
{
	char netname[8];
	uint8_t bsic;//hex
	uint16_t lac;//hex
	uint16_t id;//hex
	uint16_t arfcn;//dec
} 
__attribute__ ((packed)) Gsm_cellmon_record_t;
typedef Gsm_cellmon_record_t gsm_cellmon_record_t;

typedef struct
{
	Gsm_signal_quality_data_t signal_quality_data;
	Gsm_cellmon_record_t gsm_cellmon_record;
	uint8_t seqnum;
}
__attribute__ ((packed)) Gsm_header_t;
typedef Gsm_header_t gsm_header_t;
	

#define MAXCELLMONRECORDS (3)
typedef struct
{
  Gsm_cellmon_record_t towers[MAXCELLMONRECORDS];
}
__attribute__ ((packed)) Gsm_cellmon_data_t;
typedef Gsm_cellmon_data_t gsm_cellmon_data_t;

typedef uint16_t gsm_error_t;
enum
{
	//Error codes specified in Telit_AT_Commands_Reference_Guide
	MS_OPERATION_NOT_SUPPORTED = 303u,
	MS_SIM_NOT_INSERTED = 310u,
	MS_SIM_PIN_REQUIRED = 311u,
	MS_NO_NETWORK_SERVICE = 331u,
	MS_NETWORK_TIMEOUT = 332u,
	//Error codes >=1000 are custom defined
	NO_MODEM_RESPONSE = 1000u,
	GSM_UART_HANDLER_SEND_CMD_FAIL = 1001u,
	SMS_PROMPT_NOT_RECVD = 1002u,
	GSM_NOT_ASSOCIATED = 1003u,
	GSM_BUSY = 1004u,
	INVALID_PARAM = 1005u,
	WATCHDOG_TIMED_OUT = 1006u,
	UNSUPPORTED_FEATURE = 1007u,
	NO_ERROR = 65535u,
};

#ifdef __cplusplus
}
#endif

#endif

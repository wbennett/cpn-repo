%module Tracker
%{
#include <tracker.h>
%}

%include gsm.i
%include storage.i

extern Sms_packet_t decode_sms_message(char *buffer, uint8_t length);

#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

/* GPS constants */
#ifndef GPS_TIMER_RATE_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define GPS_TIMER_RATE_PARAM (1024u)
#endif
#define GPS_TIMER_RATE 		((uint16_t)GPS_TIMER_RATE_PARAM)

#ifndef GPS_MAX_WAIT_CYCLES_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define GPS_MAX_WAIT_CYCLES_PARAM (10)
#endif
#define GPS_MAX_WAIT_CYCLES (GPS_MAX_WAIT_CYCLES_PARAM)

/* long sleeep time */
#ifndef LONG_SLEEP_TIME_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define LONG_SLEEP_TIME_PARAM (3000u)
#endif
#define LONG_SLEEP_TIME		((uint32_t)LONG_SLEEP_TIME_PARAM)


#ifndef INIT_DELAY_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define INIT_DELAY_PARAM (15720u)
#endif
#define INIT_DELAY			((uint32_t)INIT_DELAY_PARAM)

/* Compass constants */
#ifndef COMPASS_MAX_TIME_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define COMPASS_MAX_TIME_PARAM (614400u)
#endif
#define COMPASS_MAX_TIME	((uint32_t)COMPASS_MAX_TIME_PARAM)

#ifndef COMPASS_MINOR_COUNT_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define COMPASS_MINOR_COUNT_PARAM (10)
#endif
#define COMPASS_MINOR_COUNT	(COMPASS_MINOR_COUNT_PARAM)

#ifndef COMPASS_MAJOR_COUNT_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define COMPASS_MAJOR_COUNT_PARAM (1024u)
#endif
#define COMPASS_MAJOR_COUNT	(COMPASS_MAJOR_COUNT_PARAM)

#ifndef COMPASS_MINOR_DELAY_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define COMPASS_MINOR_DELAY_PARAM (1024u)
#endif
#define COMPASS_MINOR_DELAY	((uint16_t)COMPASS_MINOR_DELAY_PARAM)

#ifndef COMPASS_MAJOR_DELAY_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define COMPASS_MAJOR_DELAY_PARAM (1024u)
#endif
#define COMPASS_MAJOR_DELAY	((uint32_t)COMPASS_MAJOR_DELAY_PARAM)	

/* Radio constants */
#ifndef RADIO_LISTEN_TIME_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define RADIO_LISTEN_TIME_PARAM (3000u)
#endif
#define RADIO_LISTEN_TIME	((uint32_t)RADIO_LISTEN_TIME_PARAM)	

/*sleep time*/
#ifndef RADIO_SLEEP_TIME_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define RADIO_SLEEP_TIME_PARAM (3000u)
#endif
#define RADIO_SLEEP_TIME ((uint32_t)RADIO_LISTEN_TIME_PARAM)

#ifndef GSM_SLEEP_TIME_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define GSM_SLEEP_TIME_PARAM (3000u)
#endif
#define GSM_SLEEP_TIME ((uint32_t)GSM_SLEEP_TIME_PARAM)

/* GSM constants */
#ifndef GSM_LISTEN_TIME_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define GSM_LISTEN_TIME_PARAM (307200u)
#endif
#define GSM_LISTEN_TIME		((uint32_t)GSM_LISTEN_TIME_PARAM)

/* 	Voltage constants as measured by ADC, with a 250 mV compensation to compensate for offset from actual reading
*	adc = ((1100 * 1024) / v) + 25
*	vreal = (adc/1023)*vref0
*	TODO:  Verify these constants are correct
*/
#ifndef VOLTAGE_GPS_MIN_PARAM
#warning "PARAM NOT SET USING DEFAULT"
#define VOLTAGE_GPS_MIN_PARAM (777)
#endif
#define VOLTAGE_GPS_MIN			((uint16_t)VOLTAGE_GPS_MIN_PARAM)	/* ~3100 mV	*/
#endif

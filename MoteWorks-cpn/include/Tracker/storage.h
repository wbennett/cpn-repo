#ifndef STORAGE_H
#define STORAGE_H
//#include "PowerMonitor/types.h"
#include "gsm.h"

#ifdef __cplusplus
extern "C" {
#endif



#define GENERIC_RECORD_SIZE	(sizeof(generic_record_t))
#define COMPASS_STORAGE_COUNT (10)
#define SOLAR_STORAGE_COUNT (10)

enum
	{
	GPS_DATA			= 0,
	ACCEL_X_DATA		= 1,
	ACCEL_Y_DATA		= 2,
	ACCEL_Z_DATA		= 3,
	HEADING_DATA		= 4,
	PITCH_DATA		= 5,
	ROLL_DATA		= 6,
	TEMP_DATA			= 7,
	SOLAR_I_DATA 	=8,
	SOLAR_V_DATA = 9,
	
	FIRST_RECORD_TYPE	= GPS_DATA,
	LAST_RECORD_TYPE	= SOLAR_V_DATA
	};

/*
 * The limit of the storage size is 128 bytes as specified in 802.15.4 specification. This
 * variable is set during compile time with the flag -DTOSH_DATA_LENGH=X. This size needs
 * to match across the Zigbee network. Otherwise, the packets will be dropped.
 *
 */
typedef struct
	{
	uint16_t sample[COMPASS_STORAGE_COUNT];
	}
__attribute__ ((packed)) Compass_record_t;
typedef Compass_record_t compass_record_t;

typedef struct {
	uint16_t sample[SOLAR_STORAGE_COUNT];
}
__attribute__ ((packed)) Solar_monitor_record_t;
typedef Solar_monitor_record_t solar_monitor_record_t;

typedef struct
	{
	/* Time */
	uint8_t  hours;
	uint8_t  minutes;
	uint32_t date;
	
	/* Position */
	uint8_t  lat_deg;
	uint32_t lat_dec_min;
	uint8_t  lon_deg;
	uint32_t lon_dec_min;
	
	/* Other */
	uint16_t altitude;
	uint16_t sog;
	}
__attribute__ ((packed)) Gps_record_t;
typedef Gps_record_t gps_record_t;

typedef struct
	{
	uint8_t	type;
	uint16_t vref_solar;//voltage of solar panel during sample
	uint16_t iref_solar;//current reading on solar panel during sample
	uint8_t vref_bat;//voltage of battery during sample
	uint8_t record_num;//record number in memory #for debugging
	uint32_t sys_time;//seconds since epoch #EPOCH STARTS at Sat Jan 01 00:00:00 UTC 2011 or 129384000 seconds
	union
		{
		Compass_record_t	compass_record;
		Gps_record_t 		gps_record;
		Solar_monitor_record_t solar_record;
		};
	}
__attribute__ ((packed)) Generic_record_t;
typedef Generic_record_t generic_record_t;

typedef struct {

	Gsm_header_t header;
	Generic_record_t payload[MAX_NUM_RECORDS];
}
__attribute__ ((packed)) Sms_packet_t;
typedef Sms_packet_t sms_packet_t;

/*
 *
 * This function will decode a gsm sms message into a packet
 * this is intended to generalize the decoding process
 *
 */ 

static Sms_packet_t decode_sms_message(char const*buffer, uint8_t length) {
		Sms_packet_t rval;
		uint8_t *tBuffer;
		uint8_t i;
		uint8_t t_idx;
		tBuffer = (uint8_t*)&rval;
		i = 0;
		//unpack all of the record
		for(i = 0;i< length;i+=2) {
			tBuffer[t_idx] = ((buffer[i]-65) & 0x0f) << 4;
			tBuffer[t_idx] += ((buffer[i+1]-65) &0x0f);
			t_idx++;
		}
		return rval;
	}
#ifdef __cplusplus
}
#endif
#endif

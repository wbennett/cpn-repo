#ifndef COMPASS_MSG_H
#define COMPASS_MSG_H

typedef struct {
	uint8_t seqnum;
	uint16_t heading;
	int16_t pitch;
	int16_t roll;
	int16_t ax;
	int16_t ay;
	int16_t az;
}__attribute__((packed))compass_msg_t;

enum {
	AM_COMPASS_BASE_ADDR = 0,
	AM_COMPASS_MSG = 0x17,
};

#endif

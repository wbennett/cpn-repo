#ifndef CRANE_GAME_H
#define CRANE_GAME_H

#ifndef CRANE_GAME_GATEWAY_ADDR
#define CRANE_GAME_GATEWAY_ADDR (0x02)
#endif

enum {
	USER_STATE_FLYING,
	USER_STATE_FORAGING,
	USER_STATE_NOMOVEMENT,
	USER_STATE_DANCING,
	USER_STATE_ERROR,
	CRANE_GAME_AM_MSG = 0x17,
};
typedef uint8_t user_state_t;
typedef struct {
	uint8_t seqnum;
	user_state_t user_state;
	int32_t ms3x;
	int32_t ms3y;
	int32_t ms3z;
	int16_t ms2x;
	int16_t ms2y;
	int16_t ms2z;
}__attribute__((packed))cranegame_comm_t;

#endif

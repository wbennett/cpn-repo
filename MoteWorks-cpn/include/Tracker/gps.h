/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: gps.h,v 1.1.2.4 2007/12/03 22:28:11 xyang Exp $
 */


#ifndef XBOW_GPS_H
#define XBOW_GPS_H

#define GPS_DATA_LENGTH  128             //max payload length of GPS_MSG
#define GPS_PACKET_START 0x24            //start of gps packet
#define GPS_PACKET_END1  0x0D            // <LF> end if gps packet
#define GPS_PACKET_END2  0x0A            // <CR> end of gps packet

//#define GPS_MSG_LENGTH 100
//#define GPS_CHAR 11
#define GGA_FIELDS 				( 14 )	//these define the temp buffer for GGA Parsing
#define RMC_FIELDS				( 15 )
#define GPS_CHAR_PER_FIELD		( 10 )
#define GPS_DELIMITER 		','
#define GPS_END_MSG 		'*'


/**
 *  This packet is what the TOS_MSG is cast into when the gps signals
 *  the Raw Receive.
 */
typedef struct GPS_Msg {
  uint8_t length;  //does not include len or crc
  int8_t data[GPS_DATA_LENGTH];  //should be uint8_t
  uint16_t crc;
} __attribute__ ((packed)) GPS_Msg;

typedef GPS_Msg *GPS_MsgPtr;

typedef union raw_storage_buffer {
	char parsed_gga[GGA_FIELDS][GPS_CHAR_PER_FIELD];
	char parsed_rmc[RMC_FIELDS][GPS_CHAR_PER_FIELD];
} raw_storage_buffer;

/**
 *  This packet is what the TOS_MSG is cast into when the gps signals
 *  GGA receive.
 */
typedef struct GGAMsg 
	{
	bool valid;
	uint8_t TimeHrs;
	uint8_t TimeMin;
	uint8_t TimeSec;

	uint8_t Lat_deg;
	uint32_t Lat_dec_min;
	uint8_t Lon_deg;
	uint32_t Lon_dec_min;			//dddmm.mmmm in binary....
	uint16_t Alt;			//in meters
	
 	//MsgId 			0
	//FixTime 		1
	//Lat 				2
	//LatDir 			3
	//Lon 				4
	//LonDir 			5
	//FixQuality 	6
	//SatsUsed 		7
	//HDOP 				8
	//Alt 				9
	//AltUnit 		10
	//GeoSep 			11
	//GeoSepUnit 	12
	//DGPSage 		13
	//DGPSstaID 	14
	//ChkSum 			15
	} __attribute__ ((packed)) GGAMsg;

typedef struct RMCMsg 
	{
	bool valid;
	uint16_t Speed; 		//tenths of a knot
	uint16_t Tcourse; //tenths of a degree
	uint32_t FixDate;		//ddmmyy (have to convert it.... stored in binary...
	//(ddmmyy in binary)
	
 //MsgId 			0
 //FixTime 		1
 //FixStatus 	2
 //Lat 				3
 //LatDir 		4
 //Lon 				5
 //LonDir 		6
 //Speed 			7
 //Tcourse 		8
 //FixDate 		9
 //MagVar 		10
 //MVDir 			11
 //FixMode 		12
 //ChkSum 		13
	
	} __attribute__ ((packed)) RMCMsg;

typedef union NMEAMsg
	{
	GGAMsg	GGAMsgBuffer;
	RMCMsg	RMCMsgBuffer;
	} NMEAMsg;


//NMEA_EXTRA_STUFF
//relics from an old gps.h i based this off of
#ifndef NMEA_EXTRACTION_MACROS
#define NMEA_EXTRACTION_MACROS

#define is_gga_string_m(ns) ((ns[3]=='G')&&(ns[4]=='G')&&(ns[5]=='A'))
#define is_rmc_string_m(ns) ((ns[3]=='R')&&(ns[4]=='M')&&(ns[5]=='C'))

#define extract_hours_m(data) (10*(data[0]-'0') + (data[1]-'0'))
#define extract_minutes_m(data) (10*(data[2]-'0')+(data[3]-'0'))
#define extract_seconds_m(data) (10*(data[4]-'0')+(data[5]-'0'))

#define extract_altitude_m(data) (1000*(data[0]-'0')+100*(data[1]-'0')+10*(data[2]-'0')+(data[3]-'0'))

#define extract_speed_m(data) (1000*(data[0]-'0')+100*(data[1]-'0')+10*(data[2]-'0')+(data[3]-'0'))

#define extract_course_m(data) (1000*(data[0]-'0')+100*(data[1]-'0')+10*(data[2]-'0')+(data[3]-'0'))
#define extract_date_m(data) ((uint32_t)(100000*((uint8_t)data[0]-'0')+10000*((uint8_t)data[1]-'0')+1000*((uint8_t)data[2]-'0')+100*((uint8_t)data[3]-'0')+10*((uint8_t)data[4]-'0')+((uint8_t)data[5]-'0')))








#define extract_lat_deg_m(data)      (10*(data[0]-'0') + (data[1]-'0'))

#define extract_lat_dec_min_m(data)  (10*(data[2]-'0') +  (data[3]-'0') + 0.1*(data[5]-'0') \
                                      + 0.01*(data[6]-'0') + 0.001*(data[7]-'0') + 0.0001*(data[8]-'0'))
                                      
#define extract_lon_deg_m(data)     (100*(data[0]-'0') + 10*(data[1]-'0') + (data[2]-'0'))

#define extract_lon_dec_min_m(data) (10*(data[3]-'0') +  (data[4]-'0') + 0.1*(data[6]-'0') \
                      + 0.01*(data[7]-'0') + 0.001*(data[8]-'0') + 0.0001*(data[9]-'0'))






//NOTE:
//We don't reall need this param, but the old sensor board apps do
#define GPS_MAX_WAIT 10

#endif  
//NMEA_EXTRA_STUFF


#endif /* XBOW_GPS_H */


#ifndef AOP_H
#define AOP_H

#ifndef bool
#define bool uint8_t
#endif
#ifndef TRUE
#define TRUE (1)
#endif
#ifndef FALSE
#define FALSE (0)
#endif
#include <avr/io.h>
//#include <avr/signal.h>	//deprecated
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>


#ifndef sbi
/* avr-libc 1.2.3 doesn't include these anymore. */
#define sbi(port, bit) ((port) |= _BV(bit))
#define cbi(port, bit) ((port) &= ~_BV(bit))
#define inp(port) (port)
#define inb(port) (port)
#define outp(value, port) ((port) = (value))
#define outb(port, value) ((port) = (value))
#define inw(port) (*(volatile uint16_t *)&port)
#define PRG_RDB(addr) pgm_read_byte(addr)
#endif
#ifndef SUCCESS
	#define SUCCESS (1)
#endif
#ifndef FAIL
	#define FAIL (0)
#endif
/*#ifndef result_t
typedef uint8_t result_t;
#endif
*/
extern uint8_t DebugI2CM__flush(void);


void inline TOSH_uwait(int u_sec) {
    while (u_sec > 0) {
      asm volatile  ("nop" ::);
      asm volatile  ("nop" ::);
      asm volatile  ("nop" ::);
      asm volatile  ("nop" ::);
      asm volatile  ("nop" ::);
      asm volatile  ("nop" ::);
      asm volatile  ("nop" ::);
      asm volatile  ("nop" ::);
      u_sec--;
    }
}

#endif

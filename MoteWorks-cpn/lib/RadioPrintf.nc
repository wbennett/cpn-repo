/*
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: Blink.nc,v 1.1.4.1 2007/04/26 19:35:39 njain Exp $
 */

/**
 *
 * @author tinyos-help@millennium.berkeley.edu
 **/
#include "RadioPrintf.h"
configuration RadioPrintf {
	provides {
		command result_t flush();
	}
}
implementation {
  components Main, RadioPrintfM, GenericComm as Comm, LedsC;

	Main.StdControl -> RadioPrintfM.StdControl;

	flush = RadioPrintfM.flush;
  RadioPrintfM.Leds -> LedsC;
	RadioPrintfM.RadioControl -> Comm;
	RadioPrintfM.SendMsg -> Comm.SendMsg[AM_RADIO_PRINTF_MSG];
}


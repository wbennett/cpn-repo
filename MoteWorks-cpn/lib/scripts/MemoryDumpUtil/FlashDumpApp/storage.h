#ifndef STORAGE_H
#define STORAGE_H

#define GENERIC_RECORD_SIZE	27

enum
	{
	GPS_DATA	= 0,
	ADC_DATA	= 1,
	MAG_DATA	= 2
	};

typedef struct
	{
	uint16_t vref;
	uint16_t sensirion_temp; 
	uint16_t humidity;
	uint16_t intersematemp;
	uint16_t intersemapressure;
	uint16_t taosch0;
	uint16_t taosch1;
	uint16_t accel_x;
	uint16_t accel_y;
	} __attribute__ ((packed)) adc_record_t;

typedef struct
	{
	/* Time */
	uint8_t  hours;
	uint8_t  minutes;
	uint32_t dec_sec;
	uint32_t date;
	
	/* Position */
	uint8_t  lat_deg;
	uint32_t lat_dec_min;
	uint8_t  lon_deg;
	uint32_t lon_dec_min;
	
	/* Other */
	uint16_t altitude;
	uint16_t sog;
	uint16_t cog;
	} __attribute__ ((packed)) gps_record_t;

typedef struct
	{
	uint8_t	type;
	union
		{
		adc_record_t adc;
		gps_record_t gps;
		};
	} __attribute__ ((packed)) generic_record_t;

#endif

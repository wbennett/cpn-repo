/*
 * Copyright (c) 2004-2007 Crossbow Technology, Inc.
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: FlashDump.nc,v 1.1.2.2 2007/04/26 20:04:27 njain Exp $
 */
 

/**
 * This module shows how to use the Timer, LED, ADC and Messaging components.
 * Sensor messages are sent to the serial port
 **/
configuration FlashDump {
}
implementation {


enum {
  BYTE_EEPROM_ID = unique("ByteEEPROM"),
};

  components Main, FlashDumpM, TimerC, LedsC, Photo, ByteEEPROM;
  
  Main.StdControl -> TimerC.StdControl;
  Main.StdControl -> ByteEEPROM; 
  Main.StdControl -> FlashDumpM.StdControl;
  
  FlashDumpM.Timer -> TimerC.Timer[unique("Timer")];
  FlashDumpM.Leds -> LedsC.Leds;
  FlashDumpM.AllocationReq -> ByteEEPROM.AllocationReq[BYTE_EEPROM_ID];
  FlashDumpM.ReadData->ByteEEPROM.ReadData[BYTE_EEPROM_ID];
}


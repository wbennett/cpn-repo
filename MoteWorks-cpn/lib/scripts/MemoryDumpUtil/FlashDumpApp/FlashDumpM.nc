/**
 * Reads through a mote's flash memory and outputs the records
 * as if they had been received over the air from a base station
 **/
module FlashDumpM
	{
	provides
		{
		interface StdControl;
		}
	uses 
		{
		interface Timer;
		interface Leds;
		interface AllocationReq;
		interface ReadData;
		}
	}
	
implementation
	{
	#define DBG_PKT  1
  #define SO_DEBUG  1
	#include "PageEEPROM.h"
  #include "SOdebug.h"
	#include "storage.h"


	#define RECORD_LENGTH (27)
	
	#define FAKE_MOTE_ID	( 0 )		/* fake mote ID we claim to be							*/
	#define READ_WAIT 		( 50 ) 		/* Delay between reading consecutive records			*/
	#define INIT_WAIT 		( 20000 )	/* Delay between allocating memory and reading from it	*/
	/* How much flash memory we request */
	#define TOTAL_SIZE			((uint32_t)((((uint32_t)TOS_EEPROM_MAX_PAGES) << ((uint32_t)TOS_EEPROM_PAGE_SIZE_LOG2))))	/* All of the flash on the board */
	#define RECORD_SIZE_REQUEST ((uint32_t)(((uint32_t)TOTAL_SIZE) / ((uint32_t)GENERIC_RECORD_SIZE)) * ((uint32_t)GENERIC_RECORD_SIZE))	/* Flash for records */
	
	uint32_t 			read_idx;	/* current read location 	*/
	generic_record_t	record;		/* data destination			*/
	
	void outputADCData();
	void outputGPSData();
	
	/**
	* Initialize the component.
	* 
	* @return Always returns <code>SUCCESS</code>
	**/
	command result_t StdControl.init()
		{
		read_idx = 0;
		call Leds.init(); 
		call AllocationReq.request(RECORD_SIZE_REQUEST);  
	
		return SUCCESS;
		}

	/**
	* Start things up.  This just sets the rate for the clock component.
	* 
	* @return Always returns <code>SUCCESS</code>
	**/
	command result_t StdControl.start()
		{
		return SUCCESS;
		}

	/**
	* Halt execution of the application.
	* This just disables the clock component.
	* 
	* @return Always returns <code>SUCCESS</code>
	**/
	command result_t StdControl.stop()
		{
		return call Timer.stop();
		}

	event result_t AllocationReq.requestProcessed(result_t success)
		{
		// Allocation must succeed
		if(success == SUCCESS)
			{
			call Leds.greenOn();
			SODbg(1,"\n");
			call Timer.start(TIMER_ONE_SHOT, INIT_WAIT);
			}
		return SUCCESS;
		}  

	/**
	* Toggle the red LED in response to the <code>Timer.fired</code> event.  
	* Start the Light sensor control and sample the data
	*
	* @return Always returns <code>SUCCESS</code>
	**/
	event result_t Timer.fired()
		{
		/* Check to see if we've reached the end of memory */
		if(read_idx + GENERIC_RECORD_SIZE < (uint32_t)RECORD_SIZE_REQUEST)
			{
			call Leds.yellowToggle();
			call ReadData.read(read_idx, (uint8_t *)&record, GENERIC_RECORD_SIZE);
			}
		else
			{
			call Leds.greenOff();
			call Leds.redOn();
			}
		return SUCCESS;
		} 

	event result_t ReadData.readDone(uint8_t *data, uint32_t numBytesRead, result_t success) 
		{
		uint8_t i;
		uint8_t *err_ptr;
		uint8_t *buf;
		err_ptr = (uint8_t *)&record;
		
		read_idx = read_idx + numBytesRead;
			buf = (uint8_t *)&record;
		SODbg(1,"%x", buf[0]);
		for(i=1;i<RECORD_LENGTH;i++)
			{
			SODbg(1,",%x", buf[i]);
			}
		SODbg(1,"\n", NULL);

		/* Get next piece of memory */
		call Timer.start(TIMER_ONE_SHOT, READ_WAIT);
	
		return SUCCESS;
		}
	}

#!/usr/bin/ruby
require 'time'
require 'date'
# To change this template, choose Tools | Templates
# and open the template in the editor.
@gpssamples = [];
@voltagesamples = [];
@accelxsamples = [];
@accelysamples = [];
@accelzsamples = [];
@headingsamples = [];
@pitchsamples = [];
@rollsamples = [];
@tempsamples = [];
class GpsSample 
  attr_accessor :valid, :hours, :minutes, :date, :lat_deg, :lat_dec_min,
    :lon_deg, :lon_dec_min,:altitude,:sog,:cog, :latitude,:longitude
  def initialize
    @valid = -1
    @hours = -1
    @minutes = -1
    @date = -1
    @lat_deg = -1
    @lat_dec_min = -1
    @lon_deg = -1
    @lon_dec_min = -1
    @altitude = -1
    @sog = -1
    @cog = -1
    @latitude = -1
    @longitude = -1
  end
end

class SingleValueSample
  attr_accessor :count, :date, :seqnum, :value
  def initialize
    @count = -1
    @date = -1
    @seqnum = -1
    @value = -1
  end
end

class VoltageSample < SingleValueSample
end
class CompassSample < SingleValueSample
  
end

def unpack_to_int(array,index,numofbytes) 
  temp = array[index..(index+numofbytes-1)]
  #puts temp
  result = 0;
  #count =numofbytes-1;
  (0).upto(numofbytes-1) { |i| 
    result += (temp[i].to_i << (8*(i)));
  }
  return result;
end

def unpack_to_int_signed(array,index,numofbytes) 
  temp = array[index..(index+numofbytes-1)]
  #puts temp
  result = 0;
  #count =numofbytes-1;
  (0).upto(numofbytes-1) { |i| 
    result += (temp[i] << (8*(i)));
  }
  return (result << 16)>>16;
end

def parse_hex(records)
  normal_record = []
  record.each do |token|
    token = token.to_i(16)
    #printf "#{token},"
    normal_record.push(token)
  end
end

def parse_gps(record)
  normal_record = record
  puts normal_record
  #puts "\n"
  # goes in voltage type
  index = 1
  #msb = record[index+=1].to_i(16)
  #lsb = record[index+=1].to_i(16)
  if(unpack_to_int(normal_record, index, 2) == 0)
    return;
  end
  val = (1100*1024)/(unpack_to_int(normal_record, index, 2))+250
  voltage = val.to_f/1000;
  #puts "voltage: #{voltage}"
  v = VoltageSample.new;
  v.value = voltage;
  @voltagesamples.push(v);
  index+=2
  #below goes in the gps message
  #find the hours
  valid = true;
  hours = unpack_to_int(normal_record, index, 1);
  if(hours == 255)
    valid = false;
  end
  index+=1
  minutes = unpack_to_int(normal_record,index,1);
  if(minutes == 255)
    valid = false;
  end
  index+=1
  #date = (record[index+3] << 24) + (record[index+2] << 16)+ (record[index+1] << 8)+ record[]
  date = unpack_to_int(normal_record, index, 4);
  if(date == 4294967295)
    valid = false;
  end
  index+=4  
  lat_deg = unpack_to_int(normal_record, index, 1);
  if(lat_deg == 255)
    valid = false;
  end
  index+=1
  lat_dec_min = unpack_to_int(normal_record, index, 4)
  if(lat_dec_min == 4294967295)
    valid = false;
  end
  index+=4
  lon_deg = unpack_to_int(normal_record, index, 1);
  if(lon_deg == 255)
    valid = false;
  end
  index+=1
  lon_dec_min = unpack_to_int(normal_record, index, 4)
  if(lon_dec_min == 4294967295)
    valid = false;
  end
  index+=4
  altitude = unpack_to_int(normal_record, index, 2)
  if(altitude == 65535)
    valid = false;
  end
  index+=2
  sog = unpack_to_int(normal_record, index, 2)
  if(sog == 65535)
    valid = false;
  end
  index+=2
  cog = unpack_to_int(normal_record,index,2)
  if(cog == 65535)
    valid = false;
  end
  index+=2
  #puts "gps:valid #{valid},hours #{hours},minutes #{minutes},date #{date},lat_deg #{lat_deg},lat_dec_min #{lat_dec_min},lon_deg #{lon_deg},lon_dec_min #{lon_dec_min},altitude #{altitude},sog #{sog},cog #{cog}"
  
  gps_sample = GpsSample.new
  gps_sample.valid = valid;
  gps_sample.hours = hours;
  gps_sample.minutes = minutes;
  gps_sample.date = date;
  gps_sample.lat_deg = lat_deg;
  gps_sample.lat_dec_min =lat_dec_min;
  gps_sample.lon_deg = lon_deg;
  gps_sample.lon_dec_min = lon_dec_min;
  gps_sample.altitude = altitude;
  gps_sample.sog = sog;
  gps_sample.cog = cog;
  gps_sample.latitude = lat_deg.to_f + (lat_dec_min.to_f/10000.0)/60.0;
  gps_sample.longitude = (-1.0)*(lon_deg.to_f + (lon_dec_min.to_f/10000.0)/60.0);
  @gpssamples.push(gps_sample)
end

def parse_compass(record)
  type = parse_type(record)
  normal_record = record
  #puts "type: #{type}"
  # goes in voltage type
  index = 1
  #set the count and then the samples for that count
  rec_count = unpack_to_int(normal_record, index, 2)
  index+=2
  #puts "count #{rec_count}"
  1.upto(12){
    value = unpack_to_int(normal_record, index, 2)
    index+=2
    crec = CompassSample.new
    crec.count = rec_count
    crec.value = value
    
    case type
    when 1
      
      crec.value = crec.value.to_f/1000.0;
      #printf "accelx: #{crec.value}\n"
      @accelxsamples.push(crec);
    when 2
      
      crec.value = crec.value.to_f/1000.0;
      #printf "accely: #{crec.value}\n"
      @accelysamples.push(crec);
    when 3
      crec.value = crec.value.to_f/1000.0;
      #printf "accelz: #{crec.value}\n"
      @accelzsamples.push(crec);
    when 4
      crec.value = crec.value.to_f/10.0;
      #printf "heading: #{crec.value}\n"
      @headingsamples.push(crec);
    when 5
      crec.value = to_signed(crec.value, 16)
      crec.value = crec.value.to_f/10.0;
      #printf "pitch: #{crec.value}\n"
      @pitchsamples.push(crec);
    when 6
      crec.value = to_signed(crec.value, 16)
      crec.value = crec.value.to_f/10.0;
      #printf "roll: #{crec.value}\n"
      @rollsamples.push(crec);
    when 9
      crec.value = crec.value.to_f/10.0;
      #printf "temp: #{crec.value}\n"
      @tempsamples.push(crec);
    else
    end
  }
end

def to_signed(n, length)
  mid = 2**(length-1)
  max_unsigned = 2**length
  return (n>=mid) ? n - max_unsigned : n
end

def parse_type(records) 
  return records[0].to_i(16)
end

def parse_delimiters(variable)
  return variable.split(",")
end

puts "Parsing lines..."

f = File.open(ARGV[0],'r');
directory = ARGV[1]

#puts "type "
f.readline
f.each_line do |line|
  
  #puts "#{line}"
  puts "#{line}"
  tokens = parse_delimiters(line)
  if(ARGV[2] != nil && ARGV[2].include?('h'))
    tokens = parse_hex(tokens)
  else
    tokens.each do |element|
      element = element.to_i
    end
  end
  
  type = parse_type(tokens);
  case type
  when 0 #gps message
    parse_gps(tokens)
  when 255
  else
    parse_compass(tokens)
  end
  #puts parseType(tokens)
end
f.close

#write out the files for this run
now = Time.now.to_s.gsub(" ","").gsub(":", "-")
now << ".csv"
puts now;
#start with the gps
gpsheader = "count:valid:hours:minutes:date:lat_deg:lat_dec_min:lon_deg:lon_dec_min:altitude:sog:cog:latitude:longitude\n"
compassheader = "index:date:count:seqnum:value\n"
puts "Writing out gps..."
#start with the gps
filename = directory + "gps-" + now;
puts filename
f = File.open(filename,"w")
f.write(gpsheader.gsub(":", "\t"))
index = 1;
@gpssamples.each do |sample|
  str = "#{index}\t"<<
    "#{sample.valid}\t"<<
    "#{sample.hours}\t"<<
    "#{sample.minutes}\t"<<
    "#{sample.date}\t"<<
    "#{sample.lat_deg}\t"<<
    "#{sample.lat_dec_min}\t"<<
    "#{sample.lon_deg}\t"<<
    "#{sample.lon_dec_min}\t"<<
    "#{sample.altitude}\t"<<
    "#{sample.sog}\t"<<
    "#{sample.cog}\t"<<
    "#{sample.latitude}\t"<<
    "#{sample.longitude}\n"
  f.write(str);
  index+=1
end
f.close
#voltage
puts "Writing out voltage..."
filename = directory + "voltage-" + now;
puts filename
f = File.open(filename,"w")
f.write(compassheader.gsub(":", "\t"))
index = 1;
@voltagesamples.each do |sample|
  str = "#{index}\t"<<
    "#{sample.date}\t"<<
    "#{sample.count}\t"<<
    "#{sample.seqnum}\t"<<
    "#{sample.value}\n"
  f.write(str);
  index+=1
end
f.close

#accelx
puts "Writing out accelx..."
filename = directory + "accelx-" + now;
puts filename
f = File.open(filename,"w")
f.write(compassheader.gsub(":", "\t"))
index = 1;
@accelxsamples.each do |sample|
  str = "#{index}\t"<<
    "#{sample.date}\t"<<
    "#{sample.count}\t"<<
    "#{sample.seqnum}\t"<<
    "#{sample.value}\n"
  f.write(str);
  index+=1
end
f.close

#accely
puts "Writing out accely..."
filename = directory + "accely-" + now;
puts filename
f = File.open(filename,"w")
f.write(compassheader.gsub(":", "\t"))
index = 1;
@accelysamples.each do |sample|
  str = "#{index}\t"<<
    "#{sample.date}\t"<<
    "#{sample.count}\t"<<
    "#{sample.seqnum}\t"<<
    "#{sample.value}\n"
  f.write(str);
  index+=1
end
f.close

#accelz
puts "Writing out accelz..."
filename = directory + "accelz-" + now;
puts filename
f = File.open(filename,"w")
f.write(compassheader.gsub(":", "\t"))
index = 1;
@accelzsamples.each do |sample|
  str = "#{index}\t"<<
    "#{sample.date}\t"<<
    "#{sample.count}\t"<<
    "#{sample.seqnum}\t"<<
    "#{sample.value}\n"
  f.write(str);
  index+=1
end
f.close

#heading
puts "Writing out heading..."
filename = directory + "heading-" + now;
puts filename
f = File.open(filename,"w")
f.write(compassheader.gsub(":", "\t"))
index = 1;
@headingsamples.each do |sample|
  str = "#{index}\t"<<
    "#{sample.date}\t"<<
    "#{sample.count}\t"<<
    "#{sample.seqnum}\t"<<
    "#{sample.value}\n"
  f.write(str);
  index+=1
end
f.close

#pitch
puts "Writing out pitch..."
filename = directory + "pitch-" + now;
puts filename
f = File.open(filename,"w")
f.write(compassheader.gsub(":", "\t"))
index = 1;
@pitchsamples.each do |sample|
  str = "#{index}\t"<<
    "#{sample.date}\t"<<
    "#{sample.count}\t"<<
    "#{sample.seqnum}\t"<<
    "#{sample.value}\n"
  f.write(str);
  index+=1
end
f.close

#roll
puts "Writing out roll..."
filename = directory + "roll-" + now;
puts filename
f = File.open(filename,"w")
f.write(compassheader.gsub(":", "\t"))
index = 1;
@rollsamples.each do |sample|
  str = "#{index}\t"<<
    "#{sample.date}\t"<<
    "#{sample.count}\t"<<
    "#{sample.seqnum}\t"<<
    "#{sample.value}\n"
  f.write(str);
  index+=1
end
f.close

#temp
puts "Writing out temp..."
filename = directory + "temp-" + now;
puts filename
f = File.open(filename,"w")
f.write(compassheader.gsub(":", "\t"))
index = 1;
@tempsamples.each do |sample|
  str = "#{index}\t"<<
    "#{sample.date}\t"<<
    "#{sample.count}\t"<<
    "#{sample.seqnum}\t"<<
    "#{sample.value}\n"
  f.write(str);
  index+=1
end
f.close
puts "Done!"

#!/usr/bin/ruby
require 'rubygems'
require 'date'
require 'time'
require 'serialport'

if ARGV.size < 2
	STDERR.print <<EOF
	Usage: ruby #{$0} port bps > file.to.output.data
EOF
	exit(1)
end
puts ARGV[0], ARGV[1]
sp = SerialPort.new("#{ARGV[0]}", ARGV[1].to_i, 8,1,SerialPort::NONE)
queue = Queue.new

producer = Thread.new {
str = ""
loop  do
	val = sp.gets()
	if val == nil
	break
	else
	print val
	end
end
}
producer.join
puts "closing socket.."
sp.close

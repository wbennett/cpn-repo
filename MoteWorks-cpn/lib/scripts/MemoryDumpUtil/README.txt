%
% README.txt
% AUTHOR:wbennett
% VERSION:0.1
% EMAIL: wbennett@cse.unl.edu
% 
%
%

**NOTE*** If you run into missing require errors, you probably are missing ruby gems. Google
the require for these gems. To install a gem use gem install <gem name>

The application that dumps the memory is in the folder FlashDumpApp. Load
this program on to the mote needed memory dumped. Then run the serial_logger.rb and pipe the output into a text file. Example (ruby ./serial_logger.rb /dev/ttyUSB2 115200 > dumpdata.txt). Then run the parser and modify the parsing of it according to the storage.h file. The code should be self explanatory.

The parser is in the DumpParser directory, the file main.rb is the actual script. 


Email if you have questions.

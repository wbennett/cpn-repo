#!/usr/bin/ruby

require 'rubygems'
require 'builder'
require 'date'
require 'time'
require 'matlab'

class CraneRecord
	attr_accessor :time, :lat, :lon
end

class Crane
	attr_accessor :ptt_id, :records
	def initialize(id= nil , records = nil)
		if records == nil
			records = []
		end
	end
end

cranes = Hash.new

file = File.open(ARGV[0],'r')

#chomp first line
line = file.gets
#read through each line
while line = file.gets
	#the format for the csv is the following
	#id, date(yyyy-mm-dd),lat,lon
	tokens = line.split(',')
	if cranes[tokens[0]] == nil
		#make a new one
		cranes[tokens[0]] = Crane.new
		cranes[tokens[0]].ptt_id = tokens[0]
		cranes[tokens[0]].records = []
	end
	#make a new record from the line
	record = CraneRecord.new
	record.time = Time.parse(tokens[1].to_s).to_i 
	record.lat = tokens[2].to_f
	record.lon = tokens[3].to_f
	cranes[tokens[0]].records << record
end
file.close

#print out the kml for each crane 
cranes.each do |cranename, crane|

	xml = Builder::XmlMarkup.new(:indent => 2)
	xml.instruct! :xml
	xml.kml("kmlns" => "http://www.opengis.net/kml/2.2") {
		xml.Document {
			xml.name("#{crane.ptt_id}")
			xml.open("1")
			xml.description("Crane #{crane.ptt_id} known locations")
			xml.Folder {
				crane.records.each do |r|
					xml.Placemark {
						xml.name("#{r.time}")
						xml.description("#{crane.ptt_id} was here at #{Time.at(r.time)}")
						xml.Point {
							xml.coordinates("#{r.lon},#{r.lat},4")
						}
					}
				end
			}
		}
	}
	File.open("./#{crane.ptt_id}.kml",'w') {
		|fo|
		str = xml.to_s
		str = str.gsub('<to_s/>','')
		fo.write(str)
	}
end

#save in matlab format for each crane
engine = Matlab::Engine.new

cranes.each do |cranename, crane|
	matrix = Matlab::Matrix.new(3, crane.records.length)
	(0..2).each { |m|
		(0..crane.records.length-1).each {
			|n|
			case m
			when 0
				matrix[m,n] = crane.records[n].time
			when 1
				matrix[m,n] = crane.records[n].lat
			when 2
				matrix[m,n] = crane.records[n].lon
			end
		}
	}
	engine.put_variable "Crane#{crane.ptt_id.to_s}", matrix
end

engine.save "./position-data.mat"
engine.close



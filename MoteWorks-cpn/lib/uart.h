#ifndef uart_h
#define uart_h
#include <avr/io.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include "pins.h"
#define F_CPU (8000000)
#define clockCyclesPerMicrosecond() ( F_CPU / 1000000L )
#define clockCyclesToMicroseconds(a) ((a) / clockCyclesPerMicrosecond())
#include <inttypes.h>
static uint8_t _uart_rx_pin;
static uint8_t _uart_tx_pin;
static uint8_t _uart_buad_rate;
static int _bit_period;
void uart_init(uint8_t,uint8_t);
void printNumber(unsigned long, uint8_t);
void uart_begin(long);
int read_byte();
void print_char(char);
void print_str(const char[]);
void print_int(int);
void print_uint(unsigned int);
void print_long(long);
void print_ulong(unsigned long);
void print_base(long,int);
void println(void);
void println_char(char);
void println_str(const char[]);
void println_byte(uint8_t);
void println_int(int);
void println_long(long);
void println_ulong(unsigned long);
void println_base(long,int);
void digitalWrite(uint8_t,uint8_t);
uint8_t digitalRead(uint8_t pin); 
void delayMicroseconds(unsigned int us);

static inline void turnOffPWM(uint8_t timer) __attribute__ ((always_inline));
static inline void turnOffPWM(uint8_t timer)
{
  if (timer == TIMER1A) cbi(TCCR1A, COM1A1);
  if (timer == TIMER1B) cbi(TCCR1A, COM1B1);
  if (timer == TIMER0A) cbi(TCCR0A, COM0A1);
  if (timer == TIMER0B) cbi(TCCR0A, COM0B1);
  if (timer == TIMER2A) cbi(TCCR2A, COM2A1);
  if (timer == TIMER2B) cbi(TCCR2A, COM2B1);
  if (timer == TIMER3A) cbi(TCCR3A, COM3A1);
  if (timer == TIMER3B) cbi(TCCR3A, COM3B1);
  if (timer == TIMER3C) cbi(TCCR3A, COM3C1);
  if (timer == TIMER4A) cbi(TCCR4A, COM4A1);
  if (timer == TIMER4B) cbi(TCCR4A, COM4B1);
  if (timer == TIMER4C) cbi(TCCR4A, COM4C1);
  if (timer == TIMER5A) cbi(TCCR5A, COM5A1);
  if (timer == TIMER5B) cbi(TCCR5A, COM5B1);
  if (timer == TIMER5C) cbi(TCCR5A, COM5C1);
}
void delayMicroseconds(unsigned int us) {	
	// calling avrlib's delay_us() function with low values (e.g. 1 or
  // 2 microseconds) gives delays longer than desired.
  //delay_us(us);

#if F_CPU >= 16000000L
  // for the 16 MHz clock on most Arduino boards

  // for a one-microsecond delay, simply return.  the overhead
  // of the function call yields a delay of approximately 1 1/8 us.
  if (--us == 0)
    return;

  // the following loop takes a quarter of a microsecond (4 cycles)
  // per iteration, so execute it four times for each microsecond of
  // delay requested.
  us <<= 2;

  // account for the time taken in the preceeding commands.

 -= 2;
#else
  // for the 8 MHz internal clock on the ATmega168

  // for a one- or two-microsecond delay, simply return.  the overhead of
  // the function calls takes more than two microseconds.  can't just
  // subtract two, since us is unsigned; we'd overflow.
  if (--us == 0)
    return;
  if (--us == 0)
    return;

  // the following loop takes half of a microsecond (4 cycles)
  // per iteration, so execute it twice for each microsecond of
  // delay requested.
  us <<= 1;

  // partially compensate for the time taken by the preceeding commands.
  // we can't subtract any more than this or we'd overflow w/ small delays.
  us--;
#endif

  // busy wait
  __asm__ __volatile__ (
    "1: sbiw %0,1" "\n\t" // 2 cycles
    "brne 1b" : "=w" (us) : "0" (us) // 2 cycles
  );

}

#define LOW (0x0)
#define HIGH (0x1)
#define MAKE_OUTPUT TOSH_MAKE_PW6_OUTPUT()
#define MAKE_INPUT TOSH_MAKE_PW7_INPUT()
#define WRITE_BIT TOSH_SET_PW6_PIN()
#define CLEAR_BIT TOSH_CLR_PW6_PIN()
#define READ_BIT TOSH_READ_PW7_PIN()

void digitalWrite(uint8_t pin, uint8_t val) {
	uint8_t timer;
	uint8_t bit;
	uint8_t port;
	volatile uint8_t *out;
	timer = digitalPinToTimer(pin);
	bit = digitalPinToBitMask(pin);
	port = digitalPinToBitMask(pin);
	if(port == NOT_A_PIN) return;
	//if the pin suports pwm turn it off
	if(timer != NOT_ON_TIMER) turnOffPWM(timer);
	
	//out = portOutputRegister(port);
	if(val == LOW) {
		uint8_t oldSREG = SREG;
		cli();
		*out &= ~bit;
		SREG = oldSREG;
		
		//CLEAR_BIT;
	} else {
	
		uint8_t oldSREG = SREG;
		cli();
		*out |= bit;
		SREG = oldSREG;
		
		//WRITE_BIT;
	}
}

uint8_t digitalRead(uint8_t pin) {
	uint8_t timer;
	uint8_t bit;
	uint8_t port;
	timer = digitalPinToTimer(pin);
	bit = digitalPinToBitMask(pin);
	port = digitalPinToPort(pin);
	
	if(port == NOT_A_PIN) return LOW;
	
	//if the bpin that support PWM output, we need to turn it off
	//before reading
	if(timer != NOT_ON_TIMER) turnOffPWM(timer);

	if(*portInputRegister(port) & bit) return HIGH;
		return LOW;
	//return READ_BIT;
}
void uart_init(uint8_t receivePin, uint8_t transmitPin) {
	_uart_rx_pin = receivePin;
	_uart_tx_pin = transmitPin;
	_uart_buad_rate = 0;
	//MAKE_OUTPUT;
	//MAKE_INPUT;
}

void uart_begin(long speed) {
	_uart_buad_rate = speed;
	_bit_period = 1000000 /_uart_buad_rate;
	
	digitalWrite(_uart_tx_pin, HIGH);
	delayMicroseconds(_bit_period);//if low signifies the end
}
uint8_t uart_read() {
	uint8_t val;
  int bitDelay = _bit_period - clockCyclesToMicroseconds(50);
	val = 0;
 
  // one byte of serial data (LSB first)
  // ...--\    /--\/--\/--\/--\/--\/--\/--\/--\/--...
  //   \--/\--/\--/\--/\--/\--/\--/\--/\--/
  //  start  0   1   2   3   4   5   6   7 stop

  while (digitalRead(_uart_rx_pin));

  // confirm that this is a real start bit, not line noise
  if (digitalRead(_uart_rx_pin) == LOW) {    // frame start indicated by a falling edge and low start bit
		int offset;
    // jump to the middle of the low start bit
    delayMicroseconds(bitDelay / 2 - clockCyclesToMicroseconds(50));

    // offset of the bit in the byte: from 0 (LSB) to 7 (MSB)
    for (offset = 0; offset < 8; offset++) {
  		// jump to middle of next bit
  		delayMicroseconds(bitDelay);

  		// read bit
  		val |= digitalRead(_uart_rx_pin) << offset;
    }
    delayMicroseconds(_bit_period);
		//read bit
    return val;
  }
 
  return -1;

}

void print_byte(uint8_t b) {
	int bitDelay;
	uint8_t mask;
	if(_uart_buad_rate == 0) return;
	bitDelay = _bit_period = clockCyclesToMicroseconds(50);//a digital write about 50 clock cycles
	digitalWrite(_uart_tx_pin, LOW);
	delayMicroseconds(bitDelay);

	for(mask = 0x01; mask; mask <<=1) {
		if(b & mask) {
			digitalWrite(_uart_tx_pin,HIGH);//send 1
		}
		else {
			digitalWrite(_uart_rx_pin,LOW);//send 0
		}
		delayMicroseconds(bitDelay);
	}
	digitalWrite(_uart_tx_pin, HIGH);
	delayMicroseconds(bitDelay);
}

void print_str(const char *s) {
	while(*s) {
		print_byte(*s++);
	}
}
void print_char(char c) {
	print_byte((uint8_t)c);
}
void print_int(int n) {
	print_long((long) n);
}
void print_uint(unsigned int n) {
	print_ulong((unsigned long) n);	
}
void print_long(long n) {
	if(n < 0) {
		print_byte('-');
		n = -n;
	}
	printNumber(n,10);
}

void print_ulong(unsigned long n) {
	printNumber(n,10);
}

void print_base(long n, int base) {
	if(base == 0) {
		print_byte((char) n);
	}else if(base == 10) {
		print_byte(n);
	}else {
		printNumber(n, base);
	}
}
void println(void) {
	print_byte('\r');
	print_byte('\n');
}
void println_char(char c) {
	print_char(c);
	println();
}
void println_str(const char c[]) {
	print_str(c);
	println();
}

void println_byte(uint8_t b) {
	print_byte(b);
	println();
}

void println_int(int n) {
	print_int(n);
	println();
}

void println_long(long n) {
	print_long(n);
	println();
}

void println_ulong(unsigned long n) {
	print_ulong(n);
	println();
}

void println_base(long n, int base) {
	print_base(n,base);
	println();
}

void printNumber(unsigned long n, uint8_t base) {
	unsigned char buf[8*sizeof(long)];
	unsigned long i =0;
	if(n == 0) {
		print_byte('0');
		return;
	}

	while(n>0) {
		buf[i++] = n % base;
		n /= base;
	}

	for(;i>0;i--) {
		print_byte((char) (buf[i-1] <10 ? '0' + buf[i-1] : 'A' + buf[i-1] -10));
	}
}
#endif

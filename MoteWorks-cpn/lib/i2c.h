#ifndef i2c_h
#define i2c_h

#include <stdio.h>
#include <string.h>
#include "twi_func.h"
//#include "twi.h"
//don't know if we need this outside tinyos...
//extern int sprintf(char *_s,const char*__fmt, ...)__attribute__((C));
static uint8_t rxBuffer[BUFFER_LENGTH];
static uint8_t rxBufferIndex = 0;
static uint8_t rxBufferLength = 0;

static uint8_t txAddress;
static uint8_t txBuffer[BUFFER_LENGTH];
static uint8_t txBufferIndex = 0;
static uint8_t txBufferLength = 0;

static uint8_t transmitting;
static bool isEnabled = FALSE;
void i2c_begin();
uint8_t i2c_requestFrom(uint8_t address, uint8_t quantity); 
uint8_t i2c_endTransmission();
void i2c_send_byte(uint8_t data);
uint8_t available();
uint8_t i2c_recv_byte();
void enable_i2c();
void disable_i2c();
int8_t i2c_printf(const uint8_t *format, ...); 

void i2c_begin() {
	rxBufferIndex = 0;
	rxBufferLength = 0;
	txBufferIndex = 0;
	txBufferLength = 0;

	twi_init();
}

uint8_t i2c_requestFrom(uint8_t address, uint8_t quantity) {
	uint8_t read;
	if(quantity > BUFFER_LENGTH) {
		quantity = BUFFER_LENGTH;
	}
	read = twi_readFrom(address,rxBuffer, quantity);
	rxBufferIndex = 0;
	rxBufferLength = read;
	return read;
}

void i2c_beginTransmission(uint8_t address) {
	// indicate that we are transmitting
	transmitting = 1;
	txAddress = address;
	txBufferIndex = 0;
	txBufferLength = 0;
}

uint8_t i2c_endTransmission() {
	int8_t ret;
	ret= twi_writeTo(txAddress, txBuffer, txBufferLength, 1);
	txBufferIndex = 0;
	txBufferLength = 0;
	//indicate that we are done transmitting
	transmitting = 0;
	return ret;
}

void i2c_send_byte(uint8_t data) {
	if(transmitting) {
		if(txBufferLength >= BUFFER_LENGTH) {
			return;
		}
		txBuffer[txBufferIndex] = data;
		++txBufferIndex;
		txBufferLength = txBufferIndex;
	}
}

uint8_t available() {
	atomic return rxBufferLength - rxBufferIndex;
}

uint8_t i2c_recv_byte() {
	uint8_t value = '\0';
	// get each successive byte on each call
	if(rxBufferIndex < rxBufferLength) {
		value = rxBuffer[rxBufferIndex];
		++rxBufferIndex;
	}
	return value;
}
//these macros go down to the avr calls, so lets be
//dry in this case...
#define I2C_ENABLE TOSH_MAKE_PW2_OUTPUT();\
				TOSH_SET_PW2_PIN()

#define I2C_DISABLE TOSH_CLR_PW2_PIN()
#define DEBUG_ADDR (42 >> 1)
#define I2C_MAX_BUFFER (512)
void enable_i2c() {
	//enable the hardware...
	I2C_ENABLE;
	TOSH_uwait(10);
	isEnabled = TRUE;
}
void disable_i2c() {
		//disable the hardware
		I2C_DISABLE;
		isEnabled = FALSE;
}
int8_t i2c_printf(const uint8_t *format, ...) {
	char out_buffer[I2C_MAX_BUFFER];
	uint16_t i,k;
	int ret;
	va_list ap;
	i =0;
	k =0;
	if(isEnabled == FALSE) {
		enable_i2c();
		i2c_begin();
	}
	//clear the previous memory out...
	memset(out_buffer,'\0',I2C_MAX_BUFFER);
	//access the parameters
	va_start(ap,format);
	//format nicely -- this is expensive but i don't feel like
	//implementing printf...
	ret = sprintf(out_buffer,format,ap);
	va_end(ap);
	//activate the twi registers init vars
		//send the data in 32 byte chunks
	 	while(k < strlen(out_buffer)) {
			//regular i2c protocol...
			i2c_beginTransmission(DEBUG_ADDR);
			for(i=0;(k<strlen(out_buffer) && i<TWI_BUFFER_LENGTH);i++) {
				i2c_send_byte(out_buffer[k]);
				k++;
			}
			i2c_endTransmission();
	 	}
	return ret;
}
#endif

% This file has some generic system notes.
% Author:wbennett
%
%
%
UART0
	- GSM uses this bus 
	- Shared functionality with the GSM unit
	- Shared functionality with the RADIO
	- Shared functionality with the Flash Memory
	- SODebug.h uses this bus
	- Shared functionality with anything that uses UART0, SPI, MOSI/MISO

UART1
	- GPS uses this bus
	- SODebug1.h uses this bus

I2C
	- Compass uses this bus
	- Pressure sensor uses this bus

SYSTEM_WIDE
	- TinyOS for the iris doesn't implement the 38400 baud rate, so the uart1.extra is required to be
		dropped into your make/avr directory. ONLY in the Moteworks environment.

#ifndef debug_constants_h
#define debug_constants_h
//#define USESODBG 
//#define I2CDBG

#ifdef I2CDBG

#include <stdarg.h>
#include <stdio.h>
#warning "Using I2C Debugger I2CDBG_WIRING in component file and I2CDBG_CONFIG in the module file..."
#define I2C_BUFFER_SIZE (256)
static char dbg_buffer[I2C_BUFFER_SIZE];
static uint16_t dbg_read_pointer;
static uint16_t dbg_write_pointer;
static int16_t dbg_data_size;
void (*full_event)(void) = NULL;//function pointer to full event
#define DBG_SPECIAL_CHAR (0x20)
#define I2CDBG_WIRING(upper,module)	components DebugI2CC;\
											upper.StdControl -> DebugI2CC.StdControl;\
											module.flush -> DebugI2CC.flush
#define I2CDBG_CONFIG() command result_t flush()

											//sprintf(dbg_buffer,__VA_ARGS__);
											//call i2cPrint(dbg_buffer)

#define mult(...) twi_printf(__VA_ARGS__);\
									call flush();
#define dbgline3 "[file %s, line %d ]: "
#define dbgargs3 __FILE__, __LINE__
#define debug3(...)	mult(__VA_ARGS__)
#define dbgline2 "[file %s, line %d, funct %s ] "
#define dbgargs2 __FILE__, __LINE__, __FUNCTION__
#define debug2(...) mult(__VA_ARGS__)

void twi_push_char(char c) {
	//increase write pointer, check if at end
	if(++dbg_write_pointer >= I2C_BUFFER_SIZE) dbg_write_pointer = 0;
	dbg_buffer[dbg_write_pointer] = c;
	dbg_data_size++;
}

char twi_pull_char(void) {
	char rval;
	if(++dbg_read_pointer >= I2C_BUFFER_SIZE) dbg_read_pointer = 0;
	rval = dbg_buffer[dbg_read_pointer];
	dbg_buffer[dbg_read_pointer] = DBG_SPECIAL_CHAR;
	dbg_data_size--;
	return rval;
}

int8_t twi_buffer_full(void) {
	return dbg_read_pointer == dbg_write_pointer &&
					dbg_data_size == I2C_BUFFER_SIZE;
}

void twi_attach_full_event(void (*handler)(void)) {
	full_event = handler;
}

int8_t twi_buffer_empty(void) {
	return dbg_read_pointer == dbg_write_pointer &&
					dbg_data_size == 0;
}

int twi_printf(const char*fmt, ...) {
	char buf[256];
	uint8_t max;
	uint8_t i;
	uint8_t ret;
	va_list ap;
	va_start(ap,fmt);
	ret = vsprintf(buf,fmt,ap);
	va_end(ap);
	max = strlen(buf);
	for(i = 0;i<max;i++) {
		if(twi_buffer_full() == TRUE && full_event != NULL) {
			//handle the full event so the user can pull it out...
			full_event();
		}else {
			twi_push_char(buf[i]);
		}
	}
	return SUCCESS;
}
#elif defined USESODBG1

#include <stdarg.h>
#include <stdio.h>
#include "SOdebug1.h"
#warning "Using UART Debugger"
static char dbg_buffer[256];
#define mult(...) sprintf(dbg_buffer,__VA_ARGS__);\
											 SODbg(1,"%s",dbg_buffer)
#define dbgline3 "[file %s, line %d ]: "
#define dbgargs3 __FILE__, __LINE__
#define debug3(...)	mult(__VA_ARGS__)
#define dbgline2 "[file %s, line %d, funct %s ] "
#define dbgargs2 __FILE__, __LINE__, __FUNCTION__
#define debug2(...) mult(__VA_ARGS__)

#elif defined USESODBG0

#include <stdarg.h>
#include <stdio.h>
#include "SOdebug0.h"
#warning "Using UART Debugger"
static char dbg_buffer[256];
#define mult(...) sprintf(dbg_buffer,__VA_ARGS__);\
											 SODbg(1,"%s",dbg_buffer)
#define dbgline3 "[file %s, line %d ]: "
#define dbgargs3 __FILE__, __LINE__
#define debug3(...)	mult(__VA_ARGS__)
#define dbgline2 "[file %s, line %d, funct %s ] "
#define dbgargs2 __FILE__, __LINE__, __FUNCTION__
#define debug2(...) mult(__VA_ARGS__)

#elif defined USESODBG



#include <stdarg.h>
#include <stdio.h>
#include "SOdebug0.h"
#warning "Using UART Debugger"
static char dbg_buffer[256];
#define mult(...) sprintf(dbg_buffer,__VA_ARGS__);\
											 SODbg(1,"%s",dbg_buffer)
#define dbgline3 "[file %s, line %d ]: "
#define dbgargs3 __FILE__, __LINE__
#define debug3(...)	mult(__VA_ARGS__)
#define dbgline2 "[file %s, line %d, funct %s ] "
#define dbgargs2 __FILE__, __LINE__, __FUNCTION__
#define debug2(...) mult(__VA_ARGS__)

#elif defined USERADIODBG

#include <stdarg.h>
#include <stdio.h>
#warning "Using Radio Debugger RADIODBG_WIRING in component file and RADIODBG_CONFIG in the module file..."
#define RADIO_BUFFER_SIZE 	(60)

static char dbg_buffer[RADIO_BUFFER_SIZE];
static uint16_t dbg_read_pointer;
static uint16_t dbg_write_pointer;
static int16_t dbg_data_size;
void (*full_event)(void) = NULL;//function pointer to full event

#define DBG_SPECIAL_CHAR 	 (0x20)

#define RADIODBG_WIRING(module)		components RadioPrintf;\
																	module.flush -> RadioPrintf.flush

#define RADIODBG_CONFIG()		command result_t flush()

#define mult(...) radio_printf(__VA_ARGS__);\
									call flush()

#define dbgline3 "[file %s, line %d ]: "
#define dbgargs3 __FILE__, __LINE__
#define debug3(...)	mult(__VA_ARGS__)
#define dbgline2 "[file %s, line %d, funct %s ] "
#define dbgargs2 __FILE__, __LINE__, __FUNCTION__
#define debug2(...) mult(__VA_ARGS__)

void radio_push_char(char c) {
	//increase write pointer, check if at end
	if(++dbg_write_pointer >= RADIO_BUFFER_SIZE) dbg_write_pointer = 0;
	dbg_buffer[dbg_write_pointer] = c;
	dbg_data_size++;
}

char radio_pull_char(void) {
	char rval;
	if(++dbg_read_pointer >= RADIO_BUFFER_SIZE) dbg_read_pointer = 0;
	rval = dbg_buffer[dbg_read_pointer];
	dbg_buffer[dbg_read_pointer] = DBG_SPECIAL_CHAR;
	dbg_data_size--;
	return rval;
}

int8_t radio_buffer_full(void) {
	return dbg_read_pointer == dbg_write_pointer &&
					dbg_data_size == RADIO_BUFFER_SIZE;
}

void radio_attach_full_event(void (*handler)(void)) {
	full_event = handler;
}

int8_t radio_buffer_empty(void) {
	return dbg_read_pointer == dbg_write_pointer &&
					dbg_data_size == 0;
}

int radio_printf(const char*fmt, ...) {
	char buf[RADIO_BUFFER_SIZE];
	uint8_t max;
	uint8_t i;
	uint8_t ret;
	va_list ap;
	va_start(ap,fmt);
	ret = vsprintf(buf,fmt,ap);
	va_end(ap);
	max = strlen(buf);
	for(i = 0;i<max;i++) {
		if(radio_buffer_full() == TRUE && full_event != NULL) {
			//handle the full event so the user can pull it out...
			full_event();
		}else {
			radio_push_char(buf[i]);
		}
	}
	return SUCCESS;
}


#else

#warning "using simulator"
#define mult(...) printf(__VA_ARGS__)
#define dbgline3 "[file %s, line %d ]: "
#define dbgargs3 __FILE__, __LINE__
#define debug3(...)	dbg(DBG_USR3, __VA_ARGS__)
#define dbgline2 "[file %s, line %d, funct %s ] "
#define dbgargs2 __FILE__, __LINE__, __FUNCTION__
#define debug2(...) dbg(DBG_USR2, __VA_ARGS__)

#endif

#define var_to_str(variable) ""#variable""
#define debug_state(_fmt, ...)	debug3(dbgline3 _fmt, dbgargs3, __VA_ARGS__)
#define debug_trace()	debug2(dbgline2 "\n", dbgargs2)
#define dprintf(_fmt, ...) mult(_fmt, __VA_ARGS__) 
#define lcd(_fmt, ...) debug2(">>" _fmt "<<",__VA_ARGS__)

#endif

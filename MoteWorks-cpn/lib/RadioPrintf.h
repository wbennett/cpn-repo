#ifndef RADIO_PRINTF_H
#define RADIO_PRINTF_H
/*
 * this is the message type for the printf messages
 *
 *
 */
enum {
	AM_RADIO_PRINTF_MSG = 0x63,
};
#define RADIO_PRINTF_MSG_SIZE (TOSH_DATA_LENGTH)
typedef struct radio_printf_t {
	char buffer[RADIO_PRINTF_MSG_SIZE];
}__attribute__ ((packed)) radio_printf_t;


#endif

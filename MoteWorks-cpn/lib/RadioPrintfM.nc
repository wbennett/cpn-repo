/*
 * Copyright (c) 2002-2005 Intel Corporation
 * Copyright (c) 2000-2005 The Regents of the University of California
 * All rights reserved.
 * See license.txt file included with the distribution.
 *
 * $Id: BlinkM.nc,v 1.1.4.1 2007/04/26 19:35:47 njain Exp $
 */

/**
 * Implementation for Blink application.  Toggle the red LED when a
 * Timer fires.
 **/
#include <stdio.h>
#include "debug_constants.h"
#include "RadioPrintf.h"
module RadioPrintfM {
  provides {
    interface StdControl;
		command result_t flush();
  }
  uses {
    interface Leds;
		interface StdControl as RadioControl;
		interface SendMsg;
  }
}
implementation {
	
	bool isOn = FALSE;
	bool isBusy = FALSE;
	bool isSent = TRUE;
	char currentBuffer[RADIO_PRINTF_MSG_SIZE];
	TOS_Msg lmsg;
	uint8_t failCount = 0;
	void full_event_handler();
	task void printBufferTask();
	
	void full_event_handler() {
		call flush();
	}
	task void printBufferTask() {
		static uint16_t i;
		//start the radio
		if(!isBusy) {
			if(isOn == FALSE) {
				isOn = TRUE;
				call Leds.greenToggle();
				call RadioControl.start();
			}
			if(isSent) {
				//send the next RADIO_PRINTF_MSG_SIZE bytes in the buffer
				//memset(currentBuffer,'\0',RADIO_PRINTF_MSG_SIZE);
				//pack the buffer and send with an ack...
				for(i=0;i<RADIO_PRINTF_MSG_SIZE;i++) {
					if(radio_buffer_empty() == TRUE) {			
						break;
					}else {
						currentBuffer[i] = radio_pull_char();
					}
				}
				//copy into the message buffer
				memcpy(lmsg.data,currentBuffer,i);
				//send the message....
			}

			if(call SendMsg.send(0,RADIO_PRINTF_MSG_SIZE,&lmsg) == FAIL) {
				isBusy = FALSE;
				if(failCount++ < 1) {
					post printBufferTask();
					isSent = FALSE;
				}else {
					isSent = TRUE;
					failCount = 0;
				}	
			}else {
				isSent = TRUE;
			}
		}else if (radio_buffer_empty() == FALSE){
			post printBufferTask();
		}
	}

	command result_t StdControl.init() {
		radio_attach_full_event(full_event_handler);
		call RadioControl.init();
		call Leds.init(); 
    return SUCCESS;
  }

	command result_t flush() {
		post printBufferTask();
		return SUCCESS;
	}
	

  command result_t StdControl.start() {
		return SUCCESS;
  }

  command result_t StdControl.stop() {
    return SUCCESS; 
  }

	/*
	 * Radio message
	 *
	 */
	event result_t SendMsg.sendDone(TOS_MsgPtr msg, result_t success) {
		
		isBusy = FALSE;
		if(success == SUCCESS) {
			call Leds.greenToggle();
		}else {
			//call Leds.yellowToggle();
		}
		if(radio_buffer_empty() == FALSE) {
			post printBufferTask();
		}else {
			call Leds.yellowToggle();
			call RadioControl.stop();
			isOn = FALSE;
		}
		return SUCCESS;
	}

}



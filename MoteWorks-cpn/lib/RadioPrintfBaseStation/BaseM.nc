/*
 * BaseM.nc
 *
 * Sends an 8-bit global struct orientation
 * <header>Data<header>
 *
 **/
#include "RadioPrintf.h" 
#define DBG_PKT  1
#define SO_DEBUG  1

#include "SOdebug.h"
#include <string.h>
module BaseM {
  provides interface StdControl;
  uses {
    interface Leds;
    interface ReceiveMsg;
  }
}
implementation {

	
	command result_t StdControl.init() {
    call Leds.init();     
		call Leds.redToggle();
    return SUCCESS;
  }


  command result_t StdControl.start() {
    return SUCCESS;
  }

  command result_t StdControl.stop() {
    return SUCCESS;
  }
  
  event TOS_MsgPtr ReceiveMsg.receive(TOS_MsgPtr m) {
    radio_printf_t *message = (radio_printf_t*)m->data;

		call Leds.redToggle();
		SODbg( DBG_PKT, "%s",message->buffer);
    return m;
  }
   
}


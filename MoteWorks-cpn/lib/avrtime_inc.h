#ifndef AVRTIME_INC_H
#define AVRTIME_INC_H
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#warning "Time library is included the time is in binary milliseconds 1 second is 1/1024 ms"
//--helper macros
#define BMS_TO_MS(bms) (bms*(1000/1024)) 
#define MS_TO_BMS(ms) (ms*(1024/1000))

#define DAYS_TO_MS(days) (days*(uint32_t)86400000u)
#define HOURS_TO_MS(hours) (hours*(uint32_t)3600000u)
#define MIN_TO_MS(min) (min*(uint32_t)60000u)
#define SEC_TO_MS(sec) (sec*(uint16_t)1000u)
#define DAYS_TO_BMS(days) (days*(uint32_t)88473600u)
#define HOURS_TO_BMS(hours) (hours*(uint32_t)3686400u)
#define MIN_TO_BMS(min) (min*(uint32_t)61440u)
#define SEC_TO_BMS(sec) (sec*(uint16_t)1024u)

//end---
struct avr_tm{
	int8_t sec;//0-59
	int8_t min;//0-59
	int8_t hour;//0-23
	int8_t day;//1-31
	int8_t mon;//0-11
	int16_t	year;//1900
	int8_t wday;//0-7
	int16_t	day_of_year;//[0-365]
	uint8_t is_dst;
	uint8_t hundreth;
};//ex_tm ={0,0,0,0,0,0,0,0,0,0};
typedef uint64_t avrtime_t;

uint8_t rtcRead(struct avr_tm *timeptr);
void set_time_millis(avrtime_t set_time);

void add_time_millis(uint32_t time_to_add);

avrtime_t get_time_millis();

avrtime_t *get_time_millis_ptr();

avrtime_t get_start_time_millis();

void reset_start_time();

uint64_t get_elapsed_time_millis();

struct avr_tm * avr_gmtime(avrtime_t *lTime);

struct avr_tm * avr_localtime(avrtime_t *lTime);

char *avr_ctime(avrtime_t * lTime);

char *avr_asctime(struct avr_tm *lTime);

avrtime_t avr_mktime(struct avr_tm *timeptr);

uint64_t diffTime_ms(avrtime_t *time1,avrtime_t *time2);
uint64_t diffTime_tm(struct avr_tm *time1,struct avr_tm *time2);

bool is_time_valid(int8_t sec, 
					int8_t minutes,
					int8_t hours,
				int8_t day,
					int8_t month,
					int16_t year);
bool is_time_valid_tm(struct avr_tm *ltime);
void parse_date(uint32_t date, struct avr_tm *result_tm);

//helpers
uint32_t get_time_of_day(struct avr_tm* ltime);
//static void checkTime(struct avr_tm *timeptr);

#define TRUE (1)
#define FALSE (0)
#endif

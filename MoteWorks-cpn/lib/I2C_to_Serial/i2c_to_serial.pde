#include <Wire.h>

void setup() {
  Wire.begin(42>>1);
  Wire.onReceive(receiveEvent);
  Serial.begin(115200);
}

void loop() {
  delay(10);
}

void receiveEvent(int count) {
  while(Wire.available() ) {
    char c = Wire.receive();
    Serial.print(c);
  }
}

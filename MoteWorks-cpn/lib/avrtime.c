/*
 *  avrtime.c
 *  AvrTime
 *
 *  Created by William Bennett on 2/4/11.
 *  Copyright 2011 UNL. All rights reserved.
 */

#include "avrtime.h"
#if defined(PLATFORM_PC)
#warning "INIT THE SYSTEM TIME WITH THE CORRECT EPOCH"
static avrtime_t start_time[1000];
static avrtime_t sys_time[1000];
#else
static avrtime_t start_time;
static avrtime_t sys_time;
static bool isInit = FALSE;
#endif

void set_time_millis(avrtime_t set_time) {
#if defined(PLATFORM_PC)
	sys_time[NODE_NUM] = set_time;
#else
	atomic sys_time = set_time;
#endif

}

void init_avrtime() {
	if(isInit == FALSE) {
		avrtime_t t = UINT64_C(132489216000);
		isInit = TRUE;
		set_time_millis(t);
		reset_start_time();
	}
}

void add_time_millis(uint32_t time_to_add) {
#if defined(PLATFORM_PC)
	 sys_time[NODE_NUM] += time_to_add;
	#else
	 sys_time += time_to_add;
	#endif
}

avrtime_t get_time_millis() {
	#if defined(PLATFORM_PC)
	 return sys_time[NODE_NUM];
	#else
	 atomic return sys_time;
	#endif
}

avrtime_t *get_time_millis_ptr() {
	#if defined(PLATFORM_PC)
	return &(sys_time[NODE_NUM]);
	#else
	return &sys_time;
	#endif
}

avrtime_t get_start_time_millis() {
	#if defined(PLATFORM_PC)
	return start_time[NODE_NUM];
	#else
	return start_time;
	#endif
}

void reset_start_time() {
	#if defined(PLATFORM_PC)
	start_time[NODE_NUM] = get_time_millis();
	#else
	start_time = get_time_millis();
	#endif
}

uint64_t get_elapsed_time_millis(){
	return get_time_millis() - get_start_time_millis();
}


static char monthDays[]={31,28,31,30,31,30,31,31,30,31,30,31};
char * __month[]={"Jan","Feb","Mar","Apr","May","Jun",
				"Jul","Aug","Sep","Oct","Nov","Dec"};
char * __day[]={"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};

static char ascTimeBuffer[32];

//validate the avr_tm structure
static void checkTime(struct avr_tm *timeptr) {
	if (timeptr->sec>59)timeptr->sec=0;
	if (timeptr->sec<0)timeptr->sec = 0;
	if (timeptr->min>59)timeptr->min=0;
	if (timeptr->min<0)timeptr->min=0;
	if (timeptr->hour>23)timeptr->hour=0;
	if (timeptr->hour<0)timeptr->hour=0;
	if (timeptr->wday>6)timeptr->wday=0;
	if (timeptr->wday<0)timeptr->wday=0;
	//if (timeptr->day<1)timeptr->day=1;
	if (timeptr->day < 1) timeptr->day = 1;
	else if(timeptr->day > 31)timeptr->day = 1;
	if (timeptr->mon>11)timeptr->mon=0;
	if (timeptr->mon<0)timeptr->mon=0;
	if (timeptr->year<0)timeptr->year=1970;
}
#ifndef HAS_RTC
uint8_t rtcRead(struct avr_tm *timeptr) {
	//fill up the the time
	//convert milliseconds
	timeptr->sec = 0;
	timeptr->min = 0;
	timeptr->hour = 0;
	timeptr->wday = 0;
	timeptr->day = 1;
	timeptr->year = 1970;
	timeptr->mon = 0;
	return 0;
}
#endif
char *avr_asctime(struct avr_tm *timeptr) {
	checkTime(timeptr);
	sprintf(ascTimeBuffer, "%s %s %2d %02d:%02d:%02d %04d\n",
			__day[timeptr->wday],__month[timeptr->mon],timeptr->day,
			timeptr->hour, timeptr->min, timeptr->sec,
			timeptr->year+1900);
	return ascTimeBuffer;
}

char *avr_ctime(avrtime_t *timep){
	return avr_asctime(avr_localtime(timep));
}
static struct avr_tm lastTime;
#define LEAP_YEAR(year) ((year%4)==0)

struct avr_tm *avr_localtime(avrtime_t *timep) {
	return avr_gmtime(timep);
}

struct avr_tm *avr_gmtime(avrtime_t *timep) {
	uint64_t epoch = *timep/1000;
	uint32_t year;
	uint8_t	month, monthLength;
	uint64_t days;
	lastTime.sec = epoch%60;
	epoch/=60;
	lastTime.min = epoch%60;
	epoch/=60;
	lastTime.hour = epoch%24;
	epoch/=24;
	lastTime.wday = (epoch+4)%7;
	year = 1970;
	days=0;
	while ((days += (LEAP_YEAR(year)?366:365))<=epoch) {
		year++;
	}
	
	lastTime.year = year-1900;
	days -= LEAP_YEAR(year) ?366:365;
	epoch -= days;
	lastTime.day_of_year = epoch;
	
	days=0;
	month=0;
	monthLength=0;
	
	for (month=0; month<12; month++) {
		if (month==1) {//february
			if (LEAP_YEAR(year)) {
				monthLength=29;
			} else {
				monthLength=28;
			}
		}else {
			monthLength = monthDays[month];
		}
		
		if (epoch>=monthLength) {
			epoch-=monthLength;
		}else {
			break;
		}
	}	
	lastTime.mon = month;
	lastTime.day = epoch+1;
	lastTime.is_dst = 0;
	
	return &lastTime;
}

avrtime_t avr_mktime(struct avr_tm *timeptr) {
	//uint32_t year=timeptr->year+1900, month=timeptr->mon,i;
	uint32_t year=timeptr->year, month=timeptr->mon,i;
	uint64_t seconds;
	
	checkTime(timeptr);
	seconds=(year-1970)*(60*60*24L*365);
	
	//add extra days for leap years
	for (i=1970; i<year; i++) {
		if (LEAP_YEAR(i)) {
			seconds+=60*60*24L;
		}
	}
	//add days for this year
	for (i=0; i<month; i++) {
		if (i==1 && LEAP_YEAR(year)) {
			seconds+= 60*60*24L*29;
		}else {
			seconds+= 60*60*24L*monthDays[i];
		}
	}
	seconds+= (timeptr->day-1)*60*60*24L;
	seconds+= timeptr->hour*60*60;
	seconds+= timeptr->min*60;
	seconds+= timeptr->sec;
	return 1000*seconds;
}

uint64_t diffTime_ms(avrtime_t *time1,avrtime_t *time2) {
	if (*time1 >= *time2) {
		return *time1 - *time2;
	}else {
		return *time2 - *time1;
	}
}

uint64_t diffTime_avr_tm(struct avr_tm *time1,struct avr_tm *time2) {
	avrtime_t t1,t2;
	checkTime(time1);
	checkTime(time2);
	t1 = avr_mktime(time1);
	t2 = avr_mktime(time2);
	return diffTime_ms(&t1, &t2);
}

bool is_time_valid(int8_t sec, 
				   int8_t minutes,
				   int8_t hours,
				   int8_t day,
				   int8_t month,
				   int16_t year) {
	bool rval = TRUE;
	if (sec > 59 || sec < 0) {
		rval = FALSE;
	}
	if (minutes > 59 || minutes < 0) {
		rval = FALSE;
	}
	if (hours > 23 || hours < 0) {
		rval = FALSE;
	}
	
	if (day > 32 || day < 0 ) {
		rval = FALSE;
	}
	if (month > 12 ||month < 0) {
		rval = FALSE;
	}
	if (year < 1970) {
		rval = FALSE;
	}
	return rval;
}

bool is_time_valid_tm(const struct avr_tm * const ltime) {
	return is_time_valid(ltime->sec,
						 ltime->min,
						 ltime->hour,
						 ltime->day,
						 ltime->mon,
						 ltime->year);
}

void parse_date(const uint32_t date, struct avr_tm *result_tm) {
	uint32_t ldate = date;
	result_tm->day = ldate/10000;
	ldate -= result_tm->day*10000;
	result_tm->mon = (ldate/100)-1;
	ldate -= (result_tm->mon+1)*100;
	result_tm->year = ldate+2000;//take note...this will need fixed a hundred years :)
}

uint32_t get_time_of_day_in_millis(struct avr_tm*ltime) {
	uint32_t rval;
	checkTime(ltime);
	rval = ltime->sec;
	rval += ltime->min*60;
	rval += ltime->hour*60*60;
	return rval*1000;
}
